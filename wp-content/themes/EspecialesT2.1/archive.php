<?php get_header(); ?>
<?php if (is_category(111)) { ?>
<div id="wrapper-full">
	<div class="breadcrumb"><?php the_breadcrumbs(); ?></div>
	<h1>Productos</h1>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div class="producto">
		<?php the_post_thumbnail('producto'); ?>
		<h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>	
	</div>
<?php endwhile; endif;  ?>
<div class="navigation"><?php wp_pagenavi(); ?></div>
</div><!-- end of wrapper-full -->
<?php exit; } if (is_category(45)) { ?>
<div id="wrapper-full">
	<div class="breadcrumb"><?php the_breadcrumbs(); ?></div>
	<h1>Blog</h1>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div class="blog">
		<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
			<h2><?php the_title(); ?></h2>	
			<?php the_post_thumbnail('thumb'); ?>
			<?php the_excerpt(); ?>
		</a>
	</div>
<?php endwhile; endif; ?>
<div class="navigation"><?php wp_pagenavi(); ?></div>
</div><!-- end of wrapper-full -->
<?php exit; } if (is_author()) { ?>
<div id="wrapperUnTercio">
	<div class="breadcrumb"><?php the_breadcrumbs(); ?></div>
	<?php if(isset($_GET['author_name'])) :
	$curauth = get_userdatabylogin($author_name);
	else :
	$curauth = get_userdata(intval($author));
	endif; ?>
	<h1>Art&iacute;culos escritos por <?php echo $curauth->nickname; ?></h1>
    <div class="authorInfo">
		<h2><?php echo $curauth->nickname; ?></h2>
		<p><strong>Sitio web</strong>: <a href="<?php echo $curauth->user_url; ?>"><?php echo $curauth->user_url; ?></a></p>
		<p><strong>Sobre <?php echo $curauth->nickname; ?></strong>: <?php echo $curauth->user_description; ?></p>
    </div>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div class="unTercio">
		<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
			<h2><?php the_title(); ?></h2>	
			<?php the_post_thumbnail('thumb'); ?>
			<?php the_excerpt(); ?>
		</a>
	</div>
<?php endwhile; endif; ?>
<div class="navigation"><?php wp_pagenavi(); ?></div>
</div><!-- end of wrapper -->
<?php get_sidebar(); ?>
<?php  exit; } if (is_tag()) { ?>
<div id="wrapperUnTercio">
	<div class="breadcrumb"><?php the_breadcrumbs(); ?></div>
	<h1>Listado de art&iacute;culos con la etiqueta <?php single_tag_title(); ?></h1>
    <div class="tagCloud">
		<?php wp_tag_cloud(); ?>
    </div>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div class="unTercio">
		<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
			<h2><?php the_title(); ?></h2>	
			<?php the_post_thumbnail('thumb'); ?>
			<?php the_excerpt(); ?>
		</a>
	</div>
<?php endwhile; endif; ?>
<div class="navigation"><?php wp_pagenavi(); ?></div>
</div><!-- end of wrapper -->
<?php get_sidebar(); ?>
<?php  exit; } else { ?>
<div id="wrapper">
	<div class="breadcrumb"><?php the_breadcrumbs(); ?></div>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<div class="dos-tercios listado">
		<h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>	
		<?php the_excerpt(); ?>
		<?php the_time('F jS, Y') ?> | <?php the_author() ?>
		</div><!-- end of dos-tercios -->
	<?php endwhile; else: ?>
<h2>No encontrado</h2>
<p>Lo sentimos, intente utilizar nuestro formulario de b&uacute;squedas.</p>
<?php endif; ?>
<div class="navigation"><?php wp_pagenavi(); ?></div>
</div><!-- end of wrapper-->
<?php get_sidebar(); ?>
<?php } ?>
<?php get_footer(); ?>
