<?php get_header(); ?>
  	  
  <section>
      <div class="pagenation-holder">
        <div class="container">
          <div class="row">
          	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <div class="col-md-6">
              <h3><?php the_title(); ?></h3>
            </div>
            <div class="col-md-6 text-right">
              <div class="pagenation_links"><?php the_breadcrumb(); ?></div>
            </div>
            <?php endwhile; else: ?>
				<?php endif; ?>
          </div>
        </div>
      </div>
  </section>
  <!--end section-->
  <div class="clearfix"></div>

  <section class="sec-padding">
    <div class="container">
      <div class="row">
      	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
			<?php the_content(__('Sigue leyendo', 'ejemplo')); ?>
			<?php wp_link_pages('before=<p class="pages">' . __('Páginas:','ejemplo') . '&after=</p>'); ?>
		<?php endwhile; ?>
			<?php else : ?>
				<p class="no-posts"><?php _e('Lo siento, no hay entradas que se ajusten a lo que busca', 'ejemplo'); ?></p>
			<?php endif; ?>
      </div>
    </div>
  </section>

<?php get_footer(); ?>
