<style type="text/css">
  #regForm {
    background-color: #ffffff;
    margin: 100px auto;
    padding: 40px;
    width: 70%;
    min-width: 300px;
  }

  /* Style the input fields */
  input {
    padding: 10px;
    width: 100%;
    font-size: 17px;
    font-family: Raleway;
    border: 1px solid #aaaaaa;
  }

  /* Mark input boxes that gets an error on validation: */
  input.invalid {
    background-color: #ffdddd;
  }

  /* Hide all steps by default: */
  .tab_acciones2 {
    display: none;
  }

  /* Make circles that indicate the steps of the form: */
  .step_acciones2 {
    height: 15px;
    width: 15px;
    margin: 0 2px;
    background-color: #bbbbbb;
    border: none;  
    border-radius: 50%;
    display: inline-block;
    opacity: 0.5;
  }

  /* Mark the active step_acciones2: */
  .step_acciones2.active {
    opacity: 1;
  }

  /* Mark the steps that are finished and valid: */
  .step_acciones2.finish {
    background-color: #4CAF50;
  }
</style>

<?php global $display_name , $user_name;
      get_currentuserinfo();

     // echo $display_name . "'s email address is: " . $user_name;
      $usuario=$user_name;
?>


<div class="tab_concurrentes">
  <button  class="tablinks_concurrentes" disabled="true" id="btn_sn_indicador_reg" onclick="open_accion_sin_indicador(event, 'Accion_sn_indicador')">ACCIONES SIN INDICADOR</button>
  <button  class="tablinks_concurrentes" disabled="true" id="btn_sn_indicador_edit" onclick="open_accion_sin_indicador(event, 'Estatus_sn_indicador')">ESTATUS</button>
  <button  class="tablinks_concurrentes" disabled="true" id="btn_sn_indicador_repdf" onclick="open_accion_sin_indicador(event,'REPORTES_SN_INDICADOR')">REPORTES</button>
</div>



<div id="intro_idicador_acciones_sin_ind" class="container" >
  <p  ><font text-align: center; font-family: Graphik-Bold  color="green" size="5">ACCIONES SIN INDICADOR</font></p>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>

</div>



<div id="Accion_sn_indicador" class="tabcontent_acc_sn_indicador">

  <form method="post" id="Form_acciones_sin_indicador" name="Form_acciones_sin_indicador" >
    <br>
   <p><font text-align: center; font-family: Graphik-Bold  color="green" size="5">ACCIONES SIN INDICADOR</font></p>
     <br>
    <!-- Petición al usuario para que seleccione el eje correspondiente. -->
    <div class="tab_acciones2">
      <br>
      <br>
      <p><label for="eje">Selecciona el eje correspondiente</label>
      <div class="select">
        <br>
        <select name="eje_principal" id="eje">  
          <option value="1">Eje 1 - Gobierno Honesto, Cercano y Moderno</option>
          <option value="2">Eje 2 - Hidalgo Próspero y Dinámico</option>
          <option value="3">Eje 3 - Hidalgo Humano e Igualitario</option>
          <option value="4">Eje 4 - Hidalgo Seguro, con Justicia y en Paz</option>
          <option value="5">Eje 5 - Hidalgo con Desarrollo Sostenible</option>
        </select>
        <br>
       
      </div>
      <br>
    </div>

    <!-- Petición del objetivo estratégico al usuario. -->
    <div class="tab_acciones2">
      <br>
       <p><label for="objetivo_est">Selecciona el objetivo estratégico correspondiente</label>
        <p><input placeholder="Objetivo estratégico" name="objetivo_est" id="objetivo_est"></p>
      <br>

    </div>

    <!-- Petición del objetivo general al usuario. -->
    <div class="tab_acciones2">
      <br>
      <br>
      <p><label for="objetivo_gen">Selecciona el objetivo general correspondiente</label>
      <p><input placeholder="Objetivo general"  name="objetivo_gen" id="objetivo_gen"></p>
    </div>

    <!-- Petición de las acciones realizadas al usuario. -->
    <div class="tab_acciones2">ACCIONES
      <br>
      <br>
      <p><label id="error_accion_1" for="reconocimiento">Agregar accion:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (250 caracteres)</strong></label>
      <p><textarea placeholder="Agregar acción 1" maxlength="250" name="accion_1" id="accion_1"></textarea></p>
      <!--
      <br>
       <p><label id="error_accion_2"  for="reconocimiento">Agregar accion 2:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (250 caracteres)</strong></label>
      <p><textarea placeho¡lder="Agregar acción 2" name="accion_2" id="accion_2"></textarea></p>
      <br>
       <p><label id="error_accion_3"  for="reconocimiento">Agregar accion 3:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (250 caracteres)</strong></label>
      <p><textarea placeholder="Agregar acción 3" name="accion_3" id="accion_3"></textarea></p>
      <br>
       <p><label id="error_accion_4"  for="reconocimiento">Agregar accion 4:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (250 caracteres)</strong></label>
      <p><textarea placeholder="Agregar acción 4" name="accion_4" id="accion_4"></textarea></p>
      <br>
       <p><label id="error_accion_5"  for="reconocimiento">Agregar accion 5:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (250 caracteres)</strong></label>
      <p><textarea placeholder="Agregar acción 5" name="accion_5" id="accion_5"></textarea></p>
    -->

     
    </div>

    <div class="tab_acciones2">
      <label style="text-align: center color: #4CAF50; font-size: 17px; font-family: Graphik-Bold" >Informacion lista</label>


      <input type="hidden"  name="oculto_sin_ind" value="1">
      <input type="hidden"  name="nom_dep_sin_ind" id="nom_dep_sin_ind">
      
     
      <h2><font font-family: Graphik-Bold color="green" size="6">ENVIAR INFORMACION</font></h2>
    </div>


    <div style="overflow:auto;">
      <div style="float:right;">
        <button type="button" id="prevBtn_Acciones2" onclick="nextPrev_Acciones2_sin_indicador(-1)">Anterior</button>
        <button type="button" id="nextBtn_Acciones2" name="enviar_Form_acciones_sin_indicador"  onclick="nextPrev_Acciones2_sin_indicador(1)">Siguiente</button>
      </div>
    </div>

    <!-- Circles which indicates the steps of the form: -->
    <div style="text-align:center;margin-top:40px;">
      <span class="step_acciones2"></span>
      <span class="step_acciones2"></span>
      <span class="step_acciones2"></span>
      <span class="step_acciones2"></span>
      <span class="step_acciones2"></span>
    </div>

  </form>

</div>

<div id="Estatus_sn_indicador" class="tabcontent_acc_sn_indicador">

      <br>
      <div class="container">
      <?php 

      global $wpdb;
      $evidencias = $wpdb->prefix . 'acciones_sin_indicador';
      $registros = $wpdb->get_results("SELECT * FROM $evidencias WHERE dependencia='$user_name'", ARRAY_A);
      foreach($registros as $registro) 
        { 
          $i=$registro['id_accion_sin_ind'];
          $nom_ind=$registro['objetivo_est'];
          $btn_ac_des=$registro['status'];
         
          ?>

         <button class="accordion"  id="acordeon_id_<?php echo($registro['id_accion_sin_ind'])?>">
          <div>
            <table>
              <col width=80%>
              <col width=20%>
                <tbody>
                 <td class="" ="contenido_datos" > <?php echo $registro['objetivo_est'];?></td> 
                 <td class="" ="contenido_datos" >

                    <?php  if($registro['status'] == 0)
                    {
                      echo("< h2style='color:#008ec5;'>En proceso</h2>");

                    }
                    else if($registro['status'] == 1)
                    {
                      echo("<h4 style='color:#0bd03e;'>Aprobado</h>");
                    } 
                     else if($registro['status'] == 2)
                    {
                      echo("<h4 style='color:#f74515;'>Corregir</h>");
                    } 
                     else if($registro['status'] == 5)
                    {
                      echo("<label style='text-align: center; font-size: 20px; font-family: Graphik-Bold ' color:#727272; '>Editar</label>");
                    } 
                      ?>                      
                  </td> 
                </tbody>
            </table>
          </div>  
         </button>

          <div class="panel">
            <div class="tab_vertical">
              <button  id="defaultOpen_vertical" class="tablinks_vertical" onclick="open_vertical_tab(event, 'Parte_sn_indicador<?php echo $registro['id_accion_sin_ind']; ?>')">EJE<i  class="fas fa-redo" 
             id="check_green_indicador_<?php echo $registro['id_accion_sin_ind']; ?>" style="font-size:20px;color:green;text-shadow:2px 2px 4px #000000;"></i></button>
              <button class="tablinks_vertical" onclick="open_vertical_tab(event, 'Parte_evidencia<?php echo $registro['id_accion_sin_ind']; ?>')">ACCIONES<i  class="fa fa-check-circle-o" 
             id="check_green_evidencia_<?php echo $registro['id_accion_sin_ind']; ?>" style="font-size:20px;color:green;text-shadow:2px 2px 4px #000000;"></i></button>
              
                <script>

                  var button_add_deisabled =document.getElementById("acordeon_id_<?php echo($registro['id_accion_sin_ind'])?>");

                        if(<?php echo($registro['status'])?>==0)
                        {
                          
                          button_add_deisabled.setAttribute("disabled", true);
                         
                        }
                        else if(<?php echo($registro['status'])?>==1)
                        {
                          
                          button_add_deisabled.setAttribute("disabled", true);
                         
                        }
                        else if(<?php echo($registro['status'])?>==2)
                        {
                           
                         
                        }
                        else if(<?php echo($registro['status'])?>==5)
                        {
                           
                         //editable
                        }


                    //parte indicador
                    if("<?php echo $registro['coment_indicador']; ?>"!="" || "<?php echo $registro['status']; ?>"==5)
                    {$('#check_green_indicador_<?php echo $registro['id_accion_sin_ind']; ?>').hide();}
                    else
                    {$('#check_green_indicador_<?php echo $registro['id_accion_sin_ind']; ?>').show();}
                   
                    //parte evidencia
                    if("<?php echo $registro['coment_evidencia']; ?>"!="" || "<?php echo $registro['status']; ?>"==5)
                    {$('#check_green_evidencia_<?php echo $registro['id_accion_sin_ind']; ?>').hide();}
                    else
                    {$('#check_green_evidencia_<?php echo $registro['id_accion_sin_ind']; ?>').show();}
                    //parte inconsistencias

                    if("<?php echo $registro['coment_inconsistencias']; ?>"!="" || "<?php echo $registro['status']; ?>"==5)
                    {$('#check_green_inconsistencias_<?php echo $registro['id_accion_sin_ind']; ?>').hide();}
                    else
                    {$('#check_green_inconsistencias_<?php echo $registro['id_accion_sin_ind']; ?>').show();}
                    //parte documentyos

                </script>


            </div>




              <form  method="post" id="signupForm_correccion" name="signupForm_correccion" > 

              <div id="Parte_sn_indicador<?php echo $registro['id_accion_sin_ind']; ?>" class="tabcontent_vertical">
                <div id="global">
                    <div id="mensajes">
                      <div class="texto">
                        <div id="info_labels_<?php echo $registro['id_accion_sin_ind'];?>">
                         <form id="form_part_indicador" method="post">
                         
                            <table>
                            <col width=20%>
                            <col width=80%>
                              <tbody>
                                <?php global $wpdb;
                                $resultado;
                                $evidencias = $wpdb->prefix . 'acciones_sin_indicador';
                                $registros_1 = $wpdb->get_results("SELECT * FROM $evidencias where id_accion_sin_ind='$i'", ARRAY_A);
                                foreach($registros_1 as $registro_1) { 
                                  ?>

                                  <tr>
                                    <br>
                                    <p><label id="error_eje_sn_edit_<?php echo($registro_1['id_accion_sin_ind'])?>" for="eje_sn_edit_<?php echo($registro_1['id_accion_sin_ind'])?>">Selecciona el eje correspondiente</label>
                                    <div class="select">
                                      <br>
                                      <select name="eje_principal_concu" id="eje_sn_edit_<?php echo($registro_1['id_accion_sin_ind'])?>">  
                                        <option value="1">Eje 1 - Gobierno Honesto, Cercano y Moderno</option>
                                        <option value="2">Eje 2 - Hidalgo Próspero y Dinámico</option>
                                        <option value="3">Eje 3 - Hidalgo Humano e Igualitario</option>
                                        <option value="4">Eje 4 - Hidalgo Seguro, con Justicia y en Paz</option>
                                        <option value="5">Eje 5 - Hidalgo con Desarrollo Sostenible</option>
                                      </select>
                                      <br>
                                    </div>
                                  </tr>

                                  <tr>
                                    <br>
                                    <p><label id="error_objetivo_est_sn_<?php echo($registro_1['id_accion_sin_ind'])?>" for="objetivo_est_sn_<?php echo($registro_1['id_accion_sin_ind'])?>">Selecciona el objetivo estratégico correspondiente</label>
                                    <p><input placeholder="Objetivo estratégico" onfocus="foco_obj_est_sn(<?php echo($registro_1['eje_principal'])?>)" value="<?php echo($registro_1['objetivo_est'])?>" name="objetivo_est_concu" id="objetivo_est_sn_<?php echo($registro_1['id_accion_sin_ind'])?>"></p>
                                    <br>

                                    <br>
                                      <p><label id="error_objetivo_gen_sn_edit_<?php echo($registro_1['id_accion_sin_ind'])?>" for="objetivo_gen_sn_edit_<?php echo($registro_1['id_accion_sin_ind'])?>">Selecciona el objetivo general correspondiente</label>
                                      <p><input placeholder="Objetivo general" onfocus="foco_obj_general_sn(<?php echo($registro_1['id_accion_sin_ind'])?>)" value="<?php echo($registro_1['objetivo_gen'])?>" name="objetivo_gen_sn_edit_<?php echo($registro_1['id_accion_sin_ind'])?>" id="objetivo_gen_sn_edit_<?php echo($registro_1['id_accion_sin_ind'])?>"></p>
                                    <br>

                                  </tr>
                                 
                                  <?php } 

                                ?>
                              </tbody>
                            </table>

                        <script type="text/javascript">

                          var val_obj_edit;
                          var bandera_eje=false;


                               if("<?php echo($registro_1['eje_principal'])?>"=="1")
                               {
                                document.getElementById("eje_sn_edit_<?php echo($registro_1['id_accion_sin_ind'])?>").selectedIndex = 0;
                               
                               }
                               else if("<?php echo($registro_1['eje_principal'])?>"=="2")
                               {
                                document.getElementById("eje_sn_edit_<?php echo($registro_1['id_accion_sin_ind'])?>").selectedIndex = 1;
                                
                               }
                               else if("<?php echo($registro_1['eje_principal'])?>"=="3")
                               {
                                document.getElementById("eje_sn_edit_<?php echo($registro_1['id_accion_sin_ind'])?>").selectedIndex = 2;
                                
                               }
                               else if("<?php echo($registro_1['eje_principal'])?>"=="4")
                               {
                                document.getElementById("eje_sn_edit_<?php echo($registro_1['id_accion_sin_ind'])?>").selectedIndex = 3;
                                
                               }
                               else if("<?php echo($registro_1['eje_principal'])?>"=="5")
                               {
                                document.getElementById("eje_sn_edit_<?php echo($registro_1['id_accion_sin_ind'])?>").selectedIndex = 4;
                               
                               }

                                //var valor_eje_edit=1;
                                $('#eje_sn_edit_<?php echo($registro_1['id_accion_sin_ind'])?>').change(function()
                                { 
                                  bandera_eje=true;
                                  if($(this).val() == "1")
                                  { 
                                    valor_eje_edit=1;
                                  }
                                  else if($(this).val() == "2")
                                  {
                                    valor_eje_edit=2;
                                  }
                                  else if($(this).val() == "3")
                                  {
                                    valor_eje_edit=3;
                                  }
                                  else if($(this).val() == "4")
                                  {
                                    valor_eje_edit=4;
                                  }
                                  else if($(this).val() == "5")
                                  {
                                    valor_eje_edit=5;
                                  }
                                });

                                function foco_obj_est_sn(id)
                                {
                                  if(bandera_eje==false)
                                  {
                                    if(id=="1")
                                    {
                                      valor_eje_edit=1;
                                    }
                                    else if (id=="2")
                                    {
                                      valor_eje_edit=2;
                                    }
                                    else if(id=="3")
                                    {
                                      valor_eje_edit=3;
                                    }
                                    else if(id=="4")
                                    {
                                      valor_eje_edit=4;
                                    }
                                    else if(id=="5")
                                    {
                                      valor_eje_edit=5;
                                    }
                                  }
                                     // alert(value_eje.value)
                                     console.log(valor_eje_edit);
                                  }

                                $( "#objetivo_est_sn_<?php echo($registro_1['id_accion_sin_ind'])?>" ).autocomplete({
                                  source : function (request, response) 
                                  {
                                    $.getJSON (
                                      // Ruta del archivo que se ejecuta.
                                      "/repositorio/wp-content/themes/EspecialesT2.1/templates/searh_objetivos.php",
                                      // Valores que requiere el formulario para hacer la busqueda.
                                      {term: request.term, eje: valor_eje_edit},
                                      // Muestra los datos de la busqueda.
                                      response
                                    );
                                  },

                                  minLength: 1,
                                  select : function (event, ui) 
                                  {
                                    // En la acción seleccionada
                                  }
                                });

                          
                                function foco_obj_general_sn(id)
                                {

                                  var obj = document.getElementById("objetivo_est_sn_"+id);
                                  val_obj_edit = obj.value
                                  val_obj_edit= val_obj_edit.substr(0,3);
                                 //  alert("get new id"+val_obj_edit);
                                 console.log(val_obj_edit);
                                }
                                  

                                $( "#objetivo_gen_sn_edit_<?php echo($registro_1['id_accion_sin_ind'])?>" ).autocomplete({

                                  source : function (request, response) 
                                  {
                                    $.getJSON (
                                      // Ruta del archivo que se ejecuta.
                                      "/repositorio/wp-content/themes/EspecialesT2.1/templates/searh_objetivos.php",
                                      // Valores que requiere el formulario para hacer la busqueda.
                                      {term: request.term, eje_obj_gen: val_obj_edit},
                                      // Muestra los datos de la busqueda.
                                      response
                                    );
                                  },

                                  minLength: 1,
                                  select : function (event, ui) 
                                  {
                                    // En la acción seleccionada
                                  }
                                });
                             
      

                              function SubmitFormData_indicador_sn(valor_entra) 
                              {

                                var eje_sn_edit_ = $("#eje_sn_edit_"+valor_entra).val();
                                var objetivo_est_sn_ = $("#objetivo_est_sn_"+valor_entra).val();
                                var objetivo_gen_sn_edit_ = $("#objetivo_gen_sn_edit_"+valor_entra).val();
                                


                                

                                 var error_objetivo_est_sn_ = document.getElementById("error_objetivo_est_sn_"+valor_entra);

                                 var error_objetivo_gen_sn_edit_ = document.getElementById("error_objetivo_gen_sn_edit_"+valor_entra);


                                if(objetivo_est_sn_.length<1)
                                {
                                 
                                  error_objetivo_est_sn_.innerHTML ="Verifica tu informacion";
                                  error_objetivo_est_sn_.classList.add("error_mensaje");

                                }
                                else if(objetivo_gen_sn_edit_.length<1)
                                {
                                 
                                  objetivo_gen_sn_edit_.innerHTML ="Verifica tu informacion";
                                  objetivo_gen_sn_edit_.classList.add("error_mensaje");


                                }
                             


                                else
                                {

                                    $.ajax({
                                    type: "POST",
                                    url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                                    data: {id_indicador:valor_entra,eje_sn_edit_: eje_sn_edit_, objetivo_est_sn_:objetivo_est_sn_,objetivo_gen_sn_edit_:objetivo_gen_sn_edit_}
                                    ,
                                    success: function(msg){
                                      alert( "Informacion Actualizada" );
                                    },
                                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                                       alert( "Error Verifica tu informacion" );
                                    }
                                  });

                                    error_objetivo_est_sn_.classList.remove("error_mensaje");
                                    error_objetivo_est_sn_.innerHTML ="Selecciona el objetivo estratégico correspondiente";

                                    objetivo_gen_sn_edit_.classList.remove("error_mensaje");
                                    objetivo_gen_sn_edit_.innerHTML ="Selecciona el objetivo general correspondiente";

                                }

                              }
                        </script>
                            
                            <input style="background-color: #555555;color: white;"  type="button" id="submit_indicador" onclick="SubmitFormData_indicador_sn(<?php echo $registro['id_accion_sin_ind'];?>)" value="Actualizar Informacion" /> </input> 
                                           
                          </form>

                       
                      </div>
                    </div>
                  </div>
                  <br>
                  <br>
                </div>
              </div>

              <div id="Parte_evidencia<?php echo $registro['id_accion_sin_ind']; ?>" class="tabcontent_vertical">
                  <div id="global">
                    <div id="mensajes">
                      <div class="texto">
                        <div id="info_labels_<?php echo $registro['id_accion_sin_ind'];?>_2">
                          <form id="form_part_indicador" method="post">
                          <table>
                            <col width=20%>
                            <col width=80%>
                            <tbody>
                              <?php global $wpdb;
                                $evidencias = $wpdb->prefix . 'acciones_sin_indicador';
                                $registros_2 = $wpdb->get_results("SELECT * FROM $evidencias where id_accion_sin_ind='$i'", ARRAY_A);

                                foreach($registros_2 as $registro_2) { ?>
                                <tr>
                                  <p><label id="error_accion_1_<?php echo($registro_1['id_accion_sin_ind'])?>"  for="accion_1">Accion :<strong class="tamanio_caracteres" id="tamanio_caracteres"> (250 caracteres)</strong></label>
                                  <textarea class="textbox_corto" maxlength="250" id="accion_1_<?php echo $registro['id_accion_sin_ind'];?>" name="accion_1" placeholder="Accion :"><?php echo($registro_1['accion_1'])?></textarea>
                                </tr>

                                  <?php } 

                                ?>
                                  </tbody>
                                </table>

                          <script type="text/javascript">
                            //ocultar el mensaje datos cargados
                          function SubmitForm_accion_edicion(valor_entra) 
                          {
                            var accion_edit = $("#accion_1_"+valor_entra).val();
                            var accion_err = document.getElementById("error_accion_1_"+valor_entra);

                            if(accion_edit.length>250)
                            {
                                                                 
                                accion_err.innerHTML ="El limite son 250 caracteres";
                                accion_err.classList.add("error_mensaje");

                            }
                            else
                            {

                             // alert(valor_entra);
                                    $.ajax({
                                      type: "POST",
                                      url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                                      data: {id_indicador:valor_entra,accion_1:accion_edit}
                                      ,
                                      success: function(msg){
                                
                                        if(msg=1)
                                        {
                                          alert("Informacion Actualizada");
                                          
                                        }
                                        
                                      },
                                      error: function(XMLHttpRequest, textStatus, errorThrown) {
                                        alert( "Error Verifica tu informacion" );
                                      }
                                    });
                                    
                                    accion_err.classList.remove("error_mensaje");
                                    accion_err.innerHTML ="Descripcion del indicador";
                              
                            }
                          }
                          </script>

                                              
                          <input style="background-color: #555555;color: white;" type="button" onclick="SubmitForm_accion_edicion(<?php echo $registro['id_accion_sin_ind'];?>)"  value="Actualizar Informacion" />  
                                        
                          </form>



                        <div><!--comentarios de correccion del evaluador-->
                          <!--
                              <label>"<?php echo($registro_1['coment_evidencia']) ?>"</label>
                            -->
                        </div>
                      </div>
                     </div>
                    </div>
                  </div>
              </div>


          


        </form>
      </div>
      <?php  
      } ?>                    

      </div>

</div>

<div id="REPORTES_SN_INDICADOR" class="tabcontent_acc_sn_indicador">

    <br>
    <h3>ACCIONES SIN INDICADOR</h3>
      
      <table>
        <col width=5%>
        <col width=65%>
        <col width=30%>
        <br>
        <br>
          <tbody>
              <?php global $wpdb;
                $ind_estrategicos = $wpdb->prefix .'acciones_sin_indicador';
                $registros_estrategicos_1 = $wpdb->get_results("SELECT * FROM $ind_estrategicos WHERE dependencia ='$user_name'", ARRAY_A);
                foreach($registros_estrategicos_1 as $registro_estrategicos_1) 
                  { ?>
                    <?php $id_evidencia=$registro_estrategicos_1['id_accion_sin_ind']; 
                     $tipo="acc_sn_indicador";
                    ?>
                   
                      <tr>
                         <td>
                        
                        <img style="text-align: center" src="http://192.168.64.2/repositorio/wp-content/uploads/2018/04/PDF.png" height="42" width="30" ></img>
                      </td>
                         <td id="txt_list_pdf_estr"> <?php    echo("<p class='demo'><a href='pdf_plugin/pdf_evidencias/crear_evidencia_pdf.php?id_evidencia= $id_evidencia&arr_ids_tacticos=0&user=$user_name&tipo=$tipo' target='_blank' class='demo'>".$registro_estrategicos_1['objetivo_est']."</a></p>");?></td> 

                         <td id="contenido_datos" >   
                         
                          <?php echo(  $registro_estrategicos_1['fecha_registro']);?>
                      
                       </td>
                  <?php } ?> 
          </tbody>
        
      </table> 

</div>


<script type="text/javascript">

  $(document).ready(function() 
  {
    //activar modulos
     $.ajax({
         type: "GET",
         datatype: 'json',
         contentType: "application/json; charset=utf-8",
        
         url: "/repositorio/wp-content/themes/EspecialesT2.1/templates/ctrl_modulos.php?nom_dep="+'<?php echo($user_name);?>',
         success: function(respuesta)
          {
            
            var arr_mod = respuesta.slice(1, -1);
            var arr_mod = arr_mod.split(",");

            var i;
            for (i = 0; i < arr_mod.length; i++) { 
              if(arr_mod[i]=="0")
              {
                 $("#btn_sn_indicador_reg").removeAttr('disabled');
                 
               
              }
              else if(arr_mod[i]=="1")
              {
                 $("#btn_sn_indicador_edit").removeAttr('disabled');
                 
              }

              else if(arr_mod[i]=="2")
              {
                 $("#btn_sn_indicador_repdf").removeAttr('disabled');
                
              }


            
              //console.log(arr_mod[i]);
            }
            
           
          },
          error: function(data)
          {

          }
         });

      });





   tabcontent_acc_sn_indicador = document.getElementsByClassName("tabcontent_acc_sn_indicador");
    for (i = 0; i < tabcontent_acc_sn_indicador.length; i++) {
        tabcontent_acc_sn_indicador[i].style.display = "none";
    }

  //document.getElementById("defaultOpen_concurrentes").click();
  
  function open_accion_sin_indicador(evt, cityName) {

    $('#intro_idicador_acciones_sin_ind').hide();
    // Declare all variables
    var i, tabcontent_acc_sn_indicador, tablinks_concurrentes;

    // Get all elements with class="tabcontent_acc_sn_indicador" and hide them
    tabcontent_acc_sn_indicador = document.getElementsByClassName("tabcontent_acc_sn_indicador");
    for (i = 0; i < tabcontent_acc_sn_indicador.length; i++) {
        tabcontent_acc_sn_indicador[i].style.display = "none";
    }

    // Get all elements with class="tablinks_concurrentes" and remove the class "active"
    tablinks_concurrentes = document.getElementsByClassName("tablinks_concurrentes");
    for (i = 0; i < tablinks_concurrentes.length; i++) {
        tablinks_concurrentes[i].className = tablinks_concurrentes[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>





<script type="text/javascript">

  // Asignación del valor del "eje" a la variable "valor_eje" para poderla usar en el "autocomplete".
  var valor_eje=1;

  $('#eje').change(function()
  { 
    if($(this).val() == "1")
    { 
      valor_eje=1;
    }
    else if($(this).val() == "2")
    {
      valor_eje=2;
    }
    else if($(this).val() == "3")
    {
      valor_eje=3;
    }
    else if($(this).val() == "4")
    {
      valor_eje=4;
    }
    else if($(this).val() == "5")
    {
      valor_eje=5;
    }


  });

  // "Autocomplete" que busca los "objetivos estratégicos" en base el "eje" seleccionado.
  $( "#objetivo_est" ).autocomplete({

    source : function (request, response) 
    {
      $.getJSON (
        // Ruta del archivo que se ejecuta.
        "/repositorio/wp-content/themes/EspecialesT2.1/templates/searh_objetivos.php",
        // Valores que requiere el formulario para hacer la busqueda.
        {term: request.term, eje: valor_eje},
        // Muestra los datos de la busqueda.
        response
      );
    },

    minLength: 1,
    select : function (event, ui) 
    {
      // En la acción seleccionada
    }
  });


  

</script>

<script type="text/javascript">
  var currentTab = 0; // Current tab is set to be the first tab (0)
  showTab_Acciones2(currentTab); // Display the crurrent tab
  var val_obj;

  function showTab_Acciones2(n) {
    // This function will display the specified tab of the form...
    var x = document.getElementsByClassName("tab_acciones2");
    x[n].style.display = "block";
    //... and fix the Previous/Next buttons:
    if (n == 0) {
      document.getElementById("prevBtn_Acciones2").style.display = "none";
    } else {
      document.getElementById("prevBtn_Acciones2").style.display = "inline";
    }
    if (n == (x.length - 1)) {
      document.getElementById("nextBtn_Acciones2").innerHTML = "Enviar";
    } else {
      document.getElementById("nextBtn_Acciones2").innerHTML = "Siguiente";
    }
    //... and run a function that will display the correct step indicator:
    fixStepIndicator_Acciones2(n)
  }

  function nextPrev_Acciones2_sin_indicador(n) {
    // This function will figure out which tab to display
    var x = document.getElementsByClassName("tab_acciones2");
    // Exit the function if any field in the current tab is invalid:
    if (n == 1 && !validateForm_Acciones2()) return false;
    // Hide the current tab:
    x[currentTab].style.display = "none";
    // Increase or decrease the current tab by 1:
    currentTab = currentTab + n;
    // if you have reached the end of the form... :



    if(currentTab==2)
    {
      var obj = document.getElementById("objetivo_est");
       val_obj = obj.value
      val_obj= val_obj.substr(0,3);
     // alert(val_obj);
    }

    if (currentTab >= x.length) 
    {
     // alert("enviar a guardar");
      
      // ... the form gets submitted:
     var x = document.getElementById("nextBtn_Acciones2");
      x.setAttribute("type", "submit");
      
    }

    showTab_Acciones2(currentTab);
  }

  function validateForm_Acciones2() {
    // This function deals with validation of the form fields
    var x, y, i, valid = true;
    x = document.getElementsByClassName("tab_acciones2");
    y = x[currentTab].getElementsByTagName("input");
    // A loop that checks every input field in the current tab:
    for (i = 0; i < y.length; i++) {
      // If a field is empty...
      if (y[i].value == "") {
        // add an "invalid" class to the field:
        y[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }
    }

        txtarea = x[currentTab].getElementsByTagName("textarea");
    // A loop that checks every input field in the current tab:
    for (i = 0; i < txtarea.length; i++) 
    {
      // If a field is empttxtarea...
      if (txtarea[i].value == "") 
      {

        if(txtarea[i].id=="accion_2" )
        {

        }
        else if(txtarea[i].id=="accion_3")
        {

        }
        else if(txtarea[i].id=="accion_4")
        {

        }
        else if(txtarea[i].id=="accion_5")
        {

        }
        else
        {
                                      // add an "invalid" class to the field:
        txtarea[i].className += " invalid";
        // and set the current valid status to false:
        valid = false;
        }


      }
   
      
      //descripcion del indicador
      else if (txtarea[i].id == "accion_1" && txtarea[i].value.length > 250 ) 
      {
          //nombre del indicador
         document.getElementById('error_accion_1').innerHTML ="El limite del campo es de 250 caracteres";
         var element = document.getElementById("error_accion_1");
         element.classList.add("error_mensaje");
          
          // add an "invalid" class to the field:
        txtarea[i].className += " invalid";
         // and set the current valid status to false
        valid = false;
      }



    }
    // If the valid status is true, mark the step as finished and valid:
    if (valid) {
      document.getElementsByClassName("step_acciones2")[currentTab].className += " finish";
    }
    return valid; // return the valid status
  }

  function fixStepIndicator_Acciones2(n) {
    // This function removes the "active" class of all steps...
    var i, x = document.getElementsByClassName("step_acciones2");
    for (i = 0; i < x.length; i++) {
      x[i].className = x[i].className.replace(" active", "");
    }
    //... and adds the "active" class on the current step:
    x[n].className += " active";
  }

      // "Autocomplete" que busca los "objetivos estratégicos" en base el "eje" seleccionado.
  $( "#objetivo_gen" ).autocomplete({

    source : function (request, response) 
    {
      $.getJSON (
        // Ruta del archivo que se ejecuta.
        "/repositorio/wp-content/themes/EspecialesT2.1/templates/searh_objetivos.php",
        // Valores que requiere el formulario para hacer la busqueda.
        {term: request.term, eje_obj_gen: val_obj},
        // Muestra los datos de la busqueda.
        response
      );
    },

    minLength: 1,
    select : function (event, ui) 
    {
      // En la acción seleccionada
    }
  });
</script>