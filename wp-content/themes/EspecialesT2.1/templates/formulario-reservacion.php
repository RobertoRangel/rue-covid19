
<style>
/* Style the tab */
    div.tab {

        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
      color: #1c1c1c;
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    div.tab button.active {
        background-color: #ccc;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
        background-color: #ccc;

    }
/* Fin Style the tab */

/* Style Acordeon */

    /* Style the buttons that are used to open and close the accordion panel */
    button.accordion {
        background-color: #33393f;
        color: #e0e8ef;
        cursor: pointer;
        padding: 18px;
        width: 100%;
        text-align: left;
        border: none;
        outline: none;
        transition: 0.4s;
    }

    /* Add a background color to the button if it is clicked on (add the .active class with JS), and when you move the mouse over it (hover) */
    button.accordion.active, button.accordion:hover {
        background-color: #283037;
    }

    /* Style the accordion panel. Note: hidden by default */
    div.panel {
        padding: 0 18px;
        background-color: white;
        display: none;
    }

/* Fin Stylo Acordeon */  

/*  form steps */

    * {
      box-sizing: border-box;
    }

    body {
      background-color: #f1f1f1;
    }


    #signupForm {
      background-color: #ffffff;
      margin: 100px auto;
      font-family: Raleway;
      padding: 40px;
      width: 70%;
      min-width: 300px;
    }

    

    h1 {
      text-align: center;  
    }

    input {
      padding: 10px;
      width: 100%;
      font-size: 17px;
      font-family: Raleway;
      border: 1px solid #aaaaaa;
    }

    /* Mark input boxes that gets an error on validation: */
    input.invalid {
      background-color: #ffdddd;
    }

    select {
      padding: 10px;
      width: 100%;
      height: 30px;
      font-size: 17px;
      font-family: Raleway;
      border: 1px solid #aaaaaa;

    }
/*
option
{
      padding: 10px;
      width: 100%;
      height: 35px;
      font-size: 17px;
      font-family: Raleway;
      border: 1px solid #aaaaaa;

    }

*/

    /* Mark select boxes that gets an error on validation: */
    select.invalid {
      background-color: #ffdddd;
    }

    textarea {
      padding: 10px;
      width: 100%;
      font-size: 17px;
      font-family: Raleway;
      border: 1px solid #aaaaaa;

    }



    /* Mark textarea boxes that gets an error on validation: */
    textarea.invalid {
      background-color: #ffdddd;
    }
    label
    {
      position: relative;
      font-size: 15px;
      font-style: italic;
      width: 100%;
      font-family: Raleway;
    }





    /* Hide all steps by default: */
    .tab {
      display: none;
      font-size: 120%;
      /*text-align: left;*/
      font-weight: bold;

    }

    button {
      background-color: #4CAF50;
      color: #ffffff;
      border: none;
      padding: 10px 20px;
      font-size: 17px;
      font-family: Raleway;
      cursor: pointer;
    }

    button:hover {
      opacity: 0.8;
    }

    #prevBtn {
      background-color: #bbbbbb;
    }

    /* Make circles that indicate the steps of the form: */
    .step {
      height: 15px;
      width: 15px;
      margin: 0 2px;
      background-color: #bbbbbb;
      border: none;  
      border-radius: 50%;
      display: inline-block;
      opacity: 0.5;
    }

    .step.active {
      opacity: 1;
    }

    /* Mark the steps that are finished and valid: */
    .step.finish {
      background-color: #4CAF50;
    } 

/*  fin form steps */

  /* tablas datos indicador*/

    table {
        border-collapse: collapse;
        width: 100%;
    }

    th, td {
        padding: 8px;
        text-align: left;
       /* border-bottom: 1px solid #283037;*/

    }
    /*
    hr{

      border-bottom: 1px solid #4CAF50;
    }
*/
    #titulo_datos
    {
      padding: 10px;
      font-weight: bold;
      font-size: 19px;
      font-family: Raleway;
     

    }

     #contenido_datos
    {
      padding: 10px;
  
      font-size: 16px;
      font-family: Raleway;
     

    }
  
  .error_mensaje
  {
    color: red;
  }






  /* tablas datos indicador*/
/*  stilo validacion*/
      #commentForm {
      width: 500px;
      }
      #commentForm label {
        width: 250px;
      }
      #commentForm label.error, #commentForm input.submit {
        margin-left: 253px;
      }
      /*
      #signupForm {
        width: 670px;
      }
      */
      #signupForm label.error {
        margin-left: 10px;
        width: auto;
        display: inline;
      }
      #newsletter_topics label.error {
        display: none;
        margin-left: 103px;
      }
/*  stilo validacion*/

/* stilo vertical tabs*/

/* Style the tab */
  div.tab_vertical {
      float: left;
      border: 1px solid #ccc;
      background-color: #4e6070;
      width: 30%;
      height: 350px;
  }

  /* Style the buttons inside the tab */
  div.tab_vertical button {
      display: block;
      background-color: inherit;
      color: #eef7ff;
      padding: 22px 16px;
      width: 100%;
      border: none;
      outline: none;
      text-align: left;
      cursor: pointer;
      transition: 0.3s;
  }

  /* Change background color of buttons on hover */
  div.tab_vertical button:hover {
      background-color: #3f5263;
  }

  /* Create an active/current "tab button" class */
  div.tab_vertical button.active {
      background-color: #3f5263;
  }

  /* Style the tab content */
  .tabcontent_vertical {
      float: left;
      padding: 2px 2px;
      border: 1px solid #ccc;
      width: 70%;
      border-left: none;
      height: 300px;
      height: 350px;
      
  }
  /*fin stilo tabs vertical*/

  /* formulario responce/*

  */


/*Style inputs, select elements and textareas */
/*
input[type=text], select, textarea{
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  resize: vertical;
}


label {
  padding: 12px 12px 12px 0;
  display: inline-block;
}

/* Style the submit button */
  input[type=submit] {
    background-color: #4CAF50;
    color: white;
    padding: 12px 20px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    float: right;
  }

  /* Style the container */
  .container {
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
  }

  /* Floating column for labels: 25% width */
  .col-25 {
    float: left;
    width: 25%;
    margin-top: 6px;
  }

  /* Floating column for inputs: 75% width */
  .col-75 {
    float: left;
    width: 75%;
    margin-top: 6px;
  }

  /* Clear floats after the columns */
  .row:after {
    content: "";
    display: table;
    clear: both;
  }

  /* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
  @media (max-width: 600px) {
    .col-25, .col-75, input[type=submit] {
      width: 100%;
      margin-top: 0;
    }
  }



#global {
  height: 100%;
  width: 100%;
  border: 1px solid #ddd;
  background: #f1f1f1;
  overflow-y: scroll;
}
#mensajes {
  height: auto;
  width: 100%;
}
.texto {
  
  background:#fff;
}

/* show hide elemts revision*/




</style>



<?php /* Nombre del usuario activo*/ 
  $all_users = get_users();
  $user_name;
  $num_revision=0;
  foreach ($all_users as $user) 
  {
      
      $user_name=$user->display_name;
     // echo ($user_name);
  }
?>



<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>-->

<div class="tab">
  <button class="tablinks" onclick="open_tabs(event, 'DEFINIR_EVIDENCIA')">DEFINIR EVIDENCIA</button>

  <button class="tablinks"  onclick="open_tabs(event, 'STATUS')">STATUS</button>
</div>




<div id="INDICADORES" class="tabcontent">


    
  
</div>

<div id="STATUS" class="tabcontent">

</div>





<!--  JQUERY DatePIKERS-->

 <script>
  $( function() 
  {
    $( "#datepicker1" ).datepicker();
    $( "#datepicker2" ).datepicker();

  });
  </script>



<script>
  
  $('#existe_expediente_doc_def').change(function(){ 
    if($(this).val() == "Si"){
      //alert("Si");
      //function_mostrar_1();
      $('#parte1').hide();
      $('#parte2').show();
    }
    if($(this).val() == "No"){
       // alert("No");
      $('#parte2').hide();
      $('#parte1').show();
  
    }

});



</script>


  



