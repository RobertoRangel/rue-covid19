  <style type="text/css">


            #regForm 
            {
              background-color: #ffffff;
              margin: 100px auto;
              padding: 40px;
              width: 70%;
              min-width: 300px;
            }

            /* Style the input fields */
            input {
              padding: 10px;
              width: 100%;
              font-size: 17px;
              font-family: Raleway;
              border: 1px solid #aaaaaa;
            }

            /* Mark input boxes that gets an error on validation: */
            input.invalid {
              background-color: #ffdddd;
            }

            /* Hide all steps by default: */
            .tab_pid {
              display: none;
            }

            /* Make circles that indicate the steps of the form: */
            .step_pids {
              height: 15px;
              width: 15px;
              margin: 0 2px;
              background-color: #bbbbbb;
              border: none; 
              border-radius: 50%;
              display: inline-block;
              opacity: 0.5;
            }

            /* Mark the active step_pids: */
            .step_pids.active {
              opacity: 1;
            }

            /* Mark the step_pidss that are finished and valid: */
            .step_pids.finish {
              background-color: #4CAF50;
            }


            /*TABS*/

            /* Style the tab */
          .tab_tac_pids {
                 overflow: hidden;
              border: 1px solid #ccc;
              background-color: #
          }

          /* Style the buttons that are used to open the tab_tac_pids content */
          .tab_tac_pids button {
           background-color: inherit;
              float: left;
              border: none;
              outline: none;
              cursor: pointer;
              padding: 14px 16px;
              transition: 0.3s;
          }

          /* Change background color of buttons on hover */
          .tab_tac_pids button:hover {
               background-color: #ffffff;
          }

          /* Create an active/current tab_tac_pidslink class */
          .tab_tac_pids button.active {
             background-color: #1b2935;
            
            color: #ffffff;
          }

          /* Style the tab content */
          .tabcontent_tac_pid {
              display: none;
            padding: 6px 12px;
            border: 3px solid #ccc;
            /*border-top: none;*/
            background-color: #ffffff;
          }
          /*TABS VERTICAL*/

          /* Style the buttons that are used to open and close the accordion panel */
          .accordion {
              background-color: #eee;
              color: #444;
              cursor: pointer;
              padding: 18px;
              width: 100%;
              text-align: left;
              border: none;
              outline: none;
              transition: 0.4s;
          }

          /* Add a background color to the button if it is clicked on (add the .active class with JS), and when you move the mouse over it (hover) */
          .active, .accordion:hover {
              background-color: #ccc;
          }

          /* Style the accordion panel. Note: hidden by default */
          .panel {
              padding: 0 18px;
              background-color: white;
              display: none;
              overflow: hidden;
          }
</style>


<?php global $display_name , $user_name;
      get_currentuserinfo();
      $usuario=$user_name;
?>


<!--  Mensajes emergentes-->
    <div id="dialog-form_pid" title="Municipios">
      <p class="validateTips">Selecciona los Municipios</p>
      <form  id="form_cheks_municipios">
        <fieldset>
          <p id="check_list_municipios_pid"></p>

          <script>
            var municipios_arr = ["Acatlán",
               "Acaxochitlán",                //1
               "Actopan",                     //2
               "Agua Blanca de Iturbide",     //3
               "Ajacuba",                     //4
               "Alfajayucan",                 //5
               "Almoloya",                    //6
               "Apan",                        //7
               "Atitalaquia",                 //8
               "Atlapexco",                   //9
               "Atotonilco de Tula",          //10
               "Atotonilco el Grande",        //11
               "Calnali",                     //12
               "Cardonal",                    //13
               "Chapantongo",                 //14 
               "Chapulhuacán",                //15
               "Chilcuautla",                 //16
               "Cuautepec de Hinojosa",       //17
               "El Arenal",                   //18
               "Eloxochitlán",                //19
               "Emiliano Zapata",             //20
               "Epazoyucan",                  //21
               "Francisco I. Madero",         //22
               "Huasca de Ocampo",            //23
               "Huautla",                     //24
               "Huazalingo",                  //25
               "Huehuetla",                   //26
               "Huejutla de Reyes",           //27
               "Huichapan",                   //28
               "Ixmiquilpan",                 //29
               "Jacala de Ledezma",           //30
               "Jaltocán",
               "Juárez Hidalgo",
               "La Misión",
               "Lolotla",
               "Metepec",
               "Metztitlán",
               "Mineral de la Reforma",
               "Mineral del Chico",
               "Mineral del Monte",
               "Mixquiahuala de Juárez",
               "Molango de Escamilla",
               "Nicolás Flores",
               "Nopala de Villagrán",
               "Omitlán de Juárez",
               "Pachuca de Soto",
               "Pacula",
               "Pisaflores",
               "Progreso de Obregón",
               "San Agustín Metzquititlán",
               "San Agustín Tlaxiaca",
               "San Bartolo Tutotepec",
               "San Felipe Orizatlán",
               "San Salvador",
               "Santiago de Anaya",
               "Santiago Tulantepec de Lugo Guerre",
               "Singuilucan",
               "Tasquillo",
               "Tecozautla",
               "Tenango de Doria",
               "Tepeapulco",
               "Tepehuacán de Guerrero",
               "Tepeji del Río de Ocampo",
               "Tepetitlán",
               "Tetepango",
               "Tezontepec de Aldama",
               "Tianguistengo",
               "Tizayuca",
               "Tlahuelilpan" ,
               "Tlahuiltepa",
               "Tlanalapa",
               "Tlanchinol",
               "Tlaxcoapan",
               "Tolcayuca",
               "Tula de Allende",
               "Tulancingo de Bravo",
               "Villa de Tezontepec",
               "Xochiatipan",
               "Xochicoatlán",
               "Yahualica",
               "Zacualtipán de angeles",
               "Zapotlán de Juárez",
               "Zempoala",
               "Zimapán"];
            var text = "";
            var i;
            text+="<ul>"
            for (i = 0; i < municipios_arr.length; i++) 
            {
               text +='<div>'+  '<label class="checkbox-inline "> '+'<input type="checkbox"   class="check_muni" id="check_evi_pid_'+municipios_arr[i]+'"  name="'+municipios_arr[i]+'">'+municipios_arr[i]+'</label>'   +'</div>';


            }

            text+="</ul>"
            document.getElementById("check_list_municipios_pid").innerHTML = text;
         
          </script>
          <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
        </fieldset>
      </form>
    </div>
 <!-- Mensajes Entidades -->
    <div id="dialog-form-entidades_pid" title="Entidades">
      <p class="validateTips">Selecciona las Entidades</p>
      <form  id="form_cheks_entidades">
        <fieldset>
          <p id="check_list_entidades_pid"></p>

          <script>

            var my_data = $("#dialog-form-entidades_pid" ).data('indi');
           // alert(my_data);

            var entidades_arr = ["Aguascalientes",
                "Baja California",
                "Baja California Sur",
                "Campeche",
                "Chiapas",
                "Chihuahua",
                "Ciudad de México",
                "Coahuila",
                "Colima",
                "Durango",
                "Estado de México",
                "Guanajuato",
                "Guerrero",
                "Hidalgo",
                "Jalisco",
                "Michoacán",
                "Morelos",
                "Nayarit",
                "Nuevo León",
                "Oaxaca",
                "Puebla",
                "Querétaro",
                "Quintana Roo",
                "San Luis Potosí",
                "Sinaloa",
                "Sonora",
                "Tabasco",
                "Tamaulipas",
                "Tlaxcala",
                "Veracruz",
                "Yucatán",
                "Zacatecas"];
            var text = "";
            var i;
            text+="<ul>"
            for (i = 0; i < entidades_arr.length; i++) 
            {
               text +='<label class="checkbox-inline ">'+'<input type="checkbox"   class="check_enti" id="check_evi_pid_'+entidades_arr[i]+'"  name="'+entidades_arr[i]+'">'+entidades_arr[i]+'</label>';
            }
            text+="</ul>"
            document.getElementById("check_list_entidades_pid").innerHTML = text;
            

          </script>
          <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
        </fieldset>
      </form>
    </div>

    <div id="dialog-message-lugar_pid" title="Lugar donde se realizarion la acciones">
      <div class="btn-group">
        <button  type="button" onclick="open_estados_pid()" class="btn btn-primary">Estatal</button>
        <button type="button" onclick="open_municipios_pid()" class="btn btn-primary">Municipales</button>
        <button type="button" onclick="open_localidades_pid()" class="btn btn-primary">Localidades</button>
      </div>
                          
    </div>

    <div id="dialog-message-adjuntar-localidades_pid" title="Adjuntar archivo de localidades">
      <p>
        <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
       <b>Especifiquen los lugares donde se realizaron las acciones PID</b>
      </p>
      
      <div id="doc_res1_pid">
          <div id="upload_doc_resultados_1_pid" name="upload_doc_resultados_1_pid">  </div>
      </div>

      <div id="doc_res2_pid">
          <div id="upload_doc_resultados_2_pid" name="upload_doc_resultados_2_pid">  </div>
      </div>

      <div id="doc_res3_pid">
          <div id="upload_doc_resultados_3_pid" name="upload_doc_resultados_3_pid">  </div>
      </div>

      <div id="doc_res4_pid">
          <div id="upload_doc_resultados_4_pid" name="upload_doc_resultados_4_pid">  </div>
      </div>

      <div id="doc_res5_pid">
          <div id="upload_doc_resultados_5_pid" name="upload_doc_resultados_5_pid">  </div>
      </div>

      <p><label >Descargar el modelo de como organizar la informacion</label></p>
      <a href="/repositorio/upload_files_plugin/download_file_demo.php">Descargar</a>

    </div>





<!-- menu principal-->
<div class="tab_tac_pids">
  <button id="btn_acc_tac_pid_reg" class="tablinks_tac_pids"  onclick="open_accion_tac_pid(event, 'nuevo_pid')">NUEVA ACCION</button>
  <button id="btn_acc_tac_pid_edit" class="tablinks_tac_pids"  onclick="open_accion_tac_pid(event, 'Estatus_tac_pid')">ESTATUS</button>
  <button id="btn_acc_tac_pid_repdf" class="tablinks_tac_pids"  onclick="open_accion_tac_pid(event, 'reporte_pids')">REPORTES</button>
</div>
<!-- fin menu principal-->
<div id="intro_idicador_pids_tac_pids" class="container" >
  <p  ><font text-align: center; font-family: Graphik-Bold  color="green" size="5">COVID-19</font></p>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>

</div>
         <!--inicio nuevo pid-->
<div id="nuevo_pid" class="tabcontent_tac_pid">
  <form  method="post" id="Form_definir_evidencia" name="Form_definir_evidencia" >                               
    <br>
    <p><font text-align: center; font-family: Graphik-Bold  color="green" size="5">COVID 19</font></p>

      <div class="tab_pid">
        <br><br>
	
	<p><label id="error"  for="resultado_1_pid">Medida<strong class="tamanio_caracteres" id="tamanio_caracteres">
                (400 caracteres)</strong></label>
              <textarea  class="textbox_corto" maxlength="400" id="resultado_1_pid" name="resultado_1_pid" placeholder="Ejemplo: Para orientar a la población sobre posibles casos de COVID-19 establecimos un protocolo para la atención de llamadas en el 911 con 12 médicos en 3 turnos."></textarea>
              <!--<button  id="btn_def_lugar_1"  onclick="open_lugares_pid(1,0)" type="button" >Definir lugar 1</button>--></p>
	<label for="semaforo" >¿La medida se mantiene o se implementa?<strong class="tamanio_caracteres" id="tamanio_caracteres"></strong></label>

           <div class="select">
            <select class="inputDisabled"   name="semaforo_covid" id="semaforo_covid">
              <option value="Im">Implementada</option>
              <option value="Sm">Se Mantiene</option>
            </select>
           </div>

        <p><label id="error_resultado_1_pid"  for="resultado_1_pid">Elementos cuantitativos<strong class="tamanio_caracteres" id="tamanio_caracteres">
                (400 caracteres)</strong></label></p>

           <textarea class="textbox_corto" maxlength="400" id="que_se_hizo" name="que_se_hizo" placeholder="¿Qué se hizo? (En coordinación con la Secretaría de Salud se implementó un protocolo de atención de llamadas con sospecha de COVID-19)"></textarea>

           <textarea class="textbox_corto" maxlength="400" id="cuanto_se_hizo" name="cuanto_se_hizo" placeholder="¿Cuánto se hizo? (Un protocolo con 12 médicos en 3 turnos)"></textarea>

           <textarea class="textbox_corto" maxlength="400" id="a_que_poblacion" name="a_que_poblacion" placeholder="¿A qué población va dirigida? (A todas las personas con sospecha de estar infectadas)"></textarea>

           <p><label id="error_resultado_1_pid"  for="resultado_1_pid">Impacto en la contingencia<strong class="tamanio_caracteres" id="tamanio_caracteres">
                (400 caracteres)</strong></label></p>

           <textarea class="textbox_corto" maxlength="400" id="beneficia_accion" name="beneficia_accion" placeholder="¿En qué beneficia la medida? (Permite orientar a la población y, de identificarse, atender casos positivos)"></textarea>
	
	<p><label id="error_resultado_2_pid"  for="resultado_2_pid">Impacto en el Estado<strong class="tamanio_caracteres" id="tamanio_caracteres">
                (400 caracteres)</strong></label></p>

           <textarea class="textbox_corto" maxlength="400" id="beneficia_accion2" name="beneficia_accion2" placeholder="¿Como afecta esta medida a la economía, al desarrollo social, a los resultados esperados en la dependencia?"></textarea>
	
       

           <p><label id="error_resultado_1_pid"  for="resultado_1_pid">Lugar<strong class="tamanio_caracteres" id="tamanio_caracteres">
                (400 caracteres)</strong></label></p>

           <textarea class="textbox_corto" maxlength="400" id="donde_se_llevo" name="donde_se_llevo" placeholder="¿Dónde se llevó a cabo la medida? (En el C5i a través de la atención del 911)"></textarea>

           <label for="existe_incidencia_covid" >¿Existe alguna incidencia importante que se deba reportar?<strong class="tamanio_caracteres" id="tamanio_caracteres"></strong></label>

           <div class="select">
            <select class="inputDisabled"   name="existe_incidencia_covid" id="existe_incidencia_covid">
              <option value="No">No</option>
              <option value="Si">Si</option>
            </select>
           </div>	

           <script type="text/javascript">
             
                $('#existe_incidencia_covid').change(function()
                { 

                  if($(this).val() == "Si")
                  {
                  $('#existe_alguna_incidencia_covid').show();

                  }
                  if($(this).val() == "No"){
                  $('#existe_alguna_incidencia_covid').hide();
                  }
                });         

           </script>
           <br>
           <br>

          <textarea value= "Ninguna" hidden="true" id="existe_alguna_incidencia_covid" name="existe_alguna_incidencia_covid" placeholder="Esta medida presento alguna incidencia que sea importante reportar"></textarea>

<p><label id="bullet"  for="bullet">Bullet<strong class="tamanio_caracteres" id="tamanio_caracteres">
                (400 caracteres)</strong></label></p>

           <textarea class="textbox_corto" maxlength="400" id="bullet" name="bullet" placeholder="Redacta tu bullet"></textarea>

      </div>
      
      <div style="text-align: left;" class="tab_pid">
              <br><br>

              <p><label id="error_resultado_1_pid"  for="resultado_1_pid"> 
                Presiona Anterior para realizar algún cambio o Siguiente para cargar la información.

              </label></p>
<div>
                  <table>
                    <col width=50%>
                    <col width=50%>
                      <tbody>
                        <tr>
                           <td style="text-align:center;" >
                          <h3>Adjuntar evidencias</h5>
                          <br>
                          <p><label for="upload_adjuntar_evidencias_pid">Puedes seleccionar multiples archivos</label></p>
                          <div style="text-align:center;" id="upload_adjuntar_evidencias_pid" name="upload_adjuntar_evidencias_pid">  </div>
                        </td>
          

                        </tr>
                       
                         
                      </tbody>
                  </table>
              </div>  

	<!-- iniscript -->


<script type="text/javascript">
	$("#upload_adjuntar_evidencias_pid").uploadFile
            ({
                url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                fileName:"myfile",
                maxFileCount:60,
                showCancel: true,
                showDone: true,
                showError: true,
                maxFileSize:1000*600000,
                showPreview:true,
                previewHeight: "100px",
                previewWidth: "100px",
                formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":"evidencias"},
                onSuccess:function(files,data,xhr,pd)
                  {
                    //alert("archivo guardado falta actualizar la tabla de archivos")
 
                  }  
              
            });
</script>
 <!-- finscript -->

		<!--
              <br><br>
            
              <p><label id="error"  for="resultado_1_pid">acciones 1:<strong class="tamanio_caracteres" id="tamanio_caracteres">
                (350 caracteres)</strong></label>
              <textarea  class="textbox_corto" maxlength="350" id="resultado_1_pid" name="resultado_1_pid" placeholder="acciones 1:"></textarea> 
              <!--<button  id="btn_def_lugar_1"  onclick="open_lugares_pid(1,0)" type="button" >Definir lugar 1</button></p>
              <label id="datos_cargados_lug_1_pid" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
              <input id="res1_lugares_pid" name="res1_lugares_pid" hidden="true" > -->
<!--
              <p><label id="error_resultado_2_pid" id="error_resultado_2_pid"  for="resultado_2_pid">acciones 2:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
              <textarea class="textbox_corto" maxlength="350" id="resultado_2_pid" name="resultado_2_pid" placeholder="acciones 2:"></textarea>
              <!--<button  id="btn_def_lugar_2" class="btn_def_lugar" onclick="open_lugares_pid(2,0)" type="button" >Definir lugar 2</button></p>
              <label id="datos_cargados_lug_2_pid" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
              <input id="res2_lugares_pid" name="res2_lugares_pid" hidden="true" >

              <p><label id="error_resultado_3_pid"  for="resultado_3_pid">acciones 3:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
              <textarea class="textbox_corto" maxlength="350" id="resultado_3_pid" name="resultado_3_pid" placeholder="acciones 3:"></textarea>
              <!--<button class="btn_def_lugar" id="btn_def_lugar_3" onclick="open_lugares_pid(3,0)" type="button" >Definir lugar 3</button></p>
              <label id="datos_cargados_lug_3_pid" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
              <input id="res3_lugares_pid" name="res3_lugares_pid" hidden="true" > 

              <p><label id="error_resultado_4_pid" id="label_resultado_4_pid"  for="resultado_4_pid">acciones 4:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
              <textarea class="textbox_corto" maxlength="350" id="resultado_4_pid" name="resultado_4_pid" placeholder="acciones 4:"></textarea>
              <!--<button id="btn_def_lugar_4" class="btn_def_lugar" onclick="open_lugares_pid(4,0)"  type="button" >Definir lugar 4</button></p>
              <label id="datos_cargados_lug_4_pid" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
              <input id="res4_lugares_pid" name="res4_lugares_pid"  hidden="true" > 

              <p><label id="error_resultado_5_pid"  for="resultado_5_pid">acciones 5:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
              <textarea class="textbox_corto" maxlength="350" id="resultado_5_pid" name="resultado_5_pid" placeholder="acciones 5:"></textarea>
              <!--<button id="btn_def_lugar_5" class="btn_def_lugar" onclick="open_lugares_pid(5,0)"  type="button" >Definir lugar 5</button></p>
              <label id="datos_cargados_lug_5_pid" class="datos_cargados_lugar_resultados" >Datos Cargados</label>

              <input id="res5_lugares_pid" name="res5_lugares_pid" hidden="true" >
		
-->

              <br><br>
            
      </div> 


      <div style="text-align: left;" class="tab_pid">

        <h1>Archivos</h1>
        <br>
          <p><input style="display: none;" name="nom_usuario_pid" id="nom_usuario_pid"  value="<?php echo ($user_name); ?>" ></p>
          <input  name="oculto" value="1" style="display: none;"  >

      </div>







    <div style="overflow:auto;">
      <div style="float:right;">
        <button type="button" id="prevBtn_pid" onclick="nextPrev_covid(-1)">Anterior</button>
        <button type="button" name="enviar_form_covid" id="nextBtn_pid" onclick="nextPrev_covid(1)">Siguiente</button>
      </div>
    </div>

    <div style="text-align:center;margin-top:40px;">
      <span class="step_pids"></span>
      <span class="step_pids"></span>
      <span class="step_pids"></span>
      
   
     
    </div>
  </form>
</div>

          <!--fin nuevo pid-->  
		<!-- inicio edicion pids-->
<div id="Estatus_tac_pid" class="tabcontent_tac_pid">
  
  <br>
  <div class="container">
    <?php 
    global $wpdb;
    $pids = $wpdb->prefix . 'covid';
    $registros = $wpdb->get_results("SELECT * FROM $pids WHERE nombre_usuario_covid='$user_name'", ARRAY_A);

      foreach($registros as $registro) 
        { 
          $i=$registro['id_covid'];
          $nom_ind=$registro['nom_indicador_pid'];
          $btn_ac_des=$registro['estatus_pid'];
         // echo($nom_ind);
        ?>

        <button class="accordion"  id="acordeon_id_<?php echo($registro['id_covid'])?>">
          <div>
            <table>
              <col width=80%>
              <col width=20%>
                <tbody>
                 <td class="" ="contenido_datos" > <?php echo $registro['medida1'];?></td> 
                 <td class="" ="contenido_datos" >

                    <?php  if($registro['estatus_pid'] == 0)
                    {
                      echo("<h2 style='text-align: center; color:#727272; font-size: 20px; font-family: Graphik-Bold ''> EDITAR </h2>");
                    }
                    else if($registro['estatus_pid'] == 1)
                    {
                      echo("<h4 style='color:#0bd03e;'>Aprobado</h>");
                    } 
                     else if($registro['estatus_pid'] == 2)
                    {
                      echo("<h4 style='color:#f74515;'>Corregir</h>");
                    } 
                     else if($registro['estatus_pid'] == 5)
                    {
                      echo("<label style='text-align: center; font-size: 20px; font-family: Graphik-Bold ' color:#727272; '>Editar</label>");
                    } 
                      ?>                      
                  </td> 
                </tbody>
            </table>
          </div>  
        </button>

        <div class="panel">
          <div class="tab_vert">

            <button class="tablinks_vertical" onclick="open_vertical_tab(event, 'Parte_formulario_pids<?php echo $registro['id_covid']; ?>'); get_id_select('<?php echo $registro['id_covid']; ?>','<?php echo $registro['nom_indicador_pid']; ?>')">MEDIDAS <i   id="check_green_evidencia_<?php echo $registro['id_covid']; ?>" style="font-size:20px;color:green;text-shadow:2px 2px 4px #000000;"></i></button>

             

              <button class="tablinks_vertical" onclick="open_vertical_tab(event, 'Parte_evidencias_pids<?php echo $registro['id_covid']; ?>')">ARCHIVOS<i   id="check_green_evidencia_<?php echo $registro['id_covid']; ?>" style="font-size:20px;color:green;text-shadow:2px 2px 4px #000000;"></i></button>



<!--Nuevo material---------->


  <script type="text/javascript">



                var id_ind_localidades;

                function get_id_localidades(nom_ind)
                {
                  id_ind_localidades=nom_ind;
                  //alert(id_ind_localidades);


                  $("#upload_doc_resultados_1_pid").uploadFile
                ({
                  
                    url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                    fileName:"myfile",
                    maxFileCount:60,
                    showCancel: true,
                    showDone: true,
                    showError: true,
                    maxFileSize:1000*600000,
                    showPreview:true,
                    previewHeight: "100px",
                    previewWidth: "100px",
                    formData: {"nom_evidencia":id_ind_localidades,"usuario":"<?php echo $user_name;?>","indice":1}, 

                    onSuccess:function(files,data,xhr,pd)
                      {
                        //alert("archivo guardado falta actualizar la tabla de archivos")
     
                      } 
                }); 

                $("#upload_doc_resultados_2_pid").uploadFile
                ({
                  
                    url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                    fileName:"myfile",
                    maxFileCount:60,
                    showCancel: true,
                    showDone: true,
                    showError: true,
                    maxFileSize:1000*600000,
                    showPreview:true,
                    previewHeight: "100px",
                    previewWidth: "100px",
                    formData: {"nom_evidencia":id_ind_localidades,"usuario":"<?php echo $user_name;?>","indice":2},
                    onSuccess:function(files,data,xhr,pd)
                      {
                        //alert("archivo guardado falta actualizar la tabla de archivos")
     
                      }  
                  
                }); 

                $("#upload_doc_resultados_3_pid").uploadFile
                ({
                  
                    url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                    fileName:"myfile",
                    maxFileCount:60,
                    showCancel: true,
                    showDone: true,
                    showError: true,
                    maxFileSize:1000*600000,
                    showPreview:true,
                    previewHeight: "100px",
                    previewWidth: "100px",
                    formData: {"nom_evidencia":id_ind_localidades,"usuario":"<?php echo $user_name;?>","indice":3},
                    onSuccess:function(files,data,xhr,pd)
                      {
                        //alert("archivo guardado falta actualizar la tabla de archivos")
     
                      }   
                  
                }); 
      
                $("#upload_doc_resultados_4_pid").uploadFile
                ({
                  
                    url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                    fileName:"myfile",
                    maxFileCount:60,
                    showCancel: true,
                    showDone: true,
                    showError: true,
                    maxFileSize:1000*600000,
                    showPreview:true,
                    previewHeight: "100px",
                    previewWidth: "100px",
                    formData: {"nom_evidencia":id_ind_localidades,"usuario":"<?php echo $user_name;?>","indice":4} ,
                    onSuccess:function(files,data,xhr,pd)
                      {
                        //alert("archivo guardado falta actualizar la tabla de archivos")
     
                      }  
                  
                }); 
                                        
                $("#upload_doc_resultados_5_pid").uploadFile
                ({


                  
                    url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                    fileName:"myfile",
                    maxFileCount:60,
                    showCancel: true,
                    showDone: true,
                    showError: true,
                    maxFileSize:1000*600000,
                    showPreview:true,
                    previewHeight: "100px",
                    previewWidth: "100px",
                    formData: {"nom_evidencia":id_ind_localidades,"usuario":"<?php echo $user_name;?>","indice":5},
                    onSuccess:function(files,data,xhr,pd)
                      {
                        //alert("archivo guardado falta actualizar la tabla de archivos")
     
                      }  
                  
                }); 
                }
                

 </script>

        </div>

          <form  method="post" > 
          <!--Nuevo material-  apartado de archivos subidos--------->
          <div id="Parte_archivos_pids<?php echo $registro['id_covid']; ?>" class="tabcontent_vert">

          <div id="global">
            <div id="mensajes">
              <div class="texto">
                      
                <?php global $wpdb;
                  $id_indi= $registro['nom_indicador_pid'];
                 
                  $tipo="acuse";

                  $archivos = $wpdb->prefix . 'archivos_dependencias';
                  $archivos = $wpdb->get_results(" SELECT * FROM $archivos WHERE indicador='$id_indi' AND dependecia='$user_name'", ARRAY_A);
                    foreach($archivos as $archivo) 
                      {?>
                                           
                      <div id="info_labels_<?php echo $archivo['indicador'];?>_5">


                      <table>
                        <col width=1%>
                        <col width=29%>

                        <col width=30%>
                        
                        <col width=20%>
                        <col width=3%>
                        
                      
                        <tbody>
                                  <td>
                                    
                                  </td>

                                  <td> <p> <label style="text-align: left; font-size: 14px; font-family: Graphik-Bold" ><?php echo($archivo['filename']); ?></label>  </p> </td>

                                  <td>

                                     <p> <label style="text-align: left; font-size: 14px; font-family: Graphik-Bold" ><?php echo($archivo['comentario_archivo']); ?></label>  </p> 
                                    
                                  </td>
                                  <br>

                                  <td style="text-align: right;"> <a download  onclick="download_file_eva_org(<?php echo($archivo['id_archivo']); ?>,'<?php echo($archivo['ruta_archivo']); ?>',encodeURIComponent('<?php echo($archivo['filename']); ?>'))"   id="down_btn_eva_org_<?php echo($archivo['id_archivo']); ?>"  class="btn_down"><i class="fa fa-download"  style="text-align: right; color:green; font-size: 1.5em;" ></i></a></td>


                                  <td>
                                    
                                  </td>
                                 
                        </tbody>
                      </table>

                    </div>
                    <?php }?>
             </div>
            </div>
          </div>
          </div>
          <div id="Parte_formulario_pids<?php echo $registro['id_covid']; ?>" class="tabcontent_vert">

              <div id="global">
                <div id="mensajes">
                  <div class="texto">

                    <div id="info_labels_<?php echo $archivo['id_covid'];?>_5">
                      <table>
                        <col width=30%>
                        <col width=70%>
                      
                        <tbody>
                               <br>
                          <tr>
                              
                              <label id="error"  for="resultado_1_pid_EDIT_covid_<?php echo($registro['id_covid'])?>">Medida<strong class="tamanio_caracteres" id="tamanio_caracteres_EDIT_<?php echo($registro['id_covid'])?>">
                                (350 caracteres)</strong></label>
                              <textarea  class="textbox_corto" maxlength="350" id="resultado_1_pid_EDIT_covid_<?php echo($registro['id_covid'])?>" name="resultado_1_pid_EDIT_covid_<?php echo($registro['id_covid'])?>" placeholder="Ejemplo: Para orientar a la población sobre posibles casos de COVID-19 establecimos un protocolo para la atención de llamadas en el 911 con 12 médicos en 3 turnos."><?php echo($registro['medida1'])?></textarea>


                              <label for="semaforo_covid_EDIT_covid_<?php echo($registro['id_covid'])?>" >¿La medida se mantiene o se implementa?<strong class="tamanio_caracteres" id="tamanio_caracteres_EDIT_covid_<?php echo($registro['id_covid'])?>"></strong></label>

                               <div class="select">
                                <select class="inputDisabled"   name="semaforo_covid_EDIT_covid_<?php echo($registro['id_covid'])?>" id="semaforo_covid_EDIT_covid_<?php echo($registro['id_covid'])?>">
                                  <option value="Im">Implementada</option>
                                  <option value="Sm">Se Mantiene</option>
                                </select>
                               </div>



                              <script type="text/javascript">

                                var x = document.getElementById("semaforo_covid_EDIT_covid_<?php echo($registro['id_covid'])?>").value;
                                 alert("se modifica");

                                 if(x=="Im")
                                 {
                                  document.getElementById('semaforo_covid_EDIT_covid_<?php echo($registro['id_covid'])?>').value=Implementada;
                                 }
                                 else
                                 {
                                    document.getElementById('semaforo_covid_EDIT_covid_<?php echo($registro['id_covid'])?>').value=Se Mantiene;
                                 }

                               </script>


                          

                               <p><label id="error_resultado_1_pid_EDIT_covid_<?php echo($registro['id_covid'])?>"  for="que_se_hizo_EDIT_covid_<?php echo($registro['id_covid'])?>">Elementos cuantitativos</label></p>

                               <textarea class="textbox_corto" maxlength="350" id="que_se_hizo_EDIT_covid_<?php echo($registro['id_covid'])?>" name="que_se_hizo_EDIT_covid_<?php echo($registro['id_covid'])?>" placeholder="¿Qué se hizo? (En coordinación con la Secretaría de Salud se implementó un protocolo de atención de llamadas con sospecha de COVID-19)"><?php echo($registro['que_se_hizo'])?></textarea>

                               <textarea class="textbox_corto" maxlength="350" id="cuanto_se_hizo_EDIT_covid_<?php echo($registro['id_covid'])?>" name="cuanto_se_hizo_EDIT_covid_<?php echo($registro['id_covid'])?>" placeholder="¿Cuánto se hizo? (Un protocolo con 12 médicos en 3 turnos)"><?php echo($registro['cuanto_se_hizo'])?></textarea>

                               <textarea class="textbox_corto" maxlength="350" id="a_que_poblacion_EDIT_covid_<?php echo($registro['id_covid'])?>" name="a_que_poblacion_EDIT_covid_<?php echo($registro['id_covid'])?>" placeholder="¿A qué población va dirigida? (A todas las personas con sospecha de estar infectadas)"><?php echo($registro['que_poblacion'])?></textarea>

                               <p><label id="error_resultado_1_pid_EDIT_covid_<?php echo($registro['id_covid'])?>"  for="beneficia_accion_EDIT_covid_<?php echo($registro['id_covid'])?>">Impacto en la contingencia</label></p>

                               <textarea class="textbox_corto" maxlength="350" id="beneficia_accion_EDIT_covid_<?php echo($registro['id_covid'])?>" name="beneficia_accion_EDIT_covid_<?php echo($registro['id_covid'])?>" placeholder="¿En qué beneficia la medida? (Permite orientar a la población y, de identificarse, atender casos positivos)"><?php echo($registro['beneficia_accion'])?></textarea>
                      
                               <p><label id="error_resultado_2_pid_EDIT_covid_<?php echo($registro['id_covid'])?>"  for="beneficia_accion2_EDIT_covid_<?php echo($registro['id_covid'])?>">Impacto en el Estado</label></p>

                               <textarea class="textbox_corto" maxlength="350" id="beneficia_accion2_EDIT_covid_<?php echo($registro['id_covid'])?>" name="beneficia_accion2_EDIT_covid_<?php echo($registro['id_covid'])?>" placeholder="¿Como afecta esta medida a la economía, al desarrollo social, a los resultados esperados en la dependencia?"><?php echo($registro['beneficia2'])?></textarea>
                      
                           

                               <p><label id="error_resultado_1_pid_EDIT_covid_<?php echo($registro['id_covid'])?>"  for="donde_se_llevo_EDIT_covid_<?php echo($registro['id_covid'])?>">Lugar</label></p>

                               <textarea class="textbox_corto" maxlength="350" id="donde_se_llevo_EDIT_covid_<?php echo($registro['id_covid'])?>" name="donde_se_llevo_EDIT_covid_<?php echo($registro['id_covid'])?>" placeholder="¿Dónde se llevó a cabo la medida? (En el C5i a través de la atención del 911)"><?php echo($registro['donde_se_llevo'])?></textarea>

                               <label for="existe_incidencia_covid_EDIT_covid_<?php echo($registro['id_covid'])?>" >¿Existe alguna incidencia importante que se deba reportar?<strong class="tamanio_caracteres" id="tamanio_caracteres_EDIT_covid_<?php echo($registro['id_covid'])?>"></strong></label>

                               <div class="select">
                                <select class="inputDisabled"   name="existe_incidencia_covid_EDIT_covid_<?php echo($registro['id_covid'])?>" id="existe_incidencia_covid_EDIT_covid_<?php echo($registro['id_covid'])?>">
                                  <option value="No">No</option>
                                  <option value="Si">Si</option>
                                </select>


                                 <script type="text/javascript">
             
                                    $('#existe_incidencia_covid_EDIT_covid_<?php echo($registro['id_covid'])?>').change(function()
                                    { 
                                     // alert("se cambio un valor");

                                      if($(this).val() == "Si")
                                      {
                                      $('#existe_alguna_incidencia_covid_EDIT_covid_<?php echo($registro['id_covid'])?>').show();

                                      }
                                      if($(this).val() == "No"){
                                      $('#existe_alguna_incidencia_covid_EDIT_covid_<?php echo($registro['id_covid'])?>').hide();
                                      }
                                    });  


                                  </script>
                                         <br>
                                         <br>

                                        <textarea value= "Ninguna" hidden="true" id="existe_alguna_incidencia_covid_EDIT_covid_<?php echo($registro['id_covid'])?>" name="existe_alguna_incidencia_covid_EDIT_covid_<?php echo($registro['id_covid'])?>" placeholder="Esta medida presento alguna incidencia que sea importante reportar"><?php echo($registro['existe_alguna_incidencia'])?></textarea>

                                <p><label id="bullet"  for="bullet_EDIT_covid_<?php echo($registro['id_covid'])?>">Bullet</label></p>

                                     <textarea class="textbox_corto_EDIT_<?php echo($registro['id_covid'])?>" maxlength="350" id="bullet_EDIT_covid_<?php echo($registro['id_covid'])?>" name="bullet_EDIT_covid_<?php echo($registro['id_covid'])?>" placeholder="Redacta tu bullet"><?php echo($registro['bullet'])?></textarea>

                                            


                          </tr>

                         
                          <tr>
                       
                            <td style="text-align: right;">
                              <br><br><br>
                              <button type="button" onclick="edit_send_mjs_dep_covid(<?php echo $registro['id_covid']; ?>)" style="text-align: right;  background-color: white; color: black; border: 2px solid #4CAF50; /* Green */">Enviar</button>
                            </td>

                          </tr>

                        </tbody>
                      </table>
                      

                    </div>
                    
                  </div>
                </div>
              </div>
          </div>

<!--Nuevo material---------->

          <div id="Parte_logros_pid<?php echo $registro['id_covid']; ?>" class="tabcontent_vert">

            <div id="global">
                <div id="mensajes">
                  <div class="texto">
                    <div id="info_labels_<?php echo $registro['id_covid'];?>">
                     <form id="form_part_indicador" method="post">
                     
                        <table>
                        <col width=20%>
                        <col width=80%>
                          <tbody>
                            <?php global $wpdb;
                            $resultado;
                            $pids_datos = $wpdb->prefix . 'covid';
                            $registros_1 = $wpdb->get_results("SELECT * FROM $pids_datos where id_covid='$i'", ARRAY_A);
                            foreach($registros_1 as $registro_1) { 
                              ?>

                              <tr>
                                <p><label  id="error_descrp_tac_pid_EDIT_<?php echo($registro_1['id_covid'])?>" for="descrp_tac_pid_EDIT_<?php echo($registro_1['id_covid'])?>">Descripcion del indicador:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                                <textarea id="descrp_tac_pid_EDIT_<?php echo($registro_1['id_covid'])?>" maxlength="450" name="descrp_tac_pid_EDIT_<?php echo($registro_1['id_covid'])?>" placeholder="Descripcion:"><?php echo $registro_1['descripcion_indicador_pid']; ?></textarea>
                                </p>
                              </tr>

                              <tr>
                                 <p><label for="logros_en_indicador_pid_EDIT<?php echo($registro_1['id_covid'])?>" >¿Hubo logros en este indicador:?</label></p>
                                  <div class="select">
                                    <select name="logros_en_indicador_pid_EDIT<?php echo($registro_1['id_covid'])?>" id="logros_en_indicador_pid_EDIT<?php echo($registro_1['id_covid'])?>">
                                      <option value="No">No</option>
                                      <option value="Si">Si</option>
                                    </select>
                                  </div>
                                  <br>
                                <div id="bullet_Del_logro_pid_EDIT_<?php echo($registro_1['id_covid'])?>">
                                  <p><label id="error_id_bullet_Del_logro_pid_EDIT_<?php echo($registro_1['id_covid'])?>"  for="id_bullet_Del_logro_pid">Escribir el bullet del logro:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                                  <textarea id="id_bullet_Del_logro_pid_EDIT_<?php echo($registro_1['id_covid'])?>" maxlength="450"  name="id_bullet_Del_logro_pid_EDIT_<?php echo($registro_1['id_covid'])?>" placeholder="Escribir el bullet del logro:"   ><?php echo $registro_1['bullet_logro_pid']; ?></textarea>
                                </p>
                                </div>
                              </tr>
                              <tr>
                                  <p><label style="display: none;" for="premios_recono_pid_EDIT_<?php echo($registro_1['id_covid'])?>" >¿Hubo premios o reconocimientos?</label></p>
                                  <div class="select">
                                    <select style="display: none;" name="premios_recono_pid_EDIT_<?php echo($registro_1['id_covid'])?>" id="premios_recono_pid_EDIT_<?php echo($registro_1['id_covid'])?>">
                                      <option value="No">No</option>
                                      <option value="Si">Si</option>
                                    </select>
                                  </div>
                                  <br>
                                <div id="premios_reconocimientos_pid_EDIT_<?php echo($registro_1['id_covid'])?>">
                                  <p><label style="display: none;" id="error_reconocimiento_pid_EDIT_<?php echo($registro_1['id_covid'])?>"  for="reconocimiento_pid">Describelo:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                                  <textarea style="display: none;" maxlength="450"  id="reconocimiento_pid_EDIT_<?php echo($registro_1['id_covid'])?>" name="reconocimiento_pid_EDIT_<?php echo($registro_1['id_covid'])?>" placeholder="Escribir el bullet del premio o reconocimiento"   ><?php echo $registro_1['premio_reconocimiento_pid']; ?></textarea>
                                </p>
                                <br>
                                <br>
                              </tr>
                             
                              <?php } 

                            ?>
                          </tbody>
                        </table>

                    <script type="text/javascript">

                      $('#bullet_Del_logro_pid_EDIT_<?php echo($registro_1['id_covid'])?>').hide();
                      $('#premios_reconocimientos_pid_EDIT_<?php echo($registro_1['id_covid'])?>').hide();

                       $('#logros_en_indicador_pid_EDIT<?php echo($registro_1['id_covid'])?>').change(function()
                        { 
                         
                            if($(this).val() == "Si")
                            {
                              
                               $('#bullet_Del_logro_pid_EDIT_<?php echo($registro_1['id_covid'])?>').show();
                            }
                            else if($(this).val() == "No")
                            {
                             
                              $('#bullet_Del_logro_pid').hide();
                               $('#bullet_Del_logro_pid_EDIT_<?php echo($registro_1['id_covid'])?>').hide();

                            }


                        });

                        $('#premios_recono_pid_EDIT_<?php echo($registro_1['id_covid'])?>').change(function()
                        { 
                         
                            if($(this).val() == "Si")
                            {
                              
                               $('#premios_reconocimientos_pid_EDIT_<?php echo($registro_1['id_covid'])?>').show();
                            }
                            else if($(this).val() == "No")
                            {
                             
                              $('#bullet_Del_logro_pid').hide();
                               $('#premios_reconocimientos_pid_EDIT_<?php echo($registro_1['id_covid'])?>').hide();

                            }


                        });

                        if("<?php echo($registro_1['logros_indicador_pid'])?>"=="No")
                        {
                          document.getElementById("logros_en_indicador_pid_EDIT<?php echo($registro_1['id_covid'])?>").selectedIndex = 0;
                        }
                        else if("<?php echo($registro_1['logros_indicador_pid'])?>"=="Si")
                        {
                        
                          document.getElementById("logros_en_indicador_pid_EDIT<?php echo($registro_1['id_covid'])?>").selectedIndex = 1;
                          //mostrar el logro
                           $('#bullet_Del_logro_pid_EDIT_<?php echo($registro_1['id_covid'])?>').show();
                        }

                        if("<?php echo($registro_1['hubo_premio_pid'])?>"=="No")
                        {
                          document.getElementById("premios_recono_pid_EDIT_<?php echo($registro_1['id_covid'])?>").selectedIndex = 0;
                        }
                        else if("<?php echo($registro_1['hubo_premio_pid'])?>"=="Si")
                        {
                        
                          document.getElementById("premios_recono_pid_EDIT_<?php echo($registro_1['id_covid'])?>").selectedIndex = 1;
                          //mostrar el logro
                           $('#premios_reconocimientos_pid_EDIT_<?php echo($registro_1['id_covid'])?>').show();
                        }

                      function SubmitFormData_premios_pid(valor_entra) 
                      {

                      var descrp_tac_pid_EDIT_ = $("#descrp_tac_pid_EDIT_"+valor_entra).val();

                      var logros_en_indicador_pid_EDIT = $("#logros_en_indicador_pid_EDIT"+valor_entra).val();
                      var id_bullet_Del_logro_pid_EDIT = $("#id_bullet_Del_logro_pid_EDIT_"+valor_entra).val();

                      var premios_recono_pid_EDIT_ = $("#premios_recono_pid_EDIT_"+valor_entra).val();
                      var reconocimiento_pid_EDIT_ = $("#reconocimiento_pid_EDIT_"+valor_entra).val();

                      /*
                      console.log(descrp_tac_pid_EDIT_);
                      console.log(logros_en_indicador_pid_EDIT);
                      console.log(id_bullet_Del_logro_pid_EDIT);
                      console.log(premios_recono_pid_EDIT_);
                      console.log(reconocimiento_pid_EDIT_);
                      */

                      var error_descrp_tac_pid_EDIT_ = document.getElementById("error_descrp_tac_pid_EDIT_"+valor_entra);
                      var error_id_bullet_Del_logro_pid_EDIT_ = document.getElementById("error_id_bullet_Del_logro_pid_EDIT_"+valor_entra);

                      var error_id_bullet_Del_logro_pid_EDIT_ = document.getElementById("error_id_bullet_Del_logro_pid_EDIT_"+valor_entra);

                      var error_reconocimiento_pid_EDIT_= document.getElementById("error_reconocimiento_pid_EDIT_"+valor_entra);


                        if(descrp_tac_pid_EDIT_.length>450)
                        {
                         
                          error_descrp_tac_pid_EDIT_.innerHTML ="El limite es 450 carcateres";
                          error_descrp_tac_pid_EDIT_.classList.add("error_mensaje");

                        }
                        else if(id_bullet_Del_logro_pid_EDIT.length>450)
                        {
                         
                          error_id_bullet_Del_logro_pid_EDIT_.innerHTML ="El limite es 450 carcateres";
                          error_id_bullet_Del_logro_pid_EDIT_.classList.add("error_mensaje");
                        }
                        else if(reconocimiento_pid_EDIT_.length>450)
                        {
                         
                          error_reconocimiento_pid_EDIT_.innerHTML ="El limite es 450 carcateres";
                          error_reconocimiento_pid_EDIT_.classList.add("error_mensaje");

                        }
                     

                        else
                        {
                            var usuario_pid="<?php echo($user_name)?>";

                          
                            $.ajax({
                            type: "POST",
                            url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                            data: {id_indicador:valor_entra,descrp_tac_pid_EDIT_: descrp_tac_pid_EDIT_, logros_en_indicador_pid_EDIT:logros_en_indicador_pid_EDIT,id_bullet_Del_logro_pid_EDIT:id_bullet_Del_logro_pid_EDIT,premios_recono_pid_EDIT_:premios_recono_pid_EDIT_,reconocimiento_pid_EDIT_:reconocimiento_pid_EDIT_,nom_usuario_pid: usuario_pid   }
                            ,
                            success: function(msg){
                              alert( "Informacion Actualizada" );
                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown) {
                               alert( "Error Verifica tu informacion" );
                            }
                          });
                            /*
                            error_objetivo_est_sn_.classList.remove("error_mensaje");
                            error_objetivo_est_sn_.innerHTML ="Selecciona el objetivo estratégico correspondiente";

                            objetivo_gen_sn_edit_.classList.remove("error_mensaje");
                            objetivo_gen_sn_edit_.innerHTML ="Selecciona el objetivo general correspondiente";
                            */
                        }

                      }
                        
                    </script>
                        
                        <input style="background-color: #555555;color: white;"  type="button" id="submit_indicador" onclick="SubmitFormData_premios_pid(<?php echo $registro['id_covid'];?>)" value="Actualizar Informacion" /> </input> 
                                       
                      </form>

                   
                  </div>
                </div>
              </div>
              <br>
              <br>
            </div>
          </div>

          <div id="Parte_acciones_pids<?php echo $registro['id_covid']; ?>" class="tabcontent_vert">

              <div id="global">
                <div id="mensajes">
                  <div class="texto">
                    <div id="info_labels_<?php echo $registro['id_covid'];?>_2">
                      <form id="form_part_indicador" method="post">
                      <table>
                        <col width=20%>
                        <col width=80%>
                        <tbody>
                          <?php global $wpdb;
                            $pids_logros = $wpdb->prefix . 'covid';
                            $registros_2 = $wpdb->get_results("SELECT * FROM $pids_logros where id_covid='$i'", ARRAY_A);

                            foreach($registros_2 as $registro_2) { ?>
                               <br>

                               <tr>
                                  <p><label id="error_resultado_1_pid_EDIT_<?php echo($registro_1['id_covid'])?>"  for="resultado_1_pid_EDIT_<?php echo($registro_1['id_covid'])?>">acciones 1:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                  <textarea  class="textbox_corto" maxlength="350" id="resultado_1_pid_EDIT_<?php echo($registro_1['id_covid'])?>" name="resultado_1_pid_EDIT_<?php echo($registro_1['id_covid'])?>" placeholder="acciones 1:"><?php echo($registro_1['resultado1_pid'])?></textarea>
                                  <!--
                                  <button  id="btn_def_lugar_1"  onclick="open_lugares_pid(1,<?php echo($registro_1['id_covid'])?>)" type="button" >Definir lugar 1</button>

                                -->
                              </p>
                                  <label id="datos_cargados_lug_1_pid_EDIT_<?php echo($registro_1['id_covid'])?>" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                  <input hidden="true" value="<?php echo($registro_1['lug_result1_pid'])?>" id="res1_lugares_pid_EDIT_<?php echo($registro_1['id_covid'])?>" name="res1_lugares_pid_EDIT_<?php echo($registro_1['id_covid'])?>"  >                                   
                               </tr>

                               <tr>
                                 <p><label id="error_resultado_2_pid_EDIT_<?php echo($registro_1['id_covid'])?>" id="error_resultado_2_pid_EDIT_<?php echo($registro_1['id_covid'])?>"  for="resultado_2_pid_EDIT_<?php echo($registro_1['id_covid'])?>">acciones 2:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                 <textarea class="textbox_corto" maxlength="350" id="resultado_2_pid_EDIT_<?php echo($registro_1['id_covid'])?>" name="resultado_2_pid_EDIT_<?php echo($registro_1['id_covid'])?>" placeholder="acciones 2:"><?php echo($registro_1['resultado2_pid'])?></textarea>
                                 <!--
                                 <button  id="btn_def_lugar_2" class="btn_def_lugar" onclick="open_lugares_pid(2,<?php echo($registro_1['id_covid'])?>)" type="button" >Definir lugar 2</button>
                               -->

                                 </p><label id="datos_cargados_lug_2_pid_EDIT_<?php echo($registro_2['id_covid'])?>" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                 <input hidden="true" value="<?php echo($registro_1['lug_result2_pid'])?>" id="res2_lugares_pid_EDIT_<?php echo($registro_1['id_covid'])?>" name="res2_lugares_pid_EDIT_<?php echo($registro_1['id_covid'])?>"  >
                               </tr>
              
                               <tr>
                                <p><label id="error_resultado_3_pid_EDIT_<?php echo($registro_1['id_covid'])?>"  for="resultado_3_pid_EDIT_<?php echo($registro_1['id_covid'])?>">acciones 3:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                <textarea class="textbox_corto" maxlength="350" id="resultado_3_pid_EDIT_<?php echo($registro_1['id_covid'])?>" name="resultado_3_pid_EDIT_<?php echo($registro_1['id_covid'])?>" placeholder="acciones 3:"><?php echo($registro_1['resultado3_pid'])?></textarea>
                                <!--
                                <button class="btn_def_lugar" id="btn_def_lugar_3" onclick="open_lugares_pid(3,<?php echo($registro_1['id_covid'])?>)" type="button" >Definir lugar 3</button>
                              -->
  
                                </p>
                                <label id="datos_cargados_lug_3_pid_EDIT_<?php echo($registro_2['id_covid'])?>" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                <input hidden="true" value="<?php echo($registro_1['lug_result3_pid'])?>" id="res3_lugares_pid_EDIT_<?php echo($registro_1['id_covid'])?>" name="res3_lugares_pid_EDIT_<?php echo($registro_1['id_covid'])?>"  >
                                   
                               </tr>

                               <tr>
                                <p><label id="error_resultado_4_pid_EDIT_<?php echo($registro_1['id_covid'])?>" id="label_resultado_4_pid_EDIT_<?php echo($registro_1['id_covid'])?>"  for="resultado_4_pid_EDIT_<?php echo($registro_1['id_covid'])?>">acciones 4:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                <textarea class="textbox_corto" maxlength="350" id="resultado_4_pid_EDIT_<?php echo($registro_1['id_covid'])?>" name="resultado_4_pid_EDIT_<?php echo($registro_1['id_covid'])?>" placeholder="acciones 4:"><?php echo($registro_1['resultado4_pid'])?></textarea>
                                <!--
                                <button id="btn_def_lugar_4" class="btn_def_lugar" onclick="open_lugares_pid(4,<?php echo($registro_1['id_covid'])?>)"  type="button" >Definir lugar 4</button>
                              -->

                                </p>
                                <label id="datos_cargados_lug_4_pid_EDIT_<?php echo($registro_2['id_covid'])?>" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                <input hidden="true" value="<?php echo($registro_1['lug_result4_pid'])?>" id="res4_lugares_pid_EDIT_<?php echo($registro_1['id_covid'])?>" name="res4_lugares_pid_EDIT_<?php echo($registro_1['id_covid'])?>"   >
                               </tr>

                               <tr>
                                <p><label id="error_resultado_5_pid_EDIT_<?php echo($registro_1['id_covid'])?>" id="label_resultado_5_pid_EDIT_<?php echo($registro_1['id_covid'])?>"  for="resultado_5_pid_EDIT_<?php echo($registro_1['id_covid'])?>">acciones 5:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                <textarea class="textbox_corto" maxlength="350" id="resultado_5_pid_EDIT_<?php echo($registro_1['id_covid'])?>" name="resultado_5_pid_EDIT_<?php echo($registro_1['id_covid'])?>" placeholder="acciones 5:"><?php echo($registro_1['resultado5_pid'])?></textarea>
                                <!--
                                <button id="btn_def_lugar_5" class="btn_def_lugar" onclick="open_lugares_pid(5,<?php echo($registro_1['id_covid'])?>)"  type="button" >Definir lugar 5</button>
                              -->

                                </p>
                                <label id="datos_cargados_lug_5_pid_EDIT_<?php echo($registro_2['id_covid'])?>" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                <input hidden="true" value="<?php echo($registro_1['lug_result5_pid'])?>" id="res5_lugares_pid_EDIT_<?php echo($registro_1['id_covid'])?>" name="res5_lugares_pid_EDIT_<?php echo($registro_1['id_covid'])?>"   > 
                               </tr>



                              <br><br>
                              <!--
                              <p><label <>LO QUE SIGUE...</label></p>
                              
                              <p><label  id="error_lo_que_sigue_1_pid_EDIT_<?php echo($registro_1['id_covid'])?>" for="lo_que_sigue_1">1.- <strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                              <textarea id="lo_que_sigue_1_pid_EDIT_<?php echo($registro_1['id_covid'])?>" maxlength="450" name="lo_que_sigue_1_pid_EDIT_<?php echo($registro_1['id_covid'])?>" placeholder=""><?php echo($registro_1['lo_que_sigue_1_pid'])?></textarea>

                              <p><label  id="error_lo_que_sigue_2_pid_EDIT_<?php echo($registro_1['id_covid'])?>"  for="lo_que_sigue_2_pid_EDIT_<?php echo($registro_1['id_covid'])?>">2.- <strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                              <textarea id="lo_que_sigue_2_pid_EDIT_<?php echo($registro_1['id_covid'])?>" maxlength="450" name="lo_que_sigue_2_pid_EDIT_<?php echo($registro_1['id_covid'])?>" placeholder=""><?php echo($registro_1['lo_que_sigue_2_pid'])?></textarea>

                              <p><label  id="error_lo_que_sigue_3_pid_EDIT_<?php echo($registro_1['id_covid'])?>"  for="lo_que_sigue_3_pid_EDIT_<?php echo($registro_1['id_covid'])?>">3.- <strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong> </label>
                              <textarea id="lo_que_sigue_3_pid_EDIT_<?php echo($registro_1['id_covid'])?>" maxlength="450" name="lo_que_sigue_3_pid_EDIT_<?php echo($registro_1['id_covid'])?>" placeholder=""><?php echo($registro_1['lo_que_sigue_3_pid'])?></textarea>
                             -->

                              <?php } 

                            ?>
                              </tbody>
                            </table>

                      <script type="text/javascript">
                        //ocultar el mensaje datos cargados
                        $('#datos_cargados_lug_1_pid_EDIT_<?php echo($registro_2['id_covid'])?>').hide();
                        $('#datos_cargados_lug_2_pid_EDIT_<?php echo($registro_2['id_covid'])?>').hide();
                        $('#datos_cargados_lug_3_pid_EDIT_<?php echo($registro_2['id_covid'])?>').hide();
                        $('#datos_cargados_lug_4_pid_EDIT_<?php echo($registro_2['id_covid'])?>').hide();
                        $('#datos_cargados_lug_5_pid_EDIT_<?php echo($registro_2['id_covid'])?>').hide();


                      function SubmitForm_obras_acciones_pids(valor_entra) 
                      {
                        var res1_lugares_pid_EDIT_ = $("#res1_lugares_pid_EDIT_"+valor_entra).val();
                        var res2_lugares_pid_EDIT_ = $("#res2_lugares_pid_EDIT_"+valor_entra).val();
                        var res3_lugares_pid_EDIT_ = $("#res3_lugares_pid_EDIT_"+valor_entra).val();
                        var res4_lugares_pid_EDIT_ = $("#res4_lugares_pid_EDIT_"+valor_entra).val();
                        var res5_lugares_pid_EDIT_ = $("#res5_lugares_pid_EDIT_"+valor_entra).val();


                        var resultado_1_pid_EDIT_ = $("#resultado_1_pid_EDIT_"+valor_entra).val();
                        var resultado_2_pid_EDIT_ = $("#resultado_2_pid_EDIT_"+valor_entra).val();
                        var resultado_3_pid_EDIT_ = $("#resultado_3_pid_EDIT_"+valor_entra).val();
                        var resultado_4_pid_EDIT_ = $("#resultado_4_pid_EDIT_"+valor_entra).val();
                        var resultado_5_pid_EDIT_ = $("#resultado_5_pid_EDIT_"+valor_entra).val();

                        var lo_que_sigue_1_pid_EDIT_ = $("#lo_que_sigue_1_pid_EDIT_"+valor_entra).val();
                        var lo_que_sigue_2_pid_EDIT_ = $("#lo_que_sigue_2_pid_EDIT_"+valor_entra).val();
                        var lo_que_sigue_3_pid_EDIT_ = $("#lo_que_sigue_3_pid_EDIT_"+valor_entra).val();


                        var accion_err_1 = document.getElementById("error_resultado_1_pid_EDIT_"+valor_entra);
                        var accion_err_2 = document.getElementById("error_resultado_2_pid_EDIT_"+valor_entra);
                        var accion_err_3 = document.getElementById("error_resultado_3_pid_EDIT_"+valor_entra);
                        var accion_err_4 = document.getElementById("error_resultado_4_pid_EDIT_"+valor_entra);
                        var accion_err_5 = document.getElementById("error_resultado_5_pid_EDIT_"+valor_entra);



                        if(resultado_1_pid_EDIT_.length>350)
                        {
                                                             
                            accion_err_1.innerHTML ="El limite son 350 caracteres";
                            accion_err_1.classList.add("error_mensaje");

                        }
                        else if(resultado_2_pid_EDIT_.length>350)
                        {
                                                             
                            accion_err_2.innerHTML ="El limite son 350 caracteres";
                            accion_err_2.classList.add("error_mensaje");

                        }
                        else if(resultado_3_pid_EDIT_.length>350)
                        {
                                                             
                            accion_err_3.innerHTML ="El limite son 350 caracteres";
                            accion_err_3.classList.add("error_mensaje");

                        }
                        else if(resultado_4_pid_EDIT_.length>350)
                        {
                                                             
                            accion_err_4.innerHTML ="El limite son 350 caracteres";
                            accion_err_4.classList.add("error_mensaje");

                        }
                        else if(resultado_5_pid_EDIT_.length>350)
                        {
                                                             
                            accion_err_5.innerHTML ="El limite son 350 caracteres";
                            accion_err_5.classList.add("error_mensaje");

                        }
                        else
                        {

                        var resultado_1_pid_EDIT_ = $("#resultado_1_pid_EDIT_"+valor_entra).val();
                        var resultado_2_pid_EDIT_ = $("#resultado_2_pid_EDIT_"+valor_entra).val();
                        var resultado_3_pid_EDIT_ = $("#resultado_3_pid_EDIT_"+valor_entra).val();
                        var resultado_4_pid_EDIT_ = $("#resultado_4_pid_EDIT_"+valor_entra).val();
                        var resultado_5_pid_EDIT_ = $("#resultado_5_pid_EDIT_"+valor_entra).val();

                        var lo_que_sigue_1_pid_EDIT_ = $("#lo_que_sigue_1_pid_EDIT_"+valor_entra).val();
                        var lo_que_sigue_2_pid_EDIT_ = $("#lo_que_sigue_2_pid_EDIT_"+valor_entra).val();
                        var lo_que_sigue_3_pid_EDIT_ = $("#lo_que_sigue_3_pid_EDIT_"+valor_entra).val();

                        var usuario_pid="<?php echo($user_name)?>";



                         // alert(valor_entra);
                                $.ajax({
                                  type: "POST",
                                  url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                                  data: {id_indicador:valor_entra,resultado_1_pid_EDIT_:resultado_1_pid_EDIT_,resultado_2_pid_EDIT_:resultado_2_pid_EDIT_,resultado_3_pid_EDIT_:resultado_3_pid_EDIT_,resultado_4_pid_EDIT_:resultado_4_pid_EDIT_,resultado_5_pid_EDIT_:resultado_5_pid_EDIT_,lo_que_sigue_1_pid_EDIT_:lo_que_sigue_1_pid_EDIT_,lo_que_sigue_2_pid_EDIT_:lo_que_sigue_2_pid_EDIT_,lo_que_sigue_3_pid_EDIT_:lo_que_sigue_3_pid_EDIT_,nom_usuario_pid: usuario_pid,res1_lugares_pid_EDIT_:res1_lugares_pid_EDIT_,res2_lugares_pid_EDIT_:res2_lugares_pid_EDIT_,res3_lugares_pid_EDIT_:res3_lugares_pid_EDIT_,res4_lugares_pid_EDIT_:res4_lugares_pid_EDIT_,res5_lugares_pid_EDIT_:res5_lugares_pid_EDIT_}
                                  ,
                                  success: function(msg){
                            
                                    if(msg=1)
                                    {
                                      alert("Informacion Actualizada");
                                      
                                    }
                                    
                                  },
                                  error: function(XMLHttpRequest, textStatus, errorThrown) {
                                    alert( "Error Verifica tu informacion" );
                                  }
                                });
                                
                                accion_err.classList.remove("error_mensaje");
                                accion_err.innerHTML ="Descripcion del indicador";
                          
                        }
                      }
                      </script>

                                          
                      <input style="background-color: #555555;color: white;" type="button" onclick="SubmitForm_obras_acciones_pids(<?php echo $registro['id_covid'];?>)"  value="Actualizar Informacion" />  
                                    
                      </form>



                    <div><!--comentarios de correccion del evaluador-->
                      <!--
                          <label>"<?php echo($registro_1['coment_evidencia']) ?>"</label>
                        -->
                    </div>
                  </div>
                 </div>
                </div>
              </div>
          </div>

          <div id="Parte_indicador_pids<?php echo $registro['id_covid']; ?>" class="tabcontent_vert">

              <div id="global">
                <div id="mensajes">
                  <div class="texto">
                    <div id="info_labels_<?php echo $registro['id_covid'];?>_2">
                      <form id="form_part_indicador" method="post">
                      <table>
                        <col width=20%>
                        <col width=80%>
                        <tbody>
                          <?php global $wpdb;
                            $pids_logros = $wpdb->prefix . 'covid';
                            $registros_2 = $wpdb->get_results("SELECT * FROM $pids_logros where id_covid='$i'", ARRAY_A);

                            foreach($registros_2 as $registro_2) { ?>
                               <br>
                               <tr>
                                <p><label id="error_unidad_observacion_ind_pid_EDIT_<?php echo($registro_1['id_covid'])?>" for="unidad_observacion_ind_pid">¿Cuál es la unidad de observación del indicador? (Ej. acciones, población, etc.):<strong class="tamanio_caracteres" id="tamanio_caracteres">Valor registrado en ficha tecnica(no editable)</strong></label> 
                                <input value="<?php echo $registro_2['unidad_observacion_ind_pid'];?>" disabled="true" id="unidad_observacion_ind_pid_EDIT_<?php echo($registro_1['id_covid'])?>" name="unidad_observacion_ind_pid_EDIT_<?php echo($registro_1['id_covid'])?>" placeholder="¿Cuál es la unidad de observación del indicador? (Ej. acciones, población, etc.)"   ></p>
                               </tr>
                               <tr>
                                <br>

                                <p><label style="display: none;" id="error_descripcion_observacion_ficha_tecnica_pid_EDIT_<?php echo($registro_1['id_covid'])?>" for="descripcion_observacion_ficha_tecnica_pid_EDIT_<?php echo($registro_1['id_covid'])?>">Da una breve descripción  de la unidad de la observación (Consultar la ficha técnica)<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label><textarea style="display: none;" id="descripcion_observacion_ficha_tecnica_pid_EDIT_<?php echo($registro_1['id_covid'])?>" maxlength="450" name="descripcion_observacion_ficha_tecnica_pid_EDIT_<?php echo($registro_1['id_covid'])?>" placeholder=" Da una breve descripción de la unidad de la observación Consultar la ficha técnica" ><?php echo $registro_2['descripcion_observacion_ficha_tecnica_def_pid'];?></textarea></p>                 
                               </tr>
                               <tr>
                                 <label for="tipo_indicador_def_pid" >¿Qué tipo de indicador es? <strong class="tamanio_caracteres" id="tamanio_caracteres">Valor registrado en ficha tecnica(no editable)</strong></label>
                                 <div class="select">
                                  <select disabled="true"  name="tipo_indicador_def_pid_EDIT_<?php echo($registro_1['id_covid'])?>" id="tipo_indicador_def_pid_EDIT_<?php echo($registro_1['id_covid'])?>">
                                    <option value="Número">Número</option>
                                    <option value="Porcentaje">Porcentaje </option>
                                    <option value="Tasa">Tasa</option>
                                    <option value="Absolutos">Absolutos</option>
                                    <option value="Hectareas">Hectareas</option>
                                    <option value="Promedio">Promedio</option>
                                    <option value="Cobertura">Cobertura</option>
                                    <option value="Otro">Otro</option>
                                   </select>
                                 </div>
                               </tr>

                               <tr>
                                <br>
                                <div id="otro_tipo_indicador_pid_EDIT_<?php echo($registro_1['id_covid'])?>">
                                  <p><label for="otro_tipo_ind_pid_EDIT_<?php echo($registro_1['id_covid'])?>">¿Cual?:</label> <input value="<?php echo($registro_1['otro_tipo_ind_pid'])?>"  name="otro_tipo_ind_pid_EDIT_<?php echo($registro_1['id_covid'])?>" id="otro_tipo_ind_pid_EDIT_<?php echo($registro_1['id_covid'])?>" placeholder=""   ></p>
                                </div>
                               </tr>

                               <tr>
                                <label for="tendencia_esp_pid_EDIT_<?php echo($registro_1['id_covid'])?>">¿Qué tendencia se espera? <strong class="tamanio_caracteres" id="tamanio_caracteres">Valor registrado en ficha tecnica(no editable)</strong></label>
                                <div class="select">
                                  <select disabled="true"   name="tendencia_esp_pid_EDIT_<?php echo($registro_1['id_covid'])?>" id="tendencia_esp_pid_EDIT_<?php echo($registro_1['id_covid'])?>">
                                    <option id="tend_acs" value="Ascendente">Ascendente</option>
                                    <option id="tend_des" value="Descendente">Descendente</option>
                                    <option id="tend_cons" value="Constante">Constante</option>
                                    <option id="tend_var" value="Variable">Variable</option>
                                  </select>
                                </div>
                               </tr>

                               <tr>
                                 <br>
                                <p><label style="display: none;"  id="error_linea_base_pid_EDIT_<?php echo($registro_1['id_covid'])?>" for="linea_base_absolutos_pid_EDIT_<?php echo($registro_1['id_covid'])?>">Indicar la línea base en absolutos (números absolutos)</label>
                                <input style="display: none;"  value="<?php echo($registro_1['linea_base_absolutos_pid'])?>" id="linea_base_absolutos_pid_EDIT_<?php echo($registro_1['id_covid'])?>" minlength="0" maxlength="15" name="linea_base_absolutos_pid_EDIT_<?php echo($registro_1['id_covid'])?>" placeholder="Indicar la línea base en absolutos (números absolutos)"></p>

                                
                               </tr>

                               <tr>
                                <br>
                                 <p><label id="error_obj_especifico_prog_inst_EDIT_<?php echo($registro_1['id_covid'])?>"  for="obj_especifico_prog_inst_EDIT_<?php echo($registro_1['id_covid'])?>">Objetivo Especifico Programa Institucional:<strong class="tamanio_caracteres" id="tamanio_caracteres">Valor registrado en ficha tecnica(no editable)</strong></label>
                                 <textarea disabled="true" id="obj_especifico_prog_inst_EDIT_<?php echo($registro_1['id_covid'])?>" name="obj_especifico_prog_inst_EDIT_<?php echo($registro_1['id_covid'])?>" placeholder="Objetivo Especifico Programa Instituciona:"   ><?php echo($registro_1['obj_esp_prog_inst_pid'])?></textarea>
                                 <br>
                               </tr>

                              


                              <?php } 

                            ?>
                              </tbody>
                            </table>

                      <script type="text/javascript">

                    
                        $('#otro_tipo_indicador_pid_EDIT_<?php echo($registro_1['id_covid'])?>').hide();

                        if("<?php echo($registro_1['tipo_ind_pid'])?>"=="Número")
                        {
                          document.getElementById("tipo_indicador_def_pid_EDIT_<?php echo($registro_1['id_covid'])?>").selectedIndex = 0;
                        }
                        else if("<?php echo($registro_1['tipo_ind_pid'])?>"=="Porcentaje")
                        {
                        
                          document.getElementById("tipo_indicador_def_pid_EDIT_<?php echo($registro_1['id_covid'])?>").selectedIndex = 1;
                          //mostrar el logro
                          
                        }
                        else if("<?php echo($registro_1['tipo_ind_pid'])?>"=="Tasa")
                        {
                        
                          document.getElementById("tipo_indicador_def_pid_EDIT_<?php echo($registro_1['id_covid'])?>").selectedIndex = 2;
                          //mostrar el logro
                          
                        }
                        else if("<?php echo($registro_1['tipo_ind_pid'])?>"=="Absolutos")
                        {
                        
                          document.getElementById("tipo_indicador_def_pid_EDIT_<?php echo($registro_1['id_covid'])?>").selectedIndex = 3;
                          //mostrar el logro
                           
                        }

                        else if("<?php echo($registro_1['tipo_ind_pid'])?>"=="Hectareas")
                        {
                          
                          document.getElementById("tipo_indicador_def_pid_EDIT_<?php echo($registro_1['id_covid'])?>").selectedIndex = 4;
                          //mostrar el logro
                           
                        }
                        else if("<?php echo($registro_1['tipo_ind_pid'])?>"=="Promedio")
                        {
                        
                          document.getElementById("tipo_indicador_def_pid_EDIT_<?php echo($registro_1['id_covid'])?>").selectedIndex = 5;
                          //mostrar el logro
                           
                        }
                        else if("<?php echo($registro_1['tipo_ind_pid'])?>"=="Cobertura")
                        {
                        
                          document.getElementById("tipo_indicador_def_pid_EDIT_<?php echo($registro_1['id_covid'])?>").selectedIndex = 6;
                          //mostrar el logro
                           
                        }
                        else if("<?php echo($registro_1['tipo_ind_pid'])?>"=="Otro")
                        {
                        
                          document.getElementById("tipo_indicador_def_pid_EDIT_<?php echo($registro_1['id_covid'])?>").selectedIndex = 7;
                          //mostrar el logro

                           $('#otro_tipo_indicador_pid_EDIT_<?php echo($registro_1['id_covid'])?>').show();  
                        }





                        if("<?php echo($registro_1['tendencia_pid'])?>"=="Ascendente")
                        {
                          document.getElementById("tendencia_esp_pid_EDIT_<?php echo($registro_1['id_covid'])?>").selectedIndex = 0;
                        }

                        else if("<?php echo($registro_1['tendencia_pid'])?>"=="Descendente")
                        {
                        
                          document.getElementById("tendencia_esp_pid_EDIT_<?php echo($registro_1['id_covid'])?>").selectedIndex = 1;
                          //mostrar el logro
                          
                        }
                        else if("<?php echo($registro_1['tendencia_pid'])?>"=="Constante")
                        {
                        
                          document.getElementById("tendencia_esp_pid_EDIT_<?php echo($registro_1['id_covid'])?>").selectedIndex = 2;
                          //mostrar el logro
                          
                        }
                        else if("<?php echo($registro_1['tendencia_pid'])?>"=="Variable")
                        {
                        
                          document.getElementById("tendencia_esp_pid_EDIT_<?php echo($registro_1['id_covid'])?>").selectedIndex = 3;
                          //mostrar el logro
                        }

                          



                        //ocultar el mensaje datos cargados
                      function SubmitForm_indicador_EDIT_PID(valor_entra) 
                      {
                        var unidad_observacion_ind_pid_EDIT_ = $("#unidad_observacion_ind_pid_EDIT_"+valor_entra).val();
                        var descripcion_observacion_ficha_tecnica_pid_EDIT_ = $("#descripcion_observacion_ficha_tecnica_pid_EDIT_"+valor_entra).val();

                        var tipo_indicador_def_pid_EDIT_ = $("#tipo_indicador_def_pid_EDIT_"+valor_entra).val();

                        var otro_tipo_ind_pid_EDIT_ = $("#otro_tipo_ind_pid_EDIT_"+valor_entra).val();

                        var tendencia_esp_pid_EDIT_ = $("#tendencia_esp_pid_EDIT_"+valor_entra).val();

                        var linea_base_absolutos_pid_EDIT_ = $("#linea_base_absolutos_pid_EDIT_"+valor_entra).val();

                        var obj_especifico_prog_inst_EDIT_ = $("#obj_especifico_prog_inst_EDIT_"+valor_entra).val();




                         var error_unidad_observacion_ind_pid_EDIT_ = document.getElementById("error_unidad_observacion_ind_pid_EDIT_"+valor_entra);


                         var error_descripcion_observacion_ficha_tecnica_pid_EDIT_ = document.getElementById("error_descripcion_observacion_ficha_tecnica_pid_EDIT_"+valor_entra);

                         var error_linea_base_pid_EDIT_ = document.getElementById("error_linea_base_pid_EDIT_"+valor_entra);

                         var error_obj_especifico_prog_inst_EDIT_ = document.getElementById("error_obj_especifico_prog_inst_EDIT_"+valor_entra);

                         

                         


                        if(unidad_observacion_ind_pid_EDIT_.length>60)
                        {
                                                             
                            error_unidad_observacion_ind_pid_EDIT_.innerHTML ="El limite son 60 caracteres";
                            error_unidad_observacion_ind_pid_EDIT_.classList.add("error_mensaje");

                        }
                        else if(descripcion_observacion_ficha_tecnica_pid_EDIT_.length>350)
                        {
                           error_descripcion_observacion_ficha_tecnica_pid_EDIT_.innerHTML ="El limite son 350 caracteres";
                            error_descripcion_observacion_ficha_tecnica_pid_EDIT_.classList.add("error_mensaje");

                        }
                        else if(linea_base_absolutos_pid_EDIT_.length>30)
                        {
                            error_linea_base_pid_EDIT_.innerHTML ="El limite son 30 caracteres";
                            error_linea_base_pid_EDIT_.classList.add("error_mensaje");

                        }
                        else if(obj_especifico_prog_inst_EDIT_.length>450)
                        {
                            error_obj_especifico_prog_inst_EDIT_.innerHTML ="El limite son 450 caracteres";
                            error_obj_especifico_prog_inst_EDIT_.classList.add("error_mensaje");

                        }
                    
                        else
                        {

                           var usuario_pid="<?php echo($user_name)?>";

                          $.ajax({
                            type: "POST",
                            url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                            data: {id_indicador:valor_entra,nom_usuario_pid: usuario_pid,unidad_observacion_ind_pid_EDIT_:unidad_observacion_ind_pid_EDIT_,descripcion_observacion_ficha_tecnica_pid_EDIT_:descripcion_observacion_ficha_tecnica_pid_EDIT_,tipo_indicador_def_pid_EDIT_: tipo_indicador_def_pid_EDIT_,otro_tipo_ind_pid_EDIT_:otro_tipo_ind_pid_EDIT_, tendencia_esp_pid_EDIT_:tendencia_esp_pid_EDIT_,linea_base_absolutos_pid_EDIT_:linea_base_absolutos_pid_EDIT_,obj_especifico_prog_inst_EDIT_:obj_especifico_prog_inst_EDIT_},
                            
                            success: function(msg){
                      
                              if(msg=1)
                              {
                                alert("Informacion Actualizada");
                                
                              }
                              
                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown) {
                              alert( "Error Verifica tu informacion" );
                            }
                          });
                       
                        }
                      }
                      </script>

                                          
                      <input style="background-color: #555555;color: white;" type="button" onclick="SubmitForm_indicador_EDIT_PID(<?php echo $registro['id_covid'];?>)"  value="Actualizar Informacion" />  
                                    
                      </form>



                    <div><!--comentarios de correccion del evaluador-->
                      <!--
                          <label>"<?php echo($registro_1['coment_evidencia']) ?>"</label>
                        -->
                    </div>
                  </div>
                 </div>
                </div>
              </div>
          </div>

          <div id="Parte_metas_pids<?php echo $registro['id_covid']; ?>" class="tabcontent_vert">

              <div id="global">
                <div id="mensajes">
                  <div class="texto">
                    <div id="info_labels_<?php echo $registro['id_covid'];?>_2">
                      <form id="form_part_indicador" method="post">
                      <table>
                        <col width=20%>
                        <col width=80%>
                        <tbody>
                          <?php global $wpdb;
                            $pids_logros = $wpdb->prefix . 'covid';
                            $registros_2 = $wpdb->get_results("SELECT * FROM $pids_logros where id_covid='$i'", ARRAY_A);

                            foreach($registros_2 as $registro_2) { ?>
                               <br>

           
                               <tr>
                                <p><label id="error_fuente_pid_EDIT_<?php echo $registro['id_covid'];?>"  for="fuente_pid_EDIT_<?php echo $registro['id_covid'];?>">Fuente:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                                <textarea id="fuente_pid_EDIT_<?php echo $registro['id_covid'];?>" name="fuente_pid_EDIT_<?php echo $registro['id_covid'];?>" placeholder="Fuente:"><?php echo $registro['fuente_pid'];?></textarea>
                               </tr>

                               <tr>
                                <p><label id="error_ref_adicionales_EDIT_<?php echo $registro['id_covid'];?>"  for="ref_adicionales_EDIT_<?php echo $registro['id_covid'];?>">Referencias Adicionales:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                                <textarea  id="ref_adicionales_EDIT_<?php echo $registro['id_covid'];?>" name="ref_adicionales_EDIT_<?php echo $registro['id_covid'];?>" placeholder="Referencias Adicionales:"   ><?php echo $registro['referencias_Adi_pid'];?></textarea>
                                <br>
                                                
                               </tr>
                                <p><label  for="linea_base_gen_EDIT_<?php echo $registro['id_covid'];?>"  id="error_linea_base_gen_EDIT_<?php echo $registro['id_covid'];?>">Linea Base 2017</label>
                                <input disabled="true"  minlength="0" maxlength="15" name="linea_base_gen_EDIT_<?php echo $registro['id_covid'];?>" placeholder="" id="linea_base_gen_EDIT_<?php echo $registro['id_covid'];?>" value="<?php echo $registro['linea_base_gen'];?>"></p> 
                                <br>
                               <tr>
                                <br>
                                <p><label for="meta_2018_pid_EDIT_<?php echo $registro['id_covid'];?>"  id="error_meta_2018_pid_EDIT_<?php echo $registro['id_covid'];?>">Meta 2018 <strong class="tamanio_caracteres" id="tamanio_caracteres">Valor registrado en ficha tecnica(no editable)</strong></label>
                                <input disabled="true"  minlength="0" maxlength="15" name="meta_2018_pid_EDIT_<?php echo $registro['id_covid'];?>" placeholder="" id="meta_2018_pid_EDIT_<?php echo $registro['id_covid'];?>" value="<?php echo $registro['meta_2018_pid'];?>" ></p>
                                
                               </tr>
                                <br>
                                <p><label  for="meta_2019_pid_EDIT_<?php echo $registro['id_covid'];?>"  id="error_meta_2019_pid_EDIT_<?php echo $registro['id_covid'];?>">Meta 2019 <strong class="tamanio_caracteres" id="tamanio_caracteres">Valor registrado en ficha tecnica(no editable)</strong></label>
                                <input value="<?php echo $registro['meta_2019_pid'];?>" disabled="true"  minlength="0" maxlength="15" name="meta_2019_pid_EDIT_<?php echo $registro['id_covid'];?>" placeholder="" id="meta_2019_pid_EDIT_<?php echo $registro['id_covid'];?>"></p> 

                               <tr>
                                <br>
                                <p><label for="meta_2020_pid_EDIT_<?php echo $registro['id_covid'];?>"  id="error_meta_2020_pid_EDIT_<?php echo $registro['id_covid'];?>">Meta 2020 <strong class="tamanio_caracteres" id="tamanio_caracteres">Valor registrado en ficha tecnica(no editable)</strong></label>
                                <input value="<?php echo $registro['meta_2020_pid'];?>" disabled="true"  minlength="0" maxlength="15" name="meta_2020_pid_EDIT_<?php echo $registro['id_covid'];?>" placeholder="" id="meta_2020_pid_EDIT_<?php echo $registro['id_covid'];?>"></p> 
                               
                               </tr>

                               <tr>
                                <br>
                                <p><label for="meta_2021_pid_EDIT_<?php echo $registro['id_covid'];?>"  id="error_meta_2021_pid_EDIT_<?php echo $registro['id_covid'];?>">Meta 2021 <strong class="tamanio_caracteres" id="tamanio_caracteres">Valor registrado en ficha tecnica(no editable)</strong></label>
                                <input value="<?php echo $registro['meta_2021_pid'];?>" disabled="true"  minlength="0" maxlength="15" name="meta_2021_pid_EDIT_<?php echo $registro['id_covid'];?>" placeholder="" id="meta_2021_pid_EDIT_<?php echo $registro['id_covid'];?>"></p> 
                                
                               </tr>

                               <tr>
                                <br>
                                <p><label for="meta_2022_pid_EDIT_<?php echo $registro['id_covid'];?>"  id="error_meta_2022_pid_EDIT_<?php echo $registro['id_covid'];?>">Meta 2022 <strong class="tamanio_caracteres" id="tamanio_caracteres">Valor registrado en ficha tecnica(no editable)</strong></label>
                                <input value="<?php echo $registro['meta_2022_pid'];?>"  disabled="true"  minlength="0" maxlength="15" name="meta_2022_pid_EDIT_<?php echo $registro['id_covid'];?>" placeholder="" id="meta_2022_pid_EDIT_<?php echo $registro['id_covid'];?>"></p>
                                
                               </tr>

                               <tr>
                                <br>
                                <p><label for="meta_2030_pid_EDIT_<?php echo $registro['id_covid'];?>"  id="error_meta_2030_pid_EDIT_<?php echo $registro['id_covid'];?>">Meta 2030 <strong class="tamanio_caracteres" id="tamanio_caracteres">Valor registrado en ficha tecnica(no editable)</strong></label>
                                <input value="<?php echo $registro['meta_2030_pid'];?>" disabled="true"  minlength="0" maxlength="15" name="meta_2030_pid_EDIT_<?php echo $registro['id_covid'];?>" placeholder="" id="meta_2030_pid_EDIT_<?php echo $registro['id_covid'];?>"></p> 
                               
                               </tr>

                               <tr>
                                <p><label for="resultado_pid_2018_EDIT_<?php echo $registro['id_covid'];?>"  id="error_resultado_pid_2018_EDIT_<?php echo $registro['id_covid'];?>">Resultado PID 2018</label>
                                <input  value="<?php echo $registro['resultado_pid_2018'];?>" minlength="0" maxlength="15" name="resultado_pid_2018_EDIT_<?php echo $registro['id_covid'];?>" placeholder="" id="resultado_pid_2018_EDIT_<?php echo $registro['id_covid'];?>"></p> 
                                <br>
                               
                               </tr>

                               
              

               

                              <?php } 

                            ?>
                              </tbody>
                            </table>

                      <script type="text/javascript">
                        //ocultar el mensaje datos cargados
                      function SubmitForm_metas_EDIT_pids(valor_entra) 
                      {
                        var fuente_pid_EDIT_ = $("#fuente_pid_EDIT_"+valor_entra).val();
                        var ref_adicionales_EDIT_ = $("#ref_adicionales_EDIT_"+valor_entra).val();
                        var linea_base_gen_EDIT_ = $("#linea_base_gen_EDIT_"+valor_entra).val();
                        var meta_2018_pid_EDIT_ = $("#meta_2018_pid_EDIT_"+valor_entra).val();
                        var meta_2019_pid_EDIT_ = $("#meta_2019_pid_EDIT_"+valor_entra).val();
                        var meta_2020_pid_EDIT_ = $("#meta_2020_pid_EDIT_"+valor_entra).val();
                        var meta_2021_pid_EDIT_ = $("#meta_2021_pid_EDIT_"+valor_entra).val();
                        var meta_2022_pid_EDIT_ = $("#meta_2022_pid_EDIT_"+valor_entra).val();
                        var meta_2030_pid_EDIT_ = $("#meta_2030_pid_EDIT_"+valor_entra).val();
                        var resultado_pid_2018_EDIT_ = $("#resultado_pid_2018_EDIT_"+valor_entra).val();



                       

                        var error_fuente_pid_EDIT_ = document.getElementById("error_fuente_pid_EDIT_"+valor_entra);
                        var error_fuente_pid_EDIT_ = document.getElementById("error_fuente_pid_EDIT_"+valor_entra);
                        var error_linea_base_gen_EDIT_ = document.getElementById("error_fuente_pid_EDIT_"+valor_entra);
                        var error_meta_2018_pid_EDIT_ = document.getElementById("error_fuente_pid_EDIT_"+valor_entra);
                        var error_meta_2019_pid_EDIT_ = document.getElementById("error_fuente_pid_EDIT_"+valor_entra);
                        var error_meta_2020_pid_EDIT_ = document.getElementById("error_fuente_pid_EDIT_"+valor_entra);
                        var error_meta_2021_pid_EDIT_ = document.getElementById("error_fuente_pid_EDIT_"+valor_entra);
                        var error_meta_2022_pid_EDIT_ = document.getElementById("error_fuente_pid_EDIT_"+valor_entra);
                        var error_meta_2030_pid_EDIT_ = document.getElementById("error_fuente_pid_EDIT_"+valor_entra);
                        
                        var error_resultado_pid_2018_EDIT_ = document.getElementById("error_fuente_pid_EDIT_"+valor_entra);




                        if(fuente_pid_EDIT_.length>300)
                        {
                                                             
                            error_fuente_pid_EDIT_.innerHTML ="El limite son 250 caracteres";
                            error_fuente_pid_EDIT_.classList.add("error_mensaje");

                        }
                        else if(ref_adicionales_EDIT_.length>450)
                        {
                            error_ref_adicionales_EDIT_.innerHTML ="El limite son 450 caracteres";
                            error_ref_adicionales_EDIT_.classList.add("error_mensaje");

                        }
                        else if(linea_base_gen_EDIT_.length>250)
                        {
                            error_linea_base_gen_EDIT_.innerHTML ="El limite son 250 caracteres";
                            error_linea_base_gen_EDIT_.classList.add("error_mensaje");

                        }
                        else if(meta_2018_pid_EDIT_.length>10)
                        {
                            error_meta_2018_pid_EDIT_.innerHTML ="El limite son 10 caracteres";
                            error_meta_2018_pid_EDIT_.classList.add("error_mensaje");

                        }
                        else if(meta_2019_pid_EDIT_.length>10)
                        {
                            error_meta_2019_pid_EDIT_.innerHTML ="El limite son 10 caracteres";
                            error_meta_2019_pid_EDIT_.classList.add("error_mensaje");

                        }
                         else if(meta_2020_pid_EDIT_.length>10)
                        {
                            error_meta_2020_pid_EDIT_.innerHTML ="El limite son 10 caracteres";
                            error_meta_2020_pid_EDIT_.classList.add("error_mensaje");

                        }
                        else if(meta_2021_pid_EDIT_.length>10)
                        {
                            error_meta_2021_pid_EDIT_.innerHTML ="El limite son 10 caracteres";
                            error_meta_2021_pid_EDIT_.classList.add("error_mensaje");

                        }
                        else if(meta_2022_pid_EDIT_.length>10)
                        {
                            error_meta_2022_pid_EDIT_.innerHTML ="El limite son 10 caracteres";
                            error_meta_2022_pid_EDIT_.classList.add("error_mensaje");

                        }
                         else if(meta_2030_pid_EDIT_.length>10)
                        {
                            error_meta_2030_pid_EDIT_.innerHTML ="El limite son 10 caracteres";
                            error_meta_2030_pid_EDIT_.classList.add("error_mensaje");

                        }
                        else if(resultado_pid_2018_EDIT_.length>50)
                        {
                            error_resultado_pid_2018_EDIT_.innerHTML ="El limite son 50 caracteres";
                            error_resultado_pid_2018_EDIT_.classList.add("error_mensaje");

                        }
                        else
                        {

                          var usuario_pid="<?php echo($user_name)?>";

                          var fuente_pid_EDIT_ = $("#fuente_pid_EDIT_"+valor_entra).val();
                        var ref_adicionales_EDIT_ = $("#ref_adicionales_EDIT_"+valor_entra).val();
                        var linea_base_gen_EDIT_ = $("#linea_base_gen_EDIT_"+valor_entra).val();
                        var meta_2018_pid_EDIT_ = $("#meta_2018_pid_EDIT_"+valor_entra).val();
                        var meta_2019_pid_EDIT_ = $("#meta_2019_pid_EDIT_"+valor_entra).val();
                        var meta_2020_pid_EDIT_ = $("#meta_2020_pid_EDIT_"+valor_entra).val();
                        var meta_2021_pid_EDIT_ = $("#meta_2021_pid_EDIT_"+valor_entra).val();
                        var meta_2022_pid_EDIT_ = $("#meta_2022_pid_EDIT_"+valor_entra).val();
                        var meta_2030_pid_EDIT_ = $("#meta_2030_pid_EDIT_"+valor_entra).val();
                        var resultado_pid_2018_EDIT_ = $("#resultado_pid_2018_EDIT_"+valor_entra).val();

                          
                          $.ajax({
                            type: "POST",
                            url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                            data: {id_indicador:valor_entra,nom_usuario_pid: usuario_pid,fuente_pid_EDIT_:fuente_pid_EDIT_,ref_adicionales_EDIT_:ref_adicionales_EDIT_,linea_base_gen_EDIT_:linea_base_gen_EDIT_,meta_2018_pid_EDIT_:meta_2018_pid_EDIT_,meta_2019_pid_EDIT_:meta_2019_pid_EDIT_,meta_2020_pid_EDIT_:meta_2020_pid_EDIT_,meta_2021_pid_EDIT_:meta_2021_pid_EDIT_,meta_2022_pid_EDIT_:meta_2022_pid_EDIT_,meta_2030_pid_EDIT_:meta_2030_pid_EDIT_,resultado_pid_2018_EDIT_:resultado_pid_2018_EDIT_}
                            ,
                            success: function(msg){
                      
                              if(msg=1)
                              {
                                alert("Informacion Actualizada");
                                
                              }
                              
                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown) {
                              alert( "Error Verifica tu informacion" );
                            }
                          });
                          
                          
                          
                        }
                        
                      }
                      </script>

                                          
                      <input style="background-color: #555555;color: white;" type="button" onclick="SubmitForm_metas_EDIT_pids(<?php echo $registro['id_covid'];?>)"  value="Actualizar Informacion" />  
                                    
                      </form>



                    <div><!--comentarios de correccion del evaluador-->
                      <!--
                          <label>"<?php echo($registro_1['coment_evidencia']) ?>"</label>
                        -->
                    </div>
                  </div>
                 </div>
                </div>
              </div>
          </div>

          <div id="Parte_evidencias_pids<?php echo $registro['id_covid']; ?>" class="tabcontent_vert"
            >
              <div id="global">
                <div id="mensajes">
                  <div class="texto">
                    <div id="info_labels_<?php echo $registro['id_covid'];?>_2">
                      <form id="form_part_indicador" method="post">
                      <table>
                        <col width=20%>
                        <col width=80%>
                        <tbody>
                          <?php global $wpdb;
                            $pids_logros = $wpdb->prefix . 'covid';
                            $registros_2 = $wpdb->get_results("SELECT * FROM $pids_logros where id_covid='$i'", ARRAY_A);

                            foreach($registros_2 as $registro_2) { ?>

                              <tr>
		
                            
                                 <br>

                                 <div>
                                    <table>
                                      <col width=50%>
                                      <col width=50%>
                                        <tbody>
                                          <tr>
                                             <td style="text-align:center;" >
                                            <h3>Adjuntar evidencias</h5>
                                            <br>
                                            <p><label for="edit_upload_adjuntar_evidencias_EDITAR_pid_<?php echo $registro['id_covid'];?>">Puedes seleccionar multiples archivos</label></p>
                                            <div style="text-align:center;" id="edit_upload_adjuntar_evidencias_EDITAR_pid_<?php echo $registro['id_covid'];?>" name="edit_upload_adjuntar_evidencias_EDITAR_pid_<?php echo $registro['id_covid'];?>">  </div>
                                          </td>


                                          <td style="text-align:center;">
                                            
                                          </td>

                                          </tr>
                                         
                                           
                                        </tbody>
                                    </table>
                                </div> 


                                <script type="text/javascript">

               
                                   $(document).ready(function() 
                                                          {



                                                            /* abrir el apartado nuevo de inicio*/

                                                            open_accion_tac_pid(event, 'nuevo_pid');
       

                                     $("#edit_upload_adjuntar_evidencias_EDITAR_pid_<?php echo $registro['id_covid']; ?>").uploadFile
                                      ({

                                          url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                                          fileName:"myfile",
                                          maxFileCount:60,
                                          showCancel: true,
                                          showDone: true,
                                          showError: true,
                                          maxFileSize:1000*600000,
                                          showPreview:true,
                                          previewHeight: "100px",
                                          previewWidth: "100px",
                                          formData: {"nom_evidencia":"<?php echo $nom_ind;?> ","usuario":"<?php echo($user_name)?>","indice":"edit"},

                                          onSuccess:function(files,data,xhr,pd)
                                          {


                                            var id_sl = <?php echo $registro['id_covid']; ?>;
                                            var status = 0;

                                              $.ajax({
                                                type: "POST",
                                                url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                                                data: {id_indicador:id_sl,status:status}
                                                ,
                                              success: function(msg){
                                                //console.log(msg);
                                                if(msg==1)
                                                {
                                                  alert("Archivo subido correctamente");
                                                }
                                                  
                                                },
                                                error: function(XMLHttpRequest, textStatus, errorThrown) {
                                                   alert( "Error Verifica tu informacion" );
                                                }
                                              });
                                              //alert("arcivo subido");
                                          }
                                        
                                      }); 

                                  });




//-----------fin nuevo material--------------
                                </script> 

                              

                              <?php } 

                            ?>
                              </tbody>
                            </table>


                                    
                      </form>



                    <div><!--comentarios de correccion del evaluador-->
                
                    </div>
                  </div>
                 </div>
                </div>
              </div>
          </div>





        <?php  
        }?>                    

  </div>
</div>
		<!-- fin edicion pids-->

<div id="reporte_pids" class="tabcontent_tac_pid">


   <br>
    <h3>Reportes Acciones COVID-19</h3>
      
      <table>
        <col width=5%>
        <col width=65%>
        <col width=30%>
        <br>
        <br>
          <tbody>
              <?php global $wpdb;
                $ind_covid = $wpdb->prefix .'covid';
                $registros_pids = $wpdb->get_results("SELECT * FROM $ind_covid WHERE nombre_usuario_covid ='$user_name'", ARRAY_A);
                foreach($registros_pids as $registro_pid) 
                  { 

                    ?>
                    <?php $id_covid=$registro_pid['id_covid']; 
                     $tipo="covid";

                     echo $registros_pids;

                    ?>
                   
                      <tr>
                         <td>
                        
                        <img style="text-align: center" src="http://sigeh.hidalgo.gob.mx/repositorio/wp-content/uploads/2018/04/PDF.png" height="42" width="30" ></img>
                      </td>
                         <td id="txt_list_pdf_estr"> <?php    echo("<p class='demo'><a href='http://sigeh.hidalgo.gob.mx/repositorio/pdf_plugin/pdf_evidencias/crear_evidencia_pdf_covid_19.php?id_covid=$id_covid' target='covid' class='demo'>".$registro_pid['medida1']."</a></p>");?>
                        <br>
                                     
                      </td> 
                       <td>
                         
                       </td>

                       </tr>
                                    
                      <?php 

                    } 


          ?> 
          </tbody>
        
      </table> 

</div>

<script>
                 
    $( "#nombre_indicador_pid" ).autocomplete({  
      source: "/repositorio/wp-content/themes/EspecialesT2.1/templates/searh_indicador.php?id=<?php echo($user_name);?>&tipo=pids",
      minLength: 1,

       response: function(event, ui)        //TYERMINO DE REGISTRAR SUS INDICADORES
       {
        
          if (!ui.content.length) {
            /*
                 $('#registro_terminado').show(); 
                 var x = document.getElementById("nextBtn");
                 x.setAttribute("disabled", true);
                 */
           // alert("has terminado de cargar todos tus indicadores ");
          }
          
       }

    });

    //UNA VEZ SELECCIONADO EL NOMBRE DEL INDICADOR LOS DEMAS CAMPOS SE LLENARAN AUTOMATICAMENTE

    $( "#nombre_indicador_pid" ).autocomplete({
      close: function( event, ui ) 
      {

        
      },

      select: function( event, ui ) 
      {
        //alert(ui.item.value);
        $('#indicadore_seleccionado_autollenado_tacticos_pids').show();//RENDERIZAR EL RESTO DEL FORMULARIO

        var nom_indi_selec=ui.item.value;
        $.ajax({

               type: "GET",
               datatype: 'json',
               contentType: "application/json; charset=utf-8",
              
               url: "/repositorio/wp-content/themes/EspecialesT2.1/templates/indicador_seleccionado.php?nombre_organismo=<?php echo($user_name);?>&nom_indi_selec="+nom_indi_selec+"&tipo=pids",
               success: function(responce)
                {
                  
                  console.log(responce);
                  console.log(responce.descripcion_indicador);

                  /*-ocultos-*/
                    var nom_dep_pid = document.getElementById("nom_dependencia_pid");
                    nom_dep_pid.value=responce.nom_dependencia_asoc;

                    var descrp_tac_pid = document.getElementById("descrp_tac_pid");
                    descrp_tac_pid.value=responce.descripcion_indicador;

                    var nom_orga_pid = document.getElementById("nom_organismo_pid");
                    nom_orga_pid.value=responce.nom_organismo;

                    var periodicidad_pid = document.getElementById("periodicidad_pid");
                    periodicidad_pid.value=responce.periodicidad;

                    var indica_informe = document.getElementById("indica_informe");
                    indica_informe.value=responce.indica_informe;

                    var eje_ped_pid = document.getElementById("eje_ped_pid");
                    eje_ped_pid.value=responce.eje_ped;

                    var obj_estr_pid = document.getElementById("obj_estr_pid");
                    obj_estr_pid.value=responce.obj_estrategico;

                    var obj_gen_pid = document.getElementById("obj_gen_pid");
                    obj_gen_pid.value=responce.obj_general;

                    var avance_inf_2017 = document.getElementById("avance_inf_2017");
                    avance_inf_2017.value=responce.avanceinforme2017;


                  /*-ocultos-*/

                  var descr_indicador = document.getElementById("descrp_tac_pid");
                  descr_indicador.value=responce.descripcion_indicador;

                  var uni_obs = document.getElementById("unidad_observacion_ind_pid");
                  uni_obs.value=responce.unidad_observación_indicador;


                
                   if(responce.tipo_indicador=="Número")
                     {
                     // alert("es numero");
                      document.getElementById("tipo_indicador_def_pid").selectedIndex = 0;
                     }
                    else if(responce.tipo_indicador=="Porcentaje")
                     {

                      document.getElementById("tipo_indicador_def_pid").selectedIndex = 1;
                     }

                    else if(responce.tipo_indicador=="Tasa")
                     {
                      document.getElementById("tipo_indicador_def_pid").selectedIndex = 2;
                     }
                    else if(responce.tipo_indicador=="Absolutos")
                     {
                      document.getElementById("tipo_indicador_def_pid").selectedIndex = 3;
                     }

                     else if(responce.tipo_indicador=="Hectareas")
                     {
                      document.getElementById("tipo_indicador_def_pid").selectedIndex = 4;
                     }
                    else if(responce.tipo_indicador=="Cobertura")
                     {
                      document.getElementById("tipo_indicador_def_pid").selectedIndex = 5;
                     }

                     else if(responce.tipo_indicador=="Promedio")
                     {
                      document.getElementById("tipo_indicador_def_pid").selectedIndex = 6;
                     }

                     else if(responce.tipo_indicador=="Otro")
                     {
                      document.getElementById("tipo_indicador_def_pid").selectedIndex = 7;

                        $('#otro_tipo_indicador_pid').show();
                     }


                     if(responce.tendencia=="Ascendente")
                     {
                      
                      document.getElementById("tendencia_esp_pid").selectedIndex = 0;
                     }
                    else if(responce.tendencia=="Descendente")
                     {

                      document.getElementById("tendencia_esp_pid").selectedIndex = 1;
                     }

                    else if(responce.tendencia=="Constante")
                     {
                      document.getElementById("tendencia_esp_pid").selectedIndex = 2;
                     }
                    else if(responce.tendencia=="Variable")
                     {
                      document.getElementById("tendencia_esp_pid").selectedIndex = 3;
                     }

                     var obj_esp = document.getElementById("obj_especifico_prog_inst");
                      obj_esp.value=responce.objetivo_especifico_programa_institucional;


                      var base_Cal = document.getElementById("base_calculo_pid");
                      base_Cal.value=responce.base_calculo;

                     var fuent = document.getElementById("fuente_pid");
                      fuent.value=responce.fuente;

                      var ref_adi = document.getElementById("ref_adicionales");
                      ref_adi.value=responce.referencias_adicionales;

                      var lin_b_2017 = document.getElementById("linea_base_gen");
                      lin_b_2017.value=responce.linea_base_gen;

                     // var m_2017 = document.getElementById("meta_2017_pid");
                      //m_2017.value=responce.meta_2017;

                      var m_2018 = document.getElementById("meta_2018_pid");
                      m_2018.value=responce.meta_2018;

                      var m_2019 = document.getElementById("meta_2019_pid");
                      m_2019.value=responce.meta_2019;

                      var m_2020 = document.getElementById("meta_2020_pid");
                      m_2020.value=responce.meta_2020;

                      var m_2021 = document.getElementById("meta_2021_pid");
                      m_2021.value=responce.meta_2021;

                      var m_2022 = document.getElementById("meta_2022_pid");
                      m_2022.value=responce.meta_2022;

                      var m_2030 = document.getElementById("meta_2030_pid");
                      m_2030.value=responce.meta_2030;




                },
              error: function(data){
           }
        }); 
      }





      
    });


    $('#logros_en_indicador_pid').change(function()
      { 
          if($(this).val() == "Si")
          {
             $('#bullet_Del_logro_pid').show();
          }
          else if($(this).val() == "No")
          {
            $('#bullet_Del_logro_pid').hide();
          }


      });


      $('#premios_recono_pid').change(function()
        { 
         

             if($(this).val() == "Si")
            {
               $('#premios_reconocimientos_pid').show();
            }
            else if($(this).val() == "No")
            {
              $('#premios_reconocimientos_pid').hide();
            }


        });
</script>

<script type="text/javascript">

  var id_select_pid;
  var nom_ind_pid_d;
  var nom_ind_pid_selec_cortado;

   function get_id_select(id_select,nom_ind_pid_selec)
        {
          id_select_pid=id_select;
          nom_ind_pid_d=nom_ind_pid_selec;
          nom_ind_pid_selec_cortado = nom_ind_pid_selec.slice(0,58);
	  n_caracteres=(58);
        }
      var ruta_final;

      function download_file_eva_org(id,ruta,file)
      {

        nom_org='<?php echo $user_name;?>';
        ruta_download= "/repositorio/upload_files_plugin/php/"+nom_org+"/"+nom_ind_pid_selec_cortado+"/"+file;
        
          $.ajax({
                type: "GET",
                url: ruta_download,
                dataType: "jsonp",
                success: function (data, textStatus, xhr) {
                    alert("Success");
                },
                error: function (data, textStatus, xhr) {
                    if (data.status === 200) 
                    {
                        //alert("La 1ra direccion es correcta")
                        //alert(ruta_download);
                        document.getElementById("down_btn_eva_org_"+id).href=ruta_download;

                       // d_archive_path(id,ruta_download);
                    } 
                    else 
                    {

                      if(n_caracteres<66)
                      {
                         n_caracteres=n_caracteres+1;

                         nom_ind_pid_selec_cortado = nom_ind_pid_d.slice(0, n_caracteres);
                         download_file_eva_org(id,ruta,file);


                      }
                       
                        

                    }
                },
            });

        
        
       
      }
  

</script>

<script type="text/javascript">
                

                function edit_send_mjs_dep_covid(id_indicador)
                {
                 //
           
                 var id_indicador = id_indicador;
                 var resultado_1_pid_EDIT_ = document.getElementById("resultado_1_pid_EDIT_covid_"+id_indicador);
                 var semaforo_covid_EDIT_ = document.getElementById("semaforo_covid_EDIT_covid_"+id_indicador);
                 var que_se_hizo_EDIT_ = document.getElementById("que_se_hizo_EDIT_covid_"+id_indicador);
                 var cuanto_se_hizo_EDIT_ = document.getElementById("cuanto_se_hizo_EDIT_covid_"+id_indicador);
                 var a_que_poblacion_EDIT_ = document.getElementById("a_que_poblacion_EDIT_covid_"+id_indicador);
                 var beneficia_accion_EDIT_ = document.getElementById("beneficia_accion_EDIT_covid_"+id_indicador);
                 var beneficia_accion2_EDIT_ = document.getElementById("beneficia_accion2_EDIT_covid_"+id_indicador);
                 var donde_se_llevo_EDIT_ = document.getElementById("donde_se_llevo_EDIT_covid_"+id_indicador);
                 var existe_incidencia_covid_EDIT_ = document.getElementById("existe_incidencia_covid_EDIT_covid_"+id_indicador);
                 var existe_alguna_incidencia_covid_EDIT_ = document.getElementById("existe_alguna_incidencia_covid_EDIT_covid_"+id_indicador);
                 var bullet_EDIT_ = document.getElementById("bullet_EDIT_covid_"+id_indicador);

                 /*
                  alert(id_indicador);
                  alert(resultado_1_pid_EDIT_.value);
                  alert(semaforo_covid_EDIT_.value);
                  alert(que_se_hizo_EDIT_.value);
                  alert(cuanto_se_hizo_EDIT_.value);
                  alert(a_que_poblacion_EDIT_.value);
                  alert(beneficia_accion_EDIT_.value);
                  alert(beneficia_accion2_EDIT_.value);
                  alert(donde_se_llevo_EDIT_.value);
                  alert(existe_incidencia_covid_EDIT_.value);
                  alert(existe_alguna_incidencia_covid_EDIT_.value);
                  alert(bullet_EDIT_.value);

                  */
			var fecha = new Date();

                  var ultima_fecha_mod = "Fecha: "+fecha.getDate()+"/"+(fecha.getMonth()+1)+"/"+fecha.getFullYear()+ " "+"Hora: "+fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds();





                   $.ajax
                   ({

                    type: "POST",
                    url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia_COVID.php",
                    data: {id_indicador:id_indicador,resultado_1_pid_EDIT_:resultado_1_pid_EDIT_.value,semaforo_covid_EDIT_:semaforo_covid_EDIT_.value,que_se_hizo_EDIT_:que_se_hizo_EDIT_.value,cuanto_se_hizo_EDIT_:cuanto_se_hizo_EDIT_.value,a_que_poblacion_EDIT_:a_que_poblacion_EDIT_.value,beneficia_accion_EDIT_:beneficia_accion_EDIT_.value,beneficia_accion2_EDIT_:beneficia_accion2_EDIT_.value,donde_se_llevo_EDIT_:donde_se_llevo_EDIT_.value,existe_incidencia_covid_EDIT_:existe_incidencia_covid_EDIT_.value,existe_alguna_incidencia_covid_EDIT_:existe_alguna_incidencia_covid_EDIT_.value,bullet_EDIT_:bullet_EDIT_.value,ultima_fecha_EDIT:ultima_fecha_mod}
                    ,
                    success: function(msg)
                    {
                     // console.log(msg)
                      alert( "Informacion Actuali" );
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                       alert( "Error Verifica tu informacion" );
                    }
                   });
                   

                }
                
              </script>

<script type="text/javascript">

  $(document).ready(function() 
  {

     $('#indicadore_seleccionado_autollenado_tacticos_pids').hide();
     $('#bullet_Del_logro_pid').hide();
     $('#premios_reconocimientos_pid').hide();

      $('#datos_cargados_lug_1_pid').hide();
      $('#datos_cargados_lug_2_pid').hide();
      $('#datos_cargados_lug_3_pid').hide();
      $('#datos_cargados_lug_4_pid').hide();
      $('#datos_cargados_lug_5_pid').hide();
      $('#otro_tipo_indicador_pid').hide();
      $('#tipo_evidencia_otro_pid').hide();
      $('#existe_exp_doc').hide();



      $('#existe_alguna_incidencia').hide();
       

      $( "#resultado_1_pid" ).keyup(function() 
          {

            var nom_ind_loaded = document.getElementById("nombre_indicador_pid");
            var n_indicador=nom_ind_loaded.value;

            $("#upload_doc_resultados_1_pid").uploadFile
            ({
              
                url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                fileName:"myfile",
                maxFileCount:60,
                showCancel: true,
                showDone: true,
                showError: true,
                maxFileSize:1000*600000,
                showPreview:true,
                previewHeight: "100px",
                previewWidth: "100px",
                formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":1}, 

                onSuccess:function(files,data,xhr,pd)
                  {
                    //alert("archivo guardado falta actualizar la tabla de archivos")
 
                  } 
            }); 

            

            $("#upload_doc_resultados_2_pid").uploadFile
            ({
              
                url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                fileName:"myfile",
                maxFileCount:60,
                showCancel: true,
                showDone: true,
                showError: true,
                maxFileSize:1000*600000,
                showPreview:true,
                previewHeight: "100px",
                previewWidth: "100px",
                formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":2},
                onSuccess:function(files,data,xhr,pd)
                  {
                    //alert("archivo guardado falta actualizar la tabla de archivos")
 
                  }  
              
            }); 

            $("#upload_doc_resultados_3_pid").uploadFile
            ({
              
                url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                fileName:"myfile",
                maxFileCount:60,
                showCancel: true,
                showDone: true,
                showError: true,
                maxFileSize:1000*600000,
                showPreview:true,
                previewHeight: "100px",
                previewWidth: "100px",
                formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":3},
                onSuccess:function(files,data,xhr,pd)
                  {
                    //alert("archivo guardado falta actualizar la tabla de archivos")
 
                  }   
              
            }); 
  
            $("#upload_doc_resultados_4_pid").uploadFile
            ({
              
                url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                fileName:"myfile",
                maxFileCount:60,
                showCancel: true,
                showDone: true,
                showError: true,
                maxFileSize:1000*600000,
                showPreview:true,
                previewHeight: "100px",
                previewWidth: "100px",
                formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":4} ,
                onSuccess:function(files,data,xhr,pd)
                  {
                    //alert("archivo guardado falta actualizar la tabla de archivos")
 
                  }  
              
            }); 
                                    
            $("#upload_doc_resultados_5_pid").uploadFile
            ({


              
                url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                fileName:"myfile",
                maxFileCount:60,
                showCancel: true,
                showDone: true,
                showError: true,
                maxFileSize:1000*600000,
                showPreview:true,
                previewHeight: "100px",
                previewWidth: "100px",
                formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":5},
                onSuccess:function(files,data,xhr,pd)
                  {
                    //alert("archivo guardado falta actualizar la tabla de archivos")
 
                  }  
              
            }); 

            $("#upload_plan_trabajo").uploadFile
            ({

              
                url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                fileName:"myfile",
                maxFileCount:60,
                showCancel: true,
                showDone: true,
                showError: true,
                maxFileSize:1000*600000,
                showPreview:true,
                previewHeight: "100px",
                previewWidth: "100px",
                formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":"plan"},
                onSuccess:function(files,data,xhr,pd)
                  {
                    //alert("archivo guardado falta actualizar la tabla de archivos")
 
                  }  
              
            }); 

          $("#upload_adjuntar_evidencias_pid").uploadFile
            ({
                url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                fileName:"myfile",
                maxFileCount:60,
                showCancel: true,
                showDone: true,
                showError: true,
                maxFileSize:1000*600000,
                showPreview:true,
                previewHeight: "100px",
                previewWidth: "100px",
                formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":"evidencias"},
                onSuccess:function(files,data,xhr,pd)
                  {
                  
                  }  
              
            });

          });
});
      



  //document.getElementById("btn_acc_tac_pid_reg").click();
  
  tabcontent_tac_pid = document.getElementsByClassName("tabcontent_tac_pid");
    for (i = 0; i < tabcontent_tac_pid.length; i++) {
        tabcontent_tac_pid[i].style.display = "none";
    }

  function open_accion_tac_pid(evt, cityName) 
  {
    

    $('#intro_idicador_pids_tac_pids').hide();
    // Declare all variables
    var i, tabcontent_tac_pid, tablinks_tac_pids;

    // Get all elements with class="tabcontent_tac_pid" and hide them
    tabcontent_tac_pid = document.getElementsByClassName("tabcontent_tac_pid");
    for (i = 0; i < tabcontent_tac_pid.length; i++) {
        tabcontent_tac_pid[i].style.display = "none";
    }

    // Get all elements with class="tablinks_tac_pids" and remove the class "active"
    tablinks_tac_pids = document.getElementsByClassName("tablinks_tac_pids");
    for (i = 0; i < tablinks_tac_pids.length; i++) {
        tablinks_tac_pids[i].className = tablinks_tac_pids[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>

<script type="text/javascript">
  

  var currentTab = 0; // Current tab is set to be the first tab (0)
showTab_Acciones_pids(currentTab); // Display the current tab

function showTab_Acciones_pids(n) 
{
  // This function will display the specified tab of the form ...
  var x = document.getElementsByClassName("tab_pid");
  x[n].style.display = "block";
  // ... and fix the Previous/SIGUIENTE buttons:
  //alert(n+" ----limite"+(x.length-8));
  if (n == 0) {
    document.getElementById("prevBtn_pid").style.display = "none";
  } else {
    document.getElementById("prevBtn_pid").style.display = "inline";
  }
  if (n == (x.length-8)) 
  {     //CAMBIO DE 1 A 2 BRIAN
    document.getElementById("nextBtn_pid").innerHTML = "Enviar";
  } else {
    document.getElementById("nextBtn_pid").innerHTML = "Siguiente";
  }
  // ... and run a function that displays the correct step indicator:
  //alert(n);
  fixStepIndicator_Acciones_pids(n)
}

function nextPrev_covid(n) 
{
  //alert("entro aqui");
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab_pid");
  // Exit the function if any field in the current tab is invalid:
 if(n == 1 && !validateForm_covid()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;

  //alert(currentTab);
  //alert(x.length);



    if(currentTab==2)      
    {
    tipo_evidencia_get_values();
     //alert("cargar las evidencias seleccionaddas");
    }


   if (currentTab >= (x.length-8)) 
      {
         $('.inputDisabled').removeAttr("disabled");
        // alert("submit");
         var x = document.getElementById("nextBtn_pid");
         x.setAttribute("type", "submit");
  	      document.getElementById("nextBtn_pid").style.display = "none";
         document.getElementById("prevBtn_pid").style.display = "none";

        $('nextBtn_pid').on('click', function() 
        {
              $(this).prop('disabled', true);
        });
      return false;
      }

  showTab_Acciones_pids(currentTab);
}

function validateForm_covid() {
  
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab_pid");
  y = x[currentTab].getElementsByTagName("input");
    // A loop that checks every input field in the current tab:
    for (i = 0; i < y.length; i++) 
    {


      if (y[i].value == "") 
      {

      }

      else if(y[i].name=="myfile[]")
      {

      }


    }

    txtarea = x[currentTab].getElementsByTagName("textarea");
    // A loop that checks every input field in the current tab:
    for (i = 0; i < txtarea.length; i++) 
    {
      // If a field is empttxtarea...
      
      if (txtarea[i].value == "") 
      {

        if(txtarea[i].id=="existe_alguna_incidencia_covid")
        {

        }
        else if(txtarea[i].id=="resultado_3_pid")
        {

        }
        else if(txtarea[i].id=="resultado_4_pid")
        {

        }
        else if(txtarea[i].id=="resultado_5_pid")
        {

        }

       

        else
        {
                                      // add an "invalid" class to the field:
        txtarea[i].className += " invalid";
        // and set the current valid status to false:
        valid = false;
        }


      }
   

    }


  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step_pids")[currentTab].className += " finish";
   // alert("finish")
  }
  return valid; // return the valid status
}

function fixStepIndicator_Acciones_pids(n) {

  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step_pids");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class to the current step:
  x[n].className += " active";


  if(n==3)
  {
    alert("agregar el submit");
  }
 // alert(x[n].className);
}




</script>


  <!--DIALOG ADD MUNICIPIOS-->
<script>
  var indice=0;
  var resultados_1;
  var resultados_2;
  var resultados_3;//valor de los lugares selecionados por resultado
  var resultados_4;
  var resultados_5;

  var resultados_1_muni;
  var resultados_2_muni;
  var resultados_3_muni;
  var resultados_4_muni;
  var resultados_5_muni;
  var id_select;




    function open_lugares_pid(num_resultado,id)
    {
      
      indice=num_resultado;
      id_select=id;

       
      $( "#dialog-message-lugar_pid" ).dialog( "open" );     //   abrir menu opciones municipal estatal localidad
    }

    function open_estados_pid()
    {
      
      
      $( "#dialog-message-lugar_pid" ).dialog( "close" );    //   cerrar menu opcines
      //$( "#dialog-form-entidades_pid" ).data('indi', '1')
      $( "#dialog-form-entidades_pid" ).dialog( "open" );   //abrir estados
    
    }
    function open_municipios_pid()
    {
      //alert("municipios");
      $( "#dialog-message-lugar_pid" ).dialog( "close" );    //   cerrar menu opcines
      $( "#dialog-form_pid" ).dialog( "open" );   //abrir municipales
    }
    function open_localidades_pid()
    {
      $( "#dialog-message-lugar_pid" ).dialog( "close" );    //   cerrar menu opcines
      $( "#dialog-message-adjuntar-localidades_pid" ).dialog( "open" );   //abrir municipales

      var loc="localidades";

      if(indice==1)
      {
        $('#doc_res1_pid').show(); //mostar el aparatado parea subir archivos 1
        $('#doc_res2_pid').hide();
        $('#doc_res3_pid').hide();
        $('#doc_res4_pid').hide();
        $('#doc_res5_pid').hide();
        resultados_1_muni="localidades";
        var res1_loc =document.getElementById("res1_lugares_pid");
        res1_loc.value= "localidades";
        document.getElementById('datos_cargados_lug_1_pid').innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_1_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_1_pid').show();
      }

      else if(indice==2)
      {
        $('#doc_res2_pid').show(); //mostar el aparatado parea subir archivos 2
        $('#doc_res1_pid').hide(); 
        $('#doc_res3_pid').hide();
        $('#doc_res4_pid').hide();
        $('#doc_res5_pid').hide();

        resultados_2_muni="localidades";
        document.getElementById('datos_cargados_lug_2_pid').innerHTML ="Datos Cargados";
        var res2_loc =document.getElementById("res2_lugares_pid");
        res2_loc.value= "localidades";
         var element = document.getElementById("datos_cargados_lug_2_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_2_pid').show();
      }
      else if(indice==3)
      {
        $('#doc_res2_pid').hide(); //mostar el aparatado parea subir archivos 2
        $('#doc_res1_pid').hide(); 
        $('#doc_res3_pid').show();
        $('#doc_res4_pid').hide();
        $('#doc_res5_pid').hide();

        resultados_3_muni="localidades";
        document.getElementById('datos_cargados_lug_3_pid').innerHTML ="Datos Cargados";
        var res3_loc =document.getElementById("res3_lugares_pid");
        res3_loc.value= "localidades";
         var element = document.getElementById("datos_cargados_lug_3_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_3_pid').show();
      }
      else if(indice==4)
      {
        $('#doc_res2_pid').hide(); //mostar el aparatado parea subir archivos 2
        $('#doc_res1_pid').hide(); 
        $('#doc_res3_pid').hide();
        $('#doc_res4_pid').show();
        $('#doc_res5_pid').hide(); 

        resultados_4_muni="localidades";
        document.getElementById('datos_cargados_lug_4_pid').innerHTML ="Datos Cargados";
        var res4_loc =document.getElementById("res4_lugares_pid");
        res4_loc.value= "localidades";
         var element = document.getElementById("datos_cargados_lug_4_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_4_pid').show();
      }
      else if(indice==5)
      {
        $('#doc_res2_pid').hide(); //mostar el aparatado parea subir archivos 2
        $('#doc_res1_pid').hide(); 
        $('#doc_res3_pid').hide();
        $('#doc_res4_pid').hide();
        $('#doc_res5_pid').show();

        resultados_5_muni="localidades";
        document.getElementById('datos_cargados_lug_5_pid').innerHTML ="Datos Cargados";
        var res5_loc =document.getElementById("res5_lugares_pid");
        res5_loc.value= "localidades";
         var element = document.getElementById("datos_cargados_lug_5_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_5_pid').show();
      }
      //cao dinamico de los aparatados de edicion 
      if(document.getElementById('res1_lugares_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>') && indice==1)
      {

        //guardar info localidades en input 
        var lugar_1_edit=document.getElementById('res1_lugares_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>');

        
        lugar_1_edit.value=loc;
        //mensaje datos cargados
        document.getElementById('datos_cargados_lug_1_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>').innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_1_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_1_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>').show();


        

      }
      else if(document.getElementById('res2_lugares_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>') && indice==2)
      {
        var lugar_1_edit=document.getElementById('res2_lugares_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>');
        lugar_1_edit.value=loc;

                                    //mensaje datos cargados
        document.getElementById('datos_cargados_lug_2_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>').innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_2_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_2_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>').show();



      }

      else if(document.getElementById('res3_lugares_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>') && indice==3)
      {
        var lugar_1_edit=document.getElementById('res3_lugares_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>');
        lugar_1_edit.value=loc;

                                    //mensaje datos cargados
        document.getElementById('datos_cargados_lug_3_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>').innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_3_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_3_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>').show();



      }

      else if(document.getElementById('res4_lugares_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>') && indice==4)
      {
        var lugar_1_edit=document.getElementById('res4_lugares_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>');
        lugar_1_edit.value=loc;

                                    //mensaje datos cargados
        document.getElementById('datos_cargados_lug_4_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>').innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_4_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_4_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>').show();



      }

      else if(document.getElementById('res5_lugares_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>') && indice==5)
      {
        var lugar_1_edit=document.getElementById('res5_lugares_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>');
        lugar_1_edit.value=loc;

                                    //mensaje datos cargados
        document.getElementById('datos_cargados_lug_5_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>').innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_5_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_5_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>').show();



      }


   
    }
    








  $( function() 
  {
    var dialog, form,
 
      // From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
      emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
      name = $( "#name" ),
      email = $( "#email" ),
      password = $( "#password" ),
      allFields = $( [] ).add( name ).add( email ).add( password ),
      tips = $( ".validateTips" );
 
    function updateTips( t ) {
      tips
        .text( t )
        .addClass( "ui-state-highlight" );
      setTimeout(function() {
        tips.removeClass( "ui-state-highlight", 2500 );
      }, 500 );
    }
 
    function checkLength( o, n, min, max ) {
      if ( o.val().length > max || o.val().length < min ) {
        o.addClass( "ui-state-error" );
        updateTips( "Length of " + n + " must be between " +
          min + " and " + max + "." );
        return false;
      } else {
        return true;
      }
    }
 
    function checkRegexp( o, regexp, n ) {
      if ( !( regexp.test( o.val() ) ) ) {
        o.addClass( "ui-state-error" );
        updateTips( n );
        return false;
      } else {
        return true;
      }
    }
 
    function addUser_pid() 
    {
      $('#municipios_cargados').show();   
      var valid = true;
    
     // allFields.removeClass( "ui-state-error" );

     var arr_checks = document.getElementsByClassName("check_muni");
     var municipios_def_mun="Municipios,";


        for (i = 0; i < arr_checks.length; i++) 
      {

        if(arr_checks[i].checked == true)
        {
          municipios_def_mun+=i+",";
     
          
        }

      }


      if(indice==1)
      {
        resultados_1_muni=municipios_def_mun;
        document.getElementById('datos_cargados_lug_1_pid').innerHTML ="Datos Cargados";
        var res1_mun =document.getElementById("res1_lugares_pid");
        res1_mun.value= municipios_def_mun;
        var element = document.getElementById("datos_cargados_lug_1_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_1_pid').show();
      
      }

      else if(indice==2)
      {
        resultados_2_muni=municipios_def_mun;
        document.getElementById('datos_cargados_lug_2_pid').innerHTML ="Datos Cargados";
        var res2_mun =document.getElementById("res2_lugares_pid");
        res2_mun.value= municipios_def_mun;
         var element = document.getElementById("datos_cargados_lug_2_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_2_pid').show();
      }
      else if(indice==3)
      {
        resultados_3_muni=municipios_def_mun;
        document.getElementById('datos_cargados_lug_3_pid').innerHTML ="Datos Cargados";
        var res3_mun =document.getElementById("res3_lugares_pid");
        res3_mun.value= municipios_def_mun;
         var element = document.getElementById("datos_cargados_lug_3_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_3_pid').show();
      }
      else if(indice==4)
      {
        resultados_4_muni=municipios_def_mun;
        document.getElementById('datos_cargados_lug_4_pid').innerHTML ="Datos Cargados";
        var res4_mun =document.getElementById("res4_lugares_pid");
        res4_mun.value= municipios_def_mun;
         var element = document.getElementById("datos_cargados_lug_4_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_4_pid').show();
      }
      else if(indice==5)
      {
        resultados_5_muni=municipios_def_mun;
        document.getElementById('datos_cargados_lug_5_pid').innerHTML ="Datos Cargados";
        var res5_mun =document.getElementById("res5_lugares_pid");
        res5_mun.value= municipios_def_mun;
         var element = document.getElementById("datos_cargados_lug_5_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_5_pid').show();
      }


      if(document.getElementById('res1_lugares_pid_'+id_select) && indice==1)
      {

        //guardar info localidades en input 
        var lugar_1_edit=document.getElementById('res1_lugares_pid_'+id_select);

       
        lugar_1_edit.value=municipios_def_mun;
        //mensaje datos cargados
        document.getElementById('datos_cargados_lug_1_pid_'+id_select).innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_1_pid_"+id_select);
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_1_pid_'+id_select).show();


      }

      else if(document.getElementById('res2_lugares_pid_'+id_select) && indice==2)
      {
        var lugar_1_edit=document.getElementById('res2_lugares_pid_'+id_select);
        lugar_1_edit.value=municipios_def_mun;
                                    //mensaje datos cargados
        document.getElementById('datos_cargados_lug_2_pid_'+id_select).innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_2_pid_"+id_select);
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_2_pid_'+id_select).show();

      }

      else if(document.getElementById('res3_lugares_pid_'+id_select) && indice==3)
      {
        var lugar_1_edit=document.getElementById('res3_lugares_pid_'+id_select);
        lugar_1_edit.value=municipios_def_mun;

                                    //mensaje datos cargados
        document.getElementById('datos_cargados_lug_3_pid_'+id_select).innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_3_pid_"+id_select);
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_3_pid_'+id_select).show();


      }

      else if(document.getElementById('res4_lugares_pid_'+id_select) && indice==4)
      {
        var lugar_1_edit=document.getElementById('res4_lugares_pid_'+id_select);
        lugar_1_edit.value=municipios_def_mun;

                                    //mensaje datos cargados
        document.getElementById('datos_cargados_lug_4_pid_'+id_select).innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_4_pid_"+id_select);
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_4_pid_'+id_select).show();

      }

      else if(document.getElementById('res5_lugares_pid_'+id_select) && indice==5)
      {
        var lugar_1_edit=document.getElementById('res5_lugares_pid_'+id_select);
        lugar_1_edit.value=municipios_def_mun;

                                    //mensaje datos cargados
        document.getElementById('datos_cargados_lug_5_pid_'+id_select).innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_5_pid_"+id_select);
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_5_pid_'+id_select).show();

      }



       // alert("limpiar la fiesta");
        for (i = 0; i < arr_checks.length; i++) 
      {

        if(arr_checks[i].checked == true)
        {
          arr_checks[i].checked=false;
         // alert(municipios_def_mun);
          
        }

      }

      dialog.dialog( "close" );
     // alert("datos concatenados");
      return valid;

     
    }


    function addEntidades_pid()
    {
   
      //alert("selecciono entidades");
      $('#entidades_cargados').show();   
      var valid = true;
    
     // allFields.removeClass( "ui-state-error" );

     var arr_checks = document.getElementsByClassName("check_enti");
     var municipios_def="Entidades,";

        for (i = 0; i < arr_checks.length; i++) 
      {

        if(arr_checks[i].checked == true)
        {
          municipios_def+=i+",";
         // alert(municipios_def);
          
        }

      }

 

      if(indice==1)
      {

        resultados_1=municipios_def;
        document.getElementById('datos_cargados_lug_1_pid').innerHTML ="Datos Cargados";
        var res1 =document.getElementById("res1_lugares_pid");
        res1.value= municipios_def;
         var element = document.getElementById("datos_cargados_lug_1_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_1_pid').show();

      }
      else if(indice==2)
      {
        resultados_2=municipios_def;
        document.getElementById('datos_cargados_lug_2_pid').innerHTML ="Datos Cargados";
        var res2 =document.getElementById("res2_lugares_pid");
        res2.value= municipios_def;
         var element = document.getElementById("datos_cargados_lug_2_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_2_pid').show();
      }
      else if(indice==3)
      {
        resultados_3=municipios_def;
        document.getElementById('datos_cargados_lug_3_pid').innerHTML ="Datos Cargados";
        var res3 =document.getElementById("res3_lugares_pid");
        res3.value= municipios_def;
         var element = document.getElementById("datos_cargados_lug_3_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_3_pid').show();
      }
      else if(indice==4)
      {
        resultados_4=municipios_def;
        document.getElementById('datos_cargados_lug_4_pid').innerHTML ="Datos Cargados";
        var res4 =document.getElementById("res4_lugares_pid");
        res4.value= municipios_def;
         var element = document.getElementById("datos_cargados_lug_4_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_4_pid').show();
      }
      else if(indice==5)
      {
        resultados_5=municipios_def;
        document.getElementById('datos_cargados_lug_5_pid').innerHTML ="Datos Cargados";
        var res5 =document.getElementById("res5_lugares_pid");
        res5.value= municipios_def;
         var element = document.getElementById("datos_cargados_lug_5_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_5_pid').show();
      }



      if(document.getElementById('res1_lugares_pid_'+id_select) && indice==1)
      {
        //guardar info localidades en input 
        var lugar_1_edit=document.getElementById('res1_lugares_pid_'+id_select);

        $("#res1_lugares_pid_"+id_select).val(municipios_def);


        lugar_1_edit.value = municipios_def;
        //mensaje datos cargados
        document.getElementById('datos_cargados_lug_1_pid_'+id_select).innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_1_pid_"+id_select);
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_1_pid_'+id_select).show();


      }

      else if(document.getElementById('res2_lugares_pid_'+id_select) && indice==2)
      {
        var lugar_1_edit=document.getElementById('res2_lugares_pid_'+id_select);
        lugar_1_edit.value=municipios_def;

                                    //mensaje datos cargados
        document.getElementById('datos_cargados_lug_2_pid_'+id_select).innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_2_pid_"+id_select);
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_2_pid_'+id_select).show();



      }

      else if(document.getElementById('res3_lugares_pid_'+id_select) && indice==3)
      {
        var lugar_1_edit=document.getElementById('res3_lugares_pid_'+id_select);
        lugar_1_edit.value=municipios_def;

                                    //mensaje datos cargados
        document.getElementById('datos_cargados_lug_3_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>').innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_3_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_3_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>').show();



      }

      else if(document.getElementById('res4_lugares_pid_'+id_select) && indice==4)
      {
        var lugar_1_edit=document.getElementById('res4_lugares_pid_'+id_select);
        lugar_1_edit.value=municipios_def;

                                    //mensaje datos cargados
        document.getElementById('datos_cargados_lug_4_pid_'+id_select).innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_4_pid_"+id_select);
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_4_pid_'+id_select).show();


      }

      else if(document.getElementById('res5_lugares_pid_'+id_select) && indice==5)
      {
        var lugar_1_edit=document.getElementById('res5_lugares_pid_'+id_select);
        lugar_1_edit.value=municipios_def;

                                    //mensaje datos cargados
        document.getElementById('datos_cargados_lug_5_pid_'+id_select).innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_5_pid_"+id_select);
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_5_pid_'+id_select).show();



      }
  
      dialog_entidades_pid.dialog( "close" );
     // alert("datos concatenados");
      return valid;
    }
  //Cuadro de dialogo de municicpios
    dialog = $( "#dialog-form_pid" ).dialog({
        autoOpen: false,
        height: 700,
        width: 600,
        modal: true,
        buttons: {
          "Guardar": addUser_pid,

          Cancel: function() {
            dialog.dialog( "close" );
          }
        },
        close: function() {
          form[ 0 ].reset();
          allFields.removeClass( "ui-state-error" );
        }
    });
 
    form = dialog.find( "form" ).on( "submit", function( event ) {
      event.preventDefault();
      addUser_pid();
    });

    //Cuadro de dialogo de entidades
    dialog_entidades_pid = $( "#dialog-form-entidades_pid" ).dialog({
        autoOpen: false,
        height: 700,
        width: 600,
        modal: true,
        buttons: {
          "Guardar": addEntidades_pid,

          Cancel: function() {
            dialog_entidades_pid.dialog( "close" );
          }
        },
        close: function() {
          form[ 0 ].reset();
          allFields.removeClass( "ui-state-error" );
        }
    });
 
    form = dialog_entidades_pid.find( "form" ).on( "submit", function( event ) {
      event.preventDefault();
      addEntidades_pid();
    });



    // fin entidades
 


      $('#es_posible_desagregar_geograficamente_def').change(function()
      { 

      if($(this).val() == "Municipios")
      {
         dialog.dialog( "open" );
         $('#upload_tabla_localidades').hide();
          $('#entidades_cargados').hide();
          $('#municipios_cargados').hide();
          // document.getElementById('des_geo_entidades').value="0" ; 

      }

      else if($(this).val() == "Localidades" ){
         $('#upload_tabla_localidades').show();
         $('#municipios_cargados').hide();//ocultar mensaje de municipiuos cargardos
         $('#entidades_cargados').hide();
         
      }

      else if($(this).val() == "Otras entidades"){
         dialog_entidades_pid.dialog( "open" );

         $('#upload_tabla_localidades').hide();
         $('#municipios_cargados').hide();  //ocultar mensaje de municipiuos cargardos
         $('#entidades_cargados').hide();
         //document.getElementById('des_geo_municipios_list').value="0" ; 
      }

      else if($(this).val() == "Estado de Hidalgo"){
         $('#upload_tabla_localidades').hide();
         $('#municipios_cargados').hide();  //ocultar mensaje de municipiuos cargardos
         $('#entidades_cargados').hide();
      }

      });

  } );

</script>

<!-- DIALOGO CONFIRMACION-->
<script>

  $( function()
    {
      $( "#dialog-message-adjuntar-localidades_pid" ).dialog({
        autoOpen: false,
        modal: true,
        height: 350,
        width: 450,
        buttons: {
          Ok: function() {
            $( this ).dialog( "close" );
          }
        }
      });
    } );



  
    $( function()
    {
      $( "#dialog-message-lugar_pid" ).dialog({
        autoOpen: false,
        modal: true,
        height: 350,
        width: 450,
        buttons: {
          Ok: function() {
            $( this ).dialog( "close" );
          }
        }
      });
    } );



   $( function()
    {
      $( "#dialog-message" ).dialog({
        autoOpen: false,
        modal: true,
        height: 350,
        width: 450,
        buttons: {
          Ok: function() {
            $( this ).dialog( "close" );
          }
        }
      });
    } );


    $('#plan_trabajo_def').change(function()
    { 
        

      if($(this).val() == "No")
      {
      $( "#dialog-message" ).dialog( "open" );

      }
    });
    //si se checa la opcion otro adjuntar el docuemento q necesitan
    $('#plan_trabajo_def').change(function()
    { 
        

      if($(this).val() == "No")
      {
      $( "#dialog-message" ).dialog( "open" );

      }
    });

    $('#check_evi_pid_Otro').change(function()
    { 
      //alert("holasss")
          
         var check_otros = document.getElementById("check_evi_pid_Otro");

  
        if(check_otros.checked == true)
        {
           $('#tipo_evidencia_otro_pid').show();
        }
        else
        {
          $('#tipo_evidencia_otro_pid').hide();

        }


    });
 

  $('#logros_en_indicador').change(function()
    { 
     

         if($(this).val() == "Si")
        {
           $('#bullet_Del_logro').show();
        }
        else if($(this).val() == "No")
        {
          $('#bullet_Del_logro').hide();
        }


    });


  $('#premios_recono').change(function()
    { 
     

         if($(this).val() == "Si")
        {
           $('#premios_reconocimientos').show();
        }
        else if($(this).val() == "No")
        {
          $('#premios_reconocimientos').hide();
        }


    });

  $('#tipo_indicador_def_pid').change(function()
    { 

    if($(this).val() == "Otro")
    {
    $('#otro_tipo_indicador_pid').show();

    }
    if($(this).val() != "Otro"){
    $('#otro_tipo_indicador_pid').hide();
    }
    });


   function tipo_evidencia_get_values()
    {
      var arr_checks = document.getElementsByClassName("check_type_evi_pid");
     var tipo_evidencia_def="";

        for (i = 0; i < arr_checks.length; i++) 
      {

        if(arr_checks[i].checked == true)
        {
          tipo_evidencia_def+=arr_checks[i].name+",";
        
          
        }

      }
     var tipo_evi_ =document.getElementById("tipo_evidencia_def_list_pid");
     tipo_evi_.value= tipo_evidencia_def;
  
    }

     $('#existe_expediente_doc_pid').change(function(){ 
        if($(this).val() == "Si")

          {
          $('#no_existe_expediente_doc').hide();
          $('#existe_exp_doc').show();
      
          }
          if($(this).val() == "No"){
          $('#existe_exp_doc').hide();
          $('#no_existe_expediente_doc').show();

          }


      });


     $( function() 
      {
        $( "#datepicker_pid_inicio" ).datepicker();
       // $( "#datepicker_pid_fin" ).datepicker();
        
      });


</script>


<!-- cuadros de dialogo selectrs-->

