<style type="text/css">

  #regForm {
    background-color: #ffffff;
    margin: 100px auto;
    padding: 40px;
    width: 70%;
    min-width: 300px;
  }

  /* Style the input fields */
  input {
    padding: 10px;
    width: 100%;
    font-size: 17px;
    font-family: Raleway;
    border: 1px solid #aaaaaa;
  }

  /* Mark input boxes that gets an error on validation: */
  input.invalid {
    background-color: #ffdddd;
  }

  /* Hide all steps by default: */
  .tab_acciones {
    display: none;
  }

  /* Make circles that indicate the steps of the form: */
  .step_acciones {
    height: 15px;
    width: 15px;
    margin: 0 2px;
    background-color: #bbbbbb;
    border: none; 
    border-radius: 50%;
    display: inline-block;
    opacity: 0.5;
  }

  /* Mark the active step_acciones: */
  .step_acciones.active {
    opacity: 1;
  }

  /* Mark the step_accioness that are finished and valid: */
  .step_acciones.finish {
    background-color: #4CAF50;
  }


  /*TABS*/

  /* Style the tab */
.tab_concurrentes {
       overflow: hidden;
    border: 1px solid #ccc;
    background-color: #
}

/* Style the buttons that are used to open the tab_concurrentes content */
.tab_concurrentes button {
 background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
}

/* Change background color of buttons on hover */
.tab_concurrentes button:hover {
     background-color: #ffffff;
}

/* Create an active/current tab_concurrenteslink class */
.tab_concurrentes button.active {
   background-color: #1b2935;
  
  color: #ffffff;
}

/* Style the tab content */
.tabcontent_concurrente {
    display: none;
  padding: 6px 12px;
  border: 3px solid #ccc;
  /*border-top: none;*/
  background-color: #ffffff;
}
/*TABS VERTICAL*/

/* Style the buttons that are used to open and close the accordion panel */
.accordion {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    text-align: left;
    border: none;
    outline: none;
    transition: 0.4s;
}

/* Add a background color to the button if it is clicked on (add the .active class with JS), and when you move the mouse over it (hover) */
.active, .accordion:hover {
    background-color: #ccc;
}

/* Style the accordion panel. Note: hidden by default */
.panel {
    padding: 0 18px;
    background-color: white;
    display: none;
    overflow: hidden;
}
</style>


<?php global $display_name , $user_name;
      get_currentuserinfo();

     // echo $display_name . "'s email address is: " . $user_name;
      $usuario=$user_name;
?>



<div class="tab_concurrentes">
  <button id="btn_acc_concurrente_reg" class="tablinks_concurrentes" disabled="true" onclick="open_accion_concurrente(event, 'Accion_concurrente')">ACCION</button>
  <button id="btn_acc_concurrente_edit" class="tablinks_concurrentes" disabled="true" onclick="open_accion_concurrente(event, 'Estatus_concurrente')">ESTATUS</button>
  <button id="btn_acc_concurrente_repdf" class="tablinks_concurrentes" disabled="true" onclick="open_accion_concurrente(event, 'REPORTES_CONCURRENTES')">REPORTES</button>
</div>

<div id="intro_idicador_acciones_concurrentes" class="container" >
  <p  ><font text-align: center; font-family: Graphik-Bold  color="green" size="5">ACCIONES CONCURRENTES</font></p>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>

</div>




<div id="Accion_concurrente" class="tabcontent_concurrente">
  <form method="post" id="Form_acciones_concurrentes" name="Form_acciones_concurrentes">

    <p><font text-align: center; font-family: Graphik-Bold  color="green" size="5">ACCIONES CONCURRENTES</font></p>

    <div class="tab_acciones">
      <br>
      <br>
      <p><label for="eje_concu">Selecciona el eje correspondiente</label>
      <div class="select">
        <br>
        <select name="eje_principal_concu" id="eje_concu">  
          <option value="1">Eje 1 - Gobierno Honesto, Cercano y Moderno</option>
          <option value="2">Eje 2 - Hidalgo Próspero y Dinámico</option>
          <option value="3">Eje 3 - Hidalgo Humano e Igualitario</option>
          <option value="4">Eje 4 - Hidalgo Seguro, con Justicia y en Paz</option>
          <option value="5">Eje 5 - Hidalgo con Desarrollo Sostenible</option>
        </select>
        <br>
       
      </div>
      <br>
    </div>
   
    <div class="tab_acciones">
      <br>
       <p><label for="objetivo_est_concu">Selecciona el objetivo estratégico correspondiente</label>
        <p><input placeholder="Objetivo estratégico" name="objetivo_est_concu" id="objetivo_est_concu"></p>
      <br>
    </div>

    <div class="tab_acciones">
      <br>
      <br>
      <p><label for="objetivo_gen_concu">Selecciona el objetivo general correspondiente</label>
      <p><input placeholder="Objetivo general"  name="objetivo_gen_concu" id="objetivo_gen_concu"></p>
    </div>




      <div class="tab_acciones">
        <br>
        <br>
        <p><label id="error_nom_accion"  for="nom_accion">Nombre de la accion realizada:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (250 caracteres)</strong></label>
        
        <textarea type="text" maxlength="250" placeholder="Nombre de la accion realizada" name="accion_realizada" id="nom_accion"></textarea>
        <br>

        <p><label for="accion_realizada_concu">Selecciona Las dependencias involucradas</label>

        <fieldset>
          <p id="check_list_dependecias"></p>
          <script>
                  var dependecias_arr = [
          "Oficialía Mayor",
          "Procuraduría General del Estado",
          "Secretaría de Contraloría",
          "Secretaría de Cultura",
          "Secretaría de Desarrollo Agropecuario",
          "Secretaría de Desarrollo Económico",
          "Secretaría de Desarrollo Social",
          "Secretaría de Educación Pública",
          "Secretaría de Finanzas Públicas",
          "Secretaría de Gobierno",
          "Secretaría de Medio Ambiente y Recursos Naturales",
          "Secretaría de Movilidad y Transporte",
          "Secretaría de Obras Públicas y Ordenamiento Territorial",
          "Secretaría de Salud",
          "Secretaría de Seguridad Pública",
          "Secretaría de Turismo",
          "Secretaría del Trabajo y Previsión Social",
          "Secretaría Ejecutiva de la Política Pública Estatal",
          "Sistema Nacional para el Desarrollo Integral de la Familia",
          "Unidad de Planeación y Prospectiva"];
            var text = "";
            var i;
            text+="<ul>"
            for (i = 0; i < dependecias_arr.length; i++) 
            {
               text +='<div>'+

                 '<label align="left" class="checkbox-inline "> '+'<input type="checkbox"   class="check_dependendias" id="check_evi_'+dependecias_arr[i]+'"  name="'+dependecias_arr[i]+'">'+dependecias_arr[i]+'</label>'

                 +'</div>';
            }
            text+="</ul>"
            document.getElementById("check_list_dependecias").innerHTML = text;
         

          </script>    
        </fieldset>

        <p><input type="hidden"  id="dependecias_selecionadas_concu" name="dependecias_selecionadas_concu"></p>
        <p><input type="hidden"  id="accion_concurrente_nom_dependencia" name="accion_concurrente_nom_dependencia"></p>
      </div>

      <div class="tab_acciones">DESCRIPCION
        <br>
        <br>
        <p><label id="error_descrip_acciones"  for="nom_accion">Describe cuales fueron las acciones:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (250 caracteres)</strong></label>
        
        <p><textarea placeholder="Descripcion de las acciones" maxlength="250" name="descipcion_acciones" id="descipcion_acciones"></textarea>
      </div>


      <div class="tab_acciones">
        <label style="text-align: center color: #4CAF50; font-size: 17px; font-family: Graphik-Bold" >Informacion lista</label>
        <input type="hidden"  name="oculto_acciones" value="1">
       
        <h2><font font-family: Graphik-Bold color="green" size="6">ENVIAR INFORMACION</font></h2>
     </div>



    <div style="overflow:auto;">
      <div style="float:right;">
        <button type="button" id="prevBtn_Acciones" onclick="nextPrev_Acciones(-1)">Anterior</button>
        <button type="button" name="enviar_form_acciones_concurrentes" id="nextBtn_Acciones" onclick="nextPrev_Acciones(1)">Siguiente</button>
      </div>
    </div>

    
    <div style="text-align:center;margin-top:40px;">
      <span class="step_acciones"></span>
      <span class="step_acciones"></span>
      <span class="step_acciones"></span>
      <span class="step_acciones"></span>
      <span class="step_acciones"></span>
      <span class="step_acciones"></span>


    </div>
  <br>
  </form>
</div>

<div id="Estatus_concurrente" class="tabcontent_concurrente">
  
    <br>
      <div class="container">
      <?php 

      global $wpdb;
      $evidencias = $wpdb->prefix . 'acciones_concurrentes';
      $registros = $wpdb->get_results("SELECT * FROM $evidencias WHERE nomb_dependencia='$user_name'", ARRAY_A);
      foreach($registros as $registro) 
        { 
          $i=$registro['id_accion_concurrente'];
          $nom_ind=$registro['nombre_accion'];
          $btn_ac_des=$registro['status'];
         
          ?>

         <button class="accordion"  id="acordeon_id_<?php echo($registro['id_accion_concurrente'])?>">
          <div>
            <table>
              <col width=80%>
              <col width=20%>
                <tbody>
                 <td class="" ="contenido_datos" > <?php echo $registro['nombre_accion'];?></td> 
                 <td class="" ="contenido_datos" >

                    <?php  if($registro['status'] == 0)
                    {
                      echo("< h2style='color:#008ec5;'>En proceso</h2>");

                    }
                    else if($registro['status'] == 1)
                    {
                      echo("<h4 style='color:#0bd03e;'>Aprobado</h>");
                    } 
                     else if($registro['status'] == 2)
                    {
                      echo("<h4 style='color:#f74515;'>Corregir</h>");
                    } 
                     else if($registro['status'] == 5)
                    {
                      echo("<label style='text-align: center; font-size: 20px; font-family: Graphik-Bold ' color:#727272; '>Editar</label>");
                    } 
                      ?>                      
                  </td> 
                </tbody>
            </table>
          </div>  
         </button>

          <div class="panel">
            <div class="tab_vertical">

              <button  id="defaultOpen_vertical" class="tablinks_vertical" onclick="open_vertical_tab(event, 'Parte_eje_concu<?php echo $registro['id_accion_concurrente']; ?>')">EJE<i  class="fas fa-redo" 
             id="check_green_indicador_<?php echo $registro['id_accion_concurrente']; ?>" style="font-size:20px;color:green;text-shadow:2px 2px 4px #000000;"></i></button>
              <button class="tablinks_vertical" onclick="open_vertical_tab(event, 'Parte_accion_concu<?php echo $registro['id_accion_concurrente']; ?>')">ACCIONES<i  
             id="check_green_evidencia_<?php echo $registro['id_accion_concurrente']; ?>" style="font-size:20px;color:green;text-shadow:2px 2px 4px #000000;"></i></button>
              
                <script>

                  var button_add_deisabled =document.getElementById("acordeon_id_<?php echo($registro['id_accion_concurrente'])?>");

                        if(<?php echo($registro['status'])?>==0)
                        {
                          
                          button_add_deisabled.setAttribute("disabled", true);
                         
                        }
                        else if(<?php echo($registro['status'])?>==1)
                        {
                          
                          button_add_deisabled.setAttribute("disabled", true);
                         
                        }
                        else if(<?php echo($registro['status'])?>==2)
                        {
                           
                         
                        }
                        else if(<?php echo($registro['status'])?>==5)
                        {
                           
                         //editable
                        }


                    //parte indicador
                    if("<?php echo $registro['coment_indicador']; ?>"!="" || "<?php echo $registro['status']; ?>"==5)
                    {$('#check_green_indicador_<?php echo $registro['id_accion_concurrente']; ?>').hide();}
                    else
                    {$('#check_green_indicador_<?php echo $registro['id_accion_concurrente']; ?>').show();}
                   
                    //parte evidencia
                    if("<?php echo $registro['coment_evidencia']; ?>"!="" || "<?php echo $registro['status']; ?>"==5)
                    {$('#check_green_evidencia_<?php echo $registro['id_accion_concurrente']; ?>').hide();}
                    else
                    {$('#check_green_evidencia_<?php echo $registro['id_accion_concurrente']; ?>').show();}
                    //parte inconsistencias

                    //parte documentyos

                </script>


            </div>




              <form  method="post" id="signupForm_correccion" name="signupForm_correccion" > 

              <div id="Parte_eje_concu<?php echo $registro['id_accion_concurrente']; ?>" class="tabcontent_vertical">
                <div id="global">
                    <div id="mensajes">
                      <div class="texto">
                        <div id="info_labels_<?php echo $registro['id_accion_concurrente'];?>">
                         <form id="form_part_indicador" method="post">
                         
                            <table>
                            <col width=20%>
                            <col width=80%>
                              <tbody>
                                <?php global $wpdb;
                                $resultado;
                                $evidencias = $wpdb->prefix . 'acciones_concurrentes';
                                $registros_1 = $wpdb->get_results("SELECT * FROM $evidencias where id_accion_concurrente='$i'", ARRAY_A);
                                foreach($registros_1 as $registro_1) { 
                                  ?>

                                  <tr>
                                    <br>
                                    <p><label id="error_eje_concu_edit_<?php echo($registro_1['id_accion_concurrente'])?>" for="eje_concu_edit_<?php echo($registro_1['id_accion_concurrente'])?>">Selecciona el eje correspondiente</label>
                                    <div class="select">
                                      <br>
                                      <select name="eje_principal_concu" id="eje_concu_edit_<?php echo($registro_1['id_accion_concurrente'])?>">  
                                        <option value="1">Eje 1 - Gobierno Honesto, Cercano y Moderno</option>
                                        <option value="2">Eje 2 - Hidalgo Próspero y Dinámico</option>
                                        <option value="3">Eje 3 - Hidalgo Humano e Igualitario</option>
                                        <option value="4">Eje 4 - Hidalgo Seguro, con Justicia y en Paz</option>
                                        <option value="5">Eje 5 - Hidalgo con Desarrollo Sostenible</option>
                                      </select>
                                      <br>
                                    </div>
                                  </tr>

                                  <tr>
                                    <br>
                                    <p><label id="error_objetivo_est_concu_<?php echo($registro_1['id_accion_concurrente'])?>" for="objetivo_est_concu_<?php echo($registro_1['id_accion_concurrente'])?>">Selecciona el objetivo estratégico correspondiente</label>
                                    <p><input placeholder="Objetivo estratégico" onfocus="foco_obj_est(<?php echo($registro_1['eje_principal'])?>)" value="<?php echo($registro_1['objetivo_est'])?>" name="objetivo_est_concu" id="objetivo_est_concu_<?php echo($registro_1['id_accion_concurrente'])?>"></p>
                                    <br>

                                    <br>
                                      <p><label id="error_objetivo_gen_concu_edit_<?php echo($registro_1['id_accion_concurrente'])?>" for="objetivo_gen_concu_edit_<?php echo($registro_1['id_accion_concurrente'])?>">Selecciona el objetivo general correspondiente</label>
                                      <p><input placeholder="Objetivo general" onfocus="foco_obj_general(<?php echo($registro_1['id_accion_concurrente'])?>)" value="<?php echo($registro_1['objetivo_gen'])?>" name="objetivo_gen_concu_edit_<?php echo($registro_1['id_accion_concurrente'])?>" id="objetivo_gen_concu_edit_<?php echo($registro_1['id_accion_concurrente'])?>"></p>
                                    <br>

                                  </tr>
                                 
                                  <?php } 

                                ?>
                              </tbody>
                            </table>

                        <script type="text/javascript">

                          var val_obj_edit;
                          var bandera_eje=false;


                               if("<?php echo($registro_1['eje_principal'])?>"=="1")
                               {
                                document.getElementById("eje_concu_edit_<?php echo($registro_1['id_accion_concurrente'])?>").selectedIndex = 0;
                               
                               }
                               else if("<?php echo($registro_1['eje_principal'])?>"=="2")
                               {
                                document.getElementById("eje_concu_edit_<?php echo($registro_1['id_accion_concurrente'])?>").selectedIndex = 1;
                                
                               }
                               else if("<?php echo($registro_1['eje_principal'])?>"=="3")
                               {
                                document.getElementById("eje_concu_edit_<?php echo($registro_1['id_accion_concurrente'])?>").selectedIndex = 2;
                                
                               }
                               else if("<?php echo($registro_1['eje_principal'])?>"=="4")
                               {
                                document.getElementById("eje_concu_edit_<?php echo($registro_1['id_accion_concurrente'])?>").selectedIndex = 3;
                                
                               }
                               else if("<?php echo($registro_1['eje_principal'])?>"=="5")
                               {
                                document.getElementById("eje_concu_edit_<?php echo($registro_1['id_accion_concurrente'])?>").selectedIndex = 4;
                               
                               }

                                //var valor_eje_edit=1;
                                $('#eje_concu_edit_<?php echo($registro_1['id_accion_concurrente'])?>').change(function()
                                { 
                                  bandera_eje=true;
                                  if($(this).val() == "1")
                                  { 
                                    valor_eje_edit=1;
                                  }
                                  else if($(this).val() == "2")
                                  {
                                    valor_eje_edit=2;
                                  }
                                  else if($(this).val() == "3")
                                  {
                                    valor_eje_edit=3;
                                  }
                                  else if($(this).val() == "4")
                                  {
                                    valor_eje_edit=4;
                                  }
                                  else if($(this).val() == "5")
                                  {
                                    valor_eje_edit=5;
                                  }
                                });

                                function foco_obj_est(id)
                                {
                                  if(bandera_eje==false)
                                  {
                                    if(id=="1")
                                    {
                                      valor_eje_edit=1;
                                    }
                                    else if (id=="2")
                                    {
                                      valor_eje_edit=2;
                                    }
                                    else if(id=="3")
                                    {
                                      valor_eje_edit=3;
                                    }
                                    else if(id=="4")
                                    {
                                      valor_eje_edit=4;
                                    }
                                    else if(id=="5")
                                    {
                                      valor_eje_edit=5;
                                    }
                                  }
                                     // alert(value_eje.value)
                                     console.log(valor_eje_edit);
                                  }

                                $( "#objetivo_est_concu_<?php echo($registro_1['id_accion_concurrente'])?>" ).autocomplete({
                                  source : function (request, response) 
                                  {
                                    $.getJSON (
                                      // Ruta del archivo que se ejecuta.
                                      "/repositorio/wp-content/themes/EspecialesT2.1/templates/searh_objetivos.php",
                                      // Valores que requiere el formulario para hacer la busqueda.
                                      {term: request.term, eje: valor_eje_edit},
                                      // Muestra los datos de la busqueda.
                                      response
                                    );
                                  },

                                  minLength: 1,
                                  select : function (event, ui) 
                                  {
                                    // En la acción seleccionada
                                  }
                                });

                          
                                function foco_obj_general(id)
                                {

                                  var obj = document.getElementById("objetivo_est_concu_"+id);
                                  val_obj_edit = obj.value
                                  val_obj_edit= val_obj_edit.substr(0,3);
                                 //  alert("get new id"+val_obj_edit);
                                 console.log(val_obj_edit);
                                }
                                  

                                $( "#objetivo_gen_concu_edit_<?php echo($registro_1['id_accion_concurrente'])?>" ).autocomplete({

                                  source : function (request, response) 
                                  {
                                    $.getJSON (
                                      // Ruta del archivo que se ejecuta.
                                      "/repositorio/wp-content/themes/EspecialesT2.1/templates/searh_objetivos.php",
                                      // Valores que requiere el formulario para hacer la busqueda.
                                      {term: request.term, eje_obj_gen: val_obj_edit},
                                      // Muestra los datos de la busqueda.
                                      response
                                    );
                                  },

                                  minLength: 1,
                                  select : function (event, ui) 
                                  {
                                    // En la acción seleccionada
                                  }
                                });
                             
      

                              function SubmitFormData_up_concu_eje(valor_entra) 
                              {

                                var eje_concu_edit_ = $("#eje_concu_edit_"+valor_entra).val();
                                var objetivo_est_concu_ = $("#objetivo_est_concu_"+valor_entra).val();
                                var objetivo_gen_concu_edit_ = $("#objetivo_gen_concu_edit_"+valor_entra).val();
                                
                                


                                

                                 var error_objetivo_est_concu_ = document.getElementById("error_objetivo_est_concu_"+valor_entra);

                                 var error_objetivo_gen_concu_edit_ = document.getElementById("error_objetivo_gen_concu_edit_"+valor_entra);


                                if(objetivo_est_concu_.length<0)
                                {
                                 
                                  error_objetivo_est_concu_.innerHTML ="Verifica tu informacion";
                                  error_objetivo_est_concu_.classList.add("error_mensaje");

                                }
                                else if(objetivo_gen_concu_edit_.length<0)
                                {
                                 
                                  objetivo_gen_concu_edit_.innerHTML ="Verifica tu informacion";
                                  objetivo_gen_concu_edit_.classList.add("error_mensaje");


                                }
                             


                                else
                                {

                                    $.ajax({
                                    type: "POST",
                                    url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                                    data: {id_indicador:valor_entra,eje_concu_edit_: eje_concu_edit_, objetivo_est_concu_:objetivo_est_concu_,objetivo_gen_concu_edit_:objetivo_gen_concu_edit_}
                                    ,
                                    success: function(msg){
                                      alert( "Informacion Actualizada" );
                                    },
                                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                                       alert( "Error Verifica tu informacion" );
                                    }
                                  });


                                    error_objetivo_est_concu_.classList.remove("error_mensaje");
                                    error_objetivo_est_concu_.innerHTML ="Selecciona el objetivo estratégico correspondiente";

                                    objetivo_gen_concu_edit_.classList.remove("error_mensaje");
                                    objetivo_gen_concu_edit_.innerHTML ="Selecciona el objetivo general correspondiente";

                                }

                              }
                        </script>
                            
                            <input style="background-color: #555555;color: white;"  type="button" id="submit_indicador" onclick="SubmitFormData_up_concu_eje(<?php echo $registro['id_accion_concurrente'];?>)" value="Actualizar Informacion" /> </input> 
                                           
                          </form>

                       
                      </div>
                    </div>
                  </div>
                  <br>
                  <br>
                </div>
              </div>

              <div id="Parte_accion_concu<?php echo $registro['id_accion_concurrente']; ?>" class="tabcontent_vertical">
                  <div id="global">
                    <div id="mensajes">
                      <div class="texto">
                        <div id="info_labels_<?php echo $registro['id_accion_concurrente'];?>_2">
                          <form id="form_part_indicador" method="post">
                          <table>
                            <col width=20%>
                            <col width=80%>
                            <tbody>
                              <?php global $wpdb;
                                $evidencias = $wpdb->prefix . 'acciones_concurrentes';
                                $registros_2 = $wpdb->get_results("SELECT * FROM $evidencias where id_accion_concurrente='$i'", ARRAY_A);

                                foreach($registros_2 as $registro_2) { ?>
                                <script type="text/javascript">


                                </script>

                                <p><label id="error_accion_1_<?php echo($registro_1['id_accion_concurrente'])?>" for="accion_realizada_concu">Nombre de la accion realizada<strong class="tamanio_caracteres" id="tamanio_caracteres"> (250 caracteres)</strong></label>
                                  <textarea type="text" maxlength="250" placeholder="Nombre de la accion realizada" name="accion_realizada" id="nom_accion_<?php echo($registro_1['id_accion_concurrente'])?>" value="" ><?php echo($registro_1['nombre_accion'])?></textarea></p>
                                  <br>

                                  <p><label for="accion_realizada_concu">Selecciona Las dependencias involucradas</label>

                                  <fieldset>
                                    <p id="check_list_dependecias_<?php echo($registro_1['id_accion_concurrente'])?>"></p>
                                    <script>
                                            var dependecias_arr = [
                                    "Oficialía Mayor",
                                    "Procuraduría General del Estado",
                                    "Secretaría de Contraloría",
                                    "Secretaría de Cultura",
                                    "Secretaría de Desarrollo Agropecuario",
                                    "Secretaría de Desarrollo Económico",
                                    "Secretaría de Desarrollo Social",
                                    "Secretaría de Educación Pública",
                                    "Secretaría de Finanzas Públicas",
                                    "Secretaría de Gobierno",
                                    "Secretaría de Medio Ambiente y Recursos Naturales",
                                    "Secretaría de Movilidad y Transporte",
                                    "Secretaría de Obras Públicas y Ordenamiento Territorial",
                                    "Secretaría de Salud",
                                    "Secretaría de Seguridad Pública",
                                    "Secretaría de Turismo",
                                    "Secretaría del Trabajo y Previsión Social",
                                    "Secretaría Ejecutiva de la Política Pública Estatal",
                                    "Sistema Nacional para el Desarrollo Integral de la Familia",
                                    "Unidad de Planeación y Prospectiva"];
                                      var text = "";
                                      var i;
                                      text+="<ul>"
                                      for (i = 0; i < dependecias_arr.length; i++) 
                                      {
                                         text +='<div>'+

                                           '<label align="left" class="checkbox-inline "> '+'<input type="checkbox"   class="check_dependendias_<?php echo($registro_1['id_accion_concurrente'])?>" id="check_evi_'+dependecias_arr[i]+'"  name="'+dependecias_arr[i]+'">'+dependecias_arr[i]+'</label>'

                                           +'</div>';
                                      }
                                      text+="</ul>"
                                      document.getElementById("check_list_dependecias_<?php echo($registro_1['id_accion_concurrente'])?>").innerHTML = text;




                                    var arr_dep =  "<?php echo($registro_1['dependencias_involucradas'])?>";
                                    arr_dep = arr_dep.slice(0, -1);
                                   var lug_array = arr_dep.split(",");



                                 var arr_checks_false = document.getElementsByClassName("check_dependendias_<?php echo($registro_1['id_accion_concurrente'])?>");
                                 

                                  for (i = 0; i < lug_array.length; i++) 
                                  {
                                    
                                      if(lug_array[i]=="Oficialía Mayor"  )
                                          {
                                           // alert("oficialia mayor true");
                                          arr_checks_false[0].checked = true;

                                          }
                                      else if(lug_array[i]=="Procuraduría General del Estado"  )
                                          {
                                            //alert("es un Diagnostico");
                                            arr_checks_false[1].checked = true;
                                          }
                                      else if(lug_array[i]=="Secretaría de Contraloría"  )
                                          {
                                           // alert("Padrones o bases de datos");
                                            arr_checks_false[2].checked = true;
                                          }
                                      else if(lug_array[i]=="Secretaría de Cultura"  )
                                          {
                                            //alert("Publicación en físico o electrónico");
                                            arr_checks_false[3].checked = true;
                                          }
                                      else if(lug_array[i]=="Secretaría de Desarrollo Agropecuario"  )
                                          {
                                            //alert("Reporte");
                                            arr_checks_false[4].checked = true;
                                          }
                                      else if(lug_array[i]=="Secretaría de Desarrollo Económico"  )
                                          {
                                            //alert("Otro");
                                            arr_checks_false[5].checked = true;
                                          }
                                      else if(lug_array[i]=="Secretaría de Desarrollo Social"  )
                                          {
                                           // alert("Padrones o bases de datos");
                                            arr_checks_false[6].checked = true;
                                          }
                                      else if(lug_array[i]=="Secretaría de Educación Pública"  )
                                          {
                                            //alert("Publicación en físico o electrónico");
                                            arr_checks_false[7].checked = true;
                                          }
                                      else if(lug_array[i]=="Secretaría de Finanzas Públicas"  )
                                          {
                                            //alert("Reporte");
                                            arr_checks_false[8].checked = true;
                                          }
                                      else if(lug_array[i]=="Secretaría de Gobierno"  )
                                          {
                                            //alert("Otro");
                                            arr_checks_false[9].checked = true;
                                          }
                                      else if(lug_array[i]=="Secretaría de Medio Ambiente y Recursos Naturales"  )
                                          {
                                           // alert("Padrones o bases de datos");
                                            arr_checks_false[10].checked = true;
                                          }
                                      else if(lug_array[i]=="Secretaría de Movilidad y Transporte"  )
                                          {
                                            //alert("Publicación en físico o electrónico");
                                            arr_checks_false[11].checked = true;
                                          }
                                      else if(lug_array[i]=="Secretaría de Obras Públicas y Ordenamiento Territorial"  )
                                          {
                                            //alert("Reporte");
                                            arr_checks_false[12].checked = true;
                                          }
                                      else if(lug_array[i]=="Secretaría de Salud"  )
                                          {
                                            //alert("Otro");
                                            arr_checks_false[13].checked = true;
                                          }

                                      else if(lug_array[i]=="Secretaría de Seguridad Pública"  )
                                          {
                                           // alert("Padrones o bases de datos");
                                            arr_checks_false[14].checked = true;
                                          }
                                      else if(lug_array[i]=="Secretaría de Turismo"  )
                                          {
                                            //alert("Publicación en físico o electrónico");
                                            arr_checks_false[15].checked = true;
                                          }
                                      else if(lug_array[i]=="Secretaría del Trabajo y Previsión Social"  )
                                          {
                                            //alert("Reporte");
                                            arr_checks_false[16].checked = true;
                                          }
                                      else if(lug_array[i]=="Secretaría Ejecutiva de la Política Pública Estatal"  )
                                          {
                                            //alert("Otro");
                                            arr_checks_false[17].checked = true;
                                          }
                                      else if(lug_array[i]=="Sistema Nacional para el Desarrollo Integral de la Familia"  )
                                          {
                                           // alert("Padrones o bases de datos");
                                            arr_checks_false[18].checked = true;
                                          }
                                      else if(lug_array[i]=="Unidad de Planeación y Prospectiva"  )
                                          {
                                            //alert("Publicación en físico o electrónico");
                                            arr_checks_false[19].checked = true;
                                          }
                                    
                                  }

                                    </script>    
                                  </fieldset>
                                  <br>
                                   <tr>

                                    <p><input type="hidden"  id="dependecias_selecionadas_concu_<?php echo($registro_1['id_accion_concurrente'])?>" name="dependecias_selecionadas_concu_<?php echo($registro_1['id_accion_concurrente'])?>"></p>

                                    <p><label id="error_accion_2_<?php echo($registro_1['id_accion_concurrente'])?>" for="accion_realizada">Describe cuales fueron las acciones<strong class="tamanio_caracteres" id="tamanio_caracteres"> (250 caracteres)</strong></label>
                                    <p><textarea placeholder="Descripcion de las acciones" maxlength="250" name="descipcion_acciones" id="descipcion_acciones_<?php echo($registro_1['id_accion_concurrente'])?>" value="" ><?php echo($registro_1['descripcion_acciones'])?></textarea></p>
                                  </tr>
                                   

                                  <?php } 

                                ?>
                                  </tbody>
                                </table>

                          <script type="text/javascript">
                            //ocultar el mensaje datos cargados

                            function SubmitFormData_acc_concurrentes(valor_entra) 
                            {


                              //obtener las dependecias
                              var arr_checks = document.getElementsByClassName("check_dependendias_"+valor_entra);

                              var dependencias_check="";

                              for (i = 0; i < arr_checks.length; i++) 
                              {

                                if(arr_checks[i].checked == true)
                                {
                                  dependencias_check+=arr_checks[i].name+",";
                                }

                              }

                              var input_dependecias_seleccionadas =document.getElementById("dependecias_selecionadas_concu_"+valor_entra);
                              input_dependecias_seleccionadas= dependencias_check;

                              var nom_accion_=$("#nom_accion_"+valor_entra).val();
                              var accion_err_nom = document.getElementById("error_accion_1_"+valor_entra);
                             
                              var dependecias_selecionadas_concu_=input_dependecias_seleccionadas;

                              var descipcion_acciones_=$("#descipcion_acciones_"+valor_entra).val();  
                              var accion_err_des = document.getElementById("error_accion_2_"+valor_entra);

                              if(nom_accion_.length>250)
                              {
                                                                  
                                  accion_err_nom.innerHTML ="El limite son 250 caracteres";
                                  accion_err_nom.classList.add("error_mensaje");

                              }
                              else if(descipcion_acciones_.length>250)
                              {
                                  accion_err_des.innerHTML ="El limite son 250 caracteres";
                                  accion_err_des.classList.add("error_mensaje");

                              }
                              else
                              {
                                  $.ajax({
                                    type: "POST",
                                    url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                                    data: {id_indicador:valor_entra,nom_accion_:nom_accion_,dependecias_selecionadas_concu_:dependecias_selecionadas_concu_,descipcion_acciones_:descipcion_acciones_}
                                    ,
                                    success: function(msg){
                                      if(msg=1)
                                      {
                                        alert("Informacion Actualizada");
                                      }
                                      
                                    },
                                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                                       alert( "Error Verifica tu informacion" );
                                    }
                                  });

                                  accion_err_nom.classList.remove("error_mensaje");
                                  accion_err_nom.innerHTML ="Nombre del indicador";

                                  accion_err_des.classList.remove("error_mensaje");
                                  accion_err_des.innerHTML ="Descripcion del indicador";
                              }


                            }
                          </script>

                                              <br>
                          <input style="background-color: #555555;color: white;" type="button" id="submit_indicador" onclick="SubmitFormData_acc_concurrentes(<?php echo $registro['id_accion_concurrente'];?>)" value="Actualizar Informacion" />  
                                        
                          </form>



                        <div><!--comentarios de correccion del evaluador-->
                          <!--
                              <label>"<?php echo($registro_1['coment_evidencia']) ?>"</label>
                            -->
                        </div>
                      </div>
                     </div>
                    </div>
                  </div>
              </div>


          


        </form>
      </div>
      <?php  
      } ?>                    

      </div>


   
</div>

<div id="REPORTES_CONCURRENTES" class="tabcontent_concurrente">

    <br>
    <h3>ACCIONES CONCURRENTES</h3>
      
      <table>
        <col width=5%>
        <col width=65%>
        <col width=30%>
        <br>
        <br>
          <tbody>
              <?php global $wpdb;
                $ind_estrategicos = $wpdb->prefix .'acciones_concurrentes';
                $registros_estrategicos_1 = $wpdb->get_results("SELECT * FROM $ind_estrategicos WHERE nomb_dependencia ='$user_name'", ARRAY_A);
                foreach($registros_estrategicos_1 as $registro_estrategicos_1) 
                  { ?>
                    <?php $id_evidencia=$registro_estrategicos_1['id_accion_concurrente']; 
                     $tipo="concurrentes";
                    ?>
                   
                      <tr>
                         <td>
                        
                        <img style="text-align: center" src="http://192.168.64.2/repositorio/wp-content/uploads/2018/04/PDF.png" height="42" width="30" ></img>
                      </td>
                         <td id="txt_list_pdf_estr"> <?php    echo("<p class='demo'><a href='pdf_plugin/pdf_evidencias/crear_evidencia_pdf.php?id_evidencia= $id_evidencia&arr_ids_tacticos=0&user=$user_name&tipo=$tipo' target='_blank' class='demo'>".$registro_estrategicos_1['objetivo_est']."</a></p>");?></td> 

                         <td id="contenido_datos" >   
                         
                          <?php echo(  $registro_estrategicos_1['fecha_registro']);?>
                      
                       </td>
                  <?php } ?> 
          </tbody>
        
      </table> 

</div>



<script type="text/javascript">

  $(document).ready(function() 
  {
    //activar modulos
     $.ajax({
         type: "GET",
         datatype: 'json',
         contentType: "application/json; charset=utf-8",
        
         url: "/repositorio/wp-content/themes/EspecialesT2.1/templates/ctrl_modulos.php?nom_dep="+'<?php echo($user_name);?>',
         success: function(respuesta)
          {
            
            var arr_mod = respuesta.slice(1, -1);
            var arr_mod = arr_mod.split(",");

            var i;
            for (i = 0; i < arr_mod.length; i++) { 
              if(arr_mod[i]=="0")
              {
                 $("#btn_acc_concurrente_reg").removeAttr('disabled');
                 
               
              }
              else if(arr_mod[i]=="1")
              {
                 $("#btn_acc_concurrente_edit").removeAttr('disabled');
                 
              }

              else if(arr_mod[i]=="2")
              {
                 $("#btn_acc_concurrente_repdf").removeAttr('disabled');
                
              }


            
              //console.log(arr_mod[i]);
            }
            
           
          },
          error: function(data)
          {

          }
         });

      });



 // document.getElementById("defaultOpen_concurrentes").click();
  
  tabcontent_concurrente = document.getElementsByClassName("tabcontent_concurrente");
    for (i = 0; i < tabcontent_concurrente.length; i++) {
        tabcontent_concurrente[i].style.display = "none";
    }

  function open_accion_concurrente(evt, cityName) {

    $('#intro_idicador_acciones_concurrentes').hide();
    // Declare all variables
    var i, tabcontent_concurrente, tablinks_concurrentes;

    // Get all elements with class="tabcontent_concurrente" and hide them
    tabcontent_concurrente = document.getElementsByClassName("tabcontent_concurrente");
    for (i = 0; i < tabcontent_concurrente.length; i++) {
        tabcontent_concurrente[i].style.display = "none";
    }

    // Get all elements with class="tablinks_concurrentes" and remove the class "active"
    tablinks_concurrentes = document.getElementsByClassName("tablinks_concurrentes");
    for (i = 0; i < tablinks_concurrentes.length; i++) {
        tablinks_concurrentes[i].className = tablinks_concurrentes[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>

<script type="text/javascript">
  

  var currentTab = 0; // Current tab is set to be the first tab (0)
showTab_Acciones(currentTab); // Display the current tab

function showTab_Acciones(n) {
  // This function will display the specified tab of the form ...
  var x = document.getElementsByClassName("tab_acciones");
  x[n].style.display = "block";
  // ... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn_Acciones").style.display = "none";
  } else {
    document.getElementById("prevBtn_Acciones").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn_Acciones").innerHTML = "Enviar";
  } else {
    document.getElementById("nextBtn_Acciones").innerHTML = "Siguiente";
  }
  // ... and run a function that displays the correct step indicator:
  fixStepIndicator_Acciones(n)
}

function nextPrev_Acciones(n) {
  
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab_acciones");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm_Acciones()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;


      if(currentTab==2)
    {
      var obj = document.getElementById("objetivo_est_concu");
       val_obj = obj.value
      val_obj= val_obj.substr(0,3);
     // alert(val_obj);
    }


      if(currentTab==5)
    {
     cargar_chek_dependecias();
    }


   if (currentTab >= x.length) 
      {
       
        //cargar_chek_dependecias();
       var x = document.getElementById("nextBtn_Acciones");
       x.setAttribute("type", "submit");
       return false;
      }
  // if you have reached the end of the form... :
 // alert(currentTab);
 /*
   if (currentTab >= 2) 
   {
       var x = document.getElementById("nextBtn_Acciones");
       x.setAttribute("type", "submit");
       return false;
   }
   */

  // Otherwise, display the correct tab:
  showTab_Acciones(currentTab);
}

function validateForm_Acciones() {
  
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab_acciones");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {

        if(y[i].id=="dependecias_selecionadas_concu")
                            {

                            }
                            else
                            {
                                    // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false:
      valid = false;
                            }

    }

    else if (y[i].id == "descipcion_acciones" && y[i].value.length >250 ) 
      {
          //nombre del indicador
         document.getElementById('error_descrip_acciones').innerHTML ="el maximo es de 250 caracteres";
         var element = document.getElementById("error_descrip_acciones");
         element.classList.add("error_mensaje");
          // add an "invalid" class to the field:
        y[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }

       else if (y[i].id == "nom_accion" && y[i].value.length >250 ) 
      {
          //nombre del indicador
         document.getElementById('error_nom_accion').innerHTML ="el maximo es de 250 caracteres";
         var element = document.getElementById("error_nom_accion");
         element.classList.add("error_mensaje");
          // add an "invalid" class to the field:
        y[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step_acciones")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator_Acciones(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step_acciones");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class to the current step:
  x[n].className += " active";
}


    function cargar_chek_dependecias()
    {
     
      var arr_checks = document.getElementsByClassName("check_dependendias");
      var dependencias_check="";

        for (i = 0; i < arr_checks.length; i++) 
      {

        if(arr_checks[i].checked == true)
        {
          dependencias_check+=arr_checks[i].name+",";
     
          
        }

      }



     var input_dependecias_seleccionadas =document.getElementById("dependecias_selecionadas_concu");
     input_dependecias_seleccionadas.value= dependencias_check;
//alert(input_dependecias_se-leccionadas.value);
    }


    $( "#objetivo_gen_concu" ).autocomplete({

    source : function (request, response) 
    {
      $.getJSON (
        // Ruta del archivo que se ejecuta.
        "/repositorio/wp-content/themes/EspecialesT2.1/templates/searh_objetivos.php",
        // Valores que requiere el formulario para hacer la busqueda.
        {term: request.term, eje_obj_gen: val_obj},
        // Muestra los datos de la busqueda.
        response
      );
    },

    minLength: 1,
    select : function (event, ui) 
    {
      // En la acción seleccionada
    }
  });

</script>

<script type="text/javascript">

  // Asignación del valor del "eje" a la variable "valor_eje" para poderla usar en el "autocomplete".
  var valor_eje=1;

  $('#eje_concu').change(function()
  { 
    if($(this).val() == "1")
    { 
      valor_eje=1;
    }
    else if($(this).val() == "2")
    {
      valor_eje=2;
    }
    else if($(this).val() == "3")
    {
      valor_eje=3;
    }
    else if($(this).val() == "4")
    {
      valor_eje=4;
    }
    else if($(this).val() == "5")
    {
      valor_eje=5;
    }


  });

  // "Autocomplete" que busca los "objetivos estratégicos" en base el "eje" seleccionado.
  $( "#objetivo_est_concu" ).autocomplete({

    source : function (request, response) 
    {
      $.getJSON (
        // Ruta del archivo que se ejecuta.
        "/repositorio/wp-content/themes/EspecialesT2.1/templates/searh_objetivos.php",
        // Valores que requiere el formulario para hacer la busqueda.
        {term: request.term, eje: valor_eje},
        // Muestra los datos de la busqueda.
        response
      );
    },

    minLength: 1,
    select : function (event, ui) 
    {
      // En la acción seleccionada
    }
  });


</script>