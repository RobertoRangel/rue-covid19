<Style>
#regForm {
  background-color: #ffffff;
  margin: 100px auto;
  padding: 40px;
  width: 70%;
  min-width: 300px;
}

/* Style the input fields */
input {
  padding: 10px;
  width: 100%;
  font-size: 17px;
  font-family: Raleway;
  border: 1px solid #aaaaaa;
}

/* Mark input boxes that gets an error on validation: */
input.invalid_avance {
  background-color: #ffdddd;
}

/* Hide all step_avances by default: */
.tab_avance {
  display: none;
}

/* Make circles that indicate the step_avances of the form: */
.step_avance {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbbbbb;
  border: none; 
  border-radius: 50%;
  display: inline-block;
  opacity: 0.5;
}

/* Mark the active step_avance: */
.step_avance.active {
  opacity: 1;
}

/* Mark the step_avances that are finished and valid: */
.step_avance.finish {
  background-color: #4CAF50;
}

</Style>

<?php global $display_name , $user_name;
      get_currentuserinfo();

      $usuario=$user_name;
?>

<div class="tab_concurrentes">
  <button class="tablinks_avance_prog" disabled="true" id="btn_avance_reg" onclick="open_avance_progra(event, 'avance_programatico_reg')">AVANCE PROGRAMATICO</button>
  <button class="tablinks_avance_prog" disabled="true" id="btn_avance_edit" onclick="open_avance_progra(event, 'estatus_avance_prog')">ESTATUS</button>
  <button class="tablinks_avance_prog" disabled="true" id="btn_avance_repdf" onclick="open_avance_progra(event, 'reporte_avance_prog')">REPORTES</button>
</div>

<div id="intro_idicador_avance_prog" class="container" >
  <p  ><font text-align: center; font-family: Graphik-Bold  color="green" size="5">AVANCE PROGRAMATICO</font></p>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>

</div>




<div id="avance_programatico_reg" class="tabcontent_avance_prog">
  <form method="post" id="Form_avance_prog" name="Form_avance_prog">
      <br>
       <p><font text-align: center; font-family: Graphik-Bold  color="green" size="5">AVANCE PROGRAMATICO</font></p>
       <br>

 

      <div class="tab_avance">
        <br>
        <br>
        <p><input name="nom_prog_presupuestario" id="nom_prog_presupuestario" placeholder="Programa presupuiestario" oninput="this.className = ''"></p>
        <div id="autollenado_avance">
          <br>
          <h1 style="text-align: center;"><font text-align: center; font-family: Graphik-Bold  color="green" size="5">NIVEL FIN</font></h1>
          <br>
          <br>
          
          <p><label id="error_desc_general_fin"  for="desc_general_fin">Descripcion Generall <strong class="tamanio_caracteres" id="tamanio_caracteres"></strong></label>
          <textarea name="desc_general_fin" id="desc_general_fin" placeholder="Descripcion General" oninput="this.className = ''"></textarea>
          <br>
          <p><label id="error_indicador_fin"  for="indicador_fin">Indicador <strong class="tamanio_caracteres" id="tamanio_caracteres"></strong></label>
          <p><input name="indicador_fin" id="indicador_fin" placeholder="Indicador" oninput="this.className = ''"></p>
          <br>
          <p><label id="error_indicador_fin"  for="meta_prog_2018_fin">Meta Programada 2018 <strong class="tamanio_caracteres" id="tamanio_caracteres"></strong></label>
          <p><input name="meta_prog_2018_fin" maxlength="15" id=meta_prog_2018_fin placeholder="Meta Programada 2018" oninput="this.className = ''"></p>
          <br>
          <p><label id="error_result_estim_30_jun_prop"  for="result_estim_30_jun_fin">Resultado Estimado 30 de Junio(Primer Semestre) <strong class="tamanio_caracteres" id="tamanio_caracteres">(15 caracteres)</strong></label>
          <p><input name="result_estim_30_jun_fin" maxlength="15" id="result_estim_30_jun_fin" placeholder="Resultado Estimado 30 de Junio(Primer Semestre)" oninput="this.className = ''"></p>

           <br>
          <p><label id="error_avance_fin_meta_2018" disabled for="avance_fin_meta_2018">Avance Porcentual de la meta 2018 <strong class="tamanio_caracteres" id="tamanio_caracteres"></strong></label>
          <p><input name="avance_fin_meta_2018"  id="avance_fin_meta_2018" placeholder="Avance Porcentual de la meta 2018" oninput="this.className = ''" onfocus="calcuar_avance_fin(this)" ></p>
          <br>
          

        </div>
        <br>
      </div>

      <script type="text/javascript">
        function calcuar_avance_fin()
            {

              var result_estim_30_jun_fin = document.getElementById("result_estim_30_jun_fin");

              var meta_prog_2018_fin = document.getElementById("meta_prog_2018_fin");

              if(result_estim_30_jun_fin.value.length>10)
              {                   
                error_result_estim_30_jun_prop.innerHTML ="Verifica tu informacion";
                error_result_estim_30_jun_prop.classList.add("error_mensaje");
              }
              else
              {
                  var res_val=parseFloat(result_estim_30_jun_fin.value);
                  var meta_val=parseFloat(meta_prog_2018_fin.value);
                  
                  var avance_fin_meta_2018 = document.getElementById("avance_fin_meta_2018");
                      avance_fin_meta_2018.value=(res_val/meta_val * 100).toFixed(2);
                  //alert(avance_fin_meta_2018.value);
              }
            }

              function calcuar_avance_proposito()
            {

              var result_estim_30_jun_prop = document.getElementById("result_estim_30_jun_prop");

              var meta_prog_2018_prop = document.getElementById("meta_prog_2018_prop");

              if(result_estim_30_jun_prop.value.length>10)
              {                   
                error_result_estim_30_jun_prop.innerHTML ="Verifica tu informacion";
                error_result_estim_30_jun_prop.classList.add("error_mensaje");
              }
              else
              {

                var res_val_prop=parseFloat(result_estim_30_jun_prop.value);
                var meta_val_prop=parseFloat(meta_prog_2018_prop.value);
    

                var avance_porc_meta_2018 = document.getElementById("avance_porc_meta_2018");
                      avance_porc_meta_2018.value=(res_val_prop/meta_val_prop * 100).toFixed(2);
                //alert(avance_fin_meta_2018.value);
              }
            }



      </script>

      <div class="tab_avance">
          
          <h1 style="text-align: center;"><font text-align: center; font-family: Graphik-Bold  color="green" size="5">NIVEL PROPOSITO</font></h1>
          
     
          <br>
          <br>
          <p><label id="error_desc_general_prop"  for="desc_general_prop">Descripcion Generall <strong class="tamanio_caracteres" id="tamanio_caracteres"></strong></label>
          <textarea name="desc_general_prop" id="desc_general_prop" placeholder="Descripcion General" oninput="this.className = ''"></textarea>
          <br>
          <p><label id="error_indicador_prop"  for="indicador_prop">Indicador <strong class="tamanio_caracteres" id="tamanio_caracteres"></strong></label>
          <p><input name="indicador_prop" id="indicador_prop" placeholder="Indicador" oninput="this.className = ''"></p>
          <br>
          <p><label id="error_meta_prog_2018_prop"  for="meta_prog_2018_prop">Meta Programada 2018 <strong class="tamanio_caracteres" id="tamanio_caracteres"></strong></label>
          <p><input name="meta_prog_2018_prop" maxlength="15" id=meta_prog_2018_prop placeholder="Meta Programada 2018" oninput="this.className = ''"></p>
          <br>
          <p><label id="error_result_estim_30_jun_prop"  for="result_estim_30_jun_prop">Resultado Estimado 30 de Junio(Primer Semestre) <strong class="tamanio_caracteres" id="tamanio_caracteres">(15 caracteres)</strong></label>
          <p><input name="result_estim_30_jun_prop" maxlength="15" id="result_estim_30_jun_prop" placeholder="Resultado Estimado 30 de Junio(Primer Semestre)" oninput="this.className = ''"></p>
          <br>
          <br>
          <p><label id="error_avance_porc_meta_2018" disabled for="avance_porc_meta_2018">Avance Porcentual de la meta 2018 <strong class="tamanio_caracteres" id="tamanio_caracteres"></strong></label>
          <p><input name="avance_porc_meta_2018"  onfocus="calcuar_avance_proposito(this)" id="avance_porc_meta_2018" placeholder="Avance Porcentual de la meta 2018" oninput="this.className = ''"></p>
          <br>
        
      </div>
      <div class="tab_avance">AVANCE FINACIERO:
        <br>
        <br>
          <p><label id="error_inversion_autorizada_fin_2017"  for="inversion_autorizada_fin">PRESUPUESTO AUTORIZADO (ANUAL 2017)  <strong class="tamanio_caracteres" id="tamanio_caracteres">(15 caracteres)</strong></label>
          <p><input name="inversion_autorizada_fin" maxlength="15" id="inversion_autorizada_fin" placeholder="PRESUPUESTO AUTORIZADO (ANUAL 2017) " oninput="this.className = ''"></p>

          <br>
          <p><label id="error_inversion_ejercida_fin_2017"  for="inversion_ejercida_fin">PRESUPUESTO EJERCIDO AL 31/DIC/2017<strong class="tamanio_caracteres" id="tamanio_caracteres">(15 caracteres)</strong></label>
          <p><input name="inversion_ejercida_fin" maxlength="15" id="inversion_ejercida_fin" placeholder="PRESUPUESTO EJERCIDO AL 31/DIC/2017" oninput="this.className = ''"></p>

          <br>
          <br>

             <br>
          <p><label id="error_inversion_autorizada_fin_2018"  for="inversion_autorizada_pro">PRESUPUESTO AUTORIZADO ANUAL 2018 <strong class="tamanio_caracteres" id="tamanio_caracteres">(15 caracteres)</strong></label>
          <p><input name="inversion_autorizada_pro" maxlength="15" id="inversion_autorizada_pro" placeholder="PRESUPUESTO AUTORIZADO ANUAL 2018" oninput="this.className = ''"></p>

          <br>
          <p><label id="error_inversion_ejercida_fin_2018"  for="inversion_ejercida_pro">PRESUPUESTO EJERCIDO AL 30 DE JUN DE 2018<strong class="tamanio_caracteres" id="tamanio_caracteres">(15 caracteres)</strong></label>
          <p><input name="inversion_ejercida_pro" maxlength="15" id="inversion_ejercida_pro" placeholder="PRESUPUESTO EJERCIDO AL 30 DE JUN DE 2018" oninput="this.className = ''"></p>
          <br>

       </div>
      <div class="tab_avance">
        <label style="text-align: center color: #4CAF50; font-size: 17px; font-family: Graphik-Bold" >Informacion lista</label>

        <input type="hidden" name="nombre_dependencia_avance" id="nombre_dependencia_avance">
        <input type="hidden" name="oculto_avance" value="1">

        <input type="hidden" name="sector_fin" id="sector_fin">
        <input type="hidden" name="unidad_pres_fin" id="unidad_pres_fin">
        <input type="hidden" name="nivel_fin" id="nivel_fin">
        <input type="hidden" name="nivel_prop" id="nivel_prop">
        <input type="hidden" name="eje_ped" id="eje_ped">
       
        <h2><font font-family: Graphik-Bold color="green" size="6">ENVIAR INFORMACION</font></h2>
      </div>
      
      <div style="overflow:auto;">
        <div style="float:right;">
          <button type="button" id="prevBtn_ava" onclick="nextPrev_avance(-1)">Anterior</button>
          <button type="button" name="enviar_form_avance_programatico" id="nextBtn_ava" onclick="nextPrev_avance(1)">Siguiente</button>
        </div>
      </div>
      
      <div style="text-align:center;margin-top:40px;">
        <span class="step_avance"></span>
        <span class="step_avance"></span>
        <span class="step_avance"></span>
        <span class="step_avance"></span>
      </div>
  </form>
</div>


<div id="estatus_avance_prog" class="tabcontent_avance_prog">
   <br>
      <div class="container">
      <?php 

      global $wpdb;
      $evidencias = $wpdb->prefix . 'avance_programatico';
      $registros = $wpdb->get_results("SELECT * FROM $evidencias WHERE nomb_dependencia='$user_name'", ARRAY_A);
      foreach($registros as $registro) 
        { 
          $i=$registro['id_avance_programatico'];
          $nom_ind=$registro['nom_prog_presupuestario'];
          $btn_ac_des=$registro['status'];
         
          ?>

         <button class="accordion"  id="acordeon_id_<?php echo($registro['id_avance_programatico'])?>">
          <div>
            <table>
              <col width=80%>
              <col width=20%>
                <tbody>
                 <td class="" ="contenido_datos" > <?php echo $registro['nom_prog_presupuestario'];?></td> 
                 <td class="" ="contenido_datos" >

                    <?php  if($registro['status'] == 0)
                    {
                      echo("< h2style='color:#008ec5;'>En proceso</h2>");
                    }
                    else if($registro['status'] == 1)
                    {
                      echo("<h4 style='color:#0bd03e;'>Aprobado</h>");
                    } 
                     else if($registro['status'] == 2)
                    {
                      echo("<h4 style='color:#f74515;'>Corregir</h>");
                    } 
                     else if($registro['status'] == 5)
                    {
                      echo("<label style='text-align: center; font-size: 20px; font-family: Graphik-Bold ' color:#727272; '>Editar</label>");
                    } 
                      ?>                      
                  </td> 
                </tbody>
            </table>
          </div>  
         </button>

          <div class="panel">
            <div class="tab_vertical">
             
              <button class="tablinks_vertical" onclick="open_vertical_tab(event, 'Parte_nivel_fin<?php echo $registro['id_avance_programatico']; ?>')">NIVEL FIN<i  class="fa fa-check-circle-o" 
             id="check_green_evidencia_<?php echo $registro['id_avance_programatico']; ?>" style="font-size:20px;color:green;text-shadow:2px 2px 4px #000000;"></i></button>

              <button class="tablinks_vertical" onclick="open_vertical_tab(event, 'Parte_porposito<?php echo $registro['id_avance_programatico']; ?>')">NIVEL PROPOSITO<i  
             id="check_green_evidencia_<?php echo $registro['id_avance_programatico']; ?>" style="font-size:20px;color:green;text-shadow:2px 2px 4px #000000;"></i></button>

              <button  id="defaultOpen_vertical" class="tablinks_vertical" onclick="open_vertical_tab(event, 'Parte_sn_inversion<?php echo $registro['id_avance_programatico']; ?>')">AVANCE FINACIERO<i  class="fas fa-redo" id="check_green_indicador_<?php echo $registro['id_avance_programatico']; ?>" style="font-size:20px;color:green;text-shadow:2px 2px 4px #000000;"></i></button>
              
                <script>

                  var button_add_deisabled =document.getElementById("acordeon_id_<?php echo($registro['id_avance_programatico'])?>");

                        if(<?php echo($registro['status'])?>==0)
                        {
                          
                          button_add_deisabled.setAttribute("disabled", true);
                         
                        }
                        else if(<?php echo($registro['status'])?>==1)
                        {
                          
                          button_add_deisabled.setAttribute("disabled", true);
                         
                        }
                        else if(<?php echo($registro['status'])?>==2)
                        {
                           
                         
                        }
                        else if(<?php echo($registro['status'])?>==5)
                        {
                           
                         //editable
                        }


                    //parte indicador
                    if("<?php echo $registro['coment_indicador']; ?>"!="" || "<?php echo $registro['status']; ?>"==5)
                    {$('#check_green_indicador_<?php echo $registro['id_avance_programatico']; ?>').hide();}
                    else
                    {$('#check_green_indicador_<?php echo $registro['id_avance_programatico']; ?>').show();}
                   
                    //parte evidencia
                    if("<?php echo $registro['coment_evidencia']; ?>"!="" || "<?php echo $registro['status']; ?>"==5)
                    {$('#check_green_evidencia_<?php echo $registro['id_avance_programatico']; ?>').hide();}
                    else
                    {$('#check_green_evidencia_<?php echo $registro['id_avance_programatico']; ?>').show();}
                    //parte inconsistencias

                    if("<?php echo $registro['coment_inconsistencias']; ?>"!="" || "<?php echo $registro['status']; ?>"==5)
                    {$('#check_green_inconsistencias_<?php echo $registro['id_avance_programatico']; ?>').hide();}
                    else
                    {$('#check_green_inconsistencias_<?php echo $registro['id_avance_programatico']; ?>').show();}
                    //parte documentyos
                </script>
            </div>

              <form  method="post" id="signupForm_correccion" name="signupForm_correccion" > 


              <div id="Parte_sn_inversion<?php echo $registro['id_avance_programatico']; ?>" class="tabcontent_vertical">
                 <div id="global">
                    <div id="mensajes">
                      <div class="texto">
                        <div id="info_labels_<?php echo $registro['id_avance_programatico'];?>">
                         <form id="form_part_indicador" method="post">
                         
                            <table>
                            <col width=20%>
                            <col width=80%>
                              <tbody>
                                <?php global $wpdb;
                                $resultado;
                                $evidencias = $wpdb->prefix . 'avance_programatico';
                                $registros_1 = $wpdb->get_results("SELECT * FROM $evidencias where id_avance_programatico='$i'", ARRAY_A);
                                foreach($registros_1 as $registro_1) { 
                                  ?>
                                  <tr>
                                     <p><label id="error_inversion_autorizada_fin_2017_<?php echo $registro['id_avance_programatico']; ?>"  for="inversion_autorizada_fin">PRESUPUESTO AUTORIZADO (ANUAL 2017)  <strong class="tamanio_caracteres" id="tamanio_caracteres">(15 caracteres)</strong></label>
                                     <p><input name="inversion_autorizada_fin" maxlength="15" id="inversion_autorizada_fin_<?php echo $registro['id_avance_programatico']; ?>" placeholder="PRESUPUESTO AUTORIZADO (ANUAL 2017)" value="<?php echo($registro_1['inversion_autorizada_fin_2017'])?>" oninput="this.className = ''"></p>
                                     <br>
                                  </tr>
                                  <tr>
                                    <p><label id="error_inversion_ejercida_fin_2017_<?php echo $registro['id_avance_programatico']; ?>"  for="inversion_ejercida_fin">PRESUPUESTO EJERCIDO AL 31/DIC/2017 <strong class="tamanio_caracteres" id="tamanio_caracteres">(15 caracteres)</strong></label>
                                    <p><input name="inversion_ejercida_fin" maxlength="15" id="inversion_ejercida_fin_<?php echo $registro['id_avance_programatico']; ?>" placeholder="PRESUPUESTO EJERCIDO AL 31/DIC/2017" value="<?php echo($registro_1['inversion_ejercida_fin_2017'])?>" oninput="this.className = ''"></p>
                                    <br>
                                    
                                  </tr>
                                  <tr>
                                    <br>
                                    <p><label id="error_inversion_autorizada_fin_2018_<?php echo $registro['id_avance_programatico']; ?>"  for="inversion_autorizada_pro">PRESUPUESTO AUTORIZADO ANUAL 2018 <strong class="tamanio_caracteres" id="tamanio_caracteres">(15 caracteres)</strong></label>
                                    <p><input name="inversion_autorizada_pro" maxlength="15" value="<?php echo($registro_1['inversion_ejercida_fin_2018'])?>" id="inversion_autorizada_fin_2018_<?php echo $registro['id_avance_programatico']; ?>" placeholder="PRESUPUESTO AUTORIZADO ANUAL 2018" oninput="this.className = ''"></p>

                                  </tr>

                                  <tr>
                                    <br>
                                    <p><label id="error_inversion_ejercida_fin_2018_<?php echo $registro['id_avance_programatico']; ?>"  for="inversion_ejercida_pro">PRESUPUESTO EJERCIDO AL 30 DE JUN DE 2018 <strong class="tamanio_caracteres" id="tamanio_caracteres">(15 caracteres)</strong></label>
                                    <p><input name="inversion_ejercida_pro" maxlength="15" id="inversion_ejercida_fin_2018_<?php echo $registro['id_avance_programatico']; ?>" placeholder="PRESUPUESTO AUTORIZADO ANUAL 2018" value="<?php echo($registro_1['inversion_autorizada_fin_2018'])?>" oninput="this.className = ''"></p>
                                  <br>
                                  </tr>

                                    
                                  <?php } 

                                ?>
                              </tbody>
                            </table>

                            <script type="text/javascript">

      

                                  function SubmitFormData_inversion(valor_entra) 
                                  {

                                    var inversion_autorizada_fin_ = $("#inversion_autorizada_fin_"+valor_entra).val();
                                    var inversion_ejercida_fin_ = $("#inversion_ejercida_fin_"+valor_entra).val();
                                    var inversion_autorizada_fin_2018_ = $("#inversion_autorizada_fin_2018_"+valor_entra).val();
                                    var inversion_ejercida_fin_2018_ = $("#inversion_ejercida_fin_2018_"+valor_entra).val();


                                    var error_inversion_autorizada_fin_2017_ = document.getElementById("error_inversion_autorizada_fin_2017_"+valor_entra);
                                    var error_inversion_ejercida_fin_2017_ = document.getElementById("error_inversion_ejercida_fin_2017_"+valor_entra);
                                    var error_inversion_autorizada_fin_2018_ = document.getElementById("error_inversion_autorizada_fin_2018_"+valor_entra);
                                    var error_inversion_ejercida_fin_2018_ = document.getElementById("error_inversion_ejercida_fin_2018_"+valor_entra);



                                    if(inversion_autorizada_fin_.length<1 || inversion_autorizada_fin_.length>10)
                                    {
                          
                                      error_inversion_autorizada_fin_2017_.innerHTML ="Verifica tu informacion";
                                      error_inversion_autorizada_fin_2017_.classList.add("error_mensaje");

                                    }
                                    else if(inversion_ejercida_fin_.length<1 || inversion_ejercida_fin_.length>10)
                                    {
                                     
                                      error_inversion_ejercida_fin_2017_.innerHTML ="Verifica tu informacion";
                                      error_inversion_ejercida_fin_2017_.classList.add("error_mensaje");


                                    }
                                    else if(inversion_autorizada_fin_2018_.length<1 || inversion_autorizada_fin_2018_.length>10)
                                    {
                                     
                                      error_inversion_autorizada_fin_2018_.innerHTML ="Verifica tu informacion";
                                      error_inversion_autorizada_fin_2018_.classList.add("error_mensaje");


                                    }
                                    else if(inversion_ejercida_fin_2018_.length<1 || inversion_ejercida_fin_2018_.length>10)
                                    {
                                     
                                      error_inversion_ejercida_fin_2018_.innerHTML ="Verifica tu informacion";
                                      error_inversion_ejercida_fin_2018_.classList.add("error_mensaje");

                                    }
                                    else
                                    {


                                      var inversion_autorizada_fin_ = $("#inversion_autorizada_fin_"+valor_entra).val();
                                    var inversion_ejercida_fin_ = $("#inversion_ejercida_fin_"+valor_entra).val();
                                    var inversion_autorizada_fin_2018_ = $("#inversion_autorizada_fin_2018_"+valor_entra).val();
                                    var inversion_ejercida_fin_2018_ = $("#inversion_ejercida_fin_2018_"+valor_entra).val();


                                        $.ajax({
                                        type: "POST",
                                        url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                                        data: {id_indicador:valor_entra,inversion_autorizada_fin_: inversion_autorizada_fin_, inversion_ejercida_fin_:inversion_ejercida_fin_,inversion_autorizada_fin_2018_:inversion_autorizada_fin_2018_,inversion_ejercida_fin_2018_:inversion_ejercida_fin_2018_}
                                        ,
                                        success: function(msg){
                                          alert( "Informacion Actualizada" );
                                        },
                                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                                           alert( "Error Verifica tu informacion" );
                                        }
                                      });

                                    }

                                  }
                            </script>
                            
                            <input style="background-color: #555555;color: white;"  type="button" id="submit_indicador" onclick="SubmitFormData_inversion(<?php echo $registro['id_avance_programatico'];?>)" value="Actualizar Informacion" /> </input> 
                                           
                          </form>

                       
                      </div>
                    </div>
                  </div>
                  <br>
                  <br>
                </div>

              </div>

              <div id="Parte_nivel_fin<?php echo $registro['id_avance_programatico']; ?>" class="tabcontent_vertical">
                <div id="global">
                    <div id="mensajes">
                      <div class="texto">
                        <div id="info_labels_<?php echo $registro['id_avance_programatico'];?>">
                         <form id="form_part_indicador" method="post">
                         
                            <table>
                            <col width=20%>
                            <col width=80%>
                              <tbody>
                                <?php global $wpdb;
                                $resultado;
                                $evidencias = $wpdb->prefix . 'avance_programatico';
                                $registros_1 = $wpdb->get_results("SELECT * FROM $evidencias where id_avance_programatico='$i'", ARRAY_A);
                                foreach($registros_1 as $registro_1) { 
                                  ?>
                                   
                                    <h1 style="text-align: center;"><font text-align: center; font-family: Graphik-Bold  color="green" size="5">NIVEL FIN</font></h1>
                                    <tr>
                                    
                                    <br>

                                    <tr>
                                       <p><label id="error_desc_general_fin"  for="desc_general_fin">Descripcion Generall <strong class="tamanio_caracteres" id="tamanio_caracteres"></strong></label>
                                       <textarea name="desc_general_fin" disabled="true" id="desc_general_fin" placeholder="Descripcion General" oninput="this.className = ''"><?php echo($registro_1['desc_general_fin'])?></textarea>
                                        <br>

                                    </tr>
                                    <tr>
                                      <p><label id="error_indicador_fin"  for="indicador_fin">Indicador <strong class="tamanio_caracteres" id="tamanio_caracteres"></strong></label>
                                      <p><input name="indicador_fin" disabled="true" id="indicador_fin" placeholder="Indicador" value="<?php echo($registro_1['indicador_fin'])?>" oninput="this.className = ''"></p>
                                      <br>
                                      

                                    </tr>
                                    <tr>
                                      <p><label id="error_indicador_fin"  for="meta_prog_2018_fin">Meta Programada 2018 <strong class="tamanio_caracteres" id="tamanio_caracteres"></strong></label>
                                      <p><input name="meta_prog_2018_fin" id="meta_prog_2018_fin_<?php echo($registro_1['id_avance_programatico'])?>" value="<?php echo($registro_1['meta_prog_2018_fin'])?>" disabled="true" placeholder="Meta Programada 2018" oninput="this.className = ''"></p>
                                      <br>
                                      
                                    </tr>
                                    <tr>
                                      <p><label id="error_result_estim_30_jun_prop"  for="result_estim_30_jun_fin">Resultado Estimado 30 de Junio(Primer Semestre) <strong class="tamanio_caracteres" id="tamanio_caracteres">(10 caracteres)</strong></label>
                                      <p><input name="result_estim_30_jun_fin" maxlength="10" id="result_estim_30_jun_fin_<?php echo($registro_1['id_avance_programatico'])?>" placeholder="Resultado Estimado 30 de Junio(Primer Semestre)" value="<?php echo($registro_1['result_estim_30_jun_fin'])?>" oninput="this.className = ''" ></p>
          
                                      <br>
                                      <br>
                                    </tr>
                                  <tr>

                                    <br>
                                    <p><label id="error_avance_porc_meta_2018" disabled for="avance_porc_meta_2018">Avance Porcentual de la meta 2018 <strong class="tamanio_caracteres" id="tamanio_caracteres">(Actualiza el calculo)</strong></label>
                                    <p><input   name="avance_porc_meta_2018" id="avance_porc_meta_2018_<?php echo $registro['id_avance_programatico']; ?>" placeholder="Avance Porcentual de la meta 2018" onfocus="calcuar_avance_fin_edit('<?php echo $registro['id_avance_programatico']; ?>')"   value="<?php echo($registro_1['avance_fin_meta_2018'])?>" ></p>
                                    <br>
                                  </tr>

          



                                  <?php } 

                                ?>
                              </tbody>
                            </table>

                        <script type="text/javascript">


                             
      

                              function SubmitFormData_fin_edit(valor_entra) 
                              {

                                var result_estim_30_jun_fin_ = $("#result_estim_30_jun_fin_"+valor_entra).val();
                                var avance_porc_meta_2018_ = $("#avance_porc_meta_2018_"+valor_entra).val();

                                var error_result_estim_30_jun_prop = document.getElementById("result_estim_30_jun_fin_"+valor_entra);
                                
                                if(result_estim_30_jun_fin_.length>10)
                                {                   
                                  error_result_estim_30_jun_prop.innerHTML ="Verifica tu informacion";
                                  error_result_estim_30_jun_prop.classList.add("error_mensaje");
                                }
                                else
                                { 
                                    $.ajax({
                                    type: "POST",
                                    url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                                    data: {id_indicador:valor_entra,result_estim_30_jun_fin_: result_estim_30_jun_fin_, avance_porc_meta_2018_:avance_porc_meta_2018_}
                                    ,
                                    success: function(msg){
                                      alert( "Informacion Actualizada" );
                                    },
                                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                                       alert( "Error Verifica tu informacion" );
                                    }
                                  });

                                  error_result_estim_30_jun_prop.remove("error_mensaje");
                                  error_result_estim_30_jun_prop ="Nombre del indicador";

                                }

                    

                              }
                        </script>
                            
                            <input style="background-color: #555555;color: white;"  type="button" id="submit_indicador" onclick="SubmitFormData_fin_edit(<?php echo $registro['id_avance_programatico'];?>)" value="Actualizar Informacion" /> </input> 
                                           
                          </form>

                       
                      </div>
                    </div>
                  </div>
                  <br>
                  <br>
                </div>
              </div>

              <div id="Parte_porposito<?php echo $registro['id_avance_programatico']; ?>" class="tabcontent_vertical">
                  <div id="global">
                    <div id="mensajes">
                      <div class="texto">
                        <div id="info_labels_<?php echo $registro['id_avance_programatico'];?>_2">
                          <form id="form_part_indicador" method="post">
                          <table>
                            <col width=20%>
                            <col width=80%>
                            <tbody>
                              <?php global $wpdb;
                                $evidencias = $wpdb->prefix . 'avance_programatico';
                                $registros_2 = $wpdb->get_results("SELECT * FROM $evidencias where id_avance_programatico='$i'", ARRAY_A);

                                foreach($registros_2 as $registro_2) { ?>
                               <h1 style="text-align: center;"><font text-align: center; font-family: Graphik-Bold  color="green" size="5">NIVEL PROPOSITO</font></h1>
                               
                                <tr>
                                  <p><label id="error_desc_general_prop"  for="desc_general_prop">Descripcion Generall <strong class="tamanio_caracteres" id="tamanio_caracteres" value="<?php echo($registro_1['desc_general_prop'])?>"></strong></label>
                                </tr>
                                <tr>
                                  <textarea disabled="true" name="desc_general_prop" id="desc_general_prop" placeholder="Descripcion General" oninput="this.className = ''"><?php echo($registro_1['desc_general_prop'])?></textarea>
                                  <br>
                                </tr>
                                <tr>
                                  <p><label id="error_indicador_prop"  for="indicador_prop">Indicador <strong class="tamanio_caracteres" id="tamanio_caracteres"></strong></label>
                                  <p><input name="indicador_prop" id="indicador_prop" placeholder="Indicador" value="<?php echo($registro_1['indicador_prop'])?>" disabled="true" oninput="this.className = ''"></p>
                                </tr>
                                  <br>
                                <tr>
                                  <p><label id="error_meta_prog_2018_prop"  for="meta_prog_2018_prop">Meta Programada 2018 <strong class="tamanio_caracteres" id="tamanio_caracteres"></strong></label>
                                </tr>
                                <tr>
                                  <p><input name="meta_prog_2018_prop" id="meta_prog_2018_prop_<?php echo($registro_1['id_avance_programatico'])?>" value="<?php echo($registro_1['meta_prog_2018_prop'])?>" disabled="true" placeholder="Meta Programada 2018" oninput="this.className = ''"></p>
                                  <br>
                                </tr>
                                <tr>
                                  <p><label id="error_result_estim_30_jun_prop" for="result_estim_30_jun_prop">Resultado Estimado 30 de Junio(Primer Semestre) <strong class="tamanio_caracteres" id="tamanio_caracteres">(15 caracteres)</strong></label>
                                </tr>
                                <tr>
                                  <p><input name="result_estim_30_jun_prop" maxlength="15" id="result_estim_30_jun_prop_<?php echo($registro_1['id_avance_programatico'])?>" placeholder="Resultado Estimado 30 de Junio(Primer Semestre)" value="<?php echo($registro_1['result_estim_30_jun_prop'])?>" oninput="this.className = ''"></p>
                                  <br>
                                </tr>
                                  <br>
                                  <tr>
                                  <p><label id="error_avance_porc_meta_2018" disabled for="avance_porc_meta_2018">Avance Porcentual de la meta 2018 <strong class="tamanio_caracteres" id="tamanio_caracteres">(Actualiza el calculo)</strong></label>
                                  <p><input name="avance_porc_meta_2018" id="avance_porc_meta_2018_prop_<?php echo($registro_1['id_avance_programatico'])?>" placeholder="Avance Porcentual de la meta 2018"  onfocus="calcuar_avance_edit_prop('<?php echo $registro['id_avance_programatico']; ?>')"  value="<?php echo($registro_1['avance_porc_meta_2018'])?>"></p>
                                  <br>
                                </tr>
                                                      

                                  <?php } 

                                ?>
                                  </tbody>
                                </table>

                          <script type="text/javascript">
                            //ocultar el mensaje datos cargados

                            function SubmitFormData_prop_edit(valor_entra) 
                            {

                              var result_estim_30_jun_prop_=$("#result_estim_30_jun_prop_"+valor_entra).val();
                              var avance_porc_meta_2018_prop_=$("#avance_porc_meta_2018_prop_"+valor_entra).val();

                              var error_result_estim_30= document.getElementById("result_estim_30_jun_prop_"+valor_entra); /*Se regresa a esat caja por los nombres de las cajas semejantes: error_result_estim_30_jun_pro*/ 
                              
                              if(result_estim_30_jun_prop_.length>10)
                              {                   
                                error_result_estim_30.innerHTML ="No mas de 10 Caracteres";
                                error_result_estim_30.classList.add("error_mensaje");
                              }
                              else
                              {
                                  $.ajax({
                                    type: "POST",
                                    url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                                    data: {id_indicador:valor_entra,result_estim_30_jun_prop_:result_estim_30_jun_prop_,avance_porc_meta_2018_prop_:avance_porc_meta_2018_prop_}
                                    ,
                                    success: function(msg){
                                      if(msg=1)
                                      {
                                        alert("Informacion Actualizada");
                                      }
                                      
                                    },
                                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                                       alert( "Error Verifica tu informacion" );
                                    }
                                  });

                                  error_result_estim_30.classList.remove("error_mensaje");
                                  error_result_estim_30.innerHTML ="Nombre del indicador";
                              }


                            }
                          </script>

                                              
                          <input style="background-color: #555555;color: white;" type="button" id="submit_indicador" onclick="SubmitFormData_prop_edit(<?php echo $registro['id_avance_programatico'];?>)" value="Actualizar Informacion" />  
                                        
                          </form>



                        <div><!--comentarios de correccion del evaluador-->
                          <!--
                              <label>"<?php echo($registro_1['coment_evidencia']) ?>"</label>
                            -->
                        </div>
                      </div>
                     </div>
                    </div>
                  </div>
              </div>

        </form>
      </div>
      <?php  
      } ?>                    

      </div>
</div>

<div id="reporte_avance_prog" class="tabcontent_avance_prog">
  <br>
    <h3>AVANCE PROGRAMATICO</h3>
      
      <table>
        <col width=5%>
        <col width=65%>
        <col width=30%>
        <br>
        <br>
          <tbody>
              <?php global $wpdb;
                $ind_estrategicos = $wpdb->prefix .'avance_programatico';
                $registros_estrategicos_1 = $wpdb->get_results("SELECT * FROM $ind_estrategicos WHERE nomb_dependencia ='$user_name'", ARRAY_A);
                foreach($registros_estrategicos_1 as $registro_estrategicos_1) 
                  { ?>
                    <?php $id_evidencia=$registro_estrategicos_1['id_avance_programatico']; 
                     $tipo="avance_prog";
                    ?>
                   
                      <tr>
                         <td>
                        
                        <img style="text-align: center" src="http://192.168.64.2/repositorio/wp-content/uploads/2018/04/PDF.png" height="42" width="30" ></img>
                      </td>
                         <td id="txt_list_pdf_estr"> <?php    echo("<p class='demo'><a href='pdf_plugin/pdf_evidencias/crear_evidencia_pdf.php?id_evidencia= $id_evidencia&arr_ids_tacticos=0&user=$user_name&tipo=$tipo' target='_blank' class='demo'>".$registro_estrategicos_1['nom_prog_presupuestario']."</a></p>");?></td> 

                         <td id="contenido_datos" >   
                         
                          <?php echo(  $registro_estrategicos_1['fecha_registro']);?>
                      
                       </td>
                  <?php } ?> 
          </tbody>
        
      </table> 

</div>




<script type="text/javascript">

 // document.getElementById("defaultOpen_avance_prog").click();

  var i, tabcontent_avance_prog, tablinks_avance_prog;

    // Get all elements with class="tabcontent_avance_prog" and hide them
    tabcontent_avance_prog = document.getElementsByClassName("tabcontent_avance_prog");
    for (i = 0; i < tabcontent_avance_prog.length; i++) {
        tabcontent_avance_prog[i].style.display = "none";
    }
  
  function open_avance_progra(evt, cityName) {

    $('#intro_idicador_avance_prog').hide();
    // Declare all variables
    var i, tabcontent_avance_prog, tablinks_avance_prog;

    // Get all elements with class="tabcontent_avance_prog" and hide them
    tabcontent_avance_prog = document.getElementsByClassName("tabcontent_avance_prog");
    for (i = 0; i < tabcontent_avance_prog.length; i++) {
        tabcontent_avance_prog[i].style.display = "none";
    }

    // Get all elements with class="tablinks_avance_prog" and remove the class "active"
    tablinks_avance_prog = document.getElementsByClassName("tablinks_avance_prog");
    for (i = 0; i < tablinks_avance_prog.length; i++) {
        tablinks_avance_prog[i].className = tablinks_avance_prog[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>



<script type="text/javascript">
  

  var currentTab_avance = 0; // Current tab_avance is set to be the first tab_avance (0)
  showTab_avance(currentTab_avance); // Display the current tab_avance

  function showTab_avance(n) {
    // This function will display the specified tab_avance of the form ...
    var x = document.getElementsByClassName("tab_avance");
    x[n].style.display = "block";
    // ... and fix the Anterior/Next buttons:
    if (n == 0) {
      document.getElementById("prevBtn_ava").style.display = "none";
    } else {
      document.getElementById("prevBtn_ava").style.display = "inline";
    }
    if (n == (x.length - 1)) {
      document.getElementById("nextBtn_ava").innerHTML = "Enviar";
    } else {
      document.getElementById("nextBtn_ava").innerHTML = "Siguiente";
    }
    // ... and run a function that displays the correct step_avance indicator:
    fixStep_avanceIndicator(n)
  }

  function nextPrev_avance(n) {
    // This function will figure out which tab_avance to display
    var x = document.getElementsByClassName("tab_avance");
    // Exit the function if any field in the current tab_avance is invalid_avance:
    if (n == 1 && !validateForm_avance()) return false;
    // Hide the current tab_avance:
    x[currentTab_avance].style.display = "none";
    // Increase or decrease the current tab_avance by 1:
    currentTab_avance = currentTab_avance + n;
    // if you have reached the end of the form... :
    if (currentTab_avance >= x.length) {
      //cambiar el submit del boton next
      //quitar disabled a los inputs
      document.getElementById('avance_fin_meta_2018').disabled = false;
      document.getElementById('avance_porc_meta_2018').disabled = false;

      



       var x = document.getElementById("nextBtn_ava");
       x.setAttribute("type", "submit");
      //...the form gets submitted:
      //document.getElementById("regForm").submit();
      //return false;
    }
    // Otherwise, display the correct tab_avance:
    showTab_avance(currentTab_avance);
  }

  function validateForm_avance() {
    // This function deals with validation of the form fields
    var x, y, i, valid = true;
    
    x = document.getElementsByClassName("tab_avance");
    y = x[currentTab_avance].getElementsByTagName("input");
    // A loop that checks every input field in the current tab_avance:
    for (i = 0; i < y.length; i++) {
      // If a field is empty...
      if (y[i].value == "") {
        // add an "invalid_avance" class to the field:
        y[i].className += " invalid_avance";
        // and set the current valid status to false:
        valid = false;
      }
    }
    // If the valid status is true, mark the step_avance as finished and valid:
    if (valid) {
      document.getElementsByClassName("step_avance")[currentTab_avance].className += " finish";
    }
    return valid; // return the valid status
  }

  function fixStep_avanceIndicator(n) {
    // This function removes the "active" class of all step_avances...
    var i, x = document.getElementsByClassName("step_avance");
    for (i = 0; i < x.length; i++) {
      x[i].className = x[i].className.replace(" active", "");
    }
    //... and adds the "active" class to the current step_avance:
    x[n].className += " active";
  }
</script>

<script type="text/javascript">//autocomplete,

$(document).ready(function() 
  {
    //activar modulos
     $.ajax({
         type: "GET",
         datatype: 'json',
         contentType: "application/json; charset=utf-8",
        
         url: "/repositorio/wp-content/themes/EspecialesT2.1/templates/ctrl_modulos.php?nom_dep="+'<?php echo($user_name);?>',
         success: function(respuesta)
          {
            
            var arr_mod = respuesta.slice(1, -1);
            var arr_mod = arr_mod.split(",");

            var i;
            for (i = 0; i < arr_mod.length; i++) { 
              if(arr_mod[i]=="0")
              {
                 $("#btn_avance_reg").removeAttr('disabled');
                 
               
              }
              else if(arr_mod[i]=="1")
              {
                 $("#btn_avance_edit").removeAttr('disabled');
                 
              }

              else if(arr_mod[i]=="2")
              {
                 $("#btn_avance_repdf").removeAttr('disabled');
                
              }


            
              //console.log(arr_mod[i]);
            }
            
           
          },
          error: function(data)
          {

          }
         });

      });






$('#autollenado_avance').hide();
  //BUSQUEDA INDICADOR
  $( "#nom_prog_presupuestario" ).autocomplete({
    //source: "/repositorio/wp-content/themes/EspecialesT2.1/templates/searh_indicador.php?id=<?php echo($user_name); ?>",
    source: "/repositorio/wp-content/themes/EspecialesT2.1/templates/searh_indicador.php?id=<?php echo($usuario);?>&tipo=avance",
    minLength: 1,

     response: function(event, ui)        //TYERMINO DE REGISTRAR SUS INDICADORES
     {

     }

  });
  //INDICADOR SELECCIONADO
  
   $( "#nom_prog_presupuestario" ).autocomplete({

        close: function( event, ui ) 
        {

        var nom_indicador = document.getElementById("nom_prog_presupuestario");
        var nom_indi_selec=nom_indicador.value;
        var tipo="avance";
       // alert(nom_indi_selec);
        $.ajax({

             type: "GET",
             datatype: 'json',
             contentType: "application/json; charset=utf-8",
            
             url: "/repositorio/wp-content/themes/EspecialesT2.1/templates/indicador_seleccionado.php?nom_indi_selec="+nom_indi_selec+"&tipo="+tipo ,
             success: function(responce)
              {
                console.log(responce);


                var desc_general_fin = document.getElementById("desc_general_fin");
                desc_general_fin.value=responce.descripcion_f;

                var indicador_fin = document.getElementById("indicador_fin");
                indicador_fin.value=responce.indicador_f;

                var meta_prog_2018_fin = document.getElementById("meta_prog_2018_fin");
                meta_prog_2018_fin.value=responce.meta_p2018_f;

                var desc_general_prop = document.getElementById("desc_general_prop");
                desc_general_prop.value=responce.descripcion_p;

                var indicador_prop = document.getElementById("indicador_prop");
                indicador_prop.value=responce.indicador_p;

                var meta_p2018_p = document.getElementById("meta_prog_2018_prop");
                meta_p2018_p.value=responce.meta_p2018_p;


                var sector_fin = document.getElementById("sector_fin");
                sector_fin.value=responce.sector_f;
                var unidad_pres_fin = document.getElementById("unidad_pres_fin");
                unidad_pres_fin.value=responce.unidad_pres_f;
                var nivel_fin = document.getElementById("nivel_fin");
                nivel_fin.value=responce.nivel_f;
                var nivel_prop = document.getElementById("nivel_prop");
                nivel_prop.value=responce.nivel_p;
                var eje_ped = document.getElementById("eje_ped");
                eje_ped.value=responce.eje_ped;



                //parte 2


              },
              error: function(data)
              {


              }
      });

       $('#autollenado_avance').show();//RENDERIZAR EL RESTO DEL FORMULARIO
      }
    }); 

/*
      function calcuar_avance_fin_edit(valor_id)
      {
       console.log(valor_id);
       // alert("hcer el calcilo");
        var result_estim_30_jun_fin_edit = document.getElementById("result_estim_30_jun_fin_"+valor_id);

        var meta_prog_2018_fin_edit = document.getElementById("meta_prog_2018_fin_"+valor_id);

        var res_val_edit=parseFloat(result_estim_30_jun_fin_edit.value);
        var meta_val_edit=parseFloat(meta_prog_2018_fin_edit.value);


        var avance_fin_meta_2018 = document.getElementById("avance_porc_meta_2018_"+valor_id);
              avance_fin_meta_2018.value=(res_val_edit/meta_val_edit * 100).toFixed(2);
        //alert(avance_fin_meta_2018.value);
       
      }
      */

      function calcuar_avance_fin_edit(valor_id)
      {
       console.log(valor_id);
       // alert("hcer el calcilo");
        var result_estim_30_jun_fin_edit = document.getElementById("result_estim_30_jun_fin_"+valor_id);

        var meta_prog_2018_fin_edit = document.getElementById("meta_prog_2018_fin_"+valor_id);

        var res_val_edit=parseFloat(result_estim_30_jun_fin_edit.value);
        var meta_val_edit=parseFloat(meta_prog_2018_fin_edit.value);


        var avance_fin_meta_2018 = document.getElementById("avance_porc_meta_2018_"+valor_id);
              avance_fin_meta_2018.value=(res_val_edit/meta_val_edit * 100).toFixed(2);
        //alert(avance_fin_meta_2018.value);
       
      }





      function calcuar_avance_edit_prop(valor_id)
      {
        console.log(valor_id);
       // var valor_id=12;
       
        var result_estim_30_jun_prop_ = document.getElementById("result_estim_30_jun_prop_"+valor_id);

        var meta_prog_2018_prop_ = document.getElementById("meta_prog_2018_prop_"+valor_id);

        var res_val_edit=parseFloat(result_estim_30_jun_prop_.value);
        var meta_val_edit=parseFloat(meta_prog_2018_prop_.value);


        var avance_fin_meta_2018 = document.getElementById("avance_porc_meta_2018_prop_"+valor_id);
              avance_fin_meta_2018.value=(res_val_edit/meta_val_edit * 100).toFixed(2);
        //alert(avance_fin_meta_2018.value);
       
      }



  


</script>