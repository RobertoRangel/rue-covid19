<style type="text/css">

  #regForm {
    background-color: #ffffff;
    margin: 100px auto;
    padding: 40px;
    width: 70%;
    min-width: 300px;
  }

  /* Style the input fields */
  input {
    padding: 10px;
    width: 100%;
    font-size: 17px;
    font-family: Raleway;
    border: 1px solid #aaaaaa;
  }

  /* Mark input boxes that gets an error on validation: */
  input.invalid {
    background-color: #ffdddd;
  }

  /* Hide all steps by default: */
  .tab_pid {
    display: none;
  }

  /* Make circles that indicate the steps of the form: */
  .step_pids {
    height: 15px;
    width: 15px;
    margin: 0 2px;
    background-color: #bbbbbb;
    border: none; 
    border-radius: 50%;
    display: inline-block;
    opacity: 0.5;
  }

  /* Mark the active step_pids: */
  .step_pids.active {
    opacity: 1;
  }

  /* Mark the step_pidss that are finished and valid: */
  .step_pids.finish {
    background-color: #4CAF50;
  }


  /*TABS*/

  /* Style the tab */
.tab_tac_pids {
       overflow: hidden;
    border: 1px solid #ccc;
    background-color: #
}

/* Style the buttons that are used to open the tab_tac_pids content */
.tab_tac_pids button {
 background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
}

/* Change background color of buttons on hover */
.tab_tac_pids button:hover {
     background-color: #ffffff;
}

/* Create an active/current tab_tac_pidslink class */
.tab_tac_pids button.active {
   background-color: #1b2935;
  
  color: #ffffff;
}

/* Style the tab content */
.tabcontent_tac_pid {
    display: none;
  padding: 6px 12px;
  border: 3px solid #ccc;
  /*border-top: none;*/
  background-color: #ffffff;
}
/*TABS VERTICAL*/

/* Style the buttons that are used to open and close the accordion panel */
.accordion {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    text-align: left;
    border: none;
    outline: none;
    transition: 0.4s;
}

/* Add a background color to the button if it is clicked on (add the .active class with JS), and when you move the mouse over it (hover) */
.active, .accordion:hover {
    background-color: #ccc;
}

/* Style the accordion panel. Note: hidden by default */
.panel {
    padding: 0 18px;
    background-color: white;
    display: none;
    overflow: hidden;
}
</style>


<?php global $display_name , $user_name;
      get_currentuserinfo();

     // echo $display_name . "'s email address is: " . $user_name;
      $usuario=$user_name;
?>


<!--  Mensajes emergentes-->
    <div id="dialog-form_pid" title="Municipios">
      <p class="validateTips">Selecciona los Municipios</p>
      <form  id="form_cheks_municipios">
        <fieldset>
          <p id="check_list_municipios_pid"></p>

          <script>
            var municipios_arr = ["Acatlán",
               "Acaxochitlán",                //1
               "Actopan",                     //2
               "Agua Blanca de Iturbide",     //3
               "Ajacuba",                     //4
               "Alfajayucan",                 //5
               "Almoloya",                    //6
               "Apan",                        //7
               "Atitalaquia",                 //8
               "Atlapexco",                   //9
               "Atotonilco de Tula",          //10
               "Atotonilco el Grande",        //11
               "Calnali",                     //12
               "Cardonal",                    //13
               "Chapantongo",                 //14 
               "Chapulhuacán",                //15
               "Chilcuautla",                 //16
               "Cuautepec de Hinojosa",       //17
               "El Arenal",                   //18
               "Eloxochitlán",                //19
               "Emiliano Zapata",             //20
               "Epazoyucan",                  //21
               "Francisco I. Madero",         //22
               "Huasca de Ocampo",            //23
               "Huautla",                     //24
               "Huazalingo",                  //25
               "Huehuetla",                   //26
               "Huejutla de Reyes",           //27
               "Huichapan",                   //28
               "Ixmiquilpan",                 //29
               "Jacala de Ledezma",           //30
               "Jaltocán",
               "Juárez Hidalgo",
               "La Misión",
               "Lolotla",
               "Metepec",
               "Metztitlán",
               "Mineral de la Reforma",
               "Mineral del Chico",
               "Mineral del Monte",
               "Mixquiahuala de Juárez",
               "Molango de Escamilla",
               "Nicolás Flores",
               "Nopala de Villagrán",
               "Omitlán de Juárez",
               "Pachuca de Soto",
               "Pacula",
               "Pisaflores",
               "Progreso de Obregón",
               "San Agustín Metzquititlán",
               "San Agustín Tlaxiaca",
               "San Bartolo Tutotepec",
               "San Felipe Orizatlán",
               "San Salvador",
               "Santiago de Anaya",
               "Santiago Tulantepec de Lugo Guerre",
               "Singuilucan",
               "Tasquillo",
               "Tecozautla",
               "Tenango de Doria",
               "Tepeapulco",
               "Tepehuacán de Guerrero",
               "Tepeji del Río de Ocampo",
               "Tepetitlán",
               "Tetepango",
               "Tezontepec de Aldama",
               "Tianguistengo",
               "Tizayuca",
               "Tlahuelilpan" ,
               "Tlahuiltepa",
               "Tlanalapa",
               "Tlanchinol",
               "Tlaxcoapan",
               "Tolcayuca",
               "Tula de Allende",
               "Tulancingo de Bravo",
               "Villa de Tezontepec",
               "Xochiatipan",
               "Xochicoatlán",
               "Yahualica",
               "Zacualtipán de angeles",
               "Zapotlán de Juárez",
               "Zempoala",
               "Zimapán"];
            var text = "";
            var i;
            text+="<ul>"
            for (i = 0; i < municipios_arr.length; i++) 
            {
               text +='<div>'+  '<label class="checkbox-inline "> '+'<input type="checkbox"   class="check_muni" id="check_evi_pid_'+municipios_arr[i]+'"  name="'+municipios_arr[i]+'">'+municipios_arr[i]+'</label>'   +'</div>';


            }

            text+="</ul>"
            document.getElementById("check_list_municipios_pid").innerHTML = text;
         
          </script>
          <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
        </fieldset>
      </form>
    </div>
 <!-- Mensajes Entidades -->
    <div id="dialog-form-entidades_pid" title="Entidades">
      <p class="validateTips">Selecciona las Entidades</p>
      <form  id="form_cheks_entidades">
        <fieldset>
          <p id="check_list_entidades_pid"></p>

          <script>

            var my_data = $("#dialog-form-entidades_pid" ).data('indi');
           // alert(my_data);

            var entidades_arr = ["Aguascalientes",
                "Baja California",
                "Baja California Sur",
                "Campeche",
                "Chiapas",
                "Chihuahua",
                "Ciudad de México",
                "Coahuila",
                "Colima",
                "Durango",
                "Estado de México",
                "Guanajuato",
                "Guerrero",
                "Hidalgo",
                "Jalisco",
                "Michoacán",
                "Morelos",
                "Nayarit",
                "Nuevo León",
                "Oaxaca",
                "Puebla",
                "Querétaro",
                "Quintana Roo",
                "San Luis Potosí",
                "Sinaloa",
                "Sonora",
                "Tabasco",
                "Tamaulipas",
                "Tlaxcala",
                "Veracruz",
                "Yucatán",
                "Zacatecas"];
            var text = "";
            var i;
            text+="<ul>"
            for (i = 0; i < entidades_arr.length; i++) 
            {
               text +='<label class="checkbox-inline ">'+'<input type="checkbox"   class="check_enti" id="check_evi_pid_'+entidades_arr[i]+'"  name="'+entidades_arr[i]+'">'+entidades_arr[i]+'</label>';
            }
            text+="</ul>"
            document.getElementById("check_list_entidades_pid").innerHTML = text;
            

          </script>
          <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
        </fieldset>
      </form>
    </div>

    <div id="dialog-message-lugar_pid" title="Lugar donde se realizarion la acciones">
      <div class="btn-group">
        <button  type="button" onclick="open_estados_pid()" class="btn btn-primary">Estatal</button>
        <button type="button" onclick="open_municipios_pid()" class="btn btn-primary">Municipales</button>
        <button type="button" onclick="open_localidades_pid()" class="btn btn-primary">Localidades</button>
      </div>
                          
    </div>

    <div id="dialog-message-adjuntar-localidades_pid" title="Adjuntar archivo de localidades">
      <p>
        <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
       <b>Especifiquen los lugares donde se realizaron las acciones PID</b>
      </p>
      
      <div id="doc_res1_pid">
          <div id="upload_doc_resultados_1_pid" name="upload_doc_resultados_1_pid">  </div>
      </div>

      <div id="doc_res2_pid">
          <div id="upload_doc_resultados_2_pid" name="upload_doc_resultados_2_pid">  </div>
      </div>

      <div id="doc_res3_pid">
          <div id="upload_doc_resultados_3_pid" name="upload_doc_resultados_3_pid">  </div>
      </div>

      <div id="doc_res4_pid">
          <div id="upload_doc_resultados_4_pid" name="upload_doc_resultados_4_pid">  </div>
      </div>

      <div id="doc_res5_pid">
          <div id="upload_doc_resultados_5_pid" name="upload_doc_resultados_5_pid">  </div>
      </div>

      <p><label >Descargar el modelo de como organizar la informacion</label></p>
      <a href="/repositorio/upload_files_plugin/download_file_demo.php">Descargar</a>

    </div>


<div class="tab_tac_pids">
  <button id="btn_acc_tac_pid_reg" class="tablinks_tac_pids"  onclick="open_accion_tac_pid(event, 'nuevo_pid')">NUEVO INDICADOR</button>
  <button id="btn_acc_tac_pid_edit" class="tablinks_tac_pids"  onclick="open_accion_tac_pid(event, 'Estatus_tac_pid')">ESTATUS</button>
  <button id="btn_acc_tac_pid_repdf" class="tablinks_tac_pids"  onclick="open_accion_tac_pid(event, 'reporte_pids')">REPORTES</button>
</div>

<div id="intro_idicador_pids_tac_pids" class="container" >
  <p  ><font text-align: center; font-family: Graphik-Bold  color="green" size="5">Indicador táctico del Programa Institucional de Desarrollo</font></p>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>

</div>




<div id="nuevo_pid" class="tabcontent_tac_pid">
  <form  method="post" id="Form_definir_evidencia" name="Form_definir_evidencia" >
                                     
    <br>
    <p><font text-align: center; font-family: Graphik-Bold  color="green" size="5">Indicador táctico del Programa Institucional de Desarrollo</font></p>

     <div style="text-align: left;" class="tab_pid">ANTES DE COMENZAR:
      <br>
      <br>
      <br>
        <li>Favor de llenar todos los campos sin abreviaturas</li>
        <li>Usar letras mayúsculas y minúsculas</li>
        <li>Se recomienda tener  a la mano la ficha del indicador, informe de gobierno y evidencias</li>
      <br>
      <br>
     </div>


      <div class="tab_pid">
        <br><br>

          <p><label  id="error_nombre_indicador_pid"  for="nombre_indicador_pid">Nombre del indicador asociado:</label>
          <input id="nombre_indicador_pid" name="nombre_indicador_pid"  title="type &quot;a&quot;"  placeholder="Nombre del indicador asociado: ">
          </p>

          <div id="indicadore_seleccionado_autollenado_tacticos_pids">

              <!--hiden-->
              <p>
              <input id="nom_dependencia_pid" name="nom_dependencia_pid"  title="type &quot;a&quot;" style="display: none;">
              </p>

              <p>
              <input id="nom_organismo_pid" name="nom_organismo_pid"  title="type &quot;a&quot;"  style="display: none;" >
              </p>

              <p>
              <input id="periodicidad_pid" name="periodicidad_pid"  title="type &quot;a&quot;"  style="display: none;" >
              </p>

              <p>
              <input id="indica_informe" name="indica_informe"  title="type &quot;a&quot;"  style="display: none;" >
              </p>

              <p>
              <input id="eje_ped_pid" name="eje_ped_pid"  title="type &quot;a&quot;"  style="display: none;" >
              </p>

              <p>
              <input id="obj_estr_pid" name="obj_estr_pid"  title="type &quot;a&quot;" style="display: none;"  >
              </p>

              <p>
              <input id="obj_gen_pid" name="obj_gen_pid"  title="type &quot;a&quot;" style="display: none;"  >
              </p>

               <p>
              <input id="avance_inf_2017" name="avance_inf_2017"  title="type &quot;a&quot;" style="display: none;"  >
              </p>
              <!--hiden-->

              <br>
              <p><h2><label style="color:blue; font-size: 18px; ">En el siguiente apartado se registra la información que se mostrara en el “2do informe de gobierno.
                    Por lo cual es necesario revisar la información y el discurso que se provee
              </label></h2></p>

                <!--
              <br>
              <p><label  id="error_linea_discursiva_pid" for="linea_discursiva_pid">Linea discursiva:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
              <textarea id="linea_discursiva_pid" maxlength="350" name="linea_discursiva_pid" placeholder="Linea discursiva:"></textarea>
              </p>
                -->

              <br>

              <p><label  id="error_descrp_tac_pid" for="descrp_tac_pid">Descripcion del indicador:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
              <textarea id="descrp_tac_pid" maxlength="500" name="descrp_tac_pid" placeholder="Descripcion:"></textarea>
              </p>
              <!--
             <label for="clase_indicador" >Categoria del indicador</label>
              <div class="select">
                <select name="clase_indicador" id="clase_indicador">
                  <option value="Indicador Estrella">Indicador Estrella</option>
                  <option value="Indicador tactico convencional">Indicador tactico convencional</option>
                  <option value="indicador publico(informe-tablero)">indicador publico(informe-tablero)</option>
                </select>
              </div>
             -->

              <br>

                <br>


              <br>
              <p><label <>LOGROS</label></p>
            
              <p><label for="logros_en_indicador_pid" >¿Hubo logros en este indicador:?</label></p>
                <div class="select">
                  <select name="logros_en_indicador_pid" id="logros_en_indicador_pid">
                    <option value="No">No</option>
                    <option value="Si">Si</option>
                  </select>
                </div>
                <br>
              <div id="bullet_Del_logro_pid">
                <p><label id="error_id_bullet_Del_logro_pid"  for="id_bullet_Del_logro_pid">Escribir el bullet del logro:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                <textarea id="id_bullet_Del_logro_pid" name="id_bullet_Del_logro_pid" placeholder="Escribir el bullet del logro:"   ></textarea>
              </p>
              </div>
              <br>

                <p><label for="premios_recono_pid" >¿Hubo premios o reconocimientos?</label></p>
                <div class="select">
                  <select name="premios_recono_pid" id="premios_recono_pid">
                    <option value="No">No</option>
                    <option value="Si">Si</option>
                  </select>
                </div>
                <br>
              <div id="premios_reconocimientos_pid">
                <p><label id="error_reconocimiento_pid"  for="reconocimiento_pid">Describelo:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                <textarea id="reconocimiento_pid" name="reconocimiento_pid" placeholder="Escribir el bullet del premio o reconocimiento"   ></textarea>
              </p>
              </div>

            

          </div>
      </div>
      
      <div style="text-align: left;" class="tab_pid">
              <br><br>
            
              <p><label id="error_resultado_1_pid"  for="resultado_1_pid">Obras y acciones 1:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
              <textarea  class="textbox_corto" maxlength="350" id="resultado_1_pid" name="resultado_1_pid" placeholder="Obras y acciones 1:"></textarea>
              <button  id="btn_def_lugar_1"  onclick="open_lugares_pid(1,0)" type="button" >Definir lugar 1</button></p>
              <label id="datos_cargados_lug_1_pid" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
              <input id="res1_lugares_pid" name="res1_lugares_pid" hidden="true" >

              <p><label id="error_resultado_2_pid" id="label_resultado_2_pid"  for="resultado_2_pid">Obras y acciones 2:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
              <textarea class="textbox_corto" maxlength="350" id="resultado_2_pid" name="resultado_2_pid" placeholder="Obras y acciones 2:"></textarea>
              <button  id="btn_def_lugar_2" class="btn_def_lugar" onclick="open_lugares_pid(2,0)" type="button" >Definir lugar 2</button></p>
              <label id="datos_cargados_lug_2_pid" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
              <input id="res2_lugares_pid" name="res2_lugares_pid" hidden="true" >

              <p><label id="error_resultado_3_pid"  for="resultado_3_pid">Obras y acciones 3:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
              <textarea class="textbox_corto" maxlength="350" id="resultado_3_pid" name="resultado_3_pid" placeholder="Obras y acciones 3:"></textarea>
              <button class="btn_def_lugar" id="btn_def_lugar_3" onclick="open_lugares_pid(3,0)" type="button" >Definir lugar 3</button></p>
              <label id="datos_cargados_lug_3_pid" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
              <input id="res3_lugares_pid" name="res3_lugares_pid" hidden="true" >

              <p><label id="error_resultado_4_pid" id="label_resultado_4_pid"  for="resultado_4_pid">Obras y acciones 4:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
              <textarea class="textbox_corto" maxlength="350" id="resultado_4_pid" name="resultado_4_pid" placeholder="Obras y acciones 4:"></textarea>
              <button id="btn_def_lugar_4" class="btn_def_lugar" onclick="open_lugares_pid(4,0)"  type="button" >Definir lugar 4</button></p>
              <label id="datos_cargados_lug_4_pid" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
              <input id="res4_lugares_pid" name="res4_lugares_pid"  hidden="true" >

              <p><label id="error_resultado_5_pid"  for="resultado_5_pid">Obras y acciones 5:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
              <textarea class="textbox_corto" maxlength="350" id="resultado_5_pid" name="resultado_5_pid" placeholder="Obras y acciones 5:"></textarea>
              <button id="btn_def_lugar_5" class="btn_def_lugar" onclick="open_lugares_pid(5,0)"  type="button" >Definir lugar 5</button></p>
              <label id="datos_cargados_lug_5_pid" class="datos_cargados_lugar_resultados" >Datos Cargados</label>

              <input id="res5_lugares_pid" name="res5_lugares_pid" hidden="true" >


              <br><br>
              <p><label <>LO QUE SIGUE...</label></p>
              
              <p><label  id="error_lo_que_sigue_1" for="lo_que_sigue_1">1.- <strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
              <textarea id="lo_que_sigue_1" maxlength="450" name="lo_que_sigue_1" placeholder=""></textarea>

              <p><label  id="error_lo_que_sigue_2"  for="lo_que_sigue_2">2.- <strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
              <textarea id="lo_que_sigue_2" maxlength="450" name="lo_que_sigue_2" placeholder=""></textarea>

              <p><label  id="error_lo_que_sigue_3"  for="lo_que_sigue_3">3.- <strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong> </label>
              <textarea id="lo_que_sigue_3" maxlength="450" name="lo_que_sigue_3" placeholder=""></textarea>
      </div>

     <div style="text-align: left;" class="tab_pid">
       <div id="indicadore_seleccionado_autollenado">
        <br>
      

              <br>
              <p><label id="error_unidad_observacion_ind_pid" for="unidad_observacion_ind_pid">¿Cuál es la unidad de observación del indicador? (Ej. acciones, población, etc.):<strong class="tamanio_caracteres" id="tamanio_caracteres">Valor registrado en ficha tecnica(no editable)</strong></label> 

              <input disabled="true" id="unidad_observacion_ind_pid" name="unidad_observacion_ind_pid" placeholder="¿Cuál es la unidad de observación del indicador? (Ej. acciones, población, etc.)"   ></p>

              <br>
              <p><label id="error_descripcion_observacion_ficha_tecnica_pid" for="descripcion_observacion_ficha_tecnica_pid">Da una breve descripción  de la unidad de la observación (Consultar la ficha técnica)<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label><textarea id="descripcion_observacion_ficha_tecnica_pid" maxlength="450" name="descripcion_observacion_ficha_tecnica_pid" placeholder=" Da una breve descripción de la unidad de la observación Consultar la ficha técnica" ></textarea></p>  

              <br>

             
                <label for="tipo_indicador_def_pid" >¿Qué tipo de indicador es? <strong class="tamanio_caracteres" id="tamanio_caracteres">Valor registrado en ficha tecnica(no editable)</strong></label>
                <div class="select">
                  <select disabled="true" name="tipo_indicador_def_pid" id="tipo_indicador_def_pid">
                    <option value="Número">Número</option>
                    <option value="Porcentaje">Porcentaje </option>
                    <option value="Tasa">Tasa</option>
                    <option value="Absolutos">Absolutos</option>
                    <option value="Hectareas">Hectareas</option>
                    <option value="Promedio">Promedio</option>
                    <option value="Cobertura">Cobertura</option>
                    <option value="Otro">Otro</option>
                    
                  </select>

                </div>
           

              <br>
              <div id="otro_tipo_indicador_pid">
                <p><label for="otro_tipo_ind_pid">¿Cual?:</label> <input  name="otro_tipo_ind_pid" id="otro_tipo_ind_pid" placeholder=""   ></p>
              </div>
              <br>

              <br>
              
                <label for="tendencia_esp_pid">¿Qué tendencia se espera? <strong class="tamanio_caracteres" id="tamanio_caracteres">Valor registrado en ficha tecnica(no editable)</strong></label>
                <div class="select">
                  <select disabled="true" name="tendencia_esp_pid" id="tendencia_esp_pid">
                    <option id="tend_acs" value="Ascendente">Ascendente</option>
                    <option id="tend_des" value="Descendente">Descendente</option>
                    <option id="tend_cons" value="Constante">Constante</option>
                    <option id="tend_var" value="Variable">Variable</option>
                  </select>
                </div>
            
             
              <br>
             
              <p><label id="error_linea_base_pid" for="linea_base_absolutos_pid">Indicar la línea base en absolutos (números absolutos)</label>
              <input id="linea_base_absolutos_pid" minlength="0" maxlength="15" name="linea_base_absolutos_pid" placeholder="Indicar la línea base en absolutos (números absolutos)"   ></p>
              
              <br>

              <p><label id="error_obj_especifico_prog_inst"  for="obj_especifico_prog_inst">Objetivo Especifico Programa Institucional:<strong class="tamanio_caracteres" id="tamanio_caracteres">Valor registrado en ficha tecnica(no editable)</strong></label>
              <textarea disabled="true" id="obj_especifico_prog_inst" name="obj_especifico_prog_inst" placeholder="Objetivo Especifico Programa Instituciona:"   ></textarea>
              <br>

             

              <p><label style="display: none;" id="error_base_calculo_pid"  for="base_calculo_pid">Base Calculo:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
              <textarea style="display: none;" id="base_calculo_pid" name="base_calculo_pid" placeholder="Base Calculo:"   ></textarea>

       </div>
     </div>

     
      <div style="text-align: left;" class="tab_pid">
          <!--Robert-->
          

           <p><label id="error_fuente_pid"  for="fuente_pid">Fuente:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
          <textarea id="fuente_pid" name="fuente_pid" placeholder="Fuente:"   ></textarea>

          <p><label id="error_ref_adicionales"  for="ref_adicionales">Referencias Adicionales:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
          <textarea  id="ref_adicionales" name="ref_adicionales" placeholder="Referencias Adicionales:"   ></textarea>
          <br>

           <p><label  for="linea_base_2017_pid"  id="error_linea_base_2017_pid">Linea Base 2017</label>
          <input disabled="true"  minlength="0" maxlength="15" name="linea_base_2017_pid" placeholder="" id="linea_base_2017_pid"></p> 

          <br>
         <!--
           <p><label for="meta_2017_pid"  id="error_meta_2017_pid">Meta 2017</label>
          <input  minlength="0" maxlength="15" name="meta_2017_pid" placeholder="" id="meta_2017_pid"></p>
       -->
          <br>
           <p><label for="meta_2018_pid"  id="error_meta_2018_pid">Meta 2018 <strong class="tamanio_caracteres" id="tamanio_caracteres">Valor registrado en ficha tecnica(no editable)</strong></label>
          <input disabled="true"  minlength="0" maxlength="15" name="meta_2018_pid" placeholder="" id="meta_2018_pid"></p>
           <br>
          <p><label  for="meta_2019_pid"  id="error_meta_2019_pid">Meta 2019 <strong class="tamanio_caracteres" id="tamanio_caracteres">Valor registrado en ficha tecnica(no editable)</strong></label>
          <input disabled="true"  minlength="0" maxlength="15" name="meta_2019_pid" placeholder="" id="meta_2019_pid"></p>  
           <br>
          <p><label for="meta_2020_pid"  id="error_meta_2020_pid">Meta 2020 <strong class="tamanio_caracteres" id="tamanio_caracteres">Valor registrado en ficha tecnica(no editable)</strong></label>
          <input disabled="true"  minlength="0" maxlength="15" name="meta_2020_pid" placeholder="" id="meta_2020_pid"></p> 
           <br>
          <p><label for="meta_2021_pid"  id="error_meta_2021_pid">Meta 2020 <strong class="tamanio_caracteres" id="tamanio_caracteres">Valor registrado en ficha tecnica(no editable)</strong></label>
          <input disabled="true"  minlength="0" maxlength="15" name="meta_2021_pid" placeholder="" id="meta_2021_pid"></p> 
           <br>
          <p><label for="meta_2022_pid"  id="error_meta_2022_pid">Meta 2020 <strong class="tamanio_caracteres" id="tamanio_caracteres">Valor registrado en ficha tecnica(no editable)</strong></label>
          <input disabled="true"  minlength="0" maxlength="15" name="meta_2022_pid" placeholder="" id="meta_2022_pid"></p>
           <br>
          <p><label for="meta_2030_pid"  id="error_meta_2030_pid">Meta 2020 <strong class="tamanio_caracteres" id="tamanio_caracteres">Valor registrado en ficha tecnica(no editable)</strong></label>
          <input disabled="true"  minlength="0" maxlength="15" name="meta_2030_pid" placeholder="" id="meta_2030_pid"></p> 


           <p><label for="resultado_pid_2018"  id="error_resultado_pid_2018">Resultado PID 2018</label>
          <input  minlength="0" maxlength="15" name="resultado_pid_2018" placeholder="" id="resultado_pid_2018"></p> 

                       
      </div>

      <div style="text-align: left;" class="tab_pid">
        
        <br>
       
          <p><label for="check_list_tipo_evidencia_def_pid"> ¿Qué tipo de evidencia se presenta?  Seleccione los necesarios<strong class="tamanio_caracteres" id="tamanio_caracteres"> Puedes seleccionar mas de uno</strong></label></p>
            <div id="check_list_tipo_evidencia_def_pid"></div>

            <script>
            var t_evidencia = 
              ["Acta, Convenios o minuta",
                 "Diagnostico",
                 "Padrones o bases de datos",
                 "Publicación en físico o electrónico",
                 "Reporte",
                 "Otro"];
              var text = "";
              var i;
              text+="<ul>"
              for (i = 0; i < t_evidencia.length; i++) 
              {

                text += '<label class="checkbox-inline ">'+'<input type="checkbox"   class="check_type_evi_pid" id="check_evi_pid_'+t_evidencia[i]+'"  name="'+t_evidencia[i]+'">'+t_evidencia[i]+'</label>';
              }

              text+="</ul>"
              document.getElementById("check_list_tipo_evidencia_def_pid").innerHTML = text;
            </script>


             <div id="tipo_evidencia_otro_pid">
                <p><label for="tipo_evi_otro_pid">Definelo </label>
                <input  name="tipo_evi_otro_pid" id="tipo_evi_otro_pid" type="text" ></p>

             </div>

            <input  name="tipo_evidencia_def_list_pid" id="tipo_evidencia_def_list_pid" style="display: none;"  >


           

            <br>
            <p><label id="error_descipcion_evindecia" for="descripcion_evidencia_pid">¿Qué contiene la evidencia? Describir qué es el documento y por qué es una evidencia vinculada al indicador.<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
            <textarea id="descripcion_evidencia_pid" maxlength="450" name="descripcion_evidencia_pid" placeholder="¿Qué contiene la evidencia? Describir qué es el documento y por qué es una evidencia vinculada al indicador."   ></textarea></p> 

            <br>

	<div class="row">
      	<div class="col-sm-6" style=" text-align:center;">
        <div>
          <label for="datepicker_pid_inicio"> La fecha inicio de la evidencia es:</label>
          <input  name="datepicker_pid_inicio" id="datepicker_pid_inicio">
      
        </div>
      	</div>

      	<div class="col-sm-6" style=" text-align:center;">
        <div>
          <label for="datepicker_pid_fin"> La fecha termino de la evidencia es:</label>
          <input  name="datepicker_pid_fin" id="datepicker_pid_fin">
        </div>
      	</div>
    	</div>
            <!--
		<label for="fecha_corte_evidencia_pid"> La fecha de la evidencia es:</label>
                <div class="select">
                  <select name="fecha_corte_evidencia_pid" id="fecha_corte_evidencia_pid" >
                    <option value="Corte 15 Noviembre">Corte 15 Noviembre</option>
                    
                  </select>
                </div>
		-->


              <!--
                <div id="upload_tabla_localidades">
                  <div id="upload_def_evidencia"></div>
                </div>
        
                <input name="des_geo_municipios" id="des_geo_municipios_list" type="hidden">


                <input name="des_geo_entidades" id="des_geo_entidades_list" type="hidden">
              -->

                <br>
                <label for="existe_expediente_doc_pid">Existe expediente documental</label>
                <div class="select">
                  <select name="existe_expediente_doc_pid" id="existe_expediente_doc_pid">
                    <option value="No" >No</option>
                    <option value="Si">Si</option>
                    
                  </select>
                </div>


               <div id="existe_exp_doc">

                
                  <p><label id="error_serie_evidencia_pid" for="serie_evidencia_pid">Serie de la Evidencia</label><input id="serie_evidencia_pid" name="serie_evidencia_pid" placeholder="Serie de la Evidencia" ></p>

                 
                  <p><label id="error_seccion_pid" for="seccion_pid">Sección(opcional)</label><input  name="seccion_pid" id="seccion_pid" placeholder="sección (opcional)" ></p>
                  
                  <p><label id="error_guia_invetario_pid" for="guia_invetario_pid">Guía/Inventario en el que se incluye el expediente</label><input  name="guia_invetario_pid" id="guia_invetario_pid" placeholder="Guía/Inventario en el que se incluye el expediente" ></p>
               
               </div>



              <div id="no_existe_expediente_doc">
              <!--Robert-->
              <p><label for="medios_verificacion_evidencia_pid" id="error_medios_comunicacion_pid">Define cuál es la ubicación electrónica o física de la evidencia o cómo es posible rastrearla<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label><textarea id="medios_verificacion_evidencia_pid" maxlength="350" name="medios_verificacion_evidencia_pid" placeholder="Define cuál es la ubicación electrónica o física de la evidencia o cómo es posible rastrearla" ></textarea></p> 
              </div>

              <br>
             <p><label id="error_comentarios_pid" for="comentarios_pid">Comentarios <strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label><textarea id="comentarios_pid" maxlength="450" name="comentarios_pid" placeholder="Comentarios" ></textarea></p> 

              <br>


               <div>
                  <table>
                    <col width=50%>
                    <col width=50%>
                      <tbody>
                        <tr>
                           <td style="text-align:center;" >
                          <h3>Adjuntar evidencias</h5>
                          <br>
                          <p><label for="upload_adjuntar_evidencias_pid">Puedes seleccionar multiples archivos</label></p>
                          <div style="text-align:center;" id="upload_adjuntar_evidencias_pid" name="upload_adjuntar_evidencias_pid">  </div>
                        </td>


                        <td style="text-align:center;">
                          <!--
                          <div id="area_adj_plan_trabajo_pid">
                            <h3>Adjuntar Plan de trabajo</h3>
                            <br>
                            <p><label for="plan_trabajo_def">Puedes seleccionar multiples archivos</label></p>
                            <div style="text-align:center;" id="upload_plan_trabajo" name="upload_plan_trabajo">  </div>
                               
                             
                          </div>
                          -->
                        </td>

                        </tr>
                       
                         
                      </tbody>
                  </table>
              </div>  




              

              <br>
              <p><input style="display: none;" name="nom_usuario_pid" id="nom_usuario_pid"  value="<?php echo ($user_name); ?>" ></p>


              <input  name="oculto" value="1" style="display: none;"  >

      </div>

       <div  class="tab_pid">
        <br>
        <br>
        <br>
        <br>

        <label style="text-align: center color: #4CAF50; font-size: 17px; font-family: Graphik-Bold" >Informacion lista</label>


        <h2><font font-family: Graphik-Bold color="green" size="6">ENVIAR INFORMACION</font></h2>
       </div>


    <div style="overflow:auto;">
      <div style="float:right;">
        <button type="button" id="prevBtn_pid" onclick="nextPrev_Pids(-1)">Anterior</button>
        <button type="button" name="enviar_form_pid" id="nextBtn_pid" onclick="nextPrev_Pids(1)">Siguiente</button>
      </div>
    </div>

    <div style="text-align:center;margin-top:40px;">
      <span class="step_pids"></span>
      <span class="step_pids"></span>
      <span class="step_pids"></span>
      <span class="step_pids"></span>
      <span class="step_pids"></span>
      <span class="step_pids"></span>


     
      
    </div>
  </form>
</div>

<div id="Estatus_tac_pid" class="tabcontent_tac_pid">
  
</div>

<div id="reporte_pids" class="tabcontent_tac_pid">

   <br>
    <h3>Tacticos Pids</h3>
      
      <table>
        <col width=5%>
        <col width=65%>
        <col width=30%>
        <br>
        <br>
          <tbody>
              <?php global $wpdb;
                $ind_pids = $wpdb->prefix .'indicadores_pids';
                $registros_pids = $wpdb->get_results("SELECT * FROM $ind_pids WHERE nombre_usuario_pid ='$user_name'", ARRAY_A);
                foreach($registros_pids as $registro_pid) 
                  { ?>
                    <?php $id_evidencia=$registro_pid['id_tactico_pid']; 
                     $tipo="pids";
                    ?>
                   
                      <tr>
                         <td>
                        
                        <img style="text-align: center" src="http://192.168.64.2/repositorio/wp-content/uploads/2018/04/PDF.png" height="42" width="30" ></img>
                      </td>
                         <td id="txt_list_pdf_estr"> <?php    echo("<p class='demo'><a href='pdf_plugin/pdf_evidencias/crearEvidenciaPIDS.php?id_evidencia= $id_evidencia&arr_ids_tacticos=0&user=$user_name&tipo=$tipo' target='_blank' class='demo'>".$registro_pid['nom_indicador_pid']."</a></p>");?>
            <br>
                         
             </td> 
             <td>
               
             </td>

             </tr>
                          
            <?php } ?> 
          </tbody>
        
      </table> 

</div>

<script>
                 
    $( "#nombre_indicador_pid" ).autocomplete({  
      source: "/repositorio/wp-content/themes/EspecialesT2.1/templates/searh_indicador.php?id=<?php echo($user_name);?>&tipo=pids",
      minLength: 1,

       response: function(event, ui)        //TYERMINO DE REGISTRAR SUS INDICADORES
       {
        
          if (!ui.content.length) {
            /*
                 $('#registro_terminado').show(); 
                 var x = document.getElementById("nextBtn");
                 x.setAttribute("disabled", true);
                 */
           // alert("has terminado de cargar todos tus indicadores ");
          }
          
       }

    });

    //UNA VEZ SELECCIONADO EL NOMBRE DEL INDICADOR LOS DEMAS CAMPOS SE LLENARAN AUTOMATICAMENTE

    $( "#nombre_indicador_pid" ).autocomplete({
      close: function( event, ui ) 
      {

        
      },

      select: function( event, ui ) 
      {
        //alert(ui.item.value);
        $('#indicadore_seleccionado_autollenado_tacticos_pids').show();//RENDERIZAR EL RESTO DEL FORMULARIO

        var nom_indi_selec=ui.item.value;
        $.ajax({

               type: "GET",
               datatype: 'json',
               contentType: "application/json; charset=utf-8",
              
               url: "/repositorio/wp-content/themes/EspecialesT2.1/templates/indicador_seleccionado.php?nom_indi_selec="+nom_indi_selec+"&tipo=pids &nombre_usuario=<?php echo($user_name);?>",
               success: function(responce)
                {
                  
                  console.log(responce);
                  console.log(responce.descripcion_indicador);

                  /*-ocultos-*/
                    var nom_dep_pid = document.getElementById("nom_dependencia_pid");
                    nom_dep_pid.value=responce.nom_dependencia;

                    var nom_orga_pid = document.getElementById("nom_organismo_pid");
                    nom_orga_pid.value=responce.nom_organismo;

                    var periodicidad_pid = document.getElementById("periodicidad_pid");
                    periodicidad_pid.value=responce.periodicidad;

                    var indica_informe = document.getElementById("indica_informe");
                    indica_informe.value=responce.indica_informe;

                    var eje_ped_pid = document.getElementById("eje_ped_pid");
                    eje_ped_pid.value=responce.eje_ped;

                    var obj_estr_pid = document.getElementById("obj_estr_pid");
                    obj_estr_pid.value=responce.obj_estrategico;

                    var obj_gen_pid = document.getElementById("obj_gen_pid");
                    obj_gen_pid.value=responce.obj_general;

                    var avance_inf_2017 = document.getElementById("avance_inf_2017");
                    avance_inf_2017.value=responce.avanceinforme2017;


                    

                    

                    

                    
                  /*-ocultos-*/

                  var descr_indicador = document.getElementById("descrp_tac_pid");
                  descr_indicador.value=responce.descripcion_indicador;

                  var uni_obs = document.getElementById("unidad_observacion_ind_pid");
                  uni_obs.value=responce.unidad_observacion_indicador;


                
                   if(responce.tipo_indicador=="Número")
                     {
                     // alert("es numero");
                      document.getElementById("tipo_indicador_def_pid").selectedIndex = 0;
                     }
                    else if(responce.tipo_indicador=="Porcentaje")
                     {

                      document.getElementById("tipo_indicador_def_pid").selectedIndex = 1;
                     }

                    else if(responce.tipo_indicador=="Tasa")
                     {
                      document.getElementById("tipo_indicador_def_pid").selectedIndex = 2;
                     }
                    else if(responce.tipo_indicador=="Absolutos")
                     {
                      document.getElementById("tipo_indicador_def_pid").selectedIndex = 3;
                     }

                     else if(responce.tipo_indicador=="Hectareas")
                     {
                      document.getElementById("tipo_indicador_def_pid").selectedIndex = 4;
                     }
                    else if(responce.tipo_indicador=="Cobertura")
                     {
                      document.getElementById("tipo_indicador_def_pid").selectedIndex = 5;
                     }

                     else if(responce.tipo_indicador=="Promedio")
                     {
                      document.getElementById("tipo_indicador_def_pid").selectedIndex = 6;
                     }

                     else if(responce.tipo_indicador=="Otro")
                     {
                      document.getElementById("tipo_indicador_def_pid").selectedIndex = 7;

                        $('#otro_tipo_indicador_pid').show();
                     }


                     if(responce.tendencia=="Ascendente")
                     {
                      
                      document.getElementById("tendencia_esp_pid").selectedIndex = 0;
                     }
                    else if(responce.tendencia=="Descendente")
                     {

                      document.getElementById("tendencia_esp_pid").selectedIndex = 1;
                     }

                    else if(responce.tendencia=="Constante")
                     {
                      document.getElementById("tendencia_esp_pid").selectedIndex = 2;
                     }
                    else if(responce.tendencia=="Variable")
                     {
                      document.getElementById("tendencia_esp_pid").selectedIndex = 3;
                     }

                     var obj_esp = document.getElementById("obj_especifico_prog_inst");
                      obj_esp.value=responce.objetivo_especifico_programa_institucional;


                      var base_Cal = document.getElementById("base_calculo_pid");
                      base_Cal.value=responce.base_calculo;
                      
                     

                     var fuent = document.getElementById("fuente_pid");
                      fuent.value=responce.fuente;

                      var ref_adi = document.getElementById("ref_adicionales");
                      ref_adi.value=responce.referencias_adicionales;

                      var lin_b_2017 = document.getElementById("linea_base_2017_pid");
                      lin_b_2017.value=responce.linea_base_2017;

                     // var m_2017 = document.getElementById("meta_2017_pid");
                      //m_2017.value=responce.meta_2017;

                      var m_2018 = document.getElementById("meta_2018_pid");
                      m_2018.value=responce.meta_2018;

                      var m_2019 = document.getElementById("meta_2019_pid");
                      m_2019.value=responce.meta_2019;

                      var m_2020 = document.getElementById("meta_2020_pid");
                      m_2020.value=responce.meta_2020;

                      var m_2021 = document.getElementById("meta_2021_pid");
                      m_2021.value=responce.meta_2021;

                      var m_2022 = document.getElementById("meta_2022_pid");
                      m_2022.value=responce.meta_2022;

                      var m_2030 = document.getElementById("meta_2030_pid");
                      m_2030.value=responce.meta_2030;




                /*
               
                  

                  var descr_observacion = document.getElementById("descripcion_observacion_ficha_tecnica_pid");
                  descr_observacion.value= responce.objetivo_especifico_programa_sec


                  

                  var tendencia=document.getElementById("tendencia_esp_pid");

                  var meta_2018_db=document.getElementById("meta_2018_oculto");
                  //meta_2018_db.value=responce.meta_2018;

                  meta_2018_str=responce.meta_2018;
                  var meta_limpia = meta_2018_str.replace("%"," ");
                  
                  var meta_limpia = meta_limpia.replace(",",".");
                  meta_2018_db.value=meta_limpia;




                  //--- partte dos
                   if(responce.tendencia=="Ascendente")
                   {
                    tendencia.value = "Ascendente";
                   }
                   else if(responce.tendencia=="Descendente")
                   {
                    tendencia.value = "Descendente";
                   }
                  else if(responce.tendencia=="Estable")
                   {
                    tendencia.value = "Estable";
                   }

                   

                  var lin_b_abs_ = document.getElementById("linea_base_absolutos_pid");
                  var linb_a=responce.linea_base_absolutos;
                  if(linb_a=="N.D.")
                  {
                   
                    document.getElementById("linea_base_absolutos_pid").disabled = true;
                    lin_b_abs_.value=-1;
                    $('#area_adj_plan_trabajo_pid').show();//hacer visible la linea
                  }
                  else
                  {
                    var linb_abs_f = linb_a.replace(",",".");
                    lin_b_abs_.value=linb_abs_f;
                    

                  }

                  var prim_informe = document.getElementById("reporte_primer_informe_def");
                  var p_inf=responce.avanceinforme2017;
                  if(p_inf=="N.D.")
                  {
                      document.getElementById("reporte_primer_informe_def").disabled = true;
                      prim_informe.value=-1;

                  }
                  else
                  {
                    p_inf_limp = p_inf.replace(",",".");
                    console.log(p_inf_limp);
                    prim_informe.value=p_inf_limp; 
                  }

                    nom_evidencia = nom_indi_selec;
                    */

                  //var nom_evidencia = $('#nombre_indicador_pid').val();
                  /*
                  $("#upload_def_evidencia").uploadFile
                  ({
                      url:"/repositorio/upload_f|iles_plugin/php/upload_def_evidencia.php",
                      fileName:"myfile",
                      formData: {"nom_evidencia":nom_evidencia,"usuario":"<?php echo $user_name;?>"}  
                  });

                  $("#upload_plan_trabajo").uploadFile
                  ({
                      url:"/repositorio/upload_files_plugin/php/upload_plan_trabajo.php",
                      fileName:"myfile",
                      formData: {"nom_evidencia":nom_evidencia,"usuario":"<?php echo $user_name;?>"}  
                    
                  });
                  */
                },
              error: function(data){
           }
        }); 
      }





      
    });


//--acciones del formulario


    $('#logros_en_indicador_pid').change(function()
      { 
          if($(this).val() == "Si")
          {
             $('#bullet_Del_logro_pid').show();
          }
          else if($(this).val() == "No")
          {
            $('#bullet_Del_logro_pid').hide();
          }


      });


      $('#premios_recono_pid').change(function()
        { 
         

             if($(this).val() == "Si")
            {
               $('#premios_reconocimientos_pid').show();
            }
            else if($(this).val() == "No")
            {
              $('#premios_reconocimientos_pid').hide();
            }


        });
//--funcion fecha inicio fin
	$( function() 
      {
        $( "#datepicker_pid_inicio" ).datepicker();
        $( "#datepicker_pid_fin" ).datepicker();
        
      });

</script>



<script type="text/javascript">

  $(document).ready(function() 
  {

  
    //activar modulos
    /*
     $.ajax({
         type: "GET",
         datatype: 'json',
         contentType: "application/json; charset=utf-8",
        
         url: "/repositorio/wp-content/themes/EspecialesT2.1/templates/ctrl_modulos.php?nom_dep="+'<?php echo($user_name);?>',
         success: function(respuesta)
          {
            
            var arr_mod = respuesta.slice(1, -1);
            var arr_mod = arr_mod.split(",");

            var i;
            for (i = 0; i < arr_mod.length; i++) { 
              if(arr_mod[i]=="0")
              {
                 $("#btn_acc_tac_pid_reg").removeAttr('disabled');
                 
               
              }
              else if(arr_mod[i]=="1")
              {
                 $("#btn_acc_tac_pid_edit").removeAttr('disabled');
                 
              }

              else if(arr_mod[i]=="2")
              {
                 $("#btn_acc_tac_pid_repdf").removeAttr('disabled');
                
              }


            
              //console.log(arr_mod[i]);
            }
            
           
          },
          error: function(data)
          {

          }
         });
         */


     $('#indicadore_seleccionado_autollenado_tacticos_pids').hide();
     $('#bullet_Del_logro_pid').hide();
     $('#premios_reconocimientos_pid').hide();



      $('#datos_cargados_lug_1_pid').hide();
      $('#datos_cargados_lug_2_pid').hide();
      $('#datos_cargados_lug_3_pid').hide();
      $('#datos_cargados_lug_4_pid').hide();
      $('#datos_cargados_lug_5_pid').hide();

      $('#otro_tipo_indicador_pid').hide();
      $('#tipo_evidencia_otro_pid').hide();

      $('#existe_exp_doc').hide();
      //$('#area_adj_plan_trabajo_pid').hide();  

      $( "#resultado_1_pid" ).keyup(function() 
          {

            var nom_ind_loaded = document.getElementById("nombre_indicador_pid");
            var n_indicador=nom_ind_loaded.value;

            $("#upload_doc_resultados_1_pid").uploadFile
            ({
              
                url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                fileName:"myfile",
                maxFileCount:60,
                showCancel: true,
                showDone: true,
                showError: true,
                maxFileSize:1000*600000,
                showPreview:true,
                previewHeight: "100px",
                previewWidth: "100px",
                formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":1}, 

                onSuccess:function(files,data,xhr,pd)
                  {
                    //alert("archivo guardado falta actualizar la tabla de archivos")
 
                  } 
            }); 

            

            $("#upload_doc_resultados_2_pid").uploadFile
            ({
              
                url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                fileName:"myfile",
                maxFileCount:60,
                showCancel: true,
                showDone: true,
                showError: true,
                maxFileSize:1000*600000,
                showPreview:true,
                previewHeight: "100px",
                previewWidth: "100px",
                formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":2},
                onSuccess:function(files,data,xhr,pd)
                  {
                    //alert("archivo guardado falta actualizar la tabla de archivos")
 
                  }  
              
            }); 

            $("#upload_doc_resultados_3_pid").uploadFile
            ({
              
                url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                fileName:"myfile",
                maxFileCount:60,
                showCancel: true,
                showDone: true,
                showError: true,
                maxFileSize:1000*600000,
                showPreview:true,
                previewHeight: "100px",
                previewWidth: "100px",
                formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":3},
                onSuccess:function(files,data,xhr,pd)
                  {
                    //alert("archivo guardado falta actualizar la tabla de archivos")
 
                  }   
              
            }); 
  
            $("#upload_doc_resultados_4_pid").uploadFile
            ({
              
                url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                fileName:"myfile",
                maxFileCount:60,
                showCancel: true,
                showDone: true,
                showError: true,
                maxFileSize:1000*600000,
                showPreview:true,
                previewHeight: "100px",
                previewWidth: "100px",
                formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":4} ,
                onSuccess:function(files,data,xhr,pd)
                  {
                    //alert("archivo guardado falta actualizar la tabla de archivos")
 
                  }  
              
            }); 
                                    
            $("#upload_doc_resultados_5_pid").uploadFile
            ({


              
                url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                fileName:"myfile",
                maxFileCount:60,
                showCancel: true,
                showDone: true,
                showError: true,
                maxFileSize:1000*600000,
                showPreview:true,
                previewHeight: "100px",
                previewWidth: "100px",
                formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":5},
                onSuccess:function(files,data,xhr,pd)
                  {
                    //alert("archivo guardado falta actualizar la tabla de archivos")
 
                  }  
              
            }); 

            $("#upload_plan_trabajo").uploadFile
            ({

              
                url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                fileName:"myfile",
                maxFileCount:60,
                showCancel: true,
                showDone: true,
                showError: true,
                maxFileSize:1000*600000,
                showPreview:true,
                previewHeight: "100px",
                previewWidth: "100px",
                formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":"plan"},
                onSuccess:function(files,data,xhr,pd)
                  {
                    //alert("archivo guardado falta actualizar la tabla de archivos")
 
                  }  
              
            }); 

          $("#upload_adjuntar_evidencias_pid").uploadFile
            ({
                url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                fileName:"myfile",
                maxFileCount:60,
                showCancel: true,
                showDone: true,
                showError: true,
                maxFileSize:1000*600000,
                showPreview:true,
                previewHeight: "100px",
                previewWidth: "100px",
                formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":"evidencias"},
                onSuccess:function(files,data,xhr,pd)
                  {
                    //alert("archivo guardado falta actualizar la tabla de archivos")
 
                  }  
              
            });

            /*
              $("#area_adj_plan_trabajo_pid").uploadFile
            ({
                url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                fileName:"myfile",
                maxFileCount:60,
                showCancel: true,
                showDone: true,
                showError: true,
                maxFileSize:1000*600000,
                showPreview:true,
                previewHeight: "100px",
                previewWidth: "100px",
                formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":"plan"},
                onSuccess:function(files,data,xhr,pd)
                  {
                    //alert("archivo guardado falta actualizar la tabla de archivos")
 
                  }  
              
            });
            */


          });
});
      



  //document.getElementById("btn_acc_tac_pid_reg").click();
  
  tabcontent_tac_pid = document.getElementsByClassName("tabcontent_tac_pid");
    for (i = 0; i < tabcontent_tac_pid.length; i++) {
        tabcontent_tac_pid[i].style.display = "none";
    }

  function open_accion_tac_pid(evt, cityName) {
    

    $('#intro_idicador_pids_tac_pids').hide();
    // Declare all variables
    var i, tabcontent_tac_pid, tablinks_tac_pids;

    // Get all elements with class="tabcontent_tac_pid" and hide them
    tabcontent_tac_pid = document.getElementsByClassName("tabcontent_tac_pid");
    for (i = 0; i < tabcontent_tac_pid.length; i++) {
        tabcontent_tac_pid[i].style.display = "none";
    }

    // Get all elements with class="tablinks_tac_pids" and remove the class "active"
    tablinks_tac_pids = document.getElementsByClassName("tablinks_tac_pids");
    for (i = 0; i < tablinks_tac_pids.length; i++) {
        tablinks_tac_pids[i].className = tablinks_tac_pids[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>

<script type="text/javascript">
  

  var currentTab = 0; // Current tab is set to be the first tab (0)
showTab_Acciones_pids(currentTab); // Display the current tab

function showTab_Acciones_pids(n) {
  // This function will display the specified tab of the form ...
  var x = document.getElementsByClassName("tab_pid");
  x[n].style.display = "block";
  // ... and fix the Previous/SIGUIENTE buttons:
  //alert(n+" ----limite"+(x.length-1));
  if (n == 0) {
    document.getElementById("prevBtn_pid").style.display = "none";
  } else {
    document.getElementById("prevBtn_pid").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn_pid").innerHTML = "Enviar";
  } else {
    document.getElementById("nextBtn_pid").innerHTML = "Siguiente";
  }
  // ... and run a function that displays the correct step indicator:
  fixStepIndicator_Acciones_pids(n)
}

function nextPrev_Pids(n) {
  
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab_pid");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm_Acciones_pids()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;

    if(currentTab==6)
    {
    tipo_evidencia_get_values();
   // alert("cargar las evidencias seleccionaddas");
    }


   if (currentTab >= (x.length)-1) 
      {
       
      // alert("submit");
       var x = document.getElementById("nextBtn_pid");
       x.setAttribute("type", "submit");
       return false;
      }

  showTab_Acciones_pids(currentTab);
}

function validateForm_Acciones_pids() {
  
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab_pid");
  y = x[currentTab].getElementsByTagName("input");
    // A loop that checks every input field in the current tab:
    for (i = 0; i < y.length; i++) 
    {


      if (y[i].value == "") 
      {



        if(y[i].id=="otro_tipo_ind_pid")
        {

        }

        else if(y[i].id=="nom_dependencia_pid")
        {

        }
         else if(y[i].id=="nom_organismo_pid")
        {

        }

        else if(y[i].id=="periodicidad_pid")
        {

        }
        else if(y[i].id=="indica_informe")
        {

        }
        else if(y[i].id=="eje_ped_pid")
        {

        }
        else if(y[i].id=="obj_estr_pid")
        {

        }
         else if(y[i].id=="obj_gen_pid")
        {

        }
         else if(y[i].id=="avance_inf_2017")
        {

        }

        

        else if(y[i].id=="res1_lugares_pid")
        {
          
         document.getElementById('datos_cargados_lug_1_pid').innerHTML ="Define el lugar";
         var element = document.getElementById("datos_cargados_lug_1_pid");
         element.classList.remove("datos_cargados_lugar_resultados");
         element.classList.add("error_mensaje"); 
         $('#datos_cargados_lug_1_pid').show();

          y[i].className += " invalid";
          valid = false;
          
        }
        else if(y[i].id=="res2_lugares_pid")
        {

        }
        else if(y[i].id=="res3_lugares_pid")
        {

        }
        else if(y[i].id=="res4_lugares_pid")
        {

        }
        else if(y[i].id=="res5_lugares_pid")
        {

        }

        else if(y[i].id=="serie_evidencia_pid")
        {

        }
        else if(y[i].id=="seccion_pid")
        {

        }
         else if(y[i].id=="guia_invetario_pid")
        {

        }

        else if(y[i].name=="myfile[]")
        {

        }
        else if(y[i].name=="tipo_evi_otro_pid")
        {

        }
        else if(y[i].name=="tipo_evidencia_def_list_pid")
        {

        }

        else
        {
          // add an "invalid" class to the field:
          y[i].className += " invalid";
          // and set the current valid status to false:
          valid = false;
        }

      }


      else if(y[i].name=="myfile[]")
      {

      }

      else if (y[i].id == "nombre_indicador_pid" && y[i].value.length < 1 ) 
      {
          //nombre del indicador
         document.getElementById('error_nombre_indicador_pid').innerHTML ="Verifica el nombre de tu indicador";
         var element = document.getElementById("error_nombre_indicador_pid");
         element.classList.add("error_mensaje");
          // add an "invalid" class to the field:
        y[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }
      else if (y[i].id == "unidad_observacion_ind_pid" && y[i].value.length >70 ) 
      {
          //nombre del indicador
         document.getElementById('error_unidad_observacion_ind_pid').innerHTML ="El limite del campo es de 70 caracteres";
         var element = document.getElementById("error_unidad_observacion_ind_pid");
         element.classList.add("error_mensaje");
          // add an "invalid" class to the field:
        y[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }
      /*
      else if (y[i].id == "linea_base_absolutos_pid" ) //validar q sea solo numeros
      {
          //nombre del indicador

          if (y[i].value.match("((-|\\+)?[0-9]+(\\.[0-9]+)?)+")) 
          {
            //alert("Its a boy");
            //  System.out.println("Is a number");
              y[i].className += " valid";
              valid = true;
          } else 
          {
            //  System.out.println("Is not a number");
              document.getElementById('error_linea_base_pid').innerHTML ="El valor tiene que ser numerico";
               var element = document.getElementById("error_linea_base_pid");
               element.classList.add("error_mensaje");
                // add an "invalid" class to the field:
              y[i].className += " invalid";
              // and set the current valid status to false
              valid = false;
          }
         
      }
      */

      else if (y[i].id == "avance_meta_planeada_def" && y[i].value.length >10 ) 
      {
          //nombre del indicador
         document.getElementById('error_avance_meta_planeada_def').innerHTML ="El limite del campo es de 10 caracteres";
         var element = document.getElementById("error_avance_meta_planeada_def");
         element.classList.add("error_mensaje");
          // add an "invalid" class to the field:
        y[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }

      else if (y[i].id == "fecha_evidencia_inicio_def" && y[i].value.length !=10 ) 
      {
          //nombre del indicador
         document.getElementById('error_fecha_evidencia_inicio_def').innerHTML ="Verifica tu informacion";
         var element = document.getElementById("error_fecha_evidencia_inicio_def");
         element.classList.add("error_mensaje");
          // add an "invalid" class to the field:
        y[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }
       else if (y[i].id == "fecha_evidencia_fin_def" && y[i].value.length !=10 ) 
      {
          //nombre del indicador
         document.getElementById('error_fecha_evidencia_fin_def').innerHTML ="Verifica tu informacion";
         var element = document.getElementById("error_fecha_evidencia_fin_def");
         element.classList.add("error_mensaje");
          // add an "invalid" class to the field:
        y[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }

       else if (y[i].id == "serie_evidencia_pid" && y[i].value.length >100 ) 
      {
          //nombre del indicador
         document.getElementById('error_serie_evidencia_pid').innerHTML ="El limite del campo es de 100 caracteres";
         var element = document.getElementById("error_serie_evidencia_pid");
         element.classList.add("error_mensaje");
          // add an "invalid" class to the field:
        y[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }

       else if (y[i].id == "seccion_pid" && y[i].value.length >200 ) 
      {
          //nombre del indicador
         document.getElementById('error_seccion_pid').innerHTML ="El limite del campo es de 200 caracteres";
         var element = document.getElementById("error_seccion_pid");
         element.classList.add("error_mensaje");
          // add an "invalid" class to the field:
        y[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }


       else if (y[i].id == "guia_invetario_pid" && y[i].value.length >200 ) 
      {
          //nombre del indicador
         document.getElementById('error_guia_invetario_pid').innerHTML ="El limite del campo es de 200 caracteres";
         var element = document.getElementById("error_guia_invetario_pid");
         element.classList.add("error_mensaje");
          // add an "invalid" class to the field:
        y[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }


    }

    txtarea = x[currentTab].getElementsByTagName("textarea");
    // A loop that checks every input field in the current tab:
    for (i = 0; i < txtarea.length; i++) 
    {
      // If a field is empttxtarea...
      
      if (txtarea[i].value == "") 
      {

        if(txtarea[i].id=="id_bullet_Del_logro_pid" )
        {

        }
        else if(txtarea[i].id=="resultado_2_pid")
        {

        }
        else if(txtarea[i].id=="resultado_3_pid")
        {

        }
        else if(txtarea[i].id=="resultado_4_pid")
        {

        }
        else if(txtarea[i].id=="resultado_5_pid")
        {

        }

        else if(txtarea[i].id=="lo_que_sigue_2")
        {

        }
        else if(txtarea[i].id=="lo_que_sigue_3")
        {

        }
        
          else if(txtarea[i].id=="medios_verificacion_evidencia_pid")
        {

        }
        


        
        else if(txtarea[i].id=="comentarios_pid")
        {

        }

        else if(txtarea[i].id=="reconocimiento_pid")
        {

        }

        else
        {
                                      // add an "invalid" class to the field:
        txtarea[i].className += " invalid";
        // and set the current valid status to false:
        valid = false;
        }


      }

      else if (txtarea[i].id == "reconocimiento_pid" && txtarea[i].value.length > 450 ) 
      {
          //nombre del indicador
         document.getElementById('error_reconocimiento_pid').innerHTML ="El limite del campo es de 450 caracteres";
         var element = document.getElementById("error_reconocimiento_pid");
         element.classList.add("error_mensaje");
          
          // add an "invalid" class to the field:
        txtarea[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }


      else if (txtarea[i].id == "descripcion_evidencia_pid" && txtarea[i].value.length > 450 ) 
      {
          //nombre del indicador
         document.getElementById('error_descipcion_evindecia').innerHTML ="El limite del campo es de 450 caracteres";
         var element = document.getElementById("error_descipcion_evindecia");
         element.classList.add("error_mensaje");
          
          // add an "invalid" class to the field:
        txtarea[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }
      

      else if (txtarea[i].id == "comentarios_pid" && txtarea[i].value.length > 450 ) 
      {
          //nombre del indicador
         document.getElementById('error_comentarios_pid').innerHTML ="El limite del campo es de 450 caracteres";
         var element = document.getElementById("error_comentarios_pid");
         element.classList.add("error_mensaje");
          
          // add an "invalid" class to the field:
        txtarea[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }



      else if (txtarea[i].id == "medios_verificacion_evidencia_pid" && txtarea[i].value.length > 350 ){
          //nombre del indicador
         document.getElementById('error_medios_comunicacion_pid').innerHTML ="El limite del campo es de 350 caracteres";
         var element = document.getElementById("error_medios_comunicacion_pid");
         element.classList.add("error_mensaje");
          
          // add an "invalid" class to the field:
        txtarea[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }

      //-----paso1-----------------------
      /*
      else if (txtarea[i].id == "linea_discursiva_pid" && txtarea[i].value.length > 350 ){
          //nombre del indicador
         document.getElementById('error_linea_discursiva_pid').innerHTML ="El limite del campo es de 350 caracteres";
         var element = document.getElementById("error_linea_discursiva_pid");
         element.classList.add("error_mensaje");
          
          // add an "invalid" class to the field:
        txtarea[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }
*/
      else if (txtarea[i].id == "descrp_tac_pid" && txtarea[i].value.length > 500 ){
          //nombre del indicador
         document.getElementById('error_descrp_tac_pid').innerHTML ="El limite del campo es de 350 caracteres";
         var element = document.getElementById("error_descrp_tac_pid");
         element.classList.add("error_mensaje");
          
          // add an "invalid" class to the field:
        txtarea[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }

      

      else if (txtarea[i].id == "id_bullet_Del_logro_pid" && txtarea[i].value.length > 450 ){
          //nombre del indicador
         document.getElementById('error_id_bullet_Del_logro').innerHTML ="El limite del campo es de 450 caracteres";
         var element = document.getElementById("error_id_bullet_Del_logro");
         element.classList.add("error_mensaje");
          
          // add an "invalid" class to the field:
        txtarea[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }

      
      //fin paso 1-----------------------------------

      //paso 2----------------------------------------
         
      else if (txtarea[i].id == "resultado_1_pid" && txtarea[i].value.length > 350 ){
          //nombre del indicador
         document.getElementById('error_resultado_1_pid').innerHTML ="El limite del campo es de 350 caracteres";
         var element = document.getElementById("error_resultado_1_pid");
         element.classList.add("error_mensaje");
          
          // add an "invalid" class to the field:
        txtarea[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }
         
      else if (txtarea[i].id == "resultado_2_pid" && txtarea[i].value.length > 350 ){
          //nombre del indicador
         document.getElementById('error_resultado_2_pid').innerHTML ="El limite del campo es de 350 caracteres";
         var element = document.getElementById("error_resultado_2_pid");
         element.classList.add("error_mensaje");
          
          // add an "invalid" class to the field:
        txtarea[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }
         
      else if (txtarea[i].id == "resultado_3_pid" && txtarea[i].value.length > 350 ){
          //nombre del indicador
         document.getElementById('error_resultado_3_pid').innerHTML ="El limite del campo es de 350 caracteres";
         var element = document.getElementById("error_resultado_3_pid");
         element.classList.add("error_mensaje");
          
          // add an "invalid" class to the field:
        txtarea[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }

      else if (txtarea[i].id == "resultado_4_pid" && txtarea[i].value.length > 350 ){
          //nombre del indicador
         document.getElementById('error_resultado_4_pid').innerHTML ="El limite del campo es de 350 caracteres";
         var element = document.getElementById("error_resultado_4_pid");
         element.classList.add("error_mensaje");
          
          // add an "invalid" class to the field:
        txtarea[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }

      else if (txtarea[i].id == "resultado_5_pid" && txtarea[i].value.length > 350 ){
          //nombre del indicador
         document.getElementById('error_resultado_5_pid').innerHTML ="El limite del campo es de 350 caracteres";
         var element = document.getElementById("error_resultado_5_pid");
         element.classList.add("error_mensaje");
          
          // add an "invalid" class to the field:
        txtarea[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }

      else if (txtarea[i].id == "lo_que_sigue_1" && txtarea[i].value.length > 450 ){
          //nombre del indicador
         document.getElementById('error_lo_que_sigue_1').innerHTML ="El limite del campo es de 450 caracteres";
         var element = document.getElementById("error_lo_que_sigue_1");
         element.classList.add("error_mensaje");
          
          // add an "invalid" class to the field:
        txtarea[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }
         
      else if (txtarea[i].id == "lo_que_sigue_2" && txtarea[i].value.length > 450 ){
          //nombre del indicador
         document.getElementById('error_lo_que_sigue_2').innerHTML ="El limite del campo es de 450 caracteres";
         var element = document.getElementById("error_lo_que_sigue_2");
         element.classList.add("error_mensaje");
          
          // add an "invalid" class to the field:
        txtarea[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }
      else if (txtarea[i].id == "lo_que_sigue_3" && txtarea[i].value.length > 450 ){
          //nombre del indicador
         document.getElementById('error_lo_que_sigue_3').innerHTML ="El limite del campo es de 450 caracteres";
         var element = document.getElementById("error_lo_que_sigue_3");
         element.classList.add("error_mensaje");
          
          // add an "invalid" class to the field:
        txtarea[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }
      //fin paso2 ------------------------------------
      //paso 3 -------------------------------------

      
      else if (txtarea[i].id == "descripcion_observacion_ficha_tecnica_pid" && txtarea[i].value.length > 450 ){
          //nombre del indicador
         document.getElementById('error_descripcion_observacion_ficha_tecnica_pid').innerHTML ="El limite del campo es de 450 caracteres";
         var element = document.getElementById("error_descripcion_observacion_ficha_tecnica_pid");
         element.classList.add("error_mensaje");
          
          // add an "invalid" class to the field:
        txtarea[i].className += "invalid";
        // and set the current valid status to false
        valid = false;
      }
      //fin paso 3-----------------------------------

      

    }


  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step_pids")[currentTab].className += " finish";
    //alert("finish")
  }
  return valid; // return the valid status
}

function fixStepIndicator_Acciones_pids(n) {

  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step_pids");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class to the current step:
  x[n].className += " active";


  if(n==7)
  {
    alert("agregar el submit");
  }
 // alert(x[n].className);
}




</script>


  <!--DIALOG ADD MUNICIPIOS-->
<script>
  var indice=0;
  var resultados_1;
  var resultados_2;
  var resultados_3;//valor de los lugares selecionados por resultado
  var resultados_4;
  var resultados_5;

  var resultados_1_muni;
  var resultados_2_muni;
  var resultados_3_muni;
  var resultados_4_muni;
  var resultados_5_muni;
  var id_select;




    function open_lugares_pid(num_resultado,id)
    {
      
      indice=num_resultado;
      id_select=id;

       
      $( "#dialog-message-lugar_pid" ).dialog( "open" );     //   abrir menu opciones municipal estatal localidad
    }

    function open_estados_pid()
    {
      
      
      $( "#dialog-message-lugar_pid" ).dialog( "close" );    //   cerrar menu opcines
      //$( "#dialog-form-entidades_pid" ).data('indi', '1')
      $( "#dialog-form-entidades_pid" ).dialog( "open" );   //abrir estados
    
    }
    function open_municipios_pid()
    {
      //alert("municipios");
      $( "#dialog-message-lugar_pid" ).dialog( "close" );    //   cerrar menu opcines
      $( "#dialog-form_pid" ).dialog( "open" );   //abrir municipales
    }
    function open_localidades_pid()
    {
      $( "#dialog-message-lugar_pid" ).dialog( "close" );    //   cerrar menu opcines
      $( "#dialog-message-adjuntar-localidades_pid" ).dialog( "open" );   //abrir municipales

      var loc="localidades";

      if(indice==1)
      {
        $('#doc_res1_pid').show(); //mostar el aparatado parea subir archivos 1
        $('#doc_res2_pid').hide();
        $('#doc_res3_pid').hide();
        $('#doc_res4_pid').hide();
        $('#doc_res5_pid').hide();
        resultados_1_muni="localidades";
        var res1_loc =document.getElementById("res1_lugares_pid");
        res1_loc.value= "localidades";
        document.getElementById('datos_cargados_lug_1_pid').innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_1_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_1_pid').show();
      }

      else if(indice==2)
      {
        $('#doc_res2_pid').show(); //mostar el aparatado parea subir archivos 2
        $('#doc_res1_pid').hide(); 
        $('#doc_res3_pid').hide();
        $('#doc_res4_pid').hide();
        $('#doc_res5_pid').hide();

        resultados_2_muni="localidades";
        document.getElementById('datos_cargados_lug_2_pid').innerHTML ="Datos Cargados";
        var res2_loc =document.getElementById("res2_lugares_pid");
        res2_loc.value= "localidades";
         var element = document.getElementById("datos_cargados_lug_2_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_2_pid').show();
      }
      else if(indice==3)
      {
        $('#doc_res2_pid').hide(); //mostar el aparatado parea subir archivos 2
        $('#doc_res1_pid').hide(); 
        $('#doc_res3_pid').show();
        $('#doc_res4_pid').hide();
        $('#doc_res5_pid').hide();

        resultados_3_muni="localidades";
        document.getElementById('datos_cargados_lug_3_pid').innerHTML ="Datos Cargados";
        var res3_loc =document.getElementById("res3_lugares_pid");
        res3_loc.value= "localidades";
         var element = document.getElementById("datos_cargados_lug_3_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_3_pid').show();
      }
      else if(indice==4)
      {
        $('#doc_res2_pid').hide(); //mostar el aparatado parea subir archivos 2
        $('#doc_res1_pid').hide(); 
        $('#doc_res3_pid').hide();
        $('#doc_res4_pid').show();
        $('#doc_res5_pid').hide(); 

        resultados_4_muni="localidades";
        document.getElementById('datos_cargados_lug_4_pid').innerHTML ="Datos Cargados";
        var res4_loc =document.getElementById("res4_lugares_pid");
        res4_loc.value= "localidades";
         var element = document.getElementById("datos_cargados_lug_4_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_4_pid').show();
      }
      else if(indice==5)
      {
        $('#doc_res2_pid').hide(); //mostar el aparatado parea subir archivos 2
        $('#doc_res1_pid').hide(); 
        $('#doc_res3_pid').hide();
        $('#doc_res4_pid').hide();
        $('#doc_res5_pid').show();

        resultados_5_muni="localidades";
        document.getElementById('datos_cargados_lug_5_pid').innerHTML ="Datos Cargados";
        var res5_loc =document.getElementById("res5_lugares_pid");
        res5_loc.value= "localidades";
         var element = document.getElementById("datos_cargados_lug_5_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_5_pid').show();
      }
      //cao dinamico de los aparatados de edicion 
      if(document.getElementById('res1_lugares_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>') && indice==1)
      {

        //guardar info localidades en input 
        var lugar_1_edit=document.getElementById('res1_lugares_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>');

        
        lugar_1_edit.value=loc;
        //mensaje datos cargados
        document.getElementById('datos_cargados_lug_1_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>').innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_1_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_1_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>').show();


        

      }
      else if(document.getElementById('res2_lugares_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>') && indice==2)
      {
        var lugar_1_edit=document.getElementById('res2_lugares_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>');
        lugar_1_edit.value=loc;

                                    //mensaje datos cargados
        document.getElementById('datos_cargados_lug_2_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>').innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_2_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_2_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>').show();



      }

      else if(document.getElementById('res3_lugares_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>') && indice==3)
      {
        var lugar_1_edit=document.getElementById('res3_lugares_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>');
        lugar_1_edit.value=loc;

                                    //mensaje datos cargados
        document.getElementById('datos_cargados_lug_3_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>').innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_3_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_3_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>').show();



      }

      else if(document.getElementById('res4_lugares_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>') && indice==4)
      {
        var lugar_1_edit=document.getElementById('res4_lugares_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>');
        lugar_1_edit.value=loc;

                                    //mensaje datos cargados
        document.getElementById('datos_cargados_lug_4_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>').innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_4_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_4_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>').show();



      }

      else if(document.getElementById('res5_lugares_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>') && indice==5)
      {
        var lugar_1_edit=document.getElementById('res5_lugares_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>');
        lugar_1_edit.value=loc;

                                    //mensaje datos cargados
        document.getElementById('datos_cargados_lug_5_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>').innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_5_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_5_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>').show();



      }


   
    }
    








  $( function() 
  {
    var dialog, form,
 
      // From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
      emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
      name = $( "#name" ),
      email = $( "#email" ),
      password = $( "#password" ),
      allFields = $( [] ).add( name ).add( email ).add( password ),
      tips = $( ".validateTips" );
 
    function updateTips( t ) {
      tips
        .text( t )
        .addClass( "ui-state-highlight" );
      setTimeout(function() {
        tips.removeClass( "ui-state-highlight", 2500 );
      }, 500 );
    }
 
    function checkLength( o, n, min, max ) {
      if ( o.val().length > max || o.val().length < min ) {
        o.addClass( "ui-state-error" );
        updateTips( "Length of " + n + " must be between " +
          min + " and " + max + "." );
        return false;
      } else {
        return true;
      }
    }
 
    function checkRegexp( o, regexp, n ) {
      if ( !( regexp.test( o.val() ) ) ) {
        o.addClass( "ui-state-error" );
        updateTips( n );
        return false;
      } else {
        return true;
      }
    }
 
    function addUser_pid() 
    {
      $('#municipios_cargados').show();   
      var valid = true;
    
     // allFields.removeClass( "ui-state-error" );

     var arr_checks = document.getElementsByClassName("check_muni");
     var municipios_def_mun="Municipios,";


        for (i = 0; i < arr_checks.length; i++) 
      {

        if(arr_checks[i].checked == true)
        {
          municipios_def_mun+=i+",";
     
          
        }

      }

      /*

     var des_geo_municipios_Def_mun =document.getElementById("des_geo_municipios_list");
     des_geo_municipios_Def_mun.value= municipios_def_mun;
     */

      if(indice==1)
      {
        resultados_1_muni=municipios_def_mun;
        document.getElementById('datos_cargados_lug_1_pid').innerHTML ="Datos Cargados";
        var res1_mun =document.getElementById("res1_lugares_pid");
        res1_mun.value= municipios_def_mun;
        var element = document.getElementById("datos_cargados_lug_1_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_1_pid').show();
      
      }

      else if(indice==2)
      {
        resultados_2_muni=municipios_def_mun;
        document.getElementById('datos_cargados_lug_2_pid').innerHTML ="Datos Cargados";
        var res2_mun =document.getElementById("res2_lugares_pid");
        res2_mun.value= municipios_def_mun;
         var element = document.getElementById("datos_cargados_lug_2_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_2_pid').show();
      }
      else if(indice==3)
      {
        resultados_3_muni=municipios_def_mun;
        document.getElementById('datos_cargados_lug_3_pid').innerHTML ="Datos Cargados";
        var res3_mun =document.getElementById("res3_lugares_pid");
        res3_mun.value= municipios_def_mun;
         var element = document.getElementById("datos_cargados_lug_3_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_3_pid').show();
      }
      else if(indice==4)
      {
        resultados_4_muni=municipios_def_mun;
        document.getElementById('datos_cargados_lug_4_pid').innerHTML ="Datos Cargados";
        var res4_mun =document.getElementById("res4_lugares_pid");
        res4_mun.value= municipios_def_mun;
         var element = document.getElementById("datos_cargados_lug_4_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_4_pid').show();
      }
      else if(indice==5)
      {
        resultados_5_muni=municipios_def_mun;
        document.getElementById('datos_cargados_lug_5_pid').innerHTML ="Datos Cargados";
        var res5_mun =document.getElementById("res5_lugares_pid");
        res5_mun.value= municipios_def_mun;
         var element = document.getElementById("datos_cargados_lug_5_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_5_pid').show();
      }


      if(document.getElementById('res1_lugares_pid_'+id_select) && indice==1)
      {

        
        

        //guardar info localidades en input 
        var lugar_1_edit=document.getElementById('res1_lugares_pid_'+id_select);

       
        lugar_1_edit.value=municipios_def_mun;
        //mensaje datos cargados
        document.getElementById('datos_cargados_lug_1_pid_'+id_select).innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_1_pid_"+id_select);
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_1_pid_'+id_select).show();


      }

      else if(document.getElementById('res2_lugares_pid_'+id_select) && indice==2)
      {
        var lugar_1_edit=document.getElementById('res2_lugares_pid_'+id_select);
        lugar_1_edit.value=municipios_def_mun;
                                    //mensaje datos cargados
        document.getElementById('datos_cargados_lug_2_pid_'+id_select).innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_2_pid_"+id_select);
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_2_pid_'+id_select).show();

      }

      else if(document.getElementById('res3_lugares_pid_'+id_select) && indice==3)
      {
        var lugar_1_edit=document.getElementById('res3_lugares_pid_'+id_select);
        lugar_1_edit.value=municipios_def_mun;

                                    //mensaje datos cargados
        document.getElementById('datos_cargados_lug_3_pid_'+id_select).innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_3_pid_"+id_select);
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_3_pid_'+id_select).show();


      }

      else if(document.getElementById('res4_lugares_pid_'+id_select) && indice==4)
      {
        var lugar_1_edit=document.getElementById('res4_lugares_pid_'+id_select);
        lugar_1_edit.value=municipios_def_mun;

                                    //mensaje datos cargados
        document.getElementById('datos_cargados_lug_4_pid_'+id_select).innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_4_pid_"+id_select);
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_4_pid_'+id_select).show();

      }

      else if(document.getElementById('res5_lugares_pid_'+id_select) && indice==5)
      {
        var lugar_1_edit=document.getElementById('res5_lugares_pid_'+id_select);
        lugar_1_edit.value=municipios_def_mun;

                                    //mensaje datos cargados
        document.getElementById('datos_cargados_lug_5_pid_'+id_select).innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_5_pid_"+id_select);
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_5_pid_'+id_select).show();

      }



       // alert("limpiar la fiesta");
        for (i = 0; i < arr_checks.length; i++) 
      {

        if(arr_checks[i].checked == true)
        {
          arr_checks[i].checked=false;
         // alert(municipios_def_mun);
          
        }

      }

      dialog.dialog( "close" );
     // alert("datos concatenados");
      return valid;

     
    }


    function addEntidades_pid()
    {
   
      //alert("selecciono entidades");
      $('#entidades_cargados').show();   
      var valid = true;
    
     // allFields.removeClass( "ui-state-error" );

     var arr_checks = document.getElementsByClassName("check_enti");
     var municipios_def="Entidades,";

        for (i = 0; i < arr_checks.length; i++) 
      {

        if(arr_checks[i].checked == true)
        {
          municipios_def+=i+",";
         // alert(municipios_def);
          
        }

      }

     //var des_geo_entidades_Def =document.getElementById("des_geo_entidades_list");
     /*
     des_geo_entidades_Def.value= municipios_def;
     */  

      if(indice==1)
      {
        //alert("sonde tipo 1"+municipios_def);

        resultados_1=municipios_def;
        document.getElementById('datos_cargados_lug_1_pid').innerHTML ="Datos Cargados";
        var res1 =document.getElementById("res1_lugares_pid");
        res1.value= municipios_def;
         var element = document.getElementById("datos_cargados_lug_1_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_1_pid').show();

      }
      else if(indice==2)
      {
        resultados_2=municipios_def;
        document.getElementById('datos_cargados_lug_2_pid').innerHTML ="Datos Cargados";
        var res2 =document.getElementById("res2_lugares_pid");
        res2.value= municipios_def;
         var element = document.getElementById("datos_cargados_lug_2_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_2_pid').show();
      }
      else if(indice==3)
      {
        resultados_3=municipios_def;
        document.getElementById('datos_cargados_lug_3_pid').innerHTML ="Datos Cargados";
        var res3 =document.getElementById("res3_lugares_pid");
        res3.value= municipios_def;
         var element = document.getElementById("datos_cargados_lug_3_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_3_pid').show();
      }
      else if(indice==4)
      {
        resultados_4=municipios_def;
        document.getElementById('datos_cargados_lug_4_pid').innerHTML ="Datos Cargados";
        var res4 =document.getElementById("res4_lugares_pid");
        res4.value= municipios_def;
         var element = document.getElementById("datos_cargados_lug_4_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_4_pid').show();
      }
      else if(indice==5)
      {
        resultados_5=municipios_def;
        document.getElementById('datos_cargados_lug_5_pid').innerHTML ="Datos Cargados";
        var res5 =document.getElementById("res5_lugares_pid");
        res5.value= municipios_def;
         var element = document.getElementById("datos_cargados_lug_5_pid");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_5_pid').show();
      }



      if(document.getElementById('res1_lugares_pid_'+id_select) && indice==1)
      {
        //guardar info localidades en input 
        var lugar_1_edit=document.getElementById('res1_lugares_pid_'+id_select);

         // alert(lugar_1_edit.name);
          // alert(municipios_def);

          $("#res1_lugares_pid_"+id_select).val(municipios_def);


        lugar_1_edit.value = municipios_def;
        //mensaje datos cargados
        document.getElementById('datos_cargados_lug_1_pid_'+id_select).innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_1_pid_"+id_select);
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_1_pid_'+id_select).show();


      }

      else if(document.getElementById('res2_lugares_pid_'+id_select) && indice==2)
      {
        var lugar_1_edit=document.getElementById('res2_lugares_pid_'+id_select);
        lugar_1_edit.value=municipios_def;

                                    //mensaje datos cargados
        document.getElementById('datos_cargados_lug_2_pid_'+id_select).innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_2_pid_"+id_select);
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_2_pid_'+id_select).show();



      }

      else if(document.getElementById('res3_lugares_pid_'+id_select) && indice==3)
      {
        var lugar_1_edit=document.getElementById('res3_lugares_pid_'+id_select);
        lugar_1_edit.value=municipios_def;

                                    //mensaje datos cargados
        document.getElementById('datos_cargados_lug_3_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>').innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_3_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>");
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_3_pid_<?php echo($registro_2['id_indicador_asociado_def'])?>').show();



      }

      else if(document.getElementById('res4_lugares_pid_'+id_select) && indice==4)
      {
        var lugar_1_edit=document.getElementById('res4_lugares_pid_'+id_select);
        lugar_1_edit.value=municipios_def;

                                    //mensaje datos cargados
        document.getElementById('datos_cargados_lug_4_pid_'+id_select).innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_4_pid_"+id_select);
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_4_pid_'+id_select).show();


      }

      else if(document.getElementById('res5_lugares_pid_'+id_select) && indice==5)
      {
        var lugar_1_edit=document.getElementById('res5_lugares_pid_'+id_select);
        lugar_1_edit.value=municipios_def;

                                    //mensaje datos cargados
        document.getElementById('datos_cargados_lug_5_pid_'+id_select).innerHTML ="Datos Cargados";
        var element = document.getElementById("datos_cargados_lug_5_pid_"+id_select);
        element.classList.remove("error_mensaje");
        element.classList.add("datos_cargados_lugar_resultados"); 
        $('#datos_cargados_lug_5_pid_'+id_select).show();



      }



  
      dialog_entidades_pid.dialog( "close" );
     // alert("datos concatenados");
      return valid;
    }
  //Cuadro de dialogo de municicpios
    dialog = $( "#dialog-form_pid" ).dialog({
        autoOpen: false,
        height: 700,
        width: 600,
        modal: true,
        buttons: {
          "Guardar": addUser_pid,

          Cancel: function() {
            dialog.dialog( "close" );
          }
        },
        close: function() {
          form[ 0 ].reset();
          allFields.removeClass( "ui-state-error" );
        }
    });
 
    form = dialog.find( "form" ).on( "submit", function( event ) {
      event.preventDefault();
      addUser_pid();
    });

    //Cuadro de dialogo de entidades
    dialog_entidades_pid = $( "#dialog-form-entidades_pid" ).dialog({
        autoOpen: false,
        height: 700,
        width: 600,
        modal: true,
        buttons: {
          "Guardar": addEntidades_pid,

          Cancel: function() {
            dialog_entidades_pid.dialog( "close" );
          }
        },
        close: function() {
          form[ 0 ].reset();
          allFields.removeClass( "ui-state-error" );
        }
    });
 
    form = dialog_entidades_pid.find( "form" ).on( "submit", function( event ) {
      event.preventDefault();
      addEntidades_pid();
    });



    // fin entidades
 


      $('#es_posible_desagregar_geograficamente_def').change(function()
      { 

      if($(this).val() == "Municipios")
      {
         dialog.dialog( "open" );
         $('#upload_tabla_localidades').hide();
          $('#entidades_cargados').hide();
          $('#municipios_cargados').hide();
          // document.getElementById('des_geo_entidades').value="0" ; 

      }

      else if($(this).val() == "Localidades" ){
         $('#upload_tabla_localidades').show();
         $('#municipios_cargados').hide();//ocultar mensaje de municipiuos cargardos
         $('#entidades_cargados').hide();
         
      }

      else if($(this).val() == "Otras entidades"){
         dialog_entidades_pid.dialog( "open" );

         $('#upload_tabla_localidades').hide();
         $('#municipios_cargados').hide();  //ocultar mensaje de municipiuos cargardos
         $('#entidades_cargados').hide();
         //document.getElementById('des_geo_municipios_list').value="0" ; 
      }

      else if($(this).val() == "Estado de Hidalgo"){
         $('#upload_tabla_localidades').hide();
         $('#municipios_cargados').hide();  //ocultar mensaje de municipiuos cargardos
         $('#entidades_cargados').hide();
      }

      });

  } );

</script>

<!-- DIALOGO CONFIRMACION-->
<script>

  $( function()
    {
      $( "#dialog-message-adjuntar-localidades_pid" ).dialog({
        autoOpen: false,
        modal: true,
        height: 350,
        width: 450,
        buttons: {
          Ok: function() {
            $( this ).dialog( "close" );
          }
        }
      });
    } );



  
    $( function()
    {
      $( "#dialog-message-lugar_pid" ).dialog({
        autoOpen: false,
        modal: true,
        height: 350,
        width: 450,
        buttons: {
          Ok: function() {
            $( this ).dialog( "close" );
          }
        }
      });
    } );



   $( function()
    {
      $( "#dialog-message" ).dialog({
        autoOpen: false,
        modal: true,
        height: 350,
        width: 450,
        buttons: {
          Ok: function() {
            $( this ).dialog( "close" );
          }
        }
      });
    } );


    $('#plan_trabajo_def').change(function()
    { 
        

      if($(this).val() == "No")
      {
      $( "#dialog-message" ).dialog( "open" );

      }
    });
    //si se checa la opcion otro adjuntar el docuemento q necesitan
    $('#plan_trabajo_def').change(function()
    { 
        

      if($(this).val() == "No")
      {
      $( "#dialog-message" ).dialog( "open" );

      }
    });

    $('#check_evi_pid_Otro').change(function()
    { 
      //alert("holasss")
          
         var check_otros = document.getElementById("check_evi_pid_Otro");

  
        if(check_otros.checked == true)
        {
           $('#tipo_evidencia_otro_pid').show();
        }
        else
        {
          $('#tipo_evidencia_otro_pid').hide();

        }


    });
    /*

    $('#se_desagrego_geograficamente_def').change(function()
    { 
     
        // var check_otros = document.getElementById("check_evi_pid_Otro_pid");

  
         if($(this).val() == "Si")
        {
           $('#si_existe_informacion_acciones').show();
        }
        else
        {
          $('#si_existe_informacion_acciones').hide();

        }


    });
    */

  $('#logros_en_indicador').change(function()
    { 
     

         if($(this).val() == "Si")
        {
           $('#bullet_Del_logro').show();
        }
        else if($(this).val() == "No")
        {
          $('#bullet_Del_logro').hide();
        }


    });


  $('#premios_recono').change(function()
    { 
     

         if($(this).val() == "Si")
        {
           $('#premios_reconocimientos').show();
        }
        else if($(this).val() == "No")
        {
          $('#premios_reconocimientos').hide();
        }


    });

  $('#tipo_indicador_def_pid').change(function()
    { 

    if($(this).val() == "Otro")
    {
    $('#otro_tipo_indicador_pid').show();

    }
    if($(this).val() != "Otro"){
    $('#otro_tipo_indicador_pid').hide();
    }
    });


   function tipo_evidencia_get_values()
    {
      var arr_checks = document.getElementsByClassName("check_type_evi_pid");
     var tipo_evidencia_def="";

        for (i = 0; i < arr_checks.length; i++) 
      {

        if(arr_checks[i].checked == true)
        {
          tipo_evidencia_def+=arr_checks[i].name+",";
        
          
        }

      }
     var tipo_evi_ =document.getElementById("tipo_evidencia_def_list_pid");
     tipo_evi_.value= tipo_evidencia_def;
  
    }

     $('#existe_expediente_doc_pid').change(function(){ 
        if($(this).val() == "Si")

          {
          $('#no_existe_expediente_doc').hide();
          $('#existe_exp_doc').show();
      
          }
          if($(this).val() == "No"){
          $('#existe_exp_doc').hide();
          $('#no_existe_expediente_doc').show();

          }


      });


</script>


<!-- cuadros de dialogo selectrs-->

