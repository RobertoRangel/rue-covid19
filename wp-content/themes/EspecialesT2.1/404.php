<?php get_header(); ?>

  <section>
    <div class="pagenation-holder">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h3>Error 404</h3>
          </div>
          <div class="col-md-6 text-right">
            <div class="pagenation_links"><a href="index.html">Inicio</a><i> / </i> Error 404 </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--end section-->
  <div class="clearfix"></div>
  
 <section class="sec-padding">
    <div class="container">
      <div class="row">
      
    <div class="error_holder">
        <h1 class="uppercase title text-orange-2">404</h1>
        <br/>
        <h2 class="uppercase">Uups... PÁGINA NO ENCONTRADA!</h2>
        <p>Lo sentimos, intente utilizar nuestro formulario de búsquedas.</p>
        <br/>
        <br/>
        <br/>
        <div class="error-search-box">
          <?php dynamic_sidebar( 'Buscador' ); ?>
        </div>
      </div>
      
      </div>
    </div>
  </section>
  <!--end item -->
  <div class="clearfix"></div>

<?php get_footer(); ?>