<style>
.aLikeButtonsi {
   display: block;
   width: 400px;
   height: 28px;
   background: yellowgreen;
   text-align: center;
   border-radius: 5px;
   color: white;
   font-weight: bold;
}
.aLikeButtonno {
   display: block;
   width: 400px;
   height: 28px;
   background: red;
   text-align: center;
   border-radius: 5px;
   color: white;
   font-weight: bold;
}
.aLikeButtonotro {
   display: block;
   width: 400px;
   height: 28px;
   background: yellow;
   text-align: center;
   border-radius: 5px;
   color: white;
   font-weight: bold;
}



.w3-bar{width:100%;overflow:hidden}.w3-center 
.w3-justify{text-align:justify!important}.w3-center{text-align:center!important}
.w3-green,.w3-hover-green:hover{color:#fff!important;background-color:#4CAF50!important}
.w3-blue,.w3-hover-blue:hover{color:#fff!important;background-color:#2196F3!important}
.w3-red,.w3-hover-red:hover{color:#fff!important;background-color:#f44346!important}/* Los tipos de colores */

.w3-container:after,.w3-container:before,.w3-panel:after,.w3-panel:before,.w3-row:after,.w3-row:before,.w3-row-padding:after,.w3-row-padding:before,
.w3-cell-row:before,.w3-cell-row:after,.w3-clear:after,.w3-clear:before,.w3-bar:before,.w3-bar:after{content:"";display:table;clear:both}
.w3-col,.w3-half,.w3-third,.w3-twothird,.w3-threequarter,.w3-quarter{float:left;width:100%}


.w3-light-grey,.w3-hover-light-grey:hover,.w3-light-gray,.w3-hover-light-gray:hover{color:#000!important;background-color:#f1f1f1!important}
.w3-dark-grey,.w3-hover-dark-grey:hover,.w3-dark-gray,.w3-hover-dark-gray:hover{color:#fff!important;background-color:#616161!important}
.w3-pale-red,.w3-hover-pale-red:hover{color:#000!important;background-color:#ffdddd!important}


</style>


<?php
/**
* @author Eloy Monter Hernandez <eloymonter@gmail.com>
* @author Eliel Trigueros Hernandez <izzyoxf@gmail.com>
* @Versión 1, Septiembre/2017
*/

get_header(); ?>

<?php
$WP_User = wp_get_current_user();
$array = $WP_User->caps;
	while ($caps = current($array)) {
		if ( (key($array) == 'evaluador_eventos') OR (key($array) == 'administrator') ) {
		}else{
			
		}
		next($array);
		break;
	}
if ( have_posts() ) {
    while ( have_posts() ) {
		// Recuperar entrada
        the_post(); ?>
            
            <!-- ACF LABELS -->
            <?php
			
			// Dependencia - Select
            $AE0_field = get_field_object('dependencia');$AE0_value = $AE0_field['value'];$AE0_label = $AE0_field['choices'][ $AE0_value ];
			
			// Propósito del evento
			$AE1_field = get_field_object('prop_ev');
			$AE1_value = $AE1_field['value'];
			$AE1_label = $AE1_field['choices'][ $AE1_value ];
			// Operación Valor * ponderación
            $val1 = $AE1_value * 0.2;
			
			// Tipo de evento
			$AE2_field = get_field_object('tip_ev');$AE2_value = $AE2_field['value'];$AE2_label = $AE2_field['choices'][ $AE2_value ];
			// Operación Valor * ponderación
            $val2 = $AE2_value * 0.1;
			
			// Tipo de público  (Check)
			$AE3_field = get_field_object('tip_pub');$AE3_values = $AE3_field['value'];
							
			// Costo del evento  (Check)
			$AE4_field = get_field_object('cost_event');$AE4_values = $AE4_field['value'];
			
			// Costos indirectos  (Check)
			$AE5_field = get_field_object('cost_event_in');$AE5_values = $AE5_field['value'];
			
			// Vinculación agenda legislativa (Radio)
			$AE6_field = get_field_object('vinc_leg');$AE6_values = $AE6_field['value'];
			// Ponderación
			$val7= $AE6_values*.05;
			
			// Vinculación agenda municipalista (Radio)
			$AE7_field = get_field_object('vinc_munic');$AE7_values = $AE7_field['value'];
			// Ponderación
			$val8 = $AE7_values*.05;
			
			// Motivos de asistencia
			$MA_field = get_field_object('mot_asis');$MA_value = $MA_field['value'];$MA_label = $MA_field['choices'][ $MA_value ];
			$MAa_field = get_field_object('mot_asis_a');$MAa_value = $MAa_field['value'];$MAa_label = $MAa_field['choices'][ $MAa_value ];
			$MAb_field = get_field_object('mot_asis_b');$MAb_value = $MAb_field['value'];$MAb_label = $MAb_field['choices'][ $MAb_value ];
			$MAc_field = get_field_object('mot_asis_c');$MAc_value = $MAc_field['value'];$MAc_label = $MAc_field['choices'][ $MAc_value ];
			$MAd_field = get_field_object('mot_asis_d');$MAd_value = $MAd_field['value'];$MAd_label = $MAd_field['choices'][ $MAd_value ];
			$MAe_field = get_field_object('mot_asis_e');$MAe_value = $MAe_field['value'];$MAe_label = $MAe_field['choices'][ $MAe_value ];
			$MAf_field = get_field_object('mot_asis_f');$MAf_value = $MAf_field['value'];$MAf_label = $MAf_field['choices'][ $MAf_value ];
			$MAg_field = get_field_object('mot_asis_g');$MAg_value = $MAg_field['value'];$MAg_label = $MAg_field['choices'][ $MAg_value ];		
			// Ponderación (suma de motivos/checked options)*100  * 0.1
			$_pon_prom = ($MAa_value+$MAb_value+$MAc_value+$MAd_value+$MAe_value+$MAf_value+$MAg_value/7)*100;
			$val4 = $_pon_prom * 0.1;			
			
			// Rangos de interpretación
			$val3 = get_field('pob_imp') * 0.1;
			
            ?>
            
<!--------- Interfaz de salida -->
            

	<center><div class="content" style="border: solid black 2px; width: 50%;" >
		<h2><p style="background-color: #f2dede;">
Evaluación: <b>Evento No viable</b></p>

Estatus: <b>
	
<?php
$aprobado = 0;	
$sincalificar = 0;	
$evaluacion1=get_field_object('evaluacion1')['value'];
$evaluacion2=get_field_object('evaluacion2')['value'];
$evaluacion3=get_field_object('evaluacion3')['value'];	
if($evaluacion1 == 0 || empty($evaluacion1)){
	$sincalificar++;
}
if($evaluacion2 == 0 || empty($evaluacion2)){
	$sincalificar++;
}
if($evaluacion3 == 0 || empty($evaluacion3)){
	$sincalificar++;
}

if($evaluacion1 == 1 || $evaluacion1 == 2){
	$aprobado++;
}
if($evaluacion2 == 1 || $evaluacion2 == 2){
	$aprobado++;
}
if($evaluacion3 == 1 || $evaluacion3 == 2){
	$aprobado++;
}
//echo "aprobado ".$aprobado;	
//echo "sin claificar ".$sincalificar;	

if($aprobado == 1 AND $sincalificar <= 1){
	echo "<span class='aLikeButtonotro'>Se recomienda el uso de medios alternativos</span>";
}elseif($aprobado >= 2 AND $sincalificar <= 1){
	echo "<span class='aLikeButtonsi'>El evento ha sido aprobado y agendado</span>";
}else{
	echo "<span class='aLikeButtonno'>El evento no ha sido aprobado</span>";
}
?>
	</b>
		</center></h2>
	</div>
	<div class="content">
	<!-- DATOS ESTATICOS -->
    <h2><?php the_title(); ?></h2>
    
	<div class="ag_bar">
        <span>
            <b>Dependencia:</b> <?php echo $AE0_label ?><b style="margin-left:45px;">Tipo de evento:</b> <?php echo $AE2_label;?> <br>
            <b>Propósito del evento:</b> <?php echo $AE1_label;?><br>
            <b>Objetivo del evento:</b> <?php the_field('obj_ev'); ?><br><br>
        </span>
    </div>
            
           
  <input id="tab1" type="radio" name="tabs" checked>
  <label for="tab1">Resumen</label>
    
  <input id="tab2" type="radio" name="tabs">
  <label for="tab2">Alineación</label>
    
  <input id="tab3" type="radio" name="tabs" >
  <label for="tab3">Costos y asistentes</label>
  
  <input id="tab4" type="radio" name="tabs">
  <label for="tab4">Evaluación</label>
    
  <section id="content1">

  	


  
  <div class="ag_left">
    <table>
        <tr><td>Fecha</td><td><?php the_field('fech_ev'); ?></td></tr>
        <tr><td>Hora de inicio</td><td><?php the_field('hor_ev'); ?></td></tr>
        <tr><td>Hora de fin</td><td><?php the_field('hor_ev_a'); ?></td></tr>
        <tr><td>Lugar</td><td><?php the_field('nom_esp'); ?></td></tr>
        <tr><td>Municipio</td>
        <td><?php the_field('mun_ev'); ?></td></tr>
        <tr><td>Código Postal</td><td><?php the_field('c_p'); ?></td></tr>
        <tr><td>Domicilio</td><td><?php the_field('domicilio'); ?></td></tr>
    </table>
  </div>
  <div class="ag_right">
	  <?php  $location = get_field('mapa_g'); if( !empty($location) ):?>
      <div class="acf-map" style="height: 350px;"><div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div></div>
      <?php endif; ?>
  </div>
  
 <br> 
 <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> 
  			
  
  </section><!-- Content 1 -->
    
  <section id="content2">
  
            <table> <!-- DATOS DEL EVENTO -->
                <tr><td>Población estimada a impactar: <b><?php the_field('pob_imp'); ?></b></td>
                <td>Población que asiste al evento (Aforo): <b><?php the_field('pob_afor'); ?></b></td></tr>
                <?php if( get_field('pob_ex_s') == '1' ): ?>
                <tr><td>¿Por qué excede la población total?</td>
                    <td><?php the_field('may_pob_hgo'); ?></td>
                </tr><?php endif; ?>
                
                <?php
                if( get_field('me_imp') == '0.5' ):?>
                <tr><td>¿Hay algún otro medio para lograr el impacto?</td><td>
                <?php $AE8_field = get_field_object('med_alt');$AE8_values = $AE8_field['value'];
					if( $AE8_values ):
							foreach( $AE8_values as $AE8_value ):
								echo $AE8_field['choices'][ $AE8_value ].'<br>';
								if ($AE8_field['choices'][ $AE8_value ] == 'Otro ¿cuál?' ): the_field('med_alt_a'); endif;
							endforeach;
					endif; ?>
                </td></tr>
                <?php endif;?>
                
                <tr><td>Indicador táctico asociado</td><td>
                <?php switch($AE0_value)
					{
						case 1: $In_field = get_field_object('ind_as');$In_value = $In_field['value'];$In_label = $In_field['choices'][ $In_value ]; echo "que imprime".$In_label;
						case 2: $Ina_field = get_field_object('ind_as_a');$Ina_value = $Ina_field['value'];$Ina_label = $Ina_field['choices'][ $Ina_value ]; echo $Ina_label;
						case 3: $Inb_field = get_field_object('ind_as_b');$Inb_value = $Inb_field['value'];$Inb_label = $Inb_field['choices'][ $Inb_value ]; echo $Inb_label;
						case 4: $Inc_field = get_field_object('ind_as_c');$Inc_value = $Inc_field['value'];$Inc_label = $Inc_field['choices'][ $Inc_value ]; echo $Inc_label;
						case 5: $Ind_field = get_field_object('ind_as_d');$Ind_value = $Ind_field['value'];$Ind_label = $Ind_field['choices'][ $Ind_value ]; echo $Ind_label;
						case 6: $Ine_field = get_field_object('ind_as_e');$Ine_value = $Ine_field['value'];$Ine_label = $Ine_field['choices'][ $Ine_value ]; echo $Ine_label;
						case 7: $Inf_field = get_field_object('ind_as_f');$Inf_value = $Inf_field['value'];$Inf_label = $Inf_field['choices'][ $Inf_value ]; echo $Inf_label;
						case 8: $Ing_field = get_field_object('ind_as_g');$Ing_value = $Ing_field['value'];$Ing_label = $Ing_field['choices'][ $Ing_value ]; echo $Ing_label;
						case 9: $Inh_field = get_field_object('ind_as_h'); $Inh_value = $Inh_field['value'];$Inh_label = $Inh_field['choices'][ $Inh_value ]; echo $Inh_label;
						case 10: $Ini_field = get_field_object('ind_as_i');$Ini_value = $Ini_field['value'];$Ini_label = $Ini_field['choices'][ $Ini_value ]; echo $Ini_label;
						case 11: $Inj_field = get_field_object('ind_as_j');$Inj_value = $Inj_field['value'];$Inj_label = $Inj_field['choices'][ $Inj_value ]; echo $Inj_label;
						case 12: $Ink_field = get_field_object('ind_as_k');$Ink_value = $Ink_field['value'];$Ink_label = $Ink_field['choices'][ $Ink_value ]; echo $Ink_label;
						case 13: $Inl_field = get_field_object('ind_as_l');$Inl_value = $Inl_field['value'];$Inl_label = $Inl_field['choices'][ $Inl_value ]; echo $Insal_label;
						case 14: $Inm_field = get_field_object('ind_as_m');$Inm_value = $Inm_field['value'];$Inm_label = $Inm_field['choices'][ $Inm_value ]; echo $Inm_label;
						case 15: $Inn_field = get_field_object('ind_as_n');$Inn_value = $Inn_field['value'];$Inn_label = $Inn_field['choices'][ $Inn_value ]; echo $Inn_label;
						case 16: $Insal_field = get_field_object('ind_as_sal');$Insal_value = $Insal_field['value'];$Insal_label = $Insal_field['choices'][ $Insal_value ]; echo $Ino_label;
						case 17: $Ino_field = get_field_object('ind_as_o');$Ino_value = $Ino_field['value'];$Ino_label = $Ino_field['choices'][ $Ino_value ]; echo $Inp_label;
						case 18: $Inp_field = get_field_object('ind_as_p');$Inp_value = $Inp_field['value'];$Inp_label = $Inp_field['choices'][ $Inp_value ]; echo $Inq_label;
						case 19: $Inq_field = get_field_object('ind_as_q');$Inq_value = $Inq_field['value'];$Inq_label = $Inq_field['choices'][ $Inq_value ]; echo $Inr_label;
						case 20: $Inr_field = get_field_object('ind_as_r');$Inr_value = $Inr_field['value'];$Inr_label = $Inr_field['choices'][ $Inr_value ]; echo $Ins_label;
						case 21: $Ins_field = get_field_object('ind_as_s');$Ins_value = $Ins_field['value'];$Ins_label = $Ins_field['choices'][ $Ins_value ]; echo $Ins_label;
						break;
					}
					// Operación de indicador táctico asociado, multiplicar por el ponderador 0.1
					
				?>
                </td></tr>
                <tr><td>ODS asociado</td><td><?php the_field('ods'); ?></td></tr>
                <tr><td>Objetivo estratégico asociado</td><td>
					<?php switch($AE0_value) {
                        case 1: the_field('obj_est'); case 2: the_field('obj_est_a');case 3: the_field('obj_est_b');case 4: the_field('obj_est_c');case 5: the_field('obj_est_d');
						case 6: the_field('obj_est_e');case 7: the_field('obj_est_f');case 8: the_field('obj_est_g');case 9: the_field('obj_est_h');case 10: the_field('obj_est_i');
						case 11: the_field('obj_est_j');case 12: the_field('obj_est_k');case 13: the_field('obj_est_l');case 14: the_field('obj_est_m');case 15: the_field('obj_est_n');
						case 16: the_field('obj_est_o');case 17: the_field('obj_est_p');case 18: the_field('obj_est_q');case 19: the_field('obj_est_r');case 20: the_field('obj_est_s');
                        break;}?>
                </td></tr>
                <tr><td>Objetivo general del PED asociado</td><td>
					<?php switch($AE0_value) {
						case 1: the_field('ob_gral');case 2: the_field('ob_gral_a');case 3: the_field('ob_gral_b');case 4: the_field('ob_gral_c');case 5: the_field('ob_gral_d');
						case 6: the_field('ob_gral_e');case 7: the_field('ob_gral_f');case 8: the_field('ob_gral_g');case 9: the_field('ob_gral_h');case 10: the_field('ob_gral_i');
						case 11: the_field('ob_gral_j');case 12: the_field('ob_gral_k');case 13: the_field('ob_gral_l');case 14: the_field('ob_gral_m');case 15: the_field('ob_gral_n');
						case 16: the_field('ob_gral_o');case 17: the_field('ob_gral_p');case 18: the_field('ob_gral_q');case 19: the_field('ob_gral_r');case 20: the_field('ob_gral_s');
					break;}?>
                </td></tr>
                
                <tr><td>Objetivo general sectorial o especial</td><td>
					<?php switch($AE0_value) {
						case 1: the_field('ob_gral_aa');case 2: the_field('ob_s_e_a');case 3: the_field('ob_s_e_b');case 4: the_field('ob_s_e_c');case 5: the_field('ob_s_e_d');
						case 6: the_field('ob_s_e_e');case 7: the_field('ob_s_e_f');case 8: the_field('ob_s_e_g');case 9: the_field('ob_s_e_h');case 10: the_field('ob_s_e_i');
						case 11: the_field('ob_s_e_j');case 12: the_field('ob_s_e_k');case 13: the_field('ob_s_e_l');case 14: the_field('ob_s_e_m');case 15: the_field('ob_s_e_n');
						case 16: the_field('ob_s_e_o');case 17: the_field('ob_s_e_p');case 18: the_field('ob_s_e_q');case 19: the_field('ob_s_e_r');case 20: the_field('ob_s_e_s');
					break;}?>
                </td></tr>
                <tr><td>¿Está vinculado a la Agenda Legislativa?</td><td><?php the_field('vinc_leg'); ?></td></tr>
                <tr><td>¿Está vinculado a la Agenda Municipalista?</td><td><?php the_field('vinc_munic'); ?></td></tr>
            </table>
  
  </section><!-- content 2 -->
    
<section id="content3">
  
<h3>Asistentes</h3><!-- AISTENTES -->
				<table border="0"> 
                		<tr><th>Asistentes</th><th>Motivo de asistencia</th></tr>
                
				<?php
				
				if( $AE3_values ): ?>
                        <?php foreach( $AE3_values as $AE3_value ): ?>
                        <tr>
                            <td><?php echo $AE3_field['choices'][ $AE3_value ]; ?></td>
                            <?php if ($AE3_field['choices'][ $AE3_value ] == 'Niños y adolescentes' ): echo '<td>'.$MA_label.'</td>'; endif;?></td>
                            <?php if ($AE3_field['choices'][ $AE3_value ] == 'Mujeres' ): echo '<td>'.$MAa_label.'</td>'; endif;?></td>
                            <?php if ($AE3_field['choices'][ $AE3_value ] == 'Indígenas' ): echo '<td>'.$MAb_label.'</td>'; endif;?></td>
                            <?php if ($AE3_field['choices'][ $AE3_value ] == 'Funcionarios federales o de otras entidades' ): echo '<td>'.$MAc_label.'</td>'; endif;?></td>
                            <?php if ($AE3_field['choices'][ $AE3_value ] == 'Funcionarios locales o municipales' ): echo '<td>'.$MAd_label.'</td>'; endif;?></td>
                            <?php if ($AE3_field['choices'][ $AE3_value ] == 'Población en general' ): echo '<td>'.$MAe_label.'</td>'; endif;?></td>
                            <?php if ($AE3_field['choices'][ $AE3_value ] == 'Otros  ¿quiénes?' ): echo '<td>'.$MAf_label.'</td>'; endif;?></td>
                            <?php if ($AE3_field['choices'][ $AE3_value ] == 'Niños y adolescentes' ): echo '<td>'.$MAg_label.'</td>'; endif;?></td>
                        </tr>
                        <?php endforeach; ?>
                <?php endif; ?>
                
                <!-- VALORES DE ASISTENCIA -->
                <script type="text/javascript">
					/*
				var checkboxes = document.getElementById("tip_pub").checkbox;
				var cont1 = 0; 
					for (var x=0; x < checkboxes.length; x++) {
					 if (checkboxes[x].checked) {
					  cont = cont + 1;
					 }
				}
				
				document.write(cont1);
				*/
				</script>
                
                </table>
                <h3>Costos directos</h3><!-- COSTOS DIRECTOS -->
                <table border="0"> 
                <?php
                	$cost_a = get_field('cost_event_a');
					$cost_b = get_field('cost_event_b');
					$cost_c = get_field('cost_event_c');
					$cost_d = get_field('cost_event_d');
					$cost_e = get_field('cost_event_e');
					$cost_f = get_field('cost_event_f');
					$cost_g = get_field('cost_event_g');
					$cost_total = $cost_a+$cost_b+$cost_c+$cost_d+$cost_e+$cost_f+$cost_g;
				?>
                
                		<tr><th>Concepto</th><th>Costo</th></tr>

				<?php if( $AE4_values ): ?>
                        <?php foreach( $AE4_values as $AE4_value ): ?>
                        <tr>
                            <td><?php echo $AE4_field['choices'][ $AE4_value ]; ?></td>
                            <?php if ($AE4_field['choices'][ $AE4_value ] == 'Renta del espacio y locación' ): echo '<td>'.'$'.number_format($cost_a,2).'</td>'; endif;?>
                            <?php if ($AE4_field['choices'][ $AE4_value ] == 'Papelería' ): echo '<td>'.'$'.number_format($cost_b,2).'</td>'; endif;?>
                            <?php if ($AE4_field['choices'][ $AE4_value ] == 'Difusión y promoción' ): echo '<td>'.'$'.number_format($cost_c,2).'</td>'; endif;?>
                            <?php if ($AE4_field['choices'][ $AE4_value ] == 'Equipamiento técnico, sonido, proyección, decoración, etc.' ): echo '<td>'.'$'.number_format($cost_d,2).'</td>'; endif;?>
                            <?php if ($AE4_field['choices'][ $AE4_value ] == 'Animación del evento' ): echo '<td>'.'$'.number_format($cost_e,2).'</td>'; endif;?>
                            <?php if ($AE4_field['choices'][ $AE4_value ] == 'Honorarios por servicios profesionales' ): echo '<td>'.'$'.number_format($cost_f,2).'</td>'; endif;?>
                            <?php if ($AE4_field['choices'][ $AE4_value ] == 'Otro, ¿cuál?' ): echo '<td>'.'$'.number_format($cost_g,2).'</td>'; endif;?>
                        </tr>
                        <?php endforeach; ?>
                <?php endif; ?>
                
                		<td><b>Costo total</b></td><td><?php echo '$'.number_format($cost_total,2) ?></td>
                
                </table>
                <h3>Costos indirectos</h3><!-- COSTOS INDIRECTOS -->
                <table border="0"> 
                <?php
                	$cost_in_a = get_field('cost_ev_ind_a');
					$cost_in_b = get_field('cost_ev_ind_b');
					$cost_in = get_field('cost_ev_ind');
					$cost_in_total = $cost_in_a+$cost_in_b+$cost_in;
				?>
                
                		<tr><th>Concepto</th><th>Costo</th></tr>

				<?php if( $AE5_values ): ?>
                        <?php foreach( $AE5_values as $AE5_value ): ?>
                        <tr>
                            <td><?php echo $AE5_field['choices'][ $AE5_value ]; ?></td>
                            <?php if ($AE5_field['choices'][ $AE5_value ] == 'Traslados' ): echo '<td>'.'$'.number_format($cost_in,2).'</td>'; endif;?>
                            <?php if ($AE5_field['choices'][ $AE5_value ] == 'Alimentos y viáticos' ): echo '<td>'.'$'.number_format($cost_in_a,2).'</td>'; endif;?>
                            <?php if ($AE5_field['choices'][ $AE5_value ] == 'Otro, ¿cuál?' ):echo '<td>'.'$'.number_format($cost_in_b,2).'</td>'; endif;?>
                        </tr>
                        <?php endforeach; ?>
                <?php endif; ?>
                
                		<td><b>Costo total</b></td><td><?php echo '$'.number_format($cost_in_total,2) ?></td>
                
                </table>
                
                <?php 
				$asistencia = get_field('pob_afor');
				$total_asis = ($cost_total+$cost_in_total)/$asistencia;
				?>
                <span>Costo por asistente: <?php echo '$'.number_format($total_asis,2) ?></span><br>
                
                
                <!-- COSTO POT MINUTO -->
                <?php
                $h_inicio = get_field('hor_ev')*1440/24;
				$h_final = get_field('hor_ev_a')*1440/24;
				$cost_min = ($cost_total+$cost_in_total)/($h_final-$h_inicio);
				?>

                <span>Costo por minuto: <?php echo '$'.number_format($cost_min,2); ?></span>  

                <br>
<br>
	  <div class="w3-light-grey">
	    <div id="myBar" class=" w3-container w3-center " style="height:24px;width:0"></div>
	  </div>
<br>
<br>

            


    
</section>

 

<section id="content4">
	<span><h2>Evaluación:<b> 



		  <!--   Barra de progreso--> 



				<!--   Fin Barra de progreso--> 
	<?php 


	//1		*	3 ¿Cuál es el propósito del evento? ----> $val1
	//2		*	4 Tipo de evento ----> $val2
	//3		6	Población estimada a impactar/ Población inmersa en el tema del evento ----> $val3 (ESTA MAL POR QUE NO ES UN SELECT)
	//4		*	10 ¿Por qué necesariamente tienen que asistir? ----> ($MA_value * 0.1)
	//5		17 Costo por invitado al evento ---> $total_asis (HAY QUE VER EN QUE RANGO SE ENCUENTRA Y LUEGO MULTIPLICARLO POR SU PONDERACION)
	//6		18 Indicador táctico asociado (ESTE ESTA COMPLICADO... PRIMERO DEBES SABER A QUE SECRETARIA OPERTENECE LUEGO IR AL CAMPO DEL INDICADOR QUE CORESPONDE Y SACAR SU VALUE LUEGO MULTIPLICARLO POR LA PONDERACION)
	//7		*	23 ¿El evento implica una vinculación con la agenda legislativa?  ----> $val7
	//8		*	24 ¿El evento implica una vinculación con la agenda municipalista? ----> $val8
	//9		32 Costo por minuto $cost_min (HAY QUE VER EN QUE RANGO SE ENCUENTRA Y LUEGO MULTIPLICARLO POR SU PONDERACION)
	//10	*	33 ¿Hay algún otro medio para realizar el evento? ----> $val10
	

		echo ($val1 + $val2 + $val4 + $val7 + $val8 + $val10); 
	?>
	</h2></b></span>
</section>

            </div><!-- Cierre de contenido -->
     
            <!-- Contenido del post --> 
            
            <?php //the_content(); ?>
        
        </div><!-- Contenido -->
 
    <?php }
}
?>

<style type="text/css">
@import url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

.ag_left, .ag_right{width:50%; float:left; display:block; padding:16px;}
tr td:first-child{width:40%;}
tr:nth-child(odd){background:#FAFAFA;}

section {
  display: none;
  padding: 20px 0 0;
  border-top: 1px solid #ddd;
}

input {
  display: none;
}

label {
  display: inline-block;
  margin: 0 0 -1px;
  padding: 15px 25px;
  font-weight: 600;
  text-align: center;
  color: #bbb;
  border: 1px solid transparent;
}

label:before {
  font-family: fontawesome;
  font-weight: normal;
  margin-right: 10px;
}

label[for*='1']:before { content: '\f073'; }
label[for*='2']:before { content: '\f1fe'; }
label[for*='3']:before { content: '\f0c0'; }
label[for*='4']:before { content: '\f295'; }

label:hover {
  color: #888;
  cursor: pointer;
}

input:checked + label {
  color: #555;
  border: 1px solid #ddd;
  border-top: 3px solid #71B631;
  border-bottom: 1px solid #fff;
}

#tab1:checked ~ #content1,
#tab2:checked ~ #content2,
#tab3:checked ~ #content3,
#tab4:checked ~ #content4 {
  display: block;


@media screen and (max-width: 650px) {
  label {
    font-size: 0;
  }
  label:before {
    margin: 0;
    font-size: 18px;
  }
}

@media screen and (max-width: 400px) {
  label {
    padding: 15px;
  }
}
</style>




<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD0il894cCjGAq2J-Mt5AHEYaOIC6tC0zE"></script>-->
<script type="text/javascript">
(function($) {

/*
*  new_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/

function new_map( $el ) {
	
	// var
	var $markers = $el.find('.marker');
	
	
	// vars
	var args = {
		zoom		: 16,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP
	};
	
	
	// create map	        	
	var map = new google.maps.Map( $el[0], args);
	
	
	// add a markers reference
	map.markers = [];
	
	
	// add markers
	$markers.each(function(){
		
    	add_marker( $(this), map );
		
	});
	
	
	// center map
	center_map( map );
	
	
	// return
	return map;
	
}

/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/

function add_marker( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map
	});

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {

			infowindow.open( map, marker );

		});
	}

}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/           



	function move( valor) 
	{

	  var elem = document.getElementById("myBar");  

		if(valor<=25)
		{ 	//elem.classList.remove("w3-red");
			elem.classList.add("w3-red");
		} 
		if(valor>25 && valor<=50  )
		{	//elem.classList.remove("w3-red");		//rangos de colores
			elem.classList.add("w3-blue");			//add remove class color
		} 
		if(valor>50 && valor<75  )
		{	//elem.classList.remove("w3-red");
			elem.classList.add("w3-blue");
		} 
		if(valor>=75)
		{	//elem.classList.remove("w3-red");
			elem.classList.add("w3-green");	
		} 

	  var width = 1;
	  var id = setInterval(frame, 10);
	  function frame() 
	  {
	    if (width >= valor) 
	    {
	      clearInterval(id);
	    } 
	    else 
	    {
	      width++; 
	      elem.style.width = width + '%'; 
	      elem.innerHTML = width * 1  + '%';
	    }
	  }
	}


	


function center_map( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

		bounds.extend( latlng );

	});

	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 16 );
	}
	else
	{
		// fit to bounds
		map.fitBounds( bounds );
	}

}

/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/
// global var
var map = null;



$(document).ready(function(){

	$('.acf-map').each(function(){

		// create map
		map = new_map( $(this) );

	});

		move(2.9);  /* funcion barra progreso*/

});

})(jQuery);



</script>



<?php get_footer(); ?>


