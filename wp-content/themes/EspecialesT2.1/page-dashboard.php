
<?php get_header(); 
/*
*  Template Name: dashboard
*/
$pagina=$_GET['page_id'];
?>
<!-- STILO DE DEL MENU PRICNIAPAL -->
<style>

/* Create two equal columns that floats next to each other */
.column {
    float: left;
    width: 30%;
    padding: 10px;
    height: 200px; /* Should be removed. Only for demonstration */
    margin-left:1.5%;
    margin-right:1.5%;
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}

.center { 
    height: 50px;
    position: relative;
  /*  border: 3px solid green; */
}

.center p {
    margin: 0;
    position: absolute;
    top: 50%;
    left: 50%;
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
}
.guardar_check {width: 100%;}
.guardar_check_2 {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 10px 10px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}



 body {
      font-family: "Lato", sans-serif;
      transition: background-color .5s;
  }

  .sidenav {
      height: 100%;
      width: 0;
      position: fixed;
      z-index: 1;
      top: 5%;
      left: 0;
      background-color: #111;
      overflow-x: hidden;
      transition: 0.5s;
      padding-top: 160px;
  }

  .sidenav a {
      padding: 8px 8px 8px 32px;
      text-decoration: none;
      font-size: 15px;
      color: #818181;
      display: block;
      transition: 0.3s;
  }

  .sidenav a:hover {
      color: #f1f1f1;
  }

  .sidenav .closebtn {
      position: absolute;
      top: 20%;
      right: 25px;
      font-size: 36px;
      margin-left: 50px;
  }

  #main {
      transition: margin-left .5s;
      padding: 16px;
  }

  @media screen and (max-height: 550px) {
    .sidenav {padding-top: 15px;}
    .sidenav a {font-size: 18px;}
  }


</style>  


        <link rel="stylesheet" type="text/css" href="/repositorio/wp-content/themes/EspecialesT2.1/css/demo_btns.css" />
        <link rel="stylesheet" type="text/css" href="/repositorio/wp-content/themes/EspecialesT2.1/css/style1_btns.css" />

        <meta name="author" content="Codrops" />
        <link rel="shortcut icon" href="../favicon.ico">
        <link rel="stylesheet" type="text/css" href="/repositorio/wp-content/themes/EspecialesT2.1/css/normalize_checkbox.css" />
        <link rel="stylesheet" type="text/css" href="/repositorio/wp-content/themes/EspecialesT2.1/css/demo_checkbox.css" />
        <link rel="stylesheet" type="text/css" href="/repositorio/wp-content/themes/EspecialesT2.1/css/component_checkbox.css" />
        <script src="/repositorio/wp-content/themes/EspecialesT2.1/js/modernizr.custom.js"></script>
       <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
       <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
       <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


        <link rel="stylesheet" href="dist/bootstrap.min.css" type="text/css" media="all">
        <link href="dist/jquery.bootgrid.css" rel="stylesheet" />
        <script src="dist/jquery-1.11.1.min.js"></script>
        <script src="dist/bootstrap.min.js"></script>
        <script src="dist/jquery.bootgrid.min.js"></script>


      <!-- STILO DE DEL MENU PRICNIAPAL -->
      <section id="main-content">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
              <!--DATOS DEL USUARIO ACTIVO-->
                <div>
                      <table>
                        <col width=80%>
                        <col width=20%>
                          <tbody>
                          <td></td><td>
                          <?php /* Nombre del usuario activo*/ 
                            $all_users = get_users();
                            $user_name;
                            $user_name=$user->display_name;

                                echo 'Bienvenido: ' . $current_user->user_login . '<br />';
                                echo 'Correo: ' . $current_user->user_email . '<br />';
                                $user_name=$current_user->user_login;

                          ?>
                           </td>
                           <td> 
                                <?php
                                    $user = wp_get_current_user();
                                  if ( $user ) :
                                      ?>
                                      <img src="<?php echo esc_url( get_avatar_url( $user->ID ) ); ?>" />
                                  <?php endif; 
                                ?>
                           </td> 
                          </tbody>
                      </table>
                </div>  
              <!--FIN DATOS DEL USUARIO ACTIVO-->


              <?php while(have_posts()): the_post(); ?>

                <div id="mySidenav" class="sidenav">
                  <a href="#"></a>
                  <a href="#"></a>
                  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                  <a onclick="load_datos_depencencia('modulos')" href="#">MODULOS SECRETARIAS</a>
                  <a onclick="load_datos_depencencia('pdf')" href="#">CREAR DOCUMENTOS</a>
                </div>

                <div id="main">
                  <span style="font-size:25px;cursor:pointer" onclick="openNav()">&#9776; MENU</span>
                  <br>
                  <br>
                 
                </div>

                <h2 style="text-align:center; font-size:160%;">Panel del control</h2>
                <br>
                
                <div id="desabilitar_modulos">
                  <p style="text-align:center; font-size:120%;">Habilitar y deshabitar Modulos de las Dependencias</p>
                  <br>

                  <div class="container" >
                    
                      

                      <form method="post" class="ac-custom ac-checkbox ac-cross" id="form_ctrl">

                        <!-- Wrapper for slides -->
                        <div class="center">
                          <p><button class="guardar_check guardar_check_2" name="enviar_form_modulos"  type="submit" >Guardar Cambios</button></p>
                        </div>
                        <div class="row"> 
                              <br>
                              <div class="column" style="background-color:#bbb; ">
                                    <h2>CONTRALORIA</h2>
                                    <ul>
                                      <p id="contraloria_checks"></p>
                                      <input type="hidden" name="contraloria_valores" id="contraloria_valores">
                                    </ul>
                                     <script>
                                      var contraloria_mod = ["Nuevo Registro",
                                         "Editar Registros",                //1
                                         "Reportes PDFs",];
                                        var checks = "";
                                        var i;
                                        checks+="<ul>"
                                        for (i = 0; i < contraloria_mod.length; i++) 
                                        {
                                          checks +='<li>'+'<input type="checkbox" id="cb'+(i+1)+'" class="contraloria" name="cb'+(i+1)+'">'+'<label for="cb'+(i+1)+'"> '+contraloria_mod[i]+'</label>'+'</li>';
                                        }
                                        checks+="</ul>"
                                        document.getElementById("contraloria_checks").innerHTML = checks;
                                      </script>
                              </div>

                              <div class="column" style="background-color:#bbb; ">
                                    <h2>SEC_CULTURA</h2>
                                    <ul>
                                      <p id="sec_cultura_checks"></p>
                                      <input type="hidden" name="sec_cultura_valores" id="sec_cultura_valores">
                                    </ul>
                                     <script>
                                      var sec_cultura_mod = ["Nuevo Registro",
                                         "Editar Registros",                //1
                                         "Reportes PDFs",];
                                        var checks = "";
                                        var i;
                                        checks+="<ul>"
                                        for (i = 0; i < sec_cultura_mod.length; i++) 
                                        {
                                           checks +='<li>'+'<input type="checkbox" id="sec_cult'+(i+1)+'" class="sec_cultura" name="sec_cult'+(i+1)+'">'+'<label for="sec_cult'+(i+1)+'"> '+sec_cultura_mod[i]+'</label>'+'</li>';
                                        }
                                        checks+="</ul>"
                                        document.getElementById("sec_cultura_checks").innerHTML = checks;
                                      </script>
                              </div>
                            
                              <div class="column" style="background-color:#bbb; ">
                                    <h2>SEP</h2>
                                    <ul>
                                      <p id="sep_checks"></p>
                                       <input type="hidden" name="sep_valores" id="sep_valores">
                                    </ul>
                                     <script>
                                      var sep_mod = ["Nuevo Registro",
                                         "Editar Registros",                //1
                                         "Reportes PDFs",];
                                        var checks = "";
                                        var i;
                                        checks+="<ul>"
                                        for (i = 0; i < sep_mod.length; i++) 
                                        {
                                           checks +='<li>'+'<input type="checkbox" class="sep" id="sep'+(i+1)+'"  name="sep'+(i+1)+'">'+'<label for="sep'+(i+1)+'"> '+sep_mod[i]+'</label>'+'</li>';
                                        }
                                        checks+="</ul>"
                                        document.getElementById("sep_checks").innerHTML = checks;
                                      </script>
                              </div>
                        </div>
                            <br>
                        <div class="row">
                              <div class="column" style="background-color:#bbb; ">
                                    <h2>SEC_GOB</h2>
                                    <ul>
                                      <p id="sec_gob"></p>
                                      <input type="hidden" name="sec_gob_valores" id="sec_gob_valores">
                                    </ul>
                                     <script>
                                      var sec_gob = ["Nuevo Registro",
                                         "Editar Registros",                //1
                                         "Reportes PDFs",];
                                        var checks = "";
                                        var i;
                                        checks+="<ul>"
                                        for (i = 0; i < sec_gob.length; i++) 
                                        {
                                           checks +='<li>'+'<input class="sec_gob" type="checkbox" id="sec_gob'+(i+1)+'"  name="sec_gob'+(i+1)+'">'+'<label for="sec_gob'+(i+1)+'"> '+sec_gob[i]+'</label>'+'</li>';
                                        }
                                        checks+="</ul>"
                                        document.getElementById("sec_gob").innerHTML = checks;
                                      </script>
                              </div>
                              <div class="column" style="background-color:#bbb; ">
                                    <h2>PROCURADURIA</h2>
                                    <ul>
                                      <p id="procuraduria_checks"></p>
                                      <input type="hidden" name="procuraduria_valores" id="procuraduria_valores">
                                    </ul>
                                     <script>
                                      var procuradurioa_mod = ["Nuevo Registro",
                                         "Editar Registros",                //1
                                         "Reportes PDFs",];
                                        var checks = "";
                                        var i;
                                        checks+="<ul>"
                                        for (i = 0; i < procuradurioa_mod.length; i++) 
                                        {
                                           checks +='<li>'+'<input class="procuraduria" type="checkbox" id="procu'+(i+1)+'"  name="procu'+(i+1)+'">'+'<label for="procu'+(i+1)+'"> '+procuradurioa_mod[i]+'</label>'+'</li>';
                                        }
                                        checks+="</ul>"
                                        document.getElementById("procuraduria_checks").innerHTML = checks;
                                      </script>
                              </div>
                              <div class="column" style="background-color:#bbb; ">
                                    <h2>SEC_MOVYTRANS</h2>
                                    <ul>
                                      <p id="movi_checks"></p>
                                      <input type="hidden" name="sec_movy_valores" id="sec_movy_valores">
                                    </ul>
                                     <script>
                                      var movi_mod = ["Nuevo Registro",
                                         "Editar Registros",                //1
                                         "Reportes PDFs",];
                                        var checks = "";
                                        var i;
                                        checks+="<ul>"
                                        for (i = 0; i < movi_mod.length; i++) 
                                        {
                                           checks +='<li>'+'<input class="sec_movy" type="checkbox" id="movi'+(i+1)+'"  name="movi'+(i+1)+'">'+'<label for="movi'+(i+1)+'"> '+movi_mod[i]+'</label>'+'</li>';
                                        }
                                        checks+="</ul>"
                                        document.getElementById("movi_checks").innerHTML = checks;


                                        <?php global $wpdb;
                                          $ctrl = $wpdb->prefix . 'ctrl_modulos';
                                          $registros = $wpdb->get_results("SELECT * FROM $ctrl ", ARRAY_A);
                                          $i=0;
                                            foreach($registros as $registro) 
                                              { 
                                                ?>
                                               

                                                  var arr_contraloria = document.getElementsByClassName("contraloria");
                                                  if(<?php echo($i)?>=="0")
                                                  {
                                                    var arr_modulos='<?php echo($registro['modulos'])?>';
                                                     arr_modulos = arr_modulos.split(",");
                                                    var i;
                                                    for (i = 0; i < (arr_modulos.length-1); i++) 
                                                    { 
                                                      arr_contraloria[parseInt(arr_modulos[i])].checked = true;
                                                      console.log(arr_contraloria[parseInt(arr_modulos[i])]); 
                                                    }

                                                  }

                                                  var arr_sec_cultura = document.getElementsByClassName("sec_cultura");
                                                  if(<?php echo($i)?>=="1")
                                                  {
                                                    var arr_modulos='<?php echo($registro['modulos'])?>';
                                                     arr_modulos = arr_modulos.split(",");
                                                    var i;
                                                    for (i = 0; i < (arr_modulos.length-1); i++) 
                                                    { 
                                                      arr_sec_cultura[parseInt(arr_modulos[i])].checked = true;
                                                      console.log(arr_sec_cultura[parseInt(arr_modulos[i])]); 
                                                    }
                                                  }

                                                  var arr_sep = document.getElementsByClassName("sep");
                                                  if(<?php echo($i)?>=="2")
                                                  {
                                                    var arr_modulos='<?php echo($registro['modulos'])?>';
                                                     arr_modulos = arr_modulos.split(",");
                                                    var i;
                                                    for (i = 0; i < (arr_modulos.length-1); i++) 
                                                    { 
                                                      arr_sep[parseInt(arr_modulos[i])].checked = true;
                                                      console.log(arr_sep[parseInt(arr_modulos[i])]); 
                                                    }
                                                  }

                                                  var arr_sec_gob = document.getElementsByClassName("sec_gob");
                                                  if(<?php echo($i)?>=="3")
                                                  {
                                                    var arr_modulos='<?php echo($registro['modulos'])?>';
                                                     arr_modulos = arr_modulos.split(",");
                                                    var i;
                                                    for (i = 0; i < (arr_modulos.length-1); i++) 
                                                    { 
                                                      arr_sec_gob[parseInt(arr_modulos[i])].checked = true;
                                                      console.log(arr_sec_gob[parseInt(arr_modulos[i])]); 
                                                    }
                                                  }

                                                  var arr_procuraduria = document.getElementsByClassName("procuraduria");
                                                  if(<?php echo($i)?>=="4")
                                                  {
                                                    var arr_modulos='<?php echo($registro['modulos'])?>';
                                                     arr_modulos = arr_modulos.split(",");
                                                    var i;
                                                    for (i = 0; i < (arr_modulos.length-1); i++) 
                                                    { 
                                                      arr_procuraduria[parseInt(arr_modulos[i])].checked = true;
                                                      console.log(arr_procuraduria[parseInt(arr_modulos[i])]); 
                                                    }
                                                  }

                                                  var arr_sec_movy = document.getElementsByClassName("sec_movy");
                                                  if(<?php echo($i)?>=="5")
                                                  {
                                                    var arr_modulos='<?php echo($registro['modulos'])?>';
                                                     arr_modulos = arr_modulos.split(",");
                                                    var i;
                                                    for (i = 0; i < (arr_modulos.length-1); i++) 
                                                    { 
                                                      arr_sec_movy[parseInt(arr_modulos[i])].checked = true;
                                                      console.log(arr_sec_movy[parseInt(arr_modulos[i])]); 
                                                    }
                                                  }

                                               


                                                  

                                              

                                          <?php 
                                          $i++; 
                                        } ?>                    
                                      </script>
                              </div>
                        </div>
                            

                        <div class="row"> 
                              <br>
                              <div class="column" style="background-color:#bbb; ">
                                    <h2>SEC_SALUD</h2>
                                    <ul>
                                      <p id="sec_salud_checks"></p>
                                      <input type="hidden" name="sec_salud_valores" id="sec_salud_valores">
                                    </ul>
                                     <script>
                                      var sec_salud_mod = ["Nuevo Registro",
                                         "Editar Registros",                //1
                                         "Reportes PDFs",];
                                        var checks = "";
                                        var i;
                                        checks+="<ul>"
                                        for (i = 0; i < sec_salud_mod.length; i++) 
                                        {
                                          checks +='<li>'+'<input type="checkbox" id="sec_salud'+(i+1)+'" class="sec_salud" name="sec_salud'+(i+1)+'">'+'<label for="sec_salud'+(i+1)+'"> '+sec_salud_mod[i]+'</label>'+'</li>';
                                        }
                                        checks+="</ul>"
                                        document.getElementById("sec_salud_checks").innerHTML = checks;
                                      </script>
                              </div>

                              <div class="column" style="background-color:#bbb; ">
                                    <h2>SEC_TURISMO</h2>
                                    <ul>
                                      <p id="sec_turismo_checks"></p>
                                      <input type="hidden" name="sec_turismo_valores" id="sec_turismo_valores">
                                    </ul>
                                     <script>
                                      var sec_turismo_mod = ["Nuevo Registro",
                                         "Editar Registros",                //1
                                         "Reportes PDFs",];
                                        var checks = "";
                                        var i;
                                        checks+="<ul>"
                                        for (i = 0; i < sec_turismo_mod.length; i++) 
                                        {
                                          checks +='<li>'+'<input type="checkbox" id="sec_turismo'+(i+1)+'" class="sec_turismo" name="sec_turismo'+(i+1)+'">'+'<label for="sec_turismo'+(i+1)+'"> '+sec_turismo_mod[i]+'</label>'+'</li>';
                                        }
                                        checks+="</ul>"
                                        document.getElementById("sec_turismo_checks").innerHTML = checks;
                                      </script>
                              </div>

                              <div class="column" style="background-color:#bbb; ">
                                    <h2>SEC_TRABYPS</h2>
                                    <ul>
                                      <p id="sec_trabyps_checks"></p>
                                      <input type="hidden" name="sec_trabyps_valores" id="sec_trabyps_valores">
                                    </ul>
                                     <script>
                                      var sec_trabyps_mod = ["Nuevo Registro",
                                         "Editar Registros",                //1
                                         "Reportes PDFs",];
                                        var checks = "";
                                        var i;
                                        checks+="<ul>"
                                        for (i = 0; i < sec_trabyps_mod.length; i++) 
                                        {
                                          checks +='<li>'+'<input type="checkbox" id="sec_trabyps'+(i+1)+'" class="sec_trabyps" name="sec_trabyps'+(i+1)+'">'+'<label for="sec_trabyps'+(i+1)+'"> '+sec_trabyps_mod[i]+'</label>'+'</li>';
                                        }
                                        checks+="</ul>"
                                        document.getElementById("sec_trabyps_checks").innerHTML = checks;
                                      </script>
                              </div>

                        </div>
                        <br>
                        <div class="row">
                          <div class="column" style="background-color:#bbb; ">
                                    <h2>SEC_EJECUTIVA</h2>
                                    <ul>
                                      <p id="sec_ejecutiva_checks"></p>
                                      <input type="hidden" name="sec_ejecutiva_valores" id="sec_ejecutiva_valores">
                                    </ul>
                                     <script>
                                      var sec_ejecutiva_mod = ["Nuevo Registro",
                                         "Editar Registros",                //1
                                         "Reportes PDFs",];
                                        var checks = "";
                                        var i;
                                        checks+="<ul>"
                                        for (i = 0; i < sec_ejecutiva_mod.length; i++) 
                                        {
                                          checks +='<li>'+'<input type="checkbox" id="sec_ejecutiva'+(i+1)+'" class="sec_ejecutiva" name="sec_ejecutiva'+(i+1)+'">'+'<label for="sec_ejecutiva'+(i+1)+'"> '+sec_ejecutiva_mod[i]+'</label>'+'</li>';
                                        }
                                        checks+="</ul>"
                                        document.getElementById("sec_ejecutiva_checks").innerHTML = checks;
                                      </script>
                          </div>

                          <div class="column" style="background-color:#bbb; ">
                                    <h2>SEC_DESAGRO</h2>
                                    <ul>
                                      <p id="sec_desagro_checks"></p>
                                      <input type="hidden" name="sec_desagro_valores" id="sec_desagro_valores">
                                    </ul>
                                     <script>
                                      var sec_desagro_mod = ["Nuevo Registro",
                                         "Editar Registros",                //1
                                         "Reportes PDFs",];
                                        var checks = "";
                                        var i;
                                        checks+="<ul>"
                                        for (i = 0; i < sec_desagro_mod.length; i++) 
                                        {
                                          checks +='<li>'+'<input type="checkbox" id="sec_desagro'+(i+1)+'" class="sec_desagro" name="sec_desagro'+(i+1)+'">'+'<label for="sec_desagro'+(i+1)+'"> '+sec_desagro_mod[i]+'</label>'+'</li>';
                                        }
                                        checks+="</ul>"
                                        document.getElementById("sec_desagro_checks").innerHTML = checks;
                                      </script>
                          </div>

                          <div class="column" style="background-color:#bbb; ">
                                    <h2>SEC_DESECON</h2>
                                    <ul>
                                      <p id="sec_desecon_checks"></p>
                                      <input type="hidden" name="sec_desecon_valores" id="sec_desecon_valores">
                                    </ul>
                                     <script>
                                      var sec_desecon_mod = ["Nuevo Registro",
                                         "Editar Registros",                //1
                                         "Reportes PDFs",];
                                        var checks = "";
                                        var i;
                                        checks+="<ul>"
                                        for (i = 0; i < sec_desecon_mod.length; i++) 
                                        {
                                          checks +='<li>'+'<input type="checkbox" id="sec_desecon'+(i+1)+'" class="sec_desecon" name="sec_desecon'+(i+1)+'">'+'<label for="sec_desecon'+(i+1)+'"> '+sec_desecon_mod[i]+'</label>'+'</li>';
                                        }
                                        checks+="</ul>"
                                        document.getElementById("sec_desecon_checks").innerHTML = checks;
                                      </script>
                          </div>
                        </div>
                        <br>
                        <div class="row">
                          <div class="column" style="background-color:#bbb; ">
                                    <h2>SEMARNATH</h2>
                                    <ul>
                                      <p id="semarnath_checks"></p>
                                      <input type="hidden" name="semarnath_valores" id="semarnath_valores">
                                    </ul>
                                     <script>
                                      var semarnath_mod = ["Nuevo Registro",
                                         "Editar Registros",                //1
                                         "Reportes PDFs",];
                                        var checks = "";
                                        var i;
                                        checks+="<ul>"
                                        for (i = 0; i < semarnath_mod.length; i++) 
                                        {
                                          checks +='<li>'+'<input type="checkbox" id="semarnath'+(i+1)+'" class="semarnath" name="semarnath'+(i+1)+'">'+'<label for="semarnath'+(i+1)+'"> '+semarnath_mod[i]+'</label>'+'</li>';
                                        }
                                        checks+="</ul>"
                                        document.getElementById("semarnath_checks").innerHTML = checks;
                                      </script>
                          </div>

                          <div class="column" style="background-color:#bbb; ">
                                    <h2>UNI_PLANEA</h2>
                                    <ul>
                                      <p id="uni_planea_checks"></p>
                                      <input type="hidden" name="uni_planea_valores" id="uni_planea_valores">
                                    </ul>
                                     <script>
                                      var uni_planea_mod = ["Nuevo Registro",
                                         "Editar Registros",                //1
                                         "Reportes PDFs",];
                                        var checks = "";
                                        var i;
                                        checks+="<ul>"
                                        for (i = 0; i < uni_planea_mod.length; i++) 
                                        {
                                          checks +='<li>'+'<input type="checkbox" id="uni_planea'+(i+1)+'" class="uni_planea" name="uni_planea'+(i+1)+'">'+'<label for="uni_planea'+(i+1)+'"> '+uni_planea_mod[i]+'</label>'+'</li>';
                                        }
                                        checks+="</ul>"
                                        document.getElementById("uni_planea_checks").innerHTML = checks;
                                      </script>
                          </div>

                          <div class="column" style="background-color:#bbb; ">
                                    <h2>OFI_MAYOR</h2>
                                    <ul>
                                      <p id="ofi_mayor_checks"></p>
                                      <input type="hidden" name="ofi_mayor_valores" id="ofi_mayor_valores">
                                    </ul>
                                     <script>
                                      var ofi_mayor_mod = ["Nuevo Registro",
                                         "Editar Registros",                //1
                                         "Reportes PDFs",];
                                        var checks = "";
                                        var i;
                                        checks+="<ul>"
                                        for (i = 0; i < ofi_mayor_mod.length; i++) 
                                        {
                                          checks +='<li>'+'<input type="checkbox" id="ofi_mayor'+(i+1)+'" class="ofi_mayor" name="ofi_mayor'+(i+1)+'">'+'<label for="ofi_mayor'+(i+1)+'"> '+ofi_mayor_mod[i]+'</label>'+'</li>';
                                        }
                                        checks+="</ul>"
                                        document.getElementById("ofi_mayor_checks").innerHTML = checks;
                                      </script>
                          </div>
                        </div>
                        <br>
                        <div class="row">
                          <div class="column" style="background-color:#bbb; ">
                                    <h2>DIFH</h2>
                                    <ul>
                                      <p id="difh_checks"></p>
                                      <input type="hidden" name="difh_valores" id="difh_valores">
                                    </ul>
                                     <script>
                                      var difh_mod = ["Nuevo Registro",
                                         "Editar Registros",                //1
                                         "Reportes PDFs",];
                                        var checks = "";
                                        var i;
                                        checks+="<ul>"
                                        for (i = 0; i < difh_mod.length; i++) 
                                        {
                                          checks +='<li>'+'<input type="checkbox" id="difh'+(i+1)+'" class="difh" name="difh'+(i+1)+'">'+'<label for="difh'+(i+1)+'"> '+difh_mod[i]+'</label>'+'</li>';
                                        }
                                        checks+="</ul>"
                                        document.getElementById("difh_checks").innerHTML = checks;
                                      </script>
                          </div>

                          <div class="column" style="background-color:#bbb; ">
                                    <h2>SEC_FINANZAS</h2>
                                    <ul>
                                      <p id="sec_finanzas_checks"></p>
                                      <input type="hidden" name="sec_finanzas_valores" id="sec_finanzas_valores">
                                    </ul>
                                     <script>
                                      var sec_finanzas_mod = ["Nuevo Registro",
                                         "Editar Registros",                //1
                                         "Reportes PDFs",];
                                        var checks = "";
                                        var i;
                                        checks+="<ul>"
                                        for (i = 0; i < sec_finanzas_mod.length; i++) 
                                        {
                                          checks +='<li>'+'<input type="checkbox" id="sec_finanzas'+(i+1)+'" class="sec_finanzas" name="sec_finanzas'+(i+1)+'">'+'<label for="sec_finanzas'+(i+1)+'"> '+sec_finanzas_mod[i]+'</label>'+'</li>';
                                        }
                                        checks+="</ul>"
                                        document.getElementById("sec_finanzas_checks").innerHTML = checks;
                                      </script>
                          </div>

                          <div class="column" style="background-color:#bbb; ">
                                    <h2>SOPOT</h2>
                                    <ul>
                                      <p id="sopot_checks"></p>
                                      <input type="hidden" name="sopot_valores" id="sopot_valores">
                                    </ul>
                                     <script>
                                      var sopot_mod = ["Nuevo Registro",
                                         "Editar Registros",                //1
                                         "Reportes PDFs",];
                                        var checks = "";
                                        var i;
                                        checks+="<ul>"
                                        for (i = 0; i < sopot_mod.length; i++) 
                                        {
                                          checks +='<li>'+'<input type="checkbox" id="sopot'+(i+1)+'" class="sopot" name="sopot'+(i+1)+'">'+'<label for="sopot'+(i+1)+'"> '+sopot_mod[i]+'</label>'+'</li>';
                                        }
                                        checks+="</ul>"
                                        document.getElementById("sopot_checks").innerHTML = checks;
                                      </script>
                          </div>

                          
                        </div>

                        <br>
                        
                        <div class="row">
                          <div class="column" style="background-color:#bbb; ">
                                    <h2>SEDESO</h2>
                                    <ul>
                                      <p id="sedeso_checks"></p>
                                      <input type="hidden" name="sedeso_valores" id="sedeso_valores">
                                    </ul>
                                     <script>
                                      var sedeso_mod = ["Nuevo Registro",
                                         "Editar Registros",                //1
                                         "Reportes PDFs",];
                                        var checks = "";
                                        var i;
                                        checks+="<ul>"
                                        for (i = 0; i < sedeso_mod.length; i++) 
                                        {
                                          checks +='<li>'+'<input type="checkbox" id="sedeso'+(i+1)+'" class="sedeso" name="sedeso'+(i+1)+'">'+'<label for="sedeso'+(i+1)+'"> '+sedeso_mod[i]+'</label>'+'</li>';
                                        }
                                        checks+="</ul>"
                                        document.getElementById("sedeso_checks").innerHTML = checks;
                                      </script>
                          </div>

                          <div class="column" style="background-color:#bbb; ">
                                    <h2>CITNOVA</h2>
                                    <ul>
                                      <p id="citnova_checks"></p>
                                      <input type="hidden" name="citnova_valores" id="citnova_valores">
                                    </ul>
                                     <script>
                                      var citnova_mod = ["Nuevo Registro",
                                         "Editar Registros",                //1
                                         "Reportes PDFs",];
                                        var checks = "";
                                        var i;
                                        checks+="<ul>"
                                        for (i = 0; i < citnova_mod.length; i++) 
                                        {
                                          checks +='<li>'+'<input type="checkbox" id="citnova'+(i+1)+'" class="citnova" name="citnova'+(i+1)+'">'+'<label for="citnova'+(i+1)+'"> '+citnova_mod[i]+'</label>'+'</li>';
                                        }
                                        checks+="</ul>"
                                        document.getElementById("citnova_checks").innerHTML = checks;
                                      </script>
                          </div>

                          <div class="column" style="background-color:#bbb; ">
                                    <h2>REHILETE</h2>
                                    <ul>
                                      <p id="rehilete_checks"></p>
                                      <input type="hidden" name="rehilete_valores" id="rehilete_valores">
                                    </ul>
                                     <script>
                                      var rehilete_mod = ["Nuevo Registro",
                                         "Editar Registros",                //1
                                         "Reportes PDFs",];
                                        var checks = "";
                                        var i;
                                        checks+="<ul>"
                                        for (i = 0; i < rehilete_mod.length; i++) 
                                        {
                                          checks +='<li>'+'<input type="checkbox" id="rehilete'+(i+1)+'" class="rehilete" name="rehilete'+(i+1)+'">'+'<label for="rehilete'+(i+1)+'"> '+rehilete_mod[i]+'</label>'+'</li>';
                                        }
                                        checks+="</ul>"
                                        document.getElementById("rehilete_checks").innerHTML = checks;
                                      </script>
                          </div>
                        </div>

                        <br>
                        <div class="row">

                          <div class="column" style="background-color:#bbb; ">
                                    <h2>SEC_SEGUP</h2>
                                    <ul>
                                      <p id="sec_segup_checks"></p>
                                      <input type="hidden" name="sec_segup_valores" id="sec_segup_valores">
                                    </ul>
                                     <script>
                                      var sec_segup_mod = ["Nuevo Registro",
                                         "Editar Registros",                //1
                                         "Reportes PDFs",];
                                        var checks = "";
                                        var i;
                                        checks+="<ul>"
                                        for (i = 0; i < sec_segup_mod.length; i++) 
                                        {
                                          checks +='<li>'+'<input type="checkbox" id="sec_segup'+(i+1)+'" class="sec_segup" name="sec_segup'+(i+1)+'">'+'<label for="sec_segup'+(i+1)+'"> '+sec_segup_mod[i]+'</label>'+'</li>';
                                        }
                                        checks+="</ul>"
                                        document.getElementById("sec_segup_checks").innerHTML = checks;
                                      </script>
                          </div>

                        </div>



                        <input type="hidden" name="oculto_ctrl_modulos" value="1" >
                            
                     
                      </form>

                    
                   
                  </div>  
                </div>

                <div id="crear_pdf">
                  <div class="container">
                    <div class="">
                        <div class="">
                         <button onclick="crear()" >EXPORTAR DOCUMENTO</button>
                         <button onclick="crear_pdf_exel()" >EXPORTAR EXCEL</button>
                        <table id="employee_grid" class="table table-condensed table-hover table-striped" width="100%" cellspacing="150">
                          <thead>

                            <tr>
                              <!-- <th data-column-id="id_indicador_asociado_def" data-type="numeric" type="hidden">ID_indicador  </th> -->
                              <th data-column-id="nombre_indicador_asociado_def">Indicador</th>
                              
                              <th data-column-id="linea_discursiva">Linea Discursiva</th>
                              <th data-column-id="indicador_estrella">Tipo de indicador</th>
                              <th data-column-id="reporte_primer_informe_def">Avance</th>
                              <th data-column-id="resultado_2018_2">Resultado
                              <th data-column-id="nombre_dependencia_def">Dependencia</th>
                              <th data-column-id="eje">Eje</th>
                              <th data-column-id="bullet_logro">Bullet logro</th>
                              <th data-column-id="resultado1">Resultado 1</th>
                              <th data-column-id="lug_result1">Municipios 1</th>
                              <th data-column-id="resultado2">Resultado 2</th>
                              <th data-column-id="lug_result2">Municipios 2</th>
                              <th data-column-id="resultado3">Resultado 3</th>
                              <th data-column-id="lug_result3">Municipios 3</th>
                              <th data-column-id="resultado4">Resultado 4</th>
                              <th data-column-id="lug_result4">Municipios 4</th>
                              <th data-column-id="resultado5">Resultado 5</th>
                              <th data-column-id="lug_result5">LMunicipios 5</th>


                            </tr>
                          </thead>
                        </table>

                        
                      </div>
                    </div>
                  </div>

		 <div class="container">
                      <div class="row text-center pad-row">
                          <div class="col-md-4 col-sm-4">
                            <a class="a-btn" onclick="crear_pdf();">
                              <span class="a-btn-slide-text"><script type="text/javascript">document.write('\u2714')</script></span>
                              <img src="/repositorio/wp-content/themes/EspecialesT2.1/images/icons/20.png" alt="" />
                              <span class="a-btn-text"><small>Actualizar <br> información</small></span> 
                            </a>
			 </div>

			 <div class="col-md-4 col-sm-4">
			 </div>

			 <div class="col-md-4 col-sm-4">
			 </div>
		 </div>

                  <div class="container">
                      <div class="row text-center pad-row">
                          <div class="col-md-4 col-sm-4">
                            <a class="a-btn"  onclick="crear_pdf();">
                              <span class="a-btn-slide-text"><script type="text/javascript">document.write('\u2714')</script></span>
                              <img src="/repositorio/wp-content/themes/EspecialesT2.1/images/icons/20.png" alt="" />
                              <span class="a-btn-text"><small>3er INFORME DE</small> Gobierno</span> 
                            </a>
                            
                          </div>
                          <div class="col-md-4 col-sm-4">
                            <a href="#" class="a-btn">
                              <span class="a-btn-slide-text"><script type="text/javascript">document.write('\u2714')</script></span>
                              <img src="/repositorio/wp-content/themes/EspecialesT2.1/images/icons/20.png" alt="" />
                              <span class="a-btn-text"><small>Informe por</small> Macro Región</span> 
                            </a>
                          </div>
                          <div class="col-md-4 col-sm-4">
                            <a href="#" class="a-btn">
                              <span class="a-btn-slide-text"><script type="text/javascript">document.write('\u2714')</script></span>
                              <img src="/repositorio/wp-content/themes/EspecialesT2.1/images/icons/20.png" alt="" />
                              <span class="a-btn-text"><small>Informe</small> Secretarias</span> 
                            </a>
                          </div>
                      </div><!--FIN COL-->
                        <p>
                        <p>
                        <p>
                        <p>
                        <p>
                        <p>
                  </div>
                    

                  <script type="text/javascript">
                    function crear_pdf()
                    {

		

                           
                                    $.ajax({
                                    
                                    type: "POST",
                                    
                                    url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/actualizacion.php",
                                    data: {}
                                    ,
                                    success: function(msg){
							
                                      if(msg==1)
                                      {

                                        //alert("Informacion Actualizada");

					      var ids_ind_tac;
		

						        
					      for (i =0; i < datos.length; i++) 
					      {
						
						ids_ind_tac += datos[i].id_indicador_asociado_def+",";
					      }
					      ids_ind_tac = ids_ind_tac.substr(9);
					      ids_ind_tac = ids_ind_tac.slice(0, -1);
					       console.log(ids_ind_tac);

					       window.open("pdf_plugin/pdf_evidencias/tercer_informe.php?arr_ids_tacticos="+ids_ind_tac+"&tipo=arr_tacticos_dep&id_evidencia=0&user=0");

                                      }

                                     // alert( "Informacion Actualizada" );
                                    },
                                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                                       alert( "Error Verifica tu informacion" );
                                    }
                                  });



                     /*
                      var ids_ind_tac;
		

                                
                      for (i =0; i < datos.length; i++) 
                      {
                        
                        ids_ind_tac += datos[i].id_indicador_asociado_def+",";
                      }
                      ids_ind_tac = ids_ind_tac.substr(9);
                      ids_ind_tac = ids_ind_tac.slice(0, -1);
                       console.log(ids_ind_tac);

                       window.open("pdf_plugin/pdf_evidencias/tercer_informe.php?arr_ids_tacticos="+ids_ind_tac+"&tipo=arr_tacticos_dep&id_evidencia=0&user=0");

                     */

                    }


                   function crear_pdf_exel()
                   {

                    var ids_ind_tac;
                                
                      for (i =0; i < datos.length; i++) 
                      {
                        
                        ids_ind_tac += datos[i].id_indicador_asociado_def+",";
                      }
                      ids_ind_tac = ids_ind_tac.substr(9);
                      ids_ind_tac = ids_ind_tac.slice(0, -1);
                       console.log(ids_ind_tac);

                       window.open("/repositorio/archivoexcel.php?arr_ids_tacticos="+ids_ind_tac+"&tipo=array_ids&id_evidencia=0&user=0");
                   }
                  </script>

                  <script type="text/javascript">
                    var datos;

                    $( document ).ready(function() 
                    {
                      $("#employee_grid").bootgrid({
                        ajax: true,
                        post: function ()
                        {
                          return 
                          {
                            id: "b0df282a-0d67-40e5-8558-c9e93b7befed"
                          };
                        },
                        url: "response.php",
                        formatters: {

                        },
                        responseHandler: function (data) {
                        
                               var response = {

                                  current: data.current,
                                  rowCount: data.rowCount,
                                  rows: data.rows,
                                  total: data.total,
                                  cantidad_totales: data.cantidad_totales,
                                  id_pedido: data.id_pedido
                              };
                              //store cantidad_totales into a variable...
                              datos = data.rows;
                              console.log(datos);
                              return response;
                          },


                        });

                    });//READY
                  </script>
                </div>



                <!--Menu desplegable  -->
                <script>


                  $('#form_ctrl').submit(function() 
                  {
                    cargar_checks();
                    return true;
                  });

                  function cargar_checks()
                  {
                    //---
                     var arr_contraloria = document.getElementsByClassName("contraloria");
                     var cheks_true="";
                        for (i = 0; i < arr_contraloria.length; i++) 
                      {

                        if(arr_contraloria[i].checked == true)
                        {
                          cheks_true+=i+","; 
                        }

                      }

                      var datos_contra =document.getElementById("contraloria_valores");
                      datos_contra.value= cheks_true;
                    //---


                    //---SEC CULT
                    var arr_contraloria = document.getElementsByClassName("sec_cultura");
                     var cheks_true="";
                        for (i = 0; i < arr_contraloria.length; i++) 
                      {
                        if(arr_contraloria[i].checked == true)
                        {
                          cheks_true+=i+","; 
                        }
                      }
                      var datos_contra =document.getElementById("sec_cultura_valores");
                      datos_contra.value= cheks_true;
                    //----

                                  //---SEC CULT
                    var arr_contraloria = document.getElementsByClassName("sep");
                     var cheks_true="";
                        for (i = 0; i < arr_contraloria.length; i++) 
                      {
                        if(arr_contraloria[i].checked == true)
                        {
                          cheks_true+=i+","; 
                        }
                      }
                      var datos_contra =document.getElementById("sep_valores");
                      datos_contra.value= cheks_true;
                    //----

                    //---SEC_GOB
                    var arr_contraloria = document.getElementsByClassName("sec_gob");
                     var cheks_true="";
                        for (i = 0; i < arr_contraloria.length; i++) 
                      {
                        if(arr_contraloria[i].checked == true)
                        {
                          cheks_true+=i+","; 
                        }
                      }
                      var datos_contra =document.getElementById("sec_gob_valores");
                      datos_contra.value= cheks_true;
                    //----

                    //---PROCURADURIA
                    var arr_contraloria = document.getElementsByClassName("procuraduria");
                     var cheks_true="";
                        for (i = 0; i < arr_contraloria.length; i++) 
                      {
                        if(arr_contraloria[i].checked == true)
                        {
                          cheks_true+=i+","; 
                        }
                      }
                      var datos_contra =document.getElementById("procuraduria_valores");
                      datos_contra.value= cheks_true;
                    //----


                    //---PROCURADURIA
                    var arr_contraloria = document.getElementsByClassName("sec_movy");
                     var cheks_true="";
                        for (i = 0; i < arr_contraloria.length; i++) 
                      {
                        if(arr_contraloria[i].checked == true)
                        {
                          cheks_true+=i+","; 
                        }
                      }
                      var datos_contra =document.getElementById("sec_movy_valores");
                      datos_contra.value= cheks_true;
                    //----

                    //---SALUD
                    var sec_salud = document.getElementsByClassName("sec_salud");
                     var cheks_true="";
                        for (i = 0; i < sec_salud.length; i++) 
                      {
                        if(sec_salud[i].checked == true)
                        {
                          cheks_true+=i+","; 
                        }
                      }
                      var datos_sec_salud =document.getElementById("sec_salud_valores");
                      datos_sec_salud.value= cheks_true;
                    //----

                    //---SALUD
                    var sec_ejecutiva = document.getElementsByClassName("sec_ejecutiva");
                     var cheks_true="";
                        for (i = 0; i < sec_ejecutiva.length; i++) 
                      {
                        if(sec_ejecutiva[i].checked == true)
                        {
                          cheks_true+=i+","; 
                        }
                      }
                      var datos_sec_ejecutiva =document.getElementById("sec_ejecutiva_valores");
                      datos_sec_ejecutiva.value= cheks_true;
                    //----

                    //---SALUD
                    var sec_desagro = document.getElementsByClassName("sec_desagro");
                     var cheks_true="";
                        for (i = 0; i < sec_desagro.length; i++) 
                      {
                        if(sec_desagro[i].checked == true)
                        {
                          cheks_true+=i+","; 
                        }
                      }
                      var datos_sec_desagro =document.getElementById("sec_desagro_valores");
                      datos_sec_desagro.value= cheks_true;
                    //----

                    //---SALUD
                    var semarnath = document.getElementsByClassName("semarnath");
                     var cheks_true="";
                        for (i = 0; i < semarnath.length; i++) 
                      {
                        if(semarnath[i].checked == true)
                        {
                          cheks_true+=i+","; 
                        }
                      }
                      var datos_semarnath =document.getElementById("semarnath_valores");
                      datos_semarnath.value= cheks_true;
                    //----

                    //---SALUD
                    var difh = document.getElementsByClassName("difh");
                     var cheks_true="";
                        for (i = 0; i < difh.length; i++) 
                      {
                        if(difh[i].checked == true)
                        {
                          cheks_true+=i+","; 
                        }
                      }
                      var datos_difh =document.getElementById("difh_valores");
                      datos_difh.value= cheks_true;
                    //----

                    //---SALUD
                    var sedeso = document.getElementsByClassName("sedeso");
                     var cheks_true="";
                        for (i = 0; i < sedeso.length; i++) 
                      {
                        if(sedeso[i].checked == true)
                        {
                          cheks_true+=i+","; 
                        }
                      }
                      var datos_sedeso =document.getElementById("sedeso_valores");
                      datos_sedeso.value= cheks_true;
                    //----

                    //---SALUD
                    var sec_segup = document.getElementsByClassName("sec_segup");
                     var cheks_true="";
                        for (i = 0; i < sec_segup.length; i++) 
                      {
                        if(sec_segup[i].checked == true)
                        {
                          cheks_true+=i+","; 
                        }
                      }
                      var datos_sec_segup =document.getElementById("sec_segup_valores");
                      datos_sec_segup.value= cheks_true;
                    //----

                    //---SALUD
                    var sec_turismo = document.getElementsByClassName("sec_turismo");
                     var cheks_true="";
                        for (i = 0; i < sec_turismo.length; i++) 
                      {
                        if(sec_turismo[i].checked == true)
                        {
                          cheks_true+=i+","; 
                        }
                      }
                      var datos_sec_turismo =document.getElementById("sec_turismo_valores");
                      datos_sec_turismo.value= cheks_true;
                    //----


                    //---SALUD
                    var sec_desecon = document.getElementsByClassName("sec_desecon");
                     var cheks_true="";
                        for (i = 0; i < sec_desecon.length; i++) 
                      {
                        if(sec_desecon[i].checked == true)
                        {
                          cheks_true+=i+","; 
                        }
                      }
                      var datos_sec_desecon =document.getElementById("sec_desecon_valores");
                      datos_sec_desecon.value= cheks_true;
                    //----


                    //---SALUD
                    var sec_trabyps = document.getElementsByClassName("sec_trabyps");
                     var cheks_true="";
                        for (i = 0; i < sec_trabyps.length; i++) 
                      {
                        if(sec_trabyps[i].checked == true)
                        {
                          cheks_true+=i+","; 
                        }
                      }
                      var datos_sec_trabyps =document.getElementById("sec_trabyps_valores");
                      datos_sec_trabyps.value= cheks_true;
                    //----

                    //---SALUD
                    var sec_desecon = document.getElementsByClassName("sec_desecon");
                     var cheks_true="";
                        for (i = 0; i < sec_desecon.length; i++) 
                      {
                        if(sec_desecon[i].checked == true)
                        {
                          cheks_true+=i+","; 
                        }
                      }
                      var datos_sec_desecon =document.getElementById("sec_desecon_valores");
                      datos_sec_desecon.value= cheks_true;
                    //----

                    //---SALUD
                    var uni_planea = document.getElementsByClassName("uni_planea");
                     var cheks_true="";
                        for (i = 0; i < uni_planea.length; i++) 
                      {
                        if(uni_planea[i].checked == true)
                        {
                          cheks_true+=i+","; 
                        }
                      }
                      var datos_uni_planea =document.getElementById("uni_planea_valores");
                      datos_uni_planea.value= cheks_true;
                    //----

                    //---SALUD
                    var ofi_mayor = document.getElementsByClassName("ofi_mayor");
                     var cheks_true="";
                        for (i = 0; i < ofi_mayor.length; i++) 
                      {
                        if(ofi_mayor[i].checked == true)
                        {
                          cheks_true+=i+","; 
                        }
                      }
                      var datos_ofi_mayor =document.getElementById("ofi_mayor_valores");
                      datos_ofi_mayor.value= cheks_true;
                    //----

                     //---SALUD
                    var sec_finanzas = document.getElementsByClassName("sec_finanzas");
                     var cheks_true="";
                        for (i = 0; i < sec_finanzas.length; i++) 
                      {
                        if(sec_finanzas[i].checked == true)
                        {
                          cheks_true+=i+","; 
                        }
                      }
                      var datos_sec_finanzas =document.getElementById("sec_finanzas_valores");
                      datos_sec_finanzas.value= cheks_true;
                    //----

                    //---SALUD
                    var sopot = document.getElementsByClassName("sopot");
                     var cheks_true="";
                        for (i = 0; i < sopot.length; i++) 
                      {
                        if(sopot[i].checked == true)
                        {
                          cheks_true+=i+","; 
                        }
                      }
                      var datos_sopot =document.getElementById("sopot_valores");
                      datos_sopot.value= cheks_true;
                    //----

                    //---SALUD
                    var sopot = document.getElementsByClassName("sopot");
                     var cheks_true="";
                        for (i = 0; i < sopot.length; i++) 
                      {
                        if(sopot[i].checked == true)
                        {
                          cheks_true+=i+","; 
                        }
                      }
                      var datos_sopot =document.getElementById("sopot_valores");
                      datos_sopot.value= cheks_true;
                    //----

                    //---SALUD
                    var citnova = document.getElementsByClassName("citnova");
                     var cheks_true="";
                        for (i = 0; i < citnova.length; i++) 
                      {
                        if(citnova[i].checked == true)
                        {
                          cheks_true+=i+","; 
                        }
                      }
                      var datos_citnova =document.getElementById("citnova_valores");
                      datos_citnova.value= cheks_true;
                    //----
                    //---SALUD
                    var rehilete = document.getElementsByClassName("rehilete");
                     var cheks_true="";
                        for (i = 0; i < rehilete.length; i++) 
                      {
                        if(rehilete[i].checked == true)
                        {
                          cheks_true+=i+","; 
                        }
                      }
                      var datos_rehilete =document.getElementById("rehilete_valores");
                      datos_rehilete.value= cheks_true;
                    //----






                
                    

                  }

                  



                  $('#desabilitar_modulos').hide();
                   $('#crear_pdf').hide();

                  function openNav() {
                      document.getElementById("mySidenav").style.width = "250px";
                      document.getElementById("main").style.marginLeft = "250px";
                      document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
                  }

                  function closeNav() {
                      document.getElementById("mySidenav").style.width = "0";
                      document.getElementById("main").style.marginLeft= "0";
                      document.body.style.backgroundColor = "white";
                  }

                  function load_datos_depencencia(nom_dep) {

                    var element = document.getElementById("nom_dep_selec");

                    if(nom_dep=="modulos")
                    {
                      //alert("open nueva ventana");
                      $('#desabilitar_modulos').show();
                      $('#crear_pdf').hide();
                     // element.innerHTML = "SECRETARÍA DE CONTRALORIA";
                      //reload_data(nom_dep);
                    }
                    if(nom_dep=="pdf")
                    {
                      //alert("open nueva ventana");
                      $('#desabilitar_modulos').hide();
                      $('#crear_pdf').show();
                     // element.innerHTML = "SECRETARÍA DE CONTRALORIA";
                      //reload_data(nom_dep);
                    }



                    closeNav();
          
                  }

                  function reload_data(nom_dependecia)
                  {
                    alert(nom_dependecia); 
                  }


                </script>

              <?php endwhile; ?>
            </div>
          </div>
        </div>

      </section>


     


<?php get_footer(); ?>
