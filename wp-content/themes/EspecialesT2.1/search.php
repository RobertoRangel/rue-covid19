<?php 
$search_refer = $_GET["s"];
if ($search_refer == 'portfolio') { load_template(TEMPLATEPATH . '/search-custom.php'); }
else {
get_header(); ?>
<div id="wrapper">
	<h1>Resultados para <?php the_search_query(); ?></h1>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div class="dos-tercios listado">
	<h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>	
	<?php the_excerpt(); ?>
	<?php the_time('F jS, Y') ?> | <?php the_author() ?>
	</div><!-- end of dos-tercios -->
<?php endwhile; else: ?>
<h2>No encontrado</h2>
<p>Lo sentimos, intente utilizar nuestro formulario de b&uacute;squedas.</p>
<?php endif; ?>
<div class="navigation"><?php posts_nav_link('','<p align="right">Siguientes</p>','<p align="left">Anteriores</p>'); ?></div>
</div><!-- end of wrapper-->
<?php get_sidebar(); get_footer(); } ?>