
<?php 


header('Content-Type: text/html; charset=UTF-8');


function definicion_evidencias_guardar_pid() {


  global $wpdb;
  if(isset($_POST['enviar_form_pid']) && $_POST['oculto'] == "1" ):
     
  
  $nombre_indicador_pid = sanitize_text_field( $_POST['nombre_indicador_pid'] ); 
  $nom_dependencia_pid = sanitize_text_field( $_POST['nom_dependencia_pid'] );
  $nombre_organismo_pid = sanitize_text_field( $_POST['nom_organismo_pid'] ); 
  
  $descrp_tac_pid = sanitize_text_field( $_POST['descrp_tac_pid'] ); 
  $unidad_observacion_ind_pid = sanitize_text_field( $_POST['unidad_observacion_ind_pid'] );
  $tipo_indicador_def_pid = sanitize_text_field( $_POST['tipo_indicador_def_pid'] ); 

  $otro_tipo_ind_pid = sanitize_text_field( $_POST['otro_tipo_ind_pid']); 
  $tendencia_esp_pid = sanitize_text_field( $_POST['tendencia_esp_pid']); 
  $obj_especifico_prog_inst = sanitize_text_field( $_POST['obj_especifico_prog_inst']); 
  $base_calculo_pid = sanitize_text_field( $_POST['base_calculo_pid']); 
  $periodicidad_pid = sanitize_text_field( $_POST['periodicidad_pid']);

  $fuente_pid = sanitize_text_field( $_POST['fuente_pid']);
  $ref_adicionales = sanitize_text_field( $_POST['ref_adicionales']);
  $linea_base_absolutos_pid = sanitize_text_field( $_POST['linea_base_absolutos_pid']);
  $linea_base_2017_pid = sanitize_text_field( $_POST['linea_base_2017_pid']);
 // $meta_2017_pid = sanitize_text_field( $_POST['meta_2017_pid']);

  $meta_2018_pid = sanitize_text_field( $_POST['meta_2018_pid']);
  $meta_2019_pid = sanitize_text_field( $_POST['meta_2019_pid']);
  $meta_2020_pid = sanitize_text_field( $_POST['meta_2020_pid']);
  $meta_2021_pid = sanitize_text_field( $_POST['meta_2021_pid']);
  $meta_2022_pid = sanitize_text_field( $_POST['meta_2022_pid']);
  $meta_2030_pid = sanitize_text_field( $_POST['meta_2030_pid']);


  $nom_usuario_pid = sanitize_text_field( $_POST['nom_usuario_pid']);
 
  $indica_informe = sanitize_text_field( $_POST['indica_informe']);
 
  $eje_ped_pid = sanitize_text_field( $_POST['eje_ped_pid']);
    
  $obj_estr_pid = sanitize_text_field( $_POST['obj_estr_pid']);
  $obj_gen_pid = sanitize_text_field( $_POST['obj_gen_pid']);

  $avance_inf_2017 = sanitize_text_field( $_POST['avance_inf_2017']);
  $descripcion_observacion_ficha_tecnica_pid = sanitize_text_field( $_POST['descripcion_observacion_ficha_tecnica_pid']);
  $tipo_evidencia_pid = sanitize_text_field( $_POST['tipo_evidencia_def_list_pid']);
  $tipo_evi_otro_pid = sanitize_text_field( $_POST['tipo_evi_otro_pid']);
  $existe_expediente_doc_pid = sanitize_text_field( $_POST['existe_expediente_doc_pid']);


  $serie_evidencia_pid = sanitize_text_field( $_POST['serie_evidencia_pid']);
  $seccion_pid = sanitize_text_field( $_POST['seccion_pid']);
  $guia_invetario_pid = sanitize_text_field( $_POST['guia_invetario_pid']);
  $medios_verificacion_evidencia_pid = sanitize_text_field( $_POST['medios_verificacion_evidencia_pid']);
  $fecha_corte_evidencia_pid_inicio = sanitize_text_field( $_POST['datepicker_pid_inicio']);
  $fecha_corte_evidencia_pid_fin = sanitize_text_field( $_POST['datepicker_pid_fin']);


  $comentarios_pid = sanitize_text_field( $_POST['comentarios_pid']);
  $estatus_pid = 0;
  $logros_en_indicador_pid = sanitize_text_field( $_POST['logros_en_indicador_pid']);
  $id_bullet_Del_logro_pid = sanitize_text_field( $_POST['id_bullet_Del_logro_pid']);
  $premios_recono_pid = sanitize_text_field( $_POST['premios_recono_pid']);
  $reconocimiento_pid = sanitize_text_field( $_POST['reconocimiento_pid']);

  $resultado_1_pid = sanitize_text_field( $_POST['resultado_1_pid']);
  $resultado_2_pid = sanitize_text_field( $_POST['resultado_2_pid']);
  $resultado_3_pid = sanitize_text_field( $_POST['resultado_3_pid']);
  $resultado_4_pid = sanitize_text_field( $_POST['resultado_4_pid']);
  $resultado_5_pid = sanitize_text_field( $_POST['resultado_5_pid']);

  $res1_lugares_pid = sanitize_text_field( $_POST['res1_lugares_pid']);
  $res2_lugares_pid = sanitize_text_field( $_POST['res2_lugares_pid']);
  $res3_lugares_pid = sanitize_text_field( $_POST['res3_lugares_pid']);
  $res4_lugares_pid = sanitize_text_field( $_POST['res4_lugares_pid']);
  $res5_lugares_pid = sanitize_text_field( $_POST['res5_lugares_pid']);

  $lo_que_sigue_1 = sanitize_text_field( $_POST['lo_que_sigue_1_pid']);
  $lo_que_sigue_2 = sanitize_text_field( $_POST['lo_que_sigue_2_pid']);
  $lo_que_sigue_3 = sanitize_text_field( $_POST['lo_que_sigue_3_pid']);

  $resultado_pid_2018 = sanitize_text_field( $_POST['resultado_pid_2018']);
 $descripcion_evidencia_pid = sanitize_text_field( $_POST['descripcion_evidencia_pid']);




/*

  echo($nombre_indicador_pid."\n");
  echo($nom_dependencia_pid."\n");
  echo($nombre_organismo_pid."\n");

  echo($descrp_tac_pid."\n");
  echo($unidad_observacion_ind_pid."\n");
  echo($tipo_indicador_def_pid."\n");

  echo($otro_tipo_ind_pid."\n");
  echo($tendencia_esp_pid."\n");
  echo($obj_especifico_prog_inst."\n");
  echo($base_calculo_pid."\n");
  echo($periodicidad_pid."\n");

  echo($fuente_pid."\n");
  echo($ref_adicionales."\n");
  echo($linea_base_absolutos_pid."\n");
  echo($linea_base_2017_pid."\n");



  echo($meta_2018_pid."\n");
  echo($meta_2019_pid."\n");
  echo($meta_2020_pid."\n");
  echo($meta_2021_pid."\n");
  echo($meta_2022_pid."\n");
  echo($meta_2030_pid."\n");



  echo($nom_usuario_pid."\n");
  echo($indica_informe."\n");
  echo($eje_ped_pid."\n");
  echo($obj_estr_pid."\n");
  echo($obj_gen_pid."\n");

  echo($nombre_indicador_pid."\n");
  echo($nom_dependencia_pid."\n");
  echo($nombre_indicador_pid."\n");
  echo($nom_dependencia_pid."\n");
  echo($nombre_indicador_pid."\n");
  echo($nom_dependencia_pid."\n");


  echo($avance_inf_2017."\n");
  echo($descripcion_observacion_ficha_tecnica_pid."\n");
  echo($tipo_evidencia_pid."\n");
  echo($tipo_evi_otro_pid."\n");
  echo($existe_expediente_doc_pid."\n");

  echo($serie_evidencia_pid."\n");
  echo($seccion_pid."\n");
  echo($guia_invetario_pid."\n");
  echo($medios_verificacion_evidencia_pid."\n");
  echo($fecha_corte_evidencia_pid."\n");

   echo($comentarios_pid."\n");
  echo($estatus_pid."\n");
  echo($logros_en_indicador_pid."\n");
  echo($id_bullet_Del_logro_pid."\n");
  echo($premios_recono_pid."\n");
   echo($reconocimiento_pid."\n");


echo($resultado_pid_2018."\n");

*/


        $tabla = $wpdb->prefix . "indicadores_pids";
        $datos = array(
          
          'nom_indicador_pid'   => $nombre_indicador_pid,
          'nom_dependencia_pid' => $nom_dependencia_pid,
          'nom_organismo_pid'   => $nombre_organismo_pid,
          
          'descripcion_indicador_pid'  => $descrp_tac_pid,
          'unidad_observacion_ind_pid' => $unidad_observacion_ind_pid,
          'tipo_ind_pid'        => $tipo_indicador_def_pid,

          'otro_tipo_ind_pid'   => $otro_tipo_ind_pid,
          'tendencia_pid'       => $tendencia_esp_pid,
          'obj_esp_prog_inst_pid'      => $obj_especifico_prog_inst,
          'base_calculo_pid'    => $base_calculo_pid,
          'periodicidad_pid'    => $periodicidad_pid,

          'fuente_pid'          => $fuente_pid,
          'referencias_Adi_pid'     => $ref_adicionales,
          'linea_base_absolutos_pid'    => $linea_base_absolutos_pid,
          'linea_base_2017_pid'    => $linea_base_2017_pid,
         // 'meta_2017_pid'    => $meta_2017_pid,

          'meta_2018_pid'    => $meta_2018_pid,
          'meta_2019_pid'    => $meta_2019_pid,
          'meta_2020_pid'    => $meta_2020_pid,
          'meta_2021_pid'    => $meta_2021_pid,
          'meta_2022_pid'    => $meta_2022_pid,
          'meta_2030_pid'    => $meta_2030_pid,

          'nombre_usuario_pid'    => $nom_usuario_pid,

  
          'indica_informe_pid'    => $indica_informe,
          
          'eje_ped_pid'           => $eje_ped_pid,
           
          'obj_estrategico_pid'   => $obj_estr_pid,
          'obj_general_pid'       => $obj_gen_pid,
           
          'avance_informe_pid'       => $avance_inf_2017,
          'descripcion_observacion_ficha_tecnica_def_pid' => $descripcion_observacion_ficha_tecnica_pid,
          'tipo_evidencia_pid' => $tipo_evidencia_pid,
          'tipo_evidencia_otro_pid' => $tipo_evi_otro_pid,
          'existe_expediente_doc_pid' => $existe_expediente_doc_pid,
        

          'serie_evidencia_pid' => $serie_evidencia_pid,
          'seccion_pid' => $seccion_pid,
          'guia_invetario_expediente_pid' => $guia_invetario_pid,
          'medios_verificacion_evidencia_pid' => $medios_verificacion_evidencia_pid,
          'fecha_registro_pid_inicio' => $fecha_corte_evidencia_pid_inicio,
          'fecha_registro_pid_fin' => $fecha_corte_evidencia_pid_fin,

       
    
          'comentarios_pid' => $comentarios_pid,
          'estatus_pid' => $estatus_pid,
          'logros_indicador_pid' => $logros_en_indicador_pid,
          'bullet_logro_pid' => $id_bullet_Del_logro_pid,
          'hubo_premio_pid' => $premios_recono_pid,
          'premio_reconocimiento_pid' => $reconocimiento_pid,
         
          'resultado1_pid' => $resultado_1_pid,
          'resultado2_pid' => $resultado_2_pid,
          'resultado3_pid' => $resultado_3_pid,
          'resultado4_pid' => $resultado_4_pid,
          'resultado5_pid' => $resultado_5_pid,

          'lug_result1_pid' => $res1_lugares_pid,
          'lug_result2_pid' => $res2_lugares_pid,
          'lug_result3_pid' => $res3_lugares_pid,
          'lug_result4_pid' => $res4_lugares_pid,
          'lug_result5_pid' => $res5_lugares_pid,

          'lo_que_sigue_1_pid' => $lo_que_sigue_1,
          'lo_que_sigue_2_pid' => $lo_que_sigue_2,
          'lo_que_sigue_3_pid' => $lo_que_sigue_3,
          
          'resultado_pid_2018' => $resultado_pid_2018,
	        'descripcion_evidencia_pid' => $descripcion_evidencia_pid,
	
         

          
        );
        
        $formato = array(
          
          
          '%s',  //nombre_indicador_pid
          '%s', //nom_dependencia
          '%s', //nombre_organismo_pid

          '%s', //descrp_tac_pid
          '%s', //unidad_observacion_ind_pid
          '%s', //tipo_indicador_def_pid

          '%s', //otro_tipo_ind_pid
          '%s', //tendencia_esp_pid
          '%s', //obj_especifico_prog_inst
          '%s', //base_calculo_pid
          '%s', //periodicidad_pid

          '%s', //fuente_pid
          '%s', //ref_adicionales
          '%s', //linea_base_absolutos_pid
          '%s', //linea_base_2017_pid
        // '%s', //meta_2017_pid

          '%s', //meta_2018_pid
          '%s', //meta_2019_pid
          '%s', //meta_2020_pid
          '%s', //meta_2021_pid
          '%s', //meta_2022_pid
          '%s', //meta_2030_pid
         
     

          '%s', //nom_usuario_pid
              
          '%s', //indica_informe

          '%s', //eje_ped_pid
           
          '%s', //obj_estr_pid
          '%s', //obj_gen_pid
    
          '%s', //avance_inf_2017
          '%s', //descripcion_observacion_ficha_tecnica_pid
          '%s', //tipo_evidencia_pid
          '%s', //tipo_evi_otro_pid
          '%s', //existe_expediente_doc_pid
    
   
          '%s', //serie_evidencia_pid
          '%s', //seccion_pid
          '%s', //guia_invetario_pid
          '%s', //medios_verificacion_evidencia_pid
          '%s', //fecha_corte_evidencia_pid_inicio
          '%s', //fecha_corte_evidencia_pid_fin




          '%s', //comentarios_pid
          '%d', //estatus_pid
          '%s', //logros_en_indicador_pid
          '%s', //id_bullet_Del_logro_pid
          '%s', //premios_recono_pid
          '%s', //reconocimiento_pid
           
          '%s', //resultado_1_pid
          '%s', //resultado_2_pid
          '%s', //resultado_3_pid
          '%s', //resultado_4_pid
          '%s', //resultado_5_pid

          '%s', //lug_result1_pid
          '%s', //lug_result2_pid
          '%s', //lug_result3_pid
          '%s', //lug_result4_pid
          '%s', //lug_result5_pid

          '%s', //lo_que_sigue_1
          '%s', //lo_que_sigue_2
          '%s', //lo_que_sigue_3
           

          '%s', //resultado_pid_2018
		      '%s' //descripcion_evi_pid
        

          

        );
          $result=  $wpdb->insert($tabla, $datos, $formato);
           

        if($result==1)
       {
          $tabla_up = $wpdb->prefix . "tacticos_pids";        //actualizar la tabla_up donde estan los nombres de los indicadores
          $datos_up = array(
            'Estatus' => 1
          );
          $formato_up = array(
            '%d'
          );
          $where = array(
            'nom_indicador' => $nombre_indicador_pid, 
	//   'relation'=>'AND',
            'nombre_usuario' => $nom_usuario_pid
          );

         $result_update =$wpdb->update($tabla_up, $datos_up,$where,$formato_up );
       
          echo $result_update;
          $wpdb->last_query;                                //insertar los datos del formulario
          echo '<br>Result is '.$wpdb->last_result;
          echo '<br>Error is '.$wpdb->last_error;   

       //  echo $result_update;
         // echo ($wpdb->last_error);

        $url = get_page_by_title('Cargado Correctamente');
         wp_redirect( get_permalink( $url->ID ) );

       }
        else
        {
        // echo("no se guardo".mysqli_error($kv));
           $url = get_page_by_title('contacto_admin');
           wp_redirect( get_permalink( $url->ID ) );
        }



  
        
      
      exit();
      
        
  endif;




}

add_action('init', 'definicion_evidencias_guardar_pid');
