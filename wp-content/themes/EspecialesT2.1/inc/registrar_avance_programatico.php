
<?php 

function definicion_avance_programatico() {


  global $wpdb;
  if(isset($_POST['enviar_form_avance_programatico']) && $_POST['oculto_avance'] == "1"):

  $inversion_autorizada_fin = sanitize_text_field( $_POST['inversion_autorizada_fin']);
  $nom_dependecia = sanitize_text_field( $_POST['nombre_dependencia_avance']);
  $inversion_ejercida_fin = sanitize_text_field( $_POST['inversion_ejercida_fin']);
  $inversion_ejercida_pro = sanitize_text_field( $_POST['inversion_ejercida_pro']);
  $inversion_autorizada_pro = sanitize_text_field( $_POST['inversion_autorizada_pro']);

  $nom_prog_presupuestario = sanitize_text_field( $_POST['nom_prog_presupuestario']); 
  $desc_general_fin = sanitize_text_field( $_POST['desc_general_fin']);
  $indicador_fin = sanitize_text_field( $_POST['indicador_fin']);
  $meta_prog_2018_fin = sanitize_text_field( $_POST['meta_prog_2018_fin'] );
  $res_segundo_trimestre_2018 = sanitize_text_field( $_POST['res_segundo_trimestre_2018']);
  $result_estim_30_jun_fin = sanitize_text_field( $_POST['result_estim_30_jun_fin']); 

  $desc_general_prop = sanitize_text_field( $_POST['desc_general_prop']);
  $indicador_prop = sanitize_text_field( $_POST['indicador_prop']);
  $meta_prog_2018_prop = sanitize_text_field( $_POST['meta_prog_2018_prop'] );
  $result_estim_30_jun_prop = sanitize_text_field( $_POST['result_estim_30_jun_prop']);
  $avance_porc_meta_2018 = sanitize_text_field( $_POST['avance_porc_meta_2018']);
  $avance_fin_meta_2018 = sanitize_text_field( $_POST['avance_fin_meta_2018']);

  $sector_fin = sanitize_text_field( $_POST['sector_fin'] );
  $unidad_pres_fin = sanitize_text_field( $_POST['unidad_pres_fin']);
  $nivel_fin = sanitize_text_field( $_POST['nivel_fin']);
  $nivel_prop = sanitize_text_field( $_POST['nivel_prop']);
  $eje_ped = sanitize_text_field( $_POST['eje_ped']);

  $status =5;
  $fecha_registro =date('Y-m-d');
  

  
  $tabla = $wpdb->prefix ."avance_programatico";
  $datos = array(

      'inversion_autorizada_fin_2017' => $inversion_autorizada_fin,
      'inversion_ejercida_fin_2017' => $inversion_ejercida_fin,
      'inversion_autorizada_fin_2018' => $inversion_ejercida_pro,
      'inversion_ejercida_fin_2018' => $inversion_autorizada_pro,

      'nom_prog_presupuestario' => $nom_prog_presupuestario,
      'desc_general_fin' => $desc_general_fin,
      'indicador_fin' => $indicador_fin,
      'meta_prog_2018_fin' => $meta_prog_2018_fin,
      'result_estim_30_jun_fin' => $result_estim_30_jun_fin,

      'desc_general_prop' => $desc_general_prop,
      'indicador_prop' => $indicador_prop,
      'meta_prog_2018_prop' => $meta_prog_2018_prop,
      'result_estim_30_jun_prop' => $result_estim_30_jun_prop,
      'avance_porc_meta_2018' => $avance_porc_meta_2018,
      'avance_fin_meta_2018' => $avance_fin_meta_2018,

      'sector_fin' => $sector_fin,
      'unidad_pres_fin' => $unidad_pres_fin,
      'nivel_fin' => $nivel_fin,
      'nivel_prop' => $nivel_prop,
      'status' => $status,
      'eje' => $eje_ped,
      /*

     
      'desc_general_prop' => $desc_general_prop,
      'indicador_prop' => $indicador_prop,
      'meta_prog_2018_prop' => $meta_prog_2018_prop,
      'result_estim_30_jun_prop' => $result_estim_30_jun_prop,
      'avance_porc_meta_2018' => $avance_porc_meta_2018,
      */
      'nomb_dependencia' => $nom_dependecia,
      'status' => $status,
      'fecha_registro' => $fecha_registro,
      
     
 
    );
    
    $formato = array(
     /*
      '%s',//nombre del inidcador
      '%s',//indicador tactico vinculado
      '%s',//presupuesto ejercido
      '%s',//meta_2018
      '%s',//res_segundo_trimestre_2018
      '%s',//nomb_depenedencia
      '%s',//programa presupuestario
      '%s',//nombre del inidcador
      '%s',//indicador tactico vinculado
      '%s',//presupuesto ejercido
      */
      '%s',//nombre del inidcador
      '%s',//nombre del inidcador
      '%s',//nombre del inidcador
      '%s',//indicador tactico vinculado
      '%s',//presupuesto ejercido
      '%s',//presupuesto ejercido

      '%s',//nomb_depenedencia
      '%s',//programa presupuestario
      '%s',//nombre del inidcador
      '%s',//indicador tactico vinculado
      '%s',//presupuesto ejercido
      '%s',//presupuesto ejercido

      '%s',//nomb_depenedencia
      '%s',//programa presupuestario
      '%s',//nombre del inidcador
      '%s',//indicador tactico vinculado
      '%s',//presupuesto ejercido
      '%s',//meta_2018
      '%s',//res_segundo_trimestre_2018
      '%s',//nomb_depenedencia
      '%s',//nomb_depenedencia
      '%s',//nomb_depenedencia
      '%s',//nomb_depenedencia
      '%s'//fecha_efectivo
    );  
 
    $result=  $wpdb->insert($tabla, $datos, $formato);
      echo $result;
    $wpdb->last_query;                                //insertar los datos del formulario


        $tabla_up = $wpdb->prefix ."programa_presupuestario";        //actualizar la tabla_up donde estan los nombres de los indicadores
        $datos_up = array(
          'status' => 1,
        );
        $formato_up = array(
          '%d'
        );
        $where = array(
          'programa_presupuestario' => $nom_prog_presupuestario
        );

        $wpdb->update($tabla_up, $datos_up,$where,$formato_up );


        $url = get_page_by_title('evidencias');
        wp_redirect( get_permalink( $url->ID ) );


       
        
      exit();
        
  endif;
  


  }


add_action('init', 'definicion_avance_programatico');

