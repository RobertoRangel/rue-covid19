<?php


header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');



$kv = new mysqli('localhost', 'oscar', 'osocerdo', 'sigeh');

$kv->set_charset("utf8");
if ($kv->connect_error) 
{ // Check connection
 die("Connection failed: " . $kv->connect_error);
} 
else
{

	$id_indicador=$_POST['id_indicador'];
	
	$nom_indicador=$_POST['nom_indicador'];

/* -------------------------------------------------- Datos Complementarios para el Avance Programatico ------------------------------  */
	$consultaAP = "SELECT nom_prog_presupuestario FROM wp_avance_programatico WHERE id_avance_programatico = '$id_indicador'"; /* 01 - A */ 
	$nombre_AP1 = $kv->query($consultaAP);
	$nombre__AP01 = mysqli_fetch_array($nombre_AP1);
	$nombreAP = $nombre__AP01[0];

	$consulta2 = "SELECT sector_fin FROM wp_avance_programatico WHERE id_avance_programatico = '$id_indicador'"; /* 03 - C */ 
	$nombre_03 = $kv->query($consulta2);
	$nombre__03 = mysqli_fetch_array($nombre_03);
	$dependenciaAP = $nombre__03[0];

	$consulta3 = "SELECT nomb_dependencia FROM wp_avance_programatico WHERE id_avance_programatico = '$id_indicador'"; /* 04 - D */ 
	$nombre_04 = $kv->query($consulta3);
	$nombre__04 = mysqli_fetch_array($nombre_04);
	$usuarioAP = $nombre__04[0];	
/* -------------------------------------------------- ----------------------- ----------------------- ------------------------------  */
/* -------------------------------------------------- Datos Complementarios para Acciones sin Indicador ------------------------------  */
	$consulta_AI1 = "SELECT objetivo_est FROM wp_acciones_sin_indicador WHERE id_accion_sin_ind = '$id_indicador'"; /* 01 - A */ 
	$eje_AI01 = $kv->query($consulta_AI1);
	$eje__AI01 = mysqli_fetch_array($eje_AI01);
	$ejeAI = $eje__AI01[0];

	$consulta_AI2 = "SELECT accion_1 FROM wp_acciones_sin_indicador WHERE id_accion_sin_ind = '$id_indicador'"; /* 02 - B */ 
	$accion_AI02 = $kv->query($consulta_AI2);
	$accion__AI02 = mysqli_fetch_array($accion_AI02);
	$accionAI = $accion__AI02[0];

	$consulta_AI3 = "SELECT dependencia FROM wp_acciones_sin_indicador WHERE id_accion_sin_ind = '$id_indicador'"; /* 03 - C */ 
	$dep_AI03 = $kv->query($consulta_AI3);
	$dep__AI03 = mysqli_fetch_array($dep_AI03);
	$dependenciaAI = $dep__AI03[0];
/* -------------------------------------------------- ----------------------- ----------------------- ------------------------------  */
/* -------------------------------------------------- Datos Complementarios para Indicadores Tacticos ------------------------------  */
	$consulta_2 = "SELECT nombre_indicador_asociado_def FROM wp_definicion_evidencias WHERE id_indicador_asociado_def = '$id_indicador'"; /* 01 - A */ 
	$nomm_02 = $kv->query($consulta_2);
	$nomm__02 = mysqli_fetch_array($nomm_02);
	$nombre01 = $nomm__02[0];

	$consulta_1 = "SELECT eje_ped FROM wp_definicion_evidencias WHERE id_indicador_asociado_def = '$id_indicador'"; /* 01 - A */ 
	$eje_01 = $kv->query($consulta_1);
	$eje__01 = mysqli_fetch_array($eje_01);
	$eje01 = $eje__01[0];

	/*wp_indicadores_estrategicos_definidos*/

	$consulta_3 = "SELECT nombre_dependencia_def FROM wp_definicion_evidencias WHERE id_indicador_asociado_def = '$id_indicador'"; /* 03 - C */ 
	$dep_03 = $kv->query($consulta_3);
	$dep__03 = mysqli_fetch_array($dep_03);
	$dependencia01 = $dep__03[0];
/* -------------------------------------------------- ----------------------- ----------------------- ------------------------------  */
/* -------------------------------------------------- Datos Complementarios para Acciones Concurrentes ------------------------------  */
	$consulta_AC = "SELECT eje_principal FROM wp_acciones_concurrentes WHERE id_accion_concurrente = '$id_indicador'"; /* 01 - A */ 
	$eje01_AC = $kv->query($consulta_AC);
	$eje__AC = mysqli_fetch_array($eje01_AC);
	$ejeAC = $eje__AC[0];

	$consulta_AC1 = "SELECT nombre_accion FROM wp_acciones_concurrentes WHERE id_accion_concurrente = '$id_indicador'"; /* 01 - A */ 
	$acc01_AC = $kv->query($consulta_AC1);
	$acc__AC = mysqli_fetch_array($acc01_AC);
	$accionAC = $acc__AC[0];

	$consulta_AC2 = "SELECT nomb_dependencia FROM wp_acciones_concurrentes WHERE id_accion_concurrente = '$id_indicador'"; /* 03 - C */ 
	$dep_AC = $kv->query($consulta_AC2);
	$dep__AC = mysqli_fetch_array($dep_AC);
	$dependenciaAC = $dep__AC[0];
/* -------------------------------------------------- ----------------------- ----------------------- ------------------------------  */
/* -------------------------------------------------- Datos Complementarios para Indicadores Estrategicos ------------------------------  */
	$consulta_IE = "SELECT eje_ped FROM wp_indicadores_estrategicos_definidos WHERE id_ind_estrategico = '$id_indicador'"; /* 01 - A */ 
	$eje01_IE = $kv->query($consulta_IE);
	$eje_IE = mysqli_fetch_array($eje01_IE);
	$ejeIE = $eje_IE[0];

	$consulta_IE1 = "SELECT nom_indicador FROM wp_indicadores_estrategicos_definidos WHERE id_ind_estrategico = '$id_indicador'"; /* 01 - A */ 
	$acc01_IE = $kv->query($consulta_IE1);
	$acc__IE = mysqli_fetch_array($acc01_IE);
	$nombreIE = $acc__IE[0];

	$consulta_IE2 = "SELECT dependecia_encargada FROM wp_indicadores_estrategicos_definidos WHERE id_ind_estrategico = '$id_indicador'"; /* 03 - C */ 
	$dep_IE = $kv->query($consulta_IE2);
	$dep__IE = mysqli_fetch_array($dep_IE);
	$dependenciaIE = $dep__IE[0];
/* -------------------------------------------------- ----------------------- ----------------------- ------------------------------  */




/*_ INDICADORES TACTICOS_____________________________________________________________________________________________________________________________________________________________*/
	if(isset($_POST['edit_linea_discursiva'])) /* ---- INDICADOR ---- */ 
	{
		$edit_linea_discursiva=$_POST['edit_linea_discursiva'];
		$edit_ind_estrella=$_POST['edit_ind_estrella'];
		$logro_indicador=$_POST['logro_indicador'];
		$bullet_logro=$_POST['bullet_logro'];
		$hubo_premio=$_POST['hubo_premio'];
		$premio_reconocimiento=$_POST['premio_reconocimiento'];

/* COMPARACION ____________________________________*/
		$com01 = "SELECT linea_discursiva FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 01 - A */ 
		$com_01 = $kv->query($com01);
		$com__01 = mysqli_fetch_array($com_01);
		$compara01 = $com__01[0];

		$com02 = "SELECT indicador_estrella FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 02 - B */ 
		$com_02 = $kv->query($com02);
		$com__02 = mysqli_fetch_array($com_02);
		$compara02 = $com__02[0];

		$com03 = "SELECT logros_indicador FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 03 - C */ 
		$com_03 = $kv->query($com03);
		$com__03 = mysqli_fetch_array($com_03);
		$compara03 = $com__03[0];

		$com04 = "SELECT bullet_logro FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 04- D */ 
		$com_04 = $kv->query($com04);
		$com__04 = mysqli_fetch_array($com_04);
		$compara04 = $com__04[0];

		$com05 = "SELECT hubo_premio FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 05 - E */ 
		$com_05 = $kv->query($com05);
		$com__05 = mysqli_fetch_array($com_05);
		$compara05 = $com__05[0];

		$com06 = "SELECT premio_reconocimiento FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 06 - F */ 
		$com_06 = $kv->query($com06);
		$com__06 = mysqli_fetch_array($com_06);
		$compara06 = $com__06[0];


		if($compara01 != $edit_linea_discursiva)
		{
			$dato01 = "Linea Discursiva";
			$campo01 = "linea_discursiva";
			$fecha_cambio01 = date('Y-m-d');
			$hora_cambio01 = date('h:i:s');
			$historial01 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio01','$hora_cambio01','$eje01','$dependencia01','$dato01','$campo01','$edit_linea_discursiva')");
		}

		if($compara02 != $edit_ind_estrella)
		{
			$dato02 = "Categoria del Indicador";
			$campo02 = "indicador_estrella";
			$fecha_cambio02 = date('Y-m-d');
			$hora_cambio02 = date('h:i:s');
			$historial02 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio02','$hora_cambio02','$eje01','$dependencia01','$dato02','$campo02','$edit_ind_estrella')");
		}

		if($compara03 != $logro_indicador)
		{
			$dato03 = "LOGROS";
			$campo03 = "logros_indicador";
			$fecha_cambio03 = date('Y-m-d');
			$hora_cambio03 = date('h:i:s');
			$historial03 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio03','$hora_cambio03','$eje01','$dependencia01','$dato03','$campo03','$logro_indicador')");
		}

		if($compara04 != $bullet_logro)
		{
			$dato04 = "Escribir el bullet del logro:";
			$campo04 = "bullet_logro";
			$fecha_cambio04 = date('Y-m-d');
			$hora_cambio04 = date('h:i:s');
			$historial04 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio04','$hora_cambio04','$eje01','$dependencia01','$dato04','$campo04','$bullet_logro')");
		}

		if($compara05 != $hubo_premio)
		{
			$dato05 = "¿Hubo premios o reconocimientos?";
			$campo05 = "hubo_premio";
			$fecha_cambio05 = date('Y-m-d');
			$hora_cambio05 = date('h:i:s');
			$historial05 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio05','$hora_cambio05','$eje01','$dependencia01','$dato05','$campo05','$hubo_premio')");
		}

		if($compara06 != $premio_reconocimiento)
		{
			$dato06 = "Descripcion Premio";
			$campo06 = "premio_reconocimiento";
			$fecha_cambio06 = date('Y-m-d');
			$hora_cambio06 = date('h:i:s');
			$historial06 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio06','$hora_cambio06','$eje01','$dependencia01','$dato06','$campo06','$premio_reconocimiento')");
		}

/*____________________________________*/ 
/*------ INSERCION DE LA ACTUALIZACION ------- */ 

		$result=mysqli_query($kv, "UPDATE wp_definicion_evidencias SET linea_discursiva='$edit_linea_discursiva',indicador_estrella='$edit_ind_estrella',logros_indicador='$logro_indicador', bullet_logro='$bullet_logro',hubo_premio='$hubo_premio',premio_reconocimiento='$premio_reconocimiento'  WHERE id_indicador_asociado_def='$id_indicador'");

	}
	
	else if(isset($_POST['resultado1'])) /* ---- OBRAS Y ACCIONES ---- */ 
	{
		$resultado1=$_POST['resultado1'];
		$resultado2=$_POST['resultado2'];
		$resultado3=$_POST['resultado3'];
		$resultado4=$_POST['resultado4'];
		$resultado5=$_POST['resultado5'];
		$resultado6=$_POST['resultado6'];
		$resultado7=$_POST['resultado7'];
		$resultado8=$_POST['resultado8'];

		$reslug1=$_POST['reslug1'];
		$reslug2=$_POST['reslug2'];
		$reslug3=$_POST['reslug3'];
		$reslug4=$_POST['reslug4'];
		$reslug5=$_POST['reslug5'];
		$reslug6=$_POST['reslug6'];
		$reslug7=$_POST['reslug7'];
		$reslug8=$_POST['reslug8'];

		$reslug1_gps=$_POST['reslug1_gps'];
		$reslug2_gps=$_POST['reslug2_gps'];
		$reslug3_gps=$_POST['reslug3_gps'];
		$reslug4_gps=$_POST['reslug4_gps'];
		$reslug5_gps=$_POST['reslug5_gps'];
		$reslug6_gps=$_POST['reslug6_gps'];
		$reslug7_gps=$_POST['reslug7_gps'];
		$reslug8_gps=$_POST['reslug8_gps'];



		




		$loquesigue1=$_POST['loquesigue1'];
		$loquesigue2=$_POST['loquesigue2'];
		$loquesigue3=$_POST['loquesigue3'];

/* COMPARACION ____________________________________*/
		$com01 = "SELECT resultado1 FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 01 - A */ 
		$com_01 = $kv->query($com01);
		$com__01 = mysqli_fetch_array($com_01);
		$compara01 = $com__01[0];

		$com02 = "SELECT resultado2 FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 02 - B */ 
		$com_02 = $kv->query($com02);
		$com__02 = mysqli_fetch_array($com_02);
		$compara02 = $com__02[0];

		$com03 = "SELECT resultado3 FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 03 - C */ 
		$com_03 = $kv->query($com03);
		$com__03 = mysqli_fetch_array($com_03);
		$compara03 = $com__03[0];

		$com04 = "SELECT resultado4 FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 04- D */ 
		$com_04 = $kv->query($com04);
		$com__04 = mysqli_fetch_array($com_04);
		$compara04 = $com__04[0];

		$com05 = "SELECT resultado5 FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 05 - E */ 
		$com_05 = $kv->query($com05);
		$com__05 = mysqli_fetch_array($com_05);
		$compara05 = $com__05[0];

		$com06 = "SELECT resultado6 FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 06 - E */ 
		$com_06 = $kv->query($com06);
		$com__06 = mysqli_fetch_array($com_06);
		$compara06 = $com__06[0];

		$com07 = "SELECT resultado7 FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 07 - E */ 
		$com_07 = $kv->query($com07);
		$com__07 = mysqli_fetch_array($com_07);
		$compara07 = $com__07[0];

		$com08 = "SELECT resultado8 FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 08 - E */ 
		$com_08 = $kv->query($com08);
		$com__08 = mysqli_fetch_array($com_08);
		$compara08 = $com__08[0];

		$com06 = "SELECT lug_result1 FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 06 - F */ 
		$com_06 = $kv->query($com06);
		$com__06 = mysqli_fetch_array($com_06);
		$compara06 = $com__06[0];

		$com07 = "SELECT lug_result2 FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 06 - F */ 
		$com_07 = $kv->query($com07);
		$com__07 = mysqli_fetch_array($com_07);
		$compara07 = $com__07[0];

		$com08 = "SELECT lug_result3 FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 06 - F */ 
		$com_08 = $kv->query($com08);
		$com__08 = mysqli_fetch_array($com_08);
		$compara08 = $com__08[0];

		$com09 = "SELECT lug_result4 FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 06 - F */ 
		$com_09 = $kv->query($com09);
		$com__09 = mysqli_fetch_array($com_09);
		$compara09 = $com__09[0];

		$com10 = "SELECT lug_result5 FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 06 - F */ 
		$com_10 = $kv->query($com10);
		$com__10 = mysqli_fetch_array($com_10);
		$compara10 = $com__10[0];

		$com14 = "SELECT lug_result6 FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 06 - F */ 
		$com_14 = $kv->query($com14);
		$com__14 = mysqli_fetch_array($com_14);
		$compara14 = $com__14[0];

		$com15 = "SELECT lug_result7 FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 06 - F */ 
		$com_15 = $kv->query($com15);
		$com__15 = mysqli_fetch_array($com_15);
		$compara15 = $com__15[0];

		$com16 = "SELECT lug_result8 FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 06 - F */ 
		$com_16 = $kv->query($com16);
		$com__16 = mysqli_fetch_array($com_16);
		$compara16 = $com__16[0];

		$com11 = "SELECT lo_que_sigue_1 FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 06 - F */ 
		$com_11 = $kv->query($com11);
		$com__11 = mysqli_fetch_array($com_11);
		$compara11 = $com__11[0];

		$com12 = "SELECT lo_que_sigue_2 FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 06 - F */ 
		$com_12 = $kv->query($com12);
		$com__12 = mysqli_fetch_array($com_12);
		$compara12 = $com__12[0];

		$com13 = "SELECT lo_que_sigue_3 FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 06 - F */ 
		$com_13 = $kv->query($com13);
		$com__13 = mysqli_fetch_array($com_13);
		$compara13 = $com__13[0];


		if($compara01 != $resultado1)
		{
			$dato01 = "Obras y acciones 1";
			$campo01 = "resultado1";
			$fecha_cambio01 = date('Y-m-d');
			$hora_cambio01 = date('h:i:s');
			$historial01 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio01','$hora_cambio01','$eje01','$dependencia01','$dato01','$campo01','$resultado1')");
		}

		if($compara02 != $resultado2)
		{
			$dato02 = "Obras y acciones 2";
			$campo02 = "resultado2";
			$fecha_cambio02 = date('Y-m-d');
			$hora_cambio02 = date('h:i:s');
			$historial02 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio02','$hora_cambio02','$eje01','$dependencia01','$dato02','$campo02','$resultado2')");
		}

		if($compara03 != $resultado3)
		{
			$dato03 = "Obras y acciones 3";
			$campo03 = "resultado3";
			$fecha_cambio03 = date('Y-m-d');
			$hora_cambio03 = date('h:i:s');
			$historial03 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio03','$hora_cambio03','$eje01','$dependencia01','$dato03','$campo03','$resultado3')");
		}

		if($compara04 != $resultado4)
		{
			$dato04 = "Obras y acciones 4";
			$campo04 = "resultado4";
			$fecha_cambio04 = date('Y-m-d');
			$hora_cambio04 = date('h:i:s');
			$historial04 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio04','$hora_cambio04','$eje01','$dependencia01','$dato04','$campo04','$resultado4')");
		}

		if($compara05 != $resultado5)
		{
			$dato05 = "Obras y acciones 5";
			$campo05 = "resultado5";
			$fecha_cambio05 = date('Y-m-d');
			$hora_cambio05 = date('h:i:s');
			$historial05 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio05','$hora_cambio05','$eje01','$dependencia01','$dato05','$campo05','$resultado5')");
		}

		if($compara06 != $reslug1)
		{
			$dato06 = "Definir Lugar 1";
			$campo06 = "reslug1";
			$fecha_cambio06 = date('Y-m-d');
			$hora_cambio06 = date('h:i:s');
			$historial06 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio06','$hora_cambio06','$eje01','$dependencia01','$dato06','$campo06','$reslug1')");
		}

		if($compara07 != $reslug2)
		{
			$dato07 = "Definir Lugar 2";
			$campo07 = "reslug2";
			$fecha_cambio07 = date('Y-m-d');
			$hora_cambio07 = date('h:i:s');
			$historial07 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio07','$hora_cambio07','$eje01','$dependencia01','$dato07','$campo07','$reslug2')");
		}
		
		if($compara08 != $reslug3)
		{
			$dato08 = "Definir Lugar 3";
			$campo08 = "reslug3";
			$fecha_cambio08 = date('Y-m-d');
			$hora_cambio08 = date('h:i:s');
			$historial08 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio08','$hora_cambio08','$eje01','$dependencia01','$dato08','$campo08','$reslug3')");
		}
		
		if($compara09 != $reslug4)
		{
			$dato09 = "Definir Lugar 4";
			$campo09 = "reslug4";
			$fecha_cambio09 = date('Y-m-d');
			$hora_cambio09 = date('h:i:s');
			$historial09 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio09','$hora_cambio09','$eje01','$dependencia01','$dato09','$campo09','$reslug4')");
		}

		if($compara10 != $reslug5)
		{
			$dato10 = "Definir Lugar 5";
			$campo10 = "premio_reconocimiento";
			$fecha_cambio10 = date('Y-m-d');
			$hora_cambio10 = date('h:i:s');
			$historial10 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio10','$hora_cambio10','$eje01','$dependencia01','$dato10','$campo10','$reslug5')");
		}

		if($compara11 != $loquesigue1)
		{
			$dato11 = "LO QUE SIGUE 1";
			$campo11 = "loquesigue1";
			$fecha_cambio11 = date('Y-m-d');
			$hora_cambio11 = date('h:i:s');
			$historial11 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio11','$hora_cambio11','$eje01','$dependencia01','$dato11','$campo11','$loquesigue1')");
		}
		
		if($compara12 != $loquesigue2)
		{
			$dato12 = "LO QUE SIGUE 2";
			$campo12 = "loquesigue2";
			$fecha_cambio12 = date('Y-m-d');
			$hora_cambio12 = date('h:i:s');
			$historial12 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio12','$hora_cambio12','$eje01','$dependencia01','$dato12','$campo12','$loquesigue2')");
		}

		if($compara13 != $loquesigue3)
		{
			$dato13 = "LO QUE SIGUE 3";
			$campo13 = "loquesigue3";
			$fecha_cambio13 = date('Y-m-d');
			$hora_cambio13 = date('h:i:s');
			$historial10 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio13','$hora_cambio13','$eje01','$dependencia01','$dato13','$campo13','$loquesigue3')");
		}
/*____________________________________*/ 
/*------ INSERCION DE LA ACTUALIZACION ------- */ 	


		$result = mysqli_query($kv, "UPDATE wp_definicion_evidencias SET resultado1='$resultado1',resultado2='$resultado2',resultado3='$resultado3',resultado4='$resultado4',resultado5='$resultado5',resultado6='$resultado6',resultado7='$resultado7',resultado8='$resultado8',lo_que_sigue_1='$loquesigue1',lo_que_sigue_2='$loquesigue2',lo_que_sigue_3='$loquesigue3',lug_result1='$reslug1',lug_result2='$reslug2',lug_result3='$reslug3',lug_result4='$reslug4',lug_result5='$reslug5',lug_result6='$reslug6',lug_result7='$reslug7',lug_result8='$reslug8' WHERE id_indicador_asociado_def=$id_indicador");



		


		//echo($nom_indicador);

		if ($result = mysqli_query($kv, "UPDATE wp_obras_acciones SET res1_lugares_gps='$reslug1_gps',res2_lugares_gps='$reslug2_gps',res3_lugares_gps='$reslug3_gps',res4_lugares_gps='$reslug4_gps',res5_lugares_gps='$reslug5_gps',res6_lugares_gps='$reslug6_gps',res7_lugares_gps='$reslug7_gps',res8_lugares_gps='$reslug8_gps' WHERE nombre_indicador = '$nom_indicador'"))
		 {
		 	echo ("sin errror GPS");
 
  		 }
	    else 
	    {
	    	

	    	 echo("Error description: GPS " . mysqli_error($kv));
	    }

		
		
	}

	
	if(isset($_POST['desc_documento_vinculado'])) /* ---- IMPORTANCIA ---- */ 
	{
		$desc_documento_vinculado=$_POST['desc_documento_vinculado'];
		$unidad_observacion=$_POST['unidad_observacion'];
		$desr_ficha_tecnica=$_POST['desr_ficha_tecnica'];
		$tipo_indicador=$_POST['tipo_indicador'];
		$otro_tipo_indicador=$_POST['otro_tipo_indicador'];
	
			/* COMPARACION ____________________________________*/
		$com01 = "SELECT descripcion_indicador_def FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 01 - A */ 
		$com_01 = $kv->query($com01);
		$com__01 = mysqli_fetch_array($com_01);
		$compara01 = $com__01[0];

		$com02 = "SELECT unidad_observacion_indicador_def FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 02 - B */ 
		$com_02 = $kv->query($com02);
		$com__02 = mysqli_fetch_array($com_02);
		$compara02 = $com__02[0];

		$com03 = "SELECT descripcion_observacion_ficha_tecnica_def FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 03 - C */ 
		$com_03 = $kv->query($com03);
		$com__03 = mysqli_fetch_array($com_03);
		$compara03 = $com__03[0];

		$com04 = "SELECT tipo_indicador_def FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 04- D */ 
		$com_04 = $kv->query($com04);
		$com__04 = mysqli_fetch_array($com_04);
		$compara04 = $com__04[0];

		$com05 = "SELECT otro_tipo_indicador_def FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 05 - E */ 
		$com_05 = $kv->query($com05);
		$com__05 = mysqli_fetch_array($com_05);
		$compara05 = $com__05[0];

		if($compara01 != $desc_documento_vinculado)
		{
			$dato01 = "Descripción del indicador, ¿qué mide el indicador?";
			$campo01 = "desc_documento_vinculado";
			$fecha_cambio01 = date('Y-m-d');
			$hora_cambio01 = date('h:i:s');
			$historial01 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio01','$hora_cambio01','$eje01','$dependencia01','$dato01','$campo01','$desc_documento_vinculado')");
		}

		if($compara02 != $unidad_observacion)
		{
			$dato02 = "¿Cuál es la unidad de observación del indicador? (Ej. acciones, población, etc.):";
			$campo02 = "unidad_observacion";
			$fecha_cambio02 = date('Y-m-d');
			$hora_cambio02 = date('h:i:s');
			$historial02 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio02','$hora_cambio02','$eje01','$dependencia01','$dato02','$campo02','$unidad_observacion')");
		}

		if($compara03 != $desr_ficha_tecnica)
		{
			$dato03 = "Da una breve descripción de la unidad de la observación (Consultar la ficha técnica)";
			$campo03 = "desr_ficha_tecnica";
			$fecha_cambio03 = date('Y-m-d');
			$hora_cambio03 = date('h:i:s');
			$historial03 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio03','$hora_cambio03','$eje01','$dependencia01','$dato03','$campo03','$desr_ficha_tecnica')");
		}

		if($compara04 != $tipo_indicador)
		{
			$dato04 = "¿Qué tipo de indicador es?";
			$campo04 = "tipo_indicador";
			$fecha_cambio04 = date('Y-m-d');
			$hora_cambio04 = date('h:i:s');
			$historial04 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio04','$hora_cambio04','$eje01','$dependencia01','$dato04','$campo04','$tipo_indicador')");
		}

		if($compara05 != $otro_tipo_indicador)
		{
			$dato05 = "¿Cual?:";
			$campo05 = "otro_tipo_indicador";
			$fecha_cambio05 = date('Y-m-d');
			$hora_cambio05 = date('h:i:s');
			$historial05 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio05','$hora_cambio05','$eje01','$dependencia01','$dato05','$campo05','$otro_tipo_indicador')");
		}

			/*____________________________________*/ 
			/*------ INSERCION DE LA ACTUALIZACION ------- */ 	

		$result=mysqli_query($kv, "UPDATE wp_definicion_evidencias SET descripcion_indicador_def='$desc_documento_vinculado',unidad_observacion_indicador_def='$unidad_observacion',descripcion_observacion_ficha_tecnica_def='$desr_ficha_tecnica',tipo_indicador_def='$tipo_indicador',otro_tipo_indicador_def='$otro_tipo_indicador' WHERE id_indicador_asociado_def=$id_indicador");

	}

	if(isset($_POST['meta_2019']))/* ---- EVIDENCIAS ---- */ 
	{

		


		$meta_2019=$_POST['meta_2019'];
		$resultado_19=$_POST['resultado_19'];
		$avance_repecto_meta=$_POST['avance_repecto_meta'];
		


		$result=mysqli_query($kv, "UPDATE wp_definicion_evidencias SET meta_2019='$meta_2019',resultado_19='$resultado_19',avance_meta_planeada_def='$avance_repecto_meta' WHERE id_indicador_asociado_def=$id_indicador");

		echo($result);



//---Actualizar las ubicaciones gps
		/*
		$result=mysqli_query($kv, "UPDATE wp_obras_acciones SET res1_lugares_gps='$reslug1_gps',res2_lugares_gps='$reslug2_gps',res3_lugares_gps='$reslug3_gps',res4_lugares_gps='$reslug4_gps',res5_lugares_gps='$reslug5_gps',res6_lugares_gps='$reslug6_gps',res7_lugares_gps='$reslug7_gps',res8_lugares_gps='$reslug8_gps' WHERE id_indicador_asociado_def=$id_indicador");
		*/

	}

	if(isset($_POST['fecha_corte'])) /* ---- TIPO DE EVIDENCIAS ---- */ 
	{

		$tipo_evidencia_list=$_POST['tipo_evidencia_list'];
		$otro_tipo_evidencia=$_POST['otro_tipo_evidencia'];
		$desc_doc_vinculado_ind=$_POST['desc_doc_vinculado_ind'];

		$fecha_corte=$_POST['fecha_corte'];
		$existe_exp_doc=$_POST['existe_exp_doc'];
		$serie_evidencia=$_POST['serie_evidencia'];

		$seccion=$_POST['seccion'];
		$guia_inventario=$_POST['guia_inventario'];
		$medios_verificacion=$_POST['medios_verificacion'];
		$comentarios=$_POST['comentarios'];

/* COMPARACION ____________________________________*/
		$com01 = "SELECT tipo_evidencia_def FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 01 - A */ 
		$com_01 = $kv->query($com01);
		$com__01 = mysqli_fetch_array($com_01);
		$compara01 = $com__01[0];

		$com02 = "SELECT tipo_evidencia_otro_def FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 02 - B */ 
		$com_02 = $kv->query($com02);
		$com__02 = mysqli_fetch_array($com_02);
		$compara02 = $com__02[0];

		$com03 = "SELECT descripcion_documento_vinculado_indicador_def FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 03 - C */ 
		$com_03 = $kv->query($com03);
		$com__03 = mysqli_fetch_array($com_03);
		$compara03 = $com__03[0];

		$com04 = "SELECT tipo_evidencia_fecha_def FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 04- D */ 
		$com_04 = $kv->query($com04);
		$com__04 = mysqli_fetch_array($com_04);
		$compara04 = $com__04[0];

		$com05 = "SELECT existe_expediente_doc_def FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 05 - E */ 
		$com_05 = $kv->query($com05);
		$com__05 = mysqli_fetch_array($com_05);
		$compara05 = $com__05[0];

		$com06 = "SELECT serie_evidencia_def FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 06 - F */ 
		$com_06 = $kv->query($com06);
		$com__06 = mysqli_fetch_array($com_06);
		$compara06 = $com__06[0];

		$com07 = "SELECT seccion_def FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 06 - F */ 
		$com_07 = $kv->query($com07);
		$com__07 = mysqli_fetch_array($com_07);
		$compara07 = $com__07[0];

		$com08 = "SELECT guia_invetario_expediente_def FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 06 - F */ 
		$com_08 = $kv->query($com08);
		$com__08 = mysqli_fetch_array($com_08);
		$compara08 = $com__08[0];

		$com09 = "SELECT medios_verificacion_evidencia_def FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 06 - F */ 
		$com_09 = $kv->query($com09);
		$com__09 = mysqli_fetch_array($com_09);
		$compara09 = $com__09[0];

		$com10 = "SELECT comentarios_def FROM wp_definicion_evidencias WHERE id_indicador_asociado_def='$id_indicador'"; /* 06 - F */ 
		$com_10 = $kv->query($com10);
		$com__10 = mysqli_fetch_array($com_10);
		$compara10 = $com__10[0];

		if($compara01 != $tipo_evidencia_list)
		{
			$dato01 = "¿Qué tipo de evidencia se presenta? Seleccione los necesarios";
			$campo01 = "tipo_evidencia_list";
			$fecha_cambio01 = date('Y-m-d');
			$hora_cambio01 = date('h:i:s');
			$historial01 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio01','$hora_cambio01','$eje01','$dependencia01','$dato01','$campo01','$tipo_evidencia_list')");
		}

		if($compara02 != $otro_tipo_evidencia)
		{
			$dato02 = "Definelo";
			$campo02 = "otro_tipo_evidencia";
			$fecha_cambio02 = date('Y-m-d');
			$hora_cambio02 = date('h:i:s');
			$historial02 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio02','$hora_cambio02','$eje01','$dependencia01','$dato02','$campo02','$otro_tipo_evidencia')");
		}

		if($compara03 != $desc_doc_vinculado_ind)
		{
			$dato03 = "¿Qué contiene la evidencia? Describir qué es el documento y por qué es una evidencia vinculada al indicador.";
			$campo03 = "desc_doc_vinculado_ind";
			$fecha_cambio03 = date('Y-m-d');
			$hora_cambio03 = date('h:i:s');
			$historial03 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio03','$hora_cambio03','$eje01','$dependencia01','$dato03','$campo03','$desc_doc_vinculado_ind')");
		}

		if($compara04 != $fecha_corte)
		{
			$dato04 = "La fecha de la evidencia es:";
			$campo04 = "fecha_cort";
			$fecha_cambio04 = date('Y-m-d');
			$hora_cambio04 = date('h:i:s');
			$historial04 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio04','$hora_cambio04','$eje01','$dependencia01','$dato04','$campo04','$fecha_corte')");
		}

		if($compara05 != $existe_exp_doc)
		{
			$dato05 = "Existe expediente documental";
			$campo05 = "existe_exp_doc";
			$fecha_cambio05 = date('Y-m-d');
			$hora_cambio05 = date('h:i:s');
			$historial05 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio05','$hora_cambio05','$eje01','$dependencia01','$dato05','$campo05','$existe_exp_doc')");
		}

		if($compara06 != $serie_evidencia)
		{
			$dato06 = "Define cuál es la ubicación electrónica o física de la evidencia o cómo es posible rastrearla";
			$campo06 = "serie_evidencia";
			$fecha_cambio06 = date('Y-m-d');
			$hora_cambio06 = date('h:i:s');
			$historial06 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio06','$hora_cambio06','$eje01','$dependencia01','$dato06','$campo06','$serie_evidencia')");
		}

		if($compara07 != $seccion)
		{
			$dato07 = "Serie de la Evidencia";
			$campo07 = "seccion";
			$fecha_cambio07 = date('Y-m-d');
			$hora_cambio07 = date('h:i:s');
			$historial07 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio07','$hora_cambio07','$eje01','$dependencia01','$dato07','$campo07','$seccion')");
		}
		
		if($compara08 != $guia_inventario)
		{
			$dato08 = "Sección(opcional)";
			$campo08 = "guia_inventario";
			$fecha_cambio08 = date('Y-m-d');
			$hora_cambio08 = date('h:i:s');
			$historial08 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio08','$hora_cambio08','$eje01','$dependencia01','$dato08','$campo08','$guia_inventario')");
		}
		
		if($compara09 != $medios_verificacion)
		{
			$dato09 = "Guía/Inventario en el que se incluye el expediente";
			$campo09 = "medios_verificacion";
			$fecha_cambio09 = date('Y-m-d');
			$hora_cambio09 = date('h:i:s');
			$historial09 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio09','$hora_cambio09','$eje01','$dependencia01','$dato09','$campo09','$medios_verificacion')");
		}

		if($compara10 != $comentarios)
		{
			$dato10 = "Comentarios";
			$campo10 = "comentarios";
			$fecha_cambio10 = date('Y-m-d');
			$hora_cambio10 = date('h:i:s');
			$historial10 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_tactico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombre01','$fecha_cambio10','$hora_cambio10','$eje01','$dependencia01','$dato10','$campo10','$comentarios')");
		}

/*____________________________________*/ 
/*------ INSERCION DE LA ACTUALIZACION ------- */ 	
		
		$result=mysqli_query($kv, "UPDATE wp_definicion_evidencias SET tipo_evidencia_def='$tipo_evidencia_list',tipo_evidencia_otro_def='$otro_tipo_evidencia',descripcion_documento_vinculado_indicador_def='$desc_doc_vinculado_ind',tipo_evidencia_fecha_def='$fecha_corte',existe_expediente_doc_def='$existe_exp_doc',serie_evidencia_def='$serie_evidencia',seccion_def='$seccion',guia_invetario_expediente_def='$guia_inventario',medios_verificacion_evidencia_def='$medios_verificacion',comentarios_def='$comentarios' WHERE id_indicador_asociado_def=$id_indicador");

	
	}

/*_INDICADORES ESTRATEGICOS _____________________________________________________________________________________________________________________________________________________________*/ 






	if(isset($_POST['est_resultado1_edit_']))   /* ---- INDICADOR ---- */ 
	{

		$est_resultado1_edit_=$_POST['est_resultado1_edit_'];
		$est_resultado2_edit_=$_POST['est_resultado2_edit_'];
		$est_resultado3_edit_=$_POST['est_resultado3_edit_'];
		$est_resultado4_edit_=$_POST['est_resultado4_edit_'];
		$est_resultado5_edit_=$_POST['est_resultado5_edit_'];

		$est_res1_lugares_edit_=$_POST['est_res1_lugares_edit_'];
		$est_res2_lugares_edit_=$_POST['est_res2_lugares_edit_'];
		$est_res3_lugares_edit_=$_POST['est_res3_lugares_edit_'];
		$est_res4_lugares_edit_=$_POST['est_res4_lugares_edit_'];
		$est_res5_lugares_edit_=$_POST['est_res5_lugares_edit_'];

		$edit_res2_lugares_GPS_=$_POST['edit_res2_lugares_GPS_'];
		$edit_res3_lugares_GPS_=$_POST['edit_res3_lugares_GPS_'];
		$edit_res4_lugares_GPS_=$_POST['edit_res4_lugares_GPS_'];
		$edit_res5_lugares_GPS_=$_POST['edit_res5_lugares_GPS_'];
		$edit_res1_lugares_GPS_=$_POST['edit_res1_lugares_GPS_'];



/*____________________________________*/ 
/*------ INSERCION DE LA ACTUALIZACION ------- */ 

		if($result=mysqli_query($kv, "UPDATE wp_indicadores_estrategicos_definidos SET 
			resultado1='$est_resultado1_edit_',
			resultado2='$est_resultado2_edit_',
			resultado3='$est_resultado3_edit_',
			resultado4='$est_resultado4_edit_',
			resultado1='$est_resultado5_edit_',
			lug_result_1='$est_res1_lugares_edit_',
			lug_result_2='$est_res2_lugares_edit_',
			lug_result_3='$est_res3_lugares_edit_',
			lug_result_4='$est_res4_lugares_edit_',
			lug_result_5='$est_res5_lugares_edit_',
			est_res1_lugares_gps='$edit_res1_lugares_GPS_',
			est_res2_lugares_gps='$edit_res2_lugares_GPS_',
			est_res3_lugares_gps='$edit_res3_lugares_GPS_',
			est_res4_lugares_gps='$edit_res4_lugares_GPS_',
			est_res5_lugares_gps='$edit_res5_lugares_GPS_'



			WHERE id_ind_estrategico=$id_indicador"))
		{
				echo ("sin errror");
		}

		 else 
	    {
	    	

	    	 echo("Error description: " . mysqli_error($kv));
	    }



	
	}












	if(isset($_POST['est_edi_descrp_ind']))   /* ---- INDICADOR ---- */ 
	{

		$est_edi_descrp_ind=$_POST['est_edi_descrp_ind'];
		$est_edit_bullet_log=$_POST['est_edit_bullet_log'];
		$est_edit_linea_disc=$_POST['est_edit_linea_disc'];
		$est_edit_logro_asoc_ind=$_POST['est_edit_logro_asoc_ind'];
		$est_edit_desc_log=$_POST['est_edit_desc_log'];
		//$est_edit_lin_base=$_POST['est_edit_lin_base'];
		//$est_edit_result=$_POST['est_edit_result'];
		$est_edit_hubo_logro=$_POST['est_edit_hubo_logro'];
		
/* COMPARACION ____________________________________*/
		$com01 = "SELECT descripcion_indicador FROM wp_indicadores_estrategicos_definidos WHERE id_ind_estrategico='$id_indicador'"; /* 01 - A */ 
		$com_01 = $kv->query($com01);
		$com__01 = mysqli_fetch_array($com_01);
		$compara01 = $com__01[0];

		$com02 = "SELECT bullet_logro FROM wp_indicadores_estrategicos_definidos WHERE id_ind_estrategico='$id_indicador'"; /* 02 - B */ 
		$com_02 = $kv->query($com02);
		$com__02 = mysqli_fetch_array($com_02);
		$compara02 = $com__02[0];

		$com03 = "SELECT linea_discursiva FROM wp_indicadores_estrategicos_definidos WHERE id_ind_estrategico='$id_indicador'"; /* 03 - C */ 
		$com_03 = $kv->query($com03);
		$com__03 = mysqli_fetch_array($com_03);
		$compara03 = $com__03[0];

		$com04 = "SELECT logro_asoc_indicador FROM wp_indicadores_estrategicos_definidos WHERE id_ind_estrategico='$id_indicador'"; /* 04- D */ 
		$com_04 = $kv->query($com04);
		$com__04 = mysqli_fetch_array($com_04);
		$compara04 = $com__04[0];

		$com05 = "SELECT descripcion_logro FROM wp_indicadores_estrategicos_definidos WHERE id_ind_estrategico='$id_indicador'"; /* 05 - E */ 
		$com_05 = $kv->query($com05);
		$com__05 = mysqli_fetch_array($com_05);
		$compara05 = $com__05[0];

		$com06 = "SELECT hubo_logro FROM wp_indicadores_estrategicos_definidos WHERE id_ind_estrategico='$id_indicador'"; /* 06 - F */ 
		$com_06 = $kv->query($com06);
		$com__06 = mysqli_fetch_array($com_06);
		$compara06 = $com__06[0];

		if($compara01 != $est_edi_descrp_ind)
		{
			$dato01 = "Descripcion del indicador(";
			$campo01 = "est_edi_descrp_ind";
			$fecha_cambio01 = date('Y-m-d');
			$hora_cambio01 = date('h:i:s');
			$historial01 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_estrategico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,eje_ind,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombreIE','$fecha_cambio01','$hora_cambio01','$ejeIE','$dependenciaIE','$dato01','$campo01','$est_edi_descrp_ind')");
		}

		if($compara02 != $est_edit_bullet_log)
		{
			$dato02 = "Bullet del logro";
			$campo02 = "est_edit_bullet_log";
			$fecha_cambio02 = date('Y-m-d');
			$hora_cambio02 = date('h:i:s');
			$historial02 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_estrategico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,nombre_ind,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombreIE','$fecha_cambio02','$hora_cambio02','$ejeIE','$dependenciaIE','$dato02','$campo02','$est_edit_bullet_log')");
		}

		if($compara03 != $est_edit_linea_disc)
		{
			$dato03 = "Linea discursiva";
			$campo03 = "est_edit_linea_disc";
			$fecha_cambio03 = date('Y-m-d');
			$hora_cambio03 = date('h:i:s');
			$historial03 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_estrategico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,nombre_ind,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombreIE','$fecha_cambio03','$hora_cambio03','$ejeIE','$dependenciaIE','$dato03','$campo03','$est_edit_linea_disc')");
		}

		if($compara04 != $est_edit_logro_asoc_ind)
		{
			$dato04 = "La fecha de la evidencia es:";
			$campo04 = "fecha_cort";
			$fecha_cambio04 = date('Y-m-d');
			$hora_cambio04 = date('h:i:s');
			$historial04 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_estrategico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,nombre_ind,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombreIE','$fecha_cambio04','$hora_cambio04','$ejeIE','$dependenciaIE','$dato04','$campo04','$est_edit_logro_asoc_ind')");
		}

		if($compara05 != $est_edit_desc_log)
		{
			$dato05 = "Bullet del logro";
			$campo05 = "est_edit_desc_log";
			$fecha_cambio05 = date('Y-m-d');
			$hora_cambio05 = date('h:i:s');
			$historial05 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_estrategico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,nombre_ind,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombreIE','$fecha_cambio05','$hora_cambio05','$ejeIE','$dependenciaIE','$dato05','$campo05','$est_edit_desc_log')");
		}

		if($compara06 != $est_edit_hubo_logro)
		{
			$dato06 = "Hubo logros en este indicador";
			$campo06 = "est_edit_hubo_logro";
			$fecha_cambio06 = date('Y-m-d');
			$hora_cambio06 = date('h:i:s');
			$historial06 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_estrategico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,nombre_ind,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombreIE','$fecha_cambio06','$hora_cambio06','$ejeIE','$dependenciaIE','$dato06','$campo06','$est_edit_hubo_logro')");
		}

/*____________________________________*/ 
/*------ INSERCION DE LA ACTUALIZACION ------- */ 

		$result=mysqli_query($kv, "UPDATE wp_indicadores_estrategicos_definidos SET descripcion_indicador='$est_edi_descrp_ind',bullet_logro='$est_edit_bullet_log',linea_discursiva='$est_edit_linea_disc',logro_asoc_indicador='$est_edit_logro_asoc_ind',descripcion_logro='$est_edit_desc_log',hubo_logro='$est_edit_hubo_logro' WHERE id_ind_estrategico=$id_indicador");
	
	}

	if(isset($_POST['est_edit_resultado_2018'])) /* ---- METAS ---- */ 
	{

		$est_edit_resultado_2018=$_POST['est_edit_resultado_2018'];

		$com01 = "SELECT result_abril_2018 FROM wp_indicadores_estrategicos_definidos WHERE id_ind_estrategico='$id_indicador'"; /* 06 - F */ 
		$com_01 = $kv->query($com01);
		$com__01 = mysqli_fetch_array($com_01);
		$compara01 = $com__01[0];

		if($compara01 != $est_edit_resultado_2018)
		{
			$dato01 = "Resultado 2018";
			$campo01 = "tipo_evidencia_list";
			$fecha_cambio01 = date('Y-m-d');
			$hora_cambio01 = date('h:i:s');
			$historial01 = mysqli_query($kv, "INSERT INTO wp_historial_indicador_estrategico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,eje_ind,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombreIE','$fecha_cambio01','$hora_cambio01','$ejeIE','$dependenciaIE','$dato01','$campo01','$est_edit_resultado_2018')");
		}

/*____________________________________*/ 
/*------ INSERCION DE LA ACTUALIZACION ------- */ 

		$result=mysqli_query($kv, "UPDATE wp_indicadores_estrategicos_definidos SET result_abril_2018='$est_edit_resultado_2018' WHERE id_ind_estrategico=$id_indicador");
	}

/*_ACCIONES CONCURRENTES_____________________________________________________________________________________________________________________________________________________________*/ 
/*
	{id_indicador:valor_entra,eje_concu_edit_: eje_concu_edit_, objetivo_est_concu_:objetivo_est_concu_,objetivo_gen_concu_edit_:objetivo_gen_concu_edit_}

	*/

	if(isset($_POST['eje_concu_edit_'])) /* ---- EJE ---- */ 
	{

		$eje_concu_edit_=$_POST['eje_concu_edit_'];
		$objetivo_est_concu_=$_POST['objetivo_est_concu_'];
		$objetivo_gen_concu_edit_=$_POST['objetivo_gen_concu_edit_'];
/* COMPARACION ____________________________________*/
		$com01 = "SELECT eje_principal FROM wp_acciones_concurrentes WHERE id_accion_concurrente='$id_indicador'"; /* 01 - A */ 
		$com_01 = $kv->query($com01);
		$com__01 = mysqli_fetch_array($com_01);
		$compara01 = $com__01[0];

		$com02 = "SELECT objetivo_est FROM wp_acciones_concurrentes WHERE id_accion_concurrente='$id_indicador'"; /* 02 - B */ 
		$com_02 = $kv->query($com02);
		$com__02 = mysqli_fetch_array($com_02);
		$compara02 = $com__02[0];

		$com03 = "SELECT objetivo_gen FROM wp_acciones_concurrentes WHERE id_accion_concurrente='$id_indicador'"; /* 03 - C */ 
		$com_03 = $kv->query($com03);
		$com__03 = mysqli_fetch_array($com_03);
		$compara03 = $com__03[0];

		if($compara01 != $eje_concu_edit_)
		{
			$dato01 = "Selecciona el eje correspondiente";
			$campo01 = "eje_concu_edit_";
			$fecha_cambio01 = date('Y-m-d');
			$hora_cambio01 = date('h:i:s');
			$historial01 = mysqli_query($kv, "INSERT INTO wp_historial_acciones_concurrentes(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$ejeAC','$fecha_cambio01','$hora_cambio01','$accionAC','$dependenciaAC','$dato01','$campo01','$eje_concu_edit_')");
		}

		if($compara02 != $objetivo_est_concu_)
		{
			$dato02 = "Selecciona el objetivo estratégico correspondiente";
			$campo02 = "objetivo_est_concu_";
			$fecha_cambio02 = date('Y-m-d');
			$hora_cambio02 = date('h:i:s');
			$historial02 = mysqli_query($kv, "INSERT INTO wp_historial_acciones_concurrentes(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$ejeAC','$fecha_cambio02','$hora_cambio02','$accionAC','$dependenciaAC','$dato02','$campo02','$objetivo_est_concu_')");
		}

		if($compara03 != $objetivo_gen_concu_edit_)
		{
			$dato03 = "Selecciona el objetivo general correspondiente";
			$campo03 = "objetivo_gen_concu_edit_";
			$fecha_cambio03 = date('Y-m-d');
			$hora_cambio03 = date('h:i:s');
			$historial03 = mysqli_query($kv, "INSERT INTO wp_historial_acciones_concurrentes(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$ejeAC','$fecha_cambio03','$hora_cambio03','$accionAC','$dependenciaAC','$dato03','$campo03','$objetivo_gen_concu_edit_')");
		}

/*____________________________________*/ 
/*------ INSERCION DE LA ACTUALIZACION ------- */ 		

		$result=mysqli_query($kv, "UPDATE wp_acciones_concurrentes SET eje_principal='$eje_concu_edit_',objetivo_est='$objetivo_est_concu_',objetivo_gen='$objetivo_gen_concu_edit_' WHERE id_accion_concurrente=$id_indicador");
	}

	if(isset($_POST['nom_accion_'])) /* ---- ACCIONES ---- */ 
	{

		$nom_accion_=$_POST['nom_accion_'];
		$dependecias_selecionadas_concu_=$_POST['dependecias_selecionadas_concu_'];
		$descipcion_acciones_=$_POST['descipcion_acciones_'];
		
/* COMPARACION ____________________________________*/
		$com01 = "SELECT nombre_accion FROM wp_acciones_concurrentes WHERE id_accion_concurrente='$id_indicador'"; /* 01 - A */ 
		$com_01 = $kv->query($com01);
		$com__01 = mysqli_fetch_array($com_01);
		$compara01 = $com__01[0];

		$com02 = "SELECT dependencias_involucradas FROM wp_acciones_concurrentes WHERE id_accion_concurrente='$id_indicador'"; /* 02 - B */ 
		$com_02 = $kv->query($com02);
		$com__02 = mysqli_fetch_array($com_02);
		$compara02 = $com__02[0];

		$com03 = "SELECT descripcion_acciones FROM wp_acciones_concurrentes WHERE id_accion_concurrente='$id_indicador'"; /* 03 - C */ 
		$com_03 = $kv->query($com03);
		$com__03 = mysqli_fetch_array($com_03);
		$compara03 = $com__03[0];

		if($compara01 != $nom_accion_)
		{
			$dato01 = "Nombre del indicador";
			$campo01 = "nom_accion_";
			$fecha_cambio01 = date('Y-m-d');
			$hora_cambio01 = date('h:i:s');
			$historial01 = mysqli_query($kv, "INSERT INTO wp_historial_acciones_concurrentes(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$ejeAC','$fecha_cambio01','$hora_cambio01','$accionAC','$dependenciaAC','$dato01','$campo01','$nom_accion_')");
		}

		if($compara02 != $dependecias_selecionadas_concu_)
		{
			$dato02 = "Selecciona Las dependencias involucradas";
			$campo02 = "dependecias_selecionadas_concu_";
			$fecha_cambio02 = date('Y-m-d');
			$hora_cambio02 = date('h:i:s');
			$historial02 = mysqli_query($kv, "INSERT INTO wp_historial_acciones_concurrentes(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$ejeAC','$fecha_cambio02','$hora_cambio02','$accionAC','$dependenciaAC','$dato02','$campo02','$dependecias_selecionadas_concu_')");
		}

		if($compara03 != $descipcion_acciones_)
		{
			$dato03 = "Descripcion del indicador";
			$campo03 = "descipcion_acciones_";
			$fecha_cambio03 = date('Y-m-d');
			$hora_cambio03 = date('h:i:s');
			$historial03 = mysqli_query($kv, "INSERT INTO wp_historial_acciones_concurrentes(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$ejeAC','$fecha_cambio03','$hora_cambio03','$accionAC','$dependenciaAC','$dato03','$campo03','$descipcion_acciones_')");
		}

/*____________________________________*/ 
/*------ INSERCION DE LA ACTUALIZACION ------- */ 

		$result=mysqli_query($kv, "UPDATE wp_acciones_concurrentes SET nombre_accion='$nom_accion_',dependencias_involucradas='$dependecias_selecionadas_concu_',descripcion_acciones='$descipcion_acciones_' WHERE id_accion_concurrente=$id_indicador");
	}


/*_ ACCIONES SIN INDICADOR_____________________________________________________________________________________________________________________________________________________________*/ 
	/*id_indicador:valor_entra,nom_accion_:nom_accion_,dependecias_selecionadas_concu_:dependecias_selecionadas_concu_,descipcion_acciones_:descipcion_acciones_*/
/*
	id_indicador:valor_entra,eje_sn_edit_: eje_sn_edit_, objetivo_est_sn_:objetivo_est_sn_,objetivo_gen_sn_edit_:objetivo_gen_sn_edit_
*/

    if(isset($_POST['eje_sn_edit_'])) /* ---- EJE ---- */ 
	{

		$eje_sn_edit_=$_POST['eje_sn_edit_'];
		$objetivo_est_sn_=$_POST['objetivo_est_sn_'];
		$objetivo_gen_sn_edit_=$_POST['objetivo_gen_sn_edit_'];
	
	/* COMPARACION ____________________________________*/
		$com01 = "SELECT eje_principal FROM wp_acciones_sin_indicador WHERE id_accion_sin_ind='$id_indicador'"; /* 01 - A */ 
		$com_01 = $kv->query($com01);
		$com__01 = mysqli_fetch_array($com_01);
		$compara01 = $com__01[0];

		$com02 = "SELECT objetivo_est FROM wp_acciones_sin_indicador WHERE id_accion_sin_ind='$id_indicador'"; /* 02 - B */ 
		$com_02 = $kv->query($com02);
		$com__02 = mysqli_fetch_array($com_02);
		$compara02 = $com__02[0];

		$com03 = "SELECT objetivo_gen FROM wp_acciones_sin_indicador WHERE id_accion_sin_ind='$id_indicador'"; /* 03 - C */ 
		$com_03 = $kv->query($com03);
		$com__03 = mysqli_fetch_array($com_03);
		$compara03 = $com__03[0];

		if($compara01 != $eje_sn_edit_)
		{
			$dato01 = "eje correspondiente";
			$campo01 = "eje_principal";
			$fecha_cambio01 = date('Y-m-d');
			$hora_cambio01 = date('h:i:s');
			$historial01 = mysqli_query($kv, "INSERT INTO wp_historial_accion_sin_indicador(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$ejeAI','$fecha_cambio01','$hora_cambio01','$accionAI','$dependenciaAI','$dato01','$campo01','$eje_sn_edit_')");
		}

		if($compara02 != $objetivo_est_sn_)
		{
			$dato02 = "objetivo estratégico correspondiente";
			$campo02 = "objetivo_est";
			$fecha_cambio02 = date('Y-m-d');
			$hora_cambio02 = date('h:i:s');
			$historial02 = mysqli_query($kv, "INSERT INTO wp_historial_accion_sin_indicador(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$ejeAI','$fecha_cambio02','$hora_cambio02','$accionAI','$dependenciaAI','$dato02','$campo02','$objetivo_est_sn_')");
		}

		if($compara03 != $objetivo_gen_sn_edit_)
		{
			$dato03 = "Pobjetivo general correspondiente";
			$campo03 = "objetivo_gen";
			$fecha_cambio03 = date('Y-m-d');
			$hora_cambio03 = date('h:i:s');
			$historial03 = mysqli_query($kv, "INSERT INTO wp_historial_accion_sin_indicador(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$ejeAI','$fecha_cambio03','$hora_cambio03','$accionAI','$dependenciaAI','$dato03','$campo03','$objetivo_gen_sn_edit_')");
		}

/*____________________________________*/ 
/*------ INSERCION DE LA ACTUALIZACION ------- */ 		
		$result=mysqli_query($kv, "UPDATE wp_acciones_sin_indicador SET eje_principal='$eje_sn_edit_',objetivo_est='$objetivo_est_sn_',objetivo_gen='$objetivo_gen_sn_edit_' WHERE id_accion_sin_ind=$id_indicador");
	}

	if(isset($_POST['accion_1'])) /* ---- ACCIONES ---- */ 
	{
		$accion_1=$_POST['accion_1'];

		/* COMPARACION ____________________________________*/
		$com01 = "SELECT accion_1 FROM wp_acciones_sin_indicador WHERE id_accion_sin_ind = '$id_indicador'"; /* 01 - A */ 
		$com_01 = $kv->query($com01);
		$com__01 = mysqli_fetch_array($com_01);
		$compara01 = $com__01[0];

		if($compara01 != $accion_1)
		{
			$dato01 = "Descripcion del indicador";
			$campo01 = "accion_1";
			$fecha_cambio01 = date('Y-m-d');
			$hora_cambio01 = date('h:i:s');
			$historial01 = mysqli_query($kv, "INSERT INTO wp_historial_accion_sin_indicador(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,accion,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$ejeAI','$fecha_cambio01','$hora_cambio01','$accionAI','$dependenciaAI','$dato01','$campo01','$accion_1')");
		}
/*____________________________________*/ 
/*------ INSERCION DE LA ACTUALIZACION ------- */ 			
		$result=mysqli_query($kv, "UPDATE wp_acciones_sin_indicador SET accion_1='$accion_1' WHERE id_accion_sin_ind=$id_indicador");
	}

/*_ AVANCE PROGRAMATICO_____________________________________________________________________________________________________________________________________________________________*/ 

	if(isset($_POST['inversion_autorizada_fin_']))  /* ---- AVANCE FINANCIERO ---- */ 
	{

		$inversion_autorizada_fin_=$_POST['inversion_autorizada_fin_'];
		$inversion_ejercida_fin_=$_POST['inversion_ejercida_fin_'];
		$inversion_autorizada_fin_2018_=$_POST['inversion_autorizada_fin_2018_'];
		$inversion_ejercida_fin_2018_=$_POST['inversion_ejercida_fin_2018_'];
	/* COMPARACION ____________________________________*/ 		
		
		$com01 = "SELECT inversion_autorizada_fin_2017 FROM wp_avance_programatico WHERE id_avance_programatico='$id_indicador'"; /* 01 - A */ 
		$com_01 = $kv->query($com01);
		$com__01 = mysqli_fetch_array($com_01);
		$compara01 = $com__01[0];

		$com02 = "SELECT inversion_ejercida_fin_2017 FROM wp_avance_programatico WHERE id_avance_programatico='$id_indicador'"; /* 02 - B */ 
		$com_02 = $kv->query($com02);
		$com__02 = mysqli_fetch_array($com_02);
		$compara02 = $com__02[0];

		$com03 = "SELECT inversion_autorizada_fin_2018 FROM wp_avance_programatico WHERE id_avance_programatico='$id_indicador'"; /* 03 - C */ 
		$com_03 = $kv->query($com03);
		$com__03 = mysqli_fetch_array($com_03);
		$compara03 = $com__03[0];

		$com04 = "SELECT inversion_ejercida_fin_2018 FROM wp_avance_programatico WHERE id_avance_programatico='$id_indicador'"; /* 04 - D */ 
		$com_04 = $kv->query($com04);
		$com__04 = mysqli_fetch_array($com_04);
		$compara04 = $com__04[0];

		if($compara01 != $inversion_autorizada_fin_)
		{
			$dato01 = "PRESUPUESTO AUTORIZADO (ANUAL 2017)";
			$campo01 = "inversion_autorizada_fin_2017";
			$fecha_cambio01 = date('Y-m-d');
			$hora_cambio01 = date('h:i:s');
			$historial01 = mysqli_query($kv, "INSERT INTO wp_historial_avance_programatico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,usuario_cambio,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombreAP','$fecha_cambio01','$hora_cambio01','$usuarioAP','$dependenciaAP','$dato01','$campo01','$inversion_autorizada_fin_')");
		}

		if($compara02 != $inversion_ejercida_fin_)
		{
			$dato02 = "PRESUPUESTO EJERCIDO AL 31/DIC/2017";
			$campo02 = "inversion_ejercida_fin_2017";
			$fecha_cambio02 = date('Y-m-d');
			$hora_cambio02 = date('h:i:s');
			$historial02 = mysqli_query($kv, "INSERT INTO wp_historial_avance_programatico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,usuario_cambio,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombreAP','$fecha_cambio02','$hora_cambio02','$usuarioAP','$dependenciaAP','$dato02','$campo02','$inversion_ejercida_fin_')");
		}

		if($compara03 != $inversion_autorizada_fin_2018_)
		{
			$dato03 = "PRESUPUESTO AUTORIZADO ANUAL 2018";
			$campo03 = "inversion_autorizada_fin_2018";
			$fecha_cambio03 = date('Y-m-d');
			$hora_cambio03 = date('h:i:s');
			$historial03 = mysqli_query($kv, "INSERT INTO wp_historial_avance_programatico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,usuario_cambio,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombreAP','$fecha_cambio03','$hora_cambio03','$usuarioAP','$dependenciaAP','$dato03','$campo03','$inversion_autorizada_fin_2018_')");
		}

		if($compara04 != $inversion_ejercida_fin_2018_)
		{
			$dato04 = "PRESUPUESTO EJERCIDO AL 30 DE JUN DE 2018";
			$campo04 = "inversion_ejercida_fin_2018";
			$fecha_cambio04 = date('Y-m-d');
			$hora_cambio04 = date('h:i:s');
			$historial04 = mysqli_query($kv, "INSERT INTO wp_historial_avance_programatico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,usuario_cambio,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombreAP','$fecha_cambio04','$hora_cambio04','$usuarioAP','$dependenciaAP','$dato04','$campo04','$inversion_ejercida_fin_2018_')");
		}
/*____________________________________*/ 
/*------ INSERCION DE LA ACTUALIZACION ------- */ 
		$result=mysqli_query($kv, "UPDATE wp_avance_programatico SET inversion_autorizada_fin_2017='$inversion_autorizada_fin_',inversion_ejercida_fin_2017='$inversion_ejercida_fin_',inversion_autorizada_fin_2018='$inversion_ejercida_fin_2018_',	inversion_ejercida_fin_2018='$inversion_autorizada_fin_2018_' WHERE id_avance_programatico=$id_indicador");

	}

	if(isset($_POST['result_estim_30_jun_prop_']))  /* ---- NIVEL PROPOSITO ---- */ 
	{

		$result_estim_30_jun_prop_=$_POST['result_estim_30_jun_prop_'];
		$avance_porc_meta_2018_prop_=$_POST['avance_porc_meta_2018_prop_'];
	/* COMPARACION ____________________________________*/
		$com01 = "SELECT result_estim_30_jun_prop FROM wp_avance_programatico WHERE id_avance_programatico='$id_indicador'"; /* 01 - A */ 
		$com_01 = $kv->query($com01);
		$com__01 = mysqli_fetch_array($com_01);
		$compara01 = $com__01[0];

		$com02 = "SELECT avance_porc_meta_2018 FROM wp_avance_programatico WHERE id_avance_programatico='$id_indicador'"; /* 02 - B */ 
		$com_02 = $kv->query($com02);
		$com__02 = mysqli_fetch_array($com_02);
		$compara02 = $com__02[0];

		if($compara01 != $result_estim_30_jun_prop_)
		{
			$dato01 = "Resultado Estimado 30 de Junio(Primer Semestre)";
			$campo01 = "result_estim_30_jun_prop";
			$fecha_cambio01 = date('Y-m-d');
			$hora_cambio01 = date('h:i:s');
			$historial01 = mysqli_query($kv, "INSERT INTO wp_historial_avance_programatico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,usuario_cambio,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombreAP','$fecha_cambio01','$hora_cambio01','$usuarioAP','$dependenciaAP','$dato01','$campo01','$result_estim_30_jun_prop_')");
		}
		if($compara02 != $avance_porc_meta_2018_prop_)
		{
			$dato02 = "PRESUPUESTO EJERCIDO AL 31/DIC/2017";
			$campo02 = "inversion_ejercida_fin_2017";
			$fecha_cambio02 = date('Y-m-d');
			$hora_cambio02 = date('h:i:s');
			$historial02 = mysqli_query($kv, "INSERT INTO wp_historial_avance_programatico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,usuario_cambio,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombreAP','$fecha_cambio02','$hora_cambio02','$usuarioAP','$dependenciaAP','$dato02','$campo02','$avance_porc_meta_2018_prop_')");
		}
	/* ________________________________________*/
		/*------ INSERCION DE LA ACTUALIZACION ------- */ 
		$result=mysqli_query($kv, "UPDATE wp_avance_programatico SET result_estim_30_jun_prop='$result_estim_30_jun_prop_',avance_porc_meta_2018='$avance_porc_meta_2018_prop_' WHERE id_avance_programatico=$id_indicador");
	}
 
	if(isset($_POST['result_estim_30_jun_fin_'])) /* ---- NIVEL FIN ---- */ 
	{

		$result_estim_30_jun_fin_=$_POST['result_estim_30_jun_fin_'];
		$avance_porc_meta_2018_=$_POST['avance_porc_meta_2018_'];
	/* COMPARACION ____________________________________*/
		$com01 = "SELECT result_estim_30_jun_fin FROM wp_avance_programatico WHERE id_avance_programatico='$id_indicador'"; /* 01 - A */ 
		$com_01 = $kv->query($com01);
		$com__01 = mysqli_fetch_array($com_01);
		$compara01 = $com__01[0];

		$com02 = "SELECT avance_fin_meta_2018 FROM wp_avance_programatico WHERE id_avance_programatico='$id_indicador'"; /* 02 - B */ 
		$com_02 = $kv->query($com02);
		$com__02 = mysqli_fetch_array($com_02);
		$compara02 = $com__02[0];

		if($compara01 != $result_estim_30_jun_fin_)
		{
			$dato01 = "Resultado Estimado 30 de Junio(Primer Semestre)";
			$campo01 = "result_estim_30_jun_fin";
			$fecha_cambio01 = date('Y-m-d');
			$hora_cambio01 = date('h:i:s');
			$historial01 = mysqli_query($kv, "INSERT INTO wp_historial_avance_programatico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,usuario_cambio,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombreAP','$fecha_cambio01','$hora_cambio01','$usuarioAP','$dependenciaAP','$dato01','$campo01','$result_estim_30_jun_fin_')");
		}
		if($compara02 != $avance_porc_meta_2018_)
		{
			$dato02 = "Avance Porcentual de la meta 2018";
			$campo02 = "avance_fin_meta_2018";
			$fecha_cambio02 = date('Y-m-d');
			$hora_cambio02 = date('h:i:s');
			$historial02 = mysqli_query($kv, "INSERT INTO wp_historial_avance_programatico(ID_indicador,nombre_indicador,fecha_cambio,hora_cambio,usuario_cambio,dependencia_cambio,datoN,campoN,contenido_campoN) 
			VALUES('$id_indicador','$nombreAP','$fecha_cambio02','$hora_cambio02','$usuarioAP','$dependenciaAP','$dato02','$campo02','$avance_porc_meta_2018_')");
		}
/* ________________________________________*/
/*------ INSERCION DE LA ACTUALIZACION ------- */
		$result=mysqli_query($kv, "UPDATE wp_avance_programatico SET result_estim_30_jun_fin='$result_estim_30_jun_fin_',avance_fin_meta_2018='$avance_porc_meta_2018_' WHERE id_avance_programatico=$id_indicador");
	}


	if(isset($_POST['update_evaluado']))
	{

		$evaluacion=$_POST['update_evaluado'];
		//$result=$id_indicador;

		if($evaluacion=0 || $evaluacion=2)
		{
			$result=mysqli_query($kv, "UPDATE wp_definicion_evidencias SET estatus=$evaluacion WHERE id_indicador_asociado_def=$id_indicador");
		}
		else if($evaluacion=1)
		{
			$result=mysqli_query($kv, "UPDATE wp_definicion_evidencias SET estatus=$evaluacion,coment_indicador='' WHERE id_indicador_asociado_def=$id_indicador");

		}

		
	}

	 

	  if(isset($_POST['status']))
	{

		$status=$_POST['status'];
		//1= validado correctamente
		if($status==1)
		{
			$result=mysqli_query($kv, "UPDATE wp_definicion_evidencias SET estatus=$status,num_revision=1, coment_indicador='' WHERE id_indicador_asociado_def=$id_indicador");
		}
		else if($status==0)
		{
			$result=mysqli_query($kv, "UPDATE wp_definicion_evidencias SET estatus=$status,num_revision= -1 WHERE id_indicador_asociado_def=$id_indicador");
		}
		else
		{
			$result=mysqli_query($kv, "UPDATE wp_definicion_evidencias SET estatus=$status,num_revision= 2 WHERE id_indicador_asociado_def=$id_indicador");
		}

		
	}


	if(isset($_POST['status_pid']))
	{

		$status=$_POST['status_pid'];
		//1= validado correctamente
		if($status==1)
		{
			$result=mysqli_query($kv, "UPDATE wp_indicadores_pids SET estatus=$status,num_revision=1, coment_indicador='' WHERE id_tactico_pid=$id_indicador");
		}
		else if($status==0)
		{
			$result=mysqli_query($kv, "UPDATE wp_indicadores_pids SET estatus=$status,num_revision= -1 WHERE id_tactico_pid=$id_indicador");
		}
		else
		{
			$result=mysqli_query($kv, "UPDATE wp_indicadores_pids SET estatus=$status,num_revision= 2 WHERE id_tactico_pid=$id_indicador");
		}

		
	}

	if(isset($_POST['coment_eva']))
	{

		$coment_eva=$_POST['coment_eva'];
		$nom_evaluador=$_POST['nom_evaluador'];
		$nom_depen=$_POST['nom_depen'];
		//$result=$id_indicador;

		$result_2=mysqli_query($kv, "UPDATE wp_definicion_evidencias SET estatus=2,coment_indicador='$coment_eva',num_revision= 2 WHERE id_indicador_asociado_def=$id_indicador");

		  $result=mysqli_query($kv, "INSERT INTO wp_historial_comentarios_evaluadores (nombre_evaluador,nombre_dependencia,id_indicador,comentario) values ('$nom_evaluador','$nom_depen',$id_indicador,'$coment_eva')");
	}

/*
		if(isset($_POST['coment_eva']))
	{

		$coment_eva=$_POST['coment_eva'];
		$nom_evaluador=$_POST['nom_evaluador'];
		$nom_depen=$_POST['nom_depen'];
		//$result=$id_indicador;

		$result_2=mysqli_query($kv, "UPDATE wp_definicion_evidencias SET estatus=2,coment_indicador='$coment_eva' WHERE id_indicador_asociado_def=$id_indicador");

		  $result=mysqli_query($kv, "INSERT INTO wp_historial_comentarios_evaluadores (nombre_evaluador,nombre_dependencia,id_indicador,comentario) values ('$nom_evaluador','$nom_depen',$id_indicador,'$coment_eva')");
	}

	*/


	if(isset($_POST['mensaje_dependencia']))
	{

		$mensaje_dependencia=$_POST['mensaje_dependencia'];
		
		//$result=$id_indicador;

		$result=mysqli_query($kv, "UPDATE wp_definicion_evidencias SET num_revision=1,coment_evidencia='$mensaje_dependencia' WHERE id_indicador_asociado_def=$id_indicador");

		  //$result=mysqli_query($kv, "INSERT INTO wp_historial_comentarios_evaluadores (nombre_evaluador,nombre_dependencia,id_indicador,comentario) values ('$nom_evaluador','$nom_depen',$id_indicador,'$coment_eva')");
	}

		if(isset($_POST['comentario_archivo']))
	{

		$comentario_archivo=$_POST['comentario_archivo'];
		
		//$result=$id_indicador;

		$result=mysqli_query($kv, "UPDATE wp_archivos_dependencias SET comentario_archivo='$comentario_archivo' WHERE id_archivo=$id_indicador");

	}


	if(isset($_POST['update_estado_archivo']))
	{

		$update_estado_archivo=$_POST['update_estado_archivo'];
		
		//$result=$id_indicador;

		$result=mysqli_query($kv, "UPDATE wp_archivos_dependencias SET estatus='$update_estado_archivo' WHERE id_archivo=$id_indicador");

	}


	//data: {id_indicador:id_archivo,update_estado_archivo: update_evaluado}

	//-------------PIDS---------



	if(isset($_POST['descrp_tac_pid_EDIT_']))
	{
		$usuario=$_POST['nom_usuario_pid'];

		$descrp_tac_pid_EDIT_=$_POST['descrp_tac_pid_EDIT_'];
		$logros_en_indicador_pid_EDIT=$_POST['logros_en_indicador_pid_EDIT'];
		$id_bullet_Del_logro_pid_EDIT=$_POST['id_bullet_Del_logro_pid_EDIT'];
		$premios_recono_pid_EDIT_=$_POST['premios_recono_pid_EDIT_'];
		$reconocimiento_pid_EDIT_=$_POST['reconocimiento_pid_EDIT_'];
		
		//$result=$id_indicador;

		if ($result=mysqli_query($kv, "UPDATE wp_indicadores_pids SET descripcion_indicador_pid='$descrp_tac_pid_EDIT_',logros_indicador_pid='$logros_en_indicador_pid_EDIT',bullet_logro_pid='$id_bullet_Del_logro_pid_EDIT',	hubo_premio_pid= '$premios_recono_pid_EDIT_',premio_reconocimiento_pid='$reconocimiento_pid_EDIT_' WHERE id_tactico_pid=$id_indicador AND nombre_usuario_pid='$usuario' "))
		 {
		 	echo ("sin errror");
 
  		 }
	    else 
	    {
	    	

	    	 echo("Error description: " . mysqli_error($kv));
	    }


	}

	if(isset($_POST['resultado_1_pid_EDIT_']))
	{
			$usuario=$_POST['nom_usuario_pid'];

		$resultado_1_pid_EDIT_=$_POST['resultado_1_pid_EDIT_'];
		$resultado_2_pid_EDIT_=$_POST['resultado_2_pid_EDIT_'];
		$resultado_3_pid_EDIT_=$_POST['resultado_3_pid_EDIT_'];
		$resultado_4_pid_EDIT_=$_POST['resultado_4_pid_EDIT_'];
		$resultado_5_pid_EDIT_=$_POST['resultado_5_pid_EDIT_'];

		$lo_que_sigue_1_pid_EDIT_=$_POST['lo_que_sigue_1_pid_EDIT_'];
		$lo_que_sigue_2_pid_EDIT_=$_POST['lo_que_sigue_2_pid_EDIT_'];
		$lo_que_sigue_3_pid_EDIT_=$_POST['lo_que_sigue_3_pid_EDIT_'];

		$res1_lugares_pid_EDIT_=$_POST['res1_lugares_pid_EDIT_'];
		$res2_lugares_pid_EDIT_=$_POST['res2_lugares_pid_EDIT_'];
		$res3_lugares_pid_EDIT_=$_POST['res3_lugares_pid_EDIT_'];
		$res4_lugares_pid_EDIT_=$_POST['res4_lugares_pid_EDIT_'];
		$res5_lugares_pid_EDIT_=$_POST['res5_lugares_pid_EDIT_'];


		//$result=$id_indicador;

		if ($result=mysqli_query($kv, "UPDATE wp_indicadores_pids SET resultado1_pid='$resultado_1_pid_EDIT_',resultado2_pid='$resultado_2_pid_EDIT_',resultado3_pid='$resultado_3_pid_EDIT_',resultado4_pid= '$resultado_4_pid_EDIT_',resultado5_pid='$resultado_5_pid_EDIT_',lo_que_sigue_1_pid='$lo_que_sigue_1_pid_EDIT_',lo_que_sigue_2_pid='$lo_que_sigue_2_pid_EDIT_',lo_que_sigue_3_pid='$lo_que_sigue_3_pid_EDIT_',lug_result1_pid='$res1_lugares_pid_EDIT_',lug_result2_pid='$res2_lugares_pid_EDIT_',lug_result3_pid='$res3_lugares_pid_EDIT_',lug_result4_pid='$res4_lugares_pid_EDIT_',lug_result5_pid='$res5_lugares_pid_EDIT_' WHERE id_tactico_pid=$id_indicador AND nombre_usuario_pid='$usuario' "))
		 {
		 	echo ("sin errror");


 
  		 }
	    else 
	    {
	    	

	    	 echo("Error description: " . mysqli_error($kv));
	    }


	}


	if(isset($_POST['unidad_observacion_ind_pid_EDIT_']))
	{
		$usuario=$_POST['nom_usuario_pid'];

		$unidad_observacion_ind_pid_EDIT_=$_POST['unidad_observacion_ind_pid_EDIT_'];
		$descripcion_observacion_ficha_tecnica_pid_EDIT_=$_POST['descripcion_observacion_ficha_tecnica_pid_EDIT_'];
		$tipo_indicador_def_pid_EDIT_=$_POST['tipo_indicador_def_pid_EDIT_'];
		$otro_tipo_ind_pid_EDIT_=$_POST['otro_tipo_ind_pid_EDIT_'];
		$tendencia_esp_pid_EDIT_=$_POST['tendencia_esp_pid_EDIT_'];

		$linea_base_absolutos_pid_EDIT_=$_POST['linea_base_absolutos_pid_EDIT_'];
		$obj_especifico_prog_inst_EDIT_=$_POST['obj_especifico_prog_inst_EDIT_'];
		
		//$result=$id_indicador;

		if ($result=mysqli_query($kv, "UPDATE wp_indicadores_pids SET unidad_observacion_ind_pid='$unidad_observacion_ind_pid_EDIT_',descripcion_observacion_ficha_tecnica_def_pid='$descripcion_observacion_ficha_tecnica_pid_EDIT_',tipo_ind_pid='$tipo_indicador_def_pid_EDIT_',otro_tipo_ind_pid= '$otro_tipo_ind_pid_EDIT_',tendencia_pid='$tendencia_esp_pid_EDIT_',linea_base_absolutos_pid='$linea_base_absolutos_pid_EDIT_',obj_esp_prog_inst_pid='$obj_especifico_prog_inst_EDIT_' WHERE id_tactico_pid=$id_indicador AND nombre_usuario_pid='$usuario' "))
		 {
		 	echo ("sin errror");
 
  		 }
	    else 
	    {
	    	

	    	 echo("Error description: " . mysqli_error($kv));
	    }


	}



	if(isset($_POST['fuente_pid_EDIT_']))
	{
		$usuario=$_POST['nom_usuario_pid'];

		$fuente_pid_EDIT_=$_POST['fuente_pid_EDIT_'];
		$ref_adicionales_EDIT_=$_POST['ref_adicionales_EDIT_'];
		$linea_base_2017_pid_EDIT_=$_POST['linea_base_2017_pid_EDIT_'];
		$meta_2018_pid_EDIT_=$_POST['meta_2018_pid_EDIT_'];
		$meta_2019_pid_EDIT_=$_POST['meta_2019_pid_EDIT_'];
		$meta_2020_pid_EDIT_=$_POST['meta_2020_pid_EDIT_'];
		$meta_2021_pid_EDIT_=$_POST['meta_2021_pid_EDIT_'];

		$meta_2022_pid_EDIT_=$_POST['meta_2022_pid_EDIT_'];
		$meta_2030_pid_EDIT_=$_POST['meta_2030_pid_EDIT_'];
		$resultado_pid_2018_EDIT_=$_POST['resultado_pid_2018_EDIT_'];
		
		
		//$result=$id_indicador;

		if ($result=mysqli_query($kv, "UPDATE wp_indicadores_pids SET fuente_pid ='$fuente_pid_EDIT_',referencias_Adi_pid='$ref_adicionales_EDIT_',linea_base_2017_pid='$linea_base_2017_pid_EDIT_',meta_2018_pid= '$meta_2018_pid_EDIT_',meta_2019_pid='$meta_2019_pid_EDIT_',meta_2020_pid='$meta_2020_pid_EDIT_',meta_2021_pid='$meta_2021_pid_EDIT_',meta_2022_pid= '$meta_2022_pid_EDIT_',meta_2030_pid='$meta_2030_pid_EDIT_',resultado_pid_2018='$resultado_pid_2018_EDIT_' WHERE id_tactico_pid=$id_indicador AND nombre_usuario_pid='$usuario' "))
		 {
		 	echo ("sin errror");
 
  		 }
	    else 
	    {
	    	

	    	 echo("Error description: " . mysqli_error($kv));
	    }


	}




	if(isset($_POST['tipo_evidencia_def_list_pid_EDIT_']))
	{
		$usuario=$_POST['nom_usuario_pid'];

		$tipo_evidencia_def_list_pid_EDIT_=$_POST['tipo_evidencia_def_list_pid_EDIT_'];
		$tipo_evi_otro_pid_EDIT_=$_POST['tipo_evi_otro_pid_EDIT_'];
		$descripcion_evidencia_pid_EDIT_=$_POST['descripcion_evidencia_pid_EDIT_'];
		$tipo_evidencia_def_list_pid_EDIT_SELEC_=$_POST['tipo_evidencia_def_list_pid_EDIT_SELEC_'];
		$serie_evidencia_pid_EDIT_=$_POST['serie_evidencia_pid_EDIT_'];
		$seccion_pid_EDIT_=$_POST['seccion_pid_EDIT_'];
		$guia_invetario_pid_EDIT_=$_POST['guia_invetario_pid_EDIT_'];

		$medios_verificacion_evidencia_pid_EDIT_=$_POST['medios_verificacion_evidencia_pid_EDIT_'];
		$comentarios_pid_EDIT_=$_POST['comentarios_pid_EDIT_'];
		$datepicker_pid_inicio_EDIT_=$_POST['datepicker_pid_inicio_EDIT_'];
		$datepicker_pid_fin_EDIT_=$_POST['datepicker_pid_fin_EDIT_'];
		
		
		//$result=$id_indicador;

		if ($result=mysqli_query($kv, "UPDATE wp_indicadores_pids SET tipo_evidencia_pid ='$tipo_evidencia_def_list_pid_EDIT_',tipo_evidencia_otro_pid='$tipo_evi_otro_pid_EDIT_',descripcion_evidencia_pid='$descripcion_evidencia_pid_EDIT_',existe_expediente_doc_pid= '$tipo_evidencia_def_list_pid_EDIT_SELEC_',serie_evidencia_pid='$serie_evidencia_pid_EDIT_',seccion_pid='$seccion_pid_EDIT_',guia_invetario_expediente_pid='$guia_invetario_pid_EDIT_',medios_verificacion_evidencia_pid= '$medios_verificacion_evidencia_pid_EDIT_',comentarios_pid='$comentarios_pid_EDIT_',fecha_registro_pid_inicio='$datepicker_pid_inicio_EDIT_',fecha_registro_pid_fin='$datepicker_pid_fin_EDIT_' WHERE id_tactico_pid=$id_indicador AND nombre_usuario_pid='$usuario' "))
		 {
		 	echo ("sin errror");
 
  		 }
	    else 
	    {
	    	

	    	 echo("Error description: " . mysqli_error($kv));
	    }


	}



	if(isset($_POST['evaluador_pid']))
	{

		$status=$_POST['status'];
		//1= validado correctamente
		if($status==1)
		{
			if ($result=mysqli_query($kv,$result=mysqli_query($kv, "UPDATE wp_indicadores_pids SET estatus=$status,num_revision=1, comentarios_evaluador='' WHERE id_tactico_pid=$id_indicador")))
			{

			echo ("sin errror");
 
	  		 }
		    else 
		    {
		    	

		    	 echo("Error description: " . mysqli_error($kv));
		    }
		}
		else if($status==0)
		{
			$result=mysqli_query($kv, "UPDATE wp_indicadores_pids SET estatus=$status,num_revision= -1 WHERE id_tactico_pid=$id_indicador");
		}
		else
		{
			$result=mysqli_query($kv, "UPDATE wp_indicadores_pids SET estatus=$status,num_revision= 2 WHERE id_tactico_pid=$id_indicador");
		}

		
	}



		if(isset($_POST['coment_eva_pid']))
	{

		$coment_eva=$_POST['coment_eva'];
		$nom_evaluador=$_POST['nom_evaluador'];
		$nom_depen=$_POST['nom_depen'];
		//$result=$id_indicador;

		$result_2=mysqli_query($kv, "UPDATE wp_indicadores_pids SET estatus=2,comentarios_evaluador='$coment_eva',num_revision= 2 WHERE id_tactico_pid=$id_indicador");

		  $result=mysqli_query($kv, "INSERT INTO wp_historial_comentarios_evaluadores (nombre_evaluador,nombre_dependencia,id_indicador,comentario) values ('$nom_evaluador','$nom_depen',$id_indicador,'$coment_eva')");
	}

if(isset($_POST['mensaje_dependencia_pid']))
	{

		$mensaje_dependencia_pid=$_POST['mensaje_dependencia_pid'];
		
		//$result=$id_indicador;

		$result=mysqli_query($kv, "UPDATE wp_indicadores_pids SET num_revision=1,comentarios_dependecia='$mensaje_dependencia_pid' WHERE id_tactico_pid=$id_indicador");

		  //$result=mysqli_query($kv, "INSERT INTO wp_historial_comentarios_evaluadores (nombre_evaluador,nombre_dependencia,id_indicador,comentario) values ('$nom_evaluador','$nom_depen',$id_indicador,'$coment_eva')");
	}






 //data: {id_indicador:id_indicador,mensaje_dependencia:mensaje_dependencia}

}

echo $result;
$kv->close();


 ?>	
