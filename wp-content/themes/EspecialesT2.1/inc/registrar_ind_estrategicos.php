
<?php 

function definicion_ind_estrategico() {


  global $wpdb;
  if(isset($_POST['enviar_form_dindi_estr']) && $_POST['oculto_ind_est'] == "1" ):



  $est_nom_indicador = sanitize_text_field( $_POST['est_nom_indicador'] ); 
  $est_descrp_indicador = sanitize_text_field( $_POST['est_descrp_indicador'] );   
  $est_linea_discursiva = sanitize_text_field( $_POST['estr_linea_discursiva_1'] );

  $est_linea_discursiva_2 = sanitize_text_field( $_POST['estr_linea_discursiva_2'] );
  $est_linea_discursiva_3 = sanitize_text_field( $_POST['estr_linea_discursiva_3'] );

  $est_bullet_logro = sanitize_text_field( $_POST['est_bullet_logro'] );
  $est_logro_asoc_ind = sanitize_text_field( $_POST['est_logro_asoc_ind'] );
  $est_descripcion_logro = sanitize_text_field( $_POST['est_descripcion_logro'] ); 

  $est_resultado1 = sanitize_text_field( $_POST['est_resultado1'] );
  $est_resultado2 = sanitize_text_field( $_POST['est_resultado2'] );
  $est_resultado3 = sanitize_text_field( $_POST['est_resultado3'] );
  $est_resultado4 = sanitize_text_field( $_POST['est_resultado4'] );
  $est_resultado5 = sanitize_text_field( $_POST['est_resultado5'] );

  $est_res1_lugares = sanitize_text_field( $_POST['est_res1_lugares'] );
  $est_res2_lugares = sanitize_text_field( $_POST['est_res2_lugares'] );
  $est_res3_lugares = sanitize_text_field( $_POST['est_res3_lugares'] );
  $est_res4_lugares = sanitize_text_field( $_POST['est_res4_lugares'] );
  $est_res5_lugares = sanitize_text_field( $_POST['est_res5_lugares'] );

  $est_linea_base = sanitize_text_field( $_POST['est_linea_base'] );
  $est_res_abril = sanitize_text_field( $_POST['est_res_abril'] );
  $nom_dependencia = sanitize_text_field( $_POST['nom_dependencia'] );
  $est_hubo_logro = sanitize_text_field( $_POST['est_hubo_logro'] );
  $meta_2030 = sanitize_text_field( $_POST['meta_2030'] );
  $meta_2022 = sanitize_text_field( $_POST['meta_2022'] );
  $fecha_registro =date('Y-m-d');

  $est_eje_ped = sanitize_text_field( $_POST['est_eje_ped'] );
  $est_obj_estrategico = sanitize_text_field( $_POST['est_obj_estrategico'] );
  $est_ind_informe = sanitize_text_field( $_POST['est_ind_informe'] );
 

  $est_res1_lugares_gps = sanitize_text_field( $_POST['est_res1_lugares_gps'] );
  $est_res2_lugares_gps = sanitize_text_field( $_POST['est_res2_lugares_gps'] );
  $est_res3_lugares_gps = sanitize_text_field( $_POST['est_res3_lugares_gps'] );
  $est_res4_lugares_gps = sanitize_text_field( $_POST['est_res4_lugares_gps'] );
  $est_res5_lugares_gps = sanitize_text_field( $_POST['est_res5_lugares_gps'] );



  

  


  $tabla = $wpdb->prefix . "indicadores_estrategicos_definidos";
  $datos = array(

      'nom_indicador' => $est_nom_indicador,
      'descripcion_indicador' => $est_descrp_indicador,
      'linea_discursiva' => $est_linea_discursiva,

      'linea_discursiva_2'=> $est_linea_discursiva_2,
      'linea_discursiva_3'=> $est_linea_discursiva_3,


      'bullet_logro' => $est_bullet_logro,
      'logro_asoc_indicador' => $est_logro_asoc_ind,
      'descripcion_logro' => $est_descripcion_logro,

      'resultado1' => $est_resultado1,
      'resultado2' => $est_resultado2,
      'resultado3' => $est_resultado3,
      'resultado4' => $est_resultado4,
      'resultado5' => $est_resultado5,

      'lug_result_1' => $est_res1_lugares,
      'lug_result_2' => $est_res2_lugares,
      'lug_result_3' => $est_res3_lugares,
      'lug_result_4' => $est_res4_lugares,
      'lug_result_5' => $est_res5_lugares,


      'linea_base' => $est_linea_base, 
      'result_abril_2018' => $est_res_abril,
      'dependecia_encargada' => $nom_dependencia,
      'fecha_registro' =>$fecha_registro,
      'hubo_logro' => $est_hubo_logro,
      'meta_2030' => $meta_2030,
      'meta_2022' =>$meta_2022,
      'status' =>5,

      'eje_ped' =>$est_eje_ped,
      'indicainforme' =>$est_ind_informe ,
      'ob_estrategico' =>$est_obj_estrategico,
     

      'est_res1_lugares_gps' =>$est_res1_lugares_gps,
      'est_res2_lugares_gps' =>$est_res2_lugares_gps,
      'est_res3_lugares_gps' =>$est_res3_lugares_gps,
      'est_res4_lugares_gps' =>$est_res4_lugares_gps,
      'est_res5_lugares_gps' =>$est_res5_lugares_gps,



    );
    
    $formato = array(
      
      '%s',//nom_indicador
      '%s',//ndescripcion del indicador
      '%s',//descripcion
      '%s',//linea discursiva

      '%s',//linea discursiva 2
      '%s',//linea discursiva 3
      

      '%s',//logro asoc ind
      '%s',//descr logro

      '%s',//result1
      '%s',//result1
      '%s',//result1
      '%s',//result1
      '%s',//result1

      '%s',//lugar result 1
      '%s',//lugar result 1
      '%s',//lugar result 1
      '%s',//lugar result 1
      '%s',//lugar result 1



      '%s',//linea base
      '%s',//res abril 2018
      '%s',//dep
      '%s',//dep
      '%s',//res abril 2018
      '%s',//dep
      '%s',//dep
      '%s',//fecha
      

      '%s',//dep
      '%s',//dep
      '%s',//objetivos strategico

      '%s',//dep
      '%s',//res abril 2018
      '%s',//dep
      '%s',//dep
      '%s'//fecha


     
      
    );


       $result=  $wpdb->insert($tabla, $datos, $formato);

       //  echo $result;
         // $wpdb->last_query;                                //insertar los datos del formulario
          //echo '<br>Result is '.$wpdb->last_result;
          //echo '<br>Error is '.$wpdb->last_error;






          if($result==1)
          {
            $tabla_up = $wpdb->prefix . "indestrategicos";        //actualizar la tabla_up donde estan los nombres de los indicadores
            $datos_up = array(
              'status' => 1,
            );
            $formato_up = array(
              '%d'
            );
            $where = array(
              'IND_ESTRATEGICO' => $est_nom_indicador
            );

           $result= $wpdb->update($tabla_up, $datos_up,$where,$formato_up );
          }
    

   echo 'Last query: '.var_export($wpdb->last_query, TRUE);

        $url = get_page_by_title('Resumen de evidencias');
        wp_redirect( get_permalink( $url->ID ) );
      exit();
      
        
  endif;


  }


add_action('init', 'definicion_ind_estrategico');

