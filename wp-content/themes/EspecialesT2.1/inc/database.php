<?php 


// Inicializa la creación de las tablas nuevas
function indicadores_database(){
    // WPDB nos da los métodos para trabajar con tablas
    global $wpdb;
    // Agregamos una versión
    global $indicadores_dbversion;
    $indicadores_dbversion = '1.1';
    
    // Obtenemos el prefijo
    $tabla = $wpdb->prefix . 'indicadores';
    
    //obtenemos el collation de la instalación
    $charset_collate = $wpdb->get_charset_collate();
    
    // Agregamos la estructura de la BD
    $sql = "CREATE TABLE $tabla (
            id_indicador mediumint(9) NOT NULL AUTO_INCREMENT,
            nombre_indicador varchar(200) NOT NULL,
            existe_vinculo_evidencia float(5) NOT NULL,
            pudo_tener_linea_base varchar(10) NOT NULL,
            si_pudo_tener_linea_base float(5) NOT NULL,
            condicion_evidencia float(5) NOT NULL,
            presicion_evidencia float(5) NOT NULL,
            sistematizan_informacion float(5) NOT NULL,
            otro_sistematizacion varchar(1000) NOT NULL,
            evidencias_sustentadas_plan_trabajo float(5) NOT NULL,
            es_posible_desagregar_geograficamente float(5) NOT NULL,
            desagregar_geograficamente float(5) NOT NULL,
            num_inconsistencias_resultados float(5) NOT NULL,
            tipo_inconsistencia float(5) NOT NULL,
            resumen_repositorio varchar(1000) NOT NULL,
            num_inconsistencias_fuente_inf float(5) NOT NULL,
            comentarios varchar(500) NOT NULL,
            estatus int(9) NOT NULL,
            num_revision float(5) NOT NULL,
            activo varchar(9) NOT NULL,
            PRIMARY KEY (id_indicador)
    ) $charset_collate; ";
    
    // Se necesita dbDelta para ejecutar el SQL y está en la siguiente dirección
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);
    
    // Agregamos la versión de la BD Para compararla con futuras actualizaciones
    add_option('indicadores_dbversion', $indicadores_dbversion);
    
    // ACTUALIZAR EN CASO DE SER NECESARIO
    $version_actual = get_option('indicadores_dbversion');
    
    // Comparamos las 2 versiones
    if($indicadores_dbversion != $version_actual) {
          $tabla = $wpdb->prefix . 'indicadores';
          
          // Aquí realizarias las actualizaciones
  $sql = "CREATE TABLE $tabla (
            id_indicador mediumint(9) NOT NULL AUTO_INCREMENT,
            nombre_indicador varchar(200) NOT NULL,
            existe_vinculo_evidencia float(5) NOT NULL,
            pudo_tener_linea_base varchar(10) NOT NULL,
            si_pudo_tener_linea_base float(5) NOT NULL,
            condicion_evidencia float(5) NOT NULL,
            presicion_evidencia float(5) NOT NULL,
            sistematizan_informacion float(5) NOT NULL,
            otro_sistematizacion varchar(1000) NOT NULL,
            evidencias_sustentadas_plan_trabajo float(5) NOT NULL,
            es_posible_desagregar_geograficamente float(5) NOT NULL,
            desagregar_geograficamente float(5) NOT NULL,
            num_inconsistencias_resultados float(5) NOT NULL,
            tipo_inconsistencia float(5) NOT NULL,
            resumen_repositorio varchar(1000) NOT NULL,
            num_inconsistencias_fuente_inf float(5) NOT NULL,
            comentarios varchar(500) NOT NULL,
            estatus int(9) NOT NULL,
            num_revision float(5) NOT NULL,
            activo varchar(9) NOT NULL,
            PRIMARY KEY (id_indicador)
    ) $charset_collate; ";
          require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
          dbDelta($sql);
          //Actualizamos a la versión actual en caso de que asi sea
          update_option('indicadores_dbversion', $indicadores_dbversion);
    }
    
}
add_action('after_setup_theme', 'indicadores_database');


// Función para comprobar que la versión instalada es igual a la base de datos nueva.
function indicadores_db_revisar(){
  global $indicadores_dbversion;
  if(get_site_option('indicadores_dbversion') != $indicadores_dbversion) {
      indicadores_database();
  }
}
add_action('plugins_loaded', 'indicadores_db_revisar');



//creacion de la tabla definicion_indicadores
function definicion_indicadores_database(){
    // WPDB nos da los métodos para trabajar con tablas
    global $wpdb;
    // Agregamos una versión
    global $definicion_indicadores_db_version;
    $definicion_indicadores_db_version = '1.1';
    
    // Obtenemos el prefijo
    $tabla_def = $wpdb->prefix . 'definicion_evidencias';
    
    //obtenemos el collation de la instalación
    $charset_collate = $wpdb->get_charset_collate();



    
    // Agregamos la estructura de la BD
    $sql_def = "CREATE TABLE $tabla_def (
            id_indicador_asociado_def mediumint(3) NOT NULL AUTO_INCREMENT,
            nombre_indicador_asociado_def varchar(250) NOT NULL,
            linea_discursiva varchar(450) NOT NULL,
            indicador_estrella varchar(5) NOT NULL,
            descripcion_indicador_def varchar(500) NOT NULL,
            unidad_observacion_indicador_def varchar(60) NOT NULL,
            descripcion_observacion_ficha_tecnica_def varchar(350) NOT NULL,
            tipo_indicador_def varchar(50) NOT NULL,
            otro_tipo_indicador_def varchar(350) NOT NULL,
            tendencia_esperada_def varchar(30) NOT NULL,
            linea_base_absolutos_def varchar(30) NOT NULL,
            linea_base_tipo_indicador_def varchar(10) NOT NULL,
            reporte_primer_informe_def varchar(350) NOT NULL,
            meta_2018 varchar(4) NOT NULL,
            resultado_2018_2 varchar(10) NOT NULL,
            meta_planteada_2018 varchar(10) NOT NULL,
            avance_meta_planeada_def varchar(10) NOT NULL,
            tipo_evidencia_def varchar(120) NOT NULL,
            tipo_evidencia_otro_def varchar(350) NOT NULL, 
            descripcion_documento_vinculado_indicador_def varchar(455) NOT NULL,
            tipo_evidencia_fecha_def varchar(30) NOT NULL,
            existe_expediente_doc_def varchar(5) NOT NULL,
            serie_evidencia_def varchar(100) NOT NULL,
            seccion_def varchar(200) NOT NULL,
            guia_invetario_expediente_def varchar(200) NOT NULL,
            nombre_dependencia_def varchar(30) NOT NULL,
            medios_verificacion_evidencia_def varchar(350) NOT NULL,
            fecha_registro varchar(11) NOT NULL,
            comentarios_def varchar(350) NOT NULL,
            correcciones varchar(250) NOT NULL,
            estatus int(9) NOT NULL,
            num_revision float(5) NOT NULL,
            activo int(9) NOT NULL,
            coment_indicador varchar(150) NOT NULL,
            coment_evidencia varchar(150) NOT NULL,
            coment_inconsistencias varchar(150) NOT NULL,
            coment_archivos varchar(150) NOT NULL,
            discurso_importancia_ind varchar(350) NOT NULL,
            logros_indicador varchar(2) NOT NULL,
            bullet_logro varchar(500) NOT NULL,
            hubo_premio varchar(2) NOT NULL,
            resultado1 varchar(450) NOT NULL,
            resultado2 varchar(450) NOT NULL,
            resultado3 varchar(450) NOT NULL,
            resultado4 varchar(450) NOT NULL,
            resultado5 varchar(450) NOT NULL,
            lug_result1 varchar(400) NOT NULL,
            lug_result2 varchar(400) NOT NULL,
            lug_result3 varchar(400) NOT NULL,
            lug_result4 varchar(400) NOT NULL,
            lug_result5 varchar(400) NOT NULL,
            lo_que_sigue_1 varchar(450) NOT NULL,
            lo_que_sigue_2 varchar(450) NOT NULL,
            lo_que_sigue_3 varchar(450) NOT NULL,
            eje varchar(2) NOT NULL,
            PRIMARY KEY (id_indicador_asociado_def)
    ) $charset_collate; ";
    
    // Se necesita dbDelta para ejecutar el SQL_def y está en la siguiente dirección
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql_def);
    
    // Agregamos la versión de la BD Para compararla con futuras actualizaciones
    add_option('definicion_indicadores_db_version', $definicion_indicadores_db_version);
    
    // ACTUALIZAR EN CASO DE SER NECESARIO
    $version_actual = get_option('definicion_indicadores_db_version');
    
    // Comparamos las 2 versiones
    if($definicion_indicadores_db_version != $version_actual) {
          $tabla_def = $wpdb->prefix . 'definicion_evidencias';
          
    // Agregamos la estructura de la BD
    $sql_def = "CREATE TABLE $tabla_def (
            id_indicador_asociado_def mediumint(3) NOT NULL AUTO_INCREMENT,
            nombre_indicador_asociado_def varchar(250) NOT NULL,
            linea_discursiva varchar(450) NOT NULL,
            descripcion_indicador_def varchar(500) NOT NULL,
            unidad_observacion_indicador_def varchar(60) NOT NULL,
            descripcion_observacion_ficha_tecnica_def varchar(350) NOT NULL,
            tipo_indicador_def varchar(50) NOT NULL,
            otro_tipo_indicador_def varchar(350) NOT NULL,
            tendencia_esperada_def varchar(30) NOT NULL,
            linea_base_absolutos_def varchar(30) NOT NULL,
            linea_base_tipo_indicador_def varchar(10) NOT NULL,
            reporte_primer_informe_def varchar(350) NOT NULL,
            meta_2018 varchar(10) NOT NULL,
            resultado_2018_2 varchar(10) NOT NULL,
            meta_planteada_2018 varchar(10) NOT NULL,
            avance_meta_planeada_def varchar(10) NOT NULL,
            tipo_evidencia_def varchar(120) NOT NULL,
            tipo_evidencia_otro_def varchar(350) NOT NULL, 
            descripcion_documento_vinculado_indicador_def varchar(350) NOT NULL,
            tipo_evidencia_fecha_def varchar(30) NOT NULL,
            existe_expediente_doc_def varchar(5) NOT NULL,
            serie_evidencia_def varchar(100) NOT NULL,
            seccion_def varchar(200) NOT NULL,
            guia_invetario_expediente_def varchar(200) NOT NULL,
            nombre_dependencia_def varchar(30) NOT NULL,
            medios_verificacion_evidencia_def varchar(350) NOT NULL,
            fecha_registro varchar(11) NOT NULL,
            comentarios_def varchar(350) NOT NULL,
            correcciones varchar(250) NOT NULL,
            estatus int(9) NOT NULL,
            num_revision float(5) NOT NULL,
            activo int(9) NOT NULL,
            coment_indicador varchar(150) NOT NULL,
            coment_evidencia varchar(150) NOT NULL,
            coment_inconsistencias varchar(150) NOT NULL,
            coment_archivos varchar(150) NOT NULL,
            discurso_importancia_ind varchar(350) NOT NULL,
            logros_indicador varchar(2) NOT NULL,
            bullet_logro varchar(450) NOT NULL,
            hubo_premio varchar(2) NOT NULL,
            premio_reconocimiento varchar(350) NOT NULL,
            resultado1 varchar(350) NOT NULL,
            resultado2 varchar(350) NOT NULL,
            resultado3 varchar(350) NOT NULL,
            resultado4 varchar(350) NOT NULL,
            resultado5 varchar(350) NOT NULL,
            lug_result1 varchar(400) NOT NULL,
            lug_result2 varchar(400) NOT NULL,
            lug_result3 varchar(400) NOT NULL,
            lug_result4 varchar(400) NOT NULL,
            lug_result5 varchar(400) NOT NULL,
            lat_result_1 varchar(1000) NOT NULL,
            lat_result_2 varchar(1000) NOT NULL,
            lat_result_3 varchar(1000) NOT NULL,
            lat_result_4 varchar(1000) NOT NULL,
            lat_result_5 varchar(1000) NOT NULL,
            long_result_1 varchar(1000) NOT NULL,
            long_result_2 varchar(1000) NOT NULL,
            long_result_3 varchar(1000) NOT NULL,
            long_result_4 varchar(1000) NOT NULL,
            long_result_5 varchar(1000) NOT NULL,
            lo_que_sigue_1 varchar(450) NOT NULL,
            lo_que_sigue_2 varchar(450) NOT NULL,
            lo_que_sigue_3 varchar(450) NOT NULL,
            eje varchar(2) NOT NULL,
            PRIMARY KEY (id_indicador_asociado_def)
    ) $charset_collate; ";
          require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
          dbDelta($sql_def);
          //Actualizamos a la versión actual en caso de que asi sea
          update_option('definicion_indicadores_db_version', $definicion_indicadores_db_version);
    }
    
}
add_action('after_setup_theme', 'definicion_indicadores_database');


// Función para comprobar que la versión instalada es igual a la base de datos nueva.
function definicion_indicadores_db_revisar(){
  global $definicion_indicadores_db_version;
  if(get_site_option('definicion_indicadores_db_version') != $definicion_indicadores_db_version) {
      definicion_indicadores_database();
  }
}
add_action('plugins_loaded', 'definicion_indicadores_db_revisar');



//_-------------------

//creacion de la tabla definicion_indicadores
function definicion_indicadores_est_database(){
    // WPDB nos da los métodos para trabajar con tablas
    global $wpdb;
    // Agregamos una versión
    global $definicion_indicadores_est_db_version;
    $definicion_indicadores_est_db_version = '1.1';
    
    // Obtenemos el prefijo
    $tabla_def_est = $wpdb->prefix . 'indicadores_estrategicos_definidos';
    
    //obtenemos el collation de la instalación
    $charset_collate = $wpdb->get_charset_collate();



    
    // Agregamos la estructura de la BD
    $sql_ind_est = "CREATE TABLE $tabla_def_est (
            id_ind_estrategico mediumint(3) NOT NULL AUTO_INCREMENT,
            nom_indicador varchar(200) NOT NULL,
            descripcion_indicador varchar(350) NOT NULL,
            bullet_logro varchar(450) NOT NULL,
            linea_discursiva varchar(450) NOT NULL,
            descripcion_logro varchar(450) NOT NULL,
            logro_asoc_indicador varchar(2) NOT NULL,
            linea_base varchar(10) NOT NULL,
            result_abril_2018 varchar(10) NOT NULL,
            dependecia_encargada varchar(100) NOT NULL,
            hubo_logro varchar(2) NOT NULL,
            meta_2022 varchar(10) NOT NULL,
            meta_2030 varchar(10) NOT NULL,
            status int(2) NOT NULL,
            fecha_registro varchar(11) NOT NULL,
            PRIMARY KEY (id_ind_estrategico)
    ) $charset_collate; ";
    
    // Se necesita dbDelta para ejecutar el SQL_ind_est y está en la siguiente dirección
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql_ind_est);
    
    // Agregamos la versión de la BD Para compararla con futuras actualizaciones
    add_option('definicion_indicadores_est_db_version', $definicion_indicadores_est_db_version);
    
    // ACTUALIZAR EN CASO DE SER NECESARIO
    $version_actual = get_option('definicion_indicadores_est_db_version');
    
    // Comparamos las 2 versiones
    if($definicion_indicadores_est_db_version != $version_actual) {
          $tabla_def_est = $wpdb->prefix . 'indicadores_estrategicos_definidos';
          
    // Agregamos la estructura de la BD
    $sql_ind_est = "CREATE TABLE $tabla_def_est (
            id_ind_estrategico mediumint(3) NOT NULL AUTO_INCREMENT,
            nom_indicador varchar(200) NOT NULL,
            descripcion_indicador varchar(350) NOT NULL,
            bullet_logro varchar(450) NOT NULL,
            linea_discursiva varchar(450) NOT NULL,
            descripcion_logro varchar(450) NOT NULL,
            logro_asoc_indicador varchar(2) NOT NULL,
            linea_base varchar(10) NOT NULL,
            result_abril_2018 varchar(10) NOT NULL,    
            dependecia_encargada varchar(100) NOT NULL,
            hubo_logro varchar(2) NOT NULL,
            meta_2022 varchar(10) NOT NULL,
            meta_2030 varchar(10) NOT NULL,
            status int(2) NOT NULL,
            fecha_registro varchar(11) NOT NULL,
            PRIMARY KEY (id_ind_estrategico)
    ) $charset_collate; ";
          require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
          dbDelta($sql_ind_est);
          //Actualizamos a la versión actual en caso de que asi sea
          update_option('definicion_indicadores_est_db_version', $definicion_indicadores_est_db_version);
    }
    
}
add_action('after_setup_theme', 'definicion_indicadores_est_database');


// Función para comprobar que la versión instalada es igual a la base de datos nueva.
function definicion_indicadores_est_db_revisar(){
  global $definicion_indicadores_est_db_version;
  if(get_site_option('definicion_indicadores_est_db_version') != $definicion_indicadores_est_db_version) {
      definicion_indicadores_est_database();
  }
}
add_action('plugins_loaded', 'definicion_indicadores_est_db_revisar');





//_---------------------------------------------


//creacion de la tabla definicion_indicadores
function definicion_avance_programatico_database(){
    // WPDB nos da los métodos para trabajar con tablas
    global $wpdb;
    // Agregamos una versión
    global $definicion_avance_prog_db_version;
    $definicion_avance_prog_db_version = '1.1';
    
    // Obtenemos el prefijo
    $tabla_avance_prog = $wpdb->prefix . 'avance_programatico';
    
    //obtenemos el collation de la instalación
    $charset_collate = $wpdb->get_charset_collate();


    // Agregamos la estructura de la BD
    $sql_avance_prog = "CREATE TABLE $tabla_avance_prog (
            id_avance_programatico mediumint(3) NOT NULL AUTO_INCREMENT,
            nom_prog_presupuestario varchar(250) NOT NULL,
            desc_general_fin varchar(350) NOT NULL,
            indicador_fin varchar(250) NOT NULL,
            meta_prog_2018_fin varchar(15) NOT NULL,
            result_estim_30_jun_fin varchar(15) NOT NULL,
            eje varchar(100) NOT NULL,
            desc_general_prop varchar(350) NOT NULL,
            indicador_prop varchar(250) NOT NULL,
            meta_prog_2018_prop varchar(15) NOT NULL,
            result_estim_30_jun_prop varchar(15) NOT NULL,
            avance_porc_meta_2018 varchar(15) NOT NULL,
            avance_fin_meta_2018 varchar(15) NOT NULL,
            fecha_registro varchar(11) NOT NULL,
            status int(2) NOT NULL,
            nomb_dependencia varchar(100) NOT NULL,
            inversion_autorizada_fin_2017 varchar(15) NOT NULL,
            inversion_ejercida_fin_2017 varchar(15) NOT NULL,
            inversion_autorizada_fin_2018 varchar(15) NOT NULL,
            inversion_ejercida_fin_2018 varchar(15) NOT NULL,
            sector_fin varchar(50) NOT NULL,
            unidad_pres_fin varchar(250) NOT NULL,
            nivel_fin varchar(20) NOT NULL,
            nivel_prop varchar(20) NOT NULL, 
            PRIMARY KEY (id_avance_programatico)
    )$charset_collate; ";
    
    // Se necesita dbDelta para ejecutar el SQL_avance_prog y está en la siguiente dirección
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql_avance_prog);
    
    // Agregamos la versión de la BD Para compararla con futuras actualavance_progdd_option('definicion_indicadores_est_db_version', $definicion_indicadores_est_db_version);
    
    // ACTUALIZAR EN CASO DE SER NECESARIO
    $version_actual = get_option('definicion_avance_prog_db_version');
    
    // Comparamos las 2 versiones
    if($definicion_avance_prog_db_version != $version_actual) {
          $tabla_avance_prog = $wpdb->prefix . 'avance_programatico';
          
    // Agregamos la estructura de la BD
    $sql_avance_prog = "CREATE TABLE $tabla_avance_prog (
            id_avance_programatico mediumint(3) NOT NULL AUTO_INCREMENT,
            nom_prog_presupuestario varchar(250) NOT NULL,
            desc_general_fin varchar(350) NOT NULL,
            indicador_fin varchar(250) NOT NULL,
            meta_prog_2018_fin varchar(15) NOT NULL,
            result_estim_30_jun_fin varchar(15) NOT NULL,
            eje varchar(100) NOT NULL,
            desc_general_prop varchar(350) NOT NULL,
            indicador_prop varchar(250) NOT NULL,
            meta_prog_2018_prop varchar(15) NOT NULL,
            result_estim_30_jun_prop varchar(15) NOT NULL,
            avance_porc_meta_2018 varchar(15) NOT NULL,
            avance_fin_meta_2018 varchar(15) NOT NULL,
            fecha_registro varchar(11) NOT NULL,
            status int(2) NOT NULL,
            nomb_dependencia varchar(100) NOT NULL,
            inversion_autorizada_fin_2017 varchar(15) NOT NULL,
            inversion_ejercida_fin_2017 varchar(15) NOT NULL,
            inversion_autorizada_fin_2018 varchar(15) NOT NULL,
            inversion_ejercida_fin_2018 varchar(15) NOT NULL,
            sector_fin varchar(50) NOT NULL,
            unidad_pres_fin varchar(250) NOT NULL,
            nivel_fin varchar(20) NOT NULL,
            nivel_prop varchar(20) NOT NULL, 
            PRIMARY KEY (id_avance_programatico)
    ) $charset_collate; ";
          require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
          dbDelta($sql_avance_prog);
          //Actualizamos a la versión actual en caso de queavance_prog    update_option('definicion_indicadores_est_db_version', $definicion_indicadores_est_db_version);
    }
    
}
add_action('after_setup_theme', 'definicion_avance_programatico_database');


// Función para comprobar que la versión instalada es igual a la base de datos nueva.
function definicion_avance_prog_db_revisar(){
  global $definicion_avance_prog_db_version;
  if(get_site_option('definicion_avance_prog_db_version') != $definicion_avance_prog_db_version) {
      definicion_avance_programatico_database();
  }
}
add_action('plugins_loaded', 'definicion_avance_prog_db_revisar');

//_----- la base de las acciones concurrentes -----------------------------------------------







//creacion de la tabla definicion_indicadores
function acciones_concurrentes_database(){
    // WPDB nos da los métodos para trabajar con tablas
    global $wpdb;
    // Agregamos una versión
    global $acciones_concurrentes_db_version;
    $acciones_concurrentes_db_version = '1.1';
    
    // Obtenemos el prefijo
    $tabla_acciones_concurentes = $wpdb->prefix . 'acciones_concurrentes';
    
    //obtenemos el collation de la instalación
    $charset_collate = $wpdb->get_charset_collate();



    // Agregamos la estructura de la BD
    $sql_acciones_concurrentes = "CREATE TABLE $tabla_acciones_concurentes (
            id_accion_concurrente mediumint(3) NOT NULL AUTO_INCREMENT,
            nombre_accion varchar(250) NOT NULL,
            dependencias_involucradas varchar(500) NOT NULL,
            descripcion_acciones varchar(350) NOT NULL,
            nomb_dependencia varchar(50) NOT NULL,
            eje_principal varchar(80) NOT NULL,
            objetivo_est varchar(350) NOT NULL,
            objetivo_gen varchar(350) NOT NULL,
            fecha_registro varchar(11) NOT NULL,
            status int(2) NOT NULL,
            PRIMARY KEY (id_accion_concurrente)
    ) $charset_collate; ";
    
    // Se necesita dbDelta para ejecutar el SQL_acciones_concurrentes y está en la siguiente dirección
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql_acciones_concurrentes);
    
    // Agregamos la versión de la BD Para compararla con futuras actualavance_progdd_option('definicion_indicadores_est_db_version', $definicion_indicadores_est_db_version);
    
    // ACTUALIZAR EN CASO DE SER NECESARIO
    $version_actual = get_option('acciones_concurrentes_db_version');
    
    // Comparamos las 2 versiones
    if($acciones_concurrentes_db_version != $version_actual) {
          $tabla_acciones_concurentes = $wpdb->prefix . 'acciones_concurrentes';
          
    // Agregamos la estructura de la BD
    $sql_acciones_concurrentes = "CREATE TABLE $tabla_acciones_concurentes (
            id_accion_concurrente mediumint(3) NOT NULL AUTO_INCREMENT,
            nombre_accion varchar(250) NOT NULL,
            dependencias_involucradas varchar(500) NOT NULL,
            descripcion_acciones varchar(350) NOT NULL,
            nomb_dependencia varchar(50) NOT NULL,
            eje_principal varchar(80) NOT NULL,
            objetivo_est varchar(350) NOT NULL,
            objetivo_gen varchar(350) NOT NULL,
            fecha_registro varchar(11) NOT NULL,
            status int(2) NOT NULL,
            PRIMARY KEY (id_accion_concurrente)
    ) $charset_collate; ";
          require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
          dbDelta($sql_acciones_concurrentes);
          //Actualizamos a la versión actual en caso de queavance_prog    update_option('definicion_indicadores_est_db_version', $definicion_indicadores_est_db_version);
    }
    
}
add_action('after_setup_theme', 'acciones_concurrentes_database');


// Función para comprobar que la versión instalada es igual a la base de datos nueva.
function acciones_concurrentes_db_revisar(){
  global $acciones_concurrentes_db_version;
  if(get_site_option('acciones_concurrentes_db_version') != $acciones_concurrentes_db_version) {
      acciones_concurrentes_database();
  }
}
add_action('plugins_loaded', 'acciones_concurrentes_db_revisar');




//



//creacion de la tabla definicion_indicadores
function acciones_sin_indicador_database(){
    // WPDB nos da los métodos para trabajar con tablas
    global $wpdb;
    // Agregamos una versión
    global $acciones_sin_ind_db_version;
    $acciones_sin_ind_db_version = '1.1';
    
    // Obtenemos el prefijo
    $tabla_acciones_sin_indicador = $wpdb->prefix . 'acciones_sin_indicador';
    
    //obtenemos el collation de la instalación
    $charset_collate = $wpdb->get_charset_collate();


/*
  $eje_principal = sanitize_text_field( $_POST['eje_principal'] ); 
  $objetivo_est = sanitize_text_field( $_POST['objetivo_est'] );   
  $objetivo_gen = sanitize_text_field( $_POST['objetivo_gen'] );
  $accion_1 = sanitize_text_field( $_POST['accion_1'] );
  $accion_2 = sanitize_text_field( $_POST['accion_2'] );
  $accion_3 = sanitize_text_field( $_POST['accion_3'] );
  $accion_4 = sanitize_text_field( $_POST['accion_4'] );
  $accion_5 = sanitize_text_field( $_POST['accion_5'] );
  $fecha_registro =date('Y-m-d');
*/
    
    // Agregamos la estructura de la BD
    $sql_acciones_sin_ind = "CREATE TABLE $tabla_acciones_sin_indicador (
            id_accion_sin_ind mediumint(3) NOT NULL AUTO_INCREMENT,
            eje_principal varchar(80) NOT NULL,
            objetivo_est varchar(350) NOT NULL,
            objetivo_gen varchar(350) NOT NULL,
            accion_1 varchar(350) NOT NULL,
            dependencia varchar(50) NOT NULL,
            fecha_registro varchar(11) NOT NULL,
            status int(2) NOT NULL,
            PRIMARY KEY (id_accion_sin_ind)
    ) $charset_collate; ";
    
    // Se necesita dbDelta para ejecutar el SQL_acciones_sin_ind y está en la siguiente dirección
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql_acciones_sin_ind);
    
    // Agregamos la versión de la BD Para compararla con futuras actualavance_progdd_option('definicion_indicadores_est_db_version', $definicion_indicadores_est_db_version);
    
    // ACTUALIZAR EN CASO DE SER NECESARIO
    $version_actual = get_option('acciones_sin_ind_db_version');
    
    // Comparamos las 2 versiones
    if($acciones_sin_ind_db_version != $version_actual) {
          $tabla_acciones_sin_indicador = $wpdb->prefix . 'acciones_sin_indicador';
          
    // Agregamos la estructura de la BD
    $sql_acciones_sin_ind = "CREATE TABLE $tabla_acciones_sin_indicador (
            id_accion_sin_ind mediumint(3) NOT NULL AUTO_INCREMENT,
            eje_principal varchar(80) NOT NULL,
            objetivo_est varchar(350) NOT NULL,
            objetivo_gen varchar(350) NOT NULL,
            accion_1 varchar(350) NOT NULL,
            dependencia varchar(50) NOT NULL,
            fecha_registro varchar(11) NOT NULL,
            status int(2) NOT NULL,
            PRIMARY KEY (id_accion_sin_ind)
    ) $charset_collate; ";
          require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
          dbDelta($sql_acciones_sin_ind);
          //Actualizamos a la versión actual en caso de queavance_prog    update_option('definicion_indicadores_est_db_version', $definicion_indicadores_est_db_version);
    }
    
}
add_action('after_setup_theme', 'acciones_sin_indicador_database');


// Función para comprobar que la versión instalada es igual a la base de datos nueva.
function acciones_sin_indicador(){
  global $acciones_sin_ind_db_version;
  if(get_site_option('acciones_sin_ind_db_version') != $acciones_sin_ind_db_version) {
      acciones_sin_indicador_database();
  }
}
add_action('plugins_loaded', 'acciones_sin_indicador');

//----------------


//creacion de la tabla definicion_indicadores
function ctrl_moduos(){
    // WPDB nos da los métodos para trabajar con tablas
    global $wpdb;
    // Agregamos una versión
    global $ctrl_modulos_;
    $ctrl_modulos_ = '1.1';
    
    // Obtenemos el prefijo
    $tabla_ctrl_modulos = $wpdb->prefix . 'ctrl_modulos';
    
    //obtenemos el collation de la instalación
    $charset_collate = $wpdb->get_charset_collate();



    // Agregamos la estructura de la BD
    $sql_ctrl_modulos = "CREATE TABLE $tabla_ctrl_modulos (
            id_ctrl_modulo mediumint(3) NOT NULL AUTO_INCREMENT,
            nombre_dependencia varchar(150) NOT NULL,
            modulos varchar(50) NOT NULL,
            PRIMARY KEY (id_ctrl_modulo)
    ) $charset_collate; ";
    
    // Se necesita dbDelta para ejecutar el SQL_ctrl_modulos y está en la siguiente dirección
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql_ctrl_modulos);
    
    // Agregamos la versión de la BD Para compararla con futuras actualavance_progdd_option('definicion_indicadores_est_db_version', $definicion_indicadores_est_db_version);
    
    // ACTUALIZAR EN CASO DE SER NECESARIO
    $version_actual = get_option('ctrl_modulos_');
    
    // Comparamos las 2 versiones
    if($ctrl_modulos_ != $version_actual) {
          $tabla_ctrl_modulos = $wpdb->prefix . 'ctrl_modulos';
          
    // Agregamos la estructura de la BD
    $sql_ctrl_modulos = "CREATE TABLE $tabla_ctrl_modulos (
            id_ctrl_modulo mediumint(3) NOT NULL AUTO_INCREMENT,
            nombre_dependencia varchar(150) NOT NULL,
            modulos varchar(50) NOT NULL,
            PRIMARY KEY (id_ctrl_modulo)
    ) $charset_collate; ";
          require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
          dbDelta($sql_ctrl_modulos);
          //Actualizamos a la versión actual en caso de queavance_prog    update_option('definicion_indicadores_est_db_version', $definicion_indicadores_est_db_version);
    }
    
}
add_action('after_setup_theme', 'ctrl_moduos');


// Función para comprobar que la versión instalada es igual a la base de datos nueva.
function ctrl_modulos_db_revisar(){
  global $ctrl_modulos_;
  if(get_site_option('ctrl_modulos_') != $ctrl_modulos_) {
      ctrl_moduos();
  }
}
add_action('plugins_loaded', 'ctrl_modulos_db_revisar');






//-----------__BD PIDS--------------------------------


//creacion de la tabla definicion_indicadores
function indicadores_pids_database(){
    // WPDB nos da los métodos para trabajar con tablas
    global $wpdb;
    // Agregamos una versión
    global $pids_db_version;
    $pids_db_version = '1.1';
    
    // Obtenemos el prefijo
    $tabla_pids = $wpdb->prefix . 'indicadores_pids';
    
    //obtenemos el collation de la instalación
    $charset_collate = $wpdb->get_charset_collate();



    
    // Agregamos la estructura de la BD
    $sql_pids = "CREATE TABLE $tabla_pids (
            id_tactico_pid mediumint(3) NOT NULL AUTO_INCREMENT,
            nom_dependencia_pid varchar(250) NOT NULL,
            nom_organismo_pid varchar(250) NOT NULL,
            nom_indicador_pid varchar(250) NOT NULL,
            descripcion_indicador_pid varchar(500) NOT NULL,
            unidad_observacion_ind_pid varchar(60) NOT NULL,
            tipo_ind_pid varchar(50) NOT NULL,
            otro_tipo_ind_pid varchar(350) NOT NULL,
            tendencia_pid varchar(30) NOT NULL,
            obj_esp_prog_inst_pid varchar(450) NOT NULL, 
            base_calculo_pid varchar(1000) NOT NULL, 
            periodicidad_pid varchar(30) NOT NULL,
            fuente_pid varchar(300) NOT NULL,
            referencias_Adi_pid varchar(450) NOT NULL,
            linea_base_absolutos_pid varchar(30) NOT NULL,
            linea_base_2017_pid varchar(10) NOT NULL,
            meta_2017_pid varchar(10) NOT NULL, 
            meta_2018_pid varchar(10) NOT NULL,
            meta_2019_pid varchar(10) NOT NULL,
            meta_2020_pid varchar(10) NOT NULL,
            meta_2021_pid varchar(10) NOT NULL,
            meta_2022_pid varchar(10) NOT NULL,
            meta_2030_pid varchar(10) NOT NULL,
            nombre_usuario_pid varchar(80) NOT NULL,
            indica_informe_pid varchar(10) NOT NULL,
            eje_ped_pid varchar(100) NOT NULL,
            obj_estrategico_pid varchar(250) NOT NULL,
            obj_general_pid varchar(450) NOT NULL,
            avance_informe_pid varchar(10) NOT NULL,
            descripcion_observacion_ficha_tecnica_def_pid varchar(450) NOT NULL,
            tipo_evidencia_pid varchar(250) NOT NULL,
            tipo_evidencia_otro_pid varchar(350) NOT NULL, 
            existe_expediente_doc_pid varchar(5) NOT NULL,
            serie_evidencia_pid varchar(100) NOT NULL,
            seccion_pid varchar(200) NOT NULL,
            guia_invetario_expediente_pid varchar(200) NOT NULL,
            medios_verificacion_evidencia_pid varchar(350) NOT NULL,
            fecha_registro_pid_inicio varchar(25) NOT NULL,
            fecha_registro_pid_fin varchar(25) NOT NULL,
            comentarios_pid varchar(450) NOT NULL, 
            estatus_pid int(9) NOT NULL,
            logros_indicador_pid varchar(2) NOT NULL,
            bullet_logro_pid varchar(450) NOT NULL,
            hubo_premio_pid varchar(2) NOT NULL,
            premio_reconocimiento_pid varchar(450) NOT NULL,
            resultado1_pid varchar(350) NOT NULL,
            resultado2_pid varchar(350) NOT NULL,
            resultado3_pid varchar(350) NOT NULL,
            resultado4_pid varchar(350) NOT NULL,
            resultado5_pid varchar(350) NOT NULL,
            lug_result1_pid varchar(400) NOT NULL,
            lug_result2_pid varchar(400) NOT NULL,
            lug_result3_pid varchar(400) NOT NULL,
            lug_result4_pid varchar(400) NOT NULL,
            lug_result5_pid varchar(400) NOT NULL,
            lo_que_sigue_1_pid varchar(450) NOT NULL,
            lo_que_sigue_2_pid varchar(450) NOT NULL,
            lo_que_sigue_3_pid varchar(450) NOT NULL,
            resultado_pid_2018 varchar(50) NOT NULL,
	    descripcion_evidencia_pid varchar(450) NOT NULL,
	    comentarios_dependecia varchar(350) NOT NULL,
            comentarios_evaluador varchar(350) NOT NULL,
            ficha varchar(200) NOT NULL,
            estatus int(2) NOT NULL,
            num_revision float(5) NOT NULL,
            PRIMARY KEY (id_tactico_pid)
    ) $charset_collate; ";
    
    // Se necesita dbDelta para ejecutar el SQL_def y está en la siguiente dirección
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql_pids);
    
    // Agregamos la versión de la BD Para compararla con futuras actualizaciones
    add_option('pids_db_version', $pids_db_version);
    
    // ACTUALIZAR EN CASO DE SER NECESARIO
    $version_actual_pids = get_option('pids_db_version');
    
    // Comparamos las 2 versiones
    if($pids_db_version != $version_actual_pids) {
          $tabla_pids = $wpdb->prefix . 'indicadores_pids';
          
    // Agregamos la estructura de la BD
    $sql_pids = "CREATE TABLE $tabla_pids (
            iid_tactico_pid mediumint(3) NOT NULL AUTO_INCREMENT,
            nom_dependencia_pid varchar(250) NOT NULL,
            nom_organismo_pid varchar(250) NOT NULL,
            nom_indicador_pid varchar(250) NOT NULL,
            descripcion_indicador_pid varchar(500) NOT NULL,
            unidad_observacion_ind_pid varchar(60) NOT NULL,
            tipo_ind_pid varchar(50) NOT NULL,
            otro_tipo_ind_pid varchar(350) NOT NULL,
            tendencia_pid varchar(30) NOT NULL,
            obj_esp_prog_inst_pid varchar(450) NOT NULL, 
            base_calculo_pid varchar(1000) NOT NULL, 
            periodicidad_pid varchar(30) NOT NULL,
            fuente_pid varchar(300) NOT NULL,
            referencias_Adi_pid varchar(450) NOT NULL,
            linea_base_absolutos_pid varchar(30) NOT NULL,
            linea_base_2017_pid varchar(10) NOT NULL,
            meta_2017_pid varchar(10) NOT NULL, 
            meta_2018_pid varchar(10) NOT NULL,
            meta_2019_pid varchar(10) NOT NULL,
            meta_2020_pid varchar(10) NOT NULL,
            meta_2021_pid varchar(10) NOT NULL,
            meta_2022_pid varchar(10) NOT NULL,
            meta_2030_pid varchar(10) NOT NULL,
            nombre_usuario_pid varchar(80) NOT NULL,
            indica_informe_pid varchar(10) NOT NULL,
            eje_ped_pid varchar(100) NOT NULL,
            obj_estrategico_pid varchar(250) NOT NULL,
            obj_general_pid varchar(350) NOT NULL,
            avance_informe_pid varchar(10) NOT NULL,
            descripcion_observacion_ficha_tecnica_def_pid varchar(450) NOT NULL,
            tipo_evidencia_pid varchar(250) NOT NULL,
            tipo_evidencia_otro_pid varchar(350) NOT NULL, 
            existe_expediente_doc_pid varchar(5) NOT NULL,
            serie_evidencia_pid varchar(100) NOT NULL,
            seccion_pid varchar(200) NOT NULL,
            guia_invetario_expediente_pid varchar(200) NOT NULL,
            medios_verificacion_evidencia_pid varchar(350) NOT NULL,
            fecha_registro_pid_inicio varchar(25) NOT NULL,
            fecha_registro_pid_fin varchar(25) NOT NULL,
            comentarios_pid varchar(450) NOT NULL, 
            estatus_pid int(9) NOT NULL,
            logros_indicador_pid varchar(2) NOT NULL,
            bullet_logro_pid varchar(450) NOT NULL,
            hubo_premio_pid varchar(2) NOT NULL,
            premio_reconocimiento_pid varchar(450) NOT NULL,
            resultado1_pid varchar(350) NOT NULL,
            resultado2_pid varchar(350) NOT NULL,
            resultado3_pid varchar(350) NOT NULL,
            resultado4_pid varchar(350) NOT NULL,
            resultado5_pid varchar(350) NOT NULL,
            lug_result1_pid varchar(400) NOT NULL,
            lug_result2_pid varchar(400) NOT NULL,
            lug_result3_pid varchar(400) NOT NULL,
            lug_result4_pid varchar(400) NOT NULL,
            lug_result5_pid varchar(400) NOT NULL,
            lo_que_sigue_1_pid varchar(450) NOT NULL,
            lo_que_sigue_2_pid varchar(450) NOT NULL,
            lo_que_sigue_3_pid varchar(450) NOT NULL,
            resultado_pid_2018 varchar(50) NOT NULL,
	    descripcion_evidencia_pid varchar(450) NOT NULL,
	    comentarios_dependecia varchar(350) NOT NULL,
            comentarios_evaluador varchar(350) NOT NULL,
            ficha varchar(200) NOT NULL,
            estatus int(2) NOT NULL,
            num_revision float(5) NOT NULL,
            PRIMARY KEY (id_tactico_pid)
    ) $charset_collate; ";
          require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
          dbDelta($sql_pids);
          //Actualizamos a la versión actual en caso de que asi sea
          update_option('pids_db_version', $pids_db_version);
    }
    
}
add_action('after_setup_theme', 'indicadores_pids_database');


// Función para comprobar que la versión instalada es igual a la base de datos nueva.
function indicadores_pids_db_revisar(){
  global $pids_db_version;
  if(get_site_option('pids_db_version') != $pids_db_version) {
      indicadores_pids_database();
  }
}
add_action('plugins_loaded', 'indicadores_pids_db_revisar');





//





