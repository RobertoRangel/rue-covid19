<?php 

function actualizar_evidencia() {
  global $wpdb;

  //--actualizar Formulario general
  
  if(isset($_POST['actualizarevision']) && $_POST['oculto'] == "1" ):
        $comentarios = sanitize_text_field( $_POST['comentarios_corregir'] );
        $id_indicador = sanitize_text_field( $_POST['id_evidencia'] );
        $chek_status = sanitize_text_field( $_POST['chek_status'] );
        $num_revision=sanitize_text_field( $_POST['num_revision'] );
        $status;
        /*
        if($chek_status=='on')
          {
            $status=1;
            $comentarios="Tus Evidencias han sido Aceptadas";
          }
          else if ($comentarios!="") 
          {
          $status=2;
          $num_revision=$num_revision+0.1;
          }
          else
          {
             $status=0;
          }
          */
          

        
        $tabla = $wpdb->prefix . "definicion_evidencias";
        $datos = array(
          'correcciones' => $comentarios,
          'estatus' => $status,
          'num_revision' => $num_revision,
    
          
        );
        
        $formato = array(
          '%s',
          '%d',
          '%f'
      
        );
         $where = array(
          'id_indicador_asociado_def' => $id_indicador 
      
        );
        
       
        $wpdb->update($tabla, $datos,$where,$formato );
        
        $url = get_page_by_title('Proceso de Evaluacion');
        wp_redirect( get_permalink( $url->ID ) );
      
      //  exit();
  endif;

  //actualizarevision_archivo

  /*

  if(isset($_POST['actualizarevision_archivo']) && $_POST['oculto'] == "1" ):
        $comentarios = sanitize_text_field( $_POST['comentarios_archivo'] );
        $id_indicador = sanitize_text_field( $_POST['id_indicador'] );
        $chek_status = sanitize_text_field( $_POST['chek_status_num_version'] );
        $status;
        if($chek_status=='on')
          {
            $status=1;
          }
          else
          {
             $status=0;
          }

        
        $tabla = $wpdb->prefix . "upload_files";
        $datos = array(
          'comentarios' => $comentarios,
          'estatus' => $status,
    
          
        );
        
        $formato = array(
          '%s',
          '%d'
      
        );
         $where = array(
          'ID' => $id_indicador 
      
        );
        
       
        $wpdb->update($tabla, $datos,$where,$formato );
        
        $url = get_page_by_title('contacto');
        wp_redirect( get_permalink( $url->ID ) );
      
        exit();
  endif;

*/

}

add_action('init', 'actualizar_evidencia');