
<?php 


header('Content-Type: text/html; charset=UTF-8');


function definicion_evidencias_guardar() {

  global $wpdb;
  if(isset($_POST['enviar_form_def_evidencia']) && $_POST['oculto'] == "1" ):
  $nombre_indicador_asociado_def = sanitize_text_field( $_POST['nombre_indicador_asociado_def'] );  
  $linea_discursiva = sanitize_text_field( $_POST['discurso_importancia_ind'] );  
  $indicador_estrella = sanitize_text_field( $_POST['clase_indicador']);  

  $descripcion_indicador_dif = sanitize_text_field( $_POST['descripcion_indicador_dif'] );
  $unidad_observacion_indicador_def = sanitize_text_field( $_POST['unidad_observacion_indicador_def'] );
  $descripcion_observacion_ficha_tecnica = sanitize_text_field( $_POST['descripcion_observacion_ficha_tecnica'] );
  $tipo_indicador_def = sanitize_text_field( $_POST['tipo_indicador_def'] );
  $otro_tipo_indicador_def = sanitize_text_field( $_POST['otro_tipo_indicador_def'] );
  $tendencia_esperada_def = sanitize_text_field( $_POST['tendencia_esperada_def'] );

  $linea_base_absolutos_def = sanitize_text_field( $_POST['linea_base_absolutos_def'] );
  $descripcion_obj_secto = sanitize_text_field( $_POST['descripcion_obj_secto'] );

  
  /*
  $linea_base_tipo_indicador_def = sanitize_text_field( $_POST['linea_base_tipo_indicador_def'] );
  */
  $reporte_primer_informe_def = sanitize_text_field( $_POST['reporte_primer_informe_def'] );

  $avance_2018 = sanitize_text_field( $_POST['avance_2018'] );
  $meta_planteada_2018_oculto = sanitize_text_field( $_POST['meta_planteada_2018_oculto'] );
  $avance_meta_planeada_def = sanitize_text_field( $_POST['avance_meta_planeada_def']);
  $tipo_evidencia_def = sanitize_text_field( $_POST['tipo_evidencia_def_list'] );
  $tipo_evidencia_otro_def = sanitize_text_field( $_POST['tipo_evidencia_otro_def'] );
  $descripcion_documento_vinculado_indicador_def = sanitize_text_field( $_POST['descripcion_documento_vinculado_indicador_def'] );
  $tipo_evidencia_fecha_def = sanitize_text_field( $_POST['tipo_evidencia_fecha_def'] );
  //$fecha_evidencia_inicio_def = sanitize_text_field( $_POST['fecha_evidencia_inicio_def']);

  $existe_expediente_doc_def = sanitize_text_field( $_POST['existe_expediente_doc_def'] );
  $serie_evidencia_def = sanitize_text_field( $_POST['serie_evidencia_def'] );
  $seccion_def = sanitize_text_field( $_POST['seccion_def'] );
  $guia_invetario_expediente = sanitize_text_field( $_POST['guia_invetario_expediente_def'] );
  $nombre_dependencia_def = sanitize_text_field( $_POST['nombre_dependencia_def'] );
  $medios_verificacion_evidencia = sanitize_text_field( $_POST['medios_verificacion_evidencia'] );
  $fecha_registro =date('Y-m-d');
  $comentarios_def = sanitize_text_field( $_POST['comentarios_def'] );
  //datos que agrego luz 
  $discurso_importancia_ind = sanitize_text_field( $_POST['discurso_importancia_ind'] );
  $logros_en_indicador = sanitize_text_field( $_POST['logros_en_indicador'] );
  $bullet_Del_logro = sanitize_text_field( $_POST['id_bullet_Del_logro'] );

  $premios_recono = sanitize_text_field( $_POST['premios_recono'] );
  $reconocimiento = sanitize_text_field( $_POST['reconocimiento'] );

  

  $resultado_1 = sanitize_text_field( $_POST['resultado_1'] );
  $resultado_2 = sanitize_text_field( $_POST['resultado_2'] );
  $resultado_3 = sanitize_text_field( $_POST['resultado_3'] );
  $resultado_4 = sanitize_text_field( $_POST['resultado_4'] );
  $resultado_5 = sanitize_text_field( $_POST['resultado_5'] );

  $resultado_6 = sanitize_text_field( $_POST['resultado_6'] );
  $resultado_7 = sanitize_text_field( $_POST['resultado_7'] );
  $resultado_8 = sanitize_text_field( $_POST['resultado_8'] );


  $res1_lugares = sanitize_text_field( $_POST['res1_lugares'] );
  $res2_lugares = sanitize_text_field( $_POST['res2_lugares'] );
  $res3_lugares = sanitize_text_field( $_POST['res3_lugares'] );
  $res4_lugares = sanitize_text_field( $_POST['res4_lugares'] );
  $res5_lugares = sanitize_text_field( $_POST['res5_lugares'] );
  $res6_lugares = sanitize_text_field( $_POST['res6_lugares'] );
  $res7_lugares = sanitize_text_field( $_POST['res7_lugares'] );
  $res8_lugares = sanitize_text_field( $_POST['res8_lugares'] );


  $res1_lugares_gps = sanitize_text_field( $_POST['res1_lugares_gps'] );
  $res2_lugares_gps = sanitize_text_field( $_POST['res2_lugares_gps'] );
  $res3_lugares_gps = sanitize_text_field( $_POST['res3_lugares_gps'] );
  $res4_lugares_gps = sanitize_text_field( $_POST['res4_lugares_gps'] );
  $res5_lugares_gps = sanitize_text_field( $_POST['res5_lugares_gps'] );
  $res6_lugares_gps = sanitize_text_field( $_POST['res6_lugares_gps'] );
  $res7_lugares_gps = sanitize_text_field( $_POST['res7_lugares_gps'] );
  $res8_lugares_gps = sanitize_text_field( $_POST['res8_lugares_gps'] );



 
  $lo_que_sigue_1 = sanitize_text_field( $_POST['lo_que_sigue_1'] );
  $lo_que_sigue_2 = sanitize_text_field( $_POST['lo_que_sigue_2'] );
  $lo_que_sigue_3 = sanitize_text_field( $_POST['lo_que_sigue_3'] );
  $eje = sanitize_text_field( $_POST['eje_pertenece'] );


  $cve_unica = sanitize_text_field( $_POST['cve_unica'] );
  $cime = sanitize_text_field( $_POST['cime'] );
  $informe2019 = sanitize_text_field( $_POST['informe2019'] );
  $eje_ped = sanitize_text_field( $_POST['eje_ped'] );
  $ob_est = sanitize_text_field( $_POST['ob_est'] );
  $ob_gen = sanitize_text_field( $_POST['ob_gen'] );
  $ods = sanitize_text_field( $_POST['ods'] );
  $ob_secto = sanitize_text_field( $_POST['ob_secto'] );
  $periodisidad = sanitize_text_field( $_POST['periodisidad'] );
  $fuente = sanitize_text_field( $_POST['fuente'] );
  $ref_ad = sanitize_text_field( $_POST['ref_ad'] );


  $resultado_19=sanitize_text_field( $_POST['resultado_19'] );
  $meta_2019=sanitize_text_field( $_POST['meta_2019'] );


  

                                   


  if($existe_expediente_doc_def=="Si")//caso del expediente documental  el resto de los campos estaran vacios
  {
   $medios_verificacion_evidencia="";
  }
  else if($existe_expediente_doc_def=="No")
  {
    $serie_evidencia_def = "";
    $seccion_def = "";
    $guia_invetario_expediente = "";
  }


        $tabla = $wpdb->prefix . "definicion_evidencias";
        $datos = array(
          'nombre_indicador_asociado_def' => $nombre_indicador_asociado_def,
          'linea_discursiva' => $linea_discursiva,
          'indicador_estrella' => $indicador_estrella,
          'logros_indicador' => $logros_en_indicador,
          'bullet_logro' => $bullet_Del_logro,
          'hubo_premio' => $premios_recono,
          'premio_reconocimiento' => $reconocimiento,



          'resultado1' => $resultado_1,  
          'resultado2' => $resultado_2,
          'resultado3' => $resultado_3,
          'resultado4' => $resultado_4,
          'resultado5' => $resultado_5,

          'resultado6' => $resultado_6,
          'resultado7' => $resultado_7,
          'resultado8' => $resultado_8,

          'lug_result1' => $res1_lugares,  
          'lug_result2' => $res2_lugares,
          'lug_result3' => $res3_lugares,
          'lug_result4' => $res4_lugares,
          'lug_result5' => $res5_lugares,

          'lug_result6' => $res6_lugares,
          'lug_result7' => $res7_lugares,
          'lug_result8' => $res8_lugares,

          'lo_que_sigue_1' =>$lo_que_sigue_1,
          'lo_que_sigue_2' => $lo_que_sigue_2,
          'lo_que_sigue_3' => $lo_que_sigue_3,

          'descripcion_indicador_def' => $descripcion_indicador_dif,
          'unidad_observacion_indicador_def' => $unidad_observacion_indicador_def,
          'descripcion_observacion_ficha_tecnica_def' => $descripcion_observacion_ficha_tecnica,
          'tipo_indicador_def' => $tipo_indicador_def,
          'otro_tipo_indicador_def' => $otro_tipo_indicador_def,
         

          'tendencia_esperada_def' => $tendencia_esperada_def,
          'linea_base_absolutos_def' => $linea_base_absolutos_def,
          
          'descripcion_obj_secto' => $descripcion_obj_secto,
          'reporte_primer_informe_def' => $reporte_primer_informe_def,
          'resultado_19' => $resultado_19,
          'meta_planteada_2018' => $meta_planteada_2018_oculto,
          'avance_meta_planeada_def' => $avance_meta_planeada_def,
          
          'tipo_evidencia_def' => $tipo_evidencia_def,
          'tipo_evidencia_otro_def' => $tipo_evidencia_otro_def,
          'descripcion_documento_vinculado_indicador_def' => $descripcion_documento_vinculado_indicador_def,
          'tipo_evidencia_fecha_def' => $tipo_evidencia_fecha_def,
          'existe_expediente_doc_def' => $existe_expediente_doc_def,
          'serie_evidencia_def' => $serie_evidencia_def,
          'seccion_def' => $seccion_def,
          
          'guia_invetario_expediente_def' => $guia_invetario_expediente,
          'nombre_dependencia_def' => $nombre_dependencia_def,
          'medios_verificacion_evidencia_def' => $medios_verificacion_evidencia,
          'fecha_registro' =>$fecha_registro,
          'comentarios_def' => $comentarios_def,
          //'discurso_importancia_ind' => $discurso_importancia_ind,
          'estatus' => 0,
          'eje' => $eje,


          'cve_unica' => $cve_unica,
          'cime' => $cime,
          'informe2019' => $informe2019,
          'eje_ped' => $eje_ped,
          'ob_est' => $ob_est,
          'ob_gen' => $ob_gen,
          'ods' => $ods,
          'ob_secto' => $ob_secto,
          'periodisidad' => $periodisidad,
          'fuente' => $fuente,
          'ref_ad' => $ref_ad,
          'meta_2019' => $meta_2019
          
  
        );
        
        $formato = array(
          '%s',//nombre_indicador_asociado_def
          '%s',//linea discursiva
          '%s',//indicador estrella
          '%s', //logros_en_indicador
          '%s',//bullet_Del_logro

          '%s', //logros_en_indicador
          '%s',//bullet_Del_logro

          '%s',//resultado_1
          '%s',//resultado_2
          '%s',//resultado_3
          '%s',//resultado_4
          '%s',//resultado_5
          '%s',//resultado_6
          '%s',//resultado_7
          '%s',//resultado_8

          '%s',//result_lugar_1
          '%s',//result_lugar_2
          '%s',//result_lugar_3
          '%s',//result_lugar_4
          '%s',//result_lugar_5
          '%s',//result_lugar_6
          '%s',//result_lugar_7
          '%s',//result_lugar_8
         
          '%s',//lo_que_sigue_1
          '%s',//lo_que_sigue_2
          '%s',//lo_que_sigue_3

          '%s',//descripcion_indicador_dif
          '%s',//unidad_observacion_indicador_def
          '%s',//descripcion_observacion_ficha_tecnica
          '%s',//tipo_indicador_def
          '%s',//otro_tipo_indicador_def

          '%s',//tendencia_esperada_def
          '%s',//linea_base_absolutos_def
          '%s',//desc_oj_secto
          '%s',//meta
          '%s',//meta planteada
          '%s',//reporte_primer_informe_def
          '%s',//avance_meta_planeada_def


 

          '%s',//tipo_evidencia_def,
          '%s',//tipo_evidencia_otro_def

          '%s',//descripcion_documento_vinculado_indicador_def
          '%s',//tipo_evidencia_fecha_def
         
          '%s',//existe_expediente_doc_def
          '%s', //serie_evidencia_def
          '%s',//seccion_def
          
          '%s',//guia_invetario_expediente
          '%s',//nombre_dependencia_def
          '%s',//medios_verificacion_evidencia
          '%s',//fecha_registro
          '%s',//comentarios_def
          //'%s',//discurso_importancia_ind
          '%s',//status editable =5
          '%s',


          '%s',//cve_unica
          '%s',//cime
          '%s',//informe2019
          '%s',//eje_ped
          '%s',//ob_est
          '%s',//ob_gen
          '%s',//ods
          '%s',//ob_secto
          '%s',//periodisidad
          '%s',//fuente
          '%s',//ref_ad
          '%s'//meta2019



        );
        
        $result=  $wpdb->insert($tabla, $datos, $formato);

         echo $result;
          $wpdb->last_query;                                //insertar los datos del formulario
          echo '<br>Result is '.$wpdb->last_result;
          echo '<br>Error is '.$wpdb->last_error;

        if($result==1)//REGISTRO CORRECTO
        {
                      //REGISTRAR LAS REFERENCIAS GPS
          $tabla_gps = $wpdb->prefix . "obras_acciones";
          $datos_gps = array(

          'nombre_indicador' => $nombre_indicador_asociado_def,
          'dependencia' => $nombre_dependencia_def,

          'res1_lugares_gps' => $res1_lugares_gps,  
          'res2_lugares_gps' => $res2_lugares_gps,
          'res3_lugares_gps' => $res3_lugares_gps,  
          'res4_lugares_gps' => $res4_lugares_gps,
          'res5_lugares_gps' => $res5_lugares_gps,  
          'res6_lugares_gps' => $res6_lugares_gps,
          'res7_lugares_gps' => $res7_lugares_gps,  
          'res8_lugares_gps' => $res8_lugares_gps,
        );


          $formato_gps = array(
          '%s',//nombre_indicador
          '%s',//dependencia discursiva

          '%s',//res1_lugares_gps estrella
          '%s',//res2_lugares_gps
          '%s',//res3_lugares_gps
          '%s',//res4_lugares_gps
          '%s',//res5_lugares_gps
          '%s',//res6_lugares_gps
          '%s',//res7_lugares_gps
          '%s',//res8_lugares_gps
          '%s'
        );
        
        $result_gps=  $wpdb->insert($tabla_gps, $datos_gps, $formato_gps);
        
          echo $result_gps;
          /*$wpdb->last_query;                                //insertar los datos del formulario
          echo '<br>Result is '.$wpdb->last_result;
          echo '<br>Error is '.$wpdb->last_error;*/
          

          $tabla_up = $wpdb->prefix . "tacticos_base_2019";        //actualizar la tabla_up donde estan los nombres de los indicadores
          $datos_up = array(
            'status' => 1,
          );
          $formato_up = array(
            '%d'
          );
          $where = array(
            'indicador' => $nombre_indicador_asociado_def
          );
          $result = $wpdb->update($tabla_up, $datos_up,$where,$formato_up );
          
         $url = get_page_by_title('Cargado Correctamente');
          wp_redirect( get_permalink( $url->ID ) );
         
          
          

        }
        else
        {
         $url = get_page_by_title('Cargado Correctamente');
          wp_redirect( get_permalink( $url->ID ) );
        }
        
  


      
      exit();
      
        
  endif;




}

add_action('init', 'definicion_evidencias_guardar');
