
<?php 

function definicion_acciones_concurrentes() {

  global $wpdb;
  if(isset($_POST['enviar_form_acciones_concurrentes']) && $_POST['oculto_acciones'] == "1"):
   

  $accion_realizada = sanitize_text_field( $_POST['accion_realizada']); 
  $dependecias_selecionadas = sanitize_text_field( $_POST['dependecias_selecionadas_concu'] );  
  $descipcion_acciones = sanitize_text_field( $_POST['descipcion_acciones']);
  $acciones_nombre_dependencia = sanitize_text_field( $_POST['accion_concurrente_nom_dependencia']);

  $eje_principal = sanitize_text_field( $_POST['eje_principal_concu'] ); 
  $objetivo_est = sanitize_text_field( $_POST['objetivo_est_concu'] );   
  $objetivo_gen = sanitize_text_field( $_POST['objetivo_gen_concu'] );
  $fecha_registro =date('Y-m-d');
  
  $tabla = $wpdb->prefix ."acciones_concurrentes";
  $datos = array(

      'nombre_accion' => $accion_realizada,
      'dependencias_involucradas' => $dependecias_selecionadas,
      'descripcion_acciones' => $descipcion_acciones,
      'nomb_dependencia' => $acciones_nombre_dependencia,
      'eje_principal' => $eje_principal,
      'objetivo_est' => $objetivo_est,
      'objetivo_gen' => $objetivo_gen,
      'fecha_registro' => $fecha_registro,
      'status' =>5,
     
    );
    
    $formato = array(

      '%s',//nombre de accion
      '%s',//dependecnias involucradas
      '%s',//descripcion de las acciones realizadas
      '%s',//nomb_depenedencia
      '%s',//fecha_efectivo
      '%s',//descripcion de las acciones realizadas
      '%s',//nomb_depenedencia
      '%s',//fecha_efectivo
      '%s'//fecha_efectivo
    );
 
    $result=  $wpdb->insert($tabla, $datos, $formato);
      echo $result;
    $wpdb->last_query;                                //insertar los datos del formulario


        $url = get_page_by_title('Acciones Concurrentes');
        wp_redirect( get_permalink( $url->ID ) );
       
        
      exit();
        
  endif;
  


  }


add_action('init', 'definicion_acciones_concurrentes');

