
<?php 

function definicion_ctrl_modulos() {


  global $wpdb;
  if(isset($_POST['enviar_form_modulos']) && $_POST['oculto_ctrl_modulos'] == "1"):

  $contraloria_valores = sanitize_text_field( $_POST['contraloria_valores']);
  $sec_cultura_valores = sanitize_text_field( $_POST['sec_cultura_valores']);
  $sep_valores = sanitize_text_field( $_POST['sep_valores']);
  $sec_gob_valores = sanitize_text_field( $_POST['sec_gob_valores']);
  $procuraduria_valores = sanitize_text_field( $_POST['procuraduria_valores']);
  $sec_movy_valores = sanitize_text_field( $_POST['sec_movy_valores']);

  $sec_salud_valores = sanitize_text_field( $_POST['sec_salud_valores']);
  $sec_turismo_valores = sanitize_text_field( $_POST['sec_turismo_valores']);

  $sec_ejecutiva_valores = sanitize_text_field( $_POST['sec_ejecutiva_valores']);
  $sec_desagro_valores = sanitize_text_field( $_POST['sec_desagro_valores']);
  $sec_desecon_valores = sanitize_text_field( $_POST['sec_desecon_valores']);
  $semarnath_valores = sanitize_text_field( $_POST['semarnath_valores']);
  $uni_planea_valores = sanitize_text_field( $_POST['uni_planea_valores']);
  $ofi_mayor_valores = sanitize_text_field( $_POST['ofi_mayor_valores']);

  $difh_valores = sanitize_text_field( $_POST['difh_valores']);
  $sec_finanzas_valores = sanitize_text_field( $_POST['sec_finanzas_valores']);
  $sopot_valores = sanitize_text_field( $_POST['sopot_valores']);
  $sedeso_valores = sanitize_text_field( $_POST['sedeso_valores']);
  $rehilete_valores = sanitize_text_field( $_POST['rehilete_valores']);
  $citnova_valores = sanitize_text_field( $_POST['citnova_valores']);

  $sec_segup_valores = sanitize_text_field( $_POST['sec_segup_valores']);
  $sec_trabyps_valores = sanitize_text_field( $_POST['sec_trabyps_valores']);

  $arra_datos=Array();
  $arra_dependencias=Array("CONTRALORIA","SEC_CULTURA","SEP","SEC_GOB","PROCURADURIA","SEC_MOVYTRANS","SEC_SALUD","SEC_TURISMO","SEC_EJECUTIVA","SEC_DESAGRO","SEC_DESECON","SEMARNATH","UNI_PLANEA","OFI_MAYOR","DIFH","SEC_FINANZAS","SOPOT","SEC_SEGUP","SEDESO","CITNOVA","REHILETE","SEC_TRABYPS");
  
  array_push($arra_datos,$contraloria_valores,$sec_cultura_valores,$sep_valores,$sec_gob_valores,$procuraduria_valores,$sec_movy_valores,$sec_salud_valores,$sec_turismo_valores,$sec_ejecutiva_valores,$sec_desagro_valores,$sec_desecon_valores,$semarnath_valores,$uni_planea_valores,$ofi_mayor_valores,$difh_valores,$sec_finanzas_valores,$sopot_valores,$sec_segup_valores,$sedeso_valores,$citnova_valores,$rehilete_valores,$sec_trabyps_valores);

    $tabla_up = $wpdb->prefix . "ctrl_modulos";        //actualizar

    for ($i=0; $i < count($arra_dependencias); $i++) 
    {
      $dep=$arra_dependencias[$i];
      $valores=$arra_datos[$i];
    //  echo($dep);
      //echo($valores);

      $datos_up = array(
        'modulos' => $valores,
      );
      $formato_up = array(
        '%s'
      );
      $where = array(
        'nombre_dependencia' => $dep
      );

      $result=$wpdb->update($tabla_up, $datos_up,$where,$formato_up );
     
    }

    


   // $url = get_page_by_title('dashboard');
   // wp_redirect( get_permalink( $url->ID ) );

      exit();
        
  endif;
  


  }


add_action('init', 'definicion_ctrl_modulos');

