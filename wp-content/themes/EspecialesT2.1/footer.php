	<!--div class="space_bottom clearfix small-bottom-buffer "></div-->
<div class="main-footer" style="background-color:#1f2428 ">
    <div class="container" >
        <div class="row">
            <div class="col-sm-4">
                <a href=""><img alt="logotipo hidalgo.gob.mx" class="gobmx-footer" src="http://cdn.hidalgo.gob.mx/logo_gobhidalgo.svg" width="226" height="39" style="margin-top: 90px;" /></a>
            </div>
            <div class="imgfoot col-xs-12 col-sm-4">
                <img src="http://cdn.hidalgo.gob.mx/escudo_blanco.svg" width="50%" height="50%" style="margin-left:85px;">
                <p style="text-align:center; ">&copy; 2018 Gobierno del Estado de Hidalgo
                <p>
            </div>
            <div class="col-sm-4 redes">
                <p style="text-align:center;margin-top: 80px;"><strong style="font-size: 20px;">Contacto</strong><br>Allende 901, Centro, 42000<br>Pachuca de Soto, Hidalgo, México<br>+52 (771) 716 9079</p>
                <!--<div>
                    <center>
                        <a style="font-size:30px; padding: 10px;" href="https://es-la.facebook.com/SecretariaEjecutivaHGO/" target="_blank"><i class="fab fa-facebook-square"></i></a>
                        <a style="font-size:30px; padding: 10px;" href="https://twitter.com/seppehidalgo?lang=es" target="_blank"><i class="fab fa-twitter-square"></i></a>
                    </center>
                </div>-->
            </div>
        </div>
    </div>
</div>

	    <script type="text/javascript" src="<?php bloginfo('template_directory') ?>/js/universal/jquery.js"></script> 
		<script src="<?php bloginfo('template_directory') ?>/js/bootstrap.min.js" type="text/javascript"></script> 
		<script src="<?php bloginfo('template_directory') ?>/js/mainmenu/jquery.sticky.js"></script> 

		<script src="<?php bloginfo('template_directory') ?>/js/tabs/responsive-tabs.min.js" type="text/javascript"></script> 
 
		<script src="<?php bloginfo('template_directory') ?>/js/functions.js" type="text/javascript"></script>
		<!--sitio Secretaria 2017--> 

<?php wp_footer(); ?>
</body>
</html>