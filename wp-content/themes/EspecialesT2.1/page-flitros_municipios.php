
<?php get_header(); 
/*
*  Template Name: flitros_municipios
*/
?>

<style>

  .accordion_filtros {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
  }

  .active, .accordion_filtros:hover {
    background-color: #ccc;
  }

  .accordion_filtros:after {
    content: '\002B';
    color: #777;
    font-weight: bold;
    float: right;
    margin-left: 5px;
  }

  .active:after {
    content: "\2212";
  }

  .panel_fil {
    padding: 0 18px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
  }

</style>

<section id="main-content">
<div class="container">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12">

  <?php while(have_posts()): the_post(); ?>

   

              
          

                <h2>Busqueda por Filtros (Prueba)</h2>
                <div class="row">
                  <div class="col-md-4">

                    <h2>Estados</h2>
                    <select  id="Estados" multiple>
                      <option value="Aguascalientes">Aguascalientes</option>
                      <option value="Baja California">Baja California</option>
                      <option value="Baja California Sur">Baja California Sur</option>
                      <option value="Campeche">Campeche</option>

                      <option value="Chiapas">Chiapas</option>
                      <option value="Chihuahua">Chihuahua</option>
                      <option value="Ciudad de México">Ciudad de México</option>
                      <option value="Coahuila">Coahuila</option>

                      <option value="Colima">Colima</option>
                      <option value="Durango">Durango</option>
                      <option value="Estado de México">Estado de México</option>
                      <option value="Guanajuato">Guanajuato</option>

                      <option value="Guerrero">Guerrero</option>
                      <option selected="selected" value="Hidalgo">Hidalgo</option>
                      <option value="Jalisco">Jalisco</option>
                      <option value="Michoacán">Michoacán</option>

                      <option value="Morelos">Morelos</option>
                      <option value="Nayarit">Nayarit</option>
                      <option value="Nuevo León">Nuevo León</option>
                      <option value="Oaxaca">Oaxaca</option>

                      <option value="Puebla">Puebla</option>
                      <option value="Querétaro">Querétaro</option>
                      <option value="Quintana Roo">Quintana Roo</option>
                      <option value="San Luis Potosí">San Luis Potosí</option>

                      <option value="Sinaloa">Sinaloa</option>
                      <option value="Sonora">Sonora</option>
                      <option value="Tabasco">Tabasco</option>
                      <option value="Tamaulipas">Tamaulipas</option>

                      <option value="Tlaxcala">Tlaxcala</option>
                      <option value="Veracruz">Veracruz</option>
                      <option value="Yucatán">Yucatán</option>
                      <option value="Zacatecas">Zacatecas</option>


                    </select>
                  </div>
                  <div class="col-md-4">
                    <h2>Municipios</h2>
                    <select id="Municipios" multiple>
                      <option selected="selected" value="Acaxochitlán">Acaxochitlán</option>
                      <option value="Actopan">Actopan</option>
                      <option value="Agua Blanca de Iturbide">Agua Blanca de Iturbide</option>
                      <option value="Ajacuba">Ajacuba</option>

                      <option value="Ajacuba">Ajacuba</option>
                      <option value="Alfajayucan">Alfajayucan</option>
                      <option value="Almoloya">Almoloya</option>
                      <option value="Apan">Apan</option>

                      <option value="Atitalaquia">Atitalaquia</option>
                      <option value="Atlapexco">Atlapexco</option>
                      <option value="Atotonilco de Tula">Atotonilco de Tula</option>
                      <option value="Atotonilco el Grande">Atotonilco el Grande</option>

                      <option value="Calnali">Calnali</option>
                      <option value="Cardonal">Cardonal</option>
                      <option value="Chapantongo">Chapantongo</option>
                      <option value="Chapulhuacán">Chapulhuacán</option>

                      <option value="Chilcuautla">Chilcuautla</option>
                      <option value="Cuautepec de Hinojosa">Cuautepec de Hinojosa</option>
                      <option value="El Arenal">El Arenal</option>
                      <option value="Eloxochitlán">Eloxochitlán</option>
                    </select>
                  </div>
                  <div class="col-md-4">
                    <h2>Macroregiones</h2>

                    <button onclick="buscar_por_filtro(1)" style="width: 49%;" type="button" class="block">Macroregion 1</button>
                    <button onclick="buscar_por_filtro(2)" style="width: 49%;" type="button" class="block">Macroregion 2</button>
                    <button onclick="buscar_por_filtro(3)" style="width: 49%;" type="button" class="block">Macroregion 3</button>
                    <button onclick="buscar_por_filtro(4)" style="width: 49%;" type="button" class="block">Macroregion 4</button>

                 
                  </div>
                  <input type="text" id="num_macroregion"> 

                </div>


                <p>Manten presionado el boton Ctrl en (windows) ò Command (Mac) Para seleccionar multiples elementos</p>
              

               <br>
              <button type="button" onclick="buscar_por_filtro(0)" style=" display: block; width: 100%; border: none; background-color: #4CAF50; padding: 14px 28px; font-size: 16px; cursor: pointer; text-align: center;">Buscar</button> 

              
              <br>
             
                <button class="accordion_filtros">Section 1</button>
                <div class="panel_fil">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>

                <button class="accordion_filtros">Section 2</button>
                <div class="panel_fil">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>

                <button class="accordion_filtros">Section 3</button>
                <div class="panel_fil">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>
              

          

              <script>

              var acc = document.getElementsByClassName("accordion_filtros");
              var i;

              for (i = 0; i < acc.length; i++) {
                acc[i].addEventListener("click", function() {
                  this.classList.toggle("active");
                  var panel_fil = this.nextElementSibling;
                  if (panel_fil.style.maxHeight) {
                    panel_fil.style.maxHeight = null;
                  } else {
                    panel_fil.style.maxHeight = panel_fil.scrollHeight + "px";
                  } 
                });
              }




              

            function buscar_por_filtro(num_macroregion)
              {
                 //alert(estado);
                // alert(municipio);
                var estado;
                var municipios="";
                var macroregion;

                estado=document.getElementById("Estados").value;
                //municipios=document.getElementById("Municipios").value;
                document.getElementById("num_macroregion").value= num_macroregion;
                macroregion=document.getElementById("num_macroregion").value;
                //------------------metemos todos los municipios seleccionados a la variable municipios
                var x=document.getElementById("Municipios");
                for (var i = 0; i < x.options.length; i++) {
                   if(x.options[i].selected ==true)
                    {
                      municipios = municipios+x.options[i].value+",";   
                    }
                }
                municipios = municipios.substring(0, municipios.length - 1);
                //--------------terminamos de obtener lo cabrones municipios selecionados

                if(macroregion==0)
                {
                  if(estado == "Hidalgo")// se puede hacer busqueda por municicpios
                 {


                alert(municipios);
                

                  $.ajax({
                    type: "POST",
                    datatype: 'json',
                    contentType: "application/json; charset=utf-8",
                    url: '/repositorio/wp-content/themes/EspecialesT2.1/inc/busqueda_municipios.php?estado='+estado+'& municipios='+municipios+'& macroregion='+macroregion,
                    //data: {estado:estado,municipios:municipios,macroregion:0},
                    success: function(msg)
                    {

                      //-----------console.log(msg)
                    //  alert(msg);
                      var msg = $.parseJSON(msg);  
                      alert(msg.nombre_indicador_asociado_def);
                      //----------------------------------------






                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                       alert( "Error Verifica tu informacion" );
                    }
                  });

      
                 }
                 else//Solo se puede hacer busqueda por Estados
                 {
                  alert(estado);
                 }


                }
                else
                {
                  alert("buscar por macroregion");
                }
                 






              }
              </script>







            

  <?php endwhile; ?>

</div>
</div>
</div>
</section>

<?php get_footer(); ?>


