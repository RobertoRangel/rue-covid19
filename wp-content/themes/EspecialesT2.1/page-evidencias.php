
<?php get_header(); 
/*
*  Template 
: contacto
*/
$pagina=$_GET['page_id'];
?>
<?php

?>
<style type="text/css">
  /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 400px;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 80%;
        margin: 0;
        padding: 0;
      }
      #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
      }

      #infowindow-content .title {
        font-weight: bold;
      }

      #infowindow-content {
        display: none;
      }

      #map #infowindow-content {
        display: inline;
      }

      .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
      }

      #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
      }

      .pac-controls {
        display: inline-block;
        padding: 5px 11px;
      }

      .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
      }
      #target {
        width: 345px;
      }
</style>

<section id="main-content">
  <div>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
         
          <style>/*STYLE LOADER SPINER*/
            /* Center the loader */
            #loader {
              position: absolute;
              left: 50%;
              top: 50%;
              z-index: 1;
              width: 150px;
              height: 150px;
              margin: -75px 0 0 -75px;
              border: 16px solid #f3f3f3;
              border-radius: 50%;
              border-top: 16px solid #092432;
              width: 120px;
              height: 120px;
              -webkit-animation: spin 2s linear infinite;
              animation: spin 2s linear infinite;
            }

            @-webkit-keyframes spin {
              0% { -webkit-transform: rotate(0deg); }
              100% { -webkit-transform: rotate(360deg); }
            }

            @keyframes spin {
              0% { transform: rotate(0deg); }
              100% { transform: rotate(360deg); }
            }

            /* Add animation to "page content" */
            .animate-bottom {
              position: relative;
              -webkit-animation-name: animatebottom;
              -webkit-animation-duration: 1s;
              animation-name: animatebottom;
              animation-duration: 1s
            }

            @-webkit-keyframes animatebottom {
              from { bottom:-100px; opacity:0 } 
              to { bottom:0px; opacity:1 }
            }

            @keyframes animatebottom { 
              from{ bottom:-100px; opacity:0 } 
              to{ bottom:0; opacity:1 }
            }

            #myDiv {
              display: none;
              text-align: center;
            }


                  /* Always set the map height explicitly to define the size of the div
             * element that contains the map. */
            #map {
              height: 100%;
            }
            /* Optional: Makes the sample page fill the window. */
            /*
            html, body {
              height: 100%;
              margin: 0;
              padding: 0;
            }
            */

          </style>


          <?php while(have_posts()): the_post(); ?>

             <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
              <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
              <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!--
                
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

              
-->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<!--<src="//ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>-->
  
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>

              <body onload="myFunction()" style="margin:0;">



              <!-- USUARIO ACTIVO-->
              <div>
                  <table>
                    <col width=80%>
                    <col width=20%>
                      <tbody>
                        <td>
                          
                        </td>
                        <td>

                        <?php /* Nombre del usuario activo*/ 
                          $all_users = get_users();
                          $user_name;
                          $user_name=$user->display_name;

                              echo '<h6>Bienvenido: </6>' . $current_user->user_login . '<br />';
                              echo '<h6>Correo:</6> ' . $current_user->user_email . '<br />';
                              $user_name=$current_user->user_login;
                        ?>
                         </td>
                         <td> 
                              <?php
                                  $user = wp_get_current_user();
                                if ( $user ) :
                                    ?>
                                    <img src="<?php echo esc_url( get_avatar_url( $user->ID ) ); ?>" />
                                <?php endif; 
                              ?>
                         </td> 
                      </tbody>
                  </table>
              </div>  
              <!-- FIN USUARIO ACTIVO-->
              <div id="loader"></div>
               <div class="content">
                <br>
                <br>
                <h2> <label  onclick="ir_menu_indicadores()" style="text-align: center color: #4CAF50; font-size: 50px; font-family: Graphik-Bold" >INFORME</label> <font font-family: Graphik-Bold color="green" size="130">DE RESULTADOS</font></h2>

                <br>
                <br>


               

              </div>

               <div id="menu_registro_indicador_dependencias" style="display: none;" >
                  <ul class="ca-menu">
                      <li onmousemove="over_menu(1)" onclick="abrir_estrategico()" >
                          <a href="#">
                              <span class="ca-icon">F</span>

                              <div class="ca-content"  >
                                  <h2 class="ca-main">Indicador Estrategico</h2>
                                  
                              </div>
                              <canvas height="210" width="210" id="counter_estrategico" />
                          </a>

                      </li>
                      <li onmousemove="over_menu(2)" onclick="abrir_acciones_sin_indicador()" style="display:none;>
                          <a href="#">
                              <span class="ca-icon" id="heart">F</span>
                              <div class="ca-content">
                                  <h2 class="ca-main">Inveriones y Resultados</h2>
                                  
                              </div>
                          </a>
                      </li>
                      <li onmousemove="over_menu(3)" onclick="abrir_avance_programatico()" style="display:none;>
                          <a href="#">
                              <span class="ca-icon">F</span>
                              <div class="ca-content">
                                  <h2 class="ca-main">Avance Programatico</h2>
                                  
                              </div>
                              <canvas height="210" width="210" id="counter_avance_prog" />
                          </a>
                      </li>
                      <li onmousemove="over_menu(4)" onclick="abrir_acciones_concurrentes()" style="display:none;>
                          
                          <a href="#">
                              <span class="ca-icon">F</span>
                              <div class="ca-content">
                                  <h2 class="ca-main">Acciones Concurrentes</h2>
                              </div>
                          </a>                            
                      </li>
                      <li onmousemove="over_menu(5)" onclick="abrir_tactico()"  >
                          <a href="#">
                              <span class="ca-icon">F</span>
                              <div class="ca-content" >
                                  <h2 class="ca-main">Indicador Tactico</h2>
                              </div>
                              <canvas height="210" width="210" id="counter"/>
                          </a>
                      </li>

                  </ul>



             

              
                </div>

                <div id="menu_registro_indicador_orgnismo"  >

                  <ul class="ca-menu">
                    <li onmousemove="over_menu(6)" onclick="abrir_pid()"  >
                          <a href="#">
                              <span class="ca-icon">F</span>
                              <div class="ca-content" >
                                  <h2 class="ca-main" style="font-size: 15px;">Programa Institucional de Desarrollo</h2>
                              </div>
                              <canvas height="210" width="210" id="counter_pid"/>
                          </a>
                      </li>

                  </ul>

                </div>

                <div id="menu_covid"  >

                  <ul class="ca-menu">
                    <li onmousemove="over_menu(7)" onclick="abrir_covid()"  >
                          <a href="#">
                              <span class="ca-icon">F</span>
                              <div class="ca-content" >
                                  <h2 class="ca-main" style="font-size: 15px;">Acciones COVID-19</h2>
                              </div>
                              <canvas height="210" width="210" id="counter_covid"/>
                          </a>
                      </li>

                  </ul>

                </div>

                   




  <!-- The Modal -->
  <div class="modal" id="Modal_entidades">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Ubica el lugar geograficamente</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">

         <!-- <input id="pac-input" class="controls" type="text" placeholder="Search Box">-->
          <div class="location-map" id="location-map">
            <div style="width: 600px; height: 400px;" id="map_canvas"></div>
            </div>
         
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal" onclick="get_position()">Aceptar</button> 


        </div>
        
      </div>
    </div>
  </div>

  





             
              <div style="display:none;" id="myDiv" class="animate-bottom">

                  <div class="principal contenedor contacto">
                      <main class="contenido-paginas ">

                        

                          <!--  Mensajes emergentes-->
                          <div id="dialog-form" title="Municipios">
                            <p class="validateTips">Selecciona los Municipios</p>
                            <form  id="form_cheks_municipios">
                              <fieldset>
                                <p id="check_list_municipios"></p>

                                <script>
                                  var municipios_arr = ["Acatlán",
                                     "Acaxochitlán",                //1
                                     "Actopan",                     //2
                                     "Agua Blanca de Iturbide",     //3
                                     "Ajacuba",                     //4
                                     "Alfajayucan",                 //5
                                     "Almoloya",                    //6
                                     "Apan",                        //7
                                     "Atitalaquia",                 //8
                                     "Atlapexco",                   //9
                                     "Atotonilco de Tula",          //10
                                     "Atotonilco el Grande",        //11
                                     "Calnali",                     //12
                                     "Cardonal",                    //13
                                     "Chapantongo",                 //14 
                                     "Chapulhuacán",                //15
                                     "Chilcuautla",                 //16
                                     "Cuautepec de Hinojosa",       //17
                                     "El Arenal",                   //18
                                     "Eloxochitlán",                //19
                                     "Emiliano Zapata",             //20
                                     "Epazoyucan",                  //21
                                     "Francisco I. Madero",         //22
                                     "Huasca de Ocampo",            //23
                                     "Huautla",                     //24
                                     "Huazalingo",                  //25
                                     "Huehuetla",                   //26
                                     "Huejutla de Reyes",           //27
                                     "Huichapan",                   //28
                                     "Ixmiquilpan",                 //29
                                     "Jacala de Ledezma",           //30
                                     "Jaltocán",
                                     "Juárez Hidalgo",
                                     "La Misión",
                                     "Lolotla",
                                     "Metepec",
                                     "Metztitlán",
                                     "Mineral de la Reforma",
                                     "Mineral del Chico",
                                     "Mineral del Monte",
                                     "Mixquiahuala de Juárez",
                                     "Molango de Escamilla",
                                     "Nicolás Flores",
                                     "Nopala de Villagrán",
                                     "Omitlán de Juárez",
                                     "Pachuca de Soto",
                                     "Pacula",
                                     "Pisaflores",
                                     "Progreso de Obregón",
                                     "San Agustín Metzquititlán",
                                     "San Agustín Tlaxiaca",
                                     "San Bartolo Tutotepec",
                                     "San Felipe Orizatlán",
                                     "San Salvador",
                                     "Santiago de Anaya",
                                     "Santiago Tulantepec de Lugo Guerre",
                                     "Singuilucan",
                                     "Tasquillo",
                                     "Tecozautla",
                                     "Tenango de Doria",
                                     "Tepeapulco",
                                     "Tepehuacán de Guerrero",
                                     "Tepeji del Río de Ocampo",
                                     "Tepetitlán",
                                     "Tetepango",
                                     "Tezontepec de Aldama",
                                     "Tianguistengo",
                                     "Tizayuca",
                                     "Tlahuelilpan" ,
                                     "Tlahuiltepa",
                                     "Tlanalapa",
                                     "Tlanchinol",
                                     "Tlaxcoapan",
                                     "Tolcayuca",
                                     "Tula de Allende",
                                     "Tulancingo de Bravo",
                                     "Villa de Tezontepec",
                                     "Xochiatipan",
                                     "Xochicoatlán",
                                     "Yahualica",
                                     "Zacualtipán de angeles",
                                     "Zapotlán de Juárez",
                                     "Zempoala",
                                     "Zimapán"];
                                  var text = "";
                                  var i;
                                  text+="<ul>"
                                  for (i = 0; i < municipios_arr.length; i++) 
                                  {
                                     text +='<div>'+  '<label class="checkbox-inline "> '+'<input type="checkbox" onchange="muni_selec('+'municipios_arr['+i+']'+','+i+')"   class="check_muni_tac" id="check_evi_'+municipios_arr[i]+'"  name="'+municipios_arr[i]+'">'+municipios_arr[i]+'</label>'   +'</div>';


                                  }

                                  text+="</ul>"
                                  document.getElementById("check_list_municipios").innerHTML = text;
                               
                                </script>
                                <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
                              </fieldset>
                            </form>
                          </div>


                          <!-- Mensajes Entidades -->
                          <div id="dialog-form-entidades" title="Entidades">
                            <p class="validateTips">Selecciona las Entidades</p>
                            <form  id="form_cheks_entidades">
                              <fieldset>
                                <p id="check_list_entidades"></p>

                                <script>

                                  var my_data = $("#dialog-form-entidades" ).data('indi');
                                 // alert(my_data);

                                  var entidades_arr = ["Aguascalientes",
                                      "Baja California",
                                      "Baja California Sur",
                                      "Campeche",
                                      "Chiapas",
                                      "Chihuahua",
                                      "Ciudad de México",
                                      "Coahuila",
                                      "Colima",
                                      "Durango",
                                      "Estado de México",
                                      "Guanajuato",
                                      "Guerrero",
                                      "Hidalgo",
                                      "Jalisco",
                                      "Michoacán",
                                      "Morelos",
                                      "Nayarit",
                                      "Nuevo León",
                                      "Oaxaca",
                                      "Puebla",
                                      "Querétaro",
                                      "Quintana Roo",
                                      "San Luis Potosí",
                                      "Sinaloa",
                                      "Sonora",
                                      "Tabasco",
                                      "Tamaulipas",
                                      "Tlaxcala",
                                      "Veracruz",
                                      "Yucatán",
                                      "Zacatecas"];
                                  var text = "";
                                  var i;
                                  text+="<ul>"
                                  for (i = 0; i < entidades_arr.length; i++) 
                                  {
                                    //alert(i);
                                    //alert(entidades_arr.length);
                                     text +='<label class="checkbox-inline ">'+'<input type="checkbox" onchange="entidad_selec('+'entidades_arr['+i+']'+','+i+')"  class="check_enti_tac" id="check_evi_'+entidades_arr[i]+'"  name="'+entidades_arr[i]+'">'+entidades_arr[i]+'</label>';
                                     //alert(text);
                                  }
                                  text+="</ul>"

                                  //alert(text);
                                  document.getElementById("check_list_entidades").innerHTML = text;
                                  

                                </script>
                                <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
                              </fieldset>
                            </form>
                          </div>
                          <!-- Localidades opcions de su bir archivo
                          -->

                            <!-- Modal Entidades--------> 
                              <!-- Modal -->


                          <div id="dialog-message" title="Plan de trabajo">
                            <p>
                              <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
                            </p>
              
                                <p><label for="upload_doc">Descargar el ejemplo de como organizar tu informacion </label></p>
                                <b>Descargar</b>
                                <div id="upload_doc" name="upload_doc">  </div>
                          </div>

                          <!-- show dialog resultados-->

                          <div id="dialog-message-adjuntar-localidades" title="Adjuntar archivo de localidades">
                            <p>
                              <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
                             <b>Especifiquen los lugares donde se realizaron las acciones</b>
                            </p>
                            
                            <div id="doc_res1">
                                <div id="upload_doc_resultados_1" name="upload_doc_resultados_1">  </div>
                            </div>

                            <div id="doc_res2">
                                <div id="upload_doc_resultados_2" name="upload_doc_resultados_2">  </div>
                            </div>

                            <div id="doc_res3">
                                <div id="upload_doc_resultados_3" name="upload_doc_resultados_3">  </div>
                            </div>

                            <div id="doc_res4">
                                <div id="upload_doc_resultados_4" name="upload_doc_resultados_4">  </div>
                            </div>

                            <div id="doc_res5">
                                <div id="upload_doc_resultados_5" name="upload_doc_resultados_5">  </div>
                            </div>

                            <div id="doc_res6">
                                <div id="upload_doc_resultados_6" name="upload_doc_resultados_6">  </div>
                            </div>

                            <div id="doc_res7">
                                <div id="upload_doc_resultados_7" name="upload_doc_resultados_7">  </div>
                            </div>

                            <div id="doc_res8">
                                <div id="upload_doc_resultados_8" name="upload_doc_resultados_8">  </div>
                            </div>


                             <div id="est_doc_res1">
                                <div id="est_upload_doc_resultados_1" name="est_upload_doc_resultados_1">  </div>
                            </div>

                            <div id="est_doc_res2">
                                <div id="est_upload_doc_resultados_2" name="est_upload_doc_resultados_2">  </div>
                            </div>

                            <div id="est_doc_res3">
                                <div id="est_upload_doc_resultados_3" name="est_upload_doc_resultados_3">  </div>
                            </div>



                            <p><label >Descargar el modelo de como organizar la informacion</label></p>
                            <a href="/repositorio/upload_files_plugin/download_file_demo.php">Descargar</a>

                          </div>

                          <!-- Lugar donden se realizaron las acciones -->

                          <div id="dialog-message-lugar" title="Lugar donde se realizarion la acciones">
                            <div class="btn-group">
                              <button  type="button" onclick="open_estados()" class="btn btn-primary">Estatal</button>
                              <button type="button" onclick="open_municipios()" class="btn btn-primary">Municipales</button>
                              <button type="button" onclick="open_localidades()" class="btn btn-primary">Localidades</button>
                            </div>
                                                
                          </div>



                        <!-- Fin Mensajes emergentes-->




                          <!-- INDICADORES TACTICOS-->
                              <!-- TABS-->



                            <div id="tacticos">

                              <div class="tab_menu">
                                <button id="btn_tac_reg" disabled="true" class="tablinks" onclick="open_tabs(event, 'EVIDENCIA')"  >DEFINIR EVIDENCIA</button>
                                <button id="btn_tac_edit" disabled="true" class="tablinks" onclick="open_tabs(event, 'ESTATUS')">EDICION</button>
                                <button class="tablinks" disabled="true" onclick="open_tabs(event, 'REPORTES_TACTICOS')" id="btn_tac_repdf"  >REPORTE </button>
                              </div>
                              <!-- PESTANAS-->
                              <br>
                              <div id="intro_idicador_tactico" class="container" >
                                <p  ><font text-align: center; font-family: Graphik-Bold  color="green" size="5">INDICADOR TACTICO</font></p>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>

                              </div>
                              

                              <div id="EVIDENCIA" class="tabcontent">
                                <div class="container">
                                      <form  method="post" id="Form_definir_evidencia" name="Form_definir_evidencia" >
                                     
                                        <br>
                                        <p><font text-align: center; font-family: Graphik-Bold  color="green" size="5">INDICADOR TACTICO</font></p>

                                         <div class="tab">ANTES DE COMENZAR:
                                          <br>
                                          <br>
                                          <br>
                                            <li>Favor de llenar todos los campos sin abreviaturas</li>
                                            <li>Usar letras mayúsculas y minúsculas</li>
                                            <li>Se recomienda tener  a la mano la ficha del indicador, informe de gobierno y evidencias</li>
                                          <br>
                                          <br>
                                         </div>


                                          <div class="tab">INDICADOR:
                                            <br><br>

                                              <p><label  id="error_nombre_indicador_asociado_def"  for="nombre_indicador_asociado_def">Nombre del indicador asociado:</label>
                                              <input id="nombre_indicador_asociado_def" name="nombre_indicador_asociado_def"  title="type &quot;a&quot;"  placeholder="Nombre del indicador asociado: "   >
                                              </p>


                                              <div id="registro_terminado">
                                                 <p><h2 style="color:green;">Has terminado de registrar tus indicadores</h2>
                                              </div>

                                              <div id="indicadore_seleccionado_autollenado_tacticos">

                                                  <br>
                                                  <p><h2><label style="color:blue; font-size: 18px; ">En el siguiente apartado se registra la información que se mostrara en el “3er informe de gobierno.“
                                                        Por lo cual es necesario revisar la información y el discurso que se provee
                                                  </label></h2></p>


                                                  <br>
                                                  <p><label  id="error_discurso_importancia_ind" for="discurso_importancia_ind">Linea discursiva:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                                  <textarea id="discurso_importancia_ind" maxlength="350" name="discurso_importancia_ind" placeholder="Linea discursiva:"></textarea>
                                                  </p>

                                                     <!--                                          <br>
                                                 <label for="clase_indicador" >Categoria del indicador</label>
                                                  <div class="select">
                                                    <select name="clase_indicador" id="clase_indicador">
                                                      <option value="Indicador Estrella">Indicador Estrella</option>
                                                      <option value="Indicador tactico convencional">Indicador tactico convencional</option>
                                                      <option value="indicador publico(informe-tablero)">indicador publico(informe-tablero)</option>
                                                    </select>
                                                  </div>
                                                -->

                                                  <br>
                                                  <br>


                                                  <br>
                                                  <p><label <>LOGROS, PREMIO Y/O RECONOCIMIENTO</label></p>
                                                
                                                  <p><label for="logros_en_indicador" >¿Hubo logros en este indicador:?</label></p>
                                                    <div class="select">
                                                      <select name="logros_en_indicador" id="logros_en_indicador">
                                                        <option value="No">No</option>
                                                        <option value="Si">Si</option>
                                                      </select>
                                                    </div>
                                                    <br>
                                                  <div id="bullet_Del_logro">
                                                    <p><label id="error_id_bullet_Del_logro"  for="id_bullet_Del_logro">Escribir el bullet del logro:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                                                    <textarea id="id_bullet_Del_logro" name="id_bullet_Del_logro" placeholder="Escribir el bullet del logro:"   ></textarea>
                                                  </p>
                                                  </div>
                                                  <br>

                                                  <!--
                                                    <p><label for="premios_recono" >¿Hubo premios o reconocimientos?</label></p>
                                                    <div class="select">
                                                      <select name="premios_recono" id="premios_recono">
                                                        <option value="No">No</option>
                                                        <option value="Si">Si</option>
                                                      </select>
                                                    </div>
                                                    <br>
                                                  <div id="premios_reconocimientos">
                                                    <p><label id="error_reconocimiento"  for="reconocimiento">Describelo:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                                                    <textarea id="reconocimiento" name="reconocimiento" placeholder="Escribir el bullet del premio o reconocimiento"   ></textarea>
                                                  </p>

                                                -->
                                                  </div>

                                              </div>
                                          </div>
                                          
                                          <div class="tab">OBRAS Y ACCIONES:
                                                  <br><br>
                                                
                                                  <p><label id="error_resultado_1"  for="resultado_1">Obras y acciones 1:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                                  <textarea class="textbox_corto" maxlength="350" id="resultado_1" name="resultado_1" placeholder="Obras y acciones 1:"></textarea>
                                                  
                                                  <button id="btn_def_lugar_1"  onclick="open_lugares(1,0,0)" type="button" >Definir lugar 1</button></p>
                                                  <label id="datos_cargados_lug_1" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                                  <input id="res1_lugares" name="res1_lugares" hidden="true" >
                                                  <input id="res1_lugares_gps" name="res1_lugares_gps" hidden="true">

                                                  <p><label id="error_resultado_2" id="label_resultado_2"  for="resultado_2">Obras y acciones 2:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                                  <textarea class="textbox_corto" maxlength="350" id="resultado_2" name="resultado_2" placeholder="Obras y acciones 2:"></textarea>
                                                  <button  id="btn_def_lugar_2" class="btn_def_lugar" onclick="open_lugares(2,0,0)" type="button" >Definir lugar 2</button></p>
                                                  <label id="datos_cargados_lug_2" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                                  <input id="res2_lugares" name="res2_lugares" hidden="true>
                                                  <input id="res2_lugares_gps" name="res2_lugares_gps" hidden="true">

                                                  <p><label id="error_resultado_3"  for="resultado_3">Obras y acciones 3:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                                  <textarea class="textbox_corto" maxlength="350" id="resultado_3" name="resultado_3" placeholder="Obras y acciones 3:"></textarea>
                                                  <button class="btn_def_lugar" id="btn_def_lugar_3" onclick="open_lugares(3,0,0)" type="button" >Definir lugar 3</button></p>
                                                  <label id="datos_cargados_lug_3" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                                  <input id="res3_lugares" name="res3_lugares" hidden="true">
                                                  <input id="res3_lugares_gps" name="res3_lugares_gps" hidden="true">

                                                  <p><label id="error_resultado_4" id="label_resultado_4"  for="resultado_4">Obras y acciones 4:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                                  <textarea class="textbox_corto" maxlength="350" id="resultado_4" name="resultado_4" placeholder="Obras y acciones 4:"></textarea>
                                                  <button id="btn_def_lugar_4" class="btn_def_lugar" onclick="open_lugares(4,0,0)"  type="button" >Definir lugar 4</button></p>
                                                  <label id="datos_cargados_lug_4" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                                  <input id="res4_lugares" name="res4_lugares" hidden="true" >
                                                  <input id="res4_lugares_gps" name="res4_lugares_gps" hidden="true">

                                                  <p><label id="error_resultado_5"  for="resultado_5">Obras y acciones 5:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                                  <textarea class="textbox_corto" maxlength="350" id="resultado_5" name="resultado_5" placeholder="Obras y acciones 5:"></textarea>
                                                  <button id="btn_def_lugar_5" class="btn_def_lugar" onclick="open_lugares(5,0,0)"  type="button" >Definir lugar 5</button></p>
                                                  <label id="datos_cargados_lug_5" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                                  <input id="res5_lugares" name="res5_lugares" hidden="true" >
                                                  <input id="res5_lugares_gps" name="res5_lugares_gps" hidden="true">


                                                  <p><label id="error_resultado_6"  for="resultado_6">Obras y acciones 6:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                                  <textarea class="textbox_corto" maxlength="360" id="resultado_6" name="resultado_6" placeholder="Obras y acciones 6:"></textarea>
                                                  <button id="btn_def_lugar_6" class="btn_def_lugar" onclick="open_lugares(6,0,0)"  type="button" >Definir lugar 6</button></p>
                                                  <label id="datos_cargados_lug_6" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                                  <input id="res6_lugares" name="res6_lugares" hidden="true" >
                                                  <input id="res6_lugares_gps" name="res6_lugares_gps" hidden="true">

                                                  <p><label id="error_resultado_7"  for="resultado_7">Obras y acciones 7:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                                  <textarea class="textbox_corto" maxlength="370" id="resultado_7" name="resultado_7" placeholder="Obras y acciones 7:"></textarea>
                                                  <button id="btn_def_lugar_7" class="btn_def_lugar" onclick="open_lugares(7,0,0)"  type="button" >Definir lugar 7</button></p>
                                                  <label id="datos_cargados_lug_7" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                                  <input id="res7_lugares" name="res7_lugares" hidden="true" >
                                                  <input id="res7_lugares_gps" name="res7_lugares_gps" hidden="true">


                                                  <p><label id="error_resultado_8"  for="resultado_8">Obras y acciones 8:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                                  <textarea class="textbox_corto" maxlength="380" id="resultado_8" name="resultado_8" placeholder="Obras y acciones 8:"></textarea>
                                                  <button id="btn_def_lugar_8" class="btn_def_lugar" onclick="open_lugares(8,0,0)"  type="button" >Definir lugar 8</button></p>
                                                  <label id="datos_cargados_lug_8" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                                  <input id="res8_lugares" name="res8_lugares" hidden="true" >
                                                  <input id="res8_lugares_gps" name="res8_lugares_gps" hidden="true">


                                                  

                                                  <br><br>
                                                  <p><label <>LO QUE SIGUE...</label></p>
                                                  
                                                  <p><label  id="error_lo_que_sigue_1" for="lo_que_sigue_1">1.- <strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                                                  <textarea id="lo_que_sigue_1" maxlength="450" name="lo_que_sigue_1" placeholder=""></textarea>

                                                  <p><label  id="error_lo_que_sigue_2"  for="lo_que_sigue_2">2.- <strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                                                  <textarea id="lo_que_sigue_2" maxlength="450" name="lo_que_sigue_2" placeholder=""></textarea>

                                                  <p><label  id="error_lo_que_sigue_3"  for="lo_que_sigue_3">3.- <strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong> </label>
                                                  <textarea id="lo_que_sigue_3" maxlength="450" name="lo_que_sigue_3" placeholder=""></textarea>
                                          </div>

                                         <div class="tab">IMPORTANCIA:
                                           <div id="indicadore_seleccionado_autollenado">
                                            <br>

                                                  <p><label id="error_descripcion_indicador_def" for="descripcion_indicador_dif">Descripción del indicador, ¿qué mide el indicador?<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label> <textarea readonly id="descripcion_indicador_dif" maxlength="450" name="descripcion_indicador_dif" placeholder=" Descripción del indicador, ¿qué mide el indicador?" ></textarea></p>

                                                  <br>
                                                  <p><label id="error_unidad_observacion_indicador_def" for="unidad_observacion_indicador_def">¿Cuál es la unidad de observación del indicador? (Ej. acciones, población, etc.):</label> 

                                                  <input readonly id="unidad_observacion_indicador_def" name="unidad_observacion_indicador_def" placeholder="¿Cuál es la unidad de observación del indicador? (Ej. acciones, población, etc.)"   ></p>

                                                  <br>
                                                  <p><label id="error_descripcion_observacion_ficha_tecnica" for="descripcion_obj_secto">Descripcion del objetivo sectorial<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label><textarea readonly id="descripcion_obj_secto" maxlength="450" name="descripcion_obj_secto" placeholder=" Descripcion del objetivo sectorial" ></textarea></p>  

                                                  <br>

                                                  <label for="tipo_indicador_def" >¿Qué tipo de indicador es?</label>
                                                  <div class="select">
                                                    <p><input disabled name="tipo_indicador_def" id="tipo_indicador_def"></p>
                                                      

                                                  </div>
                                                  <br>
                                                  <div id="otro_tipo_indicador">
                                                    <p><label for="otro_tipo_indicador_def">¿Cual?:</label> <input  name="otro_tipo_indicador_def" id="otro_tipo_indicador_def" placeholder=""   ></p>
                                                  </div>
                                                  <br>
                                           </div>
                                         </div>

                                         
                                          <div class="tab">EVIDENCIAS:
                                              <!--Robert-->
                                              <br>
                                              <label for="tendencia_esperada_def">¿Qué tendencia se espera?</label>

                                              <p><input id="tendencia_esperada_def" name="tendencia_esperada_def"></p>

                                                <!--
                                              <div class="select">



                                                <select  name="tendencia_esperada_def" id="tendencia_esperada_def">
                                                  <option id="tend_acs" value="Ascendente">Ascendente</option>
                                                  <option id="tend_des" value="Descendente">Descendente</option>
                                                  <option id="tend_est" value="Estable">Estable</option>
                                                </select>
                                              </div>

                                             -->
                                             

                                              <br>

                                              <p><label id="error_linea_base_absolutos_def" for="linea_base_absolutos_def">Linea base</label>
                                              <input id="linea_base_absolutos_def" minlength="0" maxlength="15" name="linea_base_absolutos_def" placeholder="Linea base"   ></p>


                                              <br>
                                              <p><label for="resultado_18"  id="error_resultado_18">Resultado reportado en el segundo informe 2018 </label>
                                              <input readonly  minlength="0" maxlength="15" name="resultado_18" placeholder="" id="resultado_18"></p> 

                                              <br>
                                              <p><label for="avance_2018"  id="error_avance_2018">Resultado reportado para el tercer informe 2019</label>
                                              <input  minlength="0" maxlength="15" name="resultado_19" placeholder="" id="resultado_19"></p>

                                               
                                              <p><label for="meta_2019"  id="error_meta_2019">Meta planteada 2019</label>
                                              <input   name="meta_2019"  id="meta_2019"></p>


                                              <br>
                                              <p><label for="avance_meta_planeada_def" id="error_avance_meta_planeada_def">Avance sobre la meta 2019<strong class="tamanio_caracteres" id="tamanio_caracteres"> Actualiza el valor</strong></label>
                                              <input  minlength="0" maxlength="15" name="avance_meta_planeada_def" onfocus="calcuar_avance(this)" placeholder="" id="avance_meta_planeada_def"></input></p> 
                                                           
                                          </div>

                                          <div class="tab">TIPO DE EVIDENCIA :
                                            
                                            <br>
                                           
                                              <p><label for="check_list_tipo_evidencia_def"> ¿Qué tipo de evidencia se presenta?  Seleccione los necesarios<strong class="tamanio_caracteres" id="tamanio_caracteres"> Puedes seleccionar mas de uno</strong></label></p>
                                                <div id="check_list_tipo_evidencia_def"></div>

                                                <script>
                                                var t_evidencia = 
                                                  ["Acta, Convenios o minuta",
                                                     "Diagnostico",
                                                     "Padrones o bases de datos",
                                                     "Publicación en físico o electrónico",
                                                     "Reporte",
                                                     "Otro"];
                                                  var text = "";
                                                  var i;
                                                  text+="<ul>"
                                                  for (i = 0; i < t_evidencia.length; i++) 
                                                  {

                                                    text += '<label class="checkbox-inline ">'+'<input type="checkbox"   class="check_type_evi" id="check_evi_'+t_evidencia[i]+'"  name="'+t_evidencia[i]+'">'+t_evidencia[i]+'</label>';
                                                  }

                                                  text+="</ul>"
                                                  document.getElementById("check_list_tipo_evidencia_def").innerHTML = text;
                                                </script>


                                                 <div id="tipo_evidencia_otro">
                                                    <p><label for="tipo_evidencia_otro_def">Definelo </label>
                                                    <input  name="tipo_evidencia_otro_def" id="tipo_evidencia_otro_def" type="text" value="0"></p>

                                                 </div>

                                                <input  name="tipo_evidencia_def_list" id="tipo_evidencia_def_list" type="hidden" >


                                                <script >

                                                    

                                                </script>

                                                <br>
                                                <p><label id="error_descripcion_documento_vinculado_indicador_asociado_def" for="descripcion_documento_vinculado_indicador_def">¿Qué contiene la evidencia? Describir qué es el documento y por qué es una evidencia vinculada al indicador.<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                                                <textarea id="descripcion_documento_vinculado_indicador_def" maxlength="450" name="descripcion_documento_vinculado_indicador_def" placeholder="¿Qué contiene la evidencia? Describir qué es el documento y por qué es una evidencia vinculada al indicador."   ></textarea></p> 

                                                <br>
                                                <label for="tipo_evidencia_fecha_def"> La fecha de la evidencia es:</label>
                                                    <div class="select">
                                                      <select name="tipo_evidencia_fecha_def" id="tipo_evidencia_fecha_def" >
                                                        <option value="Corte 30 junio">Corte 30 junio</option>
                                                        <option value="Corte 30 Julio">Corte 30 Julio</option>
                                                        
                                                      </select>
                                                    </div>


                                                    <br>
                                                    <label for="existe_expediente_doc_def">Existe expediente documental</label>
                                                    <div class="select">
                                                      <select name="existe_expediente_doc_def" id="existe_expediente_doc_def">
                                                        <option value="No" >No</option>
                                                        <option value="Si">Si</option>
                                                        
                                                      </select>
                                                    </div>


                                                   <div id="parte1">

                                                    
                                                      <p><label id="error_serie_evidencia_def" for="serie_evidencia_def">Serie de la Evidencia</label><input id="serie_evidencia_def" name="serie_evidencia_def" placeholder="Serie de la Evidencia" ></p>

                                                     
                                                      <p><label id="error_seccion_def" for="seccion_def">Sección(opcional)</label><input  name="seccion_def" id="seccion_def" placeholder="sección (opcional)" ></p>

                                                      
                                                      <p><label id="error_guia_invetario_expediente_def" for="guia_invetario_expediente_def">Guía/Inventario en el que se incluye el expediente</label><input  name="guia_invetario_expediente_def" id="guia_invetario_expediente_def" placeholder="Guía/Inventario en el que se incluye el expediente" ></p>

                                                   
                                                   </div>



                                                  <div id="parte2">
                                                  <!--Robert-->
                                                  <p><label for="medios_verificacion_evidencia" id="error_medios_comunicacion">Define cuál es la ubicación electrónica o física de la evidencia o cómo es posible rastrearla<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label><textarea id="medios_verificacion_evidencia" maxlength="350" name="medios_verificacion_evidencia" placeholder="Define cuál es la ubicación electrónica o física de la evidencia o cómo es posible rastrearla" ></textarea></p> 
                                                  </div>

                                                  <br>
                                                 <p><label id="error_comentarios" for="comentarios_def">Comentarios <strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label><textarea id="comentarios_def" maxlength="450" name="comentarios_def" placeholder="Comentarios" ></textarea></p> 

                                                  <br>


                                                   <div>
                                                      <table>
                                                        <col width=50%>
                                                        <col width=50%>
                                                          <tbody>
                                                            <td style="text-align:center;" >
                                                              <h3>Adjuntar evidencias</h3>
                                                              <br>
                                                              <p><label for="upload_adjuntar_evidencias">Puedes seleccionar multiples archivos</label></p>
                                                              <div style="text-align:center;" id="upload_adjuntar_evidencias" name="upload_adjuntar_evidencias">  </div>
                                                            </td>


                                                            <td style="text-align:center;">
                                                              <div id="area_adj_plan_trabajo">
                                                                <h3>Adjuntar Plan de trabajo</h3>
                                                                <br>
                                                                <p><label for="plan_trabajo_def">Plan de trabajo</label></p>
                                                                <div style="text-align:center;" id="upload_plan_trabajo" name="upload_plan_trabajo">  </div>
                                                                   
                                                                 
                                                              </div>
                                                            </td>
                                                             
                                                          </tbody>
                                                      </table>
                                                  </div>  

                                                  <br>
                                                  <p><input  name="nombre_dependencia_def" id="des_geo_municipios" type="hidden" value="<?php echo ($user_name); ?>"></p>

                                                  <input type="hidden" name="oculto" id="oculto" value="1">

                                                  


                                          </div>

                                           <div class="tab">

                                            <label style="text-align: center color: #4CAF50; font-size: 17px; font-family: Graphik-Bold" >Informacion lista</label>

                                            <h2><font font-family: Graphik-Bold color="green" size="6">ENVIAR INFORMACION</font></h2>
                                            <input type="hidden" name="cve_unica" id="cve_unica" >
                                                  <input type="hidden" name="cime"      id="cime" >
                                                  <input type="hidden" name="tablero"   id="tablero" >
                                                  <input type="hidden" name="informe2019" id="informe2019" >
                                                  <input type="hidden" name="eje_ped"   id="eje_ped" >
                                                  <input type="hidden" name="ob_est"    id="ob_est" >
                                                  <input type="hidden" name="ob_gen"    id="ob_gen" >
                                                  <input type="hidden" name="ods"       id="ods" >
                                                  <input type="hidden" name="ob_secto"  id="ob_secto" >
                                                  <input type="hidden" name="periodisidad" id="periodisidad" >
                                                  <input type="hidden" name="fuente"    id="fuente" >
                                                  <input type="hidden" name="ref_ad"    id="ref_ad" >


                                           </div>


                                        <div style="overflow:auto;">
                                          <div style="float:right;">
                                            <button type="button" id="prevBtn" onclick="nextPrev(-1)">Anterior</button>
                                            <button type="button" name="enviar_form_def_evidencia" id="nextBtn" onclick="nextPrev(1)">Next</button>
                                          </div>
                                        </div>
                                
                                        <div style="text-align:center;margin-top:40px;">
                                          <span class="step"></span>
                                          <span class="step"></span>
                                          <span class="step"></span>
                                          <span class="step"></span>
                                          <span class="step"></span>
                                          <span class="step"></span>
                                          <span class="step"></span>
                                         
                                        </div>
                                      </form>
                                </div>
                              </div>

                              <div id="ESTATUS" class="tabcontent">
                                <div class="container">
                                  <?php global $wpdb;
                                    $evidencias = $wpdb->prefix . 'definicion_evidencias';
                                    $registros = $wpdb->get_results("SELECT * FROM $evidencias WHERE nombre_dependencia_def='$user_name'", ARRAY_A);
                                      foreach($registros as $registro) 
                                      { 
                                        $i=$registro['id_indicador_asociado_def'];
                                        $nom_ind=$registro['nombre_indicador_asociado_def'];
                                        $btn_ac_des=$registro['estatus'];
                                        ?>

                                        <button class="accordion"  id="acordeon_id_<?php echo($registro['id_indicador_asociado_def'])?>">
                                            <div>
                                              <table>
                                                <col width=85%>
                                                <col width=15%>
                                                  <tbody>
                                                   <td class="" ="contenido_datos" > <?php echo $registro['nombre_indicador_asociado_def'];?></td> 
                                                   <td class="" ="contenido_datos" >

                                                      <?php  
                                                      if($registro['estatus'] == 0)
                                                      {
                                                        echo("<label style='text-align: center; font-size: 17px; font-family: Graphik-Bold  color:#6c757d; '>En Proceso de Revision</label>");

                                                      }
                                                      else if($registro['estatus'] == 1)
                                                      {
                                                       

                                                        echo("<label style='text-align: center; font-size: 17px; font-family: Graphik-Bold; color:#28a745; '>ADECUADA </label>");
                                                      } 
                                                       else if($registro['estatus'] == 2)
                                                      {
                                                       
                                                        echo("<label style='text-align: center; font-size: 17px; font-family: Graphik-Bold; color:#007bff; '>POCO CUESTIONABLE</label>");
                                                      } 
                                                       else if($registro['estatus'] == 3)
                                                      {
                                                        
                                                        echo("<label style='text-align: center; font-size: 17px; font-family: Graphik-Bold; color:#ffc107; '>CUESTIONABLE </label>");
                                                      } 

                                                       else if($registro['estatus'] == 4)
                                                      {

                                                        echo("<label style='text-align: center; font-size: 17px; font-family: Graphik-Bold; color:#dc3545; '>CUESTIONABLE </label>");
                                                     
                                                      } 
                                                       else if($registro['estatus'] == 5)
                                                      {
                                                        echo("<label style='text-align: center; font-size: 17px; font-family: Graphik-Bold ' color:#6c757d; '>SIN ELEMENTOS PARA EVALUAR</label>");
                                                      } 
                                                        ?>                      
                                                    </td> 
                                                  </tbody>
                                              </table>
                                            </div>  
                                        </button>

                                          <div class="panel">

                                            <div class="tab_vert">
                                              <button  id="defaultOpen_vertical" class="tablinks_vertical" onclick="open_vertical_tab(event, 'Parte_indicador<?php echo $registro['id_indicador_asociado_def']; ?>')">INDICADOR<i
                                             id="check_green_indicador_<?php echo $registro['id_indicador_asociado_def']; ?>" ></i></button> 	
			
                                             <button   class="tablinks_vertical" onclick="open_vertical_tab(event, 'Parte_evidencia<?php echo $registro['id_indicador_asociado_def']; ?>')">OBRAS Y ACCIONES<i
                                             id="check_green_evidencia_<?php echo $registro['id_indicador_asociado_def']; ?>"></i></button>
			
                                             <!--
                                  
                                             <button  class="tablinks_vertical" onclick="open_vertical_tab(event, 'Parte_upload_evidencia_<?php echo $registro['id_indicador_asociado_def']; ?>')">IMPORTANCIA<i   
                                             id="check_green_inconsistencias_<?php echo $registro['id_indicador_asociado_def']; ?>" ></i></button>

                                             -->

                                              <button  class="tablinks_vertical" onclick="open_vertical_tab(event, 'Parte_5_<?php echo $registro['id_indicador_asociado_def']; ?>')">RESULTADO<i  
                                             id="check_green_archivos_<?php echo $registro['id_indicador_asociado_def']; ?>" ></i></button>		

                                             <button   class="tablinks_vertical" onclick="open_vertical_tab(event, 'Parte_6_<?php echo $registro['id_indicador_asociado_def']; ?>')">SUBIR EVIDENCIA<i   
                                             id="check_green_6_<?php echo $registro['id_indicador_asociado_def']; ?>"></i></button>
                                             

                                             <button class="tablinks_vertical" onclick="open_vertical_tab(event, 'Parte_7_<?php echo $registro['id_indicador_asociado_def']; ?>')">ARCHIVOS EVIDENCIAS<i   
                                             id="check_green_7_<?php echo $registro['id_indicador_asociado_def']; ?>" ></i></button>
                                              
                                             
                                             <button class="tablinks_vertical" onclick="open_vertical_tab(event, 'Parte_8_<?php echo $registro['id_indicador_asociado_def']; ?>')">COMENTARIOS DEL EVALUADOR<i   
                                             id="check_green_8_<?php echo $registro['id_indicador_asociado_def']; ?>" ></i></button>
                                           

                                           <!--
                                             <button class="tablinks_vertical" onclick="open_vertical_tab(event, 'Parte_9_<?php echo $registro['id_indicador_asociado_def']; ?>')">REPORTE<i   
                                             id="check_green_9_<?php echo $registro['id_indicador_asociado_def']; ?>" ></i></button>

                                           -->


                                            </div>
                                                <!--

                                            </div>
                                              -->



                                         <!-- <form  method="post" id="signupForm_correccion" name="signupForm_correccion" > -->
             
                                            <div id="Parte_indicador<?php echo $registro['id_indicador_asociado_def']; ?>" class="tabcontent_vert">
                                                <div id="global">
                                                    <div id="mensajes" >
                                                      <div class="texto">
                                                        <div id="info_labels_<?php echo $registro['id_indicador_asociado_def'];?>">
                                                         <form id="form_part_indicador" method="post">
                                                         
                                                            <table>
                                                            <col width=20%>
                                                            <col width=80%>
                                                              <tbody>
                                                                <?php global $wpdb;
                                                                $resultado;
                                                                $evidencias = $wpdb->prefix . 'definicion_evidencias';
                                                                $registros_1 = $wpdb->get_results("SELECT * FROM $evidencias where id_indicador_asociado_def='$i'", ARRAY_A);
                                                                foreach($registros_1 as $registro_1) { 
                                                                  ?>
                                                                 
                                                                    <p><label  id="error_edit_linea_discursiva_<?php echo($registro_1['id_indicador_asociado_def'])?>" for="edit_discurso_importancia_ind">Linea discursiva:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                                                                    <textarea  id="edit_linea_discursiva_<?php echo($registro_1['id_indicador_asociado_def'])?>" maxlength="450" name="edit_linea_discursiva_<?php echo($registro_1['id_indicador_asociado_def'])?>" placeholder="Linea discursiva:"><?php echo($registro_1['linea_discursiva'])?></textarea>
                                                                    </p>
                                                                  </tr>

                                                                  <!--
                                                                  <tr>
                                                                    <label for="edit_ind_estrella_<?php echo($registro_1['id_indicador_asociado_def'])?>" >Categoria del indicador</label>
                                                                    <div class="select">
                                                                      <select  name="edit_ind_estrella_<?php echo($registro_1['id_indicador_asociado_def'])?>" id="edit_ind_estrella_<?php echo($registro_1['id_indicador_asociado_def'])?>">
                                                                        <option value="Indicador Estrella">Indicador Estrella</option>
                                                                        <option value="Indicador tactico convencional">Indicador tactico convencional</option>
                                                                        <option value="indicador publico(informe-tablero)">indicador publico(informe-tablero)</option>
                                                                      </select>
                                                                    </div>
                                                                  </tr>

                                                                -->

                                                                  <tr>
                                                                    <br>
                                                                    <p><label >LOGROS PREMIOS Y/O RECONOCIMIENTOS</label></p>
                                                                  
                                                                    <p><label for="edit_logros_en_indicador_<?php echo($registro_1['id_indicador_asociado_def'])?>" >¿Hubo logros en este indicador:?</label></p>
                                                                      <div class="select">
                                                                        <select  name="edit_logros_en_indicador_<?php echo($registro_1['id_indicador_asociado_def'])?>" id="edit_logros_en_indicador_<?php echo($registro_1['id_indicador_asociado_def'])?>">
                                                                          <option value="No">No</option>
                                                                          <option value="Si">Si</option>
                                                                        </select>
                                                                      </div>
                                                                      <br>
                                                                    <div id="edit_bullet_Del_logro_<?php echo($registro_1['id_indicador_asociado_def'])?>">
                                                                      <p><label id="edit_error_id_bullet_Del_logro_<?php echo($registro_1['id_indicador_asociado_def'])?>"  for="edit_id_bullet_Del_logro_<?php echo($registro_1['id_indicador_asociado_def'])?>">Escribir el bullet del logro:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                                                                      <textarea  id="edit_id_bullet_Del_logro_<?php echo($registro_1['id_indicador_asociado_def'])?>" maxlength="450" name="edit_id_bullet_Del_logro_<?php echo($registro_1['id_indicador_asociado_def'])?>" placeholder="Escribir el bullet del logro:"><?php echo($registro_1['bullet_logro'])?></textarea>
                                                                    </p>
                                                                    </div>
                                                                  </tr>

                                                                  <!--
                                                                  <tr>
                                                                    <p><label for="premios_recono" >¿Hubo premios o reconocimientos?</label></p>
                                                                    <div class="select">
                                                                      <select  name="premios_recono_<?php echo($registro_1['id_indicador_asociado_def'])?>" id="premios_recono_<?php echo($registro_1['id_indicador_asociado_def'])?>">
                                                                        <option value="No">No</option>
                                                                        <option value="Si">Si</option>
                                                                      </select>
                                                                    </div>
                                                                    <br>
                                                              

                                                                  </tr>

                                                                -->
                                                                  <tr>

                                                                  <div id="premios_reconocimientos_<?php echo($registro_1['id_indicador_asociado_def'])?>">
                                                                    <p><label id="error_premio_<?php echo($registro_1['id_indicador_asociado_def'])?>"  for="reconocimiento">Describelo:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                                                                    <textarea  id="premio_<?php echo($registro_1['id_indicador_asociado_def'])?>" name="premio_<?php echo($registro_1['id_indicador_asociado_def'])?>" placeholder="Escribir el bullet del premio o reconocimiento"   ><?php echo($registro_1['premio_reconocimiento'])?></textarea>
                                                                    </p>
                                                                  </div>

                                                                  </tr>
                                                                   
                                                                  <?php } 

                                                                ?>
                                                              </tbody>
                                                            </table>

                                                            <script type="text/javascript">
                                                             

                                                              $('#alrt_parte1_actualizado').hide();
                                                              $('#edit_bullet_Del_logro_<?php echo($registro_1['id_indicador_asociado_def'])?>').hide();

                                                              $('#premios_reconocimientos_<?php echo($registro_1['id_indicador_asociado_def'])?>').hide();

                                                              //change premios reconnocimeinto

                                                                $('#premios_recono_<?php echo($registro_1['id_indicador_asociado_def'])?>').change(function()
                                                                { 
                                                                  
                                                                   
                                                                   if($(this).val() == "Si")
                                                                  {
                                                                     $('#premios_reconocimientos_<?php echo($registro_1['id_indicador_asociado_def'])?>').show();
                                                                  }
                                                                  else if($(this).val() == "No")
                                                                  {
                                                                    $('#premios_reconocimientos_<?php echo($registro_1['id_indicador_asociado_def'])?>').hide();
                                                                  }


                                                                  });
                                                                  


                                                              
                                                               $('#edit_logros_en_indicador_<?php echo($registro_1['id_indicador_asociado_def'])?>').change(function()
                                                                { 
                                                                   
                                                                   if($(this).val() == "Si")
                                                                  {
                                                                     $('#edit_bullet_Del_logro_<?php echo($registro_1['id_indicador_asociado_def'])?>').show();
                                                                  }
                                                                  else if($(this).val() == "No")
                                                                  {
                                                                    $('#edit_bullet_Del_logro_<?php echo($registro_1['id_indicador_asociado_def'])?>').hide();
                                                                  }


                                                                  });

                                                              //validar Indicadr Estrella
                                                                 if("<?php echo($registro_1['indicador_estrella'])?>"=="Indicador Estrella")
                                                                 {
                                                               
                                                                  document.getElementById("edit_ind_estrella_<?php echo($registro_1['id_indicador_asociado_def'])?>").selectedIndex = 0;
                                                                 }
                                                                 else if("<?php echo($registro_1['indicador_estrella'])?>"=="Indicador de informe")
                                                                 {

                                                                  document.getElementById("edit_ind_estrella_<?php echo($registro_1['id_indicador_asociado_def'])?>").selectedIndex = 1;
                                                                 }
                                                                 else if("<?php echo($registro_1['indicador_estrella'])?>"=="indicador publico(informe-tablero)")
                                                                 {
                                                                  document.getElementById("edit_ind_estrella_<?php echo($registro_1['id_indicador_asociado_def'])?>").selectedIndex = 2;
                                                                 }


                                                                  //validar Tuvo logro el indicador
                                                                 if("<?php echo($registro_1['logros_indicador'])?>"=="Si")
                                                                 {
                                                                  
                                                                  document.getElementById("edit_logros_en_indicador_<?php echo($registro_1['id_indicador_asociado_def'])?>").selectedIndex = 1;

                                                                  $('#edit_bullet_Del_logro_<?php echo($registro_1['id_indicador_asociado_def'])?>').show();
                                                                 }
                                                                 else if("<?php echo($registro_1['logros_indicador'])?>"=="No")
                                                                 {
                                                               
                                                                  document.getElementById("edit_logros_en_indicador_<?php echo($registro_1['id_indicador_asociado_def'])?>").selectedIndex = 0;
                                                                 }


                                                                 //premios o reconocimeintos

                                                                        //validar Tuvo logro el indicador

                                                                     
                                                                 if("<?php echo($registro_1['hubo_premio'])?>"=="Si")
                                                                 {

                                                                  
                                                                  document.getElementById("premios_recono_<?php echo($registro_1['id_indicador_asociado_def'])?>").selectedIndex = 1;

                                                                  $('#premios_reconocimientos_<?php echo($registro_1['id_indicador_asociado_def'])?>').show();
                                                                 }
                                                                 else if("<?php echo($registro_1['hubo_premio'])?>"=="No")
                                                                 {
                                                               
                                                                  document.getElementById("premios_recono_<?php echo($registro_1['id_indicador_asociado_def'])?>").selectedIndex = 0;
                                                                 }




                                                               

                                                              function SubmitFormData_indicador(valor_entra) 
                                                              {

                                                                var edit_linea_discursiva = $("#edit_linea_discursiva_"+valor_entra).val();
                                                                var edit_ind_estrella = $("#edit_ind_estrella_"+valor_entra).val();
                                                                var logro_indicador = $("#edit_logros_en_indicador_"+valor_entra).val();
                                                                var bullet_logro = $("#edit_id_bullet_Del_logro_"+valor_entra).val();

                                                                var hubo_premio = $("#premios_recono_"+valor_entra).val();
                                                                var premio_reconocimiento = $("#premio_"+valor_entra).val();


                                                                var lin_base_error = document.getElementById("error_edit_linea_discursiva_"+valor_entra);

                                                                 var bull_logro_error = document.getElementById("edit_error_id_bullet_Del_logro_"+valor_entra);

                                                                 var err_premio = document.getElementById("premio_"+valor_entra);


                                                                if(edit_linea_discursiva.length>450)  /* Edicion Robert*/ 
                                                                {
                                                                 
                                                                  lin_base_error.innerHTML ="El limite son 450 caracteres";
                                                                  lin_base_error.classList.add("error_mensaje");

                                                                }
                                                                else if(bullet_logro.length>450)
                                                                {
                                                                 
                                                                  bull_logro_error.innerHTML ="El limite son 350 caracteres";
                                                                  bull_logro_error.classList.add("error_mensaje");


                                                                }
                                                                else if(premio_reconocimiento.length>450)
                                                                {
                                                                 
                                                                  err_premio.innerHTML ="El limite son 350 caracteres";
                                                                  err_premio.classList.add("error_mensaje");
                                                                }


                                                                else
                                                                {


                                                                    $.ajax({
                                                                    type: "POST",
                                                                    url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                                                                    data: {id_indicador:valor_entra,edit_linea_discursiva: edit_linea_discursiva, edit_ind_estrella:edit_ind_estrella,logro_indicador:logro_indicador,bullet_logro,bullet_logro,hubo_premio:hubo_premio,premio_reconocimiento:premio_reconocimiento}
                                                                    ,
                                                                    success: function(msg){
                                                                      console.log(msg)

                                                                      alert( "Informacion Actualizada" );
                                                                    },
                                                                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                                                                       alert( "Error Verifica tu informacion" );
                                                                    }
                                                                  });


                                                                    lin_base_error.classList.remove("error_mensaje");
                                                                    lin_base_error.innerHTML ="Linea discursiva";

                                                                    bull_logro_error.classList.remove("error_mensaje");
                                                                    bull_logro_error.innerHTML ="Linea discursiva";

                                                          

                                                                }

                                                              }
                                                            </script>
                                                            
                                                            <input  style="background-color: #555555;color: white;"  type="button" id="submit_indicador" onclick="SubmitFormData_indicador(<?php echo $registro['id_indicador_asociado_def'];?>)" value="Actualizar Informacion" /> </input> 
                                                                           
                                                          </form>
                                                          
                                                          
                                                          <div>
                                                          </div>
                                                       
                                                      </div>
                                                    </div>
                                                  </div>
                                                  <br>
                                                  <br>
                                                </div>
                                            </div>

                                            <div id="Parte_evidencia<?php echo $registro['id_indicador_asociado_def']; ?>" class="tabcontent_vert">
                                                  <div id="global">
                                                    <div id="mensajes" >
                                                      <div class="texto">
                                                        <div id="info_labels_<?php echo $registro['id_indicador_asociado_def'];?>_2">
                                                          <form id="form_part_indicador" method="post">
                                                          <table>
                                                            <col width=20%>
                                                            <col width=80%>
                                                            <tbody>
                                                              <?php global $wpdb;
                                                                $evidencias = $wpdb->prefix . 'definicion_evidencias';
                                                                $registros_2 = $wpdb->get_results("SELECT * FROM wp_definicion_evidencias AS p JOIN wp_obras_acciones AS r ON r.nombre_indicador = p.nombre_indicador_asociado_def and p.id_indicador_asociado_def='$i'", ARRAY_A);

                                                                foreach($registros_2 as $registro_2) { ?>

                                                                <script type="text/javascript">

                                                                function update_checks_load(indice,id)
                                                                  {
                                                                    var check_bd_lug="";
                                                                    if(indice==1)
                                                                    {
                                                                     check_bd_lug=document.getElementById("res1_lugares_"+id).value;
                                                                    }
                                                                    else if(indice==2)
                                                                    {
                                                                      check_bd_lug=document.getElementById("res2_lugares_"+id).value;
                                                                    }
                                                                     else if(indice==3)
                                                                    {
                                                                      check_bd_lug=document.getElementById("res3_lugares_"+id).value;
                                                                    }
                                                                     else if(indice==4)
                                                                    {
                                                                     check_bd_lug=document.getElementById("res4_lugares_"+id).value;
                                                                    }
                                                                     else if(indice==5)
                                                                    {
                                                                       check_bd_lug=document.getElementById("res5_lugares_"+id).value;
                                                                    }
                                                                    var lug_array = check_bd_lug.split(",");
                                                                    var arr_checks_false = document.getElementsByClassName("check_enti_tac");

                                                                    var arr_checmuni_false = document.getElementsByClassName("check_muni_tac");

                                                                    if(lug_array[0]=="Entidades")
                                                                    {
                                                                      for (i = 1; i < arr_checks_false.length; i++) 
                                                                      {
                                                                        //alert(lug_array[i]);
                                                                         arr_checks_false[parseInt(lug_array[i])].checked = true;
                                                                      }

                                                                    }
                                                                    else if(lug_array[0]=="Municipios")
                                                                    {

                                                                      for (i = 1; i < arr_checmuni_false.length; i++) 
                                                                      {

                                                                         arr_checmuni_false[parseInt(lug_array[i])].checked = true;
                                                                      }

                                                                    }
                                                                                      
                                                                  }
                                                

                                                                </script>

                                                                 
                                                                  <tr>
                                                                    <p><label id="edit_error_resultado_1_resultado_1_<?php echo($registro_2['id_indicador_asociado_def'])?>"  for="resultado_2_<?php echo($registro_2['id_indicador_asociado_def'])?>">Obras y acciones 1:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>

                                                                    <textarea  class="textbox_corto" id="resultado_1_<?php echo($registro_2['id_indicador_asociado_def'])?>" maxlength="350" name="resultado_1_<?php echo($registro_2['id_indicador_asociado_def'])?>" placeholder="Obras y acciones 1:"><?php echo($registro_1['resultado1'])?></textarea>
                                                                    
                                                                    <button  id="btn_def_lugar_1"  onclick="open_lugares(1,<?php echo($registro_2['id_indicador_asociado_def'])?>,0); update_checks_load(1,<?php echo($registro_2['id_indicador_asociado_def'])?>);  "  type="button" >Definir lugar 1</button></p>
                                                                    <label id="datos_cargados_lug_1_<?php echo($registro_2['id_indicador_asociado_def'])?>" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                                                    <input type="hidden" value="<?php echo($registro_2['lug_result1'])?>"  id="res1_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>" name="res1_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>" " >

                                                                    <input type="hidden"  value="<?php echo($registro_2['res1_lugares_gps'])?>"  id="res1_lugares_GPS_<?php echo($registro_2['id_indicador_asociado_def'])?>" name="res1_lugares_GPS_<?php echo($registro_2['id_indicador_asociado_def'])?>" " >
                                                                         
                                                                  </tr>
                                                                  
                                                                  <tr>

                                                                    <p><label id="edit_error_resultado_2_resultado_1_<?php echo($registro_2['id_indicador_asociado_def'])?>" id="label_resultado_2"  for="resultado_2">Obras y acciones 2:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                                                    <textarea  class="textbox_corto" id="resultado_2_<?php echo($registro_2['id_indicador_asociado_def'])?>" maxlength="350" name="resultado_2_<?php echo($registro_2['id_indicador_asociado_def'])?>" placeholder="Obras y acciones 2:"><?php echo($registro_1['resultado2'])?></textarea>
                                                                    <button   id="btn_def_lugar_2" class="btn_def_lugar" onclick="open_lugares(2,<?php echo($registro_2['id_indicador_asociado_def'])?>,0); update_checks_load(2,<?php echo($registro_2['id_indicador_asociado_def'])?>);" type="button" >Definir lugar 2</button></p>
                                                                    <label id="datos_cargados_lug_2_<?php echo($registro_2['id_indicador_asociado_def'])?>" class="datos_cargados_lugar_resultados" >Datos Cargados</label>

                                                                   <input type="hidden" value="<?php echo($registro_2['lug_result2'])?>"  id="res2_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>" name="res2_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>" " >

                                                                    <input type="hidden" value="<?php echo($registro_2['res2_lugares_gps'])?>"  id="res2_lugares_GPS_<?php echo($registro_2['id_indicador_asociado_def'])?>" name="res2_lugares_GPS_<?php echo($registro_2['id_indicador_asociado_def'])?>" " >
                                                                                   
                                                                  </tr>

                                                                    <p><label id="edit_error_resultado_3_resultado_1_<?php echo($registro_2[' resultado3'])?>"  for="resultado_3">Obras y acciones 3:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                                                    <textarea  class="textbox_corto"  id="resultado_3_<?php echo($registro_2['id_indicador_asociado_def'])?>" maxlength="350" name="resultado_3_<?php echo($registro_2['id_indicador_asociado_def'])?>" placeholder="Obras y acciones 3:"><?php echo($registro_1['resultado3'])?></textarea>
                                                                    <button  class="btn_def_lugar" id="btn_def_lugar_3" onclick="open_lugares(3,<?php echo($registro_2['id_indicador_asociado_def'])?>,0); update_checks_load(3,<?php echo($registro_2['id_indicador_asociado_def'])?>);" type="button" >Definir lugar 3</button></p>
                                                                    <label id="datos_cargados_lug_3_<?php echo($registro_2['id_indicador_asociado_def'])?>" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                                                    
                                                                    <input type="hidden" value="<?php echo($registro_2['lug_result3'])?>"  id="res3_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>" name="res3_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>" " >

                                                                    <input type="hidden" value="<?php echo($registro_2['res3_lugares_gps'])?>"  id="res3_lugares_GPS_<?php echo($registro_2['id_indicador_asociado_def'])?>" name="res3_lugares_GPS_<?php echo($registro_2['id_indicador_asociado_def'])?>" " >

                                                                  <tr>
                                                                    <p><label id="edit_error_resultado_4_resultado_1_<?php echo($registro_2['id_indicador_asociado_def'])?>" id="label_resultado_4"  for="resultado_4">Obras y acciones 4:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                                                    <textarea  class="textbox_corto" id="resultado_4_<?php echo($registro_2['id_indicador_asociado_def'])?>" maxlength="350" name="resultado_4_<?php echo($registro_2['id_indicador_asociado_def'])?>" placeholder="Obras y acciones 4:"><?php echo($registro_1['resultado4'])?></textarea>
                                                                    <button  id="btn_def_lugar_4" class="btn_def_lugar" onclick="open_lugares(4,<?php echo($registro_2['id_indicador_asociado_def'])?>,0); update_checks_load(4,<?php echo($registro_2['id_indicador_asociado_def'])?>);"  type="button" >Definir lugar 4</button></p>
                                                                    <label id="datos_cargados_lug_4_<?php echo($registro_2['id_indicador_asociado_def'])?>" class="datos_cargados_lugar_resultados" >Datos Cargados</label>

                                                                    <input type="hidden" value="<?php echo($registro_2['lug_result4'])?>"  id="res4_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>" name="res4_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>" " >

                                                                    <input type="hidden" value="<?php echo($registro_2['res4_lugares_gps'])?>"  id="res4_lugares_GPS_<?php echo($registro_2['id_indicador_asociado_def'])?>" name="res4_lugares_GPS_<?php echo($registro_2['id_indicador_asociado_def'])?>" " >
                                                          
                                                                  <tr>
                                                                    <p><label id="edit_error_resultado_5_resultado_1_<?php echo($registro_2['id_indicador_asociado_def'])?>"  for="resultado_5">Obras y acciones 5:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                                                    <textarea  class="textbox_corto" id="resultado_5_<?php echo($registro_2['id_indicador_asociado_def'])?>" maxlength="350" name="resultado_5_<?php echo($registro_2['id_indicador_asociado_def'])?>" placeholder="Obras y acciones 5:"><?php echo($registro_1['resultado5'])?></textarea>
                                                                    <button  id="btn_def_lugar_5" class="btn_def_lugar" onclick="open_lugares(5,<?php echo($registro_2['id_indicador_asociado_def'])?>,0); update_checks_load(5,<?php echo($registro_2['id_indicador_asociado_def'])?>);"  type="button" >Definir lugar 5</button></p>
                                                                    <label id="datos_cargados_lug_5_<?php echo($registro_2['id_indicador_asociado_def'])?>" class="datos_cargados_lugar_resultados" >Datos Cargados</label>

                                                                    <input type="hidden" value="<?php echo($registro_2['lug_result5'])?>"  id="res5_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>" name="res5_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>" " >

                                                                    <input type="hidden"  value="<?php echo($registro_2['res5_lugares_gps'])?>"  id="res5_lugares_GPS_<?php echo($registro_2['id_indicador_asociado_def'])?>" name="res5_lugares_GPS_<?php echo($registro_2['id_indicador_asociado_def'])?>" " >

                                                                     <textarea  class="textbox_corto" id="resultado_6_<?php echo($registro_2['id_indicador_asociado_def'])?>" maxlength="350" name="resultado_6_<?php echo($registro_2['id_indicador_asociado_def'])?>" placeholder="Obras y acciones 6:"><?php echo($registro_1['resultado6'])?></textarea>
                                                                    <button  id="btn_def_lugar_6" class="btn_def_lugar" onclick="open_lugares(6,<?php echo($registro_2['id_indicador_asociado_def'])?>,0); update_checks_load(6,<?php echo($registro_2['id_indicador_asociado_def'])?>);"  type="button" >Definir lugar 6</button></p>
                                                                    <label id="datos_cargados_lug_6_<?php echo($registro_2['id_indicador_asociado_def'])?>" class="datos_cargados_lugar_resultados" >Datos Cargados</label>

                                                                    <input type="hidden"  value="<?php echo($registro_2['lug_result6'])?>"  id="res6_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>" name="res6_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>" " >

                                                                    <input type="hidden" value="<?php echo($registro_2['res6_lugares_gps'])?>"  id="res6_lugares_GPS_<?php echo($registro_2['id_indicador_asociado_def'])?>" name="res6_lugares_GPS_<?php echo($registro_2['id_indicador_asociado_def'])?>" " >


                                                                     <textarea  class="textbox_corto" id="resultado_7_<?php echo($registro_2['id_indicador_asociado_def'])?>" maxlength="350" name="resultado_7_<?php echo($registro_2['id_indicador_asociado_def'])?>" placeholder="Obras y acciones 7:"><?php echo($registro_1['resultado7'])?></textarea>
                                                                    <button  id="btn_def_lugar_7" class="btn_def_lugar" onclick="open_lugares(7,<?php echo($registro_2['id_indicador_asociado_def'])?>,0); update_checks_load(7,<?php echo($registro_2['id_indicador_asociado_def'])?>);"  type="button" >Definir lugar 7</button></p>
                                                                    <label id="datos_cargados_lug_7_<?php echo($registro_2['id_indicador_asociado_def'])?>" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                                                   <input type="hidden"  value="<?php echo($registro_2['lug_result7'])?>"  id="res7_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>" name="res7_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>" " >

                                                                    <input type="hidden" value="<?php echo($registro_2['res7_lugares_gps'])?>"  id="res7_lugares_GPS_<?php echo($registro_2['id_indicador_asociado_def'])?>" name="res7_lugares_GPS_<?php echo($registro_2['id_indicador_asociado_def'])?>" " >

                                                                     <textarea  class="textbox_corto" id="resultado_8_<?php echo($registro_2['id_indicador_asociado_def'])?>" maxlength="350" name="resultado_8_<?php echo($registro_2['id_indicador_asociado_def'])?>" placeholder="Obras y acciones 8:"><?php echo($registro_1['resultado8'])?></textarea>
                                                                    <button  id="btn_def_lugar_8" class="btn_def_lugar" onclick="open_lugares(8,<?php echo($registro_2['id_indicador_asociado_def'])?>,0); update_checks_load(8,<?php echo($registro_2['id_indicador_asociado_def'])?>);"  type="button" >Definir lugar 8</button></p>
                                                                    <label id="datos_cargados_lug_8_<?php echo($registro_2['id_indicador_asociado_def'])?>" class="datos_cargados_lugar_resultados" >Datos Cargados</label>


                                                                    <input type="hidden" value="<?php echo($registro_2['lug_result8'])?>"  id="res8_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>" name="res8_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>" " >

                                                                    <input type="hidden" value="<?php echo($registro_2['res8_lugares_gps'])?>"  id="res8_lugares_GPS_<?php echo($registro_2['id_indicador_asociado_def'])?>" name="res8_lugares_GPS_<?php echo($registro_2['id_indicador_asociado_def'])?>" " >
                                                                                                   
                                                                  </tr>

                                                                  <tr>
                                                                    <br><br>
                                                                    <p><label <>LO QUE SIGUE...</label></p>
                                                                    
                                                                    <p><label  id="edit_error_lo_que_sigue_1_<?php echo($registro_2['id_indicador_asociado_def'])?>" for="lo_que_sigue_1">1.- <strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                                                                    <textarea  id="lo_que_sigue_1_<?php echo($registro_2['id_indicador_asociado_def'])?>" maxlength="450" name="lo_que_sigue_1_<?php echo($registro_2['id_indicador_asociado_def'])?>" placeholder=""><?php echo($registro_1['lo_que_sigue_1'])?></textarea>
                                                                            
                                                                  </tr>

                                                                  <tr>
                                                                    <p><label  id="edit_error_lo_que_sigue_2_<?php echo($registro_2['id_indicador_asociado_def'])?>"  for="lo_que_sigue_2">2.- <strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                                                                    <textarea  id="lo_que_sigue_2_<?php echo($registro_2['id_indicador_asociado_def'])?>" maxlength="450" name="lo_que_sigue_2_<?php echo($registro_2['id_indicador_asociado_def'])?>" placeholder=""><?php echo($registro_1['lo_que_sigue_2'])?></textarea>
                                                                  </tr>

                                                                  <tr>
                                                                    <p><label  id="edit_error_lo_que_sigue_3_<?php echo($registro_2['id_indicador_asociado_def'])?>"  for="lo_que_sigue_3">3.- <strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong> </label>
                                                                    <textarea  id="lo_que_sigue_3_<?php echo($registro_2['id_indicador_asociado_def'])?>" maxlength="450" name="lo_que_sigue_3_<?php echo($registro_2['id_indicador_asociado_def'])?>" placeholder=""><?php echo($registro_1['lo_que_sigue_3'])?></textarea>
                                                                  </tr>
                                                                    <?php } 

                                                                  ?>
                                                            </tbody>
                                                          </table>

                                                          <script type="text/javascript">
                                                            //ocultar el mensaje datos cargados
                                                            
                                                            //5 mensajes de datos cargados
                                                            $('#datos_cargados_lug_1_<?php echo($registro_2['id_indicador_asociado_def'])?>').hide();
                                                            $('#datos_cargados_lug_2_<?php echo($registro_2['id_indicador_asociado_def'])?>').hide();
                                                            $('#datos_cargados_lug_3_<?php echo($registro_2['id_indicador_asociado_def'])?>').hide();
                                                            $('#datos_cargados_lug_4_<?php echo($registro_2['id_indicador_asociado_def'])?>').hide();
                                                            $('#datos_cargados_lug_5_<?php echo($registro_2['id_indicador_asociado_def'])?>').hide();

                                                            $('#datos_cargados_lug_6_<?php echo($registro_2['id_indicador_asociado_def'])?>').hide();
                                                            $('#datos_cargados_lug_7_<?php echo($registro_2['id_indicador_asociado_def'])?>').hide();
                                                            $('#datos_cargados_lug_8_<?php echo($registro_2['id_indicador_asociado_def'])?>').hide();



                                                          function SubmitFormData_evidencia(valor_entra, nom_ind) 
                                                            {

                                                              var nom_indicador=nom_ind;
                                                             // alert(nom_ind);

                                                              var resultado1=$("#resultado_1_"+valor_entra).val();

                                                              var resultado2=$("#resultado_2_"+valor_entra).val();

                                                              var resultado3=$("#resultado_3_"+valor_entra).val();

                                                              var resultado4=$("#resultado_4_"+valor_entra).val();

                                                              var resultado5=$("#resultado_5_"+valor_entra).val();

                                                              var resultado6=$("#resultado_6_"+valor_entra).val();

                                                              var resultado7=$("#resultado_7_"+valor_entra).val();

                                                              var resultado8=$("#resultado_8_"+valor_entra).val();
                                                              //-----lugares--------------------//

                                                              var reslug1 = $("#res1_lugares_"+valor_entra).val();

                                                              var reslug2 = $("#res2_lugares_"+valor_entra).val();

                                                              var reslug3 = $("#res3_lugares_"+valor_entra).val();

                                                              var reslug4 = $("#res4_lugares_"+valor_entra).val();

                                                              var reslug5 = $("#res5_lugares_"+valor_entra).val();

                                                              var reslug6 = $("#res6_lugares_"+valor_entra).val();

                                                              var reslug7 = $("#res7_lugares_"+valor_entra).val();

                                                              var reslug8 = $("#res8_lugares_"+valor_entra).val();

                                                              var reslug1_gps = $("#res1_lugares_GPS_"+valor_entra).val();

                                                              var reslug2_gps = $("#res2_lugares_GPS_"+valor_entra).val();

                                                              var reslug3_gps = $("#res3_lugares_GPS_"+valor_entra).val();

                                                              var reslug4_gps = $("#res4_lugares_GPS_"+valor_entra).val();

                                                              var reslug5_gps = $("#res5_lugares_GPS_"+valor_entra).val();

                                                              var reslug6_gps = $("#res6_lugares_GPS_"+valor_entra).val();

                                                              var reslug7_gps = $("#res7_lugares_GPS_"+valor_entra).val();

                                                              var reslug8_gps = $("#res8_lugares_GPS_"+valor_entra).val();


                                                              
                                                             



                                                              //----------lo q sigue

                                                              var loquesigue1 = $("#lo_que_sigue_1_"+valor_entra).val();

                                                              var loquesigue2=$("#lo_que_sigue_2_"+valor_entra).val();

                                                              var loquesigue3=$("#lo_que_sigue_3_"+valor_entra).val();


                                                              

                                                                var res_1 = document.getElementById("edit_error_resultado_1_resultado_1_"+valor_entra);
                                                                var res_2 = document.getElementById("edit_error_resultado_2_resultado_1_"+valor_entra);
                                                                var res_3 = document.getElementById("edit_error_resultado_3_resultado_1_"+valor_entra);
                                                                var res_4 = document.getElementById("edit_error_resultado_4_resultado_1_"+valor_entra);
                                                                var res_5 = document.getElementById("edit_error_resultado_5_resultado_1_"+valor_entra);

                                                                var res_6 = document.getElementById("edit_error_resultado_3_resultado_1_"+valor_entra);
                                                                var res_7 = document.getElementById("edit_error_resultado_4_resultado_1_"+valor_entra);
                                                                var res_8 = document.getElementById("edit_error_resultado_5_resultado_1_"+valor_entra);


                                                                var err_lo_sigue_1 = document.getElementById("edit_error_lo_que_sigue_1_"+valor_entra);
                                                                var err_lo_sigue_2 = document.getElementById("edit_error_lo_que_sigue_2_"+valor_entra);
                                                                var err_lo_sigue_3 = document.getElementById("edit_error_lo_que_sigue_3_"+valor_entra);



                                                                if(resultado1.length>350) /*Editado Robert*/ 
                                                                {
                                                                  res_1.innerHTML ="El limite son 350 caracteres";
                                                                  res_1.classList.add("error_mensaje");
                                                                }
                                                                else if(resultado2.length>350) /*Editado Robert*/ 
                                                                {
                                                                  res_2.innerHTML ="El limite son 350 caracteres";
                                                                  res_2.classList.add("error_mensaje");
                                                                }
                                                                else if(resultado3.length>350) /*Editado Robert*/ 
                                                                {
                                                                  res_3.innerHTML ="El limite son 350 caracteres";
                                                                  res_3.classList.add("error_mensaje");

                                                                }
                                                                else if(resultado4.length>350) /*Editado Robert*/ 
                                                                {
                                                                  res_4.innerHTML ="El limite son 350 caracteres";
                                                                  res_4.classList.add("error_mensaje");

                                                                }
                                                                else if(resultado5.length>350) /*Editado Robert*/ 
                                                                {
                                                                  res_5.innerHTML ="El limite son 350 caracteres";
                                                                  res_5.classList.add("error_mensaje");

                                                                }
                                                                else if(resultado6.length>350) /*Editado Robert*/ 
                                                                {
                                                                  res_6.innerHTML ="El limite son 350 caracteres";
                                                                  res_6.classList.add("error_mensaje");

                                                                }
                                                                else if(resultado7.length>350) /*Editado Robert*/ 
                                                                {
                                                                  res_7.innerHTML ="El limite son 350 caracteres";
                                                                  res_7.classList.add("error_mensaje");

                                                                }
                                                                else if(resultado8.length>350) /*Editado Robert*/ 
                                                                {
                                                                  res_8.innerHTML ="El limite son 350 caracteres";
                                                                  res_8.classList.add("error_mensaje");

                                                                }
                                                                  


                                                                   else if(loquesigue1.length>450)
                                                                  {
                                                                    err_lo_sigue_1.innerHTML ="El limite son 450 caracteres";
                                                                    err_lo_sigue_1.classList.add("error_mensaje");

                                                                  }
                                                                  else if(loquesigue2.length>450)
                                                                  {
                                                                    err_lo_sigue_2.innerHTML ="El limite son 450 caracteres";
                                                                    err_lo_sigue_2.classList.add("error_mensaje");

                                                                  }
                                                                   else if(loquesigue3.length>450)
                                                                  {
                                                                    err_lo_sigue_3.innerHTML ="El limite son 450 caracteres";
                                                                    err_lo_sigue_3.classList.add("error_mensaje");

                                                                  }
                                                                
                                                                else
                                                                {

                                                                  $.ajax({
                                                                    type: "POST",
                                                                    url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                                                                    data: {id_indicador:valor_entra,nom_indicador:nom_indicador,resultado1:resultado1,resultado2:resultado2,resultado3:resultado3,resultado4:resultado4,resultado5:resultado5,resultado6:resultado6,resultado7:resultado7,resultado8:resultado8,loquesigue1:loquesigue1,loquesigue2:loquesigue2,loquesigue3:loquesigue3,reslug1:reslug1,reslug2:reslug2,reslug3:reslug3,reslug4:reslug4,reslug5:reslug5,reslug6:reslug6,reslug7:reslug7,reslug8:reslug8,reslug1_gps:reslug1_gps,reslug2_gps:reslug2_gps,reslug3_gps:reslug3_gps,reslug4_gps:reslug4_gps,reslug5_gps:reslug5_gps,reslug6_gps:reslug6_gps,reslug7_gps:reslug7_gps,reslug8_gps:reslug8_gps}
                                                                    ,
                                                                    success: function(msg){
                                                                      if(msg=1)
                                                                      {
                                                                        alert("Informacion Actualizada");
                                                                      }
                                                                      
                                                                    },
                                                                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                                                                       alert( "Error Verifica tu informacion" );
                                                                    }
                                                                  });


                                                                    res_1.classList.remove("error_mensaje");
                                                                    res_1.innerHTML ="Obras y acciones 1: ";

                                                                    res_2.classList.remove("error_mensaje");
                                                                    res_2.innerHTML ="Obras y acciones 2: ";

                                                                    res_3.classList.remove("error_mensaje");
                                                                    res_3.innerHTML ="Obras y acciones 3: ";

                                                                    res_4.classList.remove("error_mensaje");
                                                                    res_4.innerHTML ="Obras y acciones 4: ";

                                                                    res_5.classList.remove("error_mensaje");
                                                                    res_5.innerHTML ="Obras y acciones 5: ";

                                                                    res_6.classList.remove("error_mensaje");
                                                                    res_6.innerHTML ="Obras y acciones 6: ";

                                                                    res_7.classList.remove("error_mensaje");
                                                                    res_7.innerHTML ="Obras y acciones 7: ";

                                                                    res_8.classList.remove("error_mensaje");
                                                                    res_8.innerHTML ="Obras y acciones 8: ";

                                                                    err_lo_sigue_1.classList.remove("error_mensaje");
                                                                    err_lo_sigue_1.innerHTML ="1.-";

                                                                    err_lo_sigue_2.classList.remove("error_mensaje");
                                                                    err_lo_sigue_2.innerHTML ="2.-";

                                                                    err_lo_sigue_3.classList.remove("error_mensaje");
                                                                    err_lo_sigue_3.innerHTML ="3.-";

                                                                }

                                                              
                        

                                                              
                                                              

                                                            }
                                                          </script>

                                                                              
                                                          <input  style="background-color: #555555;color: white;" type="button" id="submit_indicador" onclick="SubmitFormData_evidencia(<?php echo $registro['id_indicador_asociado_def'];?>,'<?php echo $nom_ind; ?>');" value="Actualizar Informacion" />  
                                                                        
                                                          </form>



                                                        <div>
                                                        </div>
                                                      </div>
                                                     </div>
                                                    </div>
                                                  </div>
                                            </div>

            
                                            <div id="Parte_upload_evidencia_<?php echo $registro['id_indicador_asociado_def']; ?>" class="tabcontent_vert">
                                                <div id="global">
                                                  <div id="mensajes" >
                                                    <div class="texto">
                                         
                                                       <div id="info_labels_<?php echo $registro['id_indicador_asociado_def'];?>_3">
                                                         
                                                        <form id="form_part_indicador" method="post">
                                                        <table>
                                                          <col width=20%>
                                                          <col width=80%>
                                                          <tbody>
                                                            <?php global $wpdb;
                                                              $evidencias = $wpdb->prefix . 'definicion_evidencias';
                                                              $registros_3 = $wpdb->get_results("SELECT * FROM $evidencias where id_indicador_asociado_def='$i'", ARRAY_A);
                                                              foreach($registros_3 as $registro_3) 
                                                                { ?>
                                                                 
                                                                  <tr>
                                                                    <br>
                                                                    <br>
                                                                    <p><label id="error_edit_descripcion_indicador_def_<?php echo($registro_3['id_indicador_asociado_def'])?>" for="edit_descripcion_indicador_<?php echo($registro_3['id_indicador_asociado_def'])?>">Descripción del indicador, ¿qué mide el indicador?<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label> <textarea  id="edit_descripcion_indicador_<?php echo($registro_3['id_indicador_asociado_def'])?>" maxlength="450" name="edit_descripcion_indicador_<?php echo($registro_3['id_indicador_asociado_def'])?>" placeholder=" Descripción del indicador, ¿qué mide el indicador?" ><?php echo($registro_1['descripcion_indicador_def'])?></textarea></p>
                                                                  </tr>

                                                                  <tr>
                                                                    <br>
                                                                    <p><label id="error_estr_unidad_observacion_indicador_def" for="edit_unidad_observacion_indicador_def_<?php echo($registro_3['id_indicador_asociado_def'])?>">¿Cuál es la unidad de observación del indicador? (Ej. acciones, población, etc.):</label> 

                                                                    <input  value="<?php echo($registro_1['unidad_observacion_indicador_def'])?>" id="edit_unidad_observacion_indicador_def_<?php echo($registro_3['id_indicador_asociado_def'])?>" name="edit_unidad_observacion_indicador_def_<?php echo($registro_3['id_indicador_asociado_def'])?>" placeholder="¿Cuál es la unidad de observación del indicador? (Ej. acciones, población, etc.)"   ></p>
                                                                  </tr>

                                                                  <tr>
                                                                    <br>
                                                                    <p><label id="error_edit_descr_obs_ficha_tecnica_<?php echo($registro_3['id_indicador_asociado_def'])?>" for="edit_descripcion_obj_secto_<?php echo($registro_3['id_indicador_asociado_def'])?>">Da una breve descripción  de la unidad de la observación (Consultar la ficha técnica)<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label><textarea  id="edit_descripcion_obj_secto_<?php echo($registro_3['id_indicador_asociado_def'])?>" maxlength="450" name="edit_descripcion_obj_secto_<?php echo($registro_3['id_indicador_asociado_def'])?>" placeholder=" Da una breve descripción de la unidad de la observación Consultar la ficha técnica" ><?php echo($registro_1['descripcion_obj_secto_def'])?></textarea></p>
                                                                  </tr>

                                                                  <tr>
                                                                    <br>
                                                                    <label for="edit_tipo_indicador_def_<?php echo($registro_3['id_indicador_asociado_def'])?>" >¿Qué tipo de indicador es?</label>
                                                                    <div class="select">
                                                                      <select  name="edit_tipo_indicador_def_<?php echo($registro_3['id_indicador_asociado_def'])?>" id="edit_tipo_indicador_def_<?php echo($registro_3['id_indicador_asociado_def'])?>">
                                                                        <option value="Absolutos">Absolutos</option>
                                                                        <option value="Diferencia" (resta)>Diferencia (resta)</option>
                                                                        <option value="Porcentaje acumulado">Porcentaje acumulado</option>
                                                                        <option value="Porcentaje de avance con respecto a la línea base">Porcentaje de avance con respecto a la línea base</option>
                                                                        <option value="Porcentaje de avance con respecto al año anterior">Porcentaje de avance con respecto al año anterior</option>
                                                                        <option value="Promedio">Promedio</option>
                                                                        <option value="Razon">Razon</option>
                                                                        <option value="Tasa">Tasa</option>
                                                                        <option value="índice">índice</option>
                                                                        <option value="Otro">Otro</option>
                                                                      </select>
                                                                    </div>
                                                                  </tr>

                                                                  <tr>
                                                                    <br>
                                                                      <div id="edit_otro_tipo_indicador_<?php echo($registro_3['id_indicador_asociado_def'])?>">
                                                                        <p><label for="otro_tipo_indicador_def">¿Cual?:</label> <input  value="<?php echo($registro_1['otro_tipo_indicador_def'])?>" name="edit_otro_tipo_indicador_def_<?php echo($registro_3['id_indicador_asociado_def'])?>" id="edit_otro_tipo_indicador_def_<?php echo($registro_3['id_indicador_asociado_def'])?>" placeholder=""   ></p>
                                                                      </div>
                                                                  </tr>

                                                              <?php } 

                                                                ?>
                                                          </tbody>
                                                        </table>

                                                        <script type="text/javascript">
                                                          //change opcion otro
                                                          $('#edit_tipo_indicador_def_<?php echo($registro_3['id_indicador_asociado_def'])?>').change(function()
                                                          { 

                                                            if($(this).val() == "Otro")
                                                            {
                                                            $('#edit_otro_tipo_indicador_<?php echo($registro_3['id_indicador_asociado_def'])?>').show();

                                                            }
                                                            if($(this).val() != "Otro"){
                                                            $('#edit_otro_tipo_indicador_<?php echo($registro_3['id_indicador_asociado_def'])?>').hide();
                                                            }
                                                          });
                                                          //inicar ocultando la opcin otro
                                                          $('#edit_otro_tipo_indicador_<?php echo($registro_3['id_indicador_asociado_def'])?>').hide();




                                                          if("<?php echo($registro_3['tipo_indicador_def'])?>"=="Absolutos")
                                                                 {
                                                               
                                                                  document.getElementById("edit_tipo_indicador_def_<?php echo($registro_3['id_indicador_asociado_def'])?>").selectedIndex = 0;
                                                                 }
                                                          else if("<?php echo($registro_3['tipo_indicador_def'])?>"=="Diferencia")
                                                                 {

                                                                  document.getElementById("edit_tipo_indicador_def_<?php echo($registro_3['id_indicador_asociado_def'])?>").selectedIndex = 1;
                                                                 }
                                                          else if("<?php echo($registro_3['tipo_indicador_def'])?>"=="Porcentaje acumulado")
                                                                 {

                                                                  document.getElementById("edit_tipo_indicador_def_<?php echo($registro_3['id_indicador_asociado_def'])?>").selectedIndex = 2;
                                                                 }
                                                          else if("<?php echo($registro_3['tipo_indicador_def'])?>"=="Porcentaje de avance con respecto a la línea base")
                                                                 {

                                                                  document.getElementById("edit_tipo_indicador_def_<?php echo($registro_3['id_indicador_asociado_def'])?>").selectedIndex = 3;
                                                                 }
                                                          else if("<?php echo($registro_3['tipo_indicador_def'])?>"=="Porcentaje de avance con respecto al año anterior")
                                                                 {

                                                                  document.getElementById("edit_tipo_indicador_def_<?php echo($registro_3['id_indicador_asociado_def'])?>").selectedIndex = 4;
                                                                 }
                                                          else if("<?php echo($registro_3['tipo_indicador_def'])?>"=="Promedio")
                                                                 {

                                                                  document.getElementById("edit_tipo_indicador_def_<?php echo($registro_3['id_indicador_asociado_def'])?>").selectedIndex = 5;
                                                                 }
                                                          else if("<?php echo($registro_3['tipo_indicador_def'])?>"=="Razon")
                                                                 {

                                                                  document.getElementById("edit_tipo_indicador_def_<?php echo($registro_3['id_indicador_asociado_def'])?>").selectedIndex = 6;
                                                                 }
                                                          else if("<?php echo($registro_3['tipo_indicador_def'])?>"=="Tasa")
                                                                 {

                                                                  document.getElementById("edit_tipo_indicador_def_<?php echo($registro_3['id_indicador_asociado_def'])?>").selectedIndex = 7;
                                                                 }
                                                          else if("<?php echo($registro_3['tipo_indicador_def'])?>"=="índice")
                                                                 {

                                                                  document.getElementById("edit_tipo_indicador_def_<?php echo($registro_3['id_indicador_asociado_def'])?>").selectedIndex = 8;
                                                                 }
                                                          else if("<?php echo($registro_3['tipo_indicador_def'])?>"=="Otro")
                                                                 {

                                                                  document.getElementById("edit_tipo_indicador_def_<?php echo($registro_3['id_indicador_asociado_def'])?>").selectedIndex = 9;
                                                                  $('#edit_otro_tipo_indicador_<?php echo($registro_3['id_indicador_asociado_def'])?>').show();
                                                                 }







                                                            function SubmitFormData_inconsistencias(valor_entra) 
                                                            {
                                                              var desc_documento_vinculado = $("#edit_descripcion_indicador_"+valor_entra).val();
                                                              var unidad_observacion = $("#edit_unidad_observacion_indicador_def_"+valor_entra).val();
                                                              var desr_ficha_tecnica = $("#edit_descripcion_obj_secto_"+valor_entra).val();
                                                              var tipo_indicador = $("#edit_tipo_indicador_def_"+valor_entra).val();
                                                              var otro_tipo_indicador = $("#edit_otro_tipo_indicador_def_"+valor_entra).val();

                                                              var edit_error_descripcion_ind = document.getElementById("error_edit_descripcion_indicador_def_"+valor_entra);

                                                              var edit_error_desc_ind= document.getElementById("error_edit_descr_obs_ficha_tecnica_"+valor_entra);


                                                              

                                                              if(desc_documento_vinculado.length>450) /* Editado Robert */
                                                              {
                                                                edit_error_descripcion_ind.innerHTML ="El limite son 450 caracteres";
                                                                edit_error_descripcion_ind.classList.add("error_mensaje");

                                                              }
                                                              else if(desr_ficha_tecnica.length>450) /* Editado Robert */
                                                              {
                                                                edit_error_desc_ind.innerHTML ="El limite son 450 caracteres";
                                                                edit_error_desc_ind.classList.add("error_mensaje");

                                                              }


                                                              else
                                                              {


                                                              $.ajax({
                                                                type: "POST",
                                                                    url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                                                                    data: {  id_indicador:valor_entra,desc_documento_vinculado:desc_documento_vinculado,unidad_observacion:unidad_observacion,desr_ficha_tecnica:desr_ficha_tecnica,tipo_indicador:tipo_indicador,otro_tipo_indicador:otro_tipo_indicador
                                                                      }
                                                                    ,
                                                                    success: function(msg){
                                                                      if(msg=1)
                                                                      {
                                                                        alert("Informacion Actualizada");
                                                                      }
                                                                      
                                                                    },
                                                                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                                                                       alert( "Error Verifica tu informacion" );
                                                                    }
                                                                  });

                                                              }
                                                              
                                                  


                                                            }
                                                          </script>

                                                          <br>
                                                          <input  style="background-color: #555555;color: white;" type="button" id="submit_indicador" type="button" id="submit_indicador" onclick="SubmitFormData_inconsistencias(<?php echo $registro['id_indicador_asociado_def'];?>)" value="Actualizar Informacion" />  
                                                                 <br><br>            
                                                          </form>


                                                      <div>
                                                        
                                                      </div>
                                                    </div>
                                                  </div>
                                                 </div>
                                                </div>
                                            </div>

                                            <div id="Parte_5_<?php echo $registro['id_indicador_asociado_def']; ?>" class="tabcontent_vert">
                                                <div id="global">
                                                  <div id="mensajes" >
                                                    <div class="texto">
                                               
                                                      <div id="info_labels_<?php echo $registro['id_indicador_asociado_def'];?>_5">
                                                        <table>
                                                          <col width=20%>
                                                          <col width=80%>
                                                          <tbody>

                                                            <br>
                                                            

                                                            <label style="text-align: center color: #4CAF50; font-size: 17px; font-family: Graphik-Bold" >Corte del cuarto trimestre 2019</label>


                                                              <tr>
                                                                <!--
                                                                
                                                                <p><label hidden="true" id="error_linea_base_absolutos_def" for="linea_base_absolutos_def">Indicar la línea base en absolutos (números absolutos)</label>
                                                                <input hidden="true"  maxlength="15" minlength="0" id="linea_base_absolutos_def_<?php echo($registro_1['id_indicador_asociado_def'])?>" name="linea_base_absolutos_def" placeholder="Indicar la línea base en absolutos (números absolutos)"  value="<?php echo($registro_1['linea_base_absolutos_def'])?>" ></p>
                                                              </tr>

                                                              <tr>
                                                              
                                                                <p><label hidden="true" for="resultado_18"  id="error_resultado_18">Resultado reportado para el primer informe de resultados (Resultado a primer corte)</label>
                                                                <input hidden="true"  maxlength="15" minlength="0"  name="resultado_18" placeholder="%" id="rep_prim_inf_<?php echo($registro_1['id_indicador_asociado_def'])?>" value="<?php echo($registro_1['resultado_18'])?>"></p>    
                                                              </tr>

                                                              <tr>
                                                                
                                                                <p><label hidden="true" for="avance_2018"  id="error_avance_2018">Resultado reportado para el segundo informe de resultados (primer corte)</label>
                                                                <input hidden="true"  maxlength="15" minlength="0" name="avance_2018" placeholder="%" id="avance_2018_<?php echo($registro_1['id_indicador_asociado_def'])?>" value="<?php echo($registro_1['meta_2018'])?>"></p>
                                                              </tr>

                                                            -->
                                                            

                                                            <tr>

                                                               <p style=" text-align:right ;padding: 3px 10px; ">
                                                                
                                                                  <label style="text-align: right; font-size: 17px; font-family: Graphik-Bold">Ejemplo <span class="glyphicon glyphicon-info-sign" style="  font-size:20px;color:orange;text-shadow:2px 2px 4px #000000;" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"></span></label>
                                                              </p>

                                                              

                                                              <br>
                                                              <p><label for="avance_2018_2"  id="error_avance_2018_2">Ingrese el resultado 2019 (Cifra que indica el desempeño de acuerdo al indicador, cuyo valor ideal es igual a la meta)<strong class="tamanio_caracteres" style="size: 30" id="tamanio_caracteres"> Ejemplo:"Porcentaje de cobertura en educacion preescolar"
                                                              Meta 2019:75%; Resultado:74.9%</strong></label>




                                                              <input type="number" maxlength="15" minlength="0" name="avance_2018_2" placeholder="" id="avance_2018_2_<?php echo($registro_1['id_indicador_asociado_def'])?>" value="<?php echo($registro_1['resultado_19'])?>"></p>

                                                               <label style="color: orange" for="avance_2018_2_<?php echo($registro_1['id_indicador_asociado_def'])?>"><?php echo($registro_1['unidad'])?></label>

                                                             
                                                            </tr>

                                                             <tr>
                                                              <br>

                                                              <p style=" text-align:right ;padding: 3px 10px; ">
                                                                
                                                                  <label style="text-align: right; font-size: 17px; font-family: Graphik-Bold">Ejemplo 2 <span class="glyphicon glyphicon-info-sign" style="  font-size:20px;color:orange;text-shadow:2px 2px 4px #000000;" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#ejemplo_2"></span></label>
                                                              </p>

                                                              <p><label for="avance_meta_planeada_def" id="error_avance_meta_planeada_def"  >Meta.- Nivel de desempeño esperado del indicador, planteado para el año corriente de acuerdo a su ficha. <strong class="tamanio_caracteres" id="tamanio_caracteres"> Ejemplo: Porcentaje de Cobertura en Educación Preescolar. Meta 2018: 75%</strong></label>
                                                              <input   maxlength="15" minlength="0" value="<?php echo($registro_1['meta_2019'])?>" name="avance_meta_planeada_def" disabled="true"   id="meta_2018_<?php echo($registro_1['id_indicador_asociado_def'])?>"></input></p> 
                                                            </tr>
                                                            <!--

                                                            <tr>


                                                              <br>
                                                              <label for="tendencia_esperada_def">La tendencia esperada del indicador es:</label>
                                                              <div class="select">
                                                                <select  name="tend_esperada_<?php echo($registro_1['id_indicador_asociado_def'])?>" id="tend_esperada_<?php echo($registro_1['id_indicador_asociado_def'])?>">
                                                                  <option id="tend_acs" value="Ascendente">Ascendente</option>
                                                                  <option id="tend_des" value="Descendente">Descendente</option>
                                                                  <option id="tend_est" value="Estable">Estable</option>
                                                                </select>
                                                              </div>
                                                            </tr>

                                                          -->


                                                           
                                                            <tr>
                                                              <br>
                                                              <p><label for="avance_meta_planeada_def" id="error_avance_meta_planeada_def"  >Su avance respecto a la meta 2019 (la proporcion respecto a la meta), es:<strong class="tamanio_caracteres" id="tamanio_caracteres"> </strong></label>
                                                              <input   maxlength="15" minlength="0" value="<?php echo($registro_1['avance_meta_planeada_def'])?>" name="avance_meta_planeada_def"    id="avance_meta_planeada_def_<?php echo($registro_1['id_indicador_asociado_def'])?>"></input></p> 

                                                              
                                                            </tr>

                                                            
                                                            <br>

                                                            <tr>
                                                              <input style="background-color: #555555;color: white;" type="button" id="submit_indicador" onclick="SubmitFormData_5_(<?php echo $registro['id_indicador_asociado_def'];?>)"
                                                               value="Actualizar Informacion" />  
                                                            </tr>
                                                            <br>

                                                          </tbody>
                                                        </table>

                                                              <div class="container">
                                                                <div class="modal fade" id="myModal" role="dialog">
                                                                  <div class="modal-dialog modal-lg">
                                                                    <div class="modal-content">
                                                                      <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h4 class="modal-title">EJEMPLO</h4>
                                                                      </div>
                                                                      <div class="modal-body">
                                                                        <p>L cantidad de informacion.....</p>
                                                                      </div>
                                                                      <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                                      </div>
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                              </div>

                                                              <div class="container">
                                                                <div class="modal fade" id="ejemplo_2" role="dialog">
                                                                  <div class="modal-dialog modal-lg">
                                                                    <div class="modal-content">
                                                                      <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h4 class="modal-title">EJEMPLO 2"</h4>
                                                                      </div>
                                                                      <div class="modal-body">
                                                                        <p>L cantidad de informacion 2.....</p>
                                                                      </div>
                                                                      <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                                      </div>
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                              </div>


                                                        <script>


                                                          $( "#avance_2018_2_<?php echo($registro_1['id_indicador_asociado_def'])?>" ).keyup(function() 
                                                          {
                                                            var avance_2018_2 =document.getElementById("avance_2018_2_<?php echo($registro_1['id_indicador_asociado_def'])?>");
                                                            var meta_2018 =document.getElementById("meta_2018_<?php echo($registro_1['id_indicador_asociado_def'])?>");


                                                            
                                                           var avance_2018_2_val = avance_2018_2.value.replace("%"," ");
                                                           var  meta_2018_val = meta_2018.value.replace("%"," ");
                                                          
                                                           var tipo="<?php echo($registro_1['tendencia_esperada_def'])?>";
                                                           var resp_meta;
                                                           
                                                           

                                                           if(tipo=="Ascendente")      //ACENDENTE
                                                           {

                                                            resp_meta=((avance_2018_2_val/meta_2018_val)*100).toFixed(2);

                                                             document.getElementById("avance_meta_planeada_def_<?php echo($registro_1['id_indicador_asociado_def'])?>").value = resp_meta+"%";
                                                             //console.log("Ascendente");

                                                           }
                                                           else if(tipo=="Descendente") //DECENDENTE
                                                           {
                                                            //resp_meta=(Math.abs(((avance_2018_2_val/meta_2018_val)-100)) ).toFixed(2);
                                                            resp_meta=1-((avance_2018_2_val-meta_2018_val)/meta_2018_val);

                                                            resp_meta=(resp_meta*100).toFixed(2);

                                                            document.getElementById("avance_meta_planeada_def_<?php echo($registro_1['id_indicador_asociado_def'])?>").value = resp_meta+"%";
                                                            //console.log("Desendente");

                                                           }
                                                           else if(tipo=="Estable") //ESTABLE
                                                           {

                                                            resp_meta=((1-(((meta_2018_val-avance_2018_2_val)/meta_2018_val)))*100).toFixed(2);
                                                            document.getElementById("avance_meta_planeada_def_<?php echo($registro_1['id_indicador_asociado_def'])?>").value = resp_meta+"%";
                                                            //console.log("Estable");
                                                           }
                                                           

                                                          
                                                          });



                                                         function SubmitFormData_5_(valor_entra) 
                                                            {

                                                          /*
                                                            var tendencia_esperada=$("#tend_esperada_"+valor_entra).val();
                                                            var linea_base=$("#linea_base_absolutos_def_"+valor_entra).val();
                                                            */
                                                            //var repo_1er_informe=$("#rep_prim_inf_"+valor_entra).val();
                                                            


                                                            //METa_2019
                                                           var avance_2018=$("#meta_2018_"+valor_entra).val();

                                                           //RESULTADO_19
                                                           var  avance_2018_2_=$("#avance_2018_2_"+valor_entra).val();

                                                           var  avance_repecto_meta=$("#avance_meta_planeada_def_"+valor_entra).val();


                                                            
                                                           $.ajax({
                                                              type: "POST",
                                                              url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                                                              data: {id_indicador:valor_entra,meta_2019:avance_2018,resultado_19:avance_2018_2_,avance_repecto_meta:avance_repecto_meta}
                                                              ,
                                                              success: function(msg){
                                                                
                                                                if(msg=1)
                                                                {
                                                                  alert("Informacion Actualizada");
                                                                }
                                                                
                                                              },
                                                              error: function(XMLHttpRequest, textStatus, errorThrown) {
                                                                 alert( "Error Verifica tu informacion" );
                                                              }
                                                            });
                                                              

                                                            }
                                                         
                                                            /*
                                                            if("<?php echo($registro_1['tendencia_esperada_def'])?>"=="Ascendente")
                                                               {
                                                             
                                                                document.getElementById("tend_esperada_<?php echo($registro_1['id_indicador_asociado_def'])?>").selectedIndex = 0;
                                                               }
                                                          else if("<?php echo($registro_1['tendencia_esperada_def'])?>"=="Descendente")
                                                               {

                                                                document.getElementById("tend_esperada_<?php echo($registro_1['id_indicador_asociado_def'])?>").selectedIndex = 1;

                                                               }

                                                          else if("<?php echo($registro_1['tendencia_esperada_def'])?>"=="Estable")
                                                               {

                                                                document.getElementById("tend_esperada_<?php echo($registro_1['id_indicador_asociado_def'])?>").selectedIndex = 2;
                                                              }
                                                              */

                                                        </script>

                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                            </div>


                                            <div id="Parte_6_<?php echo $registro['id_indicador_asociado_def']; ?>" class="tabcontent_vert">
                                                <div id="global">
                                                  <div id="mensajes">
                                                    <div class="texto">
                                               
                                                      <div id="info_labels_<?php echo $registro['id_indicador_asociado_def'];?>_5">
                                                        <table>
                                                          <col width=20%>
                                                          <col width=80%>
                                                          <tbody>

                                                            <tr>
                                                              <p><label hidden="true" for="edit_check_list_tipo_evidencia_def_<?php echo $registro['id_indicador_asociado_def']; ?>"> ¿Qué tipo de evidencia se presenta?  Seleccione los necesarios<strong class="tamanio_caracteres" id="tamanio_caracteres"> Puedes seleccionar mas de uno</strong></label></p>
                                                              <div hidden="true" id="edit_check_list_tipo_evidencia_def_<?php echo $registro['id_indicador_asociado_def']; ?>"></div>

                                                              <script>

                                                                var t_evidencia = 
                                                                ["Acta, Convenios o minuta",
                                                                   "Diagnostico",
                                                                   "Padrones o bases de datos",
                                                                   "Publicación en físico o electrónico",
                                                                   "Reporte",
                                                                   "Otro"];
                                                                var text = "";
                                                                var i;
                                                                text+="<ul>"
                                                                for (i = 0; i < t_evidencia.length; i++) 
                                                                {

                                                                  text += '<label class="checkbox-inline ">'+'<input  type="checkbox"  class="check_load_<?php echo $registro['id_indicador_asociado_def']; ?>" class="check_type_evi_edit_<?php echo $registro['id_indicador_asociado_def'];?>" id="edit_check_evi_'+t_evidencia[i]+'_'+<?php echo $registro['id_indicador_asociado_def'];?>+'"  name="'+t_evidencia[i]+'">'+t_evidencia[i]+'</label>';
                                                                }

                                                                text+="</ul>"
                                                                document.getElementById("edit_check_list_tipo_evidencia_def_<?php echo $registro['id_indicador_asociado_def']; ?>").innerHTML = text;
                                                              


                                                                var check_bd_select="<?php echo $registro['tipo_evidencia_def']; ?>";
                                                                

                                                                var array = check_bd_select.split(",");

                                                                var arr_checks_false = document.getElementsByClassName("check_load_<?php echo $registro['id_indicador_asociado_def']; ?>");


                                                                for (i = 0; i < array.length; i++) 
                                                                {
                                                                
                                                                  if(array[i]=="Acta" || array[i]==" Convenios o minuta" )
                                                                      {
                                                                        //alert("es un acta o convenio");
                                                                      arr_checks_false[0].checked = true;

                                                                      }

                                                                  else if(array[i]=="Diagnostico"  )
                                                                      {
                                                                        //alert("es un Diagnostico");
                                                                        arr_checks_false[1].checked = true;
                                                                      }

                                                                  else if(array[i]=="Padrones o bases de datos"  )
                                                                      {
                                                                       // alert("Padrones o bases de datos");
                                                                        arr_checks_false[2].checked = true;
                                                                      }

                                                                  else if(array[i]=="Publicación en físico o electrónico"  )
                                                                      {
                                                                        //alert("Publicación en físico o electrónico");
                                                                        arr_checks_false[3].checked = true;
                                                                      }

                                                                  else if(array[i]=="Reporte"  )
                                                                      {
                                                                        //alert("Reporte");
                                                                        arr_checks_false[4].checked = true;
                                                                      }
                                                                  else if(array[i]=="Otro"  )
                                                                      {
                                                                        //alert("Otro");
                                                                        arr_checks_false[5].checked = true;
                                                                      }

                                                                
                                                                }

                                                              </script>

                                                              <div hidden="true" id="tipo_evidencia_otro_<?php echo $registro['id_indicador_asociado_def'];?>">
                                                                  <p><label hidden="true" for="tipo_evidencia_otro_def">Definelo </label>
                                                                  <input hidden="true" name="tipo_evidencia_otro_def" id="tipo_evidencia_otro_def_<?php echo $registro['id_indicador_asociado_def'];?>" type="text" ></p>
                                                              </div>

                                                              <input hidden="true"  name="edit_tipo_evidencia_def_list_<?php echo $registro['id_indicador_asociado_def'];?>" id="edit_tipo_evidencia_def_list_<?php echo $registro['id_indicador_asociado_def'];?>" type="hidden" >

                                                              <script >

                                                                $('#tipo_evidencia_otro_<?php echo $registro['id_indicador_asociado_def'];?>').hide();


                                                                    $('#edit_check_evi_Otro_<?php echo $registro['id_indicador_asociado_def'];?>').change(function()
                                                                    
                                                                      { 
                                                                       
                                                                           var check_otros = document.getElementById("edit_check_evi_Otro_<?php echo $registro['id_indicador_asociado_def'];?>");
                                                                    
                                                                          if(check_otros.checked == true)
                                                                          {
                                                                             $('#tipo_evidencia_otro_<?php echo $registro['id_indicador_asociado_def'];?>').show();
                                                                          }
                                                                          else
                                                                          {
                                                                            $('#tipo_evidencia_otro_<?php echo $registro['id_indicador_asociado_def'];?>').hide();

                                                                          }

                                                                      });

                                                                function tipo_evidencia_get_values_edit(valor_entra)
                                                                {              
                                                                            
                                                                  var arr_checks = document.getElementsByClassName("check_load_"+valor_entra);
                                                                  var tipo_evidencia_def="";

                                                                    for (i = 0; i < arr_checks.length; i++) 
                                                                  {

                                                                    if(arr_checks[i].checked == true)
                                                                    {
                                                                      tipo_evidencia_def+=arr_checks[i].name+",";
                                                                    
                                                                    }

                                                                  }
                                                                  //alert(tipo_evidencia_def);

                                                                 var tipo_evi_Edit_select =document.getElementById("edit_tipo_evidencia_def_list_"+valor_entra);
                                                       

                                                                 tipo_evi_Edit_select.value= tipo_evidencia_def;
                                                                 
                                                                 //volver los check a uncheked

                                                                  var arr_checks_false = document.getElementsByClassName("check_type_evi_edit");
                                                                  

                                                                    for (i = 0; i < arr_checks_false.length; i++) 
                                                                  {
                                                                    arr_checks_false[i].checked = false;

                                                                  }
                                                                 
                                                                 

                                                              
                                                                }

                                                               


                                                              </script>

                                                              <br>
                                                            </tr>

                                                            <tr>
                                                              <p><label hidden="true" id="error_desc_doc_vin_<?php echo $registro['id_indicador_asociado_def'];?>" for="descripcion_documento_vinculado_indicador_def">¿Qué contiene la evidencia? Describir qué es el documento y por qué es una evidencia vinculada al indicador.<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>

                                                              <textarea hidden="true" id="descripcion_documento_vinculado_indicador_def_<?php echo $registro['id_indicador_asociado_def'];?>" maxlength="450" name="descripcion_documento_vinculado_indicador_def" placeholder="¿Qué contiene la evidencia? Describir qué es el documento y por qué es una evidencia vinculada al indicador."   ><?php echo($registro_1['descripcion_documento_vinculado_indicador_def'])?></textarea></p> 

                                                            </tr>

                                                            <tr>
                                                              <br>
                                                              <label hidden="true" for="tipo_evidencia_fecha_def"> La fecha de la evidencia es:</label>
                                                              <div hidden="true" class="select">
                                                                <select  hidden="true" name="tipo_evidencia_fecha_def" id="tipo_evidencia_fecha_def_<?php echo($registro_1['id_indicador_asociado_def'])?>" >
                                                                  <option value="Corte 30 Abril">Corte 30 Abril</option>
                                                                  <option value="Corte 30 Julio">Corte 30 Julio</option>
                                                                  
                                                                </select>
                                                              </div>   
                                                            </tr>

                                                            <tr>
                                                              <br>
                                                              <label hidden="true" for="existe_expediente_doc_def">Existe expediente documental</label>
                                                              <div class="select">
                                                                <select hidden="true" name="existe_expediente_doc_def" id="existe_expediente_doc_def_<?php echo $registro['id_indicador_asociado_def'];?>">
                                                                  <option value="No" >No</option>
                                                                  <option value="Si">Si</option>
                                                                </select>
                                                              </div>
                                                            </tr>
                                                           
                                                            <tr>
                                                              <div hidden="true" id="parte1_<?php echo($registro_1['id_indicador_asociado_def'])?>">
                                                                <p><label hidden="true" for="serie_evidencia_def">Serie de la Evidencia</label><input hidden="true" id="serie_evidencia_def_<?php echo($registro_1['id_indicador_asociado_def'])?>" name="serie_evidencia_def" placeholder="Serie de la Evidencia" value="<?php echo($registro_1['serie_evidencia_def'])?>" ></p>

                                                                <p><label hidden="true" for="seccion_def">Sección(opcional)</label><input hidden="true"   name="seccion_def" id="seccion_def_<?php echo($registro_1['id_indicador_asociado_def'])?>" placeholder="sección (opcional)" value="<?php echo($registro_1['seccion_def'])?>" ></p>

                                                                <p><label hidden="true" for="guia_invetario_expediente_def">Guía/Inventario en el que se incluye el expediente</label><input hidden="true"  name="guia_invetario_expediente_def" id="guia_invetario_expediente_def_<?php echo($registro_1['id_indicador_asociado_def'])?>" placeholder="Guía/Inventario en el que se incluye el expediente" value="<?php echo($registro_1['guia_invetario_expediente_def'])?>" ></p>
                                                              </div>

                                                              <div hidden="true" id="parte2_<?php echo($registro_1['id_indicador_asociado_def'])?>">
                                                              
                                                                <p><label hidden="true" for="medios_verificacion_evidencia" id="error_medios_comunicacion_<?php echo($registro_1['id_indicador_asociado_def'])?>">Define cuál es la ubicación electrónica o física de la evidencia o cómo es posible rastrearla<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label><textarea hidden="true" id="medios_verificacion_evidencia_<?php echo($registro_1['id_indicador_asociado_def'])?>" maxlength="350" name="medios_verificacion_evidencia_<?php echo($registro_1['id_indicador_asociado_def'])?>" placeholder="Define cuál es la ubicación electrónica o física de la evidencia o cómo es posible rastrearla" ><?php echo($registro_1['medios_verificacion_evidencia'])?><?php echo($registro_1['medios_verificacion_evidencia_def'])?></textarea></p> 

                                                              </div>
                                                            </tr>

                                                            <tr>
                                                               <br>
                                                               <p><label hidden="true" id="error_comentarios_<?php echo($registro_1['id_indicador_asociado_def'])?>" for="comentarios_def">Comentarios <strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label><textarea hidden="true" id="comentarios_def_<?php echo($registro_1['id_indicador_asociado_def'])?>" maxlength="450" name="comentarios_def" placeholder="Comentarios" ><?php echo($registro_1['comentarios_def'])?></textarea></p> 
                                                            </tr>
                                                            <tr>
                                                              
                                                               <h3>Adjuntar evidencias</h5>
                                                              <br>
                                                              <p><label for="edit_upload_adjuntar_evidencias_<?php echo $registro['id_indicador_asociado_def']; ?>">Puedes seleccionar multiples archivos</label></p>
                                                              <div style="text-align:center;" id="edit_upload_adjuntar_evidencias_<?php echo $registro['id_indicador_asociado_def']; ?>" name="edit_upload_adjuntar_evidencias_<?php echo $registro['id_indicador_asociado_def']; ?>">  </div>
                                                              <br>
                                                            
                                                            </tr>

                                                            <tr>
                                                              <input hidden="true" style="background-color: #555555;color: white;" type="button" id="submit_indicador" onclick="SubmitFormData_6(<?php echo $registro['id_indicador_asociado_def'];?>)"
                                                               value="Actualizar Informacion" />  
                                                            </tr>
                                                            
                                                            <br>

                                                          </tbody>
                                                        </table>
                                                        <script>



                                                          $('#parte1_<?php echo($registro_1['id_indicador_asociado_def'])?>').hide();
                                                          //load datos  corte
                                                         if("<?php echo($registro_1['tipo_evidencia_fecha_def'])?>"=="Corte 30 Abril")
                                                          {
                                                             
                                                            document.getElementById("tipo_evidencia_fecha_def_<?php echo($registro_1['id_indicador_asociado_def'])?>").selectedIndex = 0;
                                                           }
                                                          else if("<?php echo($registro_1['tipo_evidencia_fecha_def'])?>"=="Corte 30 Julio")
                                                           {

                                                            document.getElementById("tipo_evidencia_fecha_def_<?php echo($registro_1['id_indicador_asociado_def'])?>").selectedIndex = 1;
                                                           }

                                                           if("<?php echo($registro_1['existe_expediente_doc_def'])?>"=="Si")
                                                          {
                                                             
                                                            document.getElementById("existe_expediente_doc_def_<?php echo $registro['id_indicador_asociado_def'];?>").selectedIndex = 1;

                                                            $('#parte1_<?php echo($registro_1['id_indicador_asociado_def'])?>').show();

                                                            $('#parte2_<?php echo($registro_1['id_indicador_asociado_def'])?>').hide();

                                                           }
                                                           else if("<?php echo($registro_1['existe_expediente_doc_def'])?>"=="No")
                                                           {

                                                            document.getElementById("existe_expediente_doc_def_<?php echo $registro['id_indicador_asociado_def'];?>").selectedIndex = 0;

                                                            $('#parte1_<?php echo($registro_1['id_indicador_asociado_def'])?>').hide();

                                                            $('#parte2_<?php echo($registro_1['id_indicador_asociado_def'])?>').show();
                                                           }

                                                          $('#existe_expediente_doc_def_<?php echo $registro['id_indicador_asociado_def'];?>').change(function()
                                                          { 

                                                            if($(this).val() == "Si")
                                                            {

                                                            $('#parte1_<?php echo($registro_1['id_indicador_asociado_def'])?>').show();

                                                            $('#parte2_<?php echo($registro_1['id_indicador_asociado_def'])?>').hide();
                                                            

                                                            }
                                                            if($(this).val() == "No"){

                                                            $('#parte1_<?php echo($registro_1['id_indicador_asociado_def'])?>').hide();

                                                            $('#parte2_<?php echo($registro_1['id_indicador_asociado_def'])?>').show();
                                                           
                                                            }
                                                          });


                                                        $(document).ready(function() 
                                                          {

                                                           // if("<?php echo ($user_name); ?>"=="Brian")
                                                            

                                                              

                                                              

                                                             $("#edit_upload_adjuntar_evidencias_<?php echo $registro['id_indicador_asociado_def']; ?>").uploadFile
                                                              ({



                                                                  url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                                                                  fileName:"myfile",
                                                                  maxFileCount:60,
                                                                  showCancel: true,
                                                                  showDone: true,
                                                                  showError: true,
                                                                  maxFileSize:1000*600000,
                                                                  showPreview:true,
                                                                  previewHeight: "100px",
                                                                  previewWidth: "100px",
                                                                  formData: {"nom_evidencia":"<?php echo($registro_1['id_indicador_asociado_def'])?>","usuario":"<?php echo $user_name;?>","indice":"edit"},

                                                                  onSuccess:function(files,data,xhr,pd)
                                                                  {

                                                                    var id_sl = <?php echo $registro['id_indicador_asociado_def']; ?>;
                                                                    var status = 0;

                                                                      $.ajax({
                                                                        type: "POST",
                                                                        url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                                                                        data: {id_indicador:id_sl,status:status}
                                                                        ,
                                                                      success: function(msg){
                                                                        //console.log(msg);
                                                                        if(msg==1)
                                                                        {
                                                                          alert("Archivo subido correctamente");
                                                                        }
                                                                          
                                                                        },
                                                                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                                                                           alert( "Error Verifica tu informacion" );
                                                                        }
                                                                      });
                                                                      //alert("arcivo subido");
                                                                  }
                                                                
                                                              }); 
                                                           // }


                                                          });


                                                         function SubmitFormData_6(valor_entra) 
                                                            {


                                                             
                                                            tipo_evidencia_get_values_edit(valor_entra);

                                                            var tipo_evidencia_list=$("#edit_tipo_evidencia_def_list_"+valor_entra).val();
                                                            var otro_tipo_evidencia=$("#tipo_evidencia_otro_def_"+valor_entra).val();
                                                            var desc_doc_vinculado_ind=$("#descripcion_documento_vinculado_indicador_def_"+valor_entra).val();
                                                            
                                                            var fecha_corte=$("#tipo_evidencia_fecha_def_"+valor_entra).val();


                                                            var existe_exp_doc=$("#existe_expediente_doc_def_"+valor_entra).val();

                                                            var serie_evidencia=$("#serie_evidencia_def_"+valor_entra).val();
                                                            var seccion=$("#seccion_def_"+valor_entra).val();
                                                            
                                                            var guia_inventario=$("#guia_invetario_expediente_def_"+valor_entra).val();

                                                            var medios_verificacion=$("#medios_verificacion_evidencia_"+valor_entra).val();

                                                            var comentarios=$("#comentarios_def_"+valor_entra).val();


                                                            var error_edit_doc_vin= document.getElementById("error_desc_doc_vin_"+valor_entra);

                                                            var error_medios_com= document.getElementById("error_medios_comunicacion_"+valor_entra);

                                                            var error_coment= document.getElementById("error_comentarios_"+valor_entra);

                                                              if(desc_doc_vinculado_ind.length>450)
                                                              {
                                                                error_edit_doc_vin.innerHTML ="El limite son 450 caracteres"; /*editado Robert*/
                                                                error_edit_doc_vin.classList.add("error_mensaje");

                                                              }
                                                              else if(medios_verificacion.length>350)
                                                              {
                                                                error_medios_com.innerHTML ="El limite son 350 caracteres"; /*editado Robert*/ 
                                                                error_medios_com.classList.add("error_mensaje");
                                                              }

                                                              else if(comentarios.length>450)
                                                              {
                                                                error_coment.innerHTML ="El limite son 450 caracteres"; /*editado Robert*/ 
                                                                error_coment.classList.add("error_mensaje");
                                                              }


                                                              else
                                                              {

                                                                  $.ajax({
                                                                  type: "POST",
                                                                  url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                                                                  data: {id_indicador:valor_entra,tipo_evidencia_list:tipo_evidencia_list,otro_tipo_evidencia:otro_tipo_evidencia,desc_doc_vinculado_ind:desc_doc_vinculado_ind,fecha_corte:fecha_corte,existe_exp_doc:existe_exp_doc,serie_evidencia:serie_evidencia,seccion:seccion,guia_inventario:guia_inventario,medios_verificacion:medios_verificacion,comentarios:comentarios}
                                                                  ,
                                                                  success: function(msg){
                                                                    
                                                                    if(msg=1)
                                                                    {
                                                                      alert("Informacion Actualizada");
                                                                    }
                                                                    
                                                                  },
                                                                  error: function(XMLHttpRequest, textStatus, errorThrown) {
                                                                     alert( "Error Verifica tu informacion" );
                                                                  }
                                                                });

                                                              }



                                                            }

                                                            /*

                                                            if("<?php echo($registro_1['tendencia_esperada_def'])?>"=="Ascendente")
                                                               {
                                                             
                                                                document.getElementById("tend_esperada_<?php echo($registro_1['id_indicador_asociado_def'])?>").selectedIndex = 0;
                                                               }
                                                          else if("<?php echo($registro_1['tendencia_esperada_def'])?>"=="Descendente")
                                                               {

                                                                document.getElementById("tend_esperada_<?php echo($registro_1['id_indicador_asociado_def'])?>").selectedIndex = 1;

                                                               }

                                                          else if("<?php echo($registro_1['tendencia_esperada_def'])?>"=="Estable")
                                                               {

                                                                document.getElementById("tend_esperada_<?php echo($registro_1['id_indicador_asociado_def'])?>").selectedIndex = 2;
                                                                
                                                              }
                                                              */

                                                        </script>

                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                            </div>

                                            <div id="Parte_7_<?php echo $registro['id_indicador_asociado_def']; ?>" class="tabcontent_vert">
                                              <div id="global">
                                                <div id="mensajes">
                                                  <div class="texto">

                                                  <?php global $wpdb;
                                                   $id_indi= $registro['id_indicador_asociado_def'];
                                                   $tipo="acuse";

                                                  $archivos = $wpdb->prefix . 'archivos_dependencias';
                                                  $archivos = $wpdb->get_results("SELECT * FROM $archivos WHERE indicador=$id_indi", ARRAY_A);
                                                    foreach($archivos as $archivo) 
                                                      {?>
                                             
                                                        <div id="info_labels_<?php echo $archivo['indicador'];?>_5">


                                                      <table>
                                                        <col width=1%>
                                                        <col width=29%>
                                                        <col width=20%>
                                                      
                                                        <col width=20%>
                                                        
                                                        <col width=30%>
                                                        
                                                      
                                                        <tbody>
                                                                  <td>
                                                                    
                                                                  </td>
    
                                                                  <td> <p> <label style="text-align: left; font-size: 14px; font-family: Graphik-Bold" ><?php echo($archivo['filename']); ?></label>  </p> </td>
                                                                  <br>
                                                                 

                                                                  <td>

                                                                     <a  class='demo'><a href='pdf_plugin/pdf_evidencias/crear_evidencia_pdf.php?id_evidencia=<?php echo($id_indi);?>&user=0&arr_ids_tacticos=0&tipo=<?php echo($tipo);?>&archivo_name=<?php echo($archivo['id_archivo']);?>' target='_blank' class='demo'><label style="text-align: center; font-size: 17px; font-family: Graphik-Bold"><span class="glyphicon glyphicon-duplicate" style="  font-size:20px;text-shadow:2px 2px 4px #9A9A9A;" ></span>  Acuse </label></a></a>

                                                                    

                                                                  </td>

                                                                  <td>
                                                                 
                                                                  



                                                                     <label onclick="ver_archivo('<?php echo $archivo['ruta_archivo'];?>','<?php echo($archivo['filename']);?>');" style="text-align: center; font-size: 17px; font-family: Graphik-Bold"><span class="glyphicon glyphicon-eye-open" style="  font-size:20px;text-shadow:2px 2px 4px #9A9A9A;" type="button"  class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal_ver"></span>  Ver </label>
                                                                  
                                                                  </td>

                                                                  <td>
                                                                 
                                                                  



                                                                     <label id="coment_status_<?php echo $archivo['id_archivo'];?>" ><?php echo $archivo['comentario_archivo'];?></label>
                                                                  
                                                                  </td>

                              


                                                        </tbody>
                                                      </table>

                                                      <script type="text/javascript">
                                                        if(<?php echo $archivo['estatus'];?>==1)
                                                        {
                                                         document.getElementById("coment_status_"+<?php echo $archivo['id_archivo'];?>).innerHTML="Archivo Aceptado";

                                                         document.getElementById("coment_status_"+<?php echo $archivo['id_archivo'];?>).style.color = "#47BF02";
                                                        }
                                                        else if(<?php echo $archivo['estatus'];?>==0)
                                                        {

                                                        }
                                                        


                                                      </script>
                                                      

                                                    </div>
                                                    <?php }?>
                                                  </div>
                                                </div>
                                              </div>
                                                
                                                             <!--       <div class="modal fade" id="myModal_ver" role="dialog">
                                                                      <div class="modal-dialog">
                                                                      
                                                                      
                                                                        <div class="modal-content">
                                                                          <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                            <h4 class="modal-title">Archivo</h4>
                                                                          </div>
                                                                          <div class="modal-body">
                                                                            <p>Nombre del archivo</p>
                                                                          </div>

                                                                         
                                                                          <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                                          </div>
                                                                        </div>
                                                                        
                                                                      </div>
                                                                    </div>  -->
                                            </div>

                                            
                                            <div id="Parte_8_<?php echo $registro['id_indicador_asociado_def']; ?>" class="tabcontent_vert">
                                                <div id="global">
                                                  <div id="mensajes">
                                                    <div class="texto">

                                                      <div id="info_labels_<?php echo $archivo['indicador'];?>_5">
                                                        <table>
                                                          <col width=30%>
                                                          <col width=70%>
                                                        
                                                          <tbody>
                                                                 <br>
                                                            <tr>
                                                              <td>
                                                                 <label style="text-align: left; color: #4CAF50; font-size: 17px; font-family: Graphik-Bold" >Comentarios del Evaluador:</label>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            
                                                            
                                                                </td>
                                                                <td>
                                                                  <label style="text-align: left;" >"<?php echo($registro_1['coment_indicador'])?>"</label>

                                                            <br>
                                                            <br>
                                                            <br>
                                                                </td>


                                                            </tr>

                                                            <tr>
                                                              <td><label style="text-align: left; color: #4CAF50;  font-size: 17px; font-family: Graphik-Bold" >Enviar un comentario o aclaracion al evaluador:</label></td>

                                                              <td>
                                                                <textarea id="msj_ev<?php echo $archivo['indicador'];?>" rows="8" cols="50"> <?php echo $registro['coment_evidencia']; ?></textarea>
                                                              </td>
                                                              

                                                            </tr>
                                                            <tr>
                                                              <td>
                                                                
                                                              </td>
                                                              <td style="text-align: left;">
                                                                <button onclick="msj_to_eval(<?php echo $registro['id_indicador_asociado_def']; ?>)" style="text-align: left;  background-color: white; color: black; border: 2px solid #4CAF50; /* Green */">Enviar</button>
                                                              </td>

                                                            </tr>

                                                          </tbody>
                                                        </table>
                                                        

                                                      </div>
                                                      
                                                    </div>
                                                  </div>
                                                </div>
                                            </div>
                                            


                                            <div id="Parte_9_<?php echo $registro['id_indicador_asociado_def']; ?>" class="tabcontent_vert">
                                                <div id="global">
                                                  <div id="mensajes">
                                                    <div class="texto">

                                                      <div id="info_labels_<?php echo $archivo['indicador'];?>_5">

                                                        <table>
                                                        <col width=5%>
                                                        <col width=65%>
                                                        <col width=30%>
                                                        <br>
                                                        <br>
                                                          <tbody>
                                                              <?php global $wpdb;
                                                              $id=$registro['id_indicador_asociado_def'];

                                                                $evidencias = $wpdb->prefix . 'definicion_evidencias';
                                                                $registros_1 = $wpdb->get_results("SELECT * FROM $evidencias WHERE id_indicador_asociado_def = $id", ARRAY_A);
                                                                foreach($registros_1 as $registro_1) 
                                                                  { ?>
                                                                    <?php $id_evidencia=$registro_1['id_indicador_asociado_def']; 
                                                                    $tipo="tactico";
                                                                    ?>

                                                                      <tr>
                                                                        <td>
                                                                          
                                                                          <img style="text-align: center" src="http://sigeh.hidalgo.gob.mx/repositorio/wp-content/uploads/2018/04/PDF.png" height="42" width="30" ></img>
                                                                        </td>
                                                                         <td id="txt_list_pdf">
                                                                          <div>
                                                                               <p class='demo'><a href='pdf_plugin/pdf_evidencias/crear_evidencia_pdf.php?id_evidencia=<?php echo($id)?>&user=<?php echo($user_name)?>&arr_ids_tacticos=0&tipo=<?php echo($tipo)?>' target='_blank' class='demo'>Reporte</a></p>
                                                                          </div>

                                                                         </td> 

                                                                         <td id="contenido_datos" >   

                                                                      
                                                                       </td>
                                                                  <?php } ?> 
                                                          </tbody>
                                                        
                                                        </table>
                                                        

                                                      </div>
                                                      
                                                    </div>
                                                  </div>
                                            </div>
                                          </div><!-- Panel -->

                                       <!-- </form>-->
                                      </div>
                                    <?php  
                                  } ?>                    

                                </div>

                                  <script type="text/javascript">//Desactivar los q esten en proceso
                                  /*
                                          var btn_ac_des = "<?php echo ($btn_ac_des); ?>"; 
                                          if(btn_ac_des=="0" )
                                          {
                                            
                                            //alert("acordeon_id_<?php echo($registro['id_indicador_asociado_def'])?>");
                                            var button_add_deisabled = document.getElementById("acordeon_id_<?php echo($registro['id_indicador_asociado_def'])?>");
                                            button_add_deisabled.setAttribute("disabled", false);
                                          }
                                          */
                                  </script>
                              </div>

                              <div id="REPORTES_TACTICOS" class="tabcontent">

                                 <br>
                                <h3>INDICADORES TACTICOS</h3>
                                <br>



                                <!--
                                <table>
                                  <col width=5%>
                                  <col width=95%>
                                  
                                  <br>
                                  <br>
                                    <tbody>
                                        <?php global $wpdb;
                                          $evidencias = $wpdb->prefix . 'definicion_evidencias';
                                          $registros_1 = $wpdb->get_results("SELECT * FROM $evidencias WHERE nombre_dependencia_def ='$user_name'", ARRAY_A);
                                          foreach($registros_1 as $registro_1) 
                                            { ?>
                                              <?php $id_evidencia=$registro_1['id_indicador_asociado_def']; 
                                              $tipo="tactico";
                                              $arr_ind_dep=$registro_1['id_indicador_asociado_def'].",".$arr_ind_dep;
                                              ?>

                                                
                                                  
                                            <?php } ?> 

                                            <td>
                                                    
                                                    <img style="text-align: center" src="http://p-sigeh.hidalgo.gob.mx/repositorio/wp-content/uploads/2018/04/PDF.png" height="42" width="30" ></img>
                                                  </td>
                                                   <td id="txt_list_pdf">
                                                    <div>
                                                         <p class='demo'><a href='pdf_plugin/pdf_evidencias/crear_evidencia_pdf.php?id_evidencia=<?php echo($arr_ind_dep) ?>&user=$user_name&arr_ids_tacticos=0&tipo=$tipo' target='_blank' class='demo'>Reporte</a></p>
                                                    </div>

                                                   </td> 

                                                   
                                    </tbody>
                                  
                                </table>

                              -->

                               <table>
                                    <col width=5%>
                                    <col width=65%>
                                    <col width=30%>
                                    <br>
                                    <br>
                                      <tbody>
                                          <?php global $wpdb;
                                            $evidencias = $wpdb->prefix . 'definicion_evidencias';
                                            $registros_1 = $wpdb->get_results("SELECT * FROM $evidencias WHERE nombre_dependencia_def ='$user_name'", ARRAY_A);
                                            foreach($registros_1 as $registro_1) 
                                              { ?>
                                                <?php $id_evidencia=$registro_1['id_indicador_asociado_def']; 
                                                $tipo="tactico";
                                                ?>

                                                  <tr>
                                                    <td>
                                                      
                                                      <img style="text-align: center" src="http://sigeh.hidalgo.gob.mx/repositorio/wp-content/uploads/2018/04/PDF.png" height="42" width="30" ></img>
                                                    </td>
                                                     <td id="txt_list_pdf">
                                                      <div>
                                                         <?php    echo("<p class='demo'><a href='pdf_plugin/pdf_evidencias/crear_evidencia_pdf.php?id_evidencia= $id_evidencia&user=$user_name&arr_ids_tacticos=0&tipo=$tipo' target='_blank' class='demo'>".$registro_1['nombre_indicador_asociado_def']."</a></p>");?>
                                                      </div>

                                                     </td> 

                                                     <td id="contenido_datos" >   
                                                      
                                                      
                                                      
                                                      <?php echo(  $registro_1['fecha_registro']);?>
                                                  
                                                   </td>
                                              <?php } ?> 
                                      </tbody>
                                    
                                  </table>

                              </div>

                            </div>
                              <!-- FIN PESTANAS-->

                          <!-- FIN INDICADOR TACTICOS-->

                          <div id="acciones_concurrentes_sin_indicadores">
                            <!-- Tab_est links -->
                            <div class="container">
                              <?php get_template_part('templates/formulario', 'resultado-inversion'); ?>
                            </div>
                          </div>

                          <div id="avance_programatico">
                                <!-- Tab_est links -->
                            <div class="container">
                              <?php get_template_part('templates/formulario', 'avance_programatico'); ?>
                            </div>
                          </div>

                          <div id="acciones_concurrentes">
                                <!-- Tab_est links -->
                            <div class="container">
                              <?php get_template_part('templates/formulario', 'acciones-concurrentes'); ?>
                            </div>

                          </div>

                          <!-- INDICADORES ESTRATEGICO-->
                              <!-- TABS Robert-->
                          <div id="estrategicos" style="display: block;">
                            <!-- Tab_est links -->
                            <div class="tab_menu" style="display: block;">
                              <button id="btn_est_reg" disabled="true" class="tablinks_est" onclick="open_tab_est(event, 'EVIDENCIA_EST')"  >DEFINIR EVIDENCIA</button>
                              <button id="btn_est_edit" disabled="true" class="tablinks_est" onclick="open_tab_est(event, 'ESTATUS_EST')">ESTATUS</button>
                              <button id="btn_est_repdf" disabled="true" class="tablinks_est" onclick="open_tab_est(event, 'REPORTES_EST_pdf') ">REPORTES</button>
                            </div>

                            <div id="intro_idicador_testrategico" class="container" >
                                <p  ><font text-align: center; font-family: Graphik-Bold  color="green" size="5">INDICADOR ESTRATEGICO</font></p>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>

                            </div>
             

                            <!-- Tab_est content -->
                              <div id="EVIDENCIA_EST" class="tabcontent_est">
                                <div class="container">
                                  <form id="" method="post">
                                    <br>
                                    <p><font text-align: center; font-family: Graphik-Bold  color="green" size="5">INDICADOR ESTRATEGICO</font>
                                    <!-- One "tab_est" for each step_est in the form: -->
                                    <div class="tab_est">INSTRUCCIONES:
                                      <br>
                                      <br>
                                      <br>
                                        <li>Favor de llenar todos los campos sin abreviaturas</li>
                                        <li>Usar letras mayúsculas y minúsculas</li>
                                        <li>Se recomienda tener  a la mano la ficha del indicador, informe de gobierno y evidencias</li>
                                      <br>
                                      <br>
                                    </div>

                                    <div class="tab_est">INDICADOR ESTRATEGICO:
                                    <!-- Robert -->
                                      <br>
                                      <br>
                                       <p><label id="error_est_nom_indicador"  for="est_nom_indicador">Nombre del indicador:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                      <p><input placeholder="Nombre del indicador" maxlength="350" name="est_nom_indicador" id="est_nom_indicador" ></p>
                                      <br>
                                      <div id="indicadore_est_seleccionado_autollenado"> 


                                        <p><label id="error_est_descrip_inidcador"  for="est_descrp_indicador">Descripcion del indicador<strong class="tamanio_caracteres" id="tamanio_caracteres">(450 caracteres)</strong></label>
                                        <textarea id="est_descrp_indicador" maxlength="450" name="est_descrp_indicador" placeholder="Descripcion del indicador"></textarea>
                                        <br> 

                                        <p><label id="est_error_linea_discursiva_1"  for="estr_linea_discursiva_1">Linea discursiva 1<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                                        <textarea id="estr_linea_discursiva_1" maxlength="450" name="estr_linea_discursiva_1" placeholder="Linea discursiva 1"></textarea>
                                        <br>

                                      
                                        <p><label id="est_error_linea_discursiva_2"  for="estr_linea_discursiva_2">Linea discursiva 2<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                                        <textarea id="estr_linea_discursiva_2" maxlength="450" name="estr_linea_discursiva_2" placeholder="Linea discursiva 2"></textarea>
                                        <br>

                                        <p><label id="est_error_linea_discursiva_3"  for="estr_linea_discursiva_3">Linea discursiva 3<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                                        <textarea id="estr_linea_discursiva_3" maxlength="450" name="estr_linea_discursiva_3" placeholder="Linea discursiva 3"></textarea>
                                        <br>

                                   



                                        <p><label id="error_est_hubo_logro"  for="est_hubo_logro">Hubo logros premio y/o reconocimiento</label>
                                        <div class="select">
                                          <select name="est_hubo_logro" id="est_hubo_logro" >  
                                            <option value="No" >No</option>
                                            <option value="Si" >Si</option>
                                          </select>
                                        </div>

                                        <div id="div_est_log">
                                          <p><label id="est_error_bullet_logro"  for="est_bullet_logro">Bullet del logro<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                                          <textarea id="est_bullet_logro" maxlength="450" name="est_bullet_logro" placeholder="Escribir el bullet del logro:"   ></textarea>
                                        </div>
                                        <br>

                                      
                                        <!--
                                        <p><label id="error_est_logros_asoc_a_este_ind"  for="est_logro_asoc_ind">Este Este indicador esta ligado a un premio o reconocimiento para el estado</label>
                                        <div class="select">
                                          <select name="est_logro_asoc_ind" id="est_logro_asoc_ind" >  
                                            <option value="No" >No</option>
                                            <option value="Si" >Si</option>
                                          </select>
                                        </div>

                                        <div id="div_est_descripcion_logro">
                                            
                                            <p><label id="error_est_descripcion_logro"  for="est_descripcion_logro">Descripcion del premio o reconocimiento<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                          <textarea id="est_descripcion_logro" maxlength="350" name="est_descripcion_logro" placeholder="Descripcion del premio o reconocimiento"   ></textarea>
                                          <br>
                                        </div>
                                        <br>
                                      -->


                                       
                                        </div><!-- fin autollenado-->
                                    </div>

                                    <div class="tab_est">OBRAS Y ACCIONES:

                            
                                      <br><br>
                                    
                                      <p><label id="est_error_resultado_1"  for="est_resultado1">Obras y acciones 1:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                      <textarea class="textbox_corto" maxlength="350" id="est_resultado1" name="est_resultado1" placeholder="Obras y acciones 1:"></textarea>
                                      
                                      <button id="est_btn_def_lugar_1"  onclick="open_lugares(1,0,1)" type="button" >Definir lugar 1</button></p>
                                      <label id="est_datos_cargados_lug_1" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                      
                                      <input type="hidden" id="est_res1_lugares" name="est_res1_lugares" hidden="true">

                                      <input type="hidden" id="est_res1_lugares_gps" name="est_res1_lugares_gps" hidden="true" >


                                      <p><label id="est_error_resultado_2" id="est_label_resultado_2"  for="est_resultado2">Obras y acciones 2:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                      <textarea class="textbox_corto" maxlength="350" id="est_resultado2" name="est_resultado2" placeholder="Obras y acciones 2:"></textarea>
                                      <button  id="est_btn_def_lugar_2" class="btn_def_lugar" onclick="open_lugares(2,0,1)" type="button" >Definir lugar 2</button></p>
                                      <label id="est_datos_cargados_lug_2" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                      <input type="hidden" id="est_res2_lugares" name="est_res2_lugares" >
                                      <input type="hidden" id="est_res2_lugares_gps" name="est_res2_lugares_gps" >

                                      <p><label id="est_error_resultado_3"  for="est_resultado3">Obras y acciones 3:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                      <textarea class="textbox_corto" maxlength="350" id="est_resultado3" name="est_resultado3" placeholder="Obras y acciones 3:"></textarea>
                                      <button class="btn_def_lugar" id="est_btn_def_lugar_3" onclick="open_lugares(3,0,1)" type="button" >Definir lugar 3</button></p>
                                      <label id="est_datos_cargados_lug_3" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                      <input type="hidden" id="est_res3_lugares" name="est_res3_lugares"  >
                                      <input type="hidden" id="est_res3_lugares_gps" name="est_res3_lugares_gps" >
                                      

                                      <p><label id="est_error_resultado_4" id="est_label_resultado_4"  for="est_resultado4">Obras y acciones 4:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                      <textarea class="textbox_corto" maxlength="350" id="est_resultado4" name="est_resultado4" placeholder="Obras y acciones 4:"></textarea>
                                      <button  id="est_btn_def_lugar_4" class="btn_def_lugar" onclick="open_lugares(4,0,1)" type="button" >Definir lugar 4</button></p>
                                      <label id="est_datos_cargados_lug_4" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                      <input type="hidden" id="est_res4_lugares" name="est_res4_lugares" >
                                      <input type="hidden" id="est_res4_lugares_gps" name="est_res4_lugares_gps" >

                                    

                                      <p><label id="est_error_resultado_5" id="est_label_resultado_5"  for="est_resultado5">Obras y acciones 5:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                      <textarea class="textbox_corto" maxlength="350" id="est_resultado5" name="est_resultado5" placeholder="Obras y acciones 5:"></textarea>
                                      <button  id="est_btn_def_lugar_5" class="btn_def_lugar" onclick="open_lugares(5,0,1)" type="button" >Definir lugar 5</button></p>
                                      <label id="est_datos_cargados_lug_5" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                      <input type="hidden" id="est_res5_lugares" name="est_res5_lugares" >
                                      <input type="hidden" id="est_res5_lugares_gps" name="est_res5_lugares_gps" >

                                      



                                 

                                      <br><br>
   

                                    </div>

                                    <div class="tab_est">META 2019:
                                      <!-- Robert -->
                                      <br>
                                      <p><label id="error_est_linea_base"  for="est_linea_base">Linea base<strong class="tamanio_caracteres" id="tamanio_caracteres"> (10 caracteres)</strong></label><input maxlength="10" placeholder="Linea base" disabled name="est_linea_base" id="est_linea_base"  ></p>
                                      <br>

                                      <p><label id="error_result_2017"  for="result_2017">Meta 2022<strong class="tamanio_caracteres" id="tamanio_caracteres"> (10 caracteres)</strong></label><input maxlength="10" id="edit_est_meta_2022" disabled placeholder="Meta 2022" name="meta_2022"   value=""></p>

                                      <br>
                                      <p><label id="error_meta_2018"  for="meta_2018">Meta 2030<strong class="tamanio_caracteres" id="tamanio_caracteres"> (10 caracteres)</strong></label><input maxlength="10" placeholder="Meta 2030"  name="meta_2030" id="meta_2030" ></p>
                                      <br>

                                      <p><label id="error_est_res_abril"  for="est_res_abril">Resultado  2019<strong class="tamanio_caracteres" id="tamanio_caracteres"> (10 caracteres)</strong></label><input maxlength="10" placeholder="Resultado  2018" name="est_res_abril" id="est_res_abril" ></p>
      

                                      <input type="hidden" name="oculto_ind_est" value="1">
                                      <input type="hidden" name="nom_dependencia" value="<?php echo ($user_name); ?>">



                                    





                                      
                                    </div>

                                    <div class="tab_est">RESULTADOS

                                        <input type="hidden"   name="est_eje_ped" id="est_eje_ped">
                                      <input type="hidden"   name="est_obj_estrategico" id="est_obj_estrategico">
                                      <input type="hidden"    name="est_ind_informe" id="est_ind_informe">
                                    

                                        <br>
                                        <label style="text-align: center color: #4CAF50; font-size: 17px; font-family: Graphik-Bold" >Informacion lista</label>

                                        <h2><font font-family: Graphik-Bold color="green" size="6">ENVIAR INFORMACION</font></h2>
                                    </div>


                                    <div style="overflow:auto;">
                                      <div style="float:right;">
                                        <button type="button" id="prevBtn_est" onclick="nextPrev_est(-1)">Anterior</button>
                                        <button type="button" id="nextBtn_est" name="enviar_form_dindi_estr" onclick="nextPrev_est(1)">Siguiente</button>
                                      </div>
                                    </div>
                                      
                                    
                                  

                                 

                                    <!-- Circles which indicates the step_ests of the form: -->
                                    <div style="text-align:center;margin-top:40px;">
                                      <span class="step_est"></span>
                                      <span class="step_est"></span>
                                      <span class="step_est"></span>
                                      <span class="step_est"></span>
                                      <span class="step_est"></span>
                                      

                                    </div>

                                  </form>
                                </div>
                              </div>





                              <div id="ESTATUS_EST" class="tabcontent_est">
                                <div class="container">

                                  <?php global $wpdb;
                                    $evidencias = $wpdb->prefix . 'indicadores_estrategicos_definidos';
                                    $registros = $wpdb->get_results("SELECT * FROM $evidencias WHERE  dependecia_encargada='$user_name'", ARRAY_A);
                                      foreach($registros as $registro) 
                                        { 
                                          $i=$registro['id_ind_estrategico'];
                                          $nom_ind=$registro['nom_indicador'];
                                          $btn_ac_des=$registro['status'];
                                         
                                          ?>
                                      

                                         <button class="accordion"  id="acordeon_id_<?php echo($registro['id_ind_estrategico'])?>">
                                          <div>
                                            <table>
                                              <col width=80%>
                                              <col width=20%>
                                                <tbody>
                                                 <td class ="contenido_datos" > <?php echo $registro['nom_indicador'];?></td> 
                                                 <td class ="contenido_datos" >

                                                    <?php  

                                                    if($registro['status'] == 0)
                                                    {
                                                      echo("<h2 style='color:#008ec5;'>En proceso</h2>");

                                                    }
                                                    else if($registro['status'] == 1)
                                                    {
                                                      echo("<h4 style='color:#0bd03e;'>Aprobado</h>");
                                                    } 
                                                     else if($registro['status'] == 2)
                                                    {
                                                      echo("<h4 style='color:#f74515;'>Corregir</h>");
                                                    } 
                                                     else if($registro['status'] == 5)
                                                    {
                                                      echo("<label style='text-align: center; font-size: 20px; font-family: Graphik-Bold ' color:#727272; '>Editar</label>");
                                                    } 
                                                      ?>                      
                                                  </td> 
                                                </tbody>
                                            </table>
                                          </div>  
                                         </button>

                                          <div class="panel">
                                            <div class="tab_vert">
                                              <button  id="defaultOpen_vertical" class="tablinks_vertical" onclick="open_vertical_tab(event, 'Est_Parte_indicador_<?php echo $registro['id_ind_estrategico']; ?>')">INDICADOR<i  class="fas fa-redo" 
                                             id="est_check_green_indicador_<?php echo $registro['id_ind_estrategico']; ?>" style="font-size:20px;color:green;text-shadow:2px 2px 4px #000000;"></i></button>

                                             <button  id="defaultOpen_vertical" class="tablinks_vertical" onclick="open_vertical_tab(event, 'Est_Parte_obras_<?php echo $registro['id_ind_estrategico']; ?>')">OBRAS Y ACCIONES<i  class="fas fa-redo" 
                                             id="est_check_green_indicador_<?php echo $registro['id_ind_estrategico']; ?>" style="font-size:20px;color:green;text-shadow:2px 2px 4px #000000;"></i></button>

                                              <button class="tablinks_vertical" onclick="open_vertical_tab(event, 'Est_Parte_evidencia_<?php echo $registro['id_ind_estrategico']; ?>')">METAS<i id="est_check_green_metas_<?php echo $registro['id_ind_estrategico']; ?>" style="font-size:20px;color:green;text-shadow:2px 2px 4px #000000;"></i></button>

                                                <script>

                                                  var button_add_deisabled =document.getElementById("acordeon_id_<?php echo($registro['id_ind_estrategico'])?>");

                                                        if(<?php echo($registro['status'])?>==0)
                                                        {
                                                          button_add_deisabled.setAttribute("disabled", true);
                                                        }
                                                        else if(<?php echo($registro['status'])?>==1)
                                                        { 
                                                          button_add_deisabled.setAttribute("disabled", true);
                                                        }
                                                        else if(<?php echo($registro['status'])?>==2)
                                                        {
                                                        }
                                                        else if(<?php echo($registro['status'])?>==5)
                                                        {
                                                         //editable
                                                        }


                                                    //parte indicador
                                                    if( "<?php echo $registro['estatus']; ?>"==5)
                                                    {$('#est_check_green_indicador_<?php echo $registro['id_ind_estrategico']; ?>').hide();}
                                                    else
                                                    {$('#est_check_green_indicador_<?php echo $registro['id_ind_estrategico']; ?>').show();}
                                                   
                                                    //parte evidencia
                                                    if( "<?php echo $registro['estatus']; ?>"==5)
                                                    {$('#est_check_green_metas_<?php echo $registro['id_ind_estrategico']; ?>').hide();}
                                                    else
                                                    {$('#est_check_green_metas_<?php echo $registro['id_ind_estrategico']; ?>').show();}
                                                    //parte inconsistencias
                                                </script>


                                            </div>



                                              <form  method="post" id="signupForm_correccion" name="signupForm_correccion" > 
             
                                              <div id="Est_Parte_indicador_<?php echo $registro['id_ind_estrategico']; ?>" class="tabcontent_vert">
                                                <div id="global">
                                                    <div id="mensajes">
                                                      <div class="texto">
                                                        <div id="info_labels_<?php echo $registro['id_ind_estrategico'];?>">
                                                         <form id="form_part_indicador" method="post">
                                                         
                                                            <table>
                                                            <col width=20%>
                                                            <col width=80%>
                                                              <tbody>
                                                                <?php global $wpdb;
                                                                $resultado;
                                                                $evidencias = $wpdb->prefix . 'indicadores_estrategicos_definidos';
                                                                $registros_1 = $wpdb->get_results("SELECT * FROM $evidencias where id_ind_estrategico='$i'", ARRAY_A);
                                                                foreach($registros_1 as $registro_1) { 
                                                                  ?>
                                                                    <br>
                                                                    <!--Robert -->
                                                                      <p><label id="error_est_edit_descrip_inidcador_<?php echo($registro_1['id_ind_estrategico'])?>"  for="est_descrp_indicador">Descripcion del indicador<strong class="tamanio_caracteres" id="tamanio_caracteres">(450 caracteres)</strong></label>
                                                                      <textarea id="est_descrp_indicador_<?php echo $registro['id_ind_estrategico'];?>" maxlength="450" name="est_descrp_indicador" placeholder="Descripcion del indicador" ><?php echo($registro_1['descripcion_indicador'])?></textarea>
                                                                      <br> 

                                                                       <p><label id="est_edit_error_linea_discursiva_<?php echo($registro_1['id_ind_estrategico'])?>"  for="estr_linea_discursiva">Linea discursiva<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                                                                      <textarea id="estr_linea_discursiva_<?php echo $registro['id_ind_estrategico'];?>" maxlength="450" name="estr_linea_discursiva" placeholder="Linea discursiva"><?php echo($registro_1['linea_discursiva'])?></textarea>
                                                                      <br>

                                                                      <p><label id="error_est_hubo_logro"  for="est_hubo_logro">Hubo logros en este indicador</label>
                                                                      <div class="select">
                                                                        <select name="est_hubo_logro_<?php echo($registro_1['id_ind_estrategico'])?>" id="est_hubo_logro_<?php echo($registro_1['id_ind_estrategico'])?>" >  
                                                                          <option value="No" >No</option>
                                                                          <option value="Si" >Si</option>
                                                                        </select>
                                                                      </div>

                                                                      <div id="div_est_hubo_logro_<?php echo($registro_1['id_ind_estrategico'])?>">
                                                                        <p><label id="est_edit_error_bullet_logro_<?php echo($registro_1['id_ind_estrategico'])?>"  for="est_bullet_logro">Bullet del logro<strong class="tamanio_caracteres" id="tamanio_caracteres"> (450 caracteres)</strong></label>
                                                                        <textarea id="est_bullet_logro_<?php echo $registro['id_ind_estrategico'];?>" maxlength="450" name="est_bullet_logro" placeholder="Escribir el bullet del logro:"><?php echo($registro_1['bullet_logro'])?></textarea>
                                                                        <br>
                                                                      </div>

                                                                      <!--

                                                                      <p><label id="error_est_logros_asoc_a_este_ind"  for="est_logro_asoc_ind">Este Este indicador esta ligado a un premio o reconocimiento para el estado</label>
                                                                      <div class="select">
                                                                        <select name="est_logro_asoc_ind_<?php echo($registro_1['id_ind_estrategico'])?>" id="est_logro_asoc_ind_<?php echo($registro_1['id_ind_estrategico'])?>" >  
                                                                          <option value="No" >No</option>
                                                                          <option value="Si" >Si</option>
                                                                        </select>
                                                                      </div>

                                                                      <div id="div_est_descripcion_logro_<?php echo($registro_1['id_ind_estrategico'])?>">
                                                                          
                                                                          <p><label id="error_est_descripcion_logro_<?php echo($registro_1['id_ind_estrategico'])?>"  for="est_descripcion_logro">Descripcion del premio o reconocimiento<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                                                        <textarea id="est_descripcion_logro_<?php echo $registro['id_ind_estrategico'];?>" maxlength="350" name="est_descripcion_logro" placeholder="Descripcion del premio o reconocimiento"   ><?php echo($registro_1['descripcion_logro'])?></textarea>
                                                                        <br>
                                                                      </div>
                                                                    -->
                                                                  <?php } 

                                                                ?>
                                                              </tbody>
                                                            </table>

                                                            <script type="text/javascript">

                                                              $('#div_est_descripcion_logro_<?php echo($registro_1['id_ind_estrategico'])?>').hide();

                                                              $('#div_est_hubo_logro_<?php echo($registro_1['id_ind_estrategico'])?>').hide();

                                                              if("<?php echo($registro_1['logro_asoc_indicador'])?>"=="No")
                                                                 {
                                                                  document.getElementById("est_logro_asoc_ind_<?php echo($registro_1['id_ind_estrategico'])?>").selectedIndex = 0;
                                                                 }
                                                                 else if("<?php echo($registro_1['logro_asoc_indicador'])?>"=="Si")
                                                                 {

                                                                  document.getElementById("est_logro_asoc_ind_<?php echo($registro_1['id_ind_estrategico'])?>").selectedIndex = 1;

                                                                  $('#div_est_descripcion_logro_<?php echo($registro_1['id_ind_estrategico'])?>').show();
                                                                 }


                                                              if("<?php echo($registro_1['hubo_logro'])?>"=="No")
                                                                 {
                                                                  document.getElementById("est_hubo_logro_<?php echo($registro_1['id_ind_estrategico'])?>").selectedIndex = 0;
                                                                 }
                                                                 else if("<?php echo($registro_1['hubo_logro'])?>"=="Si")
                                                                 {

                                                                  document.getElementById("est_hubo_logro_<?php echo($registro_1['id_ind_estrategico'])?>").selectedIndex = 1;

                                                                  $('#div_est_hubo_logro_<?php echo($registro_1['id_ind_estrategico'])?>').show();
                                                                 }





                                                                $('#est_logro_asoc_ind_<?php echo($registro_1['id_ind_estrategico'])?>').change(function()
                                                                { 
                                                                   if($(this).val() == "Si")
                                                                  {
                                                                      $('#div_est_descripcion_logro_<?php echo($registro_1['id_ind_estrategico'])?>').show();
                                                                  }
                                                                  else if($(this).val() == "No")
                                                                  {
                                                                     $('#div_est_descripcion_logro_<?php echo($registro_1['id_ind_estrategico'])?>').hide();
                                                                  }
                                                                });

                                                                $('#est_hubo_logro_<?php echo($registro_1['id_ind_estrategico'])?>').change(function()
                                                                { 
                                                                   if($(this).val() == "Si")
                                                                  {
                                                                      $('#div_est_hubo_logro_<?php echo($registro_1['id_ind_estrategico'])?>').show();
                                                                  }
                                                                  else if($(this).val() == "No")
                                                                  {
                                                                     $('#div_est_hubo_logro_<?php echo($registro_1['id_ind_estrategico'])?>').hide();
                                                                  }
                                                                });
                                                             
     

                                                               

                                                              function SubmitFormData_estr_obras(valor_entra) 
                                                              {

                                                                var id_entra=valor_entra;

                                                                var est_edi_descrp_ind = $("#est_descrp_indicador_"+valor_entra).val();
                                                                var est_edit_bullet_log = $("#est_bullet_logro_"+valor_entra).val();
                                                               var est_edit_linea_disc = $("#estr_linea_discursiva_"+valor_entra).val();
                                                                var est_edit_logro_asoc_ind = $("#est_logro_asoc_ind_"+valor_entra).val();
                                                                //var est_edit_desc_log = $("#est_descripcion_logro_"+valor_entra).val();
                                                                //var est_edit_lin_base = $("#est_linea_base_"+valor_entra).val();
                                                                var est_edit_hubo_logro = $("#est_hubo_logro_"+valor_entra).val();

                                                                
                                               
                                                                var lbl_err_desc_ind = document.getElementById("error_est_edit_descrip_inidcador_"+valor_entra);
                                                                 var lbl_err_bull_logro_error = document.getElementById("est_edit_error_bullet_logro_"+valor_entra);
                                                                 var lbl_err_lin_disc = document.getElementById("est_edit_error_linea_discursiva_"+valor_entra);

                                                                 var lbl_err_desc_logr = document.getElementById("error_est_descripcion_logro_"+valor_entra);


                                                                if(est_edi_descrp_ind.length>450) /*Robert*/ 
                                                                {
                                                                 
                                                                  lbl_err_desc_ind.innerHTML ="El limite son 450 caracteres";
                                                                  lbl_err_desc_ind.classList.add("error_mensaje");

                                                                }
                                                                else if(est_edit_bullet_log.length>450) /*Robert*/ 
                                                                {
                                                                 
                                                                  lbl_err_bull_logro_error.innerHTML ="El limite son 450 caracteres";
                                                                  lbl_err_bull_logro_error.classList.add("error_mensaje");

                                                                }
                                                                else if(est_edit_linea_disc.length>450) /*Robert*/ 
                                                                {
                                                                 
                                                                  lbl_err_lin_disc.innerHTML ="El limite son 450 caracteres";
                                                                  lbl_err_lin_disc.classList.add("error_mensaje");
                                                                }
                                                                /*

                                                                else if(est_edit_desc_log.length>350) 
                                                                {
                                                                 
                                                                  lbl_err_desc_logr.innerHTML ="El limite son 350 caracteres";
                                                                  lbl_err_desc_logr.classList.add("error_mensaje");
                                                                }
                                                                */
                                                       

                                                                else
                                                                {
                                                                  //alert(valor_entra);

                                                                    $.ajax({

                                                                      type: "POST",
                                                                    

                                                                    
                                                                    url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                                                                    data: {id_indicador:id_entra,est_edi_descrp_ind: est_edi_descrp_ind, est_edit_bullet_log:est_edit_bullet_log,est_edit_linea_disc:est_edit_linea_disc,est_edit_logro_asoc_ind,est_edit_logro_asoc_ind,est_edit_hubo_logro:est_edit_hubo_logro}
                                                                    ,
                                                                    success: function(msg){
                                                                      if(msg=1)
                                                                      {
                                                                        alert("Informacion Actualizada");
                                                                      }

                                                                     // alert( "Informacion Actualizada" );
                                                                    },
                                                                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                                                                       alert( "Error Verifica tu informacion" );
                                                                    }
                                                                  });
                                                                    

                                                                    lbl_err_desc_ind.classList.remove("error_mensaje");
                                                                    lbl_err_desc_ind.innerHTML ="Descripcion del indicador";

                                                                    lbl_err_bull_logro_error.classList.remove("error_mensaje");
                                                                    lbl_err_bull_logro_error.innerHTML ="Bullet del logro";

                                                                    lbl_err_desc_ind.classList.remove("error_mensaje");
                                                                    lbl_err_desc_ind.innerHTML ="Descripcion del indicador";

                                                                    lbl_err_bull_logro_error.classList.remove("error_mensaje");
                                                                    lbl_err_bull_logro_error.innerHTML ="Linea discursiva";


                                                                }

                                                              }

                                                              
                                                            </script>
                                                            
                                                            <input style="background-color: #555555;color: white;"  type="button" id="submit_indicador" onclick="SubmitFormData_estr_obras(<?php echo $registro['id_ind_estrategico'];?>)" value="Actualizar Informacion" /> </input> 
                                                                           
                                                          </form>
                                                          
                                                          
                                                          <div><!--comentarios de correccion del evaluador-->
                                                            <!--
                                                              <label>"<?php echo($registro_1['coment_indicador']) ?>"</label>
                                                            -->
                                                          </div>
                                                       
                                                      </div>
                                                    </div>
                                                  </div>
                                                  <br>
                                                  <br>
                                                </div>
                                              </div>

                                              <div id="Est_Parte_obras_<?php echo $registro['id_ind_estrategico']; ?>" class="tabcontent_vert">
                                                <div id="global">
                                                    <div id="mensajes">
                                                      <div class="texto">
                                                        <div id="info_labels_<?php echo $registro['id_ind_estrategico'];?>">
                                                         <form id="form_part_indicador" method="post">
                                                         
                                                            <table>
                                                            <col width=20%>
                                                            <col width=80%>
                                                              <tbody>
                                                                <?php global $wpdb;
                                                                $resultado;
                                                                $evidencias = $wpdb->prefix . 'indicadores_estrategicos_definidos';
                                                                $registros_1 = $wpdb->get_results("SELECT * FROM $evidencias where id_ind_estrategico='$i'", ARRAY_A);
                                                                foreach($registros_1 as $registro_1) { 
                                                                  ?>
                                                                    <br>
                                                                    <br><br>
                                    
                                                                      <p><label id="est_error_resultado_1_edit_<?php echo $registro['id_ind_estrategico'];?>"  for="est_resultado1_edit_<?php echo $registro['id_ind_estrategico'];?>">Obras y acciones 1:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>

                                                                      <textarea class="textbox_corto" maxlength="350" id="est_resultado1_edit_<?php echo $registro['id_ind_estrategico'];?>" name="est_resultado1_edit_<?php echo $registro['id_ind_estrategico'];?>" placeholder="Obras y acciones 1:"><?php echo $registro['resultado1'];?></textarea>
                                                                      
                                                                      <button id="est_btn_def_lugar_1"  onclick="open_lugares(1,0,1)" type="button" >Definir lugar 1</button></p>
                                                                      <label id="est_datos_cargados_lug_1_edit_<?php echo $registro['id_ind_estrategico'];?>" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                                                      
                                                                      <input type="hidden" id="est_res1_lugares_edit_<?php echo $registro['id_ind_estrategico'];?>" name="est_res1_lugares_edit_<?php echo $registro['id_ind_estrategico'];?>"  value="<?php echo $registro['resultado1'];?>">

                                                                      <input type="hidden" id="edit_res1_lugares_GPS_<?php echo $registro['id_ind_estrategico'];?>" name="edit_res1_lugares_GPS_<?php echo $registro['id_ind_estrategico'];?>" value="<?php echo $registro['est_res1_lugares_gps'];?>">


                                                                      <p><label id="est_error_resultado_2" id="est_label_resultado_2"  for="est_resultado2_edit_<?php echo $registro['id_ind_estrategico'];?>">Obras y acciones 2:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                                                      <textarea class="textbox_corto" maxlength="350" id="est_resultado2_edit_<?php echo $registro['id_ind_estrategico'];?>" name="est_resultado2_edit_<?php echo $registro['id_ind_estrategico'];?>" placeholder="Obras y acciones 2:"><?php echo $registro['resultado2'];?></textarea>
                                                                      <button  id="est_btn_def_lugar_2" class="btn_def_lugar" onclick="open_lugares(2,0,1)" type="button" >Definir lugar 2</button></p>
                                                                      <label id="est_datos_cargados_lug_2_edit_<?php echo $registro['id_ind_estrategico'];?>" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                                                      <input type="hidden" id="est_res2_lugares_edit_<?php echo $registro['id_ind_estrategico'];?>" name="est_res2_lugares_edit_<?php echo $registro['id_ind_estrategico'];?>" value="<?php echo $registro['resultado2'];?>" >
                                                                      <input type="hidden" id="edit_res2_lugares_GPS_<?php echo $registro['id_ind_estrategico'];?>" name="edit_res2_lugares_GPS_<?php echo $registro['id_ind_estrategico'];?>" value="<?php echo $registro['est_res2_lugares_gps'];?>">

                                                                      <p><label id="est_error_resultado_3"  for="est_resultado3_edit_<?php echo $registro['id_ind_estrategico'];?>">Obras y acciones 3:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                                                      <textarea class="textbox_corto" maxlength="350" id="est_resultado3_edit_<?php echo $registro['id_ind_estrategico'];?>" name="est_resultado3_edit_<?php echo $registro['id_ind_estrategico'];?>" placeholder="Obras y acciones 3:"><?php echo $registro['resultado3'];?></textarea>
                                                                      <button class="btn_def_lugar" id="est_btn_def_lugar_3" onclick="open_lugares(3,0,1)" type="button" >Definir lugar 3</button></p>
                                                                      <label id="est_datos_cargados_lug_3_edit_<?php echo $registro['id_ind_estrategico'];?>" class="datos_cargados_lugar_resultados" >Datos Cargados</label>

                                                                      <input type="hidden"  id="est_res3_lugares_edit_<?php echo $registro['id_ind_estrategico'];?>" name="est_res3_ugares_edit_<?php echo $registro['id_ind_estrategico'];?>" value="<?php echo $registro['resultado3'];?>"> 
                                                                      <input type="hidden" id="edit_res3_lugares_GPS_<?php echo $registro['id_ind_estrategico'];?>" name="edit_res3_lugares_GPS_<?php echo $registro['id_ind_estrategico'];?>" value="<?php echo $registro['est_res3_lugares_gps'];?> " >
                                                                      

                                                                      <p><label id="est_error_resultado_4" id="est_label_resultado_4"  for="est_resultado4_edit_<?php echo $registro['id_ind_estrategico'];?>">Obras y acciones 4:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                                                      <textarea class="textbox_corto" maxlength="350" id="est_resultado4_edit_<?php echo $registro['id_ind_estrategico'];?>" name="est_resultado4_edit_<?php echo $registro['id_ind_estrategico'];?>" placeholder="Obras y acciones 4:"><?php echo $registro['resultado4'];?></textarea>
                                                                      <button  id="est_btn_def_lugar_4" class="btn_def_lugar" onclick="open_lugares(4,0,1)" type="button" >Definir lugar 4</button></p>
                                                                      <label id="est_datos_cargados_lug_4_edit_<?php echo $registro['id_ind_estrategico'];?>" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                                                      <input type="hidden"  id="est_res4_lugares_edit_<?php echo $registro['id_ind_estrategico'];?>" name="est_res2_4ugares_edit_<?php echo $registro['id_ind_estrategico'];?>" value="<?php echo $registro['resultado4'];?>"> 
                                                                      <input type="hidden"  id="edit_res4_lugares_GPS_<?php echo $registro['id_ind_estrategico'];?>" name="edit_res4_lugares_GPS_<?php echo $registro['id_ind_estrategico'];?>"  value="<?php echo $registro['est_res4_lugares_gps'];?>">

                                                                    

                                                                      <p><label id="est_error_resultado_5" id="est_label_resultado_5"  for="est_resultado5_edit_<?php echo $registro['id_ind_estrategico'];?>">Obras y acciones 5:<strong class="tamanio_caracteres" id="tamanio_caracteres"> (350 caracteres)</strong></label>
                                                                      <textarea class="textbox_corto" maxlength="350" id="est_resultado5_edit_<?php echo $registro['id_ind_estrategico'];?>" name="est_resultado5_edit_<?php echo $registro['id_ind_estrategico'];?>" placeholder="Obras y acciones 5:"><?php echo $registro['resultado5'];?></textarea>
                                                                      <button  id="est_btn_def_lugar_5" class="btn_def_lugar" onclick="open_lugares(5,0,1)" type="button" >Definir lugar 5</button></p>
                                                                      <label id="est_datos_cargados_lug_5_edit_<?php echo $registro['id_ind_estrategico'];?>" class="datos_cargados_lugar_resultados" >Datos Cargados</label>
                                                                      <input type="hidden" id="est_res5_lugares_edit_<?php echo $registro['id_ind_estrategico'];?>" name="est_res5_lugares_edit_<?php echo $registro['id_ind_estrategico'];?>" value="<?php echo $registro['resultado5'];?>">
                                                                      <input type="hidden" id="edit_res5_lugares_GPS_<?php echo $registro['id_ind_estrategico'];?>" name="edit_res5_lugares_GPS_<?php echo $registro['id_ind_estrategico'];?>"  value="<?php echo $registro['est_res5_lugares_gps'];?>">

                                                                      



                                                                 

                                                                      <br><br>
                                   

                                                                  <?php } 

                                                                ?>
                                                              </tbody>
                                                            </table>

                                                            <script type="text/javascript">



                                                            $('#est_datos_cargados_lug_1_edit_<?php echo($registro['id_ind_estrategico'])?>').hide();
                                                            $('#est_datos_cargados_lug_2_edit_<?php echo($registro['id_ind_estrategico'])?>').hide();
                                                            $('#est_datos_cargados_lug_3_edit_<?php echo($registro['id_ind_estrategico'])?>').hide();
                                                            $('#est_datos_cargados_lug_4_edit_<?php echo($registro['id_ind_estrategico'])?>').hide();
                                                            $('#est_datos_cargados_lug_5_edit_<?php echo($registro['id_ind_estrategico'])?>').hide();

                                                           // alert('est_datos_cargados_lug_1_edit_<?php echo $registro['id_ind_estrategico'];?>');


                                                               
                                                             
     

                                                               

                                                              function SubmitFormData_indicador_tact(valor_entra) 
                                                              {
                                                              //  alert("uptade estrategico obras");

                                                                var id_entra=valor_entra;

                                                                var est_resultado1_edit_ = $("#est_resultado1_edit_"+valor_entra).val();

                                                                var est_resultado2_edit_ = $("#est_resultado2_edit_"+valor_entra).val();

                                                               var est_resultado3_edit_ = $("#est_resultado3_edit_"+valor_entra).val();

                                                                var est_resultado4_edit_ = $("#est_resultado4_edit_"+valor_entra).val();

                                                                var est_resultado5_edit_ = $("#est_resultado5_edit_"+valor_entra).val();




                                                                var est_res1_lugares_edit_ = $("#est_res1_lugares_edit_"+valor_entra).val();

                                                                var est_res2_lugares_edit_ = $("#est_res2_lugares_edit_"+valor_entra).val();

                                                               var est_res3_lugares_edit_ = $("#est_res3_lugares_edit_"+valor_entra).val();

                                                                var est_res4_lugares_edit_ = $("#est_res4_lugares_edit_"+valor_entra).val();

                                                                var est_res5_lugares_edit_ = $("#est_res5_lugares_edit_"+valor_entra).val();


                                                                var edit_res1_lugares_GPS_ = $("#edit_res1_lugares_GPS_"+valor_entra).val();

                                                                var edit_res2_lugares_GPS_ = $("#edit_res2_lugares_GPS_"+valor_entra).val();

                                                               var edit_res3_lugares_GPS_ = $("#edit_res3_lugares_GPS_"+valor_entra).val();

                                                                var edit_res4_lugares_GPS_ = $("#edit_res4_lugares_GPS_"+valor_entra).val();

                                                                var edit_res5_lugares_GPS_ = $("#edit_res5_lugares_GPS_"+valor_entra).val();






                                                                 var lbl_est_error_resultado_1_edit_ = $("#est_error_resultado_1_edit_"+valor_entra).val();

                                                                 var lbl_est_error_resultado_2_edit_ = $("#est_error_resultado_2_edit_"+valor_entra).val();

                                                                 var lbl_est_error_resultado_3_edit_ = $("#est_error_resultado_3_edit_"+valor_entra).val();

                                                                 var lbl_est_error_resultado_4_edit_ = $("#est_error_resultado_4_edit_"+valor_entra).val();

                                                                 var lbl_est_error_resultado_5_edit_ = $("#est_error_resultado_5_edit_"+valor_entra).val();


                                                                



                                                                if(est_resultado1_edit_.length>350) /*Robert*/ 
                                                                {
                                                                 
                                                                  lbl_est_error_resultado_1_edit_.innerHTML ="El limite son 350 caracteres";
                                                                  lbl_est_error_resultado_1_edit_.classList.add("error_mensaje");

                                                                }
                                                                else if(est_resultado1_edit_.length>350) /*Robert*/ 
                                                                {
                                                                 
                                                                  lbl_est_error_resultado_2_edit_.innerHTML ="El limite son 350 caracteres";
                                                                  lbl_est_error_resultado_2_edit_.classList.add("error_mensaje");

                                                                }
                                                                else if(est_resultado3_edit_.length>350) /*Robert*/ 
                                                                {
                                                                 
                                                                  lbl_err_lin_disc.innerHTML ="El limite son 350 caracteres";
                                                                  lbl_err_lin_disc.classList.add("error_mensaje");
                                                                }

                                                                else if(est_resultado4_edit_.length>350) /*Robert*/ 
                                                                {
                                                                 
                                                                  lbl_est_error_resultado_4_edit_.innerHTML ="El limite son 350 caracteres";
                                                                  lbl_est_error_resultado_4_edit_.classList.add("error_mensaje");
                                                                }
                                                                else if(est_resultado3_edit_.length>350) /*Robert*/ 
                                                                {
                                                                 
                                                                  lbl_est_error_resultado_5_edit_.innerHTML ="El limite son 350 caracteres";
                                                                  lbl_est_error_resultado_5_edit_.classList.add("error_mensaje");
                                                                }
                                                       

                                                                else
                                                                {
                                                           
                                                                    $.ajax({
                                                                    
                                                                    type: "POST",
                                                                    
                                                                    url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                                                                    data: {id_indicador:id_entra,
                                                                        est_resultado1_edit_:est_resultado1_edit_,est_resultado2_edit_:est_resultado2_edit_,est_resultado3_edit_:est_resultado3_edit_,est_resultado4_edit_:est_resultado4_edit_,est_resultado5_edit_:est_resultado5_edit_,est_res1_lugares_edit_:est_res1_lugares_edit_,
                                                                        est_res2_lugares_edit_:est_res2_lugares_edit_,
                                                                        est_res3_lugares_edit_:est_res3_lugares_edit_,
                                                                        est_res4_lugares_edit_:est_res4_lugares_edit_,
                                                                        est_res5_lugares_edit_:est_res5_lugares_edit_,
                                                                        edit_res1_lugares_GPS_:edit_res1_lugares_GPS_,
                                                                        edit_res2_lugares_GPS_:edit_res2_lugares_GPS_,
                                                                        edit_res3_lugares_GPS_:edit_res3_lugares_GPS_,
                                                                        edit_res4_lugares_GPS_:edit_res4_lugares_GPS_,
                                                                        edit_res5_lugares_GPS_:edit_res5_lugares_GPS_


                                                                    }
                                                                    ,
                                                                    success: function(msg){
                                                                      if(msg=1)
                                                                      {
                                                                        alert("Informacion Actualizada");
                                                                      }

                                                                     // alert( "Informacion Actualizada" );
                                                                    },
                                                                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                                                                       alert( "Error Verifica tu informacion" );
                                                                    }
                                                                  });
                                                                    


                                                                }

                                                              }

                                                              
                                                            </script>
                                                            
                                                            <input style="background-color: #555555;color: white;"  type="button" id="submit_indicador" onclick="SubmitFormData_indicador_tact(<?php echo $registro['id_ind_estrategico'];?>)" value="Actualizar Informacion" /> </input> 
                                                                           
                                                          </form>
                                                          
                                                          
                                                          <div><!--comentarios de correccion del evaluador-->
                                                            <!--
                                                              <label>"<?php echo($registro_1['coment_indicador']) ?>"</label>
                                                            -->
                                                          </div>
                                                       
                                                      </div>
                                                    </div>
                                                  </div>
                                                  <br>
                                                  <br>
                                                </div>
                                              </div>

                                              <div id="Est_Parte_evidencia_<?php echo $registro['id_ind_estrategico']; ?>" class="tabcontent_vert">
                                                  <div id="global">
                                                    <div id="mensajes">
                                                      <div class="texto">
                                                        <div id="info_labels_<?php echo $registro['id_ind_estrategico'];?>_2">
                                                          <form id="form_part_indicador" method="post">
                                                          <table>
                                                            <col width=20%>
                                                            <col width=80%>
                                                            <tbody>
                                                              <?php global $wpdb;
                                                                $evidencias = $wpdb->prefix . 'indicadores_estrategicos_definidos';
                                                                $registros_2 = $wpdb->get_results("SELECT * FROM $evidencias where id_ind_estrategico='$i'", ARRAY_A);
                                                                foreach($registros_2 as $registro_2) { ?>
                                                                  <br>
                                                                    <p><label id="error_est_linea_base"  for="est_linea_base">Linea base<strong class="tamanio_caracteres" id="tamanio_caracteres"> (10 caracteres)</strong></label><input maxlength="10" placeholder="Linea base" disabled name="est_linea_base" id="est_linea_base" value="<?php echo($registro_1['linea_base'])?>" ></p>
                                                                    <br>

                                                                    <p><label id="error_result_2017"  for="result_2017">Meta 2022<strong class="tamanio_caracteres" id="tamanio_caracteres"> (10 caracteres)</strong></label><input maxlength="10" id="edit_est_meta_2022" disabled placeholder="Meta 2022" name="meta_2022"   value="<?php echo($registro_1['meta_2022'])?>"></p>

                                                                    <br>
                                                                    <p><label id="error_meta_2018"  for="meta_2018">Meta 2030<strong class="tamanio_caracteres" id="tamanio_caracteres"> (10 caracteres)</strong></label><input maxlength="10" placeholder="Meta 2030" disabled value="<?php echo($registro_1['meta_2030'])?>" name="meta_2030" id="meta_2030" ></p>
                                                                    <br>

                                                                    <p><label id="error_est_res_abril"  for="est_res_abril">Resultado  2019<strong class="tamanio_caracteres" id="tamanio_caracteres"> (10 caracteres)</strong></label><input maxlength="10" placeholder="Resultado  2018" name="est_res_abril" id="es_edit_resul_2018_<?php echo $registro['id_ind_estrategico'];?>" value="<?php echo($registro_1['result_abril_2018'])?>" ></p>
                                                                    <?php } 

                                                                  ?>
                                                            </tbody>
                                                          </table>

                                                          <script type="text/javascript">
                                                            //ocultar el mensaje datos cargados


                                                            function SubmitFormData_meta(valor_entra) 
                                                            {


                                                              var est_edit_resultado_2018 = $("#es_edit_resul_2018_"+valor_entra).val();
                                                              /*
                                                              var est_edit_res_abril=$("#est_res_abril_"+valor_entra).val();
                                                              var est_edit_avance=$("#est_avance_meta_est_"+valor_entra).val();
                                                              */


                                                              $.ajax({
                                                                  type: "POST",
                                                                  url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                                                                  data: {id_indicador:valor_entra,est_edit_resultado_2018:est_edit_resultado_2018}
                                                                  ,
                                                                success: function(msg){
                                                                  console.log(msg);
                                                                  if(msg=1)
                                                                  {
                                                                    alert("Informacion Actualizada");
                                                                  }
                                                                    
                                                                  },
                                                                  error: function(XMLHttpRequest, textStatus, errorThrown) {
                                                                     alert( "Error Verifica tu informacion" );
                                                                  }
                                                                });

          
                                                            }
                                                          </script>

                                                                              
                                                          <input style="background-color: #555555;color: white;" type="button" id="submit_indicador" onclick="SubmitFormData_meta(<?php echo $registro['id_ind_estrategico'];?>)" value="Actualizar Informacion" />             
                                                          </form>

                                                        <div><!--comentarios de correccion del evaluador-->
                                                          <!--
                                                              <label>"<?php echo($registro_1['coment_evidencia']) ?>"</label>
                                                            -->
                                                        </div>
                                                      </div>
                                                     </div>
                                                    </div>
                                                  </div>
                                              </div>


                                        </form>
                                      </div>
                                    <?php  
                                  } ?>                    

                                </div>
                              </div>

                              <div id="REPORTES_EST_pdf" class="tabcontent_est">
                                <div class="container">
                                <br>
                                <h3>INDICADORES ESTRATEGICOS</h3>
                                  <table>
                                    <col width=5%>
                                    <col width=65%>
                                    <col width=30%>
                                    <br>
                                    <br>
                                      <tbody>
                                          <?php global $wpdb;
                                            $ind_estrategicos = $wpdb->prefix .'indicadores_estrategicos_definidos';
                                            $registros_estrategicos_1 = $wpdb->get_results("SELECT * FROM $ind_estrategicos WHERE dependecia_encargada ='$user_name'", ARRAY_A);
                                            foreach($registros_estrategicos_1 as $registro_estrategicos_1) 
                                              { ?>
                                                <?php $id_evidencia=$registro_estrategicos_1['id_ind_estrategico']; 
                                                 $tipo="estrategico";
                                                ?>
                                               
                                                  <tr>
                                                    <td>
                                                    <img style="text-align: center" src="http://sigeh.hidalgo.gob.mx/repositorio/wp-content/uploads/2018/04/PDF.png" height="42" width="30" ></img>
                                                  </td>
                                                     <td id="txt_list_pdf_estr"> <?php    echo("<p class='demo'><a href='pdf_plugin/pdf_evidencias/crear_evidencia_pdf.php?id_evidencia= $id_evidencia&arr_ids_tacticos=0&user=$user_name&tipo=$tipo' target='_blank' class='demo'>".$registro_estrategicos_1['nom_indicador']."</a></p>");?></td> 

                                                     <td id="contenido_datos" >   
                                                     
                                                      <?php echo(  $registro_estrategicos_1['fecha_registro']);?>
                                                  
                                                   </td>
                                                 </tr>
                                              <?php } ?> 
                                      </tbody>
                                    
                                  </table>
                                </div> 
                              </div>



                                
                          </div>


                          <!--COVID-->


                          <div id="seccion_covid">
                            <!-- Tab_est links -->
                            <div class="container">   
                              <?php get_template_part('templates/formulario', 'covid'); ?>

                            </div>   
                          </div>
                          <!-- FIN COVID-->



                          <div id="seccion_pids">
                            <!-- Tab_est links -->
                            <div class="container">
                              <?php get_template_part('templates/formulario', 'pids'); ?>
                            </div>
                          </div>




                     
                        <script>


    


                          $(document).ready(function() 
                            {

                              $('#menu_registro_indicador_dependencias').hide();
                               $('#menu_registro_indicador_orgnismo').hide();
                              ir_menu_indicadores();
                              //---------MAPA


                              var map = null;
                              
                              var myLatlng;

                              function initializeGMap(lat, lng) {
                                myLatlng = new google.maps.LatLng(lat, lng);

                                var myOptions = {
                                  zoom: 6,
                                  zoomControl: true,
                                  center: myLatlng,
                                  mapTypeId: google.maps.MapTypeId.ROADMAP
                                };

                                map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

                                myMarker = new google.maps.Marker({
                                  position: myLatlng,
                                  draggable:true,
                                });


                                // Create the search box and link it to the UI element.
                                var input = document.getElementById('pac-input');
                                var searchBox = new google.maps.places.SearchBox(input);
                                map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                                // Bias the SearchBox results towards current map's viewport.
                                map.addListener('bounds_changed', function() {
                                  searchBox.setBounds(map.getBounds());
                                });



                                // Create the search box and link it to the UI element.
                                var input = document.getElementById('pac-input');
                                var searchBox = new google.maps.places.SearchBox(input);
                                map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                                // Bias the SearchBox results towards current map's viewport.
                                map.addListener('bounds_changed', function() {
                                  searchBox.setBounds(map.getBounds());
                                });

                                var markers = [];
                                // Listen for the event fired when the user selects a prediction and retrieve
                                // more details for that place.
                                searchBox.addListener('places_changed', function() {
                                  var places = searchBox.getPlaces();

                                  if (places.length == 0) {
                                    return;
                                  }

                                  // Clear out the old markers.
                                  markers.forEach(function(marker) {
                                    marker.setMap(null);
                                  });
                                  markers = [];

                                  // For each place, get the icon, name and location.
                                  var bounds = new google.maps.LatLngBounds();
                                  places.forEach(function(place) {
                                    if (!place.geometry) {
                                      console.log("Returned place contains no geometry");
                                      return;
                                    }
                                    var icon = {
                                      url: place.icon,
                                      size: new google.maps.Size(71, 71),
                                      origin: new google.maps.Point(0, 0),
                                      anchor: new google.maps.Point(17, 34),
                                      scaledSize: new google.maps.Size(25, 25)
                                    };

                                    // Create a marker for each place.
                                    markers.push(new google.maps.Marker
                                    ({
                                      map: map,
                                      icon: icon,
                                      title: place.name,
                                      position: place.geometry.location
                                    }));

                                    if (place.geometry.viewport) 
                                    {
                                      // Only geocodes have viewport.
                                      bounds.union(place.geometry.viewport);
                                    } else 
                                    {
                                      bounds.extend(place.geometry.location);
                                    }
                                  });
                                  map.fitBounds(bounds);
                                });
                              



                                myMarker.setMap(map);
                              }

                              // Re-init map before show modal
                              $('#Modal_entidades').on('show.bs.modal', function(event) {
                                var button = $(event.relatedTarget);
                                initializeGMap(20.1165413,-98.7413535);
                                $("#location-map").css("width", "100%");
                                $("#map_canvas").css("width", "100%");
                              });

                              // Trigger map resize event after modal shown
                              $('#Modal_entidades').on('shown.bs.modal', function() {
                                google.maps.event.trigger(map, "resize");
                                map.setCenter(myLatlng);
                              });





                              //-----------FIN MAPA


                              //activar modulos
                               $.ajax({
                                   type: "GET",
                                   datatype: 'json',
                                   contentType: "application/json; charset=utf-8",
                                  
                                   url: "/repositorio/wp-content/themes/EspecialesT2.1/templates/ctrl_modulos.php?nom_dep="+'<?php echo($user_name);?>',
                                   success: function(respuesta)
                                    {
                                      
                                      var arr_mod = respuesta.slice(1, -1);
                                      var arr_mod = arr_mod.split(",");


                                     
                                      var i;
                                      for (i = 0; i < arr_mod.length; i++) { 
                                        if(arr_mod[i]=="0")
                                        {
                                           $("#btn_tac_reg").removeAttr('disabled');
                                           $("#btn_est_reg").removeAttr('disabled');
                                         
                                        }
                                        else if(arr_mod[i]=="1")
                                        {
                                           $("#btn_tac_edit").removeAttr('disabled');
                                           $("#btn_est_edit").removeAttr('disabled');
                                        }

                                        else if(arr_mod[i]=="2")
                                        {
                                           $("#btn_tac_repdf").removeAttr('disabled');
                                           $("#btn_est_repdf").removeAttr('disabled');
                                        }

                                      
                                        console.log(arr_mod[i]);
                                      }

                                     
                                    },
                                    error: function(data)
                                    {

                                    }
                                   });

                              var nom_evidencia;
                              var indice=1;

                              $('#parte2').hide();
                              $('#otro_tipo_indicador').hide();
                              $('#upload_tabla_localidades').hide();        //upload file definir evidencia
                              $('#municipios_cargados').hide();             //ocultar mensaje de municpios seleccionados
                              $('#entidades_cargados').hide();              //entidades

                              
                              $('#otro_tipo_indicador').hide();             //ocultar opcion otro tipo evidencia
                              $('#rango_fecha_div').show();
                              $('#fecha_evidencia_fija').hide();
                              $('#tipo_evidencia_otro').hide();
                              $('#registro_terminado').hide();            //opcultar el mensaje de registro terminado
                              $('#rango_fecha_div').hide();
                              $('#fecha_evidencia_fija').show();          //mostrar la fecha fija
                              $('#parte2').show();
                              $('#parte1').hide();                       //mostrar la serie de la evidencia NO
                              $('#bullet_Del_logro').hide();              //Bullet del logro opcion NO por default
                              $('#premios_reconocimientos').hide();

                              $('#seccion_pids').hide();
                              $('#seccion_covid').hide();

                              

                              

                              //$('#edit_bullet_Del_logro_<?php echo($registro_1['id_indicador_asociado_def'])?>').hide();              //editable
                              

                              //mensajes de datos cargados de los lugares
                              $('#datos_cargados_lug_1').hide();
                              $('#datos_cargados_lug_2').hide();
                              $('#datos_cargados_lug_3').hide();
                              $('#datos_cargados_lug_4').hide();
                              $('#datos_cargados_lug_5').hide();
                              $('#datos_cargados_lug_6').hide();
                              $('#datos_cargados_lug_7').hide();
                              $('#datos_cargados_lug_8').hide();

                              $('#est_datos_cargados_lug_1').hide();
                              $('#est_datos_cargados_lug_2').hide();
                              $('#est_datos_cargados_lug_3').hide();
                              $('#est_datos_cargados_lug_4').hide();
                              $('#est_datos_cargados_lug_5').hide();

                              $('#area_adj_plan_trabajo').hide();           //ocultar plan de trabajo hasta q se carge la linea base si 

                              $('#div_est_descripcion_logro').hide();
                              $('#div_est_log').hide();
                              

                              

                              $('#indicadore_seleccionado_autollenado_tacticos').hide();
                              $('#indicadore_est_seleccionado_autollenado').hide();

                              
                              
                              $('#tacticos').hide();          //OCULTAR EL FORMULARIO DE PASOS DEL  INDICADOR TACTICO
                              $('#estrategicos').hide(); 
                              $('#avance_programatico').hide(); 
                               $('#acciones_concurrentes').hide(); 
                               $('#acciones_concurrentes_sin_indicadores').hide();

                              $('#doc_res2').hide(); 
                              $('#oculto_edit_tac').hide(); 
                              $('#covid').hide(); 




                              
                              var input_dependecia_Avance = document.getElementById("nombre_dependencia_avance");
                                    input_dependecia_Avance.value="<?php echo $user_name;?>";

                              var input_acciones_concurrente = document.getElementById("accion_concurrente_nom_dependencia");
                                    input_acciones_concurrente.value="<?php echo $user_name;?>";
/*
                              var input_acciones_sin_ind = document.getElementById("nom_dep_sin_ind");
                                    input_acciones_sin_ind.value="<?php echo $user_name;?>";


                                    
                                    */
                                    
                             
                              $( "#descripcion_documento_vinculado_indicador_def" ).keyup(function() 
                              {
                                /*
                                  tipo_evidencia_get_values();//obtner los vaores seleccionados del list tipo de evidencias
                                    var x = document.getElementById("nextBtn");
                                    x.setAttribute("type", "submit");
                                    */
                              });

                               $( "#discurso_importancia_ind" ).keyup(function() 
                              {

                                var nom_ind_loaded = document.getElementById("nombre_indicador_asociado_def");
                                var n_indicador=nom_ind_loaded.value;
                                $("#upload_doc_resultados_1").uploadFile
                                ({
                                  
                                    url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                                    fileName:"myfile",
                                    formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":1}  
                                  
                                }); 

                                $("#upload_doc_resultados_2").uploadFile
                                ({
                                  
                                    url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                                    fileName:"myfile",
                                    formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":2}  
                                  
                                }); 

                                $("#upload_doc_resultados_3").uploadFile
                                ({
                                  
                                    url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                                    fileName:"myfile",
                                    formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":3}  
                                  
                                }); 
                      
                                $("#upload_doc_resultados_4").uploadFile
                                ({
                                  
                                    url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                                    fileName:"myfile",
                                    formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":4}  
                                  
                                }); 
                                                        
                                $("#upload_doc_resultados_5").uploadFile
                                ({


                                  
                                    url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                                    fileName:"myfile",
                                    formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":5}  
                                  
                                }); 

                                $("#upload_doc_resultados_6").uploadFile
                                ({
                                  
                                    url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                                    fileName:"myfile",
                                    formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":6}  
                                  
                                }); 
                      
                                $("#upload_doc_resultados_7").uploadFile
                                ({
                                  
                                    url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                                    fileName:"myfile",
                                    formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":7}  
                                  
                                }); 
                                                        
                                $("#upload_doc_resultados_8").uploadFile
                                ({


                                  
                                    url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                                    fileName:"myfile",
                                    formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":8}  
                                  
                                }); 

                                $("#upload_plan_trabajo").uploadFile
                                ({

                                  
                                    url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                                    fileName:"myfile",
                                    formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":"plan"}  
                                  
                                }); 

                              $("#upload_adjuntar_evidencias").uploadFile
                                ({
                                    url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                                    fileName:"myfile",
                                    formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":"evidencias"}  
                                  
                                }); 



                              }); 
 
                             


                                $( "#estr_linea_discursiva_1" ).keyup(function() 
                                {

                                  var nom_ind_loaded = document.getElementById("est_nom_indicador");
                                var n_indicador=nom_ind_loaded.value;

                                  $("#est_upload_doc_resultados_1").uploadFile
                                  ({
                                      url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                                      fileName:"myfile",
                                      formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":"evidencias"}  
                                    
                                  }); 
                                    $("#est_upload_doc_resultados_2").uploadFile
                                  ({
                                      url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                                      fileName:"myfile",
                                      formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":"evidencias"}  
                                    
                                  }); 
                                    $("#est_upload_doc_resultados_3").uploadFile
                                  ({
                                      url:"/repositorio/upload_files_plugin/php/upload_files_por_indicador.php",
                                      fileName:"myfile",
                                      formData: {"nom_evidencia":n_indicador,"usuario":"<?php echo $user_name;?>","indice":"evidencias"}  
                                    
                                  }); 


                                 });


                              




                              



                               

                              ///------------alert("CALCULAR PORCENTAJES");
                              
                                    $.ajax({

                                     type: "GET",
                                     datatype: 'json',
                                     contentType: "application/json; charset=utf-8",
                                    
                                     url: "/repositorio/wp-content/themes/EspecialesT2.1/templates/porcentaje_llenado.php?nom_dependencia=<?php echo($user_name);?>",
                                     success: function(respuesta)
                                      {


                                        console.log(respuesta);
                                        var respuesta = $.parseJSON(respuesta);         //parcer la respuesta del ajax

                                        var tacticos_totales=respuesta.tac_total;
                                        var tacticos_completos=respuesta.tac_llenos;

                                        var estrategicos_totales=respuesta.estr_total;
                                        var estrategicos_completos=respuesta.estr_llenos;

                                        var ava_prog_totales=respuesta.avanc_prog_total;
                                        var ava_completos=respuesta.avanc_prog_llenos;

                                        var pid_total=respuesta.pids_total;
                                        var pid_registrados=respuesta.pids_registrados;

                                       

                                        var porcentaje_avance_tacticos;
                                        porcentaje_avance_tacticos=(tacticos_completos*100)/tacticos_totales; //calcular porcentaje

                                        var porcentaje_avance_estrategicos;
                                        porcentaje_avance_estrategicos=(estrategicos_completos*100)/estrategicos_totales; //calcular porcentaje  

                                        var porcentaje_avance_programatico;
                                        porcentaje_avance_programatico=(ava_completos*100)/ava_prog_totales; //calcular porcentaje  

                                        var porcentaje_tacticos_pid;
                                        porcentaje_tacticos_pid=(pid_registrados*100)/pid_total; //calcular porcentaje  



                                        var counter = document.getElementById('counter').getContext('2d');
                                        var no = 0; // Starting Point
                                        var pointToFill = 4.72;  //Point from where you want to fill the circle
                                        var cw = counter.canvas.width;  //Return canvas width
                                        var ch = counter.canvas.height; //Return canvas height
                                        var diff;   // find the different between current value (no) and trageted value (100)
                                        
                                        function fillCounter_tact()
                                        {
                                          if(respuesta.tac_total!=0)
                                          {


                                            diff = ((no/100) * Math.PI*2*10);
                                            
                                            counter.clearRect(0,0,cw,ch);   // Clear canvas every time when function is call
                                            
                                            counter.lineWidth = 15;     // size of stroke
                                            
                                            counter.fillStyle = '#000000';     // color that you want to fill in counter/circle
                                            
                                            counter.strokeStyle = '#5cb85c';    // Stroke Color


                                            
                                            counter.textAlign = 'center';
                                            
                                            counter.font = "20px monospace";    //set font size and face
                                            
                                            counter.fillText(no+'%',100,170);       //fillText(text,x,y);
                                            
                                            counter.beginPath();
                                            counter.arc(105,105,90,pointToFill,diff/10+pointToFill);    //arc(x,y,radius,start,stop)
                                            
                                            counter.stroke();   // to fill stroke
                                            
                                            // now add condition
                                            
                                            if(no >= porcentaje_avance_tacticos)
                                            {
                                                clearTimeout(fill);     //fill is a variable that call the function fillcounter_tact()
                                            }
                                            no++;
                                          }
                                        }
                                        
                                        //now call the function
                                        
                                        var fill = setInterval(fillCounter_tact,10);     //call the fillCounter function after every 50MS
                                        //BAR ESTRATEGICOS

                                    var counter_str = document.getElementById('counter_estrategico').getContext('2d');
                                        var no_str = 0; // Starting Point
                                        var pointToFill = 4.72;  //Point from where you want to fill the circle
                                        var cw_str = counter_str.canvas.width;  //Return canvas width
                                        var ch_str = counter_str.canvas.height; //Return canvas height
                                        var diff_str;   // find the diff_strerent between current value (no_str) and trageted value (100)
                                        
                                        function fillCounter_str(){
                                          if(respuesta.estr_total!=0)
                                          {
                                            diff_str = ((no_str/100) * Math.PI*2*10);
                                            
                                            counter_str.clearRect(0,0,cw_str,ch_str);   // Clear canvas every time when function is call
                                            
                                            counter_str.lineWidth = 15;     // size of stroke
                                            
                                            counter_str.fillStyle = '#000000';     // color that you want to fill in counter_str/circle
                                            
                                            counter_str.strokeStyle = '#5cb85c';    // Stroke Color


                                            
                                            counter_str.textAlign = 'center';
                                            
                                            counter_str.font = "20px monospace";    //set font size and face
                                            
                                            counter_str.fillText(no_str+'%',100,170);       //fillText(text,x,y);
                                            
                                            counter_str.beginPath();
                                            counter_str.arc(105,105,90,pointToFill,diff_str/10+pointToFill);    //arc(x,y,radius,start,stop)
                                            
                                            counter_str.stroke();   // to fill stroke
                                            
                                            // now add condition
                                            
                                            if(no_str >= porcentaje_avance_estrategicos)
                                            {
                                                clearTimeout(fill_str);     //fill_str is a variable that call the function fill_strcounter_str()
                                            }
                                            no_str++;
                                          }
                                        }
                                        
                                        //now call the function
                                        
                                        var fill_str = setInterval(fillCounter_str,10);     //call the 



                                    var counter_avance_prog = document.getElementById('counter_avance_prog').getContext('2d');
                                        var no_av_prog = 0; // Starting Point
                                        var pointToFill_ava = 4.72;  //Point from where you want to fill the circle
                                        var cw_ava = counter_avance_prog.canvas.width;  //Return canvas width
                                        var ch_ava = counter_avance_prog.canvas.height; //Return canvas height
                                        var diff_ava_prog;   // find the diff_ava_progerent between current value (no_av_prog) and trageted value (100)
                                        
                                        function fillCounter_avance_prog(){
                                          if(respuesta.avanc_prog_total!=0)
                                          {
                                            diff_ava_prog = ((no_av_prog/100) * Math.PI*2*10);
                                            
                                            counter_avance_prog.clearRect(0,0,cw_ava,ch_ava);   // Clear canvas every time when function is call
                                            
                                            counter_avance_prog.lineWidth = 15;     // size of stroke
                                            
                                            counter_avance_prog.fillStyle = '#000000';     // color that you want to fill in counter_avance_prog/circle
                                            
                                            counter_avance_prog.strokeStyle = '#5cb85c';    // Stroke Color


                                            
                                            counter_avance_prog.textAlign = 'center';
                                            
                                            counter_avance_prog.font = "20px monospace";    //set font size and face
                                            
                                            counter_avance_prog.fillText(no_av_prog+'%',100,170);       //fillText(text,x,y);
                                            
                                            counter_avance_prog.beginPath();
                                            counter_avance_prog.arc(105,105,90,pointToFill_ava,diff_ava_prog/10+pointToFill_ava);    //arc(x,y,radius,start,stop)
                                            
                                            counter_avance_prog.stroke();   // to fill stroke
                                            
                                            // now add condition
                                            
                                            if(no_av_prog >= porcentaje_avance_programatico)
                                            {
                                                clearTimeout(fill_ava);     //fill_ava is a variable that call the function fill_avacounter_avance_prog()
                                            }
                                            no_av_prog++;
                                          }
                                        }
                                        
                                        //now call the function
                                        
                                        var fill_ava = setInterval(fillCounter_avance_prog,10);     //call the 


                                        /*--------barra progreso PIDS --*/

                                        var counter_pid = document.getElementById('counter_pid').getContext('2d');
                                        var no_pid = 0; // Starting Point
                                        var pointToFill_pid = 4.72;  //Point from where you want to fill the circle
                                        var cw_pid = counter_pid.canvas.width;  //Return canvas width
                                        var ch_pid = counter_pid.canvas.height; //Return canvas height
                                        var diff_pid;   // find the diff_piderent between current value (no_pid) and trageted value (100)
                                        
                                        function fillCounter_pid(){
                                          if(respuesta.pid_total!=0)
                                          {
                                            diff_pid = ((no_pid/100) * Math.PI*2*10);
                                            
                                            counter_pid.clearRect(0,0,cw_pid,ch_pid);   // Clear canvas every time when function is call
                                            
                                            counter_pid.lineWidth = 15;     // size of stroke
                                            
                                            counter_pid.fillStyle = '#000000';     // color that you want to fill in counter_pid/circle
                                            
                                            counter_pid.strokeStyle = '#5cb85c';    // Stroke Color


                                            
                                            counter_pid.textAlign = 'center';
                                            
                                            counter_pid.font = "15px monospace";    //set font size and face
                                            
                                            counter_pid.fillText(no_pid+'%',100,170);       //fillText(text,x,y);
                                            
                                            counter_pid.beginPath();
                                            counter_pid.arc(105,105,90,pointToFill_pid,diff_pid/10+pointToFill_pid);    //arc(x,y,radius,start,stop)
                                            
                                            counter_pid.stroke();   // to fill stroke
                                            
                                            // now add condition
                                            
                                            if(no_pid >= porcentaje_tacticos_pid)
                                            {
                                                clearTimeout(fill_pid);     //fill_pid is a variable that call the function fill_pidcounter_str()
                                            }
                                            no_pid++;
                                          }
                                        }
                                        
                                        //now call the function
                                        
                                        var fill_pid = setInterval(fillCounter_pid,10);     //call the 




                                        /*---------fin pids---------*/







                                      },
                                      error: function(data)
                                      {


                                      }
                                     });
                              
                            });



                        </script>



                        <!-- Tabs-->
                        <script type="text/javascript">

                          /* Ocultar los tabs verticales no se porq no se ocultan al inicio
                          */
                          /*
                            tabcontent_vertical = document.getElementsByClassName("tabcontent_vertical");
                            for (i = 0; i < tabcontent_vertical.length; i++) {
                                tabcontent_vertical[i].style.display = "none";
                            }

                            */
                          /* fin ocultar tabsvertical*/
                            //open_tabs(event, 'EVIDENCIA');
                        
                            function open_tabs(evt, nom_tab) 
                            {
                              $('#intro_idicador_tactico').hide();// quitar el mensaje de intro
                              
                                // Declare all variables
                                var i, tabcontent, tablinks;

                                // Get all elements with class="tabcontent" and hide them
                                tabcontent = document.getElementsByClassName("tabcontent");
                                for (i = 0; i < tabcontent.length; i++) {
                                    tabcontent[i].style.display = "none";
                                }

                                // Get all elements with class="tablinks" and remove the class "active"
                                tablinks = document.getElementsByClassName("tablinks");
                                for (i = 0; i < tablinks.length; i++) {
                                    tablinks[i].className = tablinks[i].className.replace(" active", "");
                                }

                                // Show the current tab, and add an "active" class to the button that opened the tab
                                document.getElementById(nom_tab).style.display = "block";
                                evt.currentTarget.className += " active";
                            }

                            //document.getElementById("defaultOpen").click();



                            /* fin tabs*/

                            function open_vertical_tab(evt, nomb_vert_tab) {


                                var i, tabcontent_vertical, tablinks_vertical;
                                tabcontent_vertical = document.getElementsByClassName("tabcontent_vert");
                                for (i = 0; i < tabcontent_vertical.length; i++) {
                                    tabcontent_vertical[i].style.display = "none";
                                }
                                tablinks_vertical = document.getElementsByClassName("tablinks_vertical");
                                for (i = 0; i < tablinks_vertical.length; i++) {
                                    tablinks_vertical[i].className = tablinks_vertical[i].className.replace(" active", "");
                                }
                               
                                document.getElementById(nomb_vert_tab).style.display = "block";
                                evt.currentTarget.className += " active";


                            }
                                  /*  Acordeon*/

                            var acc = document.getElementsByClassName("accordion");
                            var i;

                            for (i = 0; i < acc.length; i++) {
                                acc[i].onclick = function(){
                                    /* Toggle between adding and removing the "active" class,
                                    to highlight the button that controls the panel */
                                    this.classList.toggle("active");

                                    /* Toggle between hiding and showing the active panel */
                                    var panel = this.nextElementSibling;
                                    if (panel.style.display === "block") {
                                        panel.style.display = "none";
                                    } else {
                                        panel.style.display = "block";
                                    }
                                }
                            }

                            document.getElementById("defaultOpen_vertical").click();

                            /* fin acordeon */
                        </script>
                        <!--fin tabs-->
                        <!--STEPS-->
                        <script>
                          var currentTab = 0; // Current tab is set to be the first tab (0)
                          showTab(currentTab); // Display the crurrent tab

                          function showTab(n) 
                          {

                            // This function will display the specified tab of the form...
                            var x = document.getElementsByClassName("tab");
                            x[n].style.display = "block";
                            //... and fix the Previous/Next buttons:
                            if (n == 0) {
                              document.getElementById("prevBtn").style.display = "none";
                            } else {
                              document.getElementById("prevBtn").style.display = "inline";
                            }
                             
                            if (n == (x.length - 1)) 
                            {
                              document.getElementById("nextBtn").innerHTML = "Enviar";
                             // //alert("se agrega el submit");

                            }
                  
                         

                            else 
                            {
                              //$('#fileuploader').hide();
                              document.getElementById("nextBtn").innerHTML = "Siguiente";
                            }


                            //... and run a function that will display the correct step indicator:
                            fixStepIndicator(n)


                            //Hardcoreo cuando este en el ultimo tab convertir rl boton a tipo submit
                          }

                          function nextPrev(n) 
                          {
       
                            // This function will figure out which tab to display
                            var x = document.getElementsByClassName("tab");
                            // Exit the function if any field in the current tab is invalid:
                            if (n == 1 && !validateForm()) return false;
                            // Hide the current tab:
                            x[currentTab].style.display = "none";
                            // Increase or decrease the current tab by 1:
                            currentTab = currentTab + n;
                            // if you have reached the end of the form...

                            //alert("limite:"+x.length+"  pagina"+currentTab);

                            if (currentTab >= (x.length)) {
                              
                               tipo_evidencia_get_values();//obtner los vaores seleccionados del list tipo de evidencias
                                    var x = document.getElementById("nextBtn");
                                    x.setAttribute("type", "submit");
                                    return false;

                                    

                              // ... the form gets submitted:

                            //  document.getElementById("nextBtn").innerHTML = "Enviar";
                             // return false;
                            }
                           // alert(currentTab);

                            if(currentTab>=6)
                            {
                             // alert("obtener los valores");
                              tipo_evidencia_get_values();//obtner los vaores seleccionados del list tipo de 
                           
                            }
                            

                           
                

                            showTab(currentTab);
                          }


                          function tipo_evidencia_get_values()
                          {
                           // alert("AAAAAAAA");
                            var arr_checks = document.getElementsByClassName("check_type_evi");
                            var tipo_evidencia_def="";

                              for (i = 0; i < arr_checks.length; i++) 
                            {

                              if(arr_checks[i].checked == true)
                              {
                                tipo_evidencia_def+=arr_checks[i].name+",";
                              
                                
                              }

                            }
                            var tipo_evi_ =document.getElementById("tipo_evidencia_def_list");
                            tipo_evi_.value= tipo_evidencia_def;
                        
                          }




                          function validateForm() 
                          {
                            // This function deals with validation of the form fields

                            var x, y, i, valid = true;
                            x = document.getElementsByClassName("tab");

                            /*
                              y = x[currentTab_avance].getElementsByTagName("input");
                                // A loop that checks every input field in the current tab_avance:
                                for (i = 0; i < y.length; i++) {
                                  // If a field is empty...
                                  if (y[i].value == "") {
                                    // add an "invalid" class to the field:
                                    y[i].className += " invalid";
                                    // and set the current valid status to false:
                                    valid = false;
                                  }
                                }
                            */
                            
                            y = x[currentTab].getElementsByTagName("input");
                            // A loop that checks every input field in the current tab:
                            for (i = 0; i < y.length; i++) 
                            {


                              if (y[i].value == "") 
                              {

                                if(y[i].id=="otro_tipo_indicador_def")
                                {

                                }
                                else if(y[i].id=="res1_lugares")
                                {
                                  
                                 document.getElementById('datos_cargados_lug_1').innerHTML ="Define el lugar";
                                 var element = document.getElementById("datos_cargados_lug_1");
                                 element.classList.remove("datos_cargados_lugar_resultados");
                                 element.classList.add("error_mensaje"); 
                                 $('#datos_cargados_lug_1').show();

                                  y[i].className += " invalid";
                                  valid = false;
                                  
                                }

                                else if(y[i].id=="res2_lugares")
                                {
                                  /*
                                 document.getElementById('datos_cargados_lug_2').innerHTML ="Define el lugar";
                                 var element = document.getElementById("datos_cargados_lug_2");
                                 element.classList.remove("datos_cargados_lugar_resultados");
                                 element.classList.add("error_mensaje"); 
                                 $('#datos_cargados_lug_2').show();

                                 y[i].className += " invalid";
                                  valid = false;
                                  */

                                }
                                else if(y[i].id=="res3_lugares")
                                {
                                }
                                else if(y[i].id=="res4_lugares")
                                {
                                }
                                else if(y[i].id=="res5_lugares")
                                {
                                }
                                else if(y[i].id=="res6_lugares")
                                {
                                }
                                else if(y[i].id=="res7_lugares")
                                {
                                }

                                else if(y[i].id=="res8_lugares")
                                {
                                }

                                else if(y[i].id=="res1_lugares_gps")
                                {
                                }
                                else if(y[i].id=="res2_lugares_gps")
                                {
                                }
                                else if(y[i].id=="res3_lugares_gps")
                                {
                                }
                                else if(y[i].id=="res4_lugares_gps")
                                {
                                }
                                else if(y[i].id=="res5_lugares_gps")
                                {
                                }
                                else if(y[i].id=="res6_lugares_gps")
                                {
                                }
                                else if(y[i].id=="res7_lugares_gps")
                                {
                                }
                                else if(y[i].id=="res8_lugares_gps")
                                {
                                }
                               

                                else if(y[i].id=="")
                                {
         
                                }

                                else if(y[i].id=="serie_evidencia_def")
                                {

                                }
                                else if(y[i].id=="seccion_def")
                                {

                                }
                                 else if(y[i].id=="guia_invetario_expediente_def")
                                {

                                }
                     
                                else if(y[i].name=="myfile[]")
                                {

                                }
                                else if(y[i].name=="tipo_evidencia_otro_def")
                                {

                                }
                                else if(y[i].name=="tipo_evidencia_def_list")
                                {

                                }
                                else if(y[i].name=="cve_unica"){}
                                else if(y[i].name=="cime"){}
                                else if(y[i].name=="tablero"){}
                                else if(y[i].name=="informe2019"){}
                                else if(y[i].name=="eje_ped"){}
                                else if(y[i].name=="ob_est"){}
                                else if(y[i].name=="ob_gen"){}
                                else if(y[i].name=="ods"){}
                                else if(y[i].name=="ob_secto"){}
                                else if(y[i].name=="periodisidad"){}
                                else if(y[i].name=="fuente"){}
                                else if(y[i].name=="ref_ad"){}



                                else
                                {
                                  // add an "invalid" class to the field:
                                  y[i].className += " invalid";
                                  // and set the current valid status to false:
                                  valid = false;
                                }
      
                              }


                                else if(y[i].name=="myfile[]")
                                {

                                }

                            

                              else if (y[i].id == "nombre_indicador_asociado_def" && y[i].value.length < 1 ) 
                              {
                                  //nombre del indicador
                                 document.getElementById('error_nombre_indicador_asociado_def').innerHTML ="Verifica el nombre de tu indicador";
                                 var element = document.getElementById("error_nombre_indicador_asociado_def");
                                 element.classList.add("error_mensaje");
                                  // add an "invalid" class to the field:
                                y[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }
                              else if (y[i].id == "unidad_observacion_indicador_def" && y[i].value.length >70 ) 
                              {
                                  //nombre del indicador
                                 document.getElementById('error_unidad_observacion_indicador_def').innerHTML ="El limite del campo es de 70 caracteres";
                                 var element = document.getElementById("error_unidad_observacion_indicador_def");
                                 element.classList.add("error_mensaje");
                                  // add an "invalid" class to the field:
                                y[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }
                              /*
                              else if (y[i].id == "linea_base_absolutos_def" ) //validar q sea solo numeros
                              {
                                  //nombre del indicador

                                  if (y[i].value.match("((-|\\+)?[0-9]+(\\.[0-9]+)?)+")) 
                                  {
                                    //alert("Its a boy");
                                    //  System.out.println("Is a number");
                                      y[i].className += " valid";
                                      valid = true;
                                  } else 
                                  {
                                    //  System.out.println("Is not a number");
                                      document.getElementById('error_linea_base_absolutos_def').innerHTML ="El valor tiene que ser numerico";
                                       var element = document.getElementById("error_linea_base_absolutos_def");
                                       element.classList.add("error_mensaje");
                                        // add an "invalid" class to the field:
                                      y[i].className += " invalid";
                                      // and set the current valid status to false
                                      valid = false;
                                  }
                                 
                              }
                              */


                              

                              else if (y[i].id == "linea_base_tipo_indicador_def" && y[i].value.length >10 ) 
                              {
                                  //nombre del indicador
                                 document.getElementById('error_linea_base_tipo_indicador_def').innerHTML ="El limite del campo es de 10 caracteres";
                                 var element = document.getElementById("error_linea_base_tipo_indicador_def");
                                 element.classList.add("error_mensaje");
                                  // add an "invalid" class to the field:
                                y[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }



                              else if (y[i].id == "avance_2018" && y[i].value.length >10 ) 
                              {
                                  //nombre del indicador
                                 document.getElementById('error_avance_2018').innerHTML ="El limite del campo es de 10 caracteres";
                                 var element = document.getElementById("error_avance_2018");
                                 element.classList.add("error_mensaje");
                                  // add an "invalid" class to the field:
                                y[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }


                                 else if (y[i].id == "meta_2019" && y[i].value.length >10 ) 
                              {
                                  //nombre del indicador
                                 document.getElementById('error_meta_2019').innerHTML ="El limite del campo es de 10 caracteres";
                                 var element = document.getElementById("error_meta_2019");
                                 element.classList.add("error_mensaje");
                                  // add an "invalid" class to the field:
                                y[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }



                              else if (y[i].id == "resultado_18" && y[i].value.length >10 ) 
                              {
                                  //nombre del indicador
                                 document.getElementById('error_resultado_18').innerHTML ="El limite del campo es de 10 caracteres";
                                 var element = document.getElementById("error_resultado_18");
                                 element.classList.add("error_mensaje");
                                  // add an "invalid" class to the field:
                                y[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }
                              else if (y[i].id == "avance_meta_planeada_def" && y[i].value.length >10 ) 
                              {
                                  //nombre del indicador
                                 document.getElementById('error_avance_meta_planeada_def').innerHTML ="El limite del campo es de 10 caracteres";
                                 var element = document.getElementById("error_avance_meta_planeada_def");
                                 element.classList.add("error_mensaje");
                                  // add an "invalid" class to the field:
                                y[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }

                              else if (y[i].id == "fecha_evidencia_inicio_def" && y[i].value.length !=10 ) 
                              {
                                  //nombre del indicador
                                 document.getElementById('error_fecha_evidencia_inicio_def').innerHTML ="Verifica tu informacion";
                                 var element = document.getElementById("error_fecha_evidencia_inicio_def");
                                 element.classList.add("error_mensaje");
                                  // add an "invalid" class to the field:
                                y[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }
                               else if (y[i].id == "fecha_evidencia_fin_def" && y[i].value.length !=10 ) 
                              {
                                  //nombre del indicador
                                 document.getElementById('error_fecha_evidencia_fin_def').innerHTML ="Verifica tu informacion";
                                 var element = document.getElementById("error_fecha_evidencia_fin_def");
                                 element.classList.add("error_mensaje");
                                  // add an "invalid" class to the field:
                                y[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }

                               else if (y[i].id == "serie_evidencia_def" && y[i].value.length >100 ) 
                              {
                                  //nombre del indicador
                                 document.getElementById('error_serie_evidencia_def').innerHTML ="El limite del campo es de 100 caracteres";
                                 var element = document.getElementById("error_serie_evidencia_def");
                                 element.classList.add("error_mensaje");
                                  // add an "invalid" class to the field:
                                y[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }

                               else if (y[i].id == "seccion_def" && y[i].value.length >200 ) 
                              {
                                  //nombre del indicador
                                 document.getElementById('error_seccion_def').innerHTML ="El limite del campo es de 200 caracteres";
                                 var element = document.getElementById("error_seccion_def");
                                 element.classList.add("error_mensaje");
                                  // add an "invalid" class to the field:
                                y[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }


                               else if (y[i].id == "guia_invetario_expediente_def" && y[i].value.length >200 ) 
                              {
                                  //nombre del indicador
                                 document.getElementById('error_guia_invetario_expediente_def').innerHTML ="El limite del campo es de 200 caracteres";
                                 var element = document.getElementById("error_guia_invetario_expediente_def");
                                 element.classList.add("error_mensaje");
                                  // add an "invalid" class to the field:
                                y[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }


                            }

                            txtarea = x[currentTab].getElementsByTagName("textarea");
                            // A loop that checks every input field in the current tab:
                            for (i = 0; i < txtarea.length; i++) 
                            {

                             
                              // If a field is empttxtarea...
                              if (txtarea[i].value == "") 
                              {



                                if(txtarea[i].id=="id_bullet_Del_logro" )
                                {

                                }
                                else if(txtarea[i].id=="resultado_2")
                                {

                                }
                                else if(txtarea[i].id=="resultado_3")
                                {

                                }
                                else if(txtarea[i].id=="resultado_4")
                                {

                                }
                                else if(txtarea[i].id=="resultado_5")
                                {

                                }
                                else if(txtarea[i].id=="resultado_6")
                                {

                                }
                                else if(txtarea[i].id=="resultado_7")
                                {

                                }
                                else if(txtarea[i].id=="resultado_8")
                                {

                                }

                                else if(txtarea[i].id=="lo_que_sigue_2")
                                {

                                }
                                else if(txtarea[i].id=="lo_que_sigue_3")
                                {

                                }
                                
                                  else if(txtarea[i].id=="medios_verificacion_evidencia")
                                {

                                }
    
                                else if(txtarea[i].id=="comentarios_def")
                                {

                                }
                                else if(txtarea[i].id=="reconocimiento")
                                {

                                }
                                else
                                {
                                                              // add an "invalid" class to the field:
                                txtarea[i].className += " invalid";
                                // and set the current valid status to false:
                                valid = false;
                                }


                              }

                              else if (txtarea[i].id == "reconocimiento" && txtarea[i].value.length >= 450 ) 
                              {
                                  //nombre del indicador
                                 document.getElementById('error_reconocimiento').innerHTML ="El limite del campo es de 450 caracteres";
                                 var element = document.getElementById("error_reconocimiento");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }

                           

                              //descripcion del indicador
                              else if (txtarea[i].id == "descripcion_indicador_dif" && txtarea[i].value.length >= 500 ) 
                              {
                                  //nombre del indicador
                                 document.getElementById('error_descripcion_indicador_def').innerHTML ="El limite del campo es de 450 caracteres";
                                 var element = document.getElementById("error_descripcion_indicador_def");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }

                              else if (txtarea[i].id == "descripcion_documento_vinculado_indicador_def" && txtarea[i].value.length >= 450 ) 
                              {
                                  //nombre del indicador
                                 document.getElementById('error_descripcion_documento_vinculado_indicador_asociado_def').innerHTML ="El limite del campo es de 450 caracteres";
                                 var element = document.getElementById("error_descripcion_documento_vinculado_indicador_asociado_def");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }
                              

                              else if (txtarea[i].id == "comentarios_def" && txtarea[i].value.length >= 450 ) 
                              {
                                  //nombre del indicador
                                 document.getElementById('error_comentarios').innerHTML ="El limite del campo es de 450 caracteres";
                                 var element = document.getElementById("error_comentarios");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }



                              else if (txtarea[i].id == "medios_verificacion_evidencia" && txtarea[i].value.length >= 350 ){
                                  //nombre del indicador
                                 document.getElementById('error_medios_comunicacion').innerHTML ="El limite del campo es de 350 caracteres";
                                 var element = document.getElementById("error_medios_comunicacion");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }

                              //-----paso1-----------------------
                              else if (txtarea[i].id == "discurso_importancia_ind" && txtarea[i].value.length >= 350 ){
                                  //nombre del indicador
                                 document.getElementById('error_discurso_importancia_ind').innerHTML ="El limite del campo es de 350 caracteres";
                                 var element = document.getElementById("error_discurso_importancia_ind");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }

                              else if (txtarea[i].id == "id_bullet_Del_logro" && txtarea[i].value.length >= 450 ){
                                  //nombre del indicador
                                 document.getElementById('error_id_bullet_Del_logro').innerHTML ="El limite del campo es de 450 caracteres";
                                 var element = document.getElementById("error_id_bullet_Del_logro");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }

                              
                              //fin paso 1-----------------------------------

                              //paso 2----------------------------------------
                                 
                              else if (txtarea[i].id == "resultado_1" && txtarea[i].value.length >= 350 ){
                                  //nombre del indicador
                                 document.getElementById('error_resultado_1').innerHTML ="El limite del campo es de 350 caracteres";
                                 var element = document.getElementById("error_resultado_1");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }
                                 
                              else if (txtarea[i].id == "resultado_2" && txtarea[i].value.length >= 350 ){
                                  //nombre del indicador
                                 document.getElementById('error_resultado_2').innerHTML ="El limite del campo es de 350 caracteres";
                                 var element = document.getElementById("error_resultado_2");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }
                                 
                              else if (txtarea[i].id == "resultado_3" && txtarea[i].value.length >= 350 ){
                                  //nombre del indicador
                                 document.getElementById('error_resultado_3').innerHTML ="El limite del campo es de 350 caracteres";
                                 var element = document.getElementById("error_resultado_3");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }

                              else if (txtarea[i].id == "resultado_4" && txtarea[i].value.length >= 350 ){
                                  //nombre del indicador
                                 document.getElementById('error_resultado_4').innerHTML ="El limite del campo es de 350 caracteres";
                                 var element = document.getElementById("error_resultado_4");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }

                              else if (txtarea[i].id == "resultado_5" && txtarea[i].value.length >= 350 ){
                                  //nombre del indicador
                                 document.getElementById('error_resultado_5').innerHTML ="El limite del campo es de 350 caracteres";
                                 var element = document.getElementById("error_resultado_5");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }

                              else if (txtarea[i].id == "resultado_6" && txtarea[i].value.length >= 350 ){
                                  //nombre del indicador
                                 document.getElementById('error_resultado_6').innerHTML ="El limite del campo es de 350 caracteres";
                                 var element = document.getElementById("error_resultado_6");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }
                              else if (txtarea[i].id == "resultado_7" && txtarea[i].value.length >= 350 ){
                                  //nombre del indicador
                                 document.getElementById('error_resultado_7').innerHTML ="El limite del campo es de 350 caracteres";
                                 var element = document.getElementById("error_resultado_7");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }
                              else if (txtarea[i].id == "resultado_8" && txtarea[i].value.length >= 350 ){
                                  //nombre del indicador
                                 document.getElementById('error_resultado_8').innerHTML ="El limite del campo es de 350 caracteres";
                                 var element = document.getElementById("error_resultado_8");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }
                              else if (txtarea[i].id == "resultado_6" && txtarea[i].value.length >= 350 ){
                                  //nombre del indicador
                                 document.getElementById('error_resultado_6').innerHTML ="El limite del campo es de 350 caracteres";
                                 var element = document.getElementById("error_resultado_6");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }
                              else if (txtarea[i].id == "resultado_7" && txtarea[i].value.length >= 350 ){
                                  //nombre del indicador
                                 document.getElementById('error_resultado_7').innerHTML ="El limite del campo es de 350 caracteres";
                                 var element = document.getElementById("error_resultado_7");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }
                              else if (txtarea[i].id == "resultado_8" && txtarea[i].value.length >= 350 ){
                                  //nombre del indicador
                                 document.getElementById('error_resultado_8').innerHTML ="El limite del campo es de 350 caracteres";
                                 var element = document.getElementById("error_resultado_8");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }

                              else if (txtarea[i].id == "lo_que_sigue_1" && txtarea[i].value.length >= 450 ){



                                alert("mayor de 450");
                                  //nombre del indicador
                                 document.getElementById('error_lo_que_sigue_1').innerHTML ="El limite del campo es de 450 caracteres";
                                 var element = document.getElementById("error_lo_que_sigue_1");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }
                                 
                              else if (txtarea[i].id == "lo_que_sigue_2" && txtarea[i].value.length >= 450 ){
                                  //nombre del indicador
                                 document.getElementById('error_lo_que_sigue_2').innerHTML ="El limite del campo es de 450 caracteres";
                                 var element = document.getElementById("error_lo_que_sigue_2");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }
                              else if (txtarea[i].id == "lo_que_sigue_3" && txtarea[i].value.length >= 450 ){
                                  //nombre del indicador
                                 document.getElementById('error_lo_que_sigue_3').innerHTML ="El limite del campo es de 450 caracteres";
                                 var element = document.getElementById("error_lo_que_sigue_3");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid";
                                // and set the current valid status to false
                                valid = false;
                              }
                              //fin paso2 ------------------------------------
                              //paso 3 -------------------------------------

                              
                              else if (txtarea[i].id == "descripcion_obj_secto" && txtarea[i].value.length >= 450 ){
                                  //nombre del indicador
                                 document.getElementById('error_descripcion_obj_secto').innerHTML ="El limite del campo es de 450 caracteres";
                                 var element = document.getElementById("error_descripcion_obj_secto");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += "invalid";
                                // and set the current valid status to false
                                valid = false;
                              }
                              //fin paso 3-----------------------------------

                            }
                            
                            // If the valid status is true, mark the step as finished and valid:
                            if (valid) {
                              //alert("ya todo esta validadp");
                              document.getElementsByClassName("step")[currentTab].className += " finish";
                              $('#error_nombre_indicador').hide();


                            }
                            return valid; // return the valid status
                          }

                          function fixStepIndicator(n) 
                          {
                            // This function removes the "active" class of all steps...
                            var i, x = document.getElementsByClassName("step");
                            for (i = 0; i < x.length; i++) {
                              x[i].className = x[i].className.replace(" active", "");
                            }
                            //... and adds the "active" class on the current step:
                            x[n].className += " active";
                          }
                        </script>

                        <!-- pasos 2 estrategicos-->
                        <script type="text/javascript">
                          

                          var currentTab_est = 0; // Current tab_est is set to be the first tab_est (0)
                          showTab_est(currentTab_est); // Display the current tab_est
                          var contador_pasos =0;
                          var listo_eviar=false;

                          function showTab_est(n) {
                            // This function will display the specified tab_est of the form ...
                            var x = document.getElementsByClassName("tab_est");
                            x[n].style.display = "block";
                            // ... and fix the Previous/Next buttons:
                            if (n == 0) {
                              document.getElementById("prevBtn_est").style.display = "none";
                            } else {
                              document.getElementById("prevBtn_est").style.display = "inline";
                            }
                            if (n == (x.length - 1)) {
                             
                              document.getElementById("nextBtn_est").innerHTML = "Enviar";
                              listo_eviar=true;

                            } else {
                              document.getElementById("nextBtn_est").innerHTML = "Siguiente";
                            }
                            // ... and run a function that displays the correct step_est indicator:

                        /*
                                    var x = document.getElementById("nextBtn_est");
                                    x.setAttribute("type", "submit");

                         */

                            fixStep_estIndicator(n)
                          }

                          function nextPrev_est(n) {
                            

                            // This function will figure out which tab_est to display
                            var x = document.getElementsByClassName("tab_est");
                            // Exit the function if any field in the current tab_est is invalid_est:
                            if (n == 1 && !validateForm_est()) return false;
                            // Hide the current tab_est:
                            x[currentTab_est].style.display = "none";
                            // Increase or decrease the current tab_est by 1:
                            currentTab_est = currentTab_est + n;
                            // if you have reached the end of the form... :
                            if (currentTab_est >= x.length) 
                            {
                              //quitar el disabled del input
                             

                              
                              document.getElementById('est_linea_base').disabled = false;
                               document.getElementById('meta_2030').disabled = false;
                                document.getElementById('edit_est_meta_2022').disabled = false;

                              
                             // alert("listo para enviar");
                                 var x = document.getElementById("nextBtn_est");
                                    x.setAttribute("type", "submit");

                 
                              
                              
                              //...the form gets submitted:
                             // document.getElementById("regForm").submit();
                              //return false;
                            }


                            // Otherwise, display the correct tab_est:
                            

                   

                              showTab_est(currentTab_est);


                          }

                          function validateForm_est() {
                            // This function deals with validation of the form fields
                            var x, y, i, valid_est = true;
                            x = document.getElementsByClassName("tab_est");
                            y = x[currentTab_est].getElementsByTagName("input");
                            // A loop that checks every input field in the current tab_est:
                              for (i = 0; i < y.length; i++) 
                              {
                                
                                // If a field is empty...
                                if (y[i].value == "") 
                                {
                                  if(y[i].id=="otro_tipo_indicador_def")
                                  {

                                  }
                                  else if(y[i].id=="meta_2022")
                                  {

                                  }
                                  else if(y[i].id=="meta_2030")
                                  {

                                  }
                                  else if(y[i].id=="est_res1_lugares")
                                  {

                                  }
                                  else if(y[i].id=="est_res2_lugares")
                                  {

                                  }
                                  else if(y[i].id=="est_res3_lugares")
                                  {

                                  }
                                    else if(y[i].id=="est_res4_lugares")
                                  {

                                  }
                                    else if(y[i].id=="est_res5_lugares")
                                  {

                                  }

                                 

                                  



                                  else if(y[i].id=="est_res2_lugares_gps")
                                  {
                                  }
                                  else if(y[i].id=="est_res3_lugares_gps")
                                  {
                                  }
                                  else if(y[i].id=="est_res4_lugares_gps")
                                  {
                                  }
                                  else if(y[i].id=="est_res5_lugares_gps")
                                  {
                                  }
                                  else if(y[i].id=="est_res6_lugares_gps")
                                  {
                                  }
                                  else if(y[i].id=="est_res7_lugares_gps")
                                  {
                                  }
                                  else if(y[i].id=="est_res8_lugares_gps")
                                  {
                                  }
                                  
                                  else
                                  {
                                     // add an "invalid_est" class to the field:
                                    y[i].className += " invalid_est";
                                    // and set the current valid status to false:
                                    valid_est = false;
                                  }
                                }

                                else if (y[i].id == "est_nom_indicador" && y[i].value.length < 6 ) 
                                {
                                    //nombre del indicador
                                   document.getElementById('error_est_nom_indicador').innerHTML ="Verifica el nombre de tu indicador";
                                   var element = document.getElementById("error_est_nom_indicador");
                                   element.classList.add("error_mensaje");
                                        // add an "invalid_est" class to the field:
                                  y[i].className += " invalid_est";
                                  // and set the current valid status to false:
                                  valid_est = false;
                                }

                                else if (y[i].id == "est_linea_base" && y[i].value.length >10 ) 
                                {
                                    //nombre del indicador
                                   document.getElementById('error_est_linea_base').innerHTML ="el limite es de 10 caracteres";
                                   var element = document.getElementById("error_est_linea_base");
                                   element.classList.add("error_mensaje");
                                        // add an "invalid_est" class to the field:
                                  y[i].className += " invalid_est";
                                  // and set the current valid status to false:
                                  valid_est = false;
                                }
                                else if (y[i].id == "result_2017" && y[i].value.length >10 ) 
                                {
                                    //nombre del indicador
                                   document.getElementById('error_result_2017').innerHTML ="el limite es de 10 caracteres";
                                   var element = document.getElementById("error_result_2017");
                                   element.classList.add("error_mensaje");
                                        // add an "invalid_est" class to the field:
                                  y[i].className += " invalid_est";
                                  // and set the current valid status to false:
                                  valid_est = false;
                                }
                                else if (y[i].id == "meta_2018" && y[i].value.length >10 ) 
                                {
                                    //nombre del indicador
                                   document.getElementById('error_meta_2018').innerHTML ="el limite es de 10 caracteres";
                                   var element = document.getElementById("error_meta_2018");
                                   element.classList.add("error_mensaje");
                                        // add an "invalid_est" class to the field:
                                  y[i].className += " invalid_est";
                                  // and set the current valid status to false:
                                  valid_est = false;
                                }
                                else if (y[i].id == "est_res_abril" && y[i].value.length >10 ) 
                                {
                                    //nombre del indicador
                                   document.getElementById('error_est_res_abril').innerHTML ="el limite es de 10 caracteres";
                                   var element = document.getElementById("error_est_res_abril");
                                   element.classList.add("error_mensaje");
                                        // add an "invalid_est" class to the field:
                                  y[i].className += " invalid_est";
                                  // and set the current valid status to false:
                                  valid_est = false;
                                }
                                else if (y[i].id == "est_avance_meta_est" && y[i].value.length >10 ) 
                                {
                                    //nombre del indicador
                                   document.getElementById('error_est_avance_meta_est').innerHTML ="el limite es de 10 caracteres";
                                   var element = document.getElementById("error_est_avance_meta_est");
                                   element.classList.add("error_mensaje");
                                        // add an "invalid_est" class to the field:
                                  y[i].className += " invalid_est";
                                  // and set the current valid status to false:
                                  valid_est = false;
                                }
                              }

            
                            txtarea = x[currentTab_est].getElementsByTagName("textarea");
                              for (i = 0; i < txtarea.length; i++) 
                            {

                             // alert("validar textarea");
                               if (txtarea[i].value == "") 
                                {
                                  if(txtarea[i].id=="est_descripcion_logro")
                                  {

                                  }

                                  else if(txtarea[i].id=="est_bullet_logro")
                                  {

                                  }
                                  else if(txtarea[i].id=="est_descripcion_logro")
                                  {

                                  }


                                  else if(txtarea[i].id=="est_resultado2")
                                  {

                                  }
                                  else if(txtarea[i].id=="est_resultado3")
                                  {

                                  }
                                  else if(txtarea[i].id=="est_resultado4")
                                  {

                                  }
                                  else if(txtarea[i].id=="est_resultado5")
                                  {

                                  }

                                  else if(txtarea[i].id=="estr_linea_discursiva_2")
                                  {

                                  }
                                   else if(txtarea[i].id=="estr_linea_discursiva_3")
                                  {

                                  }

                                  else
                                  {
                                     // add an "invalid_est" class to the field:
                                    txtarea[i].className += " invalid";
                                    // and set the current valid status to false:
                                    valid_est = false;


                                    
                                  }
                                }


                              //descripcion del indicador
                              else if (txtarea[i].id == "est_descrp_indicador" && txtarea[i].value.length >= 448 ) //Robert
                              {
                                  //nombre del indicador
                                 document.getElementById('error_est_descrip_inidcador').innerHTML ="El limite del campo es de 450 caracteres";
                                 var element = document.getElementById("error_est_descrip_inidcador");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid_est";
                                // and set the current valid status to false
                                valid_est = false;
                              }
                               else if (txtarea[i].id == "est_bullet_logro" && txtarea[i].value.length >= 448 ) //Robert
                              {
                                  //nombre del indicador
                                 document.getElementById('est_error_bullet_logro').innerHTML ="El limite del campo es de 450 caracteres";
                                 var element = document.getElementById("est_error_bullet_logro");
                                 element.classList.add("error_mensaje");
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid_est";
                                // and set the current valid status to false
                                valid_est = false;
                              }

                               else if (txtarea[i].id == "estr_linea_discursiva_1" && txtarea[i].value.length >= 448 ) //Robert
                              {
                                  //nombre del indicador
                                 document.getElementById('est_error_linea_discursiva_1').innerHTML ="El limite del campo es de 450 caracteres";
                                 var element = document.getElementById("est_error_linea_discursiva_1");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid_est";
                                // and set the current valid status to false
                                valid_est = false;
                              }
                              
                               else if (txtarea[i].id == "estr_linea_discursiva_2" && txtarea[i].value.length >= 448 ) //Robert
                              {
                                  //nombre del indicador
                                 document.getElementById('est_error_linea_discursiva_2').innerHTML ="El limite del campo es de 450 caracteres";
                                 var element = document.getElementById("est_error_linea_discursiva_2");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid_est";
                                // and set the current valid status to false
                                valid_est = false;
                              }
                               else if (txtarea[i].id == "estr_linea_discursiva_3" && txtarea[i].value.length >= 448 ) //Robert
                              {
                                  //nombre del indicador
                                 document.getElementById('est_error_linea_discursiva_3').innerHTML ="El limite del campo es de 450 caracteres";
                                 var element = document.getElementById("est_error_linea_discursiva_3");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid_est";
                                // and set the current valid status to false
                                valid_est = false;
                              }
                              

                              else if (txtarea[i].id == "est_descripcion_logro" && txtarea[i].value.length >= 448 ) //Robert
                              {
                                  //nombre del indicador
                                 document.getElementById('error_est_descripcion_logro').innerHTML ="El limite del campo es de 450 caracteres";
                                 var element = document.getElementById("error_est_descripcion_logro");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid_est";
                                // and set the current valid status to false
                                valid_est = false;
                              }

                              else if (txtarea[i].id == "est_resultado1" && txtarea[i].value.length > 349 )
                              {

                                //alert("mayor de 450");
                                  //nombre del indicador
                                 document.getElementById('est_error_resultado_1').innerHTML ="El limite del campo es de 350 caracteres";
                                 var element = document.getElementById("est_error_resultado_1");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid_est";
                                // and set the current valid status to false
                                valid_est = false;
                              }
                               else if (txtarea[i].id == "est_resultado2" && txtarea[i].value.length > 349 ) //Robert
                              {
                                  //nombre del indicador
                                 document.getElementById('est_error_resultado_2').innerHTML ="El limite del campo es de 350 caracteres";
                                 var element = document.getElementById("est_error_resultado_2");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid_est";
                                // and set the current valid status to false
                                valid_est = false;
                              }
                               else if (txtarea[i].id == "est_resultado3" && txtarea[i].value.length > 349 ) //Robert
                              {
                                  //nombre del indicador
                                 document.getElementById('est_error_resultado_3').innerHTML ="El limite del campo es de 350 caracteres";
                                 var element = document.getElementById("est_error_resultado_3");
                                element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid_est";
                                // and set the current valid status to false
                                valid_est = false;
                              }
                               else if (txtarea[i].id == "est_resultado4" && txtarea[i].value.length > 349 ) //Robert
                              {
                                  //nombre del indicador
                                 document.getElementById('est_error_resultado_4').innerHTML ="El limite del campo es de 350 caracteres";
                                 var element = document.getElementById("est_error_resultado_4");
                               element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid_est";
                                // and set the current valid status to false
                                valid_est = false;
                              }

                              else if (txtarea[i].id == "est_resultado5" && txtarea[i].value.length > 349 ) //Robert
                              {
                                  //nombre del indicador
                                 document.getElementById('est_error_resultado_5').innerHTML ="El limite del campo es de 350 caracteres";
                                 var element = document.getElementById("est_error_resultado_5");
                                 element.classList.add("error_mensaje");
                                  
                                  // add an "invalid" class to the field:
                                txtarea[i].className += " invalid_est";
                                // and set the current valid status to false
                                valid_est = false;
                              }




                              





              

                            }



                            // If the valid status is true, mark the step_est as finished and valid:
                            if (valid_est) {
                              document.getElementsByClassName("step_est")[currentTab_est].className += " finish";
                            }
                            return valid_est; // return the valid status
                          }

                          function fixStep_estIndicator(n) {
                            // This function removes the "active" class of all step_ests...
                            var i, x = document.getElementsByClassName("step_est");
                            for (i = 0; i < x.length; i++) 
                            {
                             
                              x[i].className = x[i].className.replace(" active", "");
                            }
                            //... and adds the "active" class to the current step_est:
                            x[n].className += " active";

                          }
                        </script>


                        <!--pasos 2 estrategicos-->
                        <!--FIN STEPS-->
                        <!-- date pickers-->
                        <script>
                              $( function() 
                              {
                                $( "#datepicker1" ).datepicker();
                                $( "#datepicker2" ).datepicker();
                                $( "#datepicker3" ).datepicker();
                                $( "#datepicker4" ).datepicker();
                                $( "#datepicker5" ).datepicker();
                              });

                        </script>
                        <!-- FIN DATE PICKERS-->

                        <!-- SELECT CHANGE VALUE-->
                        <script>


                             $('#existe_expediente_doc_def').change(function(){ 
                              if($(this).val() == "Si")

                                {
                                $('#parte2').hide();
                                $('#parte1').show();
                            
                                }
                                if($(this).val() == "No"){
                                $('#parte1').hide();
                                $('#parte2').show();

                                }


                            });


                             /*
                              $('#tendencia_esperada_def').change(function(){ 
                                


                              if($(this).val() == "Descendente")

                                {

                                var x = document.getElementById("avance_meta_planeada_def");
                                         x.setAttribute("disabled", true);
                            
                                }
                                else
                                {
                                  var x = document.getElementById("avance_meta_planeada_def");
                                         x.setAttribute("disabled", false);
                                }

                                

                            });


                            */

                             

                            $('#est_logro_asoc_ind').change(function(){ 
                              if($(this).val() == "Si")
                                {
                                
                                $('#div_est_descripcion_logro').show();
                            
                                }
                                if($(this).val() == "No"){
                                $('#div_est_descripcion_logro').hide();
                               

                                }

                              

                                


                            });


                              $('#est_hubo_logro').change(function(){ 
                              if($(this).val() == "Si")
                                {
                                
                                $('#div_est_log').show();
                            
                                }
                                if($(this).val() == "No"){
                                $('#div_est_log').hide();
                               

                                }
                                });


                             


                            $('#tipo_indicador_def').change(function()
                            { 

                            if($(this).val() == "Otro")
                            {
                            $('#otro_tipo_indicador').show();

                            }
                            if($(this).val() != "Otro"){
                            $('#otro_tipo_indicador').hide();
                            }
                            });



                            $('#se_desagrego_geograficamente_def').change(function()
                            { 
                              if($(this).val() == "No")
                              {

                              var selectobject=document.getElementById("es_posible_desagregar_geograficamente_def");
                                for (var i=0; i<selectobject.length; i++){
                                if (selectobject.options[i].value == 'Estado de Hidalgo' )
                                   selectobject.remove(i);
                                }

                              }
                                //error por corregir
                              if($(this).val() != "No")
                              {

                                var selectobject=document.getElementById("es_posible_desagregar_geograficamente_def");
                                var a;
                                for (var i=0; i<selectobject.length; i++){
                                  if (selectobject.options[i].value != 'Estado de Hidalgo' )
                                  {
                                    a++;
                                  }

                                }

                                if(i==3)
                                {
                                   $("#es_posible_desagregar_geograficamente_def").append('<option value="Estado de Hidalgo">Estado de Hidalgo </option>');
                                }
                             
                              }
                            });
                           


                            $('#tipo_evidencia_fecha_def').change(function()
                            { 

                              if($(this).val() == "Rango de tiempo")
                              {
                                
                                $('#rango_fecha_div').show();
                                $('#fecha_evidencia_fija').hide();
                                
                                

                              }
                              //error por corregir
                              if($(this).val() == "Fija")
                              {

                              $('#rango_fecha_div').hide();
                              $('#fecha_evidencia_fija').show();
                             
                              }
                            });

                        </script>
                        <!-- FIN SELECT CHANGE -->

                        <!--AUTO COMPLETAR-->

                        <script > //autocompletar de idndicadores estrategicos

                            $( "#est_nom_indicador" ).autocomplete({
                              //source: "/repositorio/wp-content/themes/EspecialesT2.1/templates/searh_indicador.php?id=<?php echo($user_name); ?>",
                              source: "/repositorio/wp-content/themes/EspecialesT2.1/templates/searh_indicador.php?id=<?php echo($user_name);?>&tipo=estrategico",
                              minLength: 1,

                               response: function(event, ui)        //TYERMINO DE REGISTRAR SUS INDICADORES
                               {
                                /*
                                  if (!ui.content.length) {
                                         $('#registro_terminado').show(); 
                                         var x = document.getElementById("nextBtn");
                                         x.setAttribute("disabled", true);
                                  }
                                */
                               }

                            });

                              $( "#est_nom_indicador" ).autocomplete({

                                close: function( event, ui ) 
                                {

                                var nom_indicador = document.getElementById("est_nom_indicador");
                                var nom_indi_selec=nom_indicador.value;
                                var tipo="estrategico";
                               // alert(nom_indi_selec);
                                $.ajax({

                                     type: "GET",
                                     datatype: 'json',
                                     contentType: "application/json; charset=utf-8",
                                    
                                     url: "/repositorio/wp-content/themes/EspecialesT2.1/templates/indicador_seleccionado.php?nom_indi_selec="+nom_indi_selec+"&tipo="+tipo ,
                                     success: function(responce)
                                      {
                                        console.log(responce);

                                        var est_descripcion = document.getElementById("est_descrp_indicador");
                                        est_descripcion.value=responce.DESCRIPCION;

                                        var est_linea_base = document.getElementById("est_linea_base");
                                        est_linea_base.value=responce.LINEA_BASE;

                                        var est_meta_2022 = document.getElementById("edit_est_meta_2022");
                                        est_meta_2022.value=responce.META_2022;

                                        var est_meta_2030 = document.getElementById("meta_2030");
                                        est_meta_2030.value=responce.META_2030;



                                        var est_eje_ped = document.getElementById("est_eje_ped");
                                        est_eje_ped.value=responce.EJE_PED;

                                        var est_obj_estrategico = document.getElementById("est_obj_estrategico");
                                        est_obj_estrategico.value=responce.obj_estrategico;

                                        var est_ind_informe = document.getElementById("est_ind_informe");
                                        est_ind_informe.value=responce.ind_informa;


                                      },
                                      error: function(data)
                                      {


                                      }
                              });

                                $('#indicadore_est_seleccionado_autollenado').show();//RENDERIZAR EL RESTO DEL FORMULARIO
                              }
                            }); 
                          </script>

                        <script >
                 
                            $( "#nombre_indicador_asociado_def" ).autocomplete({
                              //source: "/repositorio/wp-content/themes/EspecialesT2.1/templates/searh_indicador.php?id=<?php echo($user_name); ?>",
                              source: "/repositorio/wp-content/themes/EspecialesT2.1/templates/searh_indicador.php?id=<?php echo($user_name); ?>&tipo='tactico'",
                              minLength: 1,

                               response: function(event, ui)        //TYERMINO DE REGISTRAR SUS INDICADORES
                               {
                                /*
                                  if (!ui.content.length) {
                                         $('#registro_terminado').show(); 
                                         var x = document.getElementById("nextBtn");
                                         x.setAttribute("disabled", true);
                                  }
                                  */
                               }



                            });

                            //UNA VEZ SELECCIONADO EL NOMBRE DEL INDICADOR LOS DEMAS CAMPOS SE LLENARAN AUTOMATICAMENTE

                            $( "#nombre_indicador_asociado_def" ).autocomplete({
                              close: function( event, ui ) 
                              {

                                
                              },

                              select: function( event, ui ) 
                              {
                                //alert(ui.item.value);
                                $('#indicadore_seleccionado_autollenado_tacticos').show();//RENDERIZAR EL RESTO DEL FORMULARIO

                                if(ui.item.value=="Acciones sin indicador asociado")// desde el php  search indicadores
                                {


                                  $("#meta_2019").removeAttr('disabled');
                                  //alert("accion limpia");
                                  //alert("no cargar los datos");
                                  //var nom_indi_extr = document.getElementById("nombre_indicador_asociado_def");
                                  //nom_indi_extr.value="";
                                }
                                else
                                {
                                 // var nom_indicador = ui.item.value;
                                //var nom_indicador_value=nom_indicador.value;
                                var nom_indi_selec=ui.item.value;
                               // alert(nom_indi_selec);
                                $.ajax({

                                       type: "GET",
                                       datatype: 'json',
                                       contentType: "application/json; charset=utf-8",
                                      
                                       url: "/repositorio/wp-content/themes/EspecialesT2.1/templates/indicador_seleccionado.php?nom_indi_selec="+nom_indi_selec+"&tipo='tactico'",
                                       success: function(responce)
                                        {
                                          
                                          console.log(responce);


                                          var descr_indicador = document.getElementById("descripcion_indicador_dif");
                                          descr_indicador.value=responce.desc_indicador;

                                          var uni_obs = document.getElementById("unidad_observacion_indicador_def");
                                          uni_obs.value=responce.unid_obser;

                                          var descr_observacion = document.getElementById("descripcion_obj_secto");
                                          descr_observacion.value= responce.ob_secto;



                                          var cve_unica=document.getElementById("cve_unica");
                                          cve_unica.value= responce.cve_unica;

                                          var cime=document.getElementById("cime");
                                          cime.value= responce.cime;

                                          var informe2019=document.getElementById("informe2019");
                                          informe2019.value= responce.informe2019;

                                          var eje_ped=document.getElementById("eje_ped");
                                          eje_ped.value= responce.eje_ped;

                                          var ob_est=document.getElementById("ob_est");
                                          ob_est.value= responce.ob_est;

                                          var ob_gen=document.getElementById("ob_gen");
                                          ob_gen.value= responce.ob_gen;

                                          var ods=document.getElementById("ods");
                                          ods.value= responce.ods;

                                          var ob_secto=document.getElementById("ob_secto");
                                          ob_secto.value= responce.ob_secto;

                                          var periodisidad=document.getElementById("periodisidad");
                                          periodisidad.value= responce.periodisidad;

                                          var fuente=document.getElementById("fuente");
                                          fuente.value= responce.fuente;

                                          var ref_ad=document.getElementById("ref_ad");
                                          ref_ad.value= responce.ref_ad;


                                          var rep_1_info=document.getElementById("resultado_18");
                                          rep_1_info.value= responce.resultado18;


                                          var meta19=document.getElementById("meta_2019");
                                          meta19.value= responce.meta19;


                                           var tendencia=document.getElementById("tendencia_esperada_def");

                                           tendencia.value=responce.tendencia;


                                          var lin_b_abs_ = document.getElementById("linea_base_absolutos_def");
                                            lin_b_abs_.value=responce.linea_base;


                                              // var tipo_ind =  responce.tipo_ind;
                                         var select_tipo_ind=document.getElementById("tipo_indicador_def");
                                          select_tipo_ind.value= responce.tipo_ind;


                                            nom_evidencia = nom_indi_selec;

                                        },
                                      error: function(data){
                                   }
                                });
                                 
                                }
                              }





                              
                            });
                        </script>
                        <!-- FIN AUTOCOMPLETAR-->

                        <!--DIALOG ADD MUNICIPIOS-->
                        <script>
                          var indice=0;
                          var resultados_1;
                          var resultados_2;
                          var resultados_3;//valor de los lugares selecionados por resultado
                          var resultados_4;
                          var resultados_5;
                          var resultados_6;
                          var resultados_7;
                          var resultados_8;

                          var est_resultados_1;
                          var est_resultados_2;
                          var est_resultados_3;
                        

                          var resultados_1_muni;
                          var resultados_2_muni;
                          var resultados_3_muni;
                          var resultados_4_muni;
                          var resultados_5_muni;
                          var resultados_6_muni;
                          var resultados_7_muni;
                          var resultados_8_muni;

                          var resultados_1_muni_est;
                          var resultados_2_muni_est;
                          var resultados_3_muni_est;

                          var id_select;
                          var est_tact;
                          var indice;



                          //tipo o es tactico tipo 1 es estrategico
                            function open_lugares(num_resultado,id,tipo)
                            {

                              est_tact=tipo;

                              indice=num_resultado;
                              id_select=id;

  
                              $( "#dialog-message-lugar" ).dialog( "open" );     //abrir menu opciones municipal estatal localidad
                            }

                            function open_estados()
                            {
                              
                              $( "#dialog-message-lugar" ).dialog( "close" );    //   cerrar menu opcines
                              $( "#dialog-form-entidades" ).dialog( "open" );   //abrir estados
                            
                            }
                            function open_municipios()
                            {
                              $( "#dialog-message-lugar" ).dialog( "close" );    //   cerrar menu opcines
                              $( "#dialog-form" ).dialog( "open" );   //abrir municipales
                            }
                            function open_localidades()
                            {
                              $( "#dialog-message-lugar" ).dialog( "close" );    //   cerrar menu opcines
                              $( "#dialog-message-adjuntar-localidades" ).dialog( "open" );   //abrir municipales

                              var loc="localidades";


                              if(est_tact==0)
                              {

                                if(indice==1)
                                {
                                  $('#doc_res1').show(); //mostar el aparatado parea subir archivos 1
                                  $('#doc_res2').hide();
                                  $('#doc_res3').hide();
                                  $('#doc_res4').hide();
                                  $('#doc_res5').hide();
                                  $('#doc_res6').hide();
                                  $('#doc_res7').hide();
                                  $('#doc_res8').hide();
                                  resultados_1_muni="localidades";
                                  var res1_loc =document.getElementById("res1_lugares");
                                  res1_loc.value= "localidades";
                                  document.getElementById('datos_cargados_lug_1').innerHTML ="Datos Cargados";
                                  var element = document.getElementById("datos_cargados_lug_1");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#datos_cargados_lug_1').show();

                                  var res1_loc_gps1_loc =document.getElementById("res1_lugares_gps");
                                  res1_loc_gps1_loc.value= "localidades";

                                  //GPS


                                }

                                else if(indice==2)
                                {
                                  $('#doc_res2').show(); //mostar el aparatado parea subir archivos 2
                                  $('#doc_res1').hide(); 
                                  $('#doc_res3').hide();
                                  $('#doc_res4').hide();
                                  $('#doc_res5').hide();
                                  $('#doc_res6').hide();
                                  $('#doc_res7').hide();
                                  $('#doc_res8').hide();

                                  resultados_2_muni="localidades";
                                  document.getElementById('datos_cargados_lug_2').innerHTML ="Datos Cargados";
                                  var res2_loc =document.getElementById("res2_lugares");
                                  res2_loc.value= "localidades";
                                   var element = document.getElementById("datos_cargados_lug_2");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#datos_cargados_lug_2').show();

                                  var res1_loc_gps2_loc =document.getElementById("res2_lugares_gps");
                                  res1_loc_gps2_loc.value= "localidades";
                                }
                                else if(indice==3)
                                {
                                  $('#doc_res2').hide(); //mostar el aparatado parea subir archivos 2
                                  $('#doc_res1').hide(); 
                                  $('#doc_res3').show();
                                  $('#doc_res4').hide();
                                  $('#doc_res5').hide();
                                  $('#doc_res6').hide();
                                  $('#doc_res7').hide();
                                  $('#doc_res8').hide();

                                  resultados_3_muni="localidades";
                                  document.getElementById('datos_cargados_lug_3').innerHTML ="Datos Cargados";
                                  var res3_loc =document.getElementById("res3_lugares");
                                  res3_loc.value= "localidades";
                                   var element = document.getElementById("datos_cargados_lug_3");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#datos_cargados_lug_3').show();

                                  var res1_loc_gps3_loc =document.getElementById("res3_lugares_gps");
                                  res1_loc_gps3_loc.value= "localidades";
                                }
                                else if(indice==4)
                                {
                                  $('#doc_res2').hide(); //mostar el aparatado parea subir archivos 2
                                  $('#doc_res1').hide(); 
                                  $('#doc_res3').hide();
                                  $('#doc_res4').show();
                                  $('#doc_res5').hide();
                                  $('#doc_res6').hide();
                                  $('#doc_res7').hide();
                                  $('#doc_res8').hide(); 

                                  resultados_4_muni="localidades";
                                  document.getElementById('datos_cargados_lug_4').innerHTML ="Datos Cargados";
                                  var res4_loc =document.getElementById("res4_lugares");
                                  res4_loc.value= "localidades";
                                   var element = document.getElementById("datos_cargados_lug_4");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#datos_cargados_lug_4').show();

                                  var res1_loc_gps4_loc =document.getElementById("res4_lugares_gps");
                                  res1_loc_gps4_loc.value= "localidades";
                                }
                                else if(indice==5)
                                {
                                  $('#doc_res2').hide(); //mostar el aparatado parea subir archivos 2
                                  $('#doc_res1').hide(); 
                                  $('#doc_res3').hide();
                                  $('#doc_res4').hide();
                                  $('#doc_res5').show();
                                  $('#doc_res6').hide();
                                  $('#doc_res7').hide();
                                  $('#doc_res8').hide();

                                  resultados_5_muni="localidades";
                                  document.getElementById('datos_cargados_lug_5').innerHTML ="Datos Cargados";
                                  var res5_loc =document.getElementById("res5_lugares");
                                  res5_loc.value= "localidades";
                                   var element = document.getElementById("datos_cargados_lug_5");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#datos_cargados_lug_5').show();

                                  var res1_loc_gps5_loc =document.getElementById("res5_lugares_gps");
                                  res1_loc_gps5_loc.value= "localidades";
                                }
                                else if(indice==6)
                                {
                                  $('#doc_res2').hide(); //mostar el aparatado parea subir archivos 2
                                  $('#doc_res1').hide(); 
                                  $('#doc_res3').hide();
                                  $('#doc_res4').hide();
                                  $('#doc_res5').hide();
                                  $('#doc_res6').show();
                                  $('#doc_res7').hide();
                                  $('#doc_res8').hide();

                                  resultados_6_muni="localidades";
                                  document.getElementById('datos_cargados_lug_6').innerHTML ="Datos Cargados";
                                  var res6_loc =document.getElementById("res6_lugares");
                                  res6_loc.value= "localidades";
                                   var element = document.getElementById("datos_cargados_lug_6");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#datos_cargados_lug_6').show();

                                  var res6_loc_gps5_loc =document.getElementById("res6_lugares_gps");
                                  res6_loc_gps5_loc.value= "localidades";
                                }

                                else if(indice==7)
                                {
                                  $('#doc_res2').hide(); //mostar el aparatado parea subir archivos 2
                                  $('#doc_res1').hide(); 
                                  $('#doc_res3').hide();
                                  $('#doc_res4').hide();
                                  $('#doc_res5').hide();
                                  $('#doc_res6').hide();
                                  $('#doc_res7').show();
                                  $('#doc_res8').hide();

                                  resultados_7_muni="localidades";
                                  document.getElementById('datos_cargados_lug_7').innerHTML ="Datos Cargados";
                                  var res7_loc =document.getElementById("res7_lugares");
                                  res7_loc.value= "localidades";
                                   var element = document.getElementById("datos_cargados_lug_7");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#datos_cargados_lug_7').show();

                                  var res6_loc_gps7_loc =document.getElementById("res7_lugares_gps");
                                  res6_loc_gps7_loc.value= "localidades";
                                }
                                else if(indice==8)
                                {
                                  $('#doc_res2').hide(); //mostar el aparatado parea subir archivos 2
                                  $('#doc_res1').hide(); 
                                  $('#doc_res3').hide();
                                  $('#doc_res4').hide();
                                  $('#doc_res5').hide();
                                  $('#doc_res6').hide();
                                  $('#doc_res7').hide();
                                  $('#doc_res8').show();

                                  resultados_8_muni="localidades";
                                  document.getElementById('datos_cargados_lug_8').innerHTML ="Datos Cargados";
                                  var res8_loc =document.getElementById("res8_lugares");
                                  res8_loc.value= "localidades";
                                   var element = document.getElementById("datos_cargados_lug_8");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#datos_cargados_lug_8').show();

                                  var res6_loc_gps8_loc =document.getElementById("res8_lugares_gps");
                                  res6_loc_gps8_loc.value= "localidades";
                                }



                              }
  


                              //caso de los estrategicos
                              if(est_tact == 1)
                              {
                                //alert("localidades");
                                if(indice==1)
                                {
                                 // alert("localidades");

                                    $('#doc_res1').hide();
                                    $('#doc_res2').hide();
                                    $('#doc_res3').hide();  //estos son los tacticos

                                  
                                  $('#est_doc_res1').show(); //mostar el aparatado parea subir archivos 1
                                  $('#est_doc_res2').hide();
                                  $('#est_doc_res3').hide();

                                  $('#est_doc_res4').hide();
                                  $('#est_doc_res5').hide();
                                 
                                  resultados_1_muni_est="localidades";
                                  var res1_loc =document.getElementById("est_res1_lugares");
                                  res1_loc.value= "localidades";
                                  document.getElementById('est_datos_cargados_lug_1').innerHTML ="Datos Cargados";
                                  var element = document.getElementById("est_datos_cargados_lug_1");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#est_datos_cargados_lug_1').show();

                                  
                                  var est_res1_lugares_gps =document.getElementById("est_res1_lugares_gps");
                                  est_res1_lugares_gps.value= "localidades";




                                }

                                if(indice==2)
                                {

                                   $('#doc_res1').hide();
                                    $('#doc_res2').hide();
                                    $('#doc_res3').hide();  //estos son los tacticos


                                  $('#est_doc_res2').show(); //mostar el aparatado parea subir archivos 1
                                  $('#est_doc_res1').hide();
                                  $('#est_doc_res3').hide();

                                  $('#est_doc_res4').hide();
                                  $('#est_doc_res5').hide();
                                 
                                  resultados_1_muni_est="localidades";
                                  var res1_loc =document.getElementById("est_res2_lugares");
                                  res1_loc.value= "localidades";
                                  document.getElementById('est_datos_cargados_lug_2').innerHTML ="Datos Cargados";
                                  var element = document.getElementById("est_datos_cargados_lug_2");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#est_datos_cargados_lug_2').show();

                                  var est_res2_lugares_gps =document.getElementById("est_res2_lugares_gps");
                                  est_res2_lugares_gps.value= "localidades";
                                }

                                if(indice==3)
                                {

                                   $('#doc_res1').hide();
                                    $('#doc_res2').hide();
                                    $('#doc_res3').hide();  //estos son los tacticos


                                  $('#est_doc_res2').hide(); //mostar el aparatado parea subir archivos 1
                                  $('#est_doc_res1').hide();
                                  $('#est_doc_res3').show();

                                  $('#est_doc_res4').hide();
                                  $('#est_doc_res5').hide();
                                 
                                  resultados_1_muni_est="localidades";
                                  var res1_loc =document.getElementById("est_res3_lugares");
                                  res1_loc.value= "localidades";
                                  document.getElementById('est_datos_cargados_lug_3').innerHTML ="Datos Cargados";
                                  var element = document.getElementById("est_datos_cargados_lug_3");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#est_datos_cargados_lug_3').show();

                                  var est_res3_lugares_gps =document.getElementById("est_res3_lugares_gps");
                                  est_res3_lugares_gps.value= "localidades";
                                }

                                if(indice==4)
                                {

                                   $('#doc_res1').hide();
                                    $('#doc_res2').hide();
                                    $('#doc_res3').hide();  //estos son los tacticos


                                  $('#est_doc_res2').hide(); //mostar el aparatado parea subir archivos 1
                                  $('#est_doc_res1').hide();
                                  $('#est_doc_res3').hide();

                                  $('#est_doc_res4').show();
                                  $('#est_doc_res5').hide();
                                 
                                  resultados_1_muni_est="localidades";
                                  var res1_loc =document.getElementById("est_res4_lugares");
                                  res1_loc.value= "localidades";
                                  document.getElementById('est_datos_cargados_lug_4').innerHTML ="Datos Cargados";
                                  var element = document.getElementById("est_datos_cargados_lug_4");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#est_datos_cargados_lug_4').show();

                                  var est_res4_lugares_gps =document.getElementById("est_res4_lugares_gps");
                                  est_res4_lugares_gps.value= "localidades";
                                }

                                if(indice==5)
                                {

                                   $('#doc_res1').hide();
                                    $('#doc_res2').hide();
                                    $('#doc_res3').hide();  //estos son los tacticos


                                  $('#est_doc_res2').hide(); //mostar el aparatado parea subir archivos 1
                                  $('#est_doc_res1').hide();
                                  $('#est_doc_res3').hide();

                                  $('#est_doc_res4').hide();
                                  $('#est_doc_res5').show();
                                 
                                  resultados_1_muni_est="localidades";
                                  var res1_loc =document.getElementById("est_res5_lugares");
                                  res1_loc.value= "localidades";
                                  document.getElementById('est_datos_cargados_lug_5').innerHTML ="Datos Cargados";
                                  var element = document.getElementById("est_datos_cargados_lug_5");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#est_datos_cargados_lug_5').show();

                                  var resres1_loc_gps1_loc =document.getElementById("res5_lugares_gps");
                                  resres1_loc_gps1_loc.value= "localidades";

                                   var est_res5_lugares_gps =document.getElementById("est_res5_lugares_gps");
                                  est_res5_lugares_gps.value= "localidades";



                                }





                                   
                              }






                              //cao dinamico de los aparatados de edicion 
                              if(document.getElementById('res1_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>') && indice==1)
                              {

                                //guardar info localidades en input 
                                var lugar_1_edit=document.getElementById('res1_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>');

                                
                                lugar_1_edit.value=loc;
                                //mensaje datos cargados
                                document.getElementById('datos_cargados_lug_1_<?php echo($registro_2['id_indicador_asociado_def'])?>').innerHTML ="Datos Cargados";
                                var element = document.getElementById("datos_cargados_lug_1_<?php echo($registro_2['id_indicador_asociado_def'])?>");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_1_<?php echo($registro_2['id_indicador_asociado_def'])?>').show();



                                var resres1_loc_gps1_loc =document.getElementById("res1_lugares_GPS_<?php echo($registro_2['id_indicador_asociado_def'])?>");
                                  resres1_loc_gps1_loc.value= "localidades";


                                

                              }
                              else if(document.getElementById('res2_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>') && indice==2)
                              {
                                var lugar_1_edit=document.getElementById('res2_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>');
                                lugar_1_edit.value=loc;

                                                            //mensaje datos cargados
                                document.getElementById('datos_cargados_lug_2_<?php echo($registro_2['id_indicador_asociado_def'])?>').innerHTML ="Datos Cargados";
                                var element = document.getElementById("datos_cargados_lug_2_<?php echo($registro_2['id_indicador_asociado_def'])?>");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_2_<?php echo($registro_2['id_indicador_asociado_def'])?>').show();

                                 var resres1_loc_gps1_loc =document.getElementById("res2_lugares_GPS_<?php echo($registro_2['id_indicador_asociado_def'])?>");
                                  resres1_loc_gps1_loc.value= "localidades";



                              }

                              else if(document.getElementById('res3_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>') && indice==3)
                              {
                                var lugar_1_edit=document.getElementById('res3_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>');
                                lugar_1_edit.value=loc;

                                                            //mensaje datos cargados
                                document.getElementById('datos_cargados_lug_3_<?php echo($registro_2['id_indicador_asociado_def'])?>').innerHTML ="Datos Cargados";
                                var element = document.getElementById("datos_cargados_lug_3_<?php echo($registro_2['id_indicador_asociado_def'])?>");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_3_<?php echo($registro_2['id_indicador_asociado_def'])?>').show();

                                 var resres1_loc_gps1_loc =document.getElementById("res3_lugares_GPS_<?php echo($registro_2['id_indicador_asociado_def'])?>");
                                  resres1_loc_gps1_loc.value= "localidades";



                              }

                              else if(document.getElementById('res4_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>') && indice==4)
                              {
                                var lugar_1_edit=document.getElementById('res4_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>');
                                lugar_1_edit.value=loc;

                                                            //mensaje datos cargados
                                document.getElementById('datos_cargados_lug_4_<?php echo($registro_2['id_indicador_asociado_def'])?>').innerHTML ="Datos Cargados";
                                var element = document.getElementById("datos_cargados_lug_4_<?php echo($registro_2['id_indicador_asociado_def'])?>");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_4_<?php echo($registro_2['id_indicador_asociado_def'])?>').show();

                                 var resres1_loc_gps1_loc =document.getElementById("res4_lugares_GPS_<?php echo($registro_2['id_indicador_asociado_def'])?>");
                                  resres1_loc_gps1_loc.value= "localidades";


                              }

                              else if(document.getElementById('res5_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>') && indice==5)
                              {
                                var lugar_1_edit=document.getElementById('res5_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>');
                                lugar_1_edit.value=loc;

                                                            //mensaje datos cargados
                                document.getElementById('datos_cargados_lug_5_<?php echo($registro_2['id_indicador_asociado_def'])?>').innerHTML ="Datos Cargados";
                                var element = document.getElementById("datos_cargados_lug_5_<?php echo($registro_2['id_indicador_asociado_def'])?>");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_5_<?php echo($registro_2['id_indicador_asociado_def'])?>').show();

                                 var resres1_loc_gps1_loc =document.getElementById("res5_lugares_GPS_<?php echo($registro_2['id_indicador_asociado_def'])?>");
                                  resres1_loc_gps1_loc.value= "localidades";

                              }

                              else if(document.getElementById('res6_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>') && indice==6)
                              {
                                var lugar_1_edit=document.getElementById('res6_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>');
                                lugar_1_edit.value=loc;

                                                            //mensaje datos cargados
                                document.getElementById('datos_cargados_lug_6_<?php echo($registro_2['id_indicador_asociado_def'])?>').innerHTML ="Datos Cargados";
                                var element = document.getElementById("datos_cargados_lug_6_<?php echo($registro_2['id_indicador_asociado_def'])?>");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_6_<?php echo($registro_2['id_indicador_asociado_def'])?>').show();

                                 var resres1_loc_gps1_loc =document.getElementById("res6_lugares_GPS_<?php echo($registro_2['id_indicador_asociado_def'])?>");
                                  resres1_loc_gps1_loc.value= "localidades";
                              }

                              else if(document.getElementById('res7_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>') && indice==7)
                              {
                                var lugar_1_edit=document.getElementById('res7_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>');
                                lugar_1_edit.value=loc;

                                                            //mensaje datos cargados
                                document.getElementById('datos_cargados_lug_7_<?php echo($registro_2['id_indicador_asociado_def'])?>').innerHTML ="Datos Cargados";
                                var element = document.getElementById("datos_cargados_lug_7_<?php echo($registro_2['id_indicador_asociado_def'])?>");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_7_<?php echo($registro_2['id_indicador_asociado_def'])?>').show();

                                 var resres1_loc_gps1_loc =document.getElementById("res7_lugares_GPS_<?php echo($registro_2['id_indicador_asociado_def'])?>");
                                  resres1_loc_gps1_loc.value= "localidades";
                              }


                              else if(document.getElementById('res8_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>') && indice==8)
                              {
                                var lugar_1_edit=document.getElementById('res8_lugares_<?php echo($registro_2['id_indicador_asociado_def'])?>');
                                lugar_1_edit.value=loc;

                                                            //mensaje datos cargados
                                document.getElementById('datos_cargados_lug_8_<?php echo($registro_2['id_indicador_asociado_def'])?>').innerHTML ="Datos Cargados";
                                var element = document.getElementById("datos_cargados_lug_8_<?php echo($registro_2['id_indicador_asociado_def'])?>");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_8_<?php echo($registro_2['id_indicador_asociado_def'])?>').show();

                                 var resres1_loc_gps1_loc =document.getElementById("res8_lugares_GPS_<?php echo($registro_2['id_indicador_asociado_def'])?>");
                                  resres1_loc_gps1_loc.value= "localidades";
                              }





                              ///EDICION ESTRATEGICOS



                              if(document.getElementById('est_res1_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>') && indice==1)
                              {

                                //guardar info localidades en input 
                                var lugar_1_edit=document.getElementById('est_res1_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>');

                                //alert("update ubicacion 1");
                                
                                lugar_1_edit.value="localidades";
                                //mensaje datos cargados
                                document.getElementById('est_datos_cargados_lug_1_edit_<?php echo($registro_2['id_ind_estrategico'])?>').innerHTML ="Datos Cargados";
                                var element = document.getElementById("est_datos_cargados_lug_1_edit_<?php echo($registro_2['id_ind_estrategico'])?>");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#est_datos_cargados_lug_1_edit_<?php echo($registro_2['id_ind_estrategico'])?>').show();

                                var resres1_loc_gps1_loc =document.getElementById("edit_res1_lugares_GPS_<?php echo($registro_2['id_ind_estrategico'])?>");
                                  resres1_loc_gps1_loc.value= "localidades";  

                              }



                              if(document.getElementById('est_res2_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>') && indice==2)
                              {

                                //guardar info localidades en input 
                                var lugar_1_edit=document.getElementById('est_res2_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>');

                                //alert("update ubicacion 1");
                                
                                lugar_1_edit.value="localidades";
                                //mensaje datos cargados
                                document.getElementById('est_datos_cargados_lug_2_edit_<?php echo($registro_2['id_ind_estrategico'])?>').innerHTML ="Datos Cargados";
                                var element = document.getElementById("est_datos_cargados_lug_2_edit_<?php echo($registro_2['id_ind_estrategico'])?>");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#est_datos_cargados_lug_2_edit_<?php echo($registro_2['id_ind_estrategico'])?>').show();

                                var resres1_loc_gps1_loc =document.getElementById("edit_res2_lugares_GPS_<?php echo($registro_2['id_ind_estrategico'])?>");
                                  resres1_loc_gps1_loc.value= "localidades";  

                              }

                              if(document.getElementById('est_res3_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>') && indice==3)
                              {

                                //guardar info localidades en input 
                                var lugar_2_edit=document.getElementById('est_res3_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>');

                                //alert("update ubicacion 2");
                                
                                lugar_2_edit.value="localidades";
                                //mensaje datos cargados
                                document.getElementById('est_datos_cargados_lug_3_edit_<?php echo($registro_2['id_ind_estrategico'])?>').innerHTML ="Datos Cargados";
                                var element = document.getElementById("est_datos_cargados_lug_3_edit_<?php echo($registro_2['id_ind_estrategico'])?>");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#est_datos_cargados_lug_3_edit_<?php echo($registro_2['id_ind_estrategico'])?>').show();

                                var resres2_loc_gps2_loc =document.getElementById("edit_res3_lugares_GPS_<?php echo($registro_2['id_ind_estrategico'])?>");
                                  resres2_loc_gps2_loc.value= "localidades";  

                              }

                              if(document.getElementById('est_res4_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>') && indice==4)
                              {

                                //guardar info localidades en input 
                                var lugar_2_edit=document.getElementById('est_res4_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>');

                                //alert("update ubicacion 2");
                                
                                lugar_2_edit.value="localidades";
                                //mensaje datos cargados
                                document.getElementById('est_datos_cargados_lug_4_edit_<?php echo($registro_2['id_ind_estrategico'])?>').innerHTML ="Datos Cargados";
                                var element = document.getElementById("est_datos_cargados_lug_4_edit_<?php echo($registro_2['id_ind_estrategico'])?>");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#est_datos_cargados_lug_4_edit_<?php echo($registro_2['id_ind_estrategico'])?>').show();

                                var resres2_loc_gps2_loc =document.getElementById("edit_res4_lugares_GPS_<?php echo($registro_2['id_ind_estrategico'])?>");
                                  resres2_loc_gps2_loc.value= "localidades";  

                              }


                              if(document.getElementById('est_res5_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>') && indice==5)
                              {

                                //guardar info localidades en input 
                                var lugar_3_edit=document.getElementById('est_res5_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>');

                                //alert("update ubicacion 2");
                                
                                lugar_3_edit.value="localidades";
                                //mensaje datos cargados
                                document.getElementById('est_datos_cargados_lug_5_edit_<?php echo($registro_2['id_ind_estrategico'])?>').innerHTML ="Datos Cargados";
                                var element = document.getElementById("est_datos_cargados_lug_5_edit_<?php echo($registro_2['id_ind_estrategico'])?>");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#est_datos_cargados_lug_5_edit_<?php echo($registro_2['id_ind_estrategico'])?>').show();

                                var resres2_loc_gps2_loc =document.getElementById("edit_res5_lugares_GPS_<?php echo($registro_2['id_ind_estrategico'])?>");
                                  resres2_loc_gps2_loc.value= "localidades";  

                              }







                              


                          
                               //alert("est_res1_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>");
                            
                           
                            }










                          var myMarker;
                          var lat;
                          var lng;

                         
                          var arr_gps="";
                          var arr_enti="";
                          var arr_muni="";

                          function get_position()
                          {
                             lat = myMarker.getPosition().lat();
                             lng = myMarker.getPosition().lng();
                          }
                            








                          $( function() 
                          {
                            var dialog, form,
                         
                              // From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
                              emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
                              name = $( "#name" ),
                              email = $( "#email" ),
                              password = $( "#password" ),
                              allFields = $( [] ).add( name ).add( email ).add( password ),
                              tips = $( ".validateTips" );
                         
                            function updateTips( t ) {
                              tips
                                .text( t )
                                .addClass( "ui-state-highlight" );
                              setTimeout(function() {
                                tips.removeClass( "ui-state-highlight", 2500 );
                              }, 500 );
                            }
                         
                            function checkLength( o, n, min, max ) {
                              if ( o.val().length > max || o.val().length < min ) {
                                o.addClass( "ui-state-error" );
                                updateTips( "Length of " + n + " must be between " +
                                  min + " and " + max + "." );
                                return false;
                              } else {
                                return true;
                              }
                            }
                         
                            function checkRegexp( o, regexp, n ) {
                              if ( !( regexp.test( o.val() ) ) ) {
                                o.addClass( "ui-state-error" );
                                updateTips( n );
                                return false;
                              } else {
                                return true;
                              }
                            }
                         
                            function addUser() 
                            {
                              $('#municipios_cargados').show();   
                              var valid = true;
                            
                             // allFields.removeClass( "ui-state-error" );


                             var arr_checks = document.getElementsByClassName("check_muni_tac");
                             var municipios_def_mun="Municipios,";

                             arr_gps="{";



                             
                             // alert(arr_checks.length);
                                for (e = 0; e < arr_checks.length; e++) 
                              {

                                if(arr_checks[e].checked == true)
                                {
                                  

                                  municipios_def_mun+=e+",";
                                  arr_gps+="%"+arr_checks[e].name+"%:"+"%"+lat+" | "+lng+"%"+",";
 
                                }

                              }

                              arr_gps = arr_gps.slice(0, -1);

                              arr_gps+="}";

                        
                               if(est_tact == 0)
                              {
                                if(indice==1)
                                {
                                  resultados_1_muni=municipios_def_mun;
                                  document.getElementById('datos_cargados_lug_1').innerHTML ="Datos Cargados";
                                  var res1_mun =document.getElementById("res1_lugares");
                                  res1_mun.value= municipios_def_mun;
                                  var element = document.getElementById("datos_cargados_lug_1");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#datos_cargados_lug_1').show();

                                  var res1_mun_gps =document.getElementById("res1_lugares_gps");
                                  res1_mun_gps.value= arr_gps;

                                }

                                else if(indice==2)
                                {
                                  resultados_2_muni=municipios_def_mun;
                                  document.getElementById('datos_cargados_lug_2').innerHTML ="Datos Cargados";
                                  var res2_mun =document.getElementById("res2_lugares");
                                  res2_mun.value= municipios_def_mun;
                                   var element = document.getElementById("datos_cargados_lug_2");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#datos_cargados_lug_2').show();

                                  var res2_mun_gps =document.getElementById("res2_lugares_gps");
                                  res2_mun_gps.value= arr_gps;
                                }
                                else if(indice==3)
                                {
                                  resultados_3_muni=municipios_def_mun;
                                  document.getElementById('datos_cargados_lug_3').innerHTML ="Datos Cargados";
                                  var res3_mun =document.getElementById("res3_lugares");
                                  res3_mun.value= municipios_def_mun;
                                   var element = document.getElementById("datos_cargados_lug_3");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#datos_cargados_lug_3').show();

                                  var res3_mun_gps =document.getElementById("res3_lugares_gps");
                                  res3_mun_gps.value= arr_gps;
                                }
                                else if(indice==4)
                                {
                                  resultados_4_muni=municipios_def_mun;
                                  document.getElementById('datos_cargados_lug_4').innerHTML ="Datos Cargados";
                                  var res4_mun =document.getElementById("res4_lugares");
                                  res4_mun.value= municipios_def_mun;
                                   var element = document.getElementById("datos_cargados_lug_4");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#datos_cargados_lug_4').show();

                                  var res4_mun_gps =document.getElementById("res4_lugares_gps");
                                  res4_mun_gps.value= arr_gps;
                                }
                                else if(indice==5)
                                {
                                  resultados_5_muni=municipios_def_mun;
                                  document.getElementById('datos_cargados_lug_5').innerHTML ="Datos Cargados";
                                  var res5_mun =document.getElementById("res5_lugares");
                                  res5_mun.value= municipios_def_mun;
                                   var element = document.getElementById("datos_cargados_lug_5");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#datos_cargados_lug_5').show();

                                  var res5_mun_gps =document.getElementById("res5_lugares_gps");
                                  res5_mun_gps.value= arr_gps;
                                }


                                else if(indice==6)
                                {
                                  resultados_6_muni=municipios_def_mun;
                                  document.getElementById('datos_cargados_lug_6').innerHTML ="Datos Cargados";
                                  var res6_mun =document.getElementById("res6_lugares");
                                  res6_mun.value= municipios_def_mun;
                                   var element = document.getElementById("datos_cargados_lug_6");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#datos_cargados_lug_6').show();

                                  var res6_mun_gps =document.getElementById("res6_lugares_gps");
                                  res6_mun_gps.value= arr_gps;
                                }
                                else if(indice==7)
                                {
                                  resultados_7_muni=municipios_def_mun;
                                  document.getElementById('datos_cargados_lug_7').innerHTML ="Datos Cargados";
                                  var res7_mun =document.getElementById("res7_lugares");
                                  res7_mun.value= municipios_def_mun;
                                   var element = document.getElementById("datos_cargados_lug_7");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#datos_cargados_lug_7').show();

                                  var res7_mun_gps =document.getElementById("res7_lugares_gps");
                                  res7_mun_gps.value= arr_gps;
                                }
                                else if(indice==8)
                                {
                                  resultados_8_muni=municipios_def_mun;
                                  document.getElementById('datos_cargados_lug_8').innerHTML ="Datos Cargados";
                                  var res8_mun =document.getElementById("res8_lugares");
                                  res8_mun.value= municipios_def_mun;
                                   var element = document.getElementById("datos_cargados_lug_8");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#datos_cargados_lug_8').show();

                                  var res8_mun_gps =document.getElementById("res8_lugares_gps");
                                  res8_mun_gps.value= arr_gps;
                                }
                            }



                              if(est_tact == 1)
                              {

                                 if(indice==1)
                                {
                                  resultados_1_muni=municipios_def_mun;
                                  document.getElementById('est_datos_cargados_lug_1').innerHTML ="Datos Cargados";
                                  var res1_mun =document.getElementById("est_res1_lugares");
                                  res1_mun.value= municipios_def_mun;
                                  var element = document.getElementById("est_datos_cargados_lug_1");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#est_datos_cargados_lug_1').show();

                                  var res1_mun_gps =document.getElementById("est_res1_lugares_gps");
                                  res1_mun_gps.value= arr_gps;


                                
                                }

                                else if(indice==2)
                                {
                                  resultados_2_muni=municipios_def_mun;
                                  document.getElementById('est_datos_cargados_lug_2').innerHTML ="Datos Cargados";
                                  var res2_mun =document.getElementById("est_res2_lugares");
                                  res2_mun.value= municipios_def_mun;
                                   var element = document.getElementById("est_datos_cargados_lug_2");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#est_datos_cargados_lug_2').show();

                                  var res1_mun_gps =document.getElementById("est_res2_lugares_gps");
                                  res1_mun_gps.value= arr_gps;


                                }
                                else if(indice==3)
                                {
                                  resultados_3_muni=municipios_def_mun;
                                  document.getElementById('est_datos_cargados_lug_3').innerHTML ="Datos Cargados";
                                  var res3_mun =document.getElementById("est_res3_lugares");
                                  res3_mun.value= municipios_def_mun;
                                   var element = document.getElementById("est_datos_cargados_lug_3");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#est_datos_cargados_lug_3').show();

                                  var res1_mun_gps =document.getElementById("est_res3_lugares_gps");
                                  res1_mun_gps.value= arr_gps;
                                }
                                else if(indice==4)
                                {
                                  resultados_4_muni=municipios_def_mun;
                                  document.getElementById('est_datos_cargados_lug_4').innerHTML ="Datos Cargados";
                                  var res4_mun =document.getElementById("est_res4_lugares");
                                  res4_mun.value= municipios_def_mun;
                                   var element = document.getElementById("est_datos_cargados_lug_4");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#est_datos_cargados_lug_4').show();

                                  var res1_mun_gps =document.getElementById("est_res4_lugares_gps");
                                  res1_mun_gps.value= arr_gps;
                                }

                                else if(indice==5)
                                {
                                  resultados_5_muni=municipios_def_mun;
                                  document.getElementById('est_datos_cargados_lug_5').innerHTML ="Datos Cargados";
                                  var res5_mun =document.getElementById("est_res5_lugares");
                                  res5_mun.value= municipios_def_mun;
                                   var element = document.getElementById("est_datos_cargados_lug_5");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#est_datos_cargados_lug_5').show();

                                  var res1_mun_gps =document.getElementById("est_res5_lugares_gps");
                                  res1_mun_gps.value= arr_gps;
                                }
                              


                              }


                              if(document.getElementById('res1_lugares_'+id_select) && indice==1)
                              {

 
                                //guardar info localidades en input 
                                var lugar_1_edit=document.getElementById('res1_lugares_'+id_select);

                               
                                lugar_1_edit.value=municipios_def_mun;
                                //mensaje datos cargados
                                document.getElementById('datos_cargados_lug_1_'+id_select).innerHTML ="Datos Cargados";
                                var element = document.getElementById("datos_cargados_lug_1_"+id_select);
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_1_'+id_select).show();

                                var res1_mun_gps =document.getElementById("res1_lugares_GPS_"+id_select);
                                res1_mun_gps.value= arr_gps;


                              }

                              else if(document.getElementById('res2_lugares_'+id_select) && indice==2)
                              {
                                var lugar_1_edit=document.getElementById('res2_lugares_'+id_select);
                                lugar_1_edit.value=municipios_def_mun;
                                                            //mensaje datos cargados
                                document.getElementById('datos_cargados_lug_2_'+id_select).innerHTML ="Datos Cargados";
                                var element = document.getElementById("datos_cargados_lug_2_"+id_select);
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_2_'+id_select).show();

                                var res1_mun_gps =document.getElementById("res2_lugares_GPS_"+id_select);
                                res1_mun_gps.value= arr_gps;

                              }

                              else if(document.getElementById('res3_lugares_'+id_select) && indice==3)
                              {
                                var lugar_1_edit=document.getElementById('res3_lugares_'+id_select);
                                lugar_1_edit.value=municipios_def_mun;

                                                            //mensaje datos cargados
                                document.getElementById('datos_cargados_lug_3_'+id_select).innerHTML ="Datos Cargados";
                                var element = document.getElementById("datos_cargados_lug_3_"+id_select);
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_3_'+id_select).show();

                                var res1_mun_gps =document.getElementById("res3_lugares_GPS_"+id_select);
                                res1_mun_gps.value= arr_gps;


                              }

                              else if(document.getElementById('res4_lugares_'+id_select) && indice==4)
                              {
                                var lugar_1_edit=document.getElementById('res4_lugares_'+id_select);
                                lugar_1_edit.value=municipios_def_mun;

                                                            //mensaje datos cargados
                                document.getElementById('datos_cargados_lug_4_'+id_select).innerHTML ="Datos Cargados";
                                var element = document.getElementById("datos_cargados_lug_4_"+id_select);
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_4_'+id_select).show();

                                var res1_mun_gps =document.getElementById("res4_lugares_GPS_"+id_select);
                                res1_mun_gps.value= arr_gps;

                              }

                              else if(document.getElementById('res5_lugares_'+id_select) && indice==5)
                              {
                                var lugar_1_edit=document.getElementById('res5_lugares_'+id_select);
                                lugar_1_edit.value=municipios_def_mun;

                                                            //mensaje datos cargados
                                document.getElementById('datos_cargados_lug_5_'+id_select).innerHTML ="Datos Cargados";
                                var element = document.getElementById("datos_cargados_lug_5_"+id_select);
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_5_'+id_select).show();

                                var res1_mun_gps =document.getElementById("res5_lugares_GPS_"+id_select);
                                res1_mun_gps.value= arr_gps;

                              }

                              else if(document.getElementById('res6_lugares_'+id_select) && indice==6)
                              {
                                var lugar_1_edit=document.getElementById('res6_lugares_'+id_select);
                                lugar_1_edit.value=municipios_def_mun;

                                                            //mensaje datos cargados
                                document.getElementById('datos_cargados_lug_6_'+id_select).innerHTML ="Datos Cargados";
                                var element = document.getElementById("datos_cargados_lug_6_"+id_select);
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_6_'+id_select).show();

                                var res1_mun_gps =document.getElementById("res6_lugares_GPS_"+id_select);
                                res1_mun_gps.value= arr_gps;

                              }

                              else if(document.getElementById('res7_lugares_'+id_select) && indice==7)
                              {
                                var lugar_1_edit=document.getElementById('res7_lugares_'+id_select);
                                lugar_1_edit.value=municipios_def_mun;

                                                            //mensaje datos cargados
                                document.getElementById('datos_cargados_lug_7_'+id_select).innerHTML ="Datos Cargados";
                                var element = document.getElementById("datos_cargados_lug_7_"+id_select);
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_7_'+id_select).show();

                                var res1_mun_gps =document.getElementById("res7_lugares_GPS_"+id_select);
                                res1_mun_gps.value= arr_gps;

                              }

                              else if(document.getElementById('res8_lugares_'+id_select) && indice==8)
                              {
                                var lugar_1_edit=document.getElementById('res8_lugares_'+id_select);
                                lugar_1_edit.value=municipios_def_mun;

                                                            //mensaje datos cargados
                                document.getElementById('datos_cargados_lug_8_'+id_select).innerHTML ="Datos Cargados";
                                var element = document.getElementById("datos_cargados_lug_8_"+id_select);
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_8_'+id_select).show();

                                var res1_mun_gps =document.getElementById("res8_lugares_GPS_"+id_select);
                                res1_mun_gps.value= arr_gps;

                              }





                               ///EDICION ESTRATEGICOS

                              // alert('est_res1_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>');



                              if(document.getElementById('est_res1_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>') && indice==1)
                              {

                                //guardar info localidades en input 
                                var lugar_1_edit=document.getElementById('est_res1_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>');

                                //alert("update ubicacion 1");
                                
                                lugar_1_edit.value=municipios_def_mun;
                                //mensaje datos cargados
                                document.getElementById('est_datos_cargados_lug_1_edit_<?php echo($registro_2['id_ind_estrategico'])?>').innerHTML ="Datos Cargados";
                                var element = document.getElementById("est_datos_cargados_lug_1_edit_<?php echo($registro_2['id_ind_estrategico'])?>");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#est_datos_cargados_lug_1_edit_<?php echo($registro_2['id_ind_estrategico'])?>').show();

                                var resres1_loc_gps1_loc =document.getElementById("edit_res1_lugares_GPS_<?php echo($registro_2['id_ind_estrategico'])?>");
                                  resres1_loc_gps1_loc.value= arr_gps;  

                              }



                              if(document.getElementById('est_res2_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>') && indice==2)
                              {

                                //guardar info localidades en input 
                                var lugar_1_edit=document.getElementById('est_res2_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>');

                                //alert("update ubicacion 1");
                                
                                lugar_1_edit.value=municipios_def_mun;
                                //mensaje datos cargados
                                document.getElementById('est_datos_cargados_lug_2_edit_<?php echo($registro_2['id_ind_estrategico'])?>').innerHTML ="Datos Cargados";
                                var element = document.getElementById("est_datos_cargados_lug_2_edit_<?php echo($registro_2['id_ind_estrategico'])?>");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#est_datos_cargados_lug_2_edit_<?php echo($registro_2['id_ind_estrategico'])?>').show();

                                var resres1_loc_gps1_loc =document.getElementById("edit_res2_lugares_GPS_<?php echo($registro_2['id_ind_estrategico'])?>");
                                  resres1_loc_gps1_loc.value=arr_gps;  

                              }

                              if(document.getElementById('est_res3_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>') && indice==3)
                              {

                                //guardar info localidades en input 
                                var lugar_2_edit=document.getElementById('est_res3_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>');

                                //alert("update ubicacion 2");
                                
                                lugar_2_edit.value=municipios_def_mun;
                                //mensaje datos cargados
                                document.getElementById('est_datos_cargados_lug_3_edit_<?php echo($registro_2['id_ind_estrategico'])?>').innerHTML ="Datos Cargados";
                                var element = document.getElementById("est_datos_cargados_lug_3_edit_<?php echo($registro_2['id_ind_estrategico'])?>");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#est_datos_cargados_lug_3_edit_<?php echo($registro_2['id_ind_estrategico'])?>').show();

                                var resres2_loc_gps2_loc =document.getElementById("edit_res3_lugares_GPS_<?php echo($registro_2['id_ind_estrategico'])?>");
                                  resres2_loc_gps2_loc.value= arr_gps;  

                              }

                              if(document.getElementById('est_res4_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>') && indice==4)
                              {

                                //guardar info localidades en input 
                                var lugar_2_edit=document.getElementById('est_res4_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>');

                                //alert("update ubicacion 2");
                                
                                lugar_2_edit.value=municipios_def_mun;
                                //mensaje datos cargados
                                document.getElementById('est_datos_cargados_lug_4_edit_<?php echo($registro_2['id_ind_estrategico'])?>').innerHTML ="Datos Cargados";
                                var element = document.getElementById("est_datos_cargados_lug_4_edit_<?php echo($registro_2['id_ind_estrategico'])?>");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#est_datos_cargados_lug_4_edit_<?php echo($registro_2['id_ind_estrategico'])?>').show();

                                var resres2_loc_gps2_loc =document.getElementById("edit_res4_lugares_GPS_<?php echo($registro_2['id_ind_estrategico'])?>");
                                  resres2_loc_gps2_loc.value= arr_gps;  

                              }


                              if(document.getElementById('est_res5_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>') && indice==5)
                              {

                                //guardar info localidades en input 
                                var lugar_3_edit=document.getElementById('est_res5_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>');

                                //alert("update ubicacion 2");
                                
                                lugar_3_edit.value=municipios_def_mun;
                                //mensaje datos cargados
                                document.getElementById('est_datos_cargados_lug_5_edit_<?php echo($registro_2['id_ind_estrategico'])?>').innerHTML ="Datos Cargados";
                                var element = document.getElementById("est_datos_cargados_lug_5_edit_<?php echo($registro_2['id_ind_estrategico'])?>");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#est_datos_cargados_lug_5_edit_<?php echo($registro_2['id_ind_estrategico'])?>').show();

                                var resres2_loc_gps2_loc =document.getElementById("edit_res5_lugares_GPS_<?php echo($registro_2['id_ind_estrategico'])?>");
                                  resres2_loc_gps2_loc.value= arr_gps;  

                              }







                               // alert("limpiar la fiesta");
                                for (i = 0; i < arr_checks.length; i++) 
                              {

                                if(arr_checks[i].checked == true)
                                {
                                  arr_checks[i].checked=false;
                                 // alert(municipios_def_mun);
                                  
                                }

                              }

                              dialog.dialog( "close" );
                             // alert("datos concatenados");
                              return valid;

                             
                            }


                            function addEntidades()
                            {
                           
                                
                                $('#entidades_cargados').show();   
                                var valid = true;
                              
                               // allFields.removeClass( "ui-state-error" );

                               var arr_checks = document.getElementsByClassName("check_enti_tac");
                               var municipios_def="Entidades,";

                               arr_enti="{";

                                  for (i = 0; i < arr_checks.length; i++) 
                                {

                                  if(arr_checks[i].checked == true)
                                  {
                                    municipios_def+=i+",";

                                    arr_enti+="%"+arr_checks[i].name+"%:"+"%"+lat+" | "+lng+"%"+",";
   
                                  }

                              }


                              arr_enti = arr_enti.slice(0, -1);

                              arr_enti+="}";


                             //var des_geo_entidades_Def =document.getElementById("des_geo_entidades_list");
                             /*
                             des_geo_entidades_Def.value= municipios_def;
                             */  

                              if(indice==1)
                              {
                                resultados_1=municipios_def;
                                document.getElementById('datos_cargados_lug_1').innerHTML ="Datos Cargados";
                                var res1 =document.getElementById("res1_lugares");
                                res1.value= municipios_def;
                                 var element = document.getElementById("datos_cargados_lug_1");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_1').show();

                                var res1_ent_gps =document.getElementById("res1_lugares_gps");
                                  res1_ent_gps.value= arr_enti;

                              }
                              else if(indice==2)
                              {
                                resultados_2=municipios_def;
                                document.getElementById('datos_cargados_lug_2').innerHTML ="Datos Cargados";
                                var res2 =document.getElementById("res2_lugares");
                                res2.value= municipios_def;
                                 var element = document.getElementById("datos_cargados_lug_2");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_2').show();

                                var res2_ent_gps =document.getElementById("res2_lugares_gps");
                                  res2_ent_gps.value= arr_enti;
                              }
                              else if(indice==3)
                              {
                                resultados_3=municipios_def;
                                document.getElementById('datos_cargados_lug_3').innerHTML ="Datos Cargados";
                                var res3 =document.getElementById("res3_lugares");
                                res3.value= municipios_def;
                                 var element = document.getElementById("datos_cargados_lug_3");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_3').show();

                                var res3_ent_gps =document.getElementById("res3_lugares_gps");
                                  res3_ent_gps.value= arr_enti;
                              }
                              else if(indice==4)
                              {
                                resultados_4=municipios_def;
                                document.getElementById('datos_cargados_lug_4').innerHTML ="Datos Cargados";
                                var res4 =document.getElementById("res4_lugares");
                                res4.value= municipios_def;
                                 var element = document.getElementById("datos_cargados_lug_4");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_4').show();

                                var res4_ent_gps =document.getElementById("res4_lugares_gps");
                                  res4_ent_gps.value= arr_enti;
                              }
                              else if(indice==5)
                              {
                                resultados_5=municipios_def;
                                document.getElementById('datos_cargados_lug_5').innerHTML ="Datos Cargados";
                                var res5 =document.getElementById("res5_lugares");
                                res5.value= municipios_def;
                                 var element = document.getElementById("datos_cargados_lug_5");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_5').show();

                                var res5_ent_gps =document.getElementById("res5_lugares_gps");
                                  res5_ent_gps.value= arr_enti;
                              }

                              else if(indice==6)
                              {
                                resultados_6=municipios_def;
                                document.getElementById('datos_cargados_lug_6').innerHTML ="Datos Cargados";
                                var res6 =document.getElementById("res6_lugares");
                                res6.value= municipios_def;
                                 var element = document.getElementById("datos_cargados_lug_6");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_6').show();

                                var res6_ent_gps =document.getElementById("res6_lugares_gps");
                                res6_ent_gps.value= arr_enti;
                              }
                              else if(indice==7)
                              {
                                resultados_5=municipios_def;
                                document.getElementById('datos_cargados_lug_5').innerHTML ="Datos Cargados";
                                var res5 =document.getElementById("res5_lugares");
                                res5.value= municipios_def;
                                 var element = document.getElementById("datos_cargados_lug_5");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_5').show();

                                var res7_ent_gps =document.getElementById("res7_lugares_gps");
                                  res7_ent_gps.value= arr_enti;
                              }
                              else if(indice==8)
                              {
                                resultados_8=municipios_def;
                                document.getElementById('datos_cargados_lug_8').innerHTML ="Datos Cargados";
                                var res8 =document.getElementById("res8_lugares");
                                res8.value= municipios_def;
                                 var element = document.getElementById("datos_cargados_lug_8");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_8').show();

                                var res8_ent_gps =document.getElementById("res8_lugares_gps");
                                  res8_ent_gps.value= arr_enti;
                              }




                              if(est_tact==1)
                              {

                                if(indice==1)
                                {
                                  est_resultados_1=municipios_def;
                                  document.getElementById('est_datos_cargados_lug_1').innerHTML ="Datos Cargados";
                                  var res1 =document.getElementById("est_res1_lugares");
                                  res1.value= municipios_def;
                                   var element = document.getElementById("est_datos_cargados_lug_1");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#est_datos_cargados_lug_1').show();

                                  var res1_ent_gps =document.getElementById("est_res1_lugares_gps");
                                  res1_ent_gps.value= arr_enti;

                                }
                                else if(indice==2)
                                {
                                  est_resultados_2=municipios_def;
                                  document.getElementById('est_datos_cargados_lug_2').innerHTML ="Datos Cargados";
                                  var res2 =document.getElementById("est_res2_lugares");
                                  res2.value= municipios_def;
                                   var element = document.getElementById("est_datos_cargados_lug_2");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#est_datos_cargados_lug_2').show();

                                  var res2_ent_gps =document.getElementById("est_res2_lugares_gps");
                                  res2_ent_gps.value= arr_enti;
                                }
                                else if(indice==3)
                                {
                                  est_resultados_3=municipios_def;
                                  document.getElementById('est_datos_cargados_lug_3').innerHTML ="Datos Cargados";
                                  var res3 =document.getElementById("est_res3_lugares");
                                  res3.value= municipios_def;
                                   var element = document.getElementById("est_datos_cargados_lug_3");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#est_datos_cargados_lug_3').show();

                                  var res3_ent_gps =document.getElementById("est_res3_lugares_gps");
                                  res3_ent_gps.value= arr_enti;
                                }

                                else if(indice==4)
                                {
                                  est_resultados_4=municipios_def;
                                  document.getElementById('est_datos_cargados_lug_4').innerHTML ="Datos Cargados";
                                  var res4 =document.getElementById("est_res4_lugares");
                                  res4.value= municipios_def;
                                   var element = document.getElementById("est_datos_cargados_lug_4");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#est_datos_cargados_lug_4').show();

                                  var res4_ent_gps =document.getElementById("est_res4_lugares_gps");
                                  res4_ent_gps.value= arr_enti;
                                }

                                else if(indice==5)
                                {
                                  est_resultados_5=municipios_def;
                                  document.getElementById('est_datos_cargados_lug_5').innerHTML ="Datos Cargados";
                                  var res5 =document.getElementById("est_res5_lugares");
                                  res5.value= municipios_def;
                                   var element = document.getElementById("est_datos_cargados_lug_5");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#est_datos_cargados_lug_5').show();

                                  var res5_ent_gps =document.getElementById("est_res5_lugares_gps");
                                  res5_ent_gps.value= arr_enti;
                                }

                                else if(indice==6)
                                {
                                  est_resultados_6=municipios_def;
                                  document.getElementById('est_datos_cargados_lug_6').innerHTML ="Datos Cargados";
                                  var res6 =document.getElementById("est_res6_lugares");
                                  res6.value= municipios_def;
                                   var element = document.getElementById("est_datos_cargados_lug_6");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#est_datos_cargados_lug_6').show();

                                  var res6_ent_gps =document.getElementById("est_res6_lugares_gps");
                                  res6_ent_gps.value= arr_enti;
                                }

                                else if(indice==7)
                                {
                                  est_resultados_7=municipios_def;
                                  document.getElementById('est_datos_cargados_lug_7').innerHTML ="Datos Cargados";
                                  var res7 =document.getElementById("est_res7_lugares");
                                  res7.value= municipios_def;
                                   var element = document.getElementById("est_datos_cargados_lug_7");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#est_datos_cargados_lug_7').show();

                                  var res7_ent_gps =document.getElementById("est_res7_lugares_gps");
                                  res7_ent_gps.value= arr_enti;
                                }


                                else if(indice==8)
                                {
                                  est_resultados_8=municipios_def;
                                  document.getElementById('est_datos_cargados_lug_8').innerHTML ="Datos Cargados";
                                  var res8 =document.getElementById("est_res8_lugares");
                                  res8.value= municipios_def;
                                   var element = document.getElementById("est_datos_cargados_lug_8");
                                  element.classList.remove("error_mensaje");
                                  element.classList.add("datos_cargados_lugar_resultados"); 
                                  $('#est_datos_cargados_lug_8').show();

                                  var res8_ent_gps =document.getElementById("est_res8_lugares_gps");
                                  res8_ent_gps.value= arr_enti;
                                }
                               

                              }



                              if(document.getElementById('res1_lugares_'+id_select) && indice==1)
                              {
                                //guardar info localidades en input 
                                var lugar_1_edit=document.getElementById('res1_lugares_'+id_select);

                                 // alert(lugar_1_edit.name);
                                  // alert(municipios_def);

                                  $("#res1_lugares_"+id_select).val(municipios_def);


                                lugar_1_edit.value = municipios_def;
                                //mensaje datos cargados
                                document.getElementById('datos_cargados_lug_1_'+id_select).innerHTML ="Datos Cargados";
                                var element = document.getElementById("datos_cargados_lug_1_"+id_select);
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_1_'+id_select).show();


                                 var res1_ent_gps =document.getElementById("res1_lugares_GPS_"+id_select);
                                res1_ent_gps.value= arr_enti;




                              }

                              else if(document.getElementById('res2_lugares_'+id_select) && indice==2)
                              {
                                var lugar_1_edit=document.getElementById('res2_lugares_'+id_select);
                                lugar_1_edit.value=municipios_def;

                                                            //mensaje datos cargados
                                document.getElementById('datos_cargados_lug_2_'+id_select).innerHTML ="Datos Cargados";
                                var element = document.getElementById("datos_cargados_lug_2_"+id_select);
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_2_'+id_select).show();

                                 var res1_ent_gps =document.getElementById("res2_lugares_GPS_"+id_select);
                                res1_ent_gps.value= arr_enti;



                              }

                              else if(document.getElementById('res3_lugares_'+id_select) && indice==3)
                              {
                                var lugar_1_edit=document.getElementById('res3_lugares_'+id_select);
                                lugar_1_edit.value=municipios_def;

                                                            //mensaje datos cargados
                                document.getElementById('datos_cargados_lug_3_<?php echo($registro_2['id_indicador_asociado_def'])?>').innerHTML ="Datos Cargados";
                                var element = document.getElementById("datos_cargados_lug_3_<?php echo($registro_2['id_indicador_asociado_def'])?>");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_3_<?php echo($registro_2['id_indicador_asociado_def'])?>').show();

                                 var res1_ent_gps =document.getElementById("res3_lugares_GPS_"+id_select);
                                res1_ent_gps.value= arr_enti;



                              }

                              else if(document.getElementById('res4_lugares_'+id_select) && indice==4)
                              {
                                var lugar_1_edit=document.getElementById('res4_lugares_'+id_select);
                                lugar_1_edit.value=municipios_def;

                                                            //mensaje datos cargados
                                document.getElementById('datos_cargados_lug_4_'+id_select).innerHTML ="Datos Cargados";
                                var element = document.getElementById("datos_cargados_lug_4_"+id_select);
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_4_'+id_select).show();

                                 var res1_ent_gps =document.getElementById("res4_lugares_GPS_"+id_select);
                                res1_ent_gps.value= arr_enti;


                              }

                              else if(document.getElementById('res5_lugares_'+id_select) && indice==5)
                              {
                                var lugar_1_edit=document.getElementById('res5_lugares_'+id_select);
                                lugar_1_edit.value=municipios_def;

                                                            //mensaje datos cargados
                                document.getElementById('datos_cargados_lug_5_'+id_select).innerHTML ="Datos Cargados";
                                var element = document.getElementById("datos_cargados_lug_5_"+id_select);
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_5_'+id_select).show();

                                 var res1_ent_gps =document.getElementById("res5_lugares_GPS_"+id_select);
                                res1_ent_gps.value= arr_enti;


                              }

                              else if(document.getElementById('res6_lugares_'+id_select) && indice==6)
                              {
                                var lugar_1_edit=document.getElementById('res6_lugares_'+id_select);
                                lugar_1_edit.value=municipios_def;

                                                            //mensaje datos cargados
                                document.getElementById('datos_cargados_lug_6_'+id_select).innerHTML ="Datos Cargados";
                                var element = document.getElementById("datos_cargados_lug_6_"+id_select);
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_6_'+id_select).show();

                                 var res1_ent_gps =document.getElementById("res6_lugares_GPS_"+id_select);
                                res1_ent_gps.value= arr_enti;

                              }

                              else if(document.getElementById('res7_lugares_'+id_select) && indice==7)
                              {
                                var lugar_1_edit=document.getElementById('res7_lugares_'+id_select);
                                lugar_1_edit.value=municipios_def;

                                                            //mensaje datos cargados
                                document.getElementById('datos_cargados_lug_7_'+id_select).innerHTML ="Datos Cargados";
                                var element = document.getElementById("datos_cargados_lug_7_"+id_select);
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_7_'+id_select).show();

                                 var res1_ent_gps =document.getElementById("res7_lugares_GPS_"+id_select);
                                res1_ent_gps.value= arr_enti;

                              }

                              else if(document.getElementById('res8_lugares_'+id_select) && indice==8)
                              {
                                var lugar_1_edit=document.getElementById('res8_lugares_'+id_select);
                                lugar_1_edit.value=municipios_def;

                                                            //mensaje datos cargados
                                document.getElementById('datos_cargados_lug_8_'+id_select).innerHTML ="Datos Cargados";
                                var element = document.getElementById("datos_cargados_lug_8_"+id_select);
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#datos_cargados_lug_8_'+id_select).show();


                                 var res1_ent_gps =document.getElementById("res8_lugares_GPS_"+id_select);
                                res1_ent_gps.value= arr_enti;



                              }






                               ///EDICION ESTRATEGICOS

                              // alert('est_res1_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>');



                              if(document.getElementById('est_res1_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>') && indice==1)
                              {

                                //guardar info localidades en input 
                                var lugar_1_edit=document.getElementById('est_res1_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>');

                                //alert("update ubicacion 1");
                                
                                lugar_1_edit.value=municipios_def;
                                //mensaje datos cargados
                                document.getElementById('est_datos_cargados_lug_1_edit_<?php echo($registro_2['id_ind_estrategico'])?>').innerHTML ="Datos Cargados";
                                var element = document.getElementById("est_datos_cargados_lug_1_edit_<?php echo($registro_2['id_ind_estrategico'])?>");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#est_datos_cargados_lug_1_edit_<?php echo($registro_2['id_ind_estrategico'])?>').show();

                                var resres1_loc_gps1_loc =document.getElementById("edit_res1_lugares_GPS_<?php echo($registro_2['id_ind_estrategico'])?>");
                                  resres1_loc_gps1_loc.value= arr_enti;  

                              }



                              if(document.getElementById('est_res2_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>') && indice==2)
                              {

                                //guardar info localidades en input 
                                var lugar_1_edit=document.getElementById('est_res2_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>');

                                //alert("update ubicacion 1");
                                
                                lugar_1_edit.value=municipios_def;
                                //mensaje datos cargados
                                document.getElementById('est_datos_cargados_lug_2_edit_<?php echo($registro_2['id_ind_estrategico'])?>').innerHTML ="Datos Cargados";
                                var element = document.getElementById("est_datos_cargados_lug_2_edit_<?php echo($registro_2['id_ind_estrategico'])?>");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#est_datos_cargados_lug_2_edit_<?php echo($registro_2['id_ind_estrategico'])?>').show();

                                var resres1_loc_gps1_loc =document.getElementById("edit_res2_lugares_GPS_<?php echo($registro_2['id_ind_estrategico'])?>");
                                  resres1_loc_gps1_loc.value=arr_enti;  

                              }

                              if(document.getElementById('est_res3_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>') && indice==3)
                              {

                                //guardar info localidades en input 
                                var lugar_2_edit=document.getElementById('est_res3_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>');

                                //alert("update ubicacion 2");
                                
                                lugar_2_edit.value=municipios_def;
                                //mensaje datos cargados
                                document.getElementById('est_datos_cargados_lug_3_edit_<?php echo($registro_2['id_ind_estrategico'])?>').innerHTML ="Datos Cargados";
                                var element = document.getElementById("est_datos_cargados_lug_3_edit_<?php echo($registro_2['id_ind_estrategico'])?>");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#est_datos_cargados_lug_3_edit_<?php echo($registro_2['id_ind_estrategico'])?>').show();

                                var resres2_loc_gps2_loc =document.getElementById("edit_res3_lugares_GPS_<?php echo($registro_2['id_ind_estrategico'])?>");
                                  resres2_loc_gps2_loc.value= arr_enti;  

                              }

                              if(document.getElementById('est_res4_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>') && indice==4)
                              {

                                //guardar info localidades en input 
                                var lugar_2_edit=document.getElementById('est_res4_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>');

                                //alert("update ubicacion 2");
                                
                                lugar_2_edit.value=municipios_def;
                                //mensaje datos cargados
                                document.getElementById('est_datos_cargados_lug_4_edit_<?php echo($registro_2['id_ind_estrategico'])?>').innerHTML ="Datos Cargados";
                                var element = document.getElementById("est_datos_cargados_lug_4_edit_<?php echo($registro_2['id_ind_estrategico'])?>");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#est_datos_cargados_lug_4_edit_<?php echo($registro_2['id_ind_estrategico'])?>').show();

                                var resres2_loc_gps2_loc =document.getElementById("edit_res4_lugares_GPS_<?php echo($registro_2['id_ind_estrategico'])?>");
                                  resres2_loc_gps2_loc.value= arr_enti;  

                              }


                              if(document.getElementById('est_res5_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>') && indice==5)
                              {

                                //guardar info localidades en input 
                                var lugar_3_edit=document.getElementById('est_res5_lugares_edit_<?php echo($registro_2['id_ind_estrategico'])?>');

                                //alert("update ubicacion 2");
                                
                                lugar_3_edit.value=municipios_def;
                                //mensaje datos cargados
                                document.getElementById('est_datos_cargados_lug_5_edit_<?php echo($registro_2['id_ind_estrategico'])?>').innerHTML ="Datos Cargados";
                                var element = document.getElementById("est_datos_cargados_lug_5_edit_<?php echo($registro_2['id_ind_estrategico'])?>");
                                element.classList.remove("error_mensaje");
                                element.classList.add("datos_cargados_lugar_resultados"); 
                                $('#est_datos_cargados_lug_5_edit_<?php echo($registro_2['id_ind_estrategico'])?>').show();

                                var resres2_loc_gps2_loc =document.getElementById("edit_res5_lugares_GPS_<?php echo($registro_2['id_ind_estrategico'])?>");
                                  resres2_loc_gps2_loc.value= arr_enti;  

                              }





                             $( "#dialog-form-entidades" ).dialog( "close" );
                          
                              dialog_entidades.dialog( "close" );

                              
                             // alert("datos concatenados");
                              return valid;
                            }
                            //Cuadro de dialogo de municicpios
                              dialog = $( "#dialog-form" ).dialog({
                                  autoOpen: false,
                                  height: 700,
                                  width: 600,
                                  modal: true,
                                  buttons: {
                                    "Guardar":{
                                     text: "Guardar",
                                     id: "btn_guardar_municipios",
                                     click: addUser

                                      },

                                    Cancel: function() {
                                      dialog.dialog( "close" );
                                    }
                                  },
                                  close: function() {
                                    form[ 0 ].reset();
                                    allFields.removeClass( "ui-state-error" );
                                  }
                            });
                         
                            form = dialog.find( "form" ).on( "submit", function( event ) {
                              event.preventDefault();
                              addUser();

                            });

                            //Cuadro de dialogo de entidades
                            dialog_entidades = $( "#dialog-form-entidades" ).dialog({
                                autoOpen: false,
                                height: 700,
                                width: 600,
                                modal: true,
                                buttons: {
                                  "Guardar": {
                                   text: "Guardar",
                                   id: "btn_guardar_estados",
                                   click: addEntidades

                                    },


                                  Cancel: function() {
                                    dialog_entidades.dialog( "close" );
                                  }
                                },
                                close: function() {
                                  form[ 0 ].reset();
                                  allFields.removeClass( "ui-state-error" );
                                }
                            });
                         
                            form = dialog_entidades.find( "form" ).on( "submit", function( event ) {
                              event.preventDefault();
                              addEntidades();

                              //alert("click en guardar municipios");
                            });



                            // fin entidades
                         


                              $('#es_posible_desagregar_geograficamente_def').change(function()
                              { 

                              if($(this).val() == "Municipios")
                              {
                                 dialog.dialog( "open" );
                                 $('#upload_tabla_localidades').hide();
                                  $('#entidades_cargados').hide();
                                  $('#municipios_cargados').hide();
                                  // document.getElementById('des_geo_entidades').value="0" ; 

                              }

                              else if($(this).val() == "Localidades" ){
                                 $('#upload_tabla_localidades').show();
                                 $('#municipios_cargados').hide();//ocultar mensaje de municipiuos cargardos
                                 $('#entidades_cargados').hide();
                                 
                              }

                              else if($(this).val() == "Otras entidades"){
                                 dialog_entidades.dialog( "open" );

                                 $('#upload_tabla_localidades').hide();
                                 $('#municipios_cargados').hide();  //ocultar mensaje de municipiuos cargardos
                                 $('#entidades_cargados').hide();
                                 //document.getElementById('des_geo_municipios_list').value="0" ; 
                              }

                              else if($(this).val() == "Estado de Hidalgo"){
                                 $('#upload_tabla_localidades').hide();
                                 $('#municipios_cargados').hide();  //ocultar mensaje de municipiuos cargardos
                                 $('#entidades_cargados').hide();
                              }

                              });

                          } );

                        </script>
                        <!--DIALOG ADD MUNICIPIOS-->

                        <!-- DIALOGO CONFIRMACION-->
                          <script>

                            $( function()
                              {
                                $( "#dialog-message-adjuntar-localidades" ).dialog({
                                  autoOpen: false,
                                  modal: true,
                                  height: 350,
                                  width: 450,
                                  buttons: {
                                    Ok: function() {
                                      $( this ).dialog( "close" );
                                    }
                                  }
                                });
                              } );



                            
                              $( function()
                              {
                                $( "#dialog-message-lugar" ).dialog({
                                  autoOpen: false,
                                  modal: true,
                                  height: 350,
                                  width: 450,
                                  buttons: {
                                    Ok: function() {
                                      $( this ).dialog( "close" );
                                    }
                                  }
                                });
                              } );



                             $( function()
                              {
                                $( "#dialog-message" ).dialog({
                                  autoOpen: false,
                                  modal: true,
                                  height: 350,
                                  width: 450,
                                  buttons: {
                                    Ok: function() {
                                      $( this ).dialog( "close" );
                                    }
                                  }
                                });
                              } );


                              $('#plan_trabajo_def').change(function()
                              { 
                                  

                                if($(this).val() == "No")
                                {
                                $( "#dialog-message" ).dialog( "open" );

                                }
                              });
                              //si se checa la opcion otro adjuntar el docuemento q necesitan
                              $('#plan_trabajo_def').change(function()
                              { 
                                  

                                if($(this).val() == "No")
                                {
                                $( "#dialog-message" ).dialog( "open" );

                                }
                              });

                              $('#check_evi_Otro').change(function()
                              { 
                                //alert("holasss")
                                    
                                   var check_otros = document.getElementById("check_evi_Otro");

                            
                                  if(check_otros.checked == true)
                                  {
                                     $('#tipo_evidencia_otro').show();
                                  }
                                  else
                                  {
                                    $('#tipo_evidencia_otro').hide();

                                  }


                              });

                              $('#se_desagrego_geograficamente_def').change(function()
                              { 
                               
                                   var check_otros = document.getElementById("check_evi_Otro");

                            
                                   if($(this).val() == "Si")
                                  {
                                     $('#si_existe_informacion_acciones').show();
                                  }
                                  else
                                  {
                                    $('#si_existe_informacion_acciones').hide();

                                  }


                              });

                            $('#logros_en_indicador').change(function()
                              { 
                               

                                   if($(this).val() == "Si")
                                  {
                                     $('#bullet_Del_logro').show();
                                  }
                                  else if($(this).val() == "No")
                                  {
                                    $('#bullet_Del_logro').hide();
                                  }


                              });


                            $('#premios_recono').change(function()
                              { 
                               

                                   if($(this).val() == "Si")
                                  {
                                     $('#premios_reconocimientos').show();
                                  }
                                  else if($(this).val() == "No")
                                  {
                                    $('#premios_reconocimientos').hide();
                                  }


                              });


                            //
                            /*


                            */

                             


                            

                          </script>


                          <script type='text/javascript'>
                            //ENTRAR A REGISTRO TACTICO
                            //ENTRAR A REGISTRO ESTRATEGICO

                            function abrir_estrategico()
                            {
                              //alert("iniciar el formulario estrategico");
                              $('#estrategicos').show(); 
                               $('#menu_registro_indicador_dependencias').hide();
                               $('#menu_registro_indicador_orgnismo').hide();

                               
                              
                            }
                               function abrir_tactico()
                            {
                              //alert("iniciar el formulario tactico");
                              $('#tacticos').show();  
                              $('#menu_registro_indicador_dependencias').hide();
                              $('#menu_registro_indicador_orgnismo').hide(); 
                              
                            }

                            function abrir_avance_programatico()
                            {
                              //alert("iniciar el formulario tactico");
                              $('#avance_programatico').show();  
                              $('#menu_registro_indicador_dependencias').hide(); 
                              $('#menu_registro_indicador_orgnismo').hide();
                              
                            }

                            function abrir_acciones_concurrentes()
                            {
                              //alert("iniciar el formulario tactico");
                              $('#acciones_concurrentes').show();  
                              $('#menu_registro_indicador_dependencias').hide();
                              $('#menu_registro_indicador_orgnismo').hide(); 
                              
                            }
                            function abrir_acciones_sin_indicador()
                            {
                              //alert("sin indcado");
                              //alert("iniciar el formulario tactico");
                              $('#acciones_concurrentes_sin_indicadores').show();
                              $('#menu_registro_indicador_dependencias').hide();
                              $('#menu_registro_indicador_orgnismo').hide();
                            }

                             function abrir_pid()
                            {
                              //alert("sin indcado");
                              //alert("iniciar el formulario tactico");
                               $('#seccion_pids').show();
                              $('#menu_registro_indicador_dependencias').hide();
                              $('#menu_registro_indicador_orgnismo').hide();
                            }

 				function abrir_covid()
                            {
                              //alert("sin indcado");
                              //alert("iniciar el formulario tactico");
                              // $('#seccion_pids').show();
				                      $('#seccion_covid').show();


                              $('#menu_registro_indicador_dependencias').hide();
                              $('#menu_registro_indicador_orgnismo').hide();


                              $('#menu_covid').hide();
                            }

                            
                             function getHouseModel(){
                                var model=$('#house_model').val();

                               // alert(model);
                            }
                          
                          </script>

                          <script type="text/javascript">
                          
                          var i, tabcontent, tablinks;
                              tabcontent = document.getElementsByClassName("tabcontent_est");
                              for (i = 0; i < tabcontent.length; i++) {
                                  tabcontent[i].style.display = "none";
                              }
                          
                           function open_tab_est(evt, cityName) 
                          {
                            $('#intro_idicador_testrategico').hide();// quitar el mensaje de intro

                              var i, tabcontent, tablinks;
                              tabcontent = document.getElementsByClassName("tabcontent_est");
                              for (i = 0; i < tabcontent.length; i++) {
                                  tabcontent[i].style.display = "none";
                              }
                              tablinks = document.getElementsByClassName("tablinks_est");
                              for (i = 0; i < tablinks.length; i++) {
                                  tablinks[i].className = tablinks[i].className.replace(" active", "");
                              }
                              document.getElementById(cityName).style.display = "block";
                              evt.currentTarget.className += " active";
                          }

                            function open_tab_cov(evt, cityName) 
                          {

                            alert("covid");
                            $('#intro_idicador_covid').hide();// quitar el mensaje de intro

                              var i, tabcontent, tablinks;
                              tabcontent = document.getElementsByClassName("tabcontent_cov");
                              for (i = 0; i < tabcontent.length; i++) {
                                  tabcontent[i].style.display = "none";
                              }
                              tablinks = document.getElementsByClassName("tablinks_cov");
                              for (i = 0; i < tablinks.length; i++) {
                                  tablinks[i].className = tablinks[i].className.replace(" active", "");
                              }
                              document.getElementById(cityName).style.display = "block";
                              evt.currentTarget.className += " active";
                          }


                          

                          // Get the element with id="defaultOpen" and click on it
                        //  document.getElementById("defaultOpen_est").click();
                          </script>

                          <script type="text/javascript">
                            /* Ir al menu indicadores*/
                          

                            function ir_menu_indicadores()
                            {
                              $('#estrategicos').hide(); 
                              $('#tacticos').hide();  
                              $('#avance_programatico').hide();
                              $('#acciones_concurrentes').hide(); 
                              $('#acciones_concurrentes_sin_indicadores').hide(); 
                              $('#seccion_pids').hide();
                              $('#seccion_covid').hide();




                            if("<?php echo ($user_name); ?>"=="Brian" || "<?php echo ($user_name); ?>" =="VALLEPLATA" ||
                                "<?php echo ($user_name); ?>"=="AE_ENERGIA" || "<?php echo ($user_name); ?>"=="BACEH" ||
                                "<?php echo ($user_name); ?>"=="CENJ_MUJERES" || "<?php echo ($user_name); ?>"=="CEN_MAQUIDES" ||
                                "<?php echo ($user_name); ?>"=="COBAEH" || "<?php echo ($user_name); ?>"=="COEPROTEH" ||
                                "<?php echo ($user_name); ?>"=="CECYTEH" || "<?php echo ($user_name); ?>"=="CAASIM" ||
                                "<?php echo ($user_name); ?>"=="CEAA" || "<?php echo ($user_name); ?>"=="CEVIVIENDA" ||
                                "<?php echo ($user_name); ?>"=="COFOIN" || "<?php echo ($user_name); ?>"=="COINH" ||
                                "<?php echo ($user_name); ?>"=="DESSSH" || "<?php echo ($user_name); ?>"=="COEH" ||

                                "<?php echo ($user_name); ?>"=="IDIHCE" || "<?php echo ($user_name); ?>"=="ICATHI" ||
                                "<?php echo ($user_name); ?>"=="IHCE" || "<?php echo ($user_name); ?>"=="IHFES" ||
                                "<?php echo ($user_name); ?>"=="IHEA" || "<?php echo ($user_name); ?>"=="INHIFE" ||
                                "<?php echo ($user_name); ?>"=="IHM" || "<?php echo ($user_name); ?>"=="INHIDE" ||
                                "<?php echo ($user_name); ?>"=="IHDM" || "<?php echo ($user_name); ?>"=="ITESA" ||

                                "<?php echo ($user_name); ?>"=="ITESHU" || "<?php echo ($user_name); ?>"=="ITSOEH" ||
                                "<?php echo ($user_name); ?>"=="OEEH" || "<?php echo ($user_name); ?>"=="PIBEH" ||
                                "<?php echo ($user_name); ?>"=="REPSS" || "<?php echo ($user_name); ?>"=="UICEH" || "<?php echo ($user_name); ?>"=="UPFIM" ||
                                "<?php echo ($user_name); ?>"=="UPH" || "<?php echo ($user_name); ?>"=="UPENERGIA" ||


                                "<?php echo ($user_name); ?>"=="UPP" || "<?php echo ($user_name); ?>"=="UPT" ||
                                "<?php echo ($user_name); ?>"=="UPMETRO" || "<?php echo ($user_name); ?>"=="UTHH" ||
                                "<?php echo ($user_name); ?>"=="UTSH" || "<?php echo ($user_name); ?>"=="UTVAM" ||
                                "<?php echo ($user_name); ?>"=="UTMIR" || "<?php echo ($user_name); ?>"=="UTVM" ||
                                "<?php echo ($user_name); ?>"=="UTTT" || "<?php echo ($user_name); ?>"=="UPENERGIA" ||
                                "<?php echo ($user_name); ?>"=="UTMZ" || "<?php echo ($user_name); ?>"=="SSH" ||
                    		"<?php echo ($user_name); ?>"=="CCYCULTURA" || "<?php echo ($user_name); ?>"=="UTECT"||
		    		"<?php echo ($user_name); ?>"=="IAAMEH" || "<?php echo ($user_name); ?>"=="CATASTRO" ||
		   		"<?php echo ($user_name); ?>"=="CEDSPI" || "<?php echo ($user_name); ?>"=="CAASVAM" || 
				"<?php echo ($user_name); ?>"=="CMUJERES" || "<?php echo ($user_name); ?>"=="SITMAH" || 
		"<?php echo ($user_name); ?>"=="COHI_CAFE" || "<?php echo ($user_name); ?>"=="AT_FYBG" || 
		"<?php echo ($user_name); ?>"=="INHIDE" || 
		"<?php echo ($user_name); ?>"=="IHJ" 


                                  )
                               
                                {

                                  
                                  $('#menu_registro_indicador_orgnismo').show();
                                  
                                }
                                else if ("<?php echo ($user_name); ?>"=="REHILETE" || "<?php echo ($user_name); ?>" =="CITNOVA" || "<?php echo ($user_name); ?>" =="STCH")
                                {
                                  $('#menu_registro_indicador_orgnismo').show();
                                  $('#menu_registro_indicador_dependencias').show();
                                  
                                }
				else
				{
					$('#menu_registro_indicador_dependencias').show();

				}
       
                            }

                            

                            function calcuar_avance()
                            {

                              var avance_2018_entra = document.getElementById("resultado_19");

                              var avance_2018_db = document.getElementById("meta_2019");
                              var meta_numb=parseFloat(avance_2018_db.value);
                  

                              

                              var avance_meta_planeada_op = document.getElementById("avance_meta_planeada_def");
                                    avance_meta_planeada_op.value=(avance_2018_entra.value/meta_numb * 100).toFixed(2);
                              


                              
                             
                            }

                            function calcuar_avance_estrategico()
                            {

                              var meta_2018 = document.getElementById("meta_2018");

                              var est_res_abril = document.getElementById("est_res_abril");
                             // var meta_numb=parseFloat(est_res_abril.value);
                  

                              

                              var est_avance_meta_est = document.getElementById("est_avance_meta_est");
                              est_avance_meta_est.value=((est_res_abril.value/meta_2018.value) * 100).toFixed(2);
                              


                              
                             
                            }
                                        

                            

                          </script>

                          <script type="text/javascript">

                          </script>


                      </main>
                  </div>

              </div>
              <br>
                          <br>
                          <br>
                          <br>
                          <br>
                          <br>
                          <br>
                        
              <script>



               function entidad_selec(nombre_entidad,id_entidad)
               {


                if(document.getElementById('check_evi_'+nombre_entidad).checked )
                {
                  //alert("es true");

                  $("#Modal_entidades").modal();

                
                  
                }
                else
                {
                  //eliminar las coordenadas de esa posicion
                  //alert('falso');
                }
                  
                  
               }

                function muni_selec(nombre_entidad,id_entidad)
               {

                
                $("#Modal_entidades").modal();

               }




                var myVar;

                function myFunction() {
                    myVar = setTimeout(showPage, 1000);


                  if("<?php echo ($user_name); ?>"=="Brian" || "<?php echo ($user_name); ?>" =="VALLEPLATA" ||
                    "<?php echo ($user_name); ?>"=="AE_ENERGIA" || "<?php echo ($user_name); ?>"=="BACEH" ||
                    "<?php echo ($user_name); ?>"=="CENJ_MUJERES" || "<?php echo ($user_name); ?>"=="CEN_MAQUIDES" ||
                    "<?php echo ($user_name); ?>"=="COBAEH" || "<?php echo ($user_name); ?>"=="COEPROTEH" ||
                    "<?php echo ($user_name); ?>"=="CECYTEH" || "<?php echo ($user_name); ?>"=="CAASIM" ||
                    "<?php echo ($user_name); ?>"=="CEAA" || "<?php echo ($user_name); ?>"=="CEVIVIENDA" ||
                    "<?php echo ($user_name); ?>"=="COFOIN" || "<?php echo ($user_name); ?>"=="COINH" ||
                    "<?php echo ($user_name); ?>"=="DESSSH" || "<?php echo ($user_name); ?>"=="COEH" ||

                    "<?php echo ($user_name); ?>"=="IDIHCE" || "<?php echo ($user_name); ?>"=="ICATHI" ||
                    "<?php echo ($user_name); ?>"=="IHCE" || "<?php echo ($user_name); ?>"=="IHFES" ||
                    "<?php echo ($user_name); ?>"=="IHEA" || "<?php echo ($user_name); ?>"=="INHIFE" ||
                    "<?php echo ($user_name); ?>"=="IHM" || "<?php echo ($user_name); ?>"=="INHIDE" ||
                    "<?php echo ($user_name); ?>"=="IHDM" || "<?php echo ($user_name); ?>"=="ITESA" ||

                    "<?php echo ($user_name); ?>"=="ITESHU" || "<?php echo ($user_name); ?>"=="ITSOEH" ||
                    "<?php echo ($user_name); ?>"=="OEEH" || "<?php echo ($user_name); ?>"=="PIBEH" ||
                    "<?php echo ($user_name); ?>"=="REPSS"  ||
                    "<?php echo ($user_name); ?>"=="UICEH" || "<?php echo ($user_name); ?>"=="UPFIM" ||
                    "<?php echo ($user_name); ?>"=="UPH" || "<?php echo ($user_name); ?>"=="UPENERGIA" ||


                    "<?php echo ($user_name); ?>"=="UPP" || "<?php echo ($user_name); ?>"=="UPT" ||
                    "<?php echo ($user_name); ?>"=="UPMETRO" || "<?php echo ($user_name); ?>"=="UTHH" ||
                    "<?php echo ($user_name); ?>"=="UTSH" || "<?php echo ($user_name); ?>"=="UTVAM" ||
                    "<?php echo ($user_name); ?>"=="UTMIR" || "<?php echo ($user_name); ?>"=="UTVM" ||
                    "<?php echo ($user_name); ?>"=="UTTT" || "<?php echo ($user_name); ?>"=="UPENERGIA" ||
                    "<?php echo ($user_name); ?>"=="UTMZ" || "<?php echo ($user_name); ?>"=="SSH" ||
                    "<?php echo ($user_name); ?>"=="CCYCULTURA" || "<?php echo ($user_name); ?>"=="UTECT" ||
		    "<?php echo ($user_name); ?>"=="IAAMEH" || "<?php echo ($user_name); ?>"=="CATASTRO" ||
		   "<?php echo ($user_name); ?>"=="CEDSPI" || "<?php echo ($user_name); ?>"=="CAASVAM" || 
		"<?php echo ($user_name); ?>"=="CMUJERES" || 
		"<?php echo ($user_name); ?>"=="SITMAH" || 
		"<?php echo ($user_name); ?>"=="COHI_CAFE" || 
		"<?php echo ($user_name); ?>"=="AT_FYBG" || 
		"<?php echo ($user_name); ?>"=="INHIDE" || 
		"<?php echo ($user_name); ?>"=="IHJ" 
                      )
                   
                    {
                      
                      document.getElementById("menu_registro_indicador_orgnismo").style.display = null;
                      
                    }
				else if ("<?php echo ($user_name); ?>"=="REHILETE" || "<?php echo ($user_name); ?>" =="CITNOVA" || "<?php echo ($user_name); ?>" =="STCH" )
                                {
                                  $('#menu_registro_indicador_orgnismo').show();
                                  $('#menu_registro_indicador_dependencias').show();
                                  
                                }

                    else
                    {
                      
                      document.getElementById("menu_registro_indicador_dependencias").style.display = null;
                      
                    }
                    
                }

                function showPage() {
                  document.getElementById("loader").style.display = "none";
                  document.getElementById("myDiv").style.display = "block";
                }


                function over_menu(num_menu)
                {
                  if(num_menu==1)
                  {
                    document.getElementById("texto_selecionado").innerHTML = "Aquí podrás registrar, editar e imprimir los reportes de tus INDICADORES ESTRATEGICOS";
                  }
                  else  if(num_menu==2)
                  {
                    document.getElementById("texto_selecionado").innerHTML = "Aquí podrás registrar, editar e imprimir las ACCIONES SIN INDICADOR";
                  }
                  else  if(num_menu==3)
                  {
                    document.getElementById("texto_selecionado").innerHTML = "Aquí podrás registrar, editar e imprimir tu AVACE PROGRAMATICO";
                  }
                  else  if(num_menu==4)
                  {
                    document.getElementById("texto_selecionado").innerHTML = "Aquí podrás registrar, editar e imprimir tus ACCIONES CONCURRENTES";
                  }
                  else  if(num_menu==5)
                  {
                    document.getElementById("texto_selecionado").innerHTML = "Aquí podrás registrar, editar, Enviar evidencias de tus indicadores, e imprimir los reportes de tus INDICADORES TACTICOS";
                  }
                   else  if(num_menu==6)
                  {
                    document.getElementById("texto_selecionado").innerHTML = "Aquí podrás registrar, editar, Enviar evidencias de tus indicadores, e imprimir los reportes de tus Indicadores tacticos del Programa Institucional de Desarrollo";
                  }
else  if(num_menu==7)
                  {
                    document.getElementById("texto_selecionado").innerHTML = "Aquí podrás registrar, editar e imprimir los reportes correspondientes a COVID-19";
                  }
                  //alert(num_menu);
                  

                }

                function msj_to_eval(id_indicador)
                {
                 // alert("enviar mensaje al aevaluador"+id_indicador);
                  var mensaje_dependencia = document.getElementById("msj_ev"+id_indicador);
                  mensaje_dependencia=mensaje_dependencia.value;
                 // alert(mensaje_dependencia);
                 

                     $.ajax({
                            type: "POST",
                            url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                            data: {id_indicador:id_indicador,mensaje_dependencia:mensaje_dependencia}
                            ,
                            success: function(msg){
                             // console.log(msg)
                              alert( "Mensaje Enviado" );
                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown) {
                               alert( "Error Verifica tu informacion" );
                            }
                          });

                }


                 function ver_archivo(ruta_archivo,nom_archivo)
                {
                  var direc="/repositorio/upload_files_plugin/php/"+ruta_archivo+nom_archivo;
                  
                  //document.getElementById('iframe_ver').src = direc;
		window.open(direc);	 
			
               




                  
           /*       if($.UrlExists(direc))
                  {
                     document.getElementById('iframe_ver').src = direc;
                  }

                  else
                  {
                   nom_archivo= nom_archivo.replace('rev_2_', '');
                  // alert(nom_archivo);

                    direc="http://p-sigeh.hidalgo.gob.mx/repositorio/upload_files_plugin/php/"+ruta_archivo+nom_archivo;
			 
		    document.getElementById('iframe_ver').src = direc;
                
                  }  */
                  //alert(ruta_archivo);
                  //alert(nom_archivo);
                  
                  
                 
                }
              </script>

               <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCnopPi-29aspJWPJveFGXJSJUDAc50YiE&libraries=places&callback=initMap"
    async defer></script>


 <div class="row" >
                  <div class="col-xs-2">...</div>
                  <div class="col-xs-8" >  <Label style=" color: #4CAF50; font-size: 20px; font-family: Graphik-Bold" id="texto_selecionado">Bienvenido</Label></div>
                  <div class="col-xs-2">...</div>
                </div>

                 
              </body>

  
   
          <?php endwhile;?>

        </div>
      </div>
    </div>

</section>

<?php get_footer(); ?>
