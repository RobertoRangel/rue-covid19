<?php

// Tablas personalizadas y otras funciones
require get_template_directory() . '/inc/database.php';

// Funciones para las reservaciones

require get_template_directory() . '/inc/reservaciones.php';
require get_template_directory() . '/inc/registrar_pids.php';
require get_template_directory() . '/inc/registrar_covid.php';

require get_template_directory() . '/inc/registrar_resultado_inversion.php';

require get_template_directory() . '/inc/web_service.php';
require get_template_directory() . '/inc/actualizarevision.php';
require get_template_directory() . '/inc/registrar_ind_estrategicos.php';
require get_template_directory() . '/inc/registrar_avance_programatico.php';
require get_template_directory() . '/inc/registrar_accion_sin_indicador.php';
require get_template_directory() . '/inc/registrar_acciones_concurrentes.php';
require get_template_directory() . '/inc/busqueda_municipios.php';




// Logo personalizado en la página de login

    wp_register_style('google_fonts', 'https://fonts.googleapis.com/css?family=Open+Sans|Raleway:400,700,900', array(), '1.0.0');
    
    wp_register_style('fontawesome', get_template_directory_uri() . '/css/font-awesome.min.css', array('normalize'), '4.7.0' );
    wp_register_style('fluidboxcss', get_template_directory_uri() . '/css/fluidbox.min.css', array('normalize'), '4.7.0' );
    wp_register_style('style', get_template_directory_uri() . '/style.css', array('normalize'), '1.0' );
    wp_register_style('uploadfilecss','/upload_files_plugin/css/uploadfile.css', array(), '1.0' );
    wp_register_style('jqueryUI','/jqueryUI/jquery-ui.css', array(), 'uploadfilecss' );
    wp_register_style('oscar_evil_css',get_template_directory_uri() . '/css/oscar_evidencias.css', array(), 'jqueryUI' );
    wp_register_style('bootstrap','https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', array(), 'oscar_evil_css' );
    wp_register_style('gridboostratp','/dist/jquery.bootgrid.css', array(),'bootstrap' );
    wp_register_style('menucss',get_template_directory_uri() . '/css/demo.css', array(),'gridboostratp' );
    wp_register_style('stylemenu',get_template_directory_uri() . '/css/style10.css', array(),'menucss' );


     wp_register_style('app',get_template_directory_uri() . '/css/app.css', array(),'stylemenu' );
    wp_register_style('application',get_template_directory_uri() . '/css/application.css', array(),'app' );
    wp_register_style('coumicacion',get_template_directory_uri() . '/css/comunicacionSocial.css', array(),'application' );


    wp_enqueue_style('fontawesome');
    wp_enqueue_style('fluidboxcss');
    wp_enqueue_style('jqueryUI');
    wp_enqueue_style('style');
    wp_enqueue_style('uploadfilecss');
    wp_enqueue_style('oscar_evil_css');
    wp_enqueue_style('bootstrap');
    wp_enqueue_style('gridboostratp');
    wp_enqueue_style('menucss');
    wp_enqueue_style('stylemenu');

     // wp_enqueue_style('app');
  //  wp_enqueue_style('application');
   // wp_enqueue_style('coumicacion');



    wp_register_script('jquerymin', 'http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', array(), '', true  );
    wp_register_script('uploadfilejs', '/upload_files_plugin/js/jquery.uploadfile.js', array('jquerymin'), '4.0.11', true  );
    wp_register_script('jquery_UI_js', '/jqueryUI/jquery-ui.js', array('uploadfilejs'), '1.12.1', true  );
    wp_register_script('bootstrap_bar', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery_UI_js'), '3.3.7', true  );

     wp_register_script('gridboostrap', '/dist/jquery.bootgrid.min.js', array('bootstrap_bar'), '3.3.7', true  );


    //wp_enqueue_script('jquerymin');
    wp_enqueue_script('uploadfilejs');
    wp_enqueue_script('jquery_UI_js');
    wp_enqueue_script('bootstrap_bar');
    wp_enqueue_script('gridboostrap');


    /*


   
<script src="dist/jquery-1.11.1.min.js"></script>
<script src="dist/bootstrap.min.js"></script>
<script src="dist/jquery.bootgrid.min.js"></script>    



<link href="dist/jquery.bootgrid.css" rel="stylesheet" />
<script src="dist/jquery-1.11.1.min.js"></script>
<script src="dist/bootstrap.min.js"></script>
<script src="dist/jquery.bootgrid.min.js"></script>   



    wp_register_script('jquerymin', 'http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', array(), '', true  );
    wp_register_script('uploadfilejs', '/upload_files_plugin/js/jquery.uploadfile.js', array('jquerymin'), '4.0.11', true  );
    wp_register_script('jquery_UI_js', '/jqueryUI/jquery-ui.js', array('uploadfilejs'), '1.12.1', true  );
    wp_register_script('google_apis_js', 'http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', array('uploadfilejs'), '1.9.1', true  );
    wp_register_script('jquery_code_js', 'https://code.jquery.com/jquery-1.12.4.js', array('google_apis_js'), '1.12.1', true  );
    wp_register_script('jqueryUI', 'https://code.jquery.com/jquery-1.12.4.js', array('jquery_code_js'), '1.12.4', true  );

    wp_enqueue_script('jquerymin');
    wp_enqueue_script('uploadfilejs');
    wp_enqueue_script('jquery_UI_js');
     wp_enqueue_script('google_apis_js');
    wp_enqueue_script('jquery_code_js');
    wp_enqueue_script('jqueryUI');


    */

function custom_login_logo() {
        echo '<style type="text/css">
        h1 a { background-image: url('.get_bloginfo('template_directory').'/images/escudo-wp.png) !important; }
        </style>';
}
add_action('login_head', 'custom_login_logo');

function admin_favicon() {
    echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_bloginfo('template_directory').'/favicon.ico"" />';
}
add_action('admin_head', 'admin_favicon');

// Cambiar el pie de pagina del panel de Administración
function change_footer_admin() {  
    echo '&copy;2015 Copyright Gobierno del Estado de Hidalgo. Todos los derechos reservados - Web creada por <a href="#">Unidad de Innovación Gubernamental y Mejora Regulatoria</a>';  
}  
add_filter('admin_footer_text', 'change_footer_admin');

// Borrar opciones de admin
add_action('admin_menu', 'my_remove_menu_pages');
 
    function my_remove_menu_pages() {
            remove_menu_page('edit-comments.php'); // Comentarios
    }

//* Desactiva los comentarios de los archivos adjuntos en WordPress
function filtro_cometarios_medios( $open, $post_id ) {
    $post = get_post( $post_id );
    if( $post->post_type == 'attachment' ) {
        return false;
    }
    return $open;
}
add_filter( 'comments_open', 'filtro_cometarios_medios', 10 , 2 );

// Thumbnails
add_theme_support( 'post-thumbnails' );
add_image_size( 'interes-thumb', 45, 45, true );

// Menu
register_nav_menus( array(
'menu-top' => 'Menu superior',
));

// Función para definir el tamaño de excerpt
function new_excerpt_more( $more ) {
    return ' <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __('Leer más', 'your-text-domain') . '</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );

// widget
register_sidebar(array(
 'name' => 'Buscador',
 'before_widget' => '',
 'after_widget' => '',
 'before_title' => '<h3>',
 'after_title' => '</h3>',
 ));
register_sidebar(array(
 'name' => 'Dirección Footer',
 'before_widget' => '',
 'after_widget' => '',
 'before_title' => '',
 'after_title' => '',
 ));
register_sidebar(array(
 'name' => 'Redes Sociales Footer',
 'before_widget' => '',
 'after_widget' => '',
 'before_title' => '',
 'after_title' => '',
 ));
// Thumbnails
add_theme_support( 'post-thumbnails' );
add_image_size( 'servicios-thumb', 85, 85, true );
add_image_size( 'noticias-thumb', 80, 80, true );
add_image_size( 'conoce-thumb', 35, 35, true );
add_image_size( 'catnoticias-thumb', 500, 350, true );
add_image_size( 'categoria-thumb', 230, 220, true );


//Shortodes en widgets de texto
add_filter('widget_text', 'do_shortcode');

// Insertar Breadcrumb    
function the_breadcrumb() {
    if (!is_home()) {
        echo '<span class="removed_link" title="&#039;;
        echo get_option(&#039;home&#039;);
            echo &#039;">';
        bloginfo('name');
        echo "</span> » ";
        if (is_category() || is_single()) {
            the_category('title_li=');
            if (is_single()) {
                echo " » ";
                the_title();
            }
        } elseif (is_page()) {
            echo the_title();
        }
    }
}    
// fin breadcrumb
              
// Cambiamos el query con pre_get_posts
add_action( 'pre_get_posts', 'nuevo_query_tax' );


/**
 * Header cleanup
 */
function theme_cleanup() {
    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'feed_links', 2);
    remove_action('wp_head', 'feed_links_extra', 3);
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
    remove_action('wp_head', 'index_rel_link');
    remove_action('wp_head', 'parent_post_rel_link', 10, 0);
    remove_action('wp_head', 'start_post_rel_link', 10, 0);
    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
}
add_action('after_setup_theme', 'theme_cleanup', 16);

/**
 * Removes WordPress version from scripts
 */
function theme_remove_version_code($src) {
    if (strpos($src, 'ver=') !== false) {
        $src = remove_query_arg('ver', $src);
    }
    return $src;
}
add_filter('style_loader_src', 'theme_remove_version_code', 99);
add_filter('script_loader_src', 'theme_remove_version_code', 99);

/**
 * Removes WordPress version from RSS
 */
function theme_rss_version() {
    return '';
}
add_filter('the_generator', 'theme_rss_version');

/**
 * Removes injected CSS from gallery
 */
function theme_gallery_style($css) {
    return preg_replace("!!s", '', $css);
}
add_filter('gallery_style', 'theme_gallery_style');

/**
 * Removes injected CSS from recent comments widget
 */
function theme_remove_wp_widget_recent_comments_style() {
    if (has_filter('wp_head', 'wp_widget_recent_comments_style')) {
        remove_filter('wp_head', 'wp_widget_recent_comments_style');
    }
}
add_filter('wp_head', 'theme_remove_wp_widget_recent_comments_style', 1);



/*Agenda estrategica ETH
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_59c97d95cc921',
	'title' => 'Perfil',
	'fields' => array (
		array (
			'key' => 'field_59c55c8619391',
			'label' => 'Introducción',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_59c55c9d19392',
			'label' => 'Dependencia',
			'name' => 'dependencia',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Oficialía Mayor',
				2 => 'Procuraduría General de Justicia del Estado',
				3 => 'Secretaría de Contraloría',
				4 => 'Secretaría de Cultura',
				5 => 'Secretaría de Desarrollo Agropecuario',
				6 => 'Secretaría de Desarrollo Económico',
				7 => 'Secretaría de Desarrollo Social',
				8 => 'Secretaría de Educación Pública',
				9 => 'Secretaría de Finanzas Públicas',
				10 => 'Secretaría de Gobierno',
				11 => 'Secretaría de Movilidad y Transporte',
				12 => 'Secretaría de Obras Públicas',
				13 => 'Secretaría de Salud',
				14 => 'Secretaría de Seguridad Pública',
				15 => 'Secretaría de Turismo',
				16 => 'Secretaría del Trabajo y Previsión Social',
				17 => 'Secretaría Ejecutiva de Política Pública Estatal',
				18 => 'Secretaría de Medio Ambiente y Recursos Naturales',
				19 => 'Sistema para el Desarrollo Integral de la Familia del Estado de Hidalgo',
				20 => 'Unidad de Planeación y Prospectiva',
				21 => 'Despacho del Gobernador',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c59ef0886b6',
			'label' => 'Nombre del evento',
			'name' => 'nombre_ev',
			'type' => 'text',
			'instructions' => 'Anota el nombre del evento',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => 100,
		),
		array (
			'key' => 'field_59c59f1e76751',
			'label' => 'Alineación del evento',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_59c5a52f8b3c7',
			'label' => 'Indicador táctico asociado',
			'name' => 'ind_as',
			'type' => 'select',
			'instructions' => 'Selecciona el indicador táctico asociado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Porcentaje de acciones que innovan los procesos, recursos humanos y técnicas que aplica la Oficialía Mayor a las dependencias.',
				'0.9' => 'Porcentaje de acciones que promuevan la correcta administración de los recursos materiales, bienes muebles e inmuebles disponibles',
				'0.8' => 'Porcentaje de procedimientos realizados con apego a la normatividad de manera oportuna y con la calidad esperada',
				'0.7' => 'Porcentaje de acciones para la incorporación, acceso a la seguridad social, capacitación, profesionalización, desarrollo del capital humano para la prestación adecuada del servicio público y la desincorporación.',
				'0.6' => 'Porcentaje de acciones que integran las políticas de gobierno en favor de las trabajadoras de Gobierno del Estado de Hidalgo.',
				'0.5' => 'Porcentaje de Sistemas Integrales de Archivo operando adecuadamente',
				'0.4' => 'Porcentaje de acciones de disciplina, racionalidad y eficiencia del gasto público en la administración de los recursos del gobierno del Estado.',
				'0.3' => 'Porcentaje de acciones que integran las políticas de gobierno en favor de	hijas e hijos de los trabajadores	de Gobierno del Estado de Hidalgo.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c5aeb313645',
			'label' => 'Indicador táctico asociado',
			'name' => 'ind_as_a',
			'type' => 'select',
			'instructions' => 'Selecciona el indicador táctico asociado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '2',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Porcentaje de casos penales	atendidos por delitos de alto impacto y prioritarios',
				'0.95' => 'Porcentaje de casos penales determinados en el sistema de justicia penal acusatorio y adversarial',
				'0.9' => 'Porcentaje de atención a intervenciones periciales sustentadas con apoyos de la ciencia y tecnología',
				'0.85' => 'Porcentaje de respuesta a las activaciones de Alerta AMBER',
				'0.8' => 'Porcentaje de respuesta a las recomendaciones y peticiones realizadas a la Procuraduría General de Justicia',
				'0.75' => 'Porcentaje de atención de casos penales por delitos de homicidio doloso y feminicidio cometidos en agravio de mujeres',
				'0.7' => 'Porcentaje de abatimiento de averiguaciones previas en el sistema tradicional',
				'0.65' => 'Porcentaje de mecanismos alternativos de solución de controversias concluídos',
				'0.6' => 'Porcentaje de atención ciudadana con calificación buena o excelente',
				'0.5' => 'Porcentaje de percepción de confianza ciudadana en la Procuración de Justicia',
				'0.4' => 'Porcentaje de cumplimiento de órdenes de aprehensión',
				'0.3' => 'Porcentaje de atención de casos penales por delitos de corrupción',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c5b61592fd6',
			'label' => 'Indicador táctico asociado',
			'name' => 'ind_as_b',
			'type' => 'select',
			'instructions' => 'Selecciona el indicador táctico asociado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '3',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Porcentaje de cumplimiento de las obligaciones que en materia de Transparencia tiene el Poder Ejecutivo del Estado de Hidalgo.',
				'0.9' => 'Porcentaje de trámites y servicios públicos atendidos según estándares de eficacia',
				'0.85' => 'Porcentaje de Cumplimiento de los Compromisos Estatales formalizados en programas de trabajo en materia de Contraloría Social',
				'0.8' => 'Porcentaje de Cumplimiento en la Emisión de Información Programática, Presupuestal y Financiera',
				'0.7' => 'Porcentaje de factores de riesgo detectados en las acciones de supervisión, evaluación, control interno y vigilancia.',
				'0.6' => 'Porcentaje de revisiones a los procesos de igualdad de oportunidades para la obtención de puestos de toma de decisiones en el Gobierno del Estado de Hidalgo',
				'0.5' => 'Porcentaje de Irregularidades detectadas en Obras Públicas supervisadas',
				'0.45' => 'Porcentaje de Auditorías realizadas con carácter de representatividad.',
				'0.4' => 'Porcentaje de Trámites y Servicios que ofrece la Secretaría de Contraloría en línea.',
				'0.3' => 'Porcentaje de difusión de los derechos de niñas, niños y adolescentes a través del Programa Infantil de Valores y Cultura de la Legalidad.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c5b6643a1c2',
			'label' => 'Indicador táctico asociado',
			'name' => 'ind_as_c',
			'type' => 'select',
			'instructions' => 'Selecciona el indicador táctico asociado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '4',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Porcentaje de estímulos para la creación, producción, formación y difusión artística y cultural.',
				'0.97' => 'Porcentaje de apoyos a creadores y portadores de la cultura popular e indígena.',
				'0.94' => 'Cobertura municipal de servicios descentralizados de educación artística.',
				'0.91' => 'Porcentaje de patrimonio documental digitalizado.',
				'0.88' => 'Porcentaje de ampliación de canales de comunicación digital para la oferta cultural del Estado.',
				'0.85' => 'Porcentaje de crecimiento de alumnos del sistema de educación artística.',
				'0.82' => 'Porcentaje de acciones para crear, equipar, mantener, restaurar, rehabilitar y remodelar la infraestructura cultural y de servicios.',
				'0.79' => 'Porcentaje de Investigaciones de alto nivel académico realizadas en el Centro de Ivestigaciones Históricas y del Patrimonio Cultural',
				'0.76' => 'Porcentaje de patrimonio tangible e intangible intervenido.',
				'0.73' => 'Porcentaje de espacios culturales con modelo de sustentabilidad implementado.',
				'0.7' => 'Porcentaje de festivales artísticos y culturales apoyados',
				'0.67' => 'Porcentaje de municipios certificados en el Programa de Ciudades Culturales y Creativas.',
				'0.64' => 'Número de eventos de gastronomía tradicional apoyados.',
				'0.61' => 'Porcentaje de ramas artesanales intervenidas con acciones de innovación',
				'0.58' => 'Porcentaje de bibliotecas automatizadas y en red.',
				'0.55' => 'Porcentaje de avance del Catálogo del Patrimonio Cultural del Estado de Hidalgo.',
				'0.52' => 'Porcentaje de incremento de emprendedores culturales, profesionalizados y certificados.',
				'0.49' => 'Porcentaje de asistencia a eventos artísticos y culturales.',
				'0.46' => 'Porcentaje de eventos institucionales dirigidos a la disminución de las desigualdades.',
				'0.43' => 'Porcentaje de incremento en los servicios otorgados en la red estatal de bibliotecas públicas.',
				'0.4' => 'Porcentaje de incremento de hogares que consume contenidos culturales en los medios de comunicación digital.',
				'0.3' => 'Porcentaje de eventos dirigidos a niñas, niños y adolescentes',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c5b8264d01f',
			'label' => 'Indicador táctico asociado',
			'name' => 'ind_as_d',
			'type' => 'select',
			'instructions' => 'Selecciona el indicador táctico asociado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '5',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Porcentaje de productores con créditos obtenidos a través de la gestión de la Secretaría de Desarrollo Agropecuario',
				'0.95' => 'Porcentaje de árboles y especies forestales no maderables producidos en viveros',
				'0.9' => 'Porcentaje de incremento de la producción acuícola de crías de peces del estado.',
				'0.85 :Incremento en el porcentaje de productores con algún apoyo recibido.' => '0.85 :Incremento en el porcentaje de productores con algún apoyo recibido.',
				'0.8' => 'Porcentaje de hectáreas mecanizadas.',
				'0.75' => 'Porcentaje de vientres ovinos y bovinos inseminados a través del programa de mejoramiento genético.',
				'0.7' => 'Porcentaje de niñas, niños y adolescentes integrantes de familias que son apoyadas a través del Proyecto Estratégico para la Seguridad Alimentaria (PESA) para zonas rurales',
				'0.65' => 'Porcentaje de hectáreas con nuevos cultivos agrícolas de mayor valor de mercado.',
				'0.6' => 'Porcentaje de variación del volumen físico de la producción de café.',
				'0.55' => 'Porcentaje de avance en las fases de operación de los Centros de Desarrollo Tecnológico de la Secretaría de Desarrollo Agropecuario',
				'0.5' => 'Porcentaje de crecimiento en asesorías que se otorgan a productores en servicios de asistencia técnica, capacitación y extensionismo',
				'0.45' => 'Porcentaje de variación del volumen físico de la producción agropecuaria y pesquera.',
				'0.4' => 'Porcentaje de variación del valor de la producción agropecuaria y pesquera',
				'0.3' => 'Porcentaje de programas con criterios para otorgar apoyos en igualdad de oportunidades para mujeres y hombres.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c5b941630eb',
			'label' => 'Indicador táctico asociado',
			'name' => 'ind_as_e',
			'type' => 'select',
			'instructions' => 'Selecciona el indicador táctico asociado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '6',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Creación de nuevos empleos formales',
				'0.96' => 'Incremento de proyectos financiados a emprendedores, micro, pequeñas y medianas empresas.',
				'0.92' => 'Crecimiento de la captación de inversión privada nacional y extranjera',
				'0.88' => 'Porcentaje de becas entregadas por el programa "Mi Primer Empleo".',
				'0.84' => 'Superficie acumulada en hectáreas con inversiones asentadas en parques industriales.',
				'0.8' => 'Porcentaje de avance en la implementación de Ventanillas de Atención Empresarial (VAE), con base en la metodología del sistema de apertura rápida de empresas.',
				'0.76' => 'Superficie acumulada en hectáreas habilitada con infraestructura para el desarrollo industrial y proyectos estratégicos.',
				'0.72' => 'Proyectos concretados con la vinculación de una actividad industrial con otra.',
				'0.68' => 'Porcentaje de avance en la implementación de un ambiente de negocios para el Estado de Hidalgo.',
				'0.64' => 'Porcentaje de avance en el conjunto de acciones para incrementar la capacidad instalada de transformación en subestaciones eléctricas de distribución y potencia del Estado.',
				'0.6' => 'Incremento de acciones estratégicas de fomento para incrementar la competitividad empresarial.',
				'0.56' => 'Nivel de eficacia en el acompañamiento y políticas de estímulos para la captación de proyectos de inversión.',
				'0.52' => 'Incremento de estaciones proveedoras de combustible, instaladas.',
				'0.48' => 'Porcentaje de procesos sistematizados con apoyo de tecnologías de la información en las actividades para el impulso del desarrollo económico.',
				'0.44' => 'Porcentaje de acciones de difusión y sensibilización, en las empresas sobre los derechos de las niñas, niños y adolescentes.',
				'0.3' => 'Porcentaje de programas de la Secretaría de Desarrollo Económico que incluyen acciones orientadas a disminuir las desigualdades entre hombres y mujeres.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c5b9e900d77',
			'label' => 'Indicador táctico asociado',
			'name' => 'ind_as_f',
			'type' => 'select',
			'instructions' => 'Selecciona el indicador táctico asociado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '7',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Porcentaje de adultos mayores de 60 años y más, apoyados por el Programa para la Atención de las Personas Adultas Mayores con acciones gerontológicas.',
				'0.93' => 'Porcentaje de artesanos atendidos con apoyos gubernamentales para capacitación, producción y comercialización de sus artesanías',
				'0.86' => 'Porcentaje de incremento en la atención de migrantes y sus familias',
				'0.79' => 'Porcentaje de personas que viven con carencia alimentaria en los municipios de alta y muy alta marginación del Estado de Hidalgo, atendidas.',
				'0.72' => 'Porcentaje de personas que viven con carencia por calidad y espacios en la vivienda en el Estado de Hidalgo atendidos.',
				'0.65' => 'Porcentaje de diferencia entre hombres y mujeres que reciben apoyos de los programas y servicios que otorga la Secretaría de Desarrollo Social.',
				'0.58' => 'Porcentaje de avance en la implementación del sistema de información de los programas sociales.',
				'0.51' => 'Porcentaje de población de 12 a 29 años por municipio, con participación integral en el programa de Atención a la Juventud',
				'0.44' => 'Porcentaje de acciones para promover la seguridad de niñas, niños y adolescentes a través de los programas que ofrece la Secretaría de Desarrollo Social.',
				'0.3' => 'Porcentaje de formación de líderes sociales (agentes de cambio) en el Estado.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c5ba839c602',
			'label' => 'Indicador táctico asociado',
			'name' => 'ind_as_g',
			'type' => 'select',
			'instructions' => 'Selecciona el indicador táctico asociado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '8',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Porcentaje de estudiantes de educación Media Superior que incrementan el dominio en lenguaje y comunicación.',
				'0.97' => 'Porcentaje de Planteles de Educación Media Superior que se encuentran en el Padrón de Buena Calidad del Sistema Nacional de Educación Media Superior (PBC-SiNEMS).',
				'0.94' => 'Porcentaje de estudiantes de educación básica que incrementan el dominio en matemáticas.',
				'0.91' => 'Porcentaje de Cobertura en Educación Superior.',
				'0.88' => 'Porcentaje de Planteles Tecnológicos de Educación Media Superior con opción educativa de formación Dual.',
				'0.85' => 'Porcentaje de avance en la Instalación y funcionamiento de la Universidad Digital del Estado de Hidalgo UNIDEH',
				'0.82' => 'Porcentaje de escuelas públicas de educación básica con enfoque intercultural.',
				'0.79' => 'Porcentaje de especialidades, maestrías y doctorados con reconocimiento en el Programa Nacional de Posgrados de Calidad',
				'0.76' => 'Promedio de proyectos de investigación realizados por Instituciones Públicas de Educación Superior en el Estado de Hidalgo.',
				'0.73' => 'Porcentaje de docentes que obtienen resultados aprobatorios en la evaluación del desempeño',
				'0.7' => 'Porcentaje de abandono Escolar en Educación Secundaria.',
				'0.67' => 'Cobertura en Educación Preescolar.',
				'0.64' => 'Porcentaje de escuelas que desarrollan acciones para mejorar la convivencia escolar en la entidad.',
				'0.61' => 'Porcentaje de escuelas que desarrollan acciones orientadas a disminuir las brechas de desigualdad entre hombres y mujeres.',
				'0.58' => 'Porcentaje de Absorción en Educación Superior.',
				'0.55' => 'Porcentaje de estudiantes de educación básica que incrementan el dominio en lenguaje y comunicación.',
				'0.52' => 'Porcentaje de prácticas educativas innovadoras en el campo de las matemáticas, lenguaje y comunicación aplicados a la práctica docente.',
				'0.49' => 'Porcentaje de profesores de tiempo completo que participan en los Cuerpos Académicos',
				'0.46' => 'Porcentaje de alumnos de educación básica impactados con acciones para disminuir el sedentarismo',
				'0.43' => 'Porcentaje de abandono en Educación Media Superior.',
				'0.4' => 'Porcentaje de estudiantes de educación Media Superior que incrementan el dominio en matemáticas.',
				'0.3' => 'Porcentaje de aprovechamiento de espacios públicos para la activación física y el deporte.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c5bd1900e3e',
			'label' => 'Indicador táctico asociado',
			'name' => 'ind_as_h',
			'type' => 'select',
			'instructions' => 'Selecciona el indicador táctico asociado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '9',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Porcentaje de la Deuda Pública Directa respecto al Producto Interno Bruto Estatal.',
				'0.94' => 'Porcentaje del Gasto de Operación	respecto al Presupuesto de Egresos Ejercido',
				'0.88' => 'Porcentaje de cumplimiento de acciones necesarias para la consolidación del Presupuesto Basado en Resultados y el Sistema de Evaluación del Desempeño en el Estado de Hidalgo.',
				'0.82' => 'Porcentaje de avance de los programas de trabajo para mantener un buen perfil crediticio de acuerdo con las calificadoras',
				'0.76' => 'Porcentaje de los ingresos propios respecto al total de ingresos del Estado',
				'0.7' => 'Porcentaje de los subsidios y convenios obtenidos respecto al total del gasto programable.',
				'0.64' => 'Porcentaje de participaciones respecto a la Recaudación Federal Participable (RFP).',
				'0.58' => 'Porcentaje de la Inversión respecto al total del gasto programable.',
				'0.52' => 'Porcentaje de los recursos destinados al gasto en ciencia y tecnología respecto al total del Presupuesto de Egresos del Estado.',
				'0.46' => 'Porcentaje de los recursos destinados a la atención de programas con perspectiva de género respecto al total del Presupuesto de Egresos del Estado.',
				'0.3' => 'Porcentaje de los recursos destinados a la atención de infantes y adolescentes respecto al total del Presupuesto de Egresos del Estado.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c5bfa808786',
			'label' => 'Indicador táctico asociado',
			'name' => 'ind_as_i',
			'type' => 'select',
			'instructions' => 'Selecciona el indicador táctico asociado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '10',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Porcentaje de funcionarios de las administraciones municipales que dan cumplimiento a la obligación de certificar sus competencias laborales.',
				'0.97' => 'Porcentaje de satisfacción promedio que las mujeres otorgan a los servicios recibidos en el Centro de Justicia para Mujeres del Estado de Hidalgo.',
				'0.93' => 'Porcentaje de avance en el cumplimiento del Programa Estatal de Modernización	y Vinculación Registral',
				'0.9' => 'Porcentaje de disminución en el tiempo promedio de inscripción de las escrituras públicas ante el Instituto de la Función Registral del Estado de Hidalgo.',
				'0.86' => 'Porcentaje de peticiones relativas a problemáticas públicas con respuesta generada.',
				'0.83' => 'Porcentaje de matrices de riesgo elaboradas',
				'0.79' => 'Porcentaje de municipios que cuentan con la verificación y la definición de acciones por parte del Instituto Hidalguense para el Desarrollo Municipal.',
				'0.76' => 'Porcentaje de cobertura en registros de nacimiento en municipios con mayor rezago en el registro oportuno.',
				'0.72' => 'Porcentaje de municipios con Banco de Proyectos de Infraestructura Municipal elaborado.',
				'0.69' => 'Porcentaje de acciones concretadas del programa de trabajo del Secretariado Ejecutivo del Consejo Estatal de Seguridad Pública',
				'0.65' => 'Porcentaje de Notarías inspeccionadas con acta circunstanciada y dictamen aplicado.',
				'0.62' => 'Porcentaje de incremento de servicios en línea, de la Secretaría de Gobierno.',
				'0.58' => 'Porcentaje de avance en las etapas y acciones para la implementación del modelo de Ciudad de Mujeres en Hidalgo',
				'0.55' => 'Porcentaje de avance en la elaboración del directorio de traductores	de lenguas indígenas certificados',
				'0.51' => 'Porcentaje de acuerdos de la Comisión Interinstitucional cumplidos.',
				'0.48' => 'Porcentaje de trámites atendidos por la Coordinación General Jurídica, y las áreas e institutos que la integran.',
				'0.44' => 'Porcentaje de programas con acciones orientadas a disminuir las desigualdades entre hombres y mujeres en la Secretaría de Gobierno.',
				'0.40' => 'Porcentaje de acuerdos concretados del Sistema de Protección Integral de los Derechos de Niñas, Niños y Adolescentes',
				'0.37' => 'Proporción de Unidades Institucionales para la Igualdad entre Mujeres y Hombres con programa de trabajo establecido',
				'0.3' => 'Porcentaje de avance en el programa de trabajo en materia de política de población.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c5c0fb7d7f8',
			'label' => 'Indicador táctico asociado',
			'name' => 'ind_as_j',
			'type' => 'select',
			'instructions' => 'Selecciona el indicador táctico asociado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '11',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Porcentaje de vehículos de transporte público convencional con inspección vehicular aprobada.',
				'0.93' => 'Tiempo de espera promedio para acceder al servicio del Sistema Integrado de Transporte Masivo.',
				'0.86' => 'Porcentaje de satisfacción con el servicio del sistema integrado de transporte masivo',
				'0.79' => 'Porcentaje de uso de bicicletas como modo de transporte público no motorizado (Alternativo).',
				'0.72' => 'Porcentaje de accesibilidad urbana',
				'0.65' => 'Porcentaje de Intermovilidad metropolitana',
				'0.58' => 'Porcentaje de acciones para promover la seguridad de niñas, niños y adolescentes en los diferentes modos de transporte público del estado de Hidalgo.',
				'0.51' => 'Porcentaje de	acciones para promover la “Seguridad y No Violencia a la Mujer” en los diferentes modos de transporte público de pasajeros del estado de Hidalgo',
				'0.3' => 'Porcentaje de innovación tecnológica en materia de movilidad y transporte en el estado de Hidalgo.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c5c1a7b4e07',
			'label' => 'Indicador táctico asociado',
			'name' => 'ind_as_k',
			'type' => 'select',
			'instructions' => 'Selecciona el indicador táctico asociado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '12',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Porcentaje de Cobertura del Servicio de Agua Potable en el Estado.',
				'0.94' => 'Porcentaje de Cobertura del Servicio de Alcantarillado Sanitario en el Estado.',
				'0.88' => 'Porcentaje de kilómetros de carreteras estatales ampliadas y/o modernizadas.',
				'0.83' => 'Porcentaje de aguas residuales tratadas.',
				'0.77' => 'Porcentaje de kilómetros de carreteras del Estado en regulares y malas condiciones.',
				'0.71' => 'Porcentaje de Instrumentos de Planeación Urbana y Ordenamiento Territorial actualizados y decretados.',
				'0.65' => 'Porcentaje de Hogares que reciben subsidio para la adquisición de vivienda nueva.',
				'0.59' => 'Porcentaje de infraestructura construida con elementos de sostenibilidad (resiliente) en el Estado de Hidalgo.',
				'0.54' => 'Porcentaje de Instrumentos Normativos actualizados en materia de Ordenamiento Territorial y Desarrollo Urbano.',
				'0.48' => 'Porcentaje de procesos sistematizados para la generación y otorgamiento de productos y servicios, responsabilidad de la Secretaría de Obras Públicas y Ordenamiento Territorial.',
				'0.42' => 'Porcentaje de niñas, niños y adolescentes atendidos por infraestructura y equipamiento para su desarrollo integral.',
				'0.3' => 'Diferencia porcentual entre hombres y mujeres que participan en los Comites para la realización de Obra Pública en el estado de Hidalgo',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c5c2b11fe59',
			'label' => 'Indicador táctico asociado',
			'name' => 'ind_as_m',
			'type' => 'select',
			'instructions' => 'Selecciona el indicador táctico asociado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '14',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Porcentaje de personal profesionalizado o capacitado de las instituciones policiales estatal y municipal.',
				'0.93' => 'Avance en la implementación del Sistema Integral de video vigilancia del Estado de Hidalgo',
				'0.86' => 'Porcentaje de la población que ha participado en los programas de Prevención social de la violencia y la delincuencia.',
				'0.79' => 'Porcentaje de quejas y denuncias a integrantes de las instituciones policiales del Estado, dictaminadas.',
				'0.72' => 'Tiempo promedio de respuesta de las llamadas de emergencia recibidas y atendidas por las corporaciones.',
				'0.65' => 'Porcentaje de acciones para prevenir y combatir el delito en beneficio de la población del estado de Hidalgo.',
				'0.58' => 'Porcentaje de acciones concretadas para disminuir las brechas de desigualdad entre hombres y mujeres, y eliminar la violencia de género.',
				'0.51' => 'Porcentaje de acciones concretadas para disminuir la violencia y la delincuencia contra niñas, niños y adolescentes de tres años y más.',
				'0.3' => 'Porcentaje de acciones para mejorar las condiciones de habitabilidad en los Centros de Reinserción Social del Estado.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c5fa4c471ef',
			'label' => 'Indicador táctico asociado',
			'name' => 'ind_as_sal',
			'type' => 'select',
			'instructions' => 'Selecciona el indicador táctico asociado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '13',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Abasto de insumos médicos contenidos en el cuadro básico de los servicios de salud de Hidalgo.',
				'0.96' => 'Cobertura de vacunación con esquema completo en niñas y niños menores de un año de edad en el Estado.',
				'0.91' => 'Población sin derechohabiencia a la seguridad social afiliada y reafiliada al Seguro Popular.',
				'0.87' => 'Implementación de telemedicina en las unidades hospitalarias de los Servicios de Salud de Hidalgo',
				'0.83' => 'Detección oportuna e integral de enfermedades en adultos de 60 años y más.',
				'0.79' => 'Porcentaje de mujeres embarazadas con VIH que reciben tratamiento antirretroviral para reducir el riesgo de transmisión vertical',
				'0.74' => 'Usuarios satisfechos con la atención en salud otorgada a través de unidades médicas móviles.',
				'0.7' => 'Promedio de consultas en las unidades de los Servicios de Salud de Hidalgo.',
				'0.66' => 'Razón de muerte materna por causas evitables (hemorragia y sepsis obstétrica) respecto al año anterior.',
				'0.61' => 'Cobertura de atención especializada a mujeres de 15 años y más, unidas víctimas de violencia familiar severa',
				'0.57' => 'Unidades de Salud acreditadas o re acreditadas',
				'0.53' => 'Participación efectiva de las Secretarías del ejecutivo estatal en el Programa de hábitos de vida saludable (Reta Ya Bájale).',
				'0.48' => 'Monitoreo de cloración del Agua para uso y consumo humano.',
				'0.44' => 'Tasa de mortalidad neonatal por asfixia y trauma al nacimiento.',
				'0.4' => 'Inclusión del personal de salud al Programa Todos Somos Pacientes',
				'0.3' => 'Comunidades certificadas por primera vez	como saludables',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c5c4f03d3cd',
			'label' => 'Indicador táctico asociado',
			'name' => 'ind_as_n',
			'type' => 'select',
			'instructions' => 'Selecciona el indicador táctico asociado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '15',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Porcentaje de Prestadores de Servicios Turísticos del estado de Hidalgo Certificados ante el Sistema Nacional de Certificación Turística.',
				'0.9' => 'Porcentaje de Prestadores de Servicios Turísticos de hospedaje y alojamiento del estado de Hidalgo, que implementaron el “Código de Conducta Nacional para la Protección de las Niñas, Niños y Adolescentes en el Sector de los Viajes y el Turismo”.',
				'0.8' => 'Porcentaje de Incremento Anual de las Personas Impactadas Mediante Acciones de Promoción Turística para el estado de Hidalgo',
				'0.7' => 'Porcentaje de Establecimientos de Prestación de Servicios Turísticos del estado de Hidalgo con más de cinco personas ocupadas, que están inscritos en el Registro Nacional de Turismo.',
				'0.6' => 'Porcentaje de variación anual de visitantes a eventos especiales organizados por la Secretaría de Turismo en los recintos propiedad del Estado de Hidalgo.',
				'0.5' => 'Porcentaje de Prestadores de Servicios Turísticos Prioritarios Impactados con Acciones de Promoción para el Uso de Tecnologías e Innovación en la Prestación de su Servicio.',
				'0.3' => 'Proporción de Acciones para el Acceso a los Servicios Turísticos de Manera Equitativa entre Hombres y Mujeres en el Estado de Hidalgo.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c5c5e62684c',
			'label' => 'Indicador táctico asociado',
			'name' => 'ind_as_o',
			'type' => 'select',
			'instructions' => 'Selecciona el indicador táctico asociado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '16',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Porcentaje de empresas creadas como resultado de un proceso de incubación',
				'0.93' => 'Porcentaje de personas que lograron obtener un empleo a través de los programas del Servicio Nacional de Empleo Hidalgo',
				'0.86' => 'Porcentaje de conciliaciones exitosas',
				'0.79' => 'Porcentaje de alumnos que concluyeron satisfactoriamente su capacitación para el trabajo',
				'0.72' => 'Porcentaje de acciones realizadas por la Comisión Interinstitucional para la Erradicación del Trabajo Infantil y Protección de Adolescentes Trabajadores en Edad Permitida en el Estado de Hidalgo,	en beneficio de las niñas, niños y adolescentes',
				'0.65' => 'Tasa de crecimiento en la cobertura de las inspecciones en normatividad laboral',
				'0.58' => 'Porcentaje de empresas asesoradas que cumplieron con la normatividad laboral',
				'0.51' => 'Porcentaje de Participación Económica Femenina',
				'0.44' => 'Porcentaje de empresas que fueron atendidas con	capacitación y adiestramiento en temas de productividad',
				'0.3' => 'Porcentaje de demandas en trámite',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c5c67d78bed',
			'label' => 'Indicador táctico asociado',
			'name' => 'ind_as_p',
			'type' => 'select',
			'instructions' => 'Selecciona el indicador táctico asociado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '17',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Porcentaje de instrumentos validados de gestión estatal para promover la austeridad y eficacia en las políticas públicas.',
				'0.9' => 'Porcentaje de trámites y servicios digitalizados',
				'0.8' => 'Porcentaje de información relevante del gobierno comunicada a la ciudadanía',
				'0.7' => 'Porcentaje de Cobertura de la señal de Televisión de Hidalgo',
				'0.6' => 'Porcentaje de Cobertura de la señal de Radio de Hidalgo',
				'0.5' => 'Porcentaje de solicitudes dirigidas al Gobernador atendidas',
				'0.3' => 'Porcentaje de acciones para aplicar las políticas transversales y elementos de sostenibilidad en las políticas públicas del Estado de Hidalgo',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c5c6d559cf2',
			'label' => 'Indicador táctico asociado',
			'name' => 'ind_as_q',
			'type' => 'select',
			'instructions' => 'Selecciona el indicador táctico asociado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '18',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Porcentaje de avance en las etapas para la elaboración y aplicación inicial de la estrategia estatal de biodiversidad',
				'0.94' => 'Porcentaje de regulación ambiental de unidades económicas de competencia estatal',
				'0.88' => 'Porcentaje de Niñas, Niños y Adolescentes que realizan acciones en materia de cuidado del medio ambiente dentro de sus Centros Educativos',
				'0.83' => 'Porcentaje de Vehículos Verificados',
				'0.77' => 'Porcentaje de datos recabados que cumplen con la normatividad para la medición de la calidad del aire',
				'0.71' => 'Porcentaje de avance en la elaboración de fases para la Actualización del Programa de Ordenamiento Ecológico Territorial del Estado de Hidalgo, y la promoción de los ordenamientos ecológicos territoriales a nivel local',
				'0.65' => 'Porcentaje de residuos sólidos urbanos aprovechados y/o valorizados, por gestión o intervención de la Secretaría de Medio Ambiente y Recursos Naturales del Estado de Hidalgo',
				'0.59' => 'Porcentaje de cobertura de visitas de verificación ambiental',
				'0.54' => 'Porcentaje de investigaciones que promuevan el desarrollo sustentable en Hidalgo, concretadas por intervención de la Secretaría de Medio Ambiente y Recursos Naturales',
				'0.48' => 'Porcentaje de Programas y Acciones que ejecuta la Secretaría de Medio Ambiente y Recursos Naturales con beneficio para familias con jefatura femenina',
				'0.42' => 'Porcentaje de dependencias, entidades e instituciones educativas del Estado de Hidalgo, con programas proambientales construidos con apoyo de la Secretaría de Medio Ambiente y Recursos Naturales',
				'0.3' => 'Porcentaje de eficacia recaudatoria según facultades de la Secretaría de Medio Ambiente y Recursos Naturales',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c5c76a84532',
			'label' => 'Indicador táctico asociado',
			'name' => 'ind_as_r',
			'type' => 'select',
			'instructions' => 'Selecciona el indicador táctico asociado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '19',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Porcentaje de población atendida con programas que fortalecen las redes de apoyo que opera el Sistema para el Desarrollo Integral de la Familia.',
				'0.92' => 'Porcentaje de población atendida por el Sistema para el Desarrollo Integral de la Familia que presenta alguna condición de vulnerabilidad social',
				'0.85' => 'Porcentaje de población que disminuyó su carencia alimentaria con programas que otorga el Sistema para el Desarrollo Integral de la Familia.',
				'0.77' => 'Porcentaje de población atendida con programas preventivos y de intervención en materia de salud a través del Sistema para el Desarrollo Integral de la Familia del Estado de Hidalgo',
				'0.69' => 'Porcentaje de niñas, niños y adolescentes que recibieron acciones preventivas y de intervención a través de programas que opera el Sistema para el Desarrollo Integral de la Familia.',
				'0.62' => 'Porcentaje de población atendida con programas educativos formales, a través del Sistema para el Desarrollo Integral de la Familia del Estado de Hidalgo.',
				'0.54' => 'Porcentaje de programas asistenciales con tecnología incorporada en sus acciones.',
				'0.46' => 'Porcentaje de población atendida con apoyos directos e indirectos para mejorar la calidad y espacio en la vivienda, otorgados por el Sistema para el Desarrollo Integral de la Familia del Estado de Hidalgo',
				'0.3' => 'Porcentaje de programas asistenciales con perspectiva de género incorporada.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c5c7ff86308',
			'label' => 'Indicador táctico asociado',
			'name' => 'ind_as_s',
			'type' => 'select',
			'instructions' => 'Selecciona el indicador táctico asociado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '20',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Porcentaje de Productos de Información útiles para la toma de decisiones',
				'0.94' => 'Porcentaje de Instrumentos de Planeación Publicados en el Estado de Hidalgo.',
				'0.88' => 'Porcentaje de mecanismos instrumentados para el Sistema de Monitoreo y Evaluación Estatal.',
				'0.83' => 'Porcentaje de mecanismos instrumentados para impulsar el desarrollo metropolitano en el Estado de Hidalgo.',
				'0.77' => 'Porcentaje de generación de insumos para la innovación y la mejora en la normatividad de los procesos de planeación.',
				'0.71' => 'Porcentaje de instrumentos de Análisis y diagnóstico generados y promovidos para el impulso al Desarrollo Regional.',
				'0.65' => 'Índice de transversalización de la política de desarrollo y protección de los derechos de niñas, niños y adolescentes en los instrumentos de planeación',
				'0.59' => 'Porcentaje de Comités de Planeación para el Desarrollo Regional (COPLADERs) con Juntas Representativas formalmente instaladas y en operación regular',
				'0.54' => 'Porcentaje de trámites y servicios atendidos en las Unidades de Servicios Estatales (USE)',
				'0.48' => 'Porcentaje de proyectos identificados y aprobados por el Comité de Banco de Proyectos, con rentabilidad social.',
				'0.42' => 'Índice de transversalización de la política de incorporación de la ciencia, la tecnología y la innovación	en los instrumentos de planeación.',
				'0.3' => 'Índice de transversalización de la política de género en los instrumentos de planeación',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c5c9d3ea88e',
			'label' => 'Objetivo de Desarrollo Sostenible asociado al evento',
			'name' => 'ods',
			'type' => 'select',
			'instructions' => 'Selecciona el ODS vinculado al evento',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Objetivo1.Fin de la Pobreza',
				2 => 'Objetivo2.Hambre cero',
				3 => 'Objetivo3.Salud y Bienestar',
				4 => 'Objetivo4.Educación de calidad',
				5 => 'Objetivo5.Igualdad de Género',
				6 => 'Objetivo6.Agua limpia y Saneamiento',
				7 => 'Objetivo7.Energía Asequible y No Contaminante',
				8 => 'Objetivo8.Trabajo Decente y Crecimiento Económico',
				9 => 'Objetivo9.Industria, Innovación e Infraestructura',
				10 => 'Objetivo10.Reducción de las Desigualdades',
				11 => 'Objetivo11.Ciudades y comunidades sostenibles',
				12 => 'Objetivo12.Producción y consumo responsable',
				13 => 'Objetivo13.Acción por el Clima',
				14 => 'Objetivo14.Vida Submarina',
				15 => 'Objetivo15.Vida de ecosistemas terrestres',
				16 => 'Objetivo16.Paz, Justicia e Instituciones sólidas',
				17 => 'Objetivo17.Alianzas para lograr los objetivos',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c97d551597b',
			'label' => 'Compromiso del Gobernador',
			'name' => 'com_gob',
			'type' => 'select',
			'instructions' => 'Selecciona el compromiso al que esta alineado el evento',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Para darle seguridad a las familias hidalguenses: Primer empleo a egresados de Universidades Públicas; Más empresas, más empleos; Pon tu negocio, yo te apoyo',
				2 => 'Para la tranquilidad de tu familia: salud. Siempre habrá medicinas para ti; Brigadas médicas permanentes',
				3 => 'Para la educación de tus hijos, niños, niñas y adolescentes. Mejores instalaciones en tu escuela; Nuevo Sistema de Teleuniversidad para ti; Más becas para tus hijos; Internet en todas las escuelas; Universidad Virtual para ti.',
				4 => 'Para ser justos con los que menos tienen. Agua, Luz y Drenaje primero a comunidades indígenas; Más casas de día para adultos mayores; Pensión para más adultos mayores; Apoyo a quien sí lo necesita.',
				5 => 'Para fortalecer el campo. Crédito al campesino y su cosecha como garantía; Crear unidades productivas comunitarias; Más apoyo a proyectos productivos.',
				6 => 'Para combatir la inseguridad. Sistemas de Videovigilancia',
				7 => 'Para tener un gobierno moderno. Gobierno Digital; Gobierno Transparente',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c5ca9e052c0',
			'label' => 'Objetivo estratégico asociado',
			'name' => 'obj_est',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo estratégico que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '20',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => '1.1 Cero tolerancia a la corrupción',
				2 => '1.2 Impulso a la participación ciudadana',
				3 => '1.3 Gobierno digital Incrementar los servicios"',
				4 => '1.4 Mejora regulatoria',
				5 => '1.5 Desarrollo institucional de los municipios',
				6 => '1.6 Fortalecimiento hacendario',
				7 => '1.7 Administración eficiente de los recursos',
				45 => '5.1 Equidad de servicios e infraestructura sostenible',
				46 => '5.2 Cultura y formación ambiental',
				47 => '5.3 Ordenamiento territorial integral y sostenible',
				48 => '5.4 Movilidad sostenible y eficiente',
				49 => '5.5 Preservación del patrimonio natural',
				50 => '5.6 Planeación para el desarrollo territorial sostenible',
				51 => 'Estrategia transversal de perspectiva de género',
				52 => 'Estrategia transversal para el desarrollo y protección de niñas, niños y adolescentes"',
				53 => 'Estrategia transversal de incorporación de la ciencia, tecnología e innovación"',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c5cfb088ba8',
			'label' => 'Objetivo estratégico asociado',
			'name' => 'obj_est_a',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo estratégico que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '6',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				12 => '2.1 Progreso económico incluyente',
				15 => '2.2 Entorno económico dinámico e innovador',
				16 => '2.3 Articulación y consolidación de los sectores productivos',
				17 => '2.4 Turismo, palanca de desarrollo',
				18 => '2.5 Campo moderno y productivo',
				45 => '5.1 Equidad de servicios e infraestructura sostenible',
				46 => '5.2 Cultura y formación ambiental',
				47 => '5.3 Ordenamiento territorial integral y sostenible',
				48 => '5.4 Movilidad sostenible y eficiente',
				49 => '5.5 Preservación del patrimonio natural',
				50 => '5.6 Planeación para el desarrollo territorial sostenible',
				51 => 'Estrategia transversal de perspectiva de género',
				52 => 'Estrategia transversal para el desarrollo y protección de niñas, niños y adolescentes"',
				53 => 'Estrategia transversal de incorporación de la ciencia, tecnología e innovación"',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c5d072f77e2',
			'label' => 'Objetivo estratégico asociado',
			'name' => 'obj_est_b',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo estratégico que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '4',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				24 => '3.1 Desarrollo social, integral y solidario',
				25 => '3.2 Educación de relevancia y equipada',
				27 => '3.3 Salud con calidad y calidez',
				29 => '3.4 Cultura física y deporte',
				30 => '3.5 Arte y cultura',
				32 => 'Estrategia transversal de perspectiva de género',
				33 => 'Estrategia transversal para el desarrollo y protección de niñas, niños y adolescentes"',
				34 => 'Estrategia transversal de incorporación de la ciencia, tecnología e innovación"',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c6023250478',
			'label' => 'Objetivo estratégico asociado',
			'name' => 'obj_est_c',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo estratégico que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '7',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				24 => '3.1 Desarrollo social, integral y solidario',
				25 => '3.2 Educación de relevancia y equipada',
				27 => '3.3 Salud con calidad y calidez',
				29 => '3.4 Cultura física y deporte',
				30 => '3.5 Arte y cultura',
				32 => 'Estrategia transversal de perspectiva de género',
				33 => 'Estrategia transversal para el desarrollo y protección de niñas, niños y adolescentes"',
				34 => 'Estrategia transversal de incorporación de la ciencia, tecnología e innovación"',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c6027e37294',
			'label' => 'Objetivo estratégico asociado',
			'name' => 'obj_est_d',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo estratégico que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '13',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				24 => '3.1 Desarrollo social, integral y solidario',
				25 => '3.2 Educación de relevancia y equipada',
				27 => '3.3 Salud con calidad y calidez',
				29 => '3.4 Cultura física y deporte',
				30 => '3.5 Arte y cultura',
				32 => 'Estrategia transversal de perspectiva de género',
				33 => 'Estrategia transversal para el desarrollo y protección de niñas, niños y adolescentes"',
				34 => 'Estrategia transversal de incorporación de la ciencia, tecnología e innovación"',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c602b7e2a17',
			'label' => 'Objetivo estratégico asociado',
			'name' => 'obj_est_e',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo estratégico que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '8',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				24 => '3.1 Desarrollo social, integral y solidario',
				25 => '3.2 Educación de relevancia y equipada',
				27 => '3.3 Salud con calidad y calidez',
				29 => '3.4 Cultura física y deporte',
				30 => '3.5 Arte y cultura',
				32 => 'Estrategia transversal de perspectiva de género',
				33 => 'Estrategia transversal para el desarrollo y protección de niñas, niños y adolescentes"',
				34 => 'Estrategia transversal de incorporación de la ciencia, tecnología e innovación"',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c602e2a489c',
			'label' => 'Objetivo estratégico asociado',
			'name' => 'obj_est_f',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo estratégico que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '19',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				24 => '3.1 Desarrollo social, integral y solidario',
				25 => '3.2 Educación de relevancia y equipada',
				27 => '3.3 Salud con calidad y calidez',
				29 => '3.4 Cultura física y deporte',
				30 => '3.5 Arte y cultura',
				32 => 'Estrategia transversal de perspectiva de género',
				33 => 'Estrategia transversal para el desarrollo y protección de niñas, niños y adolescentes"',
				34 => 'Estrategia transversal de incorporación de la ciencia, tecnología e innovación"',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c5d0ce02d54',
			'label' => 'Objetivo estratégico asociado',
			'name' => 'obj_est_g',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo estratégico que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '1',
					),
				),
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '17',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => '1.1 Cero tolerancia a la corrupción',
				2 => '1.2 Impulso a la participación ciudadana',
				3 => '1.3 Gobierno digital Incrementar los servicios"',
				4 => '1.4 Mejora regulatoria',
				5 => '1.5 Desarrollo institucional de los municipios',
				6 => '1.6 Fortalecimiento hacendario',
				7 => '1.7 Administración eficiente de los recursos',
				8 => 'Estrategia transversal de perspectiva de género',
				9 => 'Estrategia transversal para el desarrollo y protección de niñas, niños y adolescentes"',
				10 => 'Estrategia transversal de incorporación de la ciencia, tecnología e innovación"',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c6040286195',
			'label' => 'Objetivo estratégico asociado',
			'name' => 'obj_est_h',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo estratégico que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '3',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => '1.1 Cero tolerancia a la corrupción',
				2 => '1.2 Impulso a la participación ciudadana',
				3 => '1.3 Gobierno digital Incrementar los servicios"',
				4 => '1.4 Mejora regulatoria',
				5 => '1.5 Desarrollo institucional de los municipios',
				6 => '1.6 Fortalecimiento hacendario',
				7 => '1.7 Administración eficiente de los recursos',
				8 => 'Estrategia transversal de perspectiva de género',
				9 => 'Estrategia transversal para el desarrollo y protección de niñas, niños y adolescentes"',
				10 => 'Estrategia transversal de incorporación de la ciencia, tecnología e innovación"',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c6052739193',
			'label' => 'Objetivo estratégico asociado',
			'name' => 'obj_est_j',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo estratégico que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '18',
					),
				),
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '11',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				45 => '5.1 Equidad de servicios e infraestructura sostenible',
				46 => '5.2 Cultura y formación ambiental',
				47 => '5.3 Ordenamiento territorial integral y sostenible',
				48 => '5.4 Movilidad sostenible y eficiente',
				49 => '5.5 Preservación del patrimonio natural',
				50 => '5.6 Planeación para el desarrollo territorial sostenible',
				51 => 'Estrategia transversal de perspectiva de género',
				52 => 'Estrategia transversal para el desarrollo y protección de niñas, niños y adolescentes"',
				53 => 'Estrategia transversal de incorporación de la ciencia, tecnología e innovación"',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c60475b2c96',
			'label' => 'Objetivo estratégico asociado',
			'name' => 'obj_est_i',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo estratégico que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '9',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => '1.1 Cero tolerancia a la corrupción',
				2 => '1.2 Impulso a la participación ciudadana',
				3 => '1.3 Gobierno digital Incrementar los servicios"',
				4 => '1.4 Mejora regulatoria',
				5 => '1.5 Desarrollo institucional de los municipios',
				6 => '1.6 Fortalecimiento hacendario',
				7 => '1.7 Administración eficiente de los recursos',
				8 => 'Estrategia transversal de perspectiva de género',
				9 => 'Estrategia transversal para el desarrollo y protección de niñas, niños y adolescentes"',
				10 => 'Estrategia transversal de incorporación de la ciencia, tecnología e innovación"',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c60618e87d0',
			'label' => 'Objetivo estratégico asociado',
			'name' => 'obj_est_k',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo estratégico que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '16',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				12 => '2.1 Progreso económico incluyente',
				15 => '2.2 Entorno económico dinámico e innovador',
				16 => '2.3 Articulación y consolidación de los sectores productivos',
				17 => '2.4 Turismo, palanca de desarrollo',
				18 => '2.5 Campo moderno y productivo',
				35 => '4.1 Gobernabilidad',
				36 => '4.2 Derechos humanos',
				37 => '4.3 Seguridad integral',
				39 => '4.4 Procuración de justicia con trato humano',
				40 => '4.5 Reinserción social',
				41 => '4.6 Protección civil',
				42 => 'Estrategia transversal de perspectiva de género',
				43 => 'Estrategia transversal para el desarrollo y protección de niñas, niños y adolescentes"',
				44 => 'Estrategia transversal de incorporación de la ciencia, tecnología e innovación"',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c606db4df5b',
			'label' => 'Objetivo estratégico asociado',
			'name' => 'obj_est_l',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo estratégico que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '2',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				35 => '4.1 Gobernabilidad',
				36 => '4.2 Derechos humanos',
				37 => '4.3 Seguridad integral',
				39 => '4.4 Procuración de justicia con trato humano',
				40 => '4.5 Reinserción social',
				41 => '4.6 Protección civil',
				42 => 'Estrategia transversal de perspectiva de género',
				43 => 'Estrategia transversal para el desarrollo y protección de niñas, niños y adolescentes"',
				44 => 'Estrategia transversal de incorporación de la ciencia, tecnología e innovación"',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c6074228d8c',
			'label' => 'Objetivo estratégico asociado',
			'name' => 'obj_est_m',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo estratégico que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '14',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				35 => '4.1 Gobernabilidad',
				36 => '4.2 Derechos humanos',
				37 => '4.3 Seguridad integral',
				39 => '4.4 Procuración de justicia con trato humano',
				40 => '4.5 Reinserción social',
				41 => '4.6 Protección civil',
				42 => 'Estrategia transversal de perspectiva de género',
				43 => 'Estrategia transversal para el desarrollo y protección de niñas, niños y adolescentes"',
				44 => 'Estrategia transversal de incorporación de la ciencia, tecnología e innovación"',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c60793c97d1',
			'label' => 'Objetivo estratégico asociado',
			'name' => 'obj_est_n',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo estratégico que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '12',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				24 => '3.1 Desarrollo social, integral y solidario',
				25 => '3.2 Educación de relevancia y equipada',
				27 => '3.3 Salud con calidad y calidez',
				29 => '3.4 Cultura física y deporte',
				30 => '3.5 Arte y cultura',
				45 => '5.1 Equidad de servicios e infraestructura sostenible',
				46 => '5.2 Cultura y formación ambiental',
				47 => '5.3 Ordenamiento territorial integral y sostenible',
				48 => '5.4 Movilidad sostenible y eficiente',
				49 => '5.5 Preservación del patrimonio natural',
				50 => '5.6 Planeación para el desarrollo territorial sostenible',
				51 => 'Estrategia transversal de perspectiva de género',
				52 => 'Estrategia transversal para el desarrollo y protección de niñas, niños y adolescentes"',
				53 => 'Estrategia transversal de incorporación de la ciencia, tecnología e innovación"',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c6086e9d4d8',
			'label' => 'Objetivo estratégico asociado',
			'name' => 'obj_est_o',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo estratégico que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '10',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => '1.1 Cero tolerancia a la corrupción',
				2 => '1.2 Impulso a la participación ciudadana',
				3 => '1.3 Gobierno digital Incrementar los servicios"',
				4 => '1.4 Mejora regulatoria',
				5 => '1.5 Desarrollo institucional de los municipios',
				6 => '1.6 Fortalecimiento hacendario',
				7 => '1.7 Administración eficiente de los recursos',
				24 => '3.1 Desarrollo social, integral y solidario',
				25 => '3.2 Educación de relevancia y equipada',
				27 => '3.3 Salud con calidad y calidez',
				29 => '3.4 Cultura física y deporte',
				30 => '3.5 Arte y cultura',
				35 => '4.1 Gobernabilidad',
				36 => '4.2 Derechos humanos',
				37 => '4.3 Seguridad integral',
				39 => '4.4 Procuración de justicia con trato humano',
				40 => '4.5 Reinserción social',
				41 => '4.6 Protección civil',
				42 => 'Estrategia transversal de perspectiva de género',
				43 => 'Estrategia transversal para el desarrollo y protección de niñas, niños y adolescentes"',
				44 => 'Estrategia transversal de incorporación de la ciencia, tecnología e innovación"',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c6097866afa',
			'label' => 'Objetivo estratégico asociado',
			'name' => 'obj_est_p',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo estratégico que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '15',
					),
				),
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '5',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				14 => '2.1 Progreso económico incluyente',
				15 => '2.2 Entorno económico dinámico e innovador',
				16 => '2.3 Articulación y consolidación de los sectores productivos',
				17 => '2.4 Turismo, palanca de desarrollo',
				18 => '2.5 Campo moderno y productivo',
				19 => 'Estrategia transversal de perspectiva de género',
				21 => 'Estrategia transversal para el desarrollo y protección de niñas, niños y adolescentes"',
				22 => 'Estrategia transversal de incorporación de la ciencia, tecnología e innovación"',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c60a2ae808c',
			'label' => 'Objetivo estratégico asociado',
			'name' => 'obj_est_q',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo estratégico que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '15',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				14 => '2.1 Progreso económico incluyente',
				15 => '2.2 Entorno económico dinámico e innovador',
				16 => '2.3 Articulación y consolidación de los sectores productivos',
				17 => '2.4 Turismo, palanca de desarrollo',
				18 => '2.5 Campo moderno y productivo',
				19 => 'Estrategia transversal de perspectiva de género',
				21 => 'Estrategia transversal para el desarrollo y protección de niñas, niños y adolescentes"',
				22 => 'Estrategia transversal de incorporación de la ciencia, tecnología e innovación"',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c99220c545b',
			'label' => 'Objetivo general del PED',
			'name' => 'ob_gral',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => '1.2.1 Optimizar los mecanismos de vinculación institucional y social para la planeación, control y evaluación de los programas y acciones de la administración pública, promoviendo la participación ciudadana.',
				2 => '1.2.5 Consolidar la operación del sistema estatal de planeación en la conducción y coordinación de los procesos institucionales de las dependencias y organismos del Gobierno del Estado, así como en la concertación con los sectores social y privado.',
				3 => '1.2.4 Normar y coordinar la capacitación, integración, actualización y producción de la información sobre diversos aspectos de la realidad demográfica, social y económica del territorio estatal y municipal.',
				4 => '1.7.1 Hacer eficiente la programación y aplicación de lo-s recursos financieros en proyectos de gasto que arrojen resultados efectivos, priorizando aquellos que aporten mayor impacto al bienestar de la población del estado de Hidalgo.',
				5 => '1.7.4 Aplicar eficientemente los recursos programados.',
				6 => '1.7.5 Evaluar el ejercicio del gasto',
				7 => '5.3.3 Garantizar un desarrollo urbano sostenible y equilibrado, en armonía con el entorno natural.',
				8 => '5.6.1 Impulsar políticas públicas de orden territorial con impacto en la sostenibilidad, el medio físico, la productividad e inclusión social de las zonas geográficas que conforman la entidad.',
				9 => '5.6.2 Propiciar el desarrollo de proyectos estratégicos de orden regional y microrregional de impacto en el desarrollo económico, social y ambiental de la entidad.',
				10 => 'G1. Promover la participación ciudadana en términos igualitarios entre mujeres y hombres, en la observación y vigilancia de los mecanismos de gestión gubernamental en los temas de transparencia y rendición de cuentas, de manera corresponsable ciudadanía-Gobierno.',
				11 => 'NNA1. Contribuir al derecho de identidad de niñas, niños y adolescentes que garantice su pleno acceso a los servicios del Estado.',
				12 => 'CTI1. Incrementar la efectividad, cercanía y transparencia en la gestión gubernamental mediante el desarrollo de conocimiento, tecnología e innovaciones que contribuyan a la digitalización del Gobierno Estatal y los municipales.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c99905c545c',
			'label' => 'Objetivo sectorial y especial',
			'name' => 'ob_gral_aa',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Ob3 Instrumentar los mecanismos institucionales en materia de evaluación y monitoreo para llevar a cabo la verificación, medición, detección y corrección de desviaciones de carácter cualitativo y cuantitativo acerca del avance y cumplimiento de los objetivos y metas de los Planes y Programas, aportando los resultados necesarios para la toma de decisiones.',
				2 => 'Ob4		Fortalecer el proceso de planeación y promoción del desarrollo integral	de las zonas metropolitanas del estado, favoreciendo la coordinación y concertación interinstitucional con programas, proyectos y acciones de gran impacto.',
				3 => 'Ob5 Implementar mecanismos e instrumentos de planeación territorial estratégica que favorezcan el desarrollo integral y equilibrado del estado, así como la reducción en la brecha de desigualdad social y económica con apego a la preservación del ambiente.',
				4 => 'OTG.	Garantizar que los instrumentos de planeación contengan planteamientos estratégicos orientados a la disminución de las desigualdades entre hombres y mujeres.',
				5 => 'OTNNA.	Garantizar que los instrumentos de planeación contengan planteamientos estratégicos orientados al desarrollo y protección de niños, niñas y adolescentes',
				6 => 'OTCTI.	Garantizar que los instrumentos de planeación contengan planteamientos estratégicos orientados a la incorporación de la ciencia, la tecnología y la innovación',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9995bc545d',
			'label' => 'Objetivo general del PED',
			'name' => 'ob_gral_a',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '2',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'O.G 4.4.1 Impulsar una procuración de justicia eficaz, eficiente, transparente y evaluable, que permita un Hidalgo seguro, privilegiando la eliminación de desigualdades en la otorgación de los servicios entre hombres y mujeres y el respeto a sus derechos humanos en igualdad; previniendo, persiguiendo y sancionando el delito, con personal calificado, procedimientos y trámites optimizados, sensibles a las necesidades ciudadanas en materia social, económica y ambiental; y con mecanismos de coordinación institucional para erradicar la corrupción',
				2 => 'O.G. 4.4.2 Propiciar el mejoramiento y profesionalización de la Procuraduría General de Justicia.',
				3 => 'O.G 4.4.3 Consolidar el Sistema de Justicia Penal Acusatorio.',
				4 => 'O.G 4.4.4 Implementar procesos que reduzcan tiempos de espera a las y los ciudadanos, aplicando TICs, bajo una administración de infraestructura de cómputo centralizada, que contribuya a alcanzar un Sistema de Justicia Penal eficaz, expedito, imparcial y transparente',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c99a0dc545e',
			'label' => 'Objetivo sectorial y especial',
			'name' => 'ob_s_e_a',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '2',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Incrementar el nivel de atención en la investigación y persecución especializada del delito, con trato humano y perspectiva de género',
				2 => 'Aumentar la confianza ciudadana en la procuración de justicia a través de la atención, modernización y profesionalización institucional',
				3 => 'Implementar procesos de mejora continua en la	vigilancia y evaluación del desempeño de	 las y los servidores públicos involucrados en la procuración de justicia',
				4 => 'Fortalecer la consolidación del sistema de justicia penal acusatorio y adversarial, y la conclusión de casos penales del sistema tradicional, para contribuir a mantener la paz, tranquilidad y seguridad de la población.',
				5 => 'Prevenir, atender y erradicar la violencia contra mujeres y niñas, en el ámbito de competencia de la Procuraduría General de Justicia del Estado de Hidalgo.',
				'Incrementar y fortalecer el nivel de prevención, investigación y persecución del delito para la protección de niñas, niños y adolescentes' => 'Incrementar y fortalecer el nivel de prevención, investigación y persecución del delito para la protección de niñas, niños y adolescentes',
				6 => 'Eficientar la procuración de justicia a través de la investigación del delito y servicios periciales optimizados, y la mejora de procedimientos y trámites que se ofrecen en la entidad.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c99a8bc545f',
			'label' => 'Objetivo general del PED',
			'name' => 'ob_gral_b',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '3',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'O.G 1.1.1 Combatir la corrupción, de tal manera que la actuación de los servidores públicos se apegue a los principios y valores éticos, a una cultura de rendición de cuentas y a una fiscalización de forma permanente de la aplicación de los recursos públicos, bajo la legalidad y la honradez.',
				2 => 'O.G 1.1.2. Promover la transparencia en la gestión pública, vigilando que la información relacionada con los recursos públicos disponible para la ciudadanía sea oportuna, fiable y de calidad.',
				3 => 'O.G 1.2.1 Optimizar los mecanismos de vinculación institucional y social para la planeación, control y evaluación de los programas y acciones de la administración pública, promoviendo la participación ciudadana.',
				4 => 'G1. Promover la participación ciudadana en términos igualitarios entre mujeres y hombres, en la observación y vigilancia de los mecanismos de gestión gubernamental en los temas de transparencia y rendición de cuentas, de manera corresponsable ciudadanía-Gobierno',
				5 => 'Contribuir para garantizar que en todas las acciones se considere el interés superior de la niñez y adolescencia, considerando a ambos grupos como sujetos de derechos que requieren la intervención de la sociedad y el estado para lograr bienestar y desarrollo pleno.',
				6 => 'Consolidar una sociedad y economía del conocimiento en el Estado de Hidalgo haciendo del desarrollo científico, tecnológico	y la innovación la base de su progreso económico y social sostenible.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c99c37c5460',
			'label' => 'Objetivo sectorial y especial',
			'name' => 'ob_s_e_b',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '3',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'OG/SC/1 Mejorar la fiscalización de los Recursos Públicos.',
				2 => 'OG/SC/2 Verificar que la información programática, presupuestal y financiera se apegue a la normatividad con el propósito de disminuir la corrupción.',
				3 => 'OG/SC/2 Consolidar acciones de supervisión y evaluación de control interno y vigilancia con un enfoque preventivo, basado en la detección de factores de riesgo para contribuir a una gestión pública estatal sin corrupción, eficaz, eficiente, honesta y transparente y generar confianza de la sociedad.',
				4 => 'OG/SC/4 Fortalecer los procesos de inspección, supervisión, vigilancia y auditoria, apegándose estrictamente a criterios normativos e imparciales para disminuir las irregularidades y deficiencias en la obra pública y las acciones en el Estado de Hidalgo.',
				5 => 'OG/SC/5 Disminuir el tiempo de respuesta en los trámites y servicios públicos que ofrece la Contraloría a través de la Dirección General de Normatividad',
				6 => 'OG/SC/6 Consolidar las acciones en materia de transparencia, anticorrupción, acceso a la Información, protección de datos personales y participación ciudadana, a través de la actualización del Portal de Transparencia del Poder ejecutivo del Estado de Hidalgo para una mejor rendición de cuentas.',
				7 => 'OG/SC/7 Optimizar los mecanismos de vinculación institucional y social para la planeación, control y evaluación de los programas y acciones de la administración pública, promoviendo la participación activa de las y los hidalguenses en la toma de decisiones y en el actuar gubernamental.',
				8 => 'OG/SC/TRANSV/GENERO Impulsar la igualdad de oportunidades de hombres y mujeres para el acceso a la toma de decisiones.',
				9 => 'OG/SC/TRANSV/NNA Coadyuvar a través de las acciones de la Secretaría para que los derechos de niñas, niños y adolescentes así como su bienestar y desarrollo sean prioridad en el quehacer gubernamental.',
				10 => 'OG/SC/TRANSV/CTI Coadyuvar al desarrollo científico, tecnológico y la innovación del estado a través de los trámites y servicios que ofrecemos como Dependencia.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c99ce5c5461',
			'label' => 'Objetivo general del PED',
			'name' => 'ob_gral_c',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '4',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'O.G. 3.5.2 Preservar el patrimonio y la diversidad cultural de Hidalgo.',
				2 => 'O.G. 3.5.5 Fortalecer el desarrollo cultural en los municipios de Hidalgo.',
				3 => 'O.G. 3.5.6 Potenciar la gastronomía tradicional como elemento de identidad cultural.4',
				4 => 'O.G. 3.5.1 Promover la creación artística y popular en Hidalgo.',
				5 => 'O.G. 3.5.3 Impulsar la educación y la investigación artística y cultural en Hidalgo.',
				6 => 'O.G. 3.5.4 Brindar acceso universal a la cultura mediante el acceso a las TICs.',
				7 => 'CTI3. Consolidar una sociedad del conocimiento mediante la creación de capacidades científicas y tecnológicas que promueva un desarrollo integral y equilibrado de todas las regiones y sectores del Estado.',
				8 => 'G3. Impulsar la igualdad sustantiva entre mujeres y hombres en los ámbitos educativo, cultural, laboral, económico, político y social para mejorar las condiciones de vida de las mujeres y cerrar las brechas de género que limitan el desarrollo del Estado.',
				'9: NNA3. Contribuir al cumplimiento del derecho a la salud, así como prevenir, atender y sancionar la violencia hacia la niñez y la adolescencia.' => '9: NNA3. Contribuir al cumplimiento del derecho a la salud, así como prevenir, atender y sancionar la violencia hacia la niñez y la adolescencia.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c99ef9c5462',
			'label' => 'Objetivo sectorial y especial',
			'name' => 'ob_s_e_c',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '4',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'OG 1.Desarrollar la infraestructura cultural de Hidalgo.',
				2 => 'OG 2. Preservar, fomentar y difundir las manifestaciones artísticas y culturales de carácter popular, urbano, rural e indígena, a través de la promoción y el fortalecimiento de la articulación del sector.',
				3 => 'OG 3. Impulsar el sistema estatal de educación artística y la red estatal de bibliotecas, así como la descentralización de estos servicios hacia los municipios.',
				4 => 'OG 4.- Fortalecer la investigación, protección y difusión del patrimonio, la diversidad cultural y la historia del estado de Hidalgo.',
				5 => 'OG 5.- Impulsar las industrias culturales y creativas, fortalecer la difusión de las actividades para generar públicos e incorporar las tecnologías de la información para brindar acceso a los bienes y servicios culturales de Hidalgo.',
				6 => 'Implementar acciones para la modernización de la Secretaría y sus procesos, incorporando herramientas tecnológicas para acerca la cultura y las artes a la población de una manera más efectiva.',
				7 => 'Impulsar la igualdad sustantiva entre hombres y mujeres en los ámbitos artístico y cultural para mejorar las condiciones de acceso a los apoyos y estímulos otorgados y cerrar las brechas de desigualdad en el sector cultural.',
				8 => 'Contribuir a que niñas, niños y adolescentes gocen de sus derechos de recreación y acceso al arte y la cultura.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c99fb9c5463',
			'label' => 'Objetivo general del PED',
			'name' => 'ob_gral_d',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '5',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'O.G. 2.5.4 Fortalecer a los productores del sector primario para incrementar su competitividad.',
				2 => 'O.G. 2.5.1 Impulsar el crecimiento de la productividad en el sector primario hidalguense.',
				3 => 'O.G. 2.5.4 Fortalecer a los productores del sector primario para incrementar su competitividad.',
				4 => 'G2. Garantizar la inclusión y participación de mujeres y hombres en igualdad de',
				'condiciones en el desarrollo económico del estado."' => 'condiciones en el desarrollo económico del estado."',
				5 => 'NNA2. Contribuir al desaliento y erradicación del trabajo infantil que interfiere en la educación y pleno desarrollo físico, mental y emocional de niñas, niños y adolescentes.',
				6 => 'CTI2. Consolidar una economía sólida, dinámica y diversa basada en la ciencia, tecnología e innovación que genere desarrollo sostenible en los sectores y actividades productivas del estado y contribuya al bienestar social y económico de la población hidalguense.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9a0a0c5464',
			'label' => 'Objetivo sectorial y especial',
			'name' => 'ob_s_e_d',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '5',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'OG-SEDAGROH-4 Favorecer el fortalecimiento de las actividades agroalimentarias con el manejo sustentable de sus recursos naturales mediante la innovación, generación de conocimiento	y transferencia de tecnología que incrementen los ingresos de todos los actores.',
				2 => 'OG-SEDAGROH-1 Impulsar el desarrollo de actividades agropecuarias y acuícolas con proyectos sustentables de alto impacto local, regional y estatal que permitan fortalecer las cadenas productivas, la conservación de los recursos naturales y mejorar la calidad de las familias rurales.',
				3 => 'OG-SEDAGROH-7 Fomentar un sector agroalimentario productivo, competitivo y sustentable, que contribuya con la seguridad alimentaria, con el aumento del valor agregado de sus productos, el uso de innovaciones tecnológicas que permitan obtener productos de calidad, sanos e inocuos, y mejoren el nivel de vida de las familias productoras especialmente vulnerables.',
				4 => 'OG-SEDAGROH-2 Promover el desarrollo sostenido de las comunidades rurales a fin de incrementar la productividad y el aprovechamiento de sus recursos naturales para elevar su calidad de vida y sus posibilidades de empleo e ingreso.',
				5 => 'OG-SEDAGROH-3 Conservar y elevar el estatus fitozoosanitario y acuícola mediante la aplicación de buenas prácticas y programas que permitan disminuir pérdidas asociadas a enfermedades y plagas, para procurar la sanidad e inocuidad alimentaria y proteger la inversión de las familias productoras.',
				6 => 'OG-SEDAGROH-5 Promover el manejo del agua agrícola con mayor eficiencia a través del uso de tecnologías sustentables que contribuyan a mitigar los efectos del cambio climático y se mantenga la disponibilidad del recurso hídrico.',
				7 => 'OG-SEDAGROH-6 Impulsar la producción cafetalera mediante la renovación y repoblación de cafetales establecidos a través de prácticas sustentables que permitan obtener un café de calidad y su comercialización justa.',
				8 => 'A1. Fomentar la inclusión y participación de mujeres y hombres en igualdad de condiciones en el desarrollo económico del Estado.',
				9 => 'B1. Contribuir para garantizar que en todas las acciones se considere el interés superior de la niñez y adolescencia, considerando a ambos grupos como sujetos de derechos que requieren la intervención de la sociedad y el Estado para lograr bienestar y desarrollo pleno.',
				10 => 'C1. Incrementar el desarrollo, innovación, uso y aplicación de tecnologías eficientes, en los sistemas de producción y en todas las redes de valor, con un enfoque transversal sustentable.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9a229c5465',
			'label' => 'Objetivo general del PED',
			'name' => 'ob_gral_e',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '6',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'O.G 2.2.3 Impulsar una política que favorezca la mejora regulatoria como una práctica para generar un ambiente de negocios competitivo que brinde certidumbre a los inversores',
				2 => 'O.G 2.3.1 Impulsar la conformación de cadenas productivas locales que permitan generar un mayor valor agregado a las micro, pequeñas y medianas empresas',
				3 => 'O.G 2.1.1 Facilitar la generación de más y mejores empleos formales y oportunidades de trabajo digno o decente, para los hidalguenses, especialmente para aquellas personas en situación de vulnerabilidad',
				4 => 'O.G 2.3.2 Promover el desarrollo industrial sostenible vinculado a los sectores primario y terciario locales, en el ámbito local',
				5 => 'O.G	5.1.3 Garantizar la dotación de energía a la población hidalguense para el desempeño de todas las actividades personales, sociales y productivas, bajo esquemas sostenibles y de eficiencia energética',
				6 => 'O.G 2.2.1 Impulsar la atracción de inversiones, favoreciendo el incremento de los flujos de inversión nacional y extranjera directa captados por el estado de Hidalgo',
				7 => 'O.G 2.2.2 Incrementar la capacidad de innovación en los procesos productivos',
				8 => 'O.G G2. Garantizar la inclusión y participación de mujeres y hombres en igualdad de condiciones en el desarrollo económico del estado',
				9 => 'O.G NNA2. Contribuir al desaliento y erradicación del trabajo infantil que interfiere en la educación y pleno desarrollo físico, mental y emocional de niñas, niños y adolescentes',
				10 => 'O.G CTI2. Consolidar una economía sólida, dinámica y diversa basada en la ciencia, tecnología e innovación que genere desarrollo sostenible en los sectores y actividades productivas del estado y contribuya al bienestar social y económico de la población hidalguense',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9a356c5466',
			'label' => 'Objetivo sectorial y especial',
			'name' => 'ob_s_e_e',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '6',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'F1-OG1. Mejorar la normativa que impacta en el ambiente de negocios y en la actividad económica',
				2 => 'F2-OG2. Consolidar las vocaciones económicas existentes y potenciales con criterio incluyente y sostenible, impulsando la conectividad, la articulación productiva y la incursión de las empresas hidalguenses en los mercados globales, a través de políticas de fomento económico con participación social',
				3 => 'F2-OG3. Contribuir al incremento de la productividad y competitividad de los emprendedores, y de las micro, pequeñas y medianas empresas, en los sectores tradicionales y estratégicos, aprovechando las capacidades y vocaciones regionales de la entidad',
				4 => 'F2-OG4. Consolidar al sector industrial en el Estado a través de la reactivación, fomento y ampliación de las zonas y parques industriales, para la captación de inversiones productivas, mediante infraestructura industrial y eléctrica así como servicios de calidad, que permitan el fortalecimiento y consolidación de una plataforma logística integral',
				5 => 'F2-OG5. Promover la cobertura de la infraestructura energética, convencional y no convencional, en el Estado de Hidalgo',
				6 => 'F3-OG6. Fomentar la captación de inversión local, nacional y extranjera en sectores estratégicos y tradicionales del Estado, bajo un atractivo entorno económico y condiciones competitivas para las empresas, que permitan la generación de nuevos empleos y la conservación de los ya existentes',
				7 => 'F3-OG7. Desarrollar la planeación, infraestructura, equipamiento y servicios en el Valle de Plata, para la ejecución de proyectos estratégicos que incentive la captación de inversión nacional y extranjera, de manera que se convierta en un detonador local y regional.',
				8 => 'OT1-PSDE. Impulsar la inclusión y participación de mujeres y hombres con criterios de igualdad en el acceso a las oportunidades de empleo formal, mediante el fomento de programas de desarrollo económico sustentable en el Estado.',
				9 => 'OT3-PSDE. Implementar procesos de innovación y acceso a la modernización tecnológica en las acciones de impulso al desarrollo económico.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9a3d7c5467',
			'label' => 'Objetivo general del PED',
			'name' => 'ob_gral_f',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '7',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'O.G. 3.1.4 Reducir la pobreza moderada en todas sus dimensiones',
				2 => 'O.G. 3.1.6 Mejorar las condiciones de vida de la población migrante internacional de origen hidalguense, sus familias y comunidades, impulsando políticas sociales igualitarias y sostenibles',
				3 => 'O.G. 3.1.7 Disminuir las carencias sociales de las localidades indígenas mediante el fortalecimiento de su cultura, el respeto a sus derechos, la obtención de ingresos sostenibles y la dotación de infraestructura básica',
				4 => 'O.G.1.2.1 Optimizar los mecanismos de vinculación institucional y social para la planeación, control y evaluación de los programas y acciones de la administración pública, promoviendo la participación ciudadana.',
				5 => 'O.G. 3.5.5 Fortalecer el desarrollo cultural en los municipios de Hidalgo',
				6 => 'O.G.3.1.9 Incrementar la cobertura de jóvenes atendidos a través de esquemas que incidan en las esferas de salud, educación, empleo y participación ciudadana.',
				7 => 'O.G. 3.1.8 Ampliar la cobertura de atención integral hacia las personas adultas mayores con calidad y calidez en pro de generar una vejez digna y activa con inclusión social.',
				8 => 'G3. Impulsar la igualdad sustantiva entre mujeres y hombres en los ámbitos',
				'educativo, cultural, laboral, económico, político y social, para mejorar las' => 'educativo, cultural, laboral, económico, político y social, para mejorar las',
				'condiciones de vida de las mujeres y cerrar las brechas de género que limitan' => 'condiciones de vida de las mujeres y cerrar las brechas de género que limitan',
				'el desarrollo del estado."' => 'el desarrollo del estado."',
				9 => 'NNA3. Contribuir al cumplimiento del derecho a la salud, así como prevenir,',
				'atender y sancionar la violencia hacia la niñez y la adolescencia."' => 'atender y sancionar la violencia hacia la niñez y la adolescencia."',
				10 => 'CTI3. Consolidar una sociedad del conocimiento mediante la creación de',
				'capacidades científicas y tecnológicas que promueva un desarrollo integral y' => 'capacidades científicas y tecnológicas que promueva un desarrollo integral y',
				'equilibrado de todas las regiones y sectores del estado."' => 'equilibrado de todas las regiones y sectores del estado."',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9a43cc5468',
			'label' => 'Objetivo sectorial y especial',
			'name' => 'ob_s_e_f',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '7',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Mejorar las condiciones de vida de los hidalguenses que habiten en municipios del Estado que presenten carencia en calidad y espacios de la vivienda',
				2 => 'Contribuir en la disminución de la carencia alimentaria en el estado de Hidalgo, dando prioridad a las zonas de alta y muy alta marginación.',
				3 => 'Aumentar el número de personas migrantes y sus familias atendidas mediante acciones de asistencia y asesoría para mejorar sus condiciones de vida',
				4 => 'Impulsar la participación de la sociedad en la toma de decisiones de política pública en el estado de Hidalgo.',
				5 => 'Apoyar a las y los artesanos hidalguense para que incrementen sus ingresos mediante la entrega de apoyos para la producción, comercialización y capacitación, que le permitan mejorar sus condiciones de vida',
				6 => 'Atender a la juventud hidalguense mediante acciones implementadas por el programa que mejoren su calidad de vida',
				7 => 'Brindar atención integral a las personas adultas mayores mediante acciones gerontológicas para mejorar su calidad de vida',
				8 => 'Objetivo Transversal 1 Fortalecer e impulsar la perspectiva de igualdad de género que tenga como propósito la protección de niñas, niños, adolescentes, personas adultas mayores y mujeres migrantes en casos de desigualdad o discriminación para garantizar su desarrollo social y ejercicio de sus derechos.',
				9 => 'Objetivo Transversal 2 Impulsar acciones integrales que tengan como propósito proteger a las niñas, niños y adolescentes, en condición de pobreza, marginación o de vulnerabilidad.',
				10 => 'Objetivo Transversal 3 Implementar el uso de las nuevas tecnologías en la construcción, aplicación, seguimiento, control y evaluación de programas, subprogramas y proyectos de desarrollo social',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9a4bfc5469',
			'label' => 'Objetivo general del PED',
			'name' => 'ob_gral_g',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '8',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'O.G.3.2.4. Ofrecer educación Incluyente y equitativa en todos los niveles y modalidades',
				2 => 'O.G.3.2.2. Incrementar la cobertura del sector educativo en el estado de Hidalgo.',
				3 => 'O.G.3.2.3. Mejorar las instalaciones del sector educativo en el estado de Hidalgo.',
				4 => 'O.G. 3.2.1. Fortalecer al personal docente de Educación Básica en el Estado de Hidalgo.',
				5 => 'CTI3. Consolidar una sociedad del conocimiento mediante la creación de capacidades científicas y tecnológicas que promueva un desarrollo integral y equilibrado de todas las regiones y sectores del estado."',
				6 => 'G3. Impulsar la igualdad sustantiva entre mujeres y hombres en los ámbitos educativo, cultural, laboral, económico, político y social, para mejorar las condiciones de vida de las mujeres y cerrar las brechas de género que limitan el desarrollo del estado."',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9a54cc546a',
			'label' => 'Objetivo sectorial y especial',
			'name' => 'ob_s_e_g',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '8',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Ampliar la vinculación entre las instituciones de educación con el sector empresarial a nivel Estatal, Nacional e Internacional',
				2 => 'Incrementar la cobertura y calidad de la oferta educativa en el Estado de Hidalgo.',
				'Fomentar en las instituciones educativas el desarrollo de valores y actitudes que respalden los derechos humanos.' => 'Fomentar en las instituciones educativas el desarrollo de valores y actitudes que respalden los derechos humanos.',
				3 => 'Fomentar prácticas educativas incluyentes con un enfoque intercultural y con perspectiva de género',
				4 => 'Promover la profesionalización de docentes y directivos en espacios de formación, capacitación, actualización y superación profesional.',
				5 => 'Garantizar el derecho de todas las niñas y mujeres a recibir servicios educativos con perspectiva de género en el estado de Hidalgo.',
				6 => 'Impulsar una educación integral que propicie el desarrollo físico, humanista y social, con un enfoque de sustentabilidad para los hidalguenses.',
				7 => 'Promover el desarrollo de la investigación científica y tecnológica en la comunidad educativa del Estado.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9a610c546b',
			'label' => 'Objetivo general del PED',
			'name' => 'ob_gral_h',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '9',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'O.G 1.6.2 Mejorar el marco jurídico fiscal.',
				2 => 'O.G 1.6.3 Implementar mecanismos de coordinación fiscal.',
				3 => 'O.G	1.7.1 Hacer eficiente la programación y aplicación de los recursos financieros en proyectos de gasto, que arrojen resultados efectivos, priorizando aquellos que aporten mayor impacto al bienestar de la población del Estado de Hidalgo.',
				4 => 'G1. Promover la participación ciudadana en términos igualitarios entre mujeres y hombres, en la observación y vigilancia de los mecanismos de gestión gubernamental en los temas de transparencia y rendición de cuentas, de manera corresponsable ciudadanía-Gobierno',
				5 => 'NNA1. Contribuir al derecho de identidad de niñas, niños y adolescentes que',
				'garantice su pleno acceso a los servicios del Estado."' => 'garantice su pleno acceso a los servicios del Estado."',
				6 => 'CTI1. Incrementar la efectividad, cercanía y transparencia en la gestión',
				'gubernamental mediante el desarrollo de conocimiento, tecnología e innovaciones' => 'gubernamental mediante el desarrollo de conocimiento, tecnología e innovaciones',
				'que contribuyan a la digitalización del Gobierno Estatal y los municipales."' => 'que contribuyan a la digitalización del Gobierno Estatal y los municipales."',
				7 => 'O.G 1.6.1 Propiciar una política hacendaria corresponsable',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9a6a2c546c',
			'label' => 'Objetivo sectorial y especial',
			'name' => 'ob_s_e_h',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '9',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Objetivo 1 Promover un ejercicio eficiente de los recursos presupuestarios disponibles que impulsen el	desarrollo económico, dentro de un marco de evaluación, transparencia y rendición de cuentas, que imprima solidez ante eventuales riesgos del entorno económico.',
				2 => 'Objetivo 2 Incrementar y consolidar	las fuentes de ingresos locales, mediante la implementación de estrategias que amplíen y optimicen la capacidad recaudatoria y fiscalizadora de la autoridad, el uso de las tecnologías de la información para facilitar al contribuyente los mecanismos de pago, en el marco de una política fiscal corresponsable, que contribuya a aumentar la capacidad financiera, la disponibilidad de recursos para atender las necesidades prioritarias de la población y coadyuvar al desarrollo de la entidad.',
				3 => 'Objetivo 3 Impulsar una política de financiamiento que permita la diversificación de opciones para el estado y los municipios, además de que posibilite su manejo adecuado y sostenible, con respeto irrestricto al	marco normativo.',
				4 => 'Objetivo 4 Impulsar un federalismo que propicie una coordinación eficaz entre los tres órdenes de gobierno, tendiente a fortalecer los ingresos del sector público, el equilibrio de las finanzas públicas, a través de esquemas de recaudación generadores de recursos, simples, progresivos, encaminados a fomentar la formalidad y el desarrollo económico.',
				5 => 'Objetivo 6 Presupuestar y programar el gasto público, particularmente el de inversión, acorde con las prioridades establecidas en el Plan Estatal de Desarrollo, los Programas Sectoriales, Regionales y Especiales, y el Presupuesto, con estricto apego a la normatividad vigente.',
				6 => 'Objetivo 5 Evaluación del Desempeño con Base en Resultados',
				7 => 'Objetivo 7 Participar activamente en la cooperación, incorporación, ejecución, vigilancia y asignación eficiente y eficaz de los recursos públicos, apoyando políticas de gasto que incidan en erradicar la violencia y discriminación contra las mujeres, y promuevan la igualdad de género.',
				8 => 'Objetivo 8 Generar una vinculación efectiva de los programas presupuestales para la protección de niñas, niños y adolescentes, para garantizar que la seguridad social de nuestra niñez y adolescencia se cumpla de manera integral, cuidando que sus derechos y obligaciones sean respetados y que los recursos se usen en la creación de infraestructura, estrategias, programas y acciones en su favor.',
				9 => 'Objetivo 9 Impulsar el uso y creación de nuevas tecnologías para mejorar de forma efectiva los procesos hacendarios, en la búsqueda de mejorar la recaudación del estado, aumentar la participación ciudadana, crear un gobierno abierto y transparente, optimizar los servicios a la ciudadanía, promover la innovación municipal y contar con plataformas digitales de calidad.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9a7dec546d',
			'label' => 'Objetivo general del PED',
			'name' => 'ob_gral_i',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '10',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'O.G 1.5.1 Fortalecer las capacidades institucionales de las administraciones municipales mediante la coordinación de las autoridades locales y las dependencias del Gobierno del Estado, la Federación y Organismos de la Sociedad Civil, mejorando la calidad de vida de su población.',
				2 => 'O.G 4.2.3. Garantizar el acceso a la justicia de las mujeres en situación de violencia.',
				3 => 'O.G 3.1.3. Lograr la Igualdad Sustantiva',
				4 => 'O.G 4.4.3 Consolidar el Sistema de Justicia Penal Acusatorio',
				5 => 'O.G 4.6.3 Participar en la coordinación entre los tres órdenes de gobierno y sectores de la sociedad, ante la eventualidad de una emergencia o desastre, preservando la vida humana, la salud, la familia, su patrimonio y su entorno, con un enfoque en la gestión integral de riesgos.',
				6 => 'O.G 3.1.1 Garantizar el acceso de las niñas, niños y adolescentes al ejercicio de su derecho a una vida plena en condiciones dignas que garantice su formación integral.',
				7 => 'O.G 3.1.4 Reducir la pobreza moderada en todas sus dimensiones',
				8 => 'O.G. 4.1.2 Propiciar la gobernabilidad democrática y el desarrollo político mediante la atención a grupos e individuos de la Sociedad Civil y la coordinación de acciones institucionales con los órdenes de gobierno y poderes en el estado.',
				9 => 'CTI4. Generar las condiciones de paz, seguridad y gobernabilidad necesarias para el desarrollo económico y social a través de la investigación científica, el desarrollo tecnológico y la innovación."',
				10 => 'O.G 4.1.4 Contribuir con la defensoría pública en la estabilidad jurídica para garantizar la certeza y seguridad a la ciudadanía.',
				11 => 'O.G 4.2.4 Dar certeza jurídica a las leyes, acuerdos, decretos, reglamentos, disposiciones de las diferentes dependencias del Gobierno Federal, Estatal, Municipal o de particulares.',
				12 => 'O.G 4.3.1 Instrumentar una política pública, de enfoque cercano, multidisciplinario e integral, que conforme un trabajo coordinado y participativo entre autoridades y sociedad; que disminuya e inhiba los factores de riesgo y fortalezca el tejido social en el marco de la legalidad, mejorando así, la calidad de vida de la población hidalguense y la optimización de las funciones de las instituciones de seguridad.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9a8c4c546e',
			'label' => 'Objetivo sectorial y especial',
			'name' => 'ob_s_e_i',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '10',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Ampliar las capacidades de las mujeres y las niñas en todos los ámbitos del desarrollo con la finalidad de garantizar el ejercicio de sus derechos humanos; impulsar su empoderamiento y disminuir la violencia que se ejerce contra ellas, así como contribuir a la igualdad sustantiva entre mujeres y hombres en el estado de Hidalgo.',
				2 => 'Brindar certeza y seguridad jurídica a la población hidalguense de manera eficaz, garantizando el pleno goce de los Derechos Humanos, cultura de la legalidad, justicia social y estado de derecho, mediante la implementación de modelos digitales.',
				3 => 'Consolidar una sociedad y economía del conocimiento en el estado de Hidalgo haciendo del desarrollo científico, tecnológico y la innovación la base de su progreso económico y social sostenible.',
				4 => 'Contribuir para garantizar que en todas las acciones se considere el interés superior de la niñez y adolescencia, considerando a ambos grupos como sujetos de derechos que requieren la intervención de la sociedad y el Estado para lograr bienestar y desarrollo pleno.',
				5 => 'Fortalecer el Sistema de Justicia Penal en el Estado de Hidalgo.',
				'Fortalecer las capacidades institucionales de las administraciones municipales, al impulsar diagnósticos, capacitación y profesionalización, mediante la coordinación y vinculación de autoridades locales, estatales, federales y organismos de la sociedad civil, en beneficio de la población.' => 'Fortalecer las capacidades institucionales de las administraciones municipales, al impulsar diagnósticos, capacitación y profesionalización, mediante la coordinación y vinculación de autoridades locales, estatales, federales y organismos de la sociedad civil, en beneficio de la población.',
				6 => 'Instrumentar de manera coordinada política integral de infancia y adolescencia con enfoque de derechos humanos.',
				7 => 'Instrumentar estrategias en materia de población y desarrollo, para que el ritmo de su crecimiento y su distribución en el Estado de Hidalgo propicien progreso y mejoren la calidad de vida de las personas.',
				8 => 'Preservar la gobernabilidad y gobernanza mediante la atención de problemáticas públicas que se presenten, a través de la canalización y la cooperación, privilegiando la tolerancia y el diálogo, con apego a derechos humanos y perspectiva de género, fortaleciendo la estabilidad, la paz y el orden social.',
				9 => 'Promover el logro de la igualdad sustantiva entre mujeres y hombres y el acceso de las mujeres a una vida libre de violencia, desde la perspectiva de género y derechos humanos, favoreciendo el desarrollo y participación de las mujeres en todos los ámbitos, en un marco de colaboración y coordinación institucional.',
				10 => 'Proteger la vida, integridad física, patrimonio y entorno de la población hidalguense, que se encuentra en riesgo por desastre y/o emergencia, causada por fenómenos perturbadores de origen natural y antropogénicos.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9a992c546f',
			'label' => 'Objetivo general del PED',
			'name' => 'ob_gral_j',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '11',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'C.T.I5. Promover el desarrollo sostenible mediante la generación y aprovechamiento del conocimiento para el uso adecuado y responsable de los recursos naturales disponibles en el estado.',
				2 => 'G5. Promover la participación de las mujeres en el desarrollo sostenible',
				3 => 'N.N.A5. Incentivar la generación de espacios con diseño sostenible al que puedan acceder y partiipar activamente niñas, niños y adolescentes, para conservar el ambiente del futuro.',
				4 => 'O.G. 5.4.1 Fomentar la movilidad motorizada sostenible mediante la incorporación de alternativas tecnológicas para los diferentes medios de transporte motorizados',
				5 => 'O.G. 5.4.2 Fortalecer y promover las alternativas de movilidad sostenible eficiente no motorizada para la población',
				6 => 'O.G. 5.4.3 Modernizar e implantar sistemas de transporte público integrados eficientes y sostenibles, que ofrezcan a la población un servicio de calidad con bajo impacto ambiental',
				7 => 'O.G. 5.4.4 Contar con la infraestructura vial adecuada, suficiente, flexible y sostenible para las necesidades de desplazamiento de la población, bienes y mercancías, en todos los medios y formas de transporte, incluida la movilidad no motorizada',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9a9fbc5470',
			'label' => 'Objetivo sectorial y especial',
			'name' => 'ob_s_e_j',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '11',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Establecer sistemas de transporte público integrados, eficientes	y sostenibles.',
				2 => 'Garantizar el acceso a sistemas integrados y sostenibles de transporte seguro y accesible	para toda la población',
				3 => 'Implantar nuevas tecnologías en materia de movilidad y transporte que detonen el desarrollo económico y social sostenible en el Estado.',
				4 => 'Impulsar la utilización del transporte alternativo no motorizado.',
				'Integrar a las zonas metropolitanas y otras zonas urbanas de relevancia estatal a un esquema de desarrollo urbano sostenible.' => 'Integrar a las zonas metropolitanas y otras zonas urbanas de relevancia estatal a un esquema de desarrollo urbano sostenible.',
				5 => 'Promover la ampliación de la infraestructura peatonal, motorizada	y no motorizada.',
				6 => 'Promover la movilidad motorizada a través de vehículos que cuenten con certificaciones de bajas emisiones contaminantes.',
				7 => 'Promover políticas públicas incluyentes en materia movilidad y transporte, impulsando la participación de la mujer, en el desarrollo sostenible en la materia.',
				8 => 'Proporcionar acceso a sistemas de transporte seguro, asequible, accesible y sostenible para todos; mejorar la seguridad vial, prestando atención especial a las necesidades de las personas en situación de vulnerabilidad, como la tienen; niñas, niños y adolescentes.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9aa52c5471',
			'label' => 'Objetivo general del PED',
			'name' => 'ob_gral_k',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '12',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'CTI5. Promover el desarrollo sostenible mediante la generación y aprovechamiento de conocimiento para el uso adecuado y responsable de los recursos naturales disponibles en el estado."',
				2 => 'G5. Promover la participación de las mujeres en el desarrollo sostenible.',
				3 => 'NNA5. Incentivar la generación de espacios con diseño sostenible al que puedan acceder y participar activamente niñas, niños y adolescentes, para conservar el ambiente del futuro."',
				4 => 'O.G 5.1.2 Asegurar que la población hidalguense cuente con sistemas sostenibles de drenaje y saneamiento de aguas residuales, mejorando las condiciones ambientales actuales.',
				5 => 'O.G 5.3.1 Asegurar la cobertura estatal, regional y municipal, de los instrumentos de planeación ecológica territorial.',
				6 => 'O.G. 5.1.1 Aumentar y asegurar la cobertura en el servicio de agua potable entubada para la población hidalguense, de manera sostenible y equitativa.',
				7 => 'O.G. 5.1.6 Generar espacios públicos de encuentro para la ciudadanía, con accesibilidad universal y adaptados para todos los usuarios; incluyentes, seguros, sostenibles y con alta calidad en su diseño, que contribuya a cultivar identidad y pertenencia.',
				8 => 'O.G. 5.4.4 Contar con la infraestructura vial adecuada, suficiente, flexible y sostenible para las necesidades de desplazamiento de la población, bienes y mercancías, en todos los medios y formas de transporte, incluida la movilidad no motorizada.',
				9 => 'O.G.5.1.5 Garantizar que cada familia hidalguense cuente con una vivienda digna, saludable, comunicada correctamente con el entorno construido y alimentada con los servicios públicos básicos',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9ac2a17681',
			'label' => 'Objetivo sectorial y especial',
			'name' => 'ob_s_e_k',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '12',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Coadyuvar en disminuir el déficit habitacional generado por aspectos de tipo cualitativo, referente al ineficiente suministro, consumo, tratamiento y saneamiento del agua y de tipo cuantitativo, relacionado a que no se tiene acceso a la vivienda, destacando los jóvenes.',
				2 => 'Desarrollar esquemas para que en el diseño y desarrollo de espacios públicos se consideren las necesidades de las niñas, niños y adolescentes, así como incorporarlos en el cuidado de los recursos naturales.',
				3 => 'Fomentar el desarrollo de infraestructura resiliente en las ciudades y localidades.',
				4 => 'Implementar un programa de desarrollo urbano y territorial equilibrado, en armonía con el entorno natural y el ordenamiento sustentable, sostenible y resiliente del territorio estatal, que permita transitar hacia un modelo de generación de áreas urbanas y rurales de escala humana.',
				5 => 'Mejorar vías de comunicaciones transitables que permitan dentro de localidades, municipios y regiones, la movilidad segura de personas, bienes y mercancías.',
				6 => 'Replantear la política pública de la inclusión de la tecnología e innovación en el sector de la obra pública y en los procesos internos de la Dependencia.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9ad0454a28',
			'label' => 'Objetivo general del PED',
			'name' => 'ob_gral_l',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '13',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'CTI3. Consolidar una sociedad del conocimiento mediante la creación de capacidades científicas y tecnológicas que promueva un desarrollo integral y equilibrado de todas las regiones y sectores del estado.',
				2 => 'G3. Impulsar la igualdad sustantiva entre mujeres y hombres en los ámbitos educativo, cultural, laboral, económico, político y social, para mejorar las condiciones de vida de las mujeres y cerrar las brechas de género que limitan el desarrollo del estado.',
				3 => 'NNA3. Contribuir al cumplimiento del derecho a la salud, así como prevenir, atender y sancionar la violencia hacia la niñez y la adolescencia.',
				4 => 'O.G. 3.3.1 Garantizar el Abasto de Medicamento en el sector salud en el estado de Hidalgo.',
				5 => 'O.G. 3.3.2 Incrementar la Calidad en el sector salud en el estado de Hidalgo',
				6 => 'O.G.3.3.3 Fortalecer la Salud Integral al Adulto Mayor.',
				7 => 'O.G. 3.3.4	Incrementar las brigadas médicas en el estado de Hidalgo.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9ad9954a29',
			'label' => 'Objetivo sectorial y especial',
			'name' => 'ob_s_e_l',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '13',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Mejorar el proceso de adquisición y abasto en insumos médicos',
				2 => 'Fortalecer el Sistema Estatal Sanitario.',
				3 => 'Mejorar los procesos y fortalecer los recursos en salud para la operación óptima, ofreciendo	servicios de atención médica con calidad y calidez.',
				4 => 'Reducir la mortalidad materna.',
				5 => 'Atención Prioritaria a padecimientos relacionados con el rezago.',
				6 => 'Otorgar atención integral a los diferentes grupos de edad, con base en acciones de salud dirigidas a la persona y a la comunidad.',
				7 => 'Fomentar la participación comunitaria en la promoción y prevención de enfermedades.',
				8 => 'Instrumentar los mecanismos para garantizar el acceso efectivo a los servicios de salud',
				9 => 'Contribuir a la identificación y atención de los determinantes en salud de los adultos mayores.',
				10 => 'Fortalecer la oferta	de servicios de atención a la salud mediante unidades médicas móviles a la población que carece o tiene dificultad para acceder a ellos.',
				11 => 'Elevar la calidad de los servicios de salud en las instituciones del sector al instrumentar modelos de atención basados en estudios científicos, inclusión de tecnología e innovación.',
				12 => 'Fortalecer las acciones de detección y atención a la salud, que permitan mejorar las condiciones de vida de las mujeres.',
				13 => 'Fortalecer y promover las acciones de los programas enfocados a la atención de la salud de las niñas, niños y adolescentes dirigidas a reducir la morbilidad y mortalidad en el Estado para contribuir en su desarrollo pleno e integral.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9ae3754a2a',
			'label' => 'Objetivo general del PED',
			'name' => 'ob_gral_m',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '14',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'O.G 4.3.1 Instrumentar una política pública, de enfoque cercano, multidisciplinario e integral, que conforme un trabajo coordinado y participativo entre autoridades y sociedad; que disminuya e inhiba los factores de riesgo y fortalezca el tejido social en el marco de la legalidad, mejorando así, la calidad de vida de la población hidalguense y la optimización de las funciones de las instituciones de seguridad.',
				2 => 'O.G 4.3.2 Desarrollar una reingeniería de las instituciones policiales con un enfoque humanístico y social que permita dignificar a las instancias de seguridad pública, promoviendo en sus integrantes un sentido de identidad, vocación y pertenencia en beneficio de la sociedad.',
				3 => 'O.G 4.3.3 Desarrollar, fortalecer y modernizar los esquemas de regionalización tendientes a la unificación de la estrategia de seguridad en todo el estado, que permita el intercambio sistematizado y eficiente de la información para la seguridad pública; así como de todos los procesos administrativos, operativos y estratégicos que habrán de utilizar las instituciones de seguridad pública con la finalidad de brindar una atención inmediata, oportuna y eficiente a la población hidalguense.',
				4 => 'O.G. 4.3.4 Brindar a la ciudadanía del estado de Hidalgo, una policía coordinada con elementos confiables, profesionales, respetuosos de los derechos humanos y de la igualdad entre hombres y mujeres.',
				5 => 'O.G 4.5.1 Establecer las normas que deberán observarse durante el internamiento por prisión preventiva y en la ejecución de penas, como medios para lograr la reinserción social.',
				6 => 'O. G 4.1 Prevenir, atender, sancionar y erradicar la violencia contra las mujeres y niñas y garantizar su acceso a la justicia.',
				7 => 'O.G CTI4. Generar las condiciones de paz, seguridad y gobernabilidad necesarias para el desarrollo económico y social a través de la investigación científica, el desarrollo tecnológico y la innovación.',
				8 => 'O.G. NNA4. Contribuir a que niñas, niños y adolescentes sean protegidos contra todo tipo de maltrato o violencia tanto en el ámbito familiar y social.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9aeb854a2b',
			'label' => 'Objetivo sectorial y especial',
			'name' => 'ob_s_e_m',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '14',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Capacitar en materia de igualdad y violencia de género a las y los integrantes de las instituciones de seguridad pública estatal y municipal, con el propósito de desarrollar en ellos la sensibilidad, aptitud y actitud para prevenir, atender, sancionar y erradicar la violencia ejercida en contra de las mujeres.',
				2 => 'Consolidar una política pública de seguridad	integral y multidisciplinaria entre la sociedad y Gobierno que permita disminuir e inhibir actos de violencia y delincuencia, mejorando así, la confianza de la ciudadanía para con sus autoridades y la calidad de vida de la sociedad hidalguense.',
				3 => 'Desarrollar plataformas tecnológicas de información policial y Georeferenciación	de los diferentes órdenes de gobierno que faciliten la toma de decisiones a través de los sistemas de inteligencia para la generación de estrategias oportunas y eficientes en la prevención y combate al delito, con una perspectiva de género y estricto respeto a los Derechos Humanos.',
				4 => 'Fortalecer las capacidades institucionales en materia de seguridad y prevención social de la violencia y la delincuencia, a través de una coordinación eficiente que garantice el respeto,	promoción y protección de los derechos de niñas, niños y adolescentes.',
				5 => 'Ampliar y aprovechar las tecnologías de la comunicación en el	intercambio, acopio y procesamiento de datos que, permitan generar la inteligencia policial necesaria en la planeación, ejecución y evaluación de programas y operativos en materia de prevención y	combate al delito.',
				'4. Coordinar todas las áreas responsables de la Seguridad del Estado de Hidalgo, donde la información de los eventos que ocurren en el Estado, durante por situaciones de emergencia, combate al delito, respuesta de emergencias, atenciones médicas y catástrofes naturales, serán analizadas sistemáticamente por un equipo de trabajo que cuente con todas las herramientas tecnológicas necesarias, para caracterizar la situación de una o más poblaciones.' => '4. Coordinar todas las áreas responsables de la Seguridad del Estado de Hidalgo, donde la información de los eventos que ocurren en el Estado, durante por situaciones de emergencia, combate al delito, respuesta de emergencias, atenciones médicas y catástrofes naturales, serán analizadas sistemáticamente por un equipo de trabajo que cuente con todas las herramientas tecnológicas necesarias, para caracterizar la situación de una o más poblaciones.',
				6 => 'Realizar procesos permanentes de profesionalización y capacitación, evaluación y control de confianza a las y los integrantes de las instituciones	 policiales, de Procuración de Justicia, del Sistema Penitenciario,	empresas de seguridad privada	y dependencias encargadas de la Seguridad Pública del ámbito estatal y municipal; logrando con ello,	dignificar la labor policial y el mejoramiento de la percepción	ciudadana para con sus autoridades, que fomente la participación corresponsable de la población hidalguense.',
				7 => 'Ofrecer un servicio de calidad a la ciudadanía en el que pueda reportar incidentes que pongan en riesgo su integridad física, su vida, su patrimonio y la paz social; así como la integración operativa de las instituciones estatales de seguridad, impartición de justicia, prevención del delito, reinserción social, protección civil, emergencias, atenciones médicas, Derechos Humanos y de seguridad en todo el estado de Hidalgo.',
				8 => 'Lograr la unificación de los cuerpos policiales del estado de Hidalgo por medio de la coordinación de las funciones operativas de seguridad pública en el territorio municipal, de conformidad con los protocolos emitidos por el Secretariado Ejecutivo del Sistema Nacional de Seguridad Pública, fomentando un actuar con estricto respeto al marco jurídico y a los Derechos Humanos.',
				9 => 'Impulsar la reinserción social efectiva de las personas privadas de la libertad en los centros penitenciarios, privilegiando que las personas sentenciadas cumplan con la pena impuesta en términos del marco constitucional, así como la aplicación de la medida cautelar de prisión preventiva para imputados, procurando en todo momento la igualdad de género y el respeto a los derechos humanos. Asimismo, garantizar que los adolescentes en conflictos con la ley desarrollen las	 habilidades y capacidades necesarias que les permitan alcanzar su reintegración social y familiar.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9af5e54a2c',
			'label' => 'Objetivo general del PED',
			'name' => 'ob_gral_n',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '15',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'CTI2. Consolidar una economía sólida, dinámica y diversa basada en la ciencia, tecnología e innovación que genere desarrollo sostenible en los sectores y actividades productivas del estado y contribuya al bienestar social y económico de la población hidalguense."',
				2 => 'G2. Garantizar la inclusión y participación de mujeres y hombres en igualdad de condiciones en el desarrollo económico del estado."',
				3 => 'NNA2. Contribuir al desaliento y erradicación del trabajo infantil que interfiere en la educación y pleno desarrollo físico, mental y emocional de niñas, niños y adolescentes."',
				4 => 'O.G. 2.4.1. Promover la industria turística hidalguense para posicionar a Hidalgo como un Estado con oferta turística diversificada y sostenible.',
				5 => 'O.G. 2.4.2 Contribuir a la consolidación de los servicios turísticos existentes y elevar la productividad de los prestadores de servicios.',
				6 => 'O.G. 2.4.3 Incentivar la prestación de servicios turísticos en el Estado con esquemas de sostenibilidad.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9afe654a2d',
			'label' => 'Objetivo sectorial y especial',
			'name' => 'ob_s_e_n',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '15',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Desarrollar acciones que garanticen el bienestar de la niñez y adolescencia, con pleno respeto a sus derechos e impulsando la participación de los principales actores en el sector.',
				2 => 'Fomentar el uso de nuevas tecnologías en la prestación del servicio, que impulsen la actividad económica del sector turismo.',
				3 => 'Impulsar el mejoramiento a la calidad de vida a través del acceso a los servicios turísticos de manera equitativa entre hombre y mujeres.',
				4 => 'Aumentar la Promoción y difusión del Estado de Hidalgo como destino turístico.',
				5 => 'Impulsar la calidad de los servicios turísticos que se presten en el Estado de Hidalgo.',
				6 => 'Contar con una base de datos sobre los prestadores de servicios turísticos a fin de conocer el mercado, impulsar su profesionalización y sistematizar la información para la generación de políticas públicas que permitan el aprovechamiento de potencial turístico de la Entidad.',
				7 => 'Operar eventos especiales para impulsar el desarrollo turístico de Hidalgo, promoviendo actividades en los recintos propiedad del Estado.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9b06b54a2e',
			'label' => 'Objetivo general del PED',
			'name' => 'ob_gral_o',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '16',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'O.G 2.1.1 Facilitar la generación de más y mejores empleos formales y oportunidades de trabajo digno o decente, para los hidalguenses, especialmente para aquellas personas en situación de vulnerabilidad.',
				2 => 'O.G 4.2.2 Garantizar el respeto de los derechos laborales y la igualdad de género en la justicia laboral.',
				3 => 'O.G G2. Garantizar la inclusión y participación de mujeres y hombres en igualdad de condiciones en el desarrollo económico del estado.',
				4 => 'O.G NNA2. Contribuir al desaliento y erradicación del trabajo infantil que interfiere en la educación y pleno desarrollo físico, mental y emocional de niñas, niños y adolescentes.',
				5 => 'O. G CTI2. Consolidar una economía sólida, dinámica y diversa basada en la ciencia, tecnología e innovación que genere desarrollo sostenible en los sectores y actividades productivas del estado y contribuya al bienestar social y económico de la población hidalguense.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9b0e154a2f',
			'label' => 'Objetivo sectorial y especial',
			'name' => 'ob_s_e_o',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '16',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Contribuir a mejorar las oportunidades de ocupación de la población en condición de desempleo o subocupación mediante eficientes mecanismos de vinculación laboral.',
				2 => 'Impulsar la competitividad y la productividad en el Estado a través de la formación y profesionalización del capital humano en congruencia a las vocaciones regionales, lo que aumente sus habilidades y conocimientos laborales.',
				3 => 'Fortalecer las capacidades de los trabajadores a efecto de aumentar su productividad, y con ello, elevar la competitividad en las empresas de la entidad.',
				4 => 'Vigilar la observancia y aplicación de las disposiciones en materia laboral, para constatar el cumplimiento de las obligaciones de las y los patrones en las empresas del ámbito local.',
				5 => 'Asesorar y representar a los trabajadores, así como proponer a las partes interesadas soluciones amistosas para el arreglo de sus conflictos, que permita solucionar con rapidez sus diferencias, antes de llegar a juicio.',
				6 => 'Agilizar la impartición de justicia laboral a través de la profesionalización y modernización, de forma igualitaria, transparente y con calidad, a los conflictos de trabajo que son sometidos, y se suscitan tanto en el sector público como en el privado.',
				7 => 'Fortalecer la política laboral para que las mujeres hidalguenses puedan acceder a mejores condiciones laborales, logrando alcanzar su autonomía económica en un ambiente de paz y tranquilidad laboral.',
				8 => 'Contribuir a la prevención y erradicación del trabajo infantil en las empresas del ámbito local.',
				9 => 'Fomentar el desarrollo científico, tecnológico e innovación; dentro de los programas del sector laboral en beneficio de la población hidalguense.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9b15254a30',
			'label' => 'Objetivo general del PED',
			'name' => 'ob_gral_p',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '17',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'G1. Promover la participación ciudadana en términos igualitarios entre mujeres y hombres, en la observación y vigilancia de los mecanismos de gestión gubernamental en los temas de transparencia y rendición de cuentas, de manera corresponsable ciudadanía-Gobierno',
				2 => 'O.G 1.2.1 Optimizar los mecanismos de vinculación institucional y social para la planeación, control y evaluación de los programas y acciones de la administración pública, promoviendo la participación ciudadana.',
				3 => 'O.G 1.3.2 Fortalecer la infraestructura tecnológica y de las comunicaciones',
				4 => 'O.G	1.7.1 Hacer eficiente la programación y aplicación de los recursos financieros en proyectos de gasto, que arrojen resultados efectivos, priorizando aquellos que aporten mayor impacto al bienestar de la población del Estado de Hidalgo.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9b1b154a31',
			'label' => 'Objetivo sectorial y especial',
			'name' => 'ob_s_e_p',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '17',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Obj1 Determinar las estrategias y mecanismos comunicacionales que permitan transmitir a la población el quehacer institucional e información relevante que contribuya al desarrollo del Estado.',
				2 => 'Obj2 Promover y coordinar las acciones de modernización e innovación gubernamental, así como la aplicación y uso de las tecnologías de información y comunicaciones, que contribuyan al seguimiento de los asuntos gubernamentales.',
				3 => 'Obj3 Garantizar que la política pública estatal, en materia de Comunicación Social, acerque a la ciudadanía, con mensajes unificados en imagen institucional,	las acciones y programas que el Gobierno Estatal lleva a cabo, para elevar la calidad de vida de todos los segmentos sociales.',
				4 => 'Ob4 Fomentar entre las dependencias y entidades de la Administración Pública el ahorro de recursos, así como el apego a la ética, honradez y a la legalidad en las acciones de los servidores públicos en el ejercicio de su función.',
				5 => 'Promover que las políticas transversales y elementos de sostenibilidad sean aplicados en las políticas públicas del Estado de Hidalgo',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9b39d54a32',
			'label' => 'Objetivo general del PED',
			'name' => 'ob_gral_q',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '18',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'CTI5. Promover el desarrollo sostenible mediante la generación y aprovechamiento de conocimiento para el uso adecuado y responsable de los recursos naturales disponibles en el estado.',
				2 => 'NNA5. Incentivar la generación de espacios con diseños sostenibles al que puedan acceder y participar activamente niñas, niños y adolescentes, para conservar el ambiente del futuro.',
				3 => 'O.G 5.1.4 Garantizar el manejo sostenible, a través del aprovechamiento, valorización y disposición final de los residuos sólidos urbanos y de manejo especial',
				4 => 'O.G 5.3.1 Asegurar la cobertura estatal, regional y municipal, de los instrumentos de planeación ecológica territorial',
				5 => 'O.G 5.5.1 Asegurar el equilibrio ambiental en el ámbito forestal, mediante la conservación y restauración, incorporando esquemas de aprovechamiento sostenible.',
				6 => 'O.G 5.5.4 Fortalecer la implementación de los instrumentos de gestión de la calidad del aire en la entidad',
				7 => 'O.G. 5.2.1	Fomentar una cultura del cuidado ambiental sólida en todos los niveles educativos y sociales, introduciendo conceptos y prácticas del desarrollo sostenible en la vida diaria de las personas',
				8 => 'O.G. 5.2.2 Contar con un marco regulatorio y legal robusto, que permita, mediante su aplicación, transitar hacia mejores prácticas ambientales',
				9 => 'O.G. 5.6.2 Propiciar el desarrollo de Proyectos estratégicos de orden regional y micro regional de impacto en el desarrollo económico, social y ambiental de la entidad.',
				10 => 'O.G.5.5.4 Fortalecer la implementación de los instrumentos de gestión de la calidad del aire en la entidad.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9b46654a33',
			'label' => 'Objetivo sectorial y especial',
			'name' => 'ob_s_e_q',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '18',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Regular la contaminación ambiental generada por las actividades y servicios que afectan la calidad del aire, agua y suelo del Estado de Hidalgo mediante la aplicación de la normatividad vigente en la materia.',
				2 => 'Desarrollar e implementar estrategias, criterios y lineamientos en materia de Ordenamiento Ecológico Territorial con la finalidad de proteger, conservar y restaurar el patrimonio natural, que sirvan como base para otros instrumentos de planeación del territorio que permitan mejorar la calidad de vida de los hidalguenses.',
				3 => 'Fortalecer el desarrollo sustentable en Hidalgo a través de investigaciones y desarrollo de nuevas tecnologías, que respondan a las necesidades de los proyectos prioritarios en materia ambiental del Estado, concretadas por intervención de la Secretaría de Medio Ambiente y Recursos Naturales',
				4 => 'Garantizar la protección de los recursos naturales y el capital natural privilegiando el enfoque preventivo sobre el correctivo, así como las acciones de participación social. Todo esto mediante la aplicación y cumplimiento efectivo, eficiente, expedito y transparente de la legislación ambiental federal y estatal vigente a través de la atención a la denuncia popular y mediante acciones de inspección, verificación, vigilancia y uso de instrumentos voluntarios.',
				5 => 'Generar instrumentos y programas de desarrollo sostenible aplicando ecotecnologías, programas de cultura y formación ambiental para incidir positivamente ante los efectos del cambio climático.',
				6 => 'Impulsar la igualdad entre hombres y mujeres en las acciones y programas que realiza la Secretaría de Medio Ambiente y Recursos Naturales',
				7 => 'Promover la participación de Niñas, Niños y Adolescentes en actividades de formación ambiental, a través de diferentes acciones culturales y de esparcimiento, las cuales favorezcan su desarrollo y crecimiento.',
				8 => 'Proteger, conservar, restaurar y aprovechar sustentablemente la biodiversidad, los recursos naturales y forestales, con el fin de asegurar la preservación del patrimonio natural del Estado a mediano y largo plazo',
				9 => 'Regular la contaminación ambiental generada por las actividades y servicios que afectan la calidad del aire, agua y suelo del estado de Hidalgo, mediante la aplicación de la normatividad vigente en la materia.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9b4d354a34',
			'label' => 'Objetivo general del PED',
			'name' => 'ob_gral_r',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '19',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'O.G.3.3.2 Incrementar la calidad en el sector salud en el estado de Hidalgo.',
				2 => 'O.G. 3.1.2 Asegurar la inclusión social',
				3 => 'O.G. 3.1.4 Reducir la pobreza moderada en todas sus dimensiones',
				4 => 'O.G 3.2.4 Ofrecer educación incluyente y equitativa en todos los niveles y modalidades.',
				5 => 'G3. Impulsar la igualdad sustantiva entre mujeres y hombres en los ámbitos educativo, cultural, laboral, económico, político y social, para mejorar las condiciones de vida de las mujeres y cerrar las brechas de género que limitan el desarrollo del estado.',
				6 => 'CTI3. Consolidar una sociedad de conocimiento mediante la creación de capacidades científicas y tecnológicas que promueva un desarrollo integral y equilibrado de todas las regiones y sectores del estado.',
				7 => 'NNA3. Contribuir al cumplimiento del derecho a la salud, así como prevenir, atender y sancionar la violencia a la niñez y la adolescencia.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9b8a90d4ab',
			'label' => 'Objetivo sectorial y especial',
			'name' => 'ob_s_e_r',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '19',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Contribuir en la ampliación de la oferta educativa para grupos vulnerables mediante la implementación de acciones que proporcionen educación formal que les permita superar su condición de desventaja social.',
				2 => 'Contribuir en la disminución de la carencia alimentaria de la población hidalguense bajo el enfoque de focalización de grupos prioritarios mediante la operación de programas alimentarios	específicos acorde con sus necesidades nutricias.',
				3 => 'Impulsar acciones preventivas y de intervención que contribuyan a garantizar la salud, especialmente por grupos de edad y discapacidad',
				4 => 'Incorporar la perspectiva de género en los programas en materia de asistencia social que se realizan en Hidalgo, a fin de disminuir la brecha de desigualdad entre hombres y mujeres',
				5 => 'Incorporar la tecnología en los programas en materia de asistencia social que se realizan en Hidalgo.',
				6 => 'Mejorar la calidad y espacios en la vivienda de población identificada como prioritaria mediante la implementación de acciones de desarrollo familiar y comunitario.',
				7 => 'Promover acciones que contribuyan a garantizar los derechos de niñas, niños y adolescentes bajo la perspectiva de salvaguardar su integridad y potencializar su desarrollo individual, familiar y social.',
				8 => 'Proporcionar bienes y servicios que coadyuven a dar soluciones prioritarias a problemáticas emergentes que presenta la población hidalguense con dos o más condiciones de vulnerabilidad en materia familiar y comunitaria',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9b9100d4ac',
			'label' => 'Objetivo general del PED',
			'name' => 'ob_gral_s',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '20',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'CTI1. Incrementar la efectividad, cercanía y transparencia en la gestión gubernamental mediante el desarrollo de conocimiento, tecnología e innovaciones que contribuyan a la digitalización del Gobierno Estatal y los municipales."',
				2 => 'G1. Promover la participación ciudadana en términos igualitarios entre mujeres y hombres, en la observación y vigilancia de los mecanismos de gestión gubernamental en los temas de transparencia y rendición de cuentas, de manera corresponsable ciudadanía-Gobierno',
				3 => 'NNA1. Contribuir al derecho de identidad de niñas, niños y adolescentes que',
				'garantice su pleno acceso a los servicios del Estado."' => 'garantice su pleno acceso a los servicios del Estado."',
				4 => 'O.G 1.2.1 Optimizar los mecanismos de vinculación institucional y social para la planeación, control y evaluación de los programas y acciones de la administración pública, promoviendo la participación ciudadana.',
				5 => 'O.G 1.2.4 Normar y coordinar la capacitación, integración, actualización y producción de la información sobre diversos aspectos de la realidad demográfica, social y económica del territorio estatal y municipal.',
				6 => 'O.G 1.7.1 Hacer eficiente la programación y aplicación de lo-s recursos financieros en proyectos de gasto que arrojen resultados efectivos, priorizando aquellos que aporten mayor impacto al bienestar de la población del estado de Hidalgo.',
				7 => 'O.G 5.3.3 Garantizar un desarrollo urbano sostenible y equilibrado, en armonía con el entorno natural.',
				8 => 'O.G 5.6.1 Impulsar políticas públicas de orden territorial con impacto en la sostenibilidad, el medio físico, la productividad e inclusión social de las zonas geográficas que conforman la entidad.',
				9 => 'O.G. 1.2.5 Consolidar la operación del sistema estatal de planeación en la conducción y coordinación de los procesos institucionales de las dependencias y organismos del Gobierno del Estado, así como en la concertación con los sectores social y privado.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9b9c20d4ad',
			'label' => 'Objetivo sectorial y especial',
			'name' => 'ob_s_e_s',
			'type' => 'select',
			'instructions' => 'Selecciona el objetivo que este vinculado al evento',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c55c9d19392',
						'operator' => '==',
						'value' => '20',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'OTNNA.	Garantizar que los instrumentos de planeación contengan planteamientos estratégicos orientados al desarrollo y protección de niños, niñas y adolescentes',
				2 => 'OTG.	Garantizar que los instrumentos de planeación contengan planteamientos estratégicos orientados a la disminución de las desigualdades entre hombres y mujeres.',
				3 => 'OTCTI.	Garantizar que los instrumentos de planeación contengan planteamientos estratégicos orientados a la incorporación de la ciencia, la tecnología y la innovación',
				4 => 'Implementar mecanismos e instrumentos de planeación territorial estratégica que favorezcan el desarrollo integral y equilibrado del estado, así como la reducción en la brecha de desigualdad social y económica con apego a la preservación del ambiente.',
				5 => 'Fortalecer el proceso de planeación y promoción del desarrollo integral	de las zonas metropolitanas del estado, favoreciendo la coordinación y concertación interinstitucional con programas, proyectos y acciones de gran impacto.',
				6 => 'Instrumentar los mecanismos institucionales en materia de evaluación y monitoreo para llevar a cabo la verificación, medición, detección y corrección de desviaciones de carácter cualitativo y cuantitativo acerca del avance y cumplimiento de los objetivos y metas de los Planes y Programas, aportando los resultados necesarios para la toma de decisiones.',
				7 => 'Normar y coordinar la captación, integración, actualización y producción de la información sobre diversos aspectos de la realidad demográfica, social y económica del territorio estatal y municipal, como apoyo a las diferentes etapas del proceso de Planeación del Desarrollo de la Entidad.',
				8 => 'Fortalecer los mecanismos e instrumentos de planeación democrática y participativa en las diferentes etapas del proceso de programación, presupuestación, evaluación y control de las instituciones públicas de la administración estatal y municipal.',
				9 => 'Instrumentar los mecanismos institucionales en materia de evaluación y monitoreo para llevar a cabo la verificación, medición, detección y corrección de desviaciones de carácter cualitativo y cuantitativo acerca del avance y cumplimiento de los objetivos y metas de los Planes y Programas, aportando los resultados necesarios para la toma de decisiones.',
				10 => 'Impulsar acciones para aprovechar de manera eficiente las tecnologías de la información y comunicación para mejorar la accesibilidad en la provisión de trámites y servicios a la ciudadanía.',
				11 => 'Garantizar la generación continua de insumos para la innovación y mejora en la normatividad de los procesos de planeación, bajo un esquema que propicie la cooperación interinstitucional en favor del desarrollo para el estado de Hidalgo.',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c60b63830e4',
			'label' => '¿El evento implica una vinculación con la agenda legislativa?',
			'name' => 'vinc_leg',
			'type' => 'radio',
			'instructions' => 'Selecciona la opción adecuada',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Sí',
				'0.5' => 'No',
			),
			'allow_null' => 0,
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => '',
			'layout' => 'vertical',
			'return_format' => 'label',
		),
		array (
			'key' => 'field_59c60bb9f409a',
			'label' => '¿El evento implica una vinculación con la agenda municipalista?',
			'name' => 'vinc_munic',
			'type' => 'radio',
			'instructions' => 'Selecciona la opción adecuada',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Sí',
				'0.5' => 'No',
			),
			'allow_null' => 0,
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => '',
			'layout' => 'vertical',
			'return_format' => 'label',
		),
		array (
			'key' => 'field_59c60bd788dd3',
			'label' => 'Impacto del evento',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_59c60c00cfa2e',
			'label' => '¿Cuál es el propósito del evento?',
			'name' => 'prop_ev',
			'type' => 'radio',
			'instructions' => 'Selecciona la opción adecuada',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Presentar un programa o política pública',
				'0.67' => 'Presentar avances de un programa',
				'0.89' => 'Firmar un acuerdo o convenio',
				'0.3' => 'Dar un anuncio',
				'0.56' => 'Iniciar una campaña',
				'0.78' => 'Inauguración de una obra (hospital, escuela, carretera)',
			),
			'allow_null' => 0,
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => '',
			'layout' => 'vertical',
			'return_format' => 'value',
		),
		array (
			'key' => 'field_59c60c8e9a9f5',
			'label' => 'Tipo de evento',
			'name' => 'tip_ev',
			'type' => 'radio',
			'instructions' => 'Selecciona el tipo de evento',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'0.8' => 'Informativo',
				'0.81' => 'Participación',
				1 => 'Informativo y participación',
			),
			'allow_null' => 0,
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => '',
			'layout' => 'vertical',
			'return_format' => 'value',
		),
		array (
			'key' => 'field_59c60cd7a0ead',
			'label' => 'Objetivo del evento',
			'name' => 'obj_ev',
			'type' => 'text',
			'instructions' => 'Describe de manera sintetizada el objetivo del evento',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => 200,
		),
		array (
			'key' => 'field_59c60d24bf3a0',
			'label' => 'Población estimada a impactar',
			'name' => 'pob_imp',
			'type' => 'number',
			'instructions' => 'Anota la cantidad de población que estimas estará interesada en el tema central del evento',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
		),
		array (
			'key' => 'field_59c60d99bf65d',
			'label' => '¿Ha excedido de la población hidalguense? (2,900,000 habitantes)?',
			'name' => 'pob_ex_s',
			'type' => 'radio',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Sí',
				2 => 'No',
			),
			'allow_null' => 0,
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => '',
			'layout' => 'vertical',
			'return_format' => 'value',
		),
		array (
			'key' => 'field_59c928bd9e2cc',
			'label' => '¿A qué se debe que el evento impacte a un número mayor de la población hidalguense?',
			'name' => 'may_pob_hgo',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c60d99bf65d',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => 150,
		),
		array (
			'key' => 'field_59c9685dd00c6',
			'label' => 'Análisis Costo- Beneficio',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_59c92f00a8d9a',
			'label' => 'Cantidad de población que asiste al evento (aforo)',
			'name' => 'pob_afor',
			'type' => 'number',
			'instructions' => 'Anota la cantidad de población que asistirá al evento',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
		),
		array (
			'key' => 'field_59c92fc1e4f09',
			'label' => '¿Qué tipo de públicos asisten al evento?',
			'name' => 'tip_pub',
			'type' => 'checkbox',
			'instructions' => 'Selecciona las opciones necesarias',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Niños y adolescentes',
				2 => 'Mujeres',
				3 => 'Indígenas',
				4 => 'Funcionarios federales o de otras entidades',
				5 => 'Funcionarios locales o municipales',
				6 => 'Población en general',
				7 => 'Otros	¿quiénes?',
			),
			'allow_custom' => 0,
			'save_custom' => 0,
			'default_value' => array (
			),
			'layout' => 'vertical',
			'toggle' => 0,
			'return_format' => 'value',
		),
		array (
			'key' => 'field_59c930696eb64',
			'label' => '¿Cuál es el otro tipo de público?',
			'name' => 'pub_otro',
			'type' => 'text',
			'instructions' => 'Describe el otro tipo de público que debe asistir y que no esta considerado en la opción anterior',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c92fc1e4f09',
						'operator' => '==',
						'value' => '7',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => 150,
		),
		array (
			'key' => 'field_59c930e4ca602',
			'label' => '¿Por qué necesariamente tienen que asistir?',
			'name' => 'mot_asis',
			'type' => 'select',
			'instructions' => 'NIÑOS Y ADOLESCENTES
Selecciona la opción adecuada',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c92fc1e4f09',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Son beneficiarios en el evento',
				'0.7' => 'Son	participantes del evento',
				'0.5' => 'Son invitados especiales',
				'0.3' => 'Otro ¿cuál?',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c934b5be1b5',
			'label' => '¿Por qué necesariamente tienen que asistir?',
			'name' => 'mot_asist_g',
			'type' => 'text',
			'instructions' => 'OTROS
Describe el motivo de asistencia que no esta considerado en la opción anterior',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c930e4ca602',
						'operator' => '==',
						'value' => '0.3',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => 150,
		),
		array (
			'key' => 'field_59c9315a86503',
			'label' => '¿Por qué necesariamente tienen que asistir?',
			'name' => 'mot_asis_a',
			'type' => 'select',
			'instructions' => 'MUJERES
Selecciona la opción adecuada',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c92fc1e4f09',
						'operator' => '==',
						'value' => '2',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Son beneficiarios en el evento',
				'0.7' => 'Son	participantes del evento',
				'0.5' => 'Son invitados especiales',
				'0.3' => 'Otro ¿cuál?',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c931b8b7753',
			'label' => '¿Por qué necesariamente tienen que asistir?',
			'name' => 'mot_asis_b',
			'type' => 'select',
			'instructions' => 'INDÍGENAS
Selecciona la opción adecuada',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c92fc1e4f09',
						'operator' => '==',
						'value' => '3',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Son beneficiarios en el evento',
				'0.7' => 'Son	participantes del evento',
				'0.5' => 'Son invitados especiales',
				'0.3' => 'Otro ¿cuál?',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c932b2f522d',
			'label' => '¿Por qué necesariamente tienen que asistir?',
			'name' => 'mot_asis_c',
			'type' => 'select',
			'instructions' => 'FUNCIONARIOS FEDERALES O DE OTRAS ENTIDADES
Selecciona la opción adecuada',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c92fc1e4f09',
						'operator' => '==',
						'value' => '4',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Son beneficiarios en el evento',
				'0.7' => 'Son	participantes del evento',
				'0.5' => 'Son invitados especiales',
				'0.3' => 'Otro ¿cuál?',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c933078a927',
			'label' => '¿Por qué necesariamente tienen que asistir?',
			'name' => 'mot_asis_d',
			'type' => 'select',
			'instructions' => 'FUNCIONARIOS LOCALES O MUNICIPALES
Selecciona la opción adecuada',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c92fc1e4f09',
						'operator' => '==',
						'value' => '5',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Son beneficiarios en el evento',
				'0.7' => 'Son	participantes del evento',
				'0.5' => 'Son invitados especiales',
				'0.3' => 'Otro ¿cuál?',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c933945f098',
			'label' => '¿Por qué necesariamente tienen que asistir?',
			'name' => 'mot_asis_e',
			'type' => 'select',
			'instructions' => 'POBLACIÓN EN GENERAL
Selecciona la opción adecuada',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c92fc1e4f09',
						'operator' => '==',
						'value' => '6',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Son beneficiarios en el evento',
				'0.7' => 'Son	participantes del evento',
				'0.5' => 'Son invitados especiales',
				'0.3' => 'Otro ¿cuál?',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9348302d3b',
			'label' => '¿Por qué necesariamente tienen que asistir?',
			'name' => 'mot_asis_f',
			'type' => 'select',
			'instructions' => 'OTROS
Selecciona la opción adecuada',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c92fc1e4f09',
						'operator' => '==',
						'value' => '7',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Son beneficiarios en el evento',
				'0.7' => 'Son	participantes del evento',
				'0.5' => 'Son invitados especiales',
				'0.3' => 'Otro ¿cuál?',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c9c129ead26',
			'label' => '¿Por qué necesariamente tienen que asistir? (otros)',
			'name' => 'mot_asis_f_otros',
			'type' => 'text',
			'instructions' => 'Describe el otro motivo',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c930e4ca602',
						'operator' => '==',
						'value' => '0.3',
					),
				),
				array (
					array (
						'field' => 'field_59c9315a86503',
						'operator' => '==',
						'value' => '0.3',
					),
				),
				array (
					array (
						'field' => 'field_59c931b8b7753',
						'operator' => '==',
						'value' => '0.3',
					),
				),
				array (
					array (
						'field' => 'field_59c932b2f522d',
						'operator' => '==',
						'value' => '0.3',
					),
				),
				array (
					array (
						'field' => 'field_59c933078a927',
						'operator' => '==',
						'value' => '0.3',
					),
				),
				array (
					array (
						'field' => 'field_59c933945f098',
						'operator' => '==',
						'value' => '0.3',
					),
				),
				array (
					array (
						'field' => 'field_59c9348302d3b',
						'operator' => '==',
						'value' => '0.3',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array (
			'key' => 'field_59c9356b0186b',
			'label' => 'Indica los conceptos que generan el gasto directo del evento',
			'name' => 'cost_event',
			'type' => 'checkbox',
			'instructions' => 'a) Incluye renta del espacio, alimentos, publicidad, etc.
b) Anota el concepto, y selecciona un tipo de evidencia',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Renta del espacio y locación',
				2 => 'Papelería',
				3 => 'Difusión y promoción',
				4 => 'Equipamiento técnico, sonido, proyección, decoración, etc.',
				5 => 'Animación del evento',
				6 => 'Honorarios por servicios profesionales',
				7 => 'Otro, ¿cuál?',
				8 => 'Ninguno',
			),
			'allow_custom' => 0,
			'save_custom' => 0,
			'default_value' => array (
			),
			'layout' => 'vertical',
			'toggle' => 0,
			'return_format' => 'value',
		),
		array (
			'key' => 'field_59c938a422735',
			'label' => 'Indica por concepto, ¿cuáles son los costos directos del evento?',
			'name' => 'cost_event_a',
			'type' => 'number',
			'instructions' => 'RENTA DEL ESPACIO Y LOCACIÓN
Anota el costo',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c9356b0186b',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
		),
		array (
			'key' => 'field_59c93aa0b6044',
			'label' => 'Indica por concepto, ¿cuáles son los costos directos del evento?',
			'name' => 'cost_event_b',
			'type' => 'number',
			'instructions' => 'PAPELERÍA
Anota el costo',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c9356b0186b',
						'operator' => '==',
						'value' => '2',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
		),
		array (
			'key' => 'field_59c93ac7ede4c',
			'label' => 'Indica por concepto, ¿cuáles son los costos directos del evento?',
			'name' => 'cost_event_c',
			'type' => 'number',
			'instructions' => 'DIFUSIÓN Y PROMOCIÓN
Anota el costo',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c9356b0186b',
						'operator' => '==',
						'value' => '3',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
		),
		array (
			'key' => 'field_59c93ae9d1f15',
			'label' => 'Indica por concepto, ¿cuáles son los costos directos del evento?',
			'name' => 'cost_event_d',
			'type' => 'number',
			'instructions' => 'EQUIPAMIENTO TÉCNICO, SONIDO, PROYECCIÓN, DECORACIÓN, ETC.
Anota el costo',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c9356b0186b',
						'operator' => '==',
						'value' => '4',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
		),
		array (
			'key' => 'field_59c93b2772062',
			'label' => 'Indica por concepto, ¿cuáles son los costos directos del evento?',
			'name' => 'cost_event_e',
			'type' => 'number',
			'instructions' => 'ANIMACIÓN DEL EVENTO
Anota el costo',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c9356b0186b',
						'operator' => '==',
						'value' => '5',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
		),
		array (
			'key' => 'field_59c93b72e0122',
			'label' => 'Indica por concepto, ¿cuáles son los costos directos del evento?',
			'name' => 'cost_event_f',
			'type' => 'number',
			'instructions' => 'HONORARIOS POR SERVICIOS PROFESIONALES
Anota el costo',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c9356b0186b',
						'operator' => '==',
						'value' => '6',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
		),
		array (
			'key' => 'field_59c93ba54f5e7',
			'label' => 'Indica por concepto, ¿cuáles son los costos directos del evento?',
			'name' => 'cost_event_g',
			'type' => 'number',
			'instructions' => 'OTROS
Anota el costo',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c9356b0186b',
						'operator' => '==',
						'value' => '7',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
		),
		array (
			'key' => 'field_59c93c07f3c2d',
			'label' => 'Indica los conceptos que generan el gasto indirecto del evento',
			'name' => 'cost_event_in',
			'type' => 'checkbox',
			'instructions' => 'a) traslados, viáticos, etc.
b) Anota el concepto, y selecciona un tipo de evidencia',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Traslados',
				2 => 'Alimentos y viáticos',
				3 => 'Otro, ¿cuál?',
				4 => 'Ninguno',
			),
			'allow_custom' => 0,
			'save_custom' => 0,
			'default_value' => array (
			),
			'layout' => 'vertical',
			'toggle' => 0,
			'return_format' => 'value',
		),
		array (
			'key' => 'field_59c93cd5f3c2e',
			'label' => 'Indica por concepto, ¿cuáles son los costos indirectos del evento?',
			'name' => 'cost_ev_ind',
			'type' => 'number',
			'instructions' => 'TRASLADOS
Anota el costo',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c93c07f3c2d',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
		),
		array (
			'key' => 'field_59c93d1cf3c2f',
			'label' => 'Indica por concepto, ¿cuáles son los costos indirectos del evento?',
			'name' => 'cost_ev_ind_a',
			'type' => 'number',
			'instructions' => 'ALIMENTOS Y VIÁTICOS
Anota el costo',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c93c07f3c2d',
						'operator' => '==',
						'value' => '2',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
		),
		array (
			'key' => 'field_59c93d30f3c30',
			'label' => 'Indica por concepto, ¿cuáles son los costos indirectos del evento?',
			'name' => 'cost_ev_ind_b',
			'type' => 'number',
			'instructions' => 'OTRO
Anota el costo',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c93c07f3c2d',
						'operator' => '==',
						'value' => '3',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
		),
		array (
			'key' => 'field_59c93d5d710d3',
			'label' => 'Justificación de evento presencial',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_59c93d7d710d4',
			'label' => '¿Hay algún otro medio para lograr el impacto?',
			'name' => 'me_imp',
			'type' => 'radio',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'0.5' => 'Sí',
				1 => 'No',
			),
			'allow_null' => 0,
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => '',
			'layout' => 'vertical',
			'return_format' => 'value',
		),
		array (
			'key' => 'field_59c93dd21dc8e',
			'label' => '¿Cuáles medios optarías para lograr el impacto?',
			'name' => 'med_alt',
			'type' => 'checkbox',
			'instructions' => 'Selecciona las opciones adecuadas',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c93d7d710d4',
						'operator' => '==',
						'value' => '0.5',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Televisión',
				2 => 'Radio',
				3 => 'Redes sociales',
				4 => 'Periódicos y revistas',
				5 => 'Mobiliario urbano (espectaculares, pendones, etc.)',
				6 => 'Otro ¿cuál?',
			),
			'allow_custom' => 0,
			'save_custom' => 0,
			'default_value' => array (
			),
			'layout' => 'vertical',
			'toggle' => 0,
			'return_format' => 'value',
		),
		array (
			'key' => 'field_59c93e511dc90',
			'label' => '¿Cuáles medios optarías para lograr el impacto?',
			'name' => 'med_alt_a',
			'type' => 'text',
			'instructions' => 'Describe cuáles otros medios podrías emplear',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c93dd21dc8e',
						'operator' => '==',
						'value' => '6',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array (
			'key' => 'field_59c96915c8243',
			'label' => 'Propuesta específica del evento',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_59c9692dc8244',
			'label' => 'Fecha del evento',
			'name' => 'fech_ev',
			'type' => 'date_picker',
			'instructions' => 'Anota la fecha en que se llevará a cabo el evento',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'display_format' => 'd/m/Y',
			'return_format' => 'd/m/Y',
			'first_day' => 1,
		),
		array (
			'key' => 'field_59c96981c8245',
			'label' => 'Horario del evento (inicio)',
			'name' => 'hor_ev',
			'type' => 'time_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'display_format' => 'H:i:s',
			'return_format' => 'H:i:s',
		),
		array (
			'key' => 'field_59c969cec8247',
			'label' => 'Horario del evento (fin)',
			'name' => 'hor_ev_a',
			'type' => 'time_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'display_format' => 'H:i:s',
			'return_format' => 'H:i:s',
		),
		array (
			'key' => 'field_59c98d38763d1',
			'label' => '¿Cómo se llama el espacio donde se realizará el evento?',
			'name' => 'nom_esp',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => 80,
		),
		array (
			'key' => 'field_59c98d5b763d2',
			'label' => '¿Qué tipo de espacio es?',
			'name' => 'tip_esp',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Plaza pública o espacio abierto',
				2 => 'Auditorio o teatro',
				3 => 'Oficina o sala de juntas de la administración pública',
				4 => 'Centros deportivos',
				5 => 'Otro, ¿cuál?',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c98de7763d3',
			'label' => '¿Qué otro tipo de espacio es?',
			'name' => 'otro_esp',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_59c98d5b763d2',
						'operator' => '==',
						'value' => '5',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array (
			'key' => 'field_59c98eef37aba',
			'label' => '¿En qué municipio se realizará el evento?',
			'name' => 'mun_ev',
			'type' => 'select',
			'instructions' => 'Selecciona el municipio adecuado',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => 'Acatlán',
				2 => 'Acaxochitlán',
				3 => 'Actopan',
				4 => 'Agua Blanca de Iturbide',
				5 => 'Ajacuba',
				6 => 'Alfajayucan',
				7 => 'Almoloya',
				8 => 'Apan',
				10 => 'Atitalaquia',
				11 => 'Atlapexco',
				13 => 'Atotonilco de Tula',
				12 => 'Atotonilco el Grande',
				14 => 'Calnali',
				15 => 'Cardonal',
				17 => 'Chapantongo',
				18 => 'Chapulhuacán',
				19 => 'Chilcuautla',
				16 => 'Cuautepec de Hinojosa',
				9 => 'El Arenal',
				20 => 'Eloxochitlán',
				21 => 'Emiliano Zapata',
				22 => 'Epazoyucan',
				23 => 'Francisco I. Madero',
				24 => 'Huasca de Ocampo',
				25 => 'Huautla',
				26 => 'Huazalingo',
				27 => 'Huehuetla',
				28 => 'Huejutla de Reyes',
				29 => 'Huichapan',
				30 => 'Ixmiquilpan',
				31 => 'Jacala de Ledezma',
				32 => 'Jaltocán',
				33 => 'Juárez Hidalgo',
				40 => 'La Misión',
				34 => 'Lolotla',
				35 => 'Metepec',
				37 => 'Metztitlán',
				51 => 'Mineral de la Reforma',
				38 => 'Mineral del Chico',
				39 => 'Mineral del Monte',
				41 => 'Mixquiahuala de Juárez',
				42 => 'Molango de Escamilla',
				43 => 'Nicolás Flores',
				44 => 'Nopala de Villagrán',
				45 => 'Omitlán de Juárez',
				48 => 'Pachuca de Soto',
				47 => 'Pacula',
				49 => 'Pisaflores',
				50 => 'Progreso de Obregón',
				36 => 'San Agustín Metzquititlán',
				52 => 'San Agustín Tlaxiaca',
				53 => 'San Bartolo Tutotepec',
				46 => 'San Felipe Orizatlán',
				54 => 'San Salvador',
				55 => 'Santiago de Anaya',
				56 => 'Santiago Tulantepec de Lugo Guerrero',
				57 => 'Singuilucan',
				58 => 'Tasquillo',
				59 => 'Tecozautla',
				60 => 'Tenango de Doria',
				61 => 'Tepeapulco',
				62 => 'Tepehuacán de Guerrero',
				63 => 'Tepeji del Río de Ocampo',
				64 => 'Tepetitlán',
				65 => 'Tetepango',
				67 => 'Tezontepec de Aldama',
				68 => 'Tianguistengo',
				69 => 'Tizayuca',
				70 => 'Tlahuelilpan',
				71 => 'Tlahuiltepa',
				72 => 'Tlanalapa',
				73 => 'Tlanchinol',
				74 => 'Tlaxcoapan',
				75 => 'Tolcayuca',
				76 => 'Tula de Allende',
				77 => 'Tulancingo de Bravo',
				66 => 'Villa de Tezontepec',
				78 => 'Xochiatipan',
				79 => 'Xochicoatlán',
				80 => 'Yahualica',
				81 => 'Zacualtipán de Ángeles',
				82 => 'Zapotlán de Juárez',
				83 => 'Zempoala',
				84 => 'Zimapán',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'label',
			'placeholder' => '',
		),
		array (
			'key' => 'field_59c990fa37abb',
			'label' => '¿Cuál es el Código Postal?',
			'name' => 'c_p',
			'type' => 'number',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
		),
		array (
			'key' => 'field_59c9911837abc',
			'label' => 'Indica el domicilio del lugar',
			'name' => 'domicilio',
			'type' => 'text',
			'instructions' => 'Anota el domicilio del lugar
Calle, número exterior, número interior, colonia',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array (
			'key' => 'field_59c9914e37abd',
			'label' => 'Ubica el espacio donde se llevará a cabo el evento',
			'name' => 'mapa_g',
			'type' => 'google_map',
			'instructions' => 'Ubica el punto geográfico donde se llevará a cabo el evento',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'center_lat' => '',
			'center_lng' => '',
			'zoom' => '',
			'height' => '',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'eventos',
			),
			//array (
			//	'param' => 'current_user_role',
			//	'operator' => '==',
			//	'value' => 'administrator',
			//),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => array (
		0 => 'the_content',
		1 => 'excerpt',
		2 => 'custom_fields',
		3 => 'discussion',
		4 => 'comments',
		5 => 'revisions',
		6 => 'slug',
		7 => 'author',
		8 => 'format',
		9 => 'page_attributes',
		10 => 'featured_image',
		11 => 'categories',
		12 => 'tags',
		13 => 'send-trackbacks',
	),
	'active' => 1,
	'description' => '',
));

endif;
Agenda estrategica ETH*/

/*Quitar el Google Maps API warning*/
function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyD0il894cCjGAq2J-Mt5AHEYaOIC6tC0zE');
}
add_action('acf/init', 'my_acf_init');



function dcms_insertar_js(){

	if (!is_home()) return;

	wp_register_script('dcms_miscript', get_template_directory_uri(). '/js/script.js', array('jquery'), '1', true );
	wp_enqueue_script('dcms_miscript');

	wp_localize_script('dcms_miscript','dcms_vars',['ajaxurl'=>admin_url('admin-ajax.php')]);
}

//Insertar Javascript js y enviar ruta admin-ajax.php
add_action('wp_enqueue_scripts', 'dcms_insertar_js');
