
<?php get_header(); 
/*
*  Template Name: evaluador_organismo
*/
$pagina=$_GET['page_id'];
?>

<style>
/* The container */
.container {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 22px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
.button2 {
    background-color: white; 
    color: green; 
    /*border: 2px solid #4CAF50;*/
}




/* Hide the browser's default checkbox */
.container input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
}

/* Create a custom checkbox */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: #eee;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
    background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container input:checked ~ .checkmark {
    background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the checkmark when checked */
.container input:checked ~ .checkmark:after {
    display: block;
}

/* Style the checkmark/indicator */
.container .checkmark:after {
    left: 9px;
    top: 5px;
    width: 5px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}

.button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}

.button1 {
    background-color: white; 
    color: black; 
    border: 2px solid #4CAF50;
}


.button5 {
    background-color: white;
    color: black;
    border: 2px solid #555555;
}

.button5:hover {
    background-color: #555555;
    color: white;
}


.btn_down {
    background-color: white;
    border: none;
    color: #4CAF50;
    padding: 12px 30px;
    cursor: pointer;
    font-size: 20px;
}

/* Darker background on mouse-over */
.btn_down:hover {
    color: #555555;
}




</style>

<section id="main-content">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">

        <!--DATOS DEL USUARIO ACTIVO-->
        <div>
            <table>
              <col width=80%>
              <col width=20%>
                <tbody>
                <td></td><td>
                <?php /* Nombre del usuario activo*/ 
                  $all_users = get_users();
                  $user_name;
                  $user_name=$user->display_name;

                      echo 'Bienvenido: ' . $current_user->user_login . '<br />';
                      echo 'Correo: ' . $current_user->user_email . '<br />';
                      $user_name=$current_user->user_login;

                ?>
                 </td>
                 <td> 
                      <?php
                          $user = wp_get_current_user();
                        if ( $user ) :
                            ?>
                            <img src="<?php echo esc_url( get_avatar_url( $user->ID ) ); ?>" />
                        <?php endif; 
                      ?>
                 </td> 
                </tbody>
            </table>
        </div>  
        <!--FIN DATOS DEL USUARIO ACTIVO-->

        <?php while(have_posts()): the_post(); ?>

          <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
          <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
          <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


          <meta name="viewport" content="width=device-width, initial-scale=1">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>






   
          <div class="principal contenedor contacto">
            <main class="contenido-paginas ">
              <div id="list_dependencias_organismos">
                <br>


             

                <?php global $wpdb;
                $evidencias = $wpdb->prefix . 'ctrl_modulos';
                $registros = $wpdb->get_results("SELECT * FROM $evidencias WHERE tipo = 'organismo'" , ARRAY_A);

                    foreach($registros as $registro) 
                    { 
                      $nom_dependencia; if($registro['nombre_dependencia']=="BACEH")
                      {
                        $nom_dependencia="Bachillerato del Estado de Hidalgo (BEH)";
                      }
                      else if($registro['nombre_dependencia']=="COBAEH")
                      {
                        $nom_dependencia="Colegio de Bachilleres del Estado de Hidalgo (COBAEH)";
                      }
                      else if($registro['nombre_dependencia']=="COEPROTEH")
                      {
                        $nom_dependencia="Colegio de Educación Profesional Técnica del Estado de Hidalgo (CONALEP)";
                      }
                      else if($registro['nombre_dependencia']=="UICEH")
                      {
                        $nom_dependencia="Universidad Intercultural del Estado de Hidalgo (UICEH)";
                      }
                      else if($registro['nombre_dependencia']=="ITESHU")
                      {
                        $nom_dependencia="Instituto Tecnológico Superior de Huichapan (ITESHU)";
                      }
                      else if($registro['nombre_dependencia']=="UTECT")
                      {
                        $nom_dependencia="Universidad Tecnológica de Tulancingo (UTEC)";
                      }
                      else if($registro['nombre_dependencia']=="UTMIR")
                      {
                        $nom_dependencia="Universidad Tecnológica de Mineral de la Reforma (UTMIR)";
                      }
                      else if($registro['nombre_dependencia']=="UTMZ")
                      {
                        $nom_dependencia="Universidad Tecnológica Minera de Zimapán (UTMZ)";
                      }
                      else if($registro['nombre_dependencia']=="UTVAM")
                      {
                        $nom_dependencia="Universidad Tecnológica de la Zona Metropolitana del Valle de México (UTVAM)";
                      }
                      else if($registro['nombre_dependencia']=="UTVM")
                      {
                        $nom_dependencia="Universidad Tecnológica del Valle del Mezquital (UTVM)";
                      }
                      else if($registro['nombre_dependencia']=="UTTT")
                      {
                        $nom_dependencia="Universidad Tecnológica de Tula Tepeji (UTTT)";
                      }
                      else if($registro['nombre_dependencia']=="ITESA")
                      {
                        $nom_dependencia="Instituto Tecnológico de Estudios Superiores del Oriente del Estado de Hidalgo (ITESA)";
                      }

                       else if($registro['nombre_dependencia']=="UTSH")
                      {
                        $nom_dependencia="Universidad Tecnológica de la Sierra Hidalguense (UTSH)";
                      }
                      else if($registro['nombre_dependencia']=="ITSOEH")
                      {
                        $nom_dependencia="Instituto Tecnológico Superior del Occidente del Estado de Hidalgo (ITSOEH)";
                      }
                      else if($registro['nombre_dependencia']=="UTHH")
                      {
                        $nom_dependencia="Universidad Tecnológica de la Huasteca Hidalguense (UTHH)";
                      }
                      else if($registro['nombre_dependencia']=="UPH")
                      {
                        $nom_dependencia="Universidad Politécnica de Huejutla (UPH)";
                      }
                      else if($registro['nombre_dependencia']=="UPP")
                      {
                        $nom_dependencia="Universidad Politécnica de Pachuca (UPP)";
                      }
                      else if($registro['nombre_dependencia']=="UPT")
                      {
                        $nom_dependencia="Universidad Politécnica de Tulancingo (UPT)";
                      }

                      else if($registro['nombre_dependencia']=="UPENERGIA")
                      {
                        $nom_dependencia="Universidad Politécnica de la Energía (UPE)";
                      }
                      else if($registro['nombre_dependencia']=="UPFIM")
                      {
                        $nom_dependencia="Universidad Politécnica de Francisco I. Madero (UPFIM)";
                      }
                      else if($registro['nombre_dependencia']=="UPMETRO")
                      {
                        $nom_dependencia="Universidad Politécnica Metropolitana de Hidalgo (UPMH)";
                      }
                      else if($registro['nombre_dependencia']=="CITNOVA")
                      {
                        $nom_dependencia="Citnova";
                      }
                      else if($registro['nombre_dependencia']=="IHEA")
                      {
                        $nom_dependencia="Instituto Hidalguense de Educación para Adultos (IHEA)";
                      }

                      else if($registro['nombre_dependencia']=="CECYTEH")
                      {
                        $nom_dependencia="Colegio de Estudios Científicos y Tecnológicos del Estado de Hidalgo (CECYTEH)";
                      }
                      else if($registro['nombre_dependencia']=="INHIFE")
                      {
                        $nom_dependencia="Instituto Hidalguense de la Infraestructura Física Educativa.";
                      }
                      else if($registro['nombre_dependencia']=="COEH")
                      {
                        $nom_dependencia="El Colegio del Estado de Hidalgo (COLEH)";
                      }
                      else if($registro['nombre_dependencia']=="IHFES")
                      {
                        $nom_dependencia="Instituto Hidalguense de Financiamiento a la Educación Superior (IHFES)";
                      }
                      else if($registro['nombre_dependencia']=="INHIDE")
                      {
                        $nom_dependencia="Instituto Hidalguense del Deporte";
                      }
                      else if($registro['nombre_dependencia']=="RADYTVH")
                      {
                        $nom_dependencia="Radio y Televisión de Hidalgo";
                      }
                      else if($registro['nombre_dependencia']=="ICATHI")
                      {
                        $nom_dependencia="Instituto de Capacitación para el Trabajo del Estado de Hidalgo (ICATHI)";
                      }
                       else if($registro['nombre_dependencia']=="REPSS")
                      {
                        $nom_dependencia="Régimen Estatal de Protección Social en Salud de Hidalgo (REPSSH)";
                      }
                      else if($registro['nombre_dependencia']=="SSH")
                      {
                        $nom_dependencia="Servicios de Salud de Hidalgo";
                      }
                      else if($registro['nombre_dependencia']=="CEN_MAQUIDES")
                      {
                        $nom_dependencia="Centro Estatal de Maquinaria para el Desarrollo (CEMD) ";
                      }
                      else if($registro['nombre_dependencia']=="CEVIVIENDA")
                      {
                        $nom_dependencia="Comisión Estatal de Vivienda (CEVI)";
                      }
                      else if($registro['nombre_dependencia']=="CEAA")
                      {
                        $nom_dependencia="Centro Estatal de Agua y Alcantarillado (CEAA)";
                      }
                      else if($registro['nombre_dependencia']=="CAASIM")
                      {
                        $nom_dependencia="Comisión de Agua,  Alcantarillado y Saneamiento Inter Metropolitano (CAASIM)";
                      }
                      else if($registro['nombre_dependencia']=="CAASVAM")
                      {
                        $nom_dependencia="Comisión  de Agua  y Alcantarillado del Sistema Valle del Mezquital (CAASVAM)";
                      }
                      else if($registro['nombre_dependencia']=="IHM")
                      {
                        $nom_dependencia="Instituto Hidalguense de las Mujeres";
                      }
                      else if($registro['nombre_dependencia']=="IHDM")
                      {
                        $nom_dependencia="Instituto Hidalguense para el Desarrollo Municipal. ";
                      }
                      else if($registro['nombre_dependencia']=="CENJ_MUJERES")
                      {
                        $nom_dependencia="Centro de Justicia para Mujeres del Estado de Hidalgo.";
                      }
                      else if($registro['nombre_dependencia']=="OEEH")
                      {
                        $nom_dependencia="Operadora de Eventos del Estado de Hidalgo. ";
                      }

                      else if($registro['nombre_dependencia']=="COINH")
                      {
                        $nom_dependencia="Corporación Internacional Hidalgo.";
                      }
                      else if($registro['nombre_dependencia']=="IHCE")
                      {
                        $nom_dependencia="Instituto Hidalguense de Competitividad Empresarial.";
                      }
                      else if($registro['nombre_dependencia']=="COFOIN")
                      {
                        $nom_dependencia="Corporación de Fomento de Infraestructura Industrial. ";
                      }
                      else if($registro['nombre_dependencia']=="VALLEPLATA")
                      {
                        $nom_dependencia="Agencia de Desarrollo Valle de Plata.  ";
                      }
                      else if($registro['nombre_dependencia']=="AE_ENERGIA")
                      {
                        $nom_dependencia="Agencia de Estatal de Energía ";
                      }




                      else if($registro['nombre_dependencia']=="STCH")
                      {
                        $nom_dependencia="Sistema de Transporte Convencional de Hidalgo. ";
                      }
                      else if($registro['nombre_dependencia']=="PIBEH")
                      {
                        $nom_dependencia="Policía Industrial Bancaria del Estado de Hidalgo (PIBEH)";
                      }
                      else if($registro['nombre_dependencia']=="IAAMEH")
                      {
                        $nom_dependencia="Instituto para la Atención de las y los Adultos Mayores del Estado de Hidalgo ";
                      }
                      else if($registro['nombre_dependencia']=="CATASTRO")
                      {
                        $nom_dependencia="Instituto Catastral del Estado de Hidalgo";
                      }
                      else if($registro['nombre_dependencia']=="REHILETE")
                      {
                        $nom_dependencia="Museo Interactivo para la Niñez y la Juventud Hidalguense El Rehilete";
                      }


                      else if($registro['nombre_dependencia']=="CITNOVA")
                      {
                        $nom_dependencia="Consejo de Ciencia, Tecnología e Innovación de Hidalgo";
                      }
                      else if($registro['nombre_dependencia']=="CCYCULTURA")
                      {
                        $nom_dependencia="Consejo Rector de Pachuca Ciudad del Conocimiento y la Cultura";
                      }
                      else if($registro['nombre_dependencia']=="CEDSPI")
                      {
                        $nom_dependencia="Comisión Estatal para el Desarrollo Sostenible de los Pueblos Indígenas";
                      }

                      
                    ?>
                         
                     <button class="accordion">
                      <div>
                       
                        <table>
                          <col width=40%>
                         
                          <col width=10%>
                          <col width=10%>
                          <col width=10%>
                          <col width=10%>
                          <col width=10%>
                            <tbody>
                              <tr>
                             <td id="contenido_datos" > <?php echo('
                              <label  style="text-align: center color: #4CAF50; font-size: 20px; font-family: Graphik-Bold" >'.
                              $nom_dependencia.'</label>');?>
                                
                              </td> 
                              <td>
                               
                                <label id="pendientes_<?php echo($registro['nombre_dependencia']);?>" style="text-align: center;  font-size: 9px; color:#28a745; font-family: Graphik-Bold  display: table-cell; vertical-align: middle; height: 0px; "  >ADECUADA</label>

                                <label id="n_adecuada_<?php echo($registro['nombre_dependencia']);?>" style="text-align: center; color: #FFFFFF; font-size: 12px; font-family: Graphik-Bold">0</label>  
                                         
                              </td>
                              
                              <td>
                                
                                <label id="pendientes_<?php echo($registros['nombre_dependencia']);?>" style="text-align: center;  font-size: 9px; color:#ffc107; font-family: Graphik-Bold  display: table-cell; vertical-align: middle; height: 0px;" >CUESTIONABLE</label>

                                <label id="n_cuestionable_<?php echo($registro['nombre_dependencia']);?>" style="text-align: center; color: #FFFFFF; font-size: 12px; font-family: Graphik-Bold">0</label>
                             
                                
                              </td>
                              <td>
                                
                                <label id="pendientes_<?php echo($registros['nombre_dependencia']);?>" style="text-align: center;  font-size: 9px; color:#dc3545; font-family: Graphik-Bold  display: table-cell; vertical-align: middle; height: 0px;" >MUY CUESTIONABLE</label>

                                <label id="n_muy_<?php echo($registro['nombre_dependencia']);?>" style="text-align: center; color: #FFFFFF; font-size: 12px; font-family: Graphik-Bold">0</label>
                                
                                
                              </td>
                              <td>
                                
                                
                                <label id="pendientes_<?php echo($registros['nombre_dependencia']);?>" style="text-align: center;  font-size: 9px; color:#6c757d; font-family: Graphik-Bold  display: table-cell; vertical-align: middle; height: 0px;" >SIN ELEMENTOS</label>

                                <label id="n_sin_<?php echo($registro['nombre_dependencia']);?>" style="text-align: center; color: #FFFFFF; font-size: 12px; font-family: Graphik-Bold">0</label>

                             
                                
                              </td>
                              <td>
                              
                                <label id="pendientes_<?php echo($registros['nombre_dependencia']);?>" style="text-align: center;  font-size: 9px; color:#FFFFFF; font-family: Graphik-Bold  display: table-cell; vertical-align: middle; height: 0px;" >TOTAL</label>

                                <label id="n_total_<?php echo($registro['nombre_dependencia']);?>" style="text-align: center; color: #FFFFFF; font-size: 12px; font-family: Graphik-Bold">0</label>
                              
                              </td>
                            </tr>
                              

                            </tbody>
                        </table>

                     
                      </div>  
                     </button>

                      <div class="panel">
                        <br>
                          <div class="list-group">

                          <?php global $wpdb;
                          $nom_dep=$registro['nombre_dependencia'];

                          
                          $total_1=0;
                          $total_2=0;
                          $total_3=0;
                          $total_4=0;
                          $total_5=0;
                          $total=0;
                          
                          





                          $evidencias = $wpdb->prefix . 'indicadores_pids';
                          $registros_ind = $wpdb->get_results("SELECT * FROM $evidencias WHERE nombre_usuario_pid ='$nom_dep'" , ARRAY_A);
                         
                              
                              foreach($registros_ind as $reg_ind) 
                                { 
                                  
                                  $total++;
                                  if($reg_ind['estatus']==1)
                                    {
                                      $total_1++;
                                      
                                    }
                                    if($reg_ind['estatus']==2)
                                    {
                                      $total_2++;
                                      
                                    }
                                     if($reg_ind['estatus']==3)
                                    {
                                      $total_3++;
                                      
                                    }
                                     if($reg_ind['estatus']==4)
                                    {
                                      $total_4++;
                                      
                                    }
                                     if($reg_ind['estatus']==5)
                                    {
                                      $total_5++;
                                      
                                    }
                                    

                                  

                                  

                                ?>
                                <table>
                                  <col width=80%>
                                  <col width=10%>
                                  <col width=10%>
                                    <tbody>
                                     <td id="contenido_datos" > 
                                       <a href="#" class="list-group-item" onclick="indicador_individual_organismos(<?php echo($reg_ind['id_tactico_pid']);?>,'<?php echo($reg_ind['nom_indicador_pid']);?>','<?php echo($reg_ind['nombre_usuario_pid']);?>','<?php echo($reg_ind['comentarios_evaluador']);?>','<?php echo($reg_ind['comentarios_dependecia']);?>','<?php echo($reg_ind['ficha']);?>')">
                                        <h4 class="list-group-item-heading"><?php echo($reg_ind['nom_indicador_pid']);?></h4>
                                        <p class="list-group-item-text"><?php echo($reg_ind['eje_ped_pid']);?></p>
                                        </a>
                                      </td> 
                                      <td>
                                         
                                            <div class="btn-group">
                                              <button id="btn_status__pid_<?php echo($reg_ind['id_tactico_pid'])?>" type="button" class="btn btn-secondary">ESTATUS</button>
                                              <button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
                                              </button>
                                              <div class="dropdown-menu">
                                                <a class="dropdown-item" id="adecuada_pid_<?php echo($reg_ind['id_tactico_pid'])?>">ADECUADA</a>
                                               <!-- <a class="dropdown-item" id="poco_cuestionable_pid_<?php echo($reg_ind['id_tactico_pid'])?>">POCO CUESTINABLE</a>-->
                                                <a class="dropdown-item" id="cuestionable<?php echo($reg_ind['id_tactico_pid'])?>">CUESTINABLE</a>

                                                <a class="dropdown-item" id="muycuestionable_pid<?php echo($reg_ind['id_tactico_pid'])?>">MUY CUESTINABLE</a>

                                                 <a class="dropdown-item" id="sin_elementos_pid<?php echo($reg_ind['id_tactico_pid'])?>">SIN ELEMENTOS PARA EVALUAR</a>

                                              </div>
                                            </div>
                                         
                                      </td>
                                      <td>
                                        <label id="estado_<?php echo($reg_ind['id_tactico_pid'])?>" style="text-align: center; font-size: 17px; font-family: Graphik-Bold;  color:#6c757d; ">Por revisar</label>
                                      </td>
                                        <script type="text/javascript">

                                         // alert(<?php echo($contraloria_1) ?>);


                                          jQuery("#adecuada_pid_<?php echo($reg_ind['id_tactico_pid'])?>").click(function(e)
                                          {
                                            var id_ind=<?php echo($reg_ind['id_tactico_pid'])?>;
                                            
                                            document.getElementById("btn_status__pid_<?php echo($reg_ind['id_tactico_pid'])?>").innerHTML="ADECUADA";

                                            var element = document.getElementById("btn_status__pid_<?php echo($reg_ind['id_tactico_pid'])?>");
                                            var stado=1;
                                            element.classList.remove("btn-secondary");
                                            element.classList.remove("btn-primary");
                                            element.classList.remove("btn-warning");
                                            element.classList.remove("btn-danger");
                                            element.classList.add("btn-success");
                                            var evaluador_pid ='evaluador_pid';

                                            

                                             $.ajax({
                                                  type: "POST",
                                                  url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                                                  data: {id_indicador:id_ind,status:stado,evaluador_pid:evaluador_pid}
                                                  ,
                                                  success: function(msg){
                                                    //console.log(msg)
                                                  mjm_to_eval = document.getElementById('estado_<?php echo($reg_ind['id_tactico_pid']);?>').innerHTML = 'Validada Correctamente';
                                                  document.getElementById('estado_<?php echo($reg_ind['id_tactico_pid']);?>').style.color = "#28a745";

                                                   // alert( "Informacion Actualizada" );
                                                  },
                                                  error: function(XMLHttpRequest, textStatus, errorThrown) {
                                                     //alert( "Error Verifica tu informacion" );
                                                  }
                                                });


                                          //e.preventDefault();
                                          });


                                          jQuery("#poco_cuestionable_pid_<?php echo($reg_ind['id_tactico_pid'])?>").click(function(e)
                                          {
                                            //alert(e.name);
                                            document.getElementById("btn_status__pid_<?php echo($reg_ind['id_tactico_pid'])?>").innerHTML="POCO CUESTINABLE";

                                            var element = document.getElementById("btn_status__pid_<?php echo($reg_ind['id_tactico_pid'])?>");
                                            element.classList.remove("btn-secondary");
                                            element.classList.remove("btn-success");
                                            element.classList.remove("btn-warning");
                                            element.classList.remove("btn-danger");
                                            element.classList.add("btn-primary");

                                            var element = document.getElementById("btn_status__pid_<?php echo($reg_ind['id_tactico_pid'])?>");

                                            var id_ind=<?php echo($reg_ind['id_tactico_pid'])?>;
                                            var stado=2;
                                            var evaluador_pid ='evaluador_pid';


                                            

                                            $.ajax({
                                                  type: "POST",
                                                  url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                                                  data: {id_indicador:id_ind,status:stado,evaluador_pid:evaluador_pid}
                                                  ,
                                                  success: function(msg){
                                                    //console.log(msg)
                                                    //cambiar el mensaje para el evaluador
                                                    document.getElementById('estado_<?php echo($reg_ind['id_tactico_pid']);?>').innerHTML = 'Esperando Resupuesta';
                                                    document.getElementById('estado_<?php echo($reg_ind['id_tactico_pid']);?>').style.color = "#727272";

                                                   // alert( "Informacion Actualizada" );
                                                  },
                                                  error: function(XMLHttpRequest, textStatus, errorThrown) {
                                                     //alert( "Error Verifica tu informacion" );
                                                  }
                                                });

                                          });

                                          jQuery("#cuestionable<?php echo($reg_ind['id_tactico_pid'])?>").click(function(e)
                                          {
                                            //alert(e.name);
                                            document.getElementById("btn_status__pid_<?php echo($reg_ind['id_tactico_pid'])?>").innerHTML="CUESTIONABLE";

                                            var element = document.getElementById("btn_status__pid_<?php echo($reg_ind['id_tactico_pid'])?>");
                                            element.classList.remove("btn-secondary");
                                            element.classList.remove("btn-success");
                                            element.classList.remove("btn-primary");
                                            element.classList.remove("btn-danger");
                                            element.classList.add("btn-warning");

                                            var element = document.getElementById("btn_status__pid_<?php echo($reg_ind['id_tactico_pid'])?>");
                                            var stado=3;
                                            var id_ind=<?php echo($reg_ind['id_tactico_pid'])?>;
                                            var evaluador_pid ='evaluador_pid';

                                            $.ajax({
                                                  type: "POST",
                                                  url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                                                  data: {id_indicador:id_ind,status:stado,evaluador_pid:evaluador_pid}
                                                  ,
                                                  success: function(msg){
                                                   // console.log(msg)
                                                    document.getElementById('estado_<?php echo($reg_ind['id_tactico_pid']);?>').innerHTML = 'Esperando Resupuesta';
                                                    document.getElementById('estado_<?php echo($reg_ind['id_tactico_pid']);?>').style.color = "#727272";

                                                   // alert( "Informacion Actualizada" );
                                                  },
                                                  error: function(XMLHttpRequest, textStatus, errorThrown) {
                                                     //alert( "Error Verifica tu informacion" );
                                                  }
                                                });

                                          });

                                          jQuery("#muycuestionable_pid<?php echo($reg_ind['id_tactico_pid'])?>").click(function(e)
                                          {
                                            //alert(e.name);
                                            document.getElementById("btn_status__pid_<?php echo($reg_ind['id_tactico_pid'])?>").innerHTML=" MUY CUESTINABLE";

                                            var element = document.getElementById("btn_status__pid_<?php echo($reg_ind['id_tactico_pid'])?>");
                                            element.classList.remove("btn-secondary");
                                            element.classList.remove("btn-success");
                                            element.classList.remove("btn-primary");
                                            element.classList.remove("btn-warning");
                                            element.classList.add("btn-danger");

                                            var element = document.getElementById("btn_status__pid_<?php echo($reg_ind['id_tactico_pid'])?>");
                                            var stado=4;
                                            var id_ind=<?php echo($reg_ind['id_tactico_pid'])?>;
                                            var evaluador_pid ='evaluador_pid';

                                            $.ajax({
                                                  type: "POST",
                                                  url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                                                  data: {id_indicador:id_ind,status:stado,evaluador_pid:evaluador_pid}
                                                  ,
                                                  success: function(msg){
                                                    //console.log(msg)
                                                    document.getElementById('estado_<?php echo($reg_ind['id_tactico_pid']);?>').innerHTML = 'Esperando Resupuesta';
                                                    document.getElementById('estado_<?php echo($reg_ind['id_tactico_pid']);?>').style.color = "#727272";

                                                   // alert( "Informacion Actualizada" );
                                                  },
                                                  error: function(XMLHttpRequest, textStatus, errorThrown) {
                                                     //alert( "Error Verifica tu informacion" );
                                                  }
                                                });

                                          });


                                          jQuery("#sin_elementos_pid<?php echo($reg_ind['id_tactico_pid'])?>").click(function(e)
                                          {
                                            //alert(e.name);
                                            document.getElementById("btn_status__pid_<?php echo($reg_ind['id_tactico_pid'])?>").innerHTML=" SIN ELEMENTOS PARA EVALUAR";

                                            var element = document.getElementById("btn_status__pid_<?php echo($reg_ind['id_tactico_pid'])?>");
                                            element.classList.remove("btn-danger");
                                            element.classList.remove("btn-success");
                                            element.classList.remove("btn-primary");
                                            element.classList.remove("btn-warning");
                                            element.classList.add("btn-secondary");

                                            var element = document.getElementById("btn_status__pid_<?php echo($reg_ind['id_tactico_pid'])?>");
                                            var stado=5;
                                            var id_ind=<?php echo($reg_ind['id_tactico_pid'])?>;
                                            var evaluador_pid ='evaluador_pid';

                                            $.ajax({
                                                  type: "POST",
                                                  url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                                                  data: {id_indicador:id_ind,status:stado,evaluador_pid:evaluador_pid}
                                                  ,
                                                  success: function(msg){
                                                   // console.log(msg);
                                                    document.getElementById('estado_<?php echo($reg_ind['id_tactico_pid']);?>').innerHTML = 'Esperando Resupuesta';
                                                    document.getElementById('estado_<?php echo($reg_ind['id_tactico_pid']);?>').style.color = "#727272";

                                                   // alert( "Informacion Actualizada" );
                                                  },
                                                  error: function(XMLHttpRequest, textStatus, errorThrown) {
                                                     //alert( "Error Verifica tu informacion" );
                                                  }
                                                });
                                       
                                          });




                                          if(<?php echo($reg_ind['num_revision']);?> == 1)
                                          {//ESTADO VALIDADO
                                            
                                            mjm_to_eval = document.getElementById('estado_<?php echo($reg_ind['id_tactico_pid']);?>').innerHTML = 'Informacion Validada';
                                            document.getElementById('estado_<?php echo($reg_ind['id_tactico_pid']);?>').style.color = "#28a745";
                                            

                                          }
                                          else if(<?php echo($reg_ind['num_revision']);?> == 2)
                                          {//ESTADO ESPERANDO RESPUESTA
                                            
                                            document.getElementById('estado_<?php echo($reg_ind['id_tactico_pid']);?>').innerHTML = 'Esperando Respuesta';
                                            document.getElementById('estado_<?php echo($reg_ind['id_tactico_pid']);?>').style.color = "#6c757d";
                                          }
                                          else if(<?php echo($reg_ind['num_revision']);?> == -1)
                                          {//ESTADO NUEVO MENSAJE
                                            
                                           // alert(<?php echo($reg_ind['num_revision']);?>);
                                            document.getElementById('estado_<?php echo($reg_ind['id_tactico_pid']);?>').innerHTML = 'Nuevo Mensaje';
                                            document.getElementById('estado_<?php echo($reg_ind['id_tactico_pid']);?>').style.color = "#FFC300";
                                          }
                                            


                                          if(<?php echo($reg_ind['estatus']);?> == 1)
                                          {
                                           
                                            //document.getElementById("check_<?php echo($reg_ind['id_tactico_pid']);?>").checked = true;
                                            //document.getElementById('estado_<?php echo($reg_ind['id_tactico_pid']);?>').innerHTML = 'Informacion Validada';
                                            //document.getElementById('estado_<?php echo($reg_ind['id_tactico_pid']);?>').style.color = "#28a745";
                                            
                                            var element = document.getElementById("btn_status__pid_<?php echo($reg_ind['id_tactico_pid'])?>");
                                            element.innerHTML=" ADECUADA";
                                            element.classList.remove("btn-secondary");
                                            element.classList.add("btn-success");
                                            

                                          }
                                          /*

                                          else if(<?php echo($reg_ind['estatus']);?> == 2)
                                          {
                                           
                                            //document.getElementById("check_<?php echo($reg_ind['id_tactico_pid']);?>").checked = true;
                                            /*
                                            document.getElementById('estado_<?php echo($reg_ind['id_tactico_pid']);?>').innerHTML = 'Por evisar';
                                            document.getElementById('estado_<?php echo($reg_ind['id_tactico_pid']);?>').style.color = "#DE7008";
                                           
                                            
                                            var element = document.getElementById("btn_status__pid_<?php echo($reg_ind['id_tactico_pid'])?>");
                                            element.innerHTML="POCO CUESTIONABLE";
                                            element.classList.remove("btn-secondary");
                                            element.classList.add("btn-primary");
                                            

                                          }
                                          */
                                          else if(<?php echo($reg_ind['estatus']);?> == 3)
                                          {

                                            var element = document.getElementById("btn_status__pid_<?php echo($reg_ind['id_tactico_pid'])?>");
                                            element.innerHTML="CUESTIONABLE";
                                            element.classList.remove("btn-secondary");
                                            element.classList.add("btn-warning");
                                            

                                          }
                                          else if(<?php echo($reg_ind['estatus']);?> == 4)
                                          {

                                            var element = document.getElementById("btn_status__pid_<?php echo($reg_ind['id_tactico_pid'])?>");
                                            element.innerHTML="MUY CUESTIONABLE";
                                            element.classList.remove("btn-secondary");
                                            element.classList.add("btn-danger");
                                            

                                          }

                                          else if(<?php echo($reg_ind['estatus']);?> == 5)
                                          {

                                            var element = document.getElementById("btn_status__pid_<?php echo($reg_ind['id_tactico_pid'])?>");
                                            element.innerHTML="SIN ELEMENTOS PARA EVALUAR";
                                            //element.classList.remove("btn-secondary");
                                            //element.classList.add("btn-secondary");
                                            

                                          }

                                          /*

                                          else if(<?php echo($reg_ind['estatus']);?> == 2)
                                          {
                                           
                                            
                                             document.getElementById('estado_<?php echo($reg_ind['id_tactico_pid']);?>').innerHTML = 'Esperando Respuesta';
                                            document.getElementById('estado_<?php echo($reg_ind['id_tactico_pid']);?>').style.color = "#65D630";
                                          }
                                          */
                                        </script>
                                                

                                    </tbody>
                                </table>



                                <?php  
                          } ?>
                              
                          </div>
                      </div>

                               <script type="text/javascript">
                  
                  //colocar el numero de indicadores revisados y su estatus
                  
               

                  document.getElementById('n_adecuada_<?php echo($registro['nombre_dependencia']);?>').innerHTML = <?php echo($total_1);?>;
                  //document.getElementById('n_poco_<?php echo($registro['nombre_dependencia']);?>').innerHTML = <?php echo($total_2);?>;
                  document.getElementById('n_cuestionable_<?php echo($registro['nombre_dependencia']);?>').innerHTML = <?php echo($total_3);?>;
                  document.getElementById('n_muy_<?php echo($registro['nombre_dependencia']);?>').innerHTML = <?php echo($total_4);?>;
                  document.getElementById('n_sin_<?php echo($registro['nombre_dependencia']);?>').innerHTML = <?php echo($total_5);?>;

                  document.getElementById('n_total_<?php echo($registro['nombre_dependencia']);?>').innerHTML = <?php echo($total);?>;







                </script>

                  <?php 
                  $total=0; 
                   $total_1=0;
                   $total_2=0;
                   $total_3=0;
                   $total_4=0;
                   $total_5=0;
                } ?>


       

              </div>

            

                



              <div id="panel_indicador_organismo">



                 
                <br>

                <div class="container-fluid"><!--titulo-->
                  <div class="row">
                    <div class="col-sm-3" >
                      <label  style="text-align: left; color: #AAAAAA; font-size: 25px; font-family: Graphik-Bold" >Nombre Indicador:</label>
                    </div>
                    <div class="col-sm-9" >
                      <label id="nom_dep_lbl"  style="text-align: left; color: #172744; font-size: 20px; font-family: Graphik-Bold" >Nombre del Indicador</label>
                    </div>
                  </div>
                </div><!-- FIN titulo-->
                <br>
                
                <br>

                <div class="row">
                  <div id="div_reporte_pid"  class="col-sm-4" style=" text-align:center; border: 2px solid #4CAF50; /* Green */" >
                    <div>
                     <a  onclick="abrir_doc_reporte()" style="text-align: center; font-size: 25px; font-family: Graphik-Bold" >REPORTE</a>
                    </div>
            

                  </div>
                  <div id="div_repositorio_pid" class="col-sm-4" style=" text-align:center;  border: 2px solid #FCFCFC;" >
                      <a  onclick="abrir_doc_repositorio()" style="text-align: center; font-size: 25px; font-family: Graphik-Bold" >REPOSITORIO DE EVIDENCIA</a>
                  </div>

                  <div id="div_ficha_pid" class="col-sm-4" style=" text-align:center; border: 2px solid #FCFCFC;">

                    <a  onclick="abrir_doc_ficha()" style="text-align: center; font-size: 25px; font-family: Graphik-Bold">FICHA TECNICA</a>

                  </div>
                </div>

                <br>


                  <div id="apartado_repositorio_organismo" style="width: 100%; height: 300px; overflow-y: scroll;">

              

                  </div>

                <iframe align="rig" id="frame_pdf_pid" style="width:100%; height:350px;" frameborder="0">
                  

                </iframe>
                <br>

              

                <form action="/action_page.php">



                  <fieldset>




                     <div class="row">
                      <div class="col-sm-5">

                        <label id="coment_dep_lbl" style="text-align: center; color: #585656; font-size: 25px; font-family: Graphik-Bold">Observaciones de la dependencia</label>
                        <br>
                        <br>

                        <textarea id="coment_dep" rows="4" cols="50"  style="text-align: center; color: #F28604; font-size: 18px; font-family: Graphik-Bold"  ></textarea>


                      </div>
                      <div class="col-sm-2"></div>
                      <div class="col-sm-5">
                        
                            <label id="coment_dep_lbl" style="text-align: center; color: #585656; font-size: 25px; font-family: Graphik-Bold">Mensaje para la dependencia:</label>
                        <br>
                        <br>

                        <textarea id="comentarios_evaluador_pid" rows="4" cols="50"  style="text-align: center; color: #47BF02; font-size: 20px; font-family: Graphik-Bold"  ></textarea>

                       
                      </div>
                    </div>



                   
                  </fieldset>
                </form>
                <br>




                <div class="row">
                  <div class="col-sm-6" style=" text-align:center;">
                    <div>
                     <button class=" button button5" onclick="volver()"  style="text-align: center; font-size: 25px; font-family: Graphik-Bold" >Atras</button>
                    </div>
                  </div>

                  <div class="col-sm-6" style=" text-align:center;">
                    <div>
                     <button class=" button button5"  onclick="enviar()" style="text-align: center;  font-size: 25px; font-family: Graphik-Bold" >Enviar</button>
                    </div>
                  </div>
            

              </div>

              <!-- Modal -->
                  <div class="modal fade" id="myModal_ver" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg"  >
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                         <!-- <h4 class="modal-title" id="myModalLabel">Modal title</h4>-->
                        </div>
                        <div class="modal-body">
                            <iframe  id="iframe_ver_doc" src="" id="info" class="iframe" name="info"  height="100%" width="100%"></iframe>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                  </div>
                      </div>
                    </div>
                  </div>


                  
               


              <iframe id="frame_download" style="display:none;"></iframe>

               
                                 



              <script type="text/javascript">
                /*  Acordeon*/

                var acc = document.getElementsByClassName("accordion");
                var i;

                for (i = 0; i < acc.length; i++) {
                    acc[i].onclick = function(){
                        /* Toggle between adding and removing the "active" class,
                        to highlight the button that controls the panel */
                        this.classList.toggle("active");

                        /* Toggle between hiding and showing the active panel */
                        var panel = this.nextElementSibling;
                        if (panel.style.display === "block") {
                            panel.style.display = "none";
                        } else {
                            panel.style.display = "block";
                        }
                    }
                }

                /* fin acordeon */
              </script>


              <script>
                $(document).ready(function() 
                  {
                    // $('#correccion_evidencia').hide();
                     //$('#check_green_indicador_<?php echo $registro['id_tactico_pid']; ?>').hide();
                    // $('#check_green_evidencia').hide();

                    $('#panel_indicador_organismo').hide();

                    $('#apartado_repositorio_organismo').hide();
                  });

                var id_ind_select;
                var nom_depen;
                var nom_evaluador;
                var comentarios_evaluador_pid;
                var ficha_nombre;
                var nom_ind_pid;

                function indicador_individual_organismos(id_indicador,nom_ind,nom_dep,coment_ind,coment_depe,ficha)
                {

                  $('#list_dependencias_organismos').hide();
                  $('#panel_indicador_organismo').show();
                   document.getElementById('nom_dep_lbl').innerHTML = nom_ind;
                   id_ind_select=id_indicador;

                   document.getElementById('coment_dep_lbl').innerHTML = " Comentarios de la dependencia:";
                   ficha_nombre=ficha;
                   nom_ind_pid=nom_ind;
                   
                   //alert(coment_depe);
                   //alert(coment_ind);
                              
                   id_ind_select=id_indicador;


                  nom_depen=nom_dep;
                  abrir_doc_reporte();
                  nom_evaluador= "<?php echo($user_name) ?>";
                  comentarios_evaluador_pid=coment_ind;
                 // alert(coment_ind);
                  document.getElementById('comentarios_evaluador_pid').value=coment_ind;
                  document.getElementById('coment_dep').innerHTML='"'+coment_depe+'"';
                  
                  
                }

                function volver()
                {
                  $('#list_dependencias_organismos').show();
                  $('#panel_indicador_organismo').hide();

                  document.getElementById('frame_pdf_pid').src = "";
                  $('#apartado_repositorio_organismo').hide();
                }

                function abrir_doc_reporte()
                {

                  //btn_select_Activo
                  document.getElementById("div_repositorio_pid").style.borderColor = "#FCFCFC";
                  document.getElementById("div_reporte_pid").style.borderColor = "#4CAF50";
                  document.getElementById("div_ficha_pid").style.borderColor = "#FCFCFC";
                  document.getElementById("frame_pdf_pid").style.display = "inline";

                 // var user_name="oscar";
                  var arr_ids_tacticos=0;
                  var tipo="pids";
                  $('#apartado_repositorio_organismo').hide();


                  document.getElementById('frame_pdf_pid').src = "pdf_plugin/pdf_evidencias/crearEvidenciaPIDS.php?id_evidencia="+id_ind_select+"&user="+nom_depen+"&arr_ids_tacticos="+arr_ids_tacticos+"&tipo="+tipo;

                 

                }

                function abrir_doc_repositorio()
                {

                //btn_select_Activo
                document.getElementById("div_repositorio_pid").style.borderColor = "#4CAF50";
                document.getElementById("div_reporte_pid").style.borderColor = "#FCFCFC";
                document.getElementById("div_ficha_pid").style.borderColor = "#FCFCFC";
                //-----fin btn_activo----

                 document.getElementById('frame_pdf_pid').src = "";
                 document.getElementById("frame_pdf_pid").style.display = "none";

                 $('#apartado_repositorio_organismo').show();
                 var load_datos_pids="load_datos_pids";


                 $.ajax({
                      type: "GET",
                      datatype: 'json',
                      contentType: "application/json; charset=utf-8",
                      url: "/repositorio/wp-content/themes/EspecialesT2.1/templates/load_archives.php",
                      data: {id_indicador:nom_ind_pid,nom_depen:nom_depen},
                      success: function(respuesta){

                        //console.log(respuesta);
                        var respuesta = $.parseJSON(respuesta);   
                       // alert(respuesta[0].filename); 

                        var informacion;
                        informacion='<div id="info_labels_'+i+'">'+'<table> <col width=15%> <col width=10%> <col width=10%> <col width=25%> <col width=10%>  <col width=10%> <col width=20%> <tbody>';
                        $.each(respuesta, function(i, item) 
                        {

                          

                         // alert(respuesta[i].filename);
                          //var url=String(respuesta[i].ruta_archivo);
                          if(i==0)
                          {
                            informacion+='<tr><td><label style="text-align: left; font-size: 18px; font-family: Graphik-Bold" >ARCHIVO</td><td><label style="text-align: left; font-size: 18px; font-family: Graphik-Bold" >'+'DESCARGA'+'</label></td><td> <label style="text-align: center; font-size: 18px; font-family: Graphik-Bold" >'+'VER'+'</label></td> <td> <label style="text-align: center; font-size: 18px; font-family: Graphik-Bold" >'+'OBSERVACIONS DE EVIDENCIA'+'</label></td> <td> </td><td><label style="text-align: left; font-size: 18px; font-family: Graphik-Bold" >'+'ESTATUS'+'</label></td> </tr>';
                          }
                          
                          informacion+='<tr><td><p><label style="text-align: left; font-size: 14px; font-family: Graphik-Bold" >'+respuesta[i].filename+'</label>  </p> </td>'+
                          '<td><a download href="" id="down_btn_'+respuesta[i].id_archivo+'" onclick=download_file('+respuesta[i].id_archivo+',"'+respuesta[i].ruta_archivo+'","'+encodeURIComponent(respuesta[i].filename)+'") class="btn_down"><i class="fa fa-download"></i></a></td>'+
                          '<td><label   style="text-align: center; font-size: 17px; font-family: Graphik-Bold"><span class="glyphicon glyphicon-eye-open" style="  font-size:20px;text-shadow:2px 2px 4px #9A9A9A;" type="button"  class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal_ver" onclick=ver_archivo("'+respuesta[i].ruta_archivo+'","'+encodeURIComponent(respuesta[i].filename)+'")></span></label></td>'+
                          '<td><div id="div_archivo_revisado_'+respuesta[i].id_archivo+'"><label style="text-align: center;  font-size: 18px; font-family: Graphik-Bold; color:#28a745;" >Archivo Revisado</label></div><div id="div_validado_'+respuesta[i].id_archivo+'" ><label style="color:#FF5733;" for="coment_archivo_pid_'+respuesta[i].id_archivo+'">max 300 caracteres</label><textarea maxlength="300"; id="coment_archivo_pid_'+respuesta[i].id_archivo+'">'+respuesta[i].comentario_archivo+'</textarea></div> </td>'+
                          '<td> <div id="div_validado2_'+respuesta[i].id_archivo+'" ><button onclick="enviar_coment_doc('+respuesta[i].id_archivo+')" class=" button2">Enviar</button></div> </td>'+

                          '<td  ><label  ><input onclick="update_archivo_status(this,'+respuesta[i].id_archivo+')"  type="checkbox" id="check_status_'+respuesta[i].id_archivo+'"><span  class="checkmark"></span></label></td><tr>';
                          
                          
                                 

                            
                        })
                        +'</tbody></table><div>';
                        $('#apartado_repositorio_organismo').html(informacion); 

                        //no se pueden hacer busquedas en los elemntos hasta q se carguen todos  .html inforcacion
                        $.each(respuesta, function(i, item) 
                        {

                            //alert(respuesta[i].filename);

                           $('#div_archivo_revisado_'+respuesta[i].id_archivo).hide();
                           /// alert('div_archivo_revisado_'+respuesta[i].id_archivo);

                            if(respuesta[i].estatus==1)
                             {

                              //var coment_archive=document.getElementById("check_status_"+respuesta[i].id_archivo).value;
                              //console.log(elemento);
                            document.getElementById("check_status_"+respuesta[i].id_archivo).checked=true;

                             $('#div_validado_'+respuesta[i].id_archivo).hide();
                             $('#div_validado2_'+respuesta[i].id_archivo).hide();
                             $('#div_archivo_revisado_'+respuesta[i].id_archivo).show();

                            //console.log(elemento);

                            //document.getElementById("check_status"+respuesta[i].id_archivo).checked=true;
                             }
                             else if(respuesta[i].estatus==0)
                             {
                              document.getElementById("check_status_"+respuesta[i].id_archivo).checked = false;

                              $('#div_validado_'+respuesta[i].id_archivo).show();
                               $('#div_validado2_'+respuesta[i].id_archivo).show();
                               $('#div_archivo_revisado_'+respuesta[i].id_archivo).hide();
                             }






                        })


                      },
                      error: function(XMLHttpRequest, textStatus, errorThrown) {
                         //alert( "Error Verifica tu informacion" );
                      }
                    });




                 

        
                }


                function download_file(id,ruta,file)
                {
                  var nom_ind_pid_selec_cortado;
                  nom_ind_pid_selec_cortado = nom_ind_pid.slice(0, 59);
                  ruta_download= "/repositorio/upload_files_plugin/php/"+nom_depen+"/"+nom_ind_pid_selec_cortado+"/"+file;
                  alert(ruta_download);

                  document.getElementById("down_btn_"+id).href=ruta_download;
                }


                function enviar_coment_doc(id_archivo)
                {


                var coment_archive=document.getElementById("coment_archivo_pid_"+id_archivo).value;
                



                   $.ajax({
                      type: "POST",
                      url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                      data: {id_indicador:id_archivo,comentario_archivo: coment_archive}
                      ,
                      success: function(msg){
                        console.log(msg)

                       alert( "Informacion Actualizada" );
                      },
                      error: function(XMLHttpRequest, textStatus, errorThrown) {
                         //alert( "Error Verifica tu informacion" );
                      }
                    });
                }

                function ver_archivo(ruta_archivo,nom_doc)
                {

                 var nom_ind_pid_selec_cortado;
                  nom_ind_pid_selec_cortado = nom_ind_pid.slice(0, 59);
                  ruta_download= "/repositorio/upload_files_plugin/php/"+nom_depen+"/"+nom_ind_pid_selec_cortado+"/"+nom_doc;
                  

                  isUrlExists(ruta_download, function(status)
                  {
                      if(status === 200){
                         // file was found
                         alert("se encontro");
                      }
                      else if(status === 404){
                         // 404 not found
                         alert("NO xse encontro");
                      }
                  });

                  //var direc="/repositorio/upload_files_plugin/php/"+ruta_archivo+nom_doc;

                  alert(ruta_download);
                  
                  document.getElementById('iframe_ver_doc').src = ruta_download;

                  
                 
                }

                function abrir_doc_ficha()
                {

                  //btn_select_Activo
                  document.getElementById("div_repositorio_pid").style.borderColor = "#FCFCFC";
                  document.getElementById("div_reporte_pid").style.borderColor = "#FCFCFC";
                  document.getElementById("div_ficha_pid").style.borderColor = "#4CAF50";
                  //-----fin btn_activo----

                  document.getElementById('frame_pdf_pid').src = "/repositorio/upload_files_plugin/php/"+nom_depen+"/ficha/"+ficha_nombre;

                  $('#apartado_repositorio_organismo').hide();
        
                }


         
                function update_archivo_status(cb,id_select) 
                {
                 // alert(cb.checked);
                 // alert(id_select);
                  var id_archivo=id_select;
                  var update_evaluado;

                  if(cb.checked==true)
                  {
                    update_evaluado=1;

                     //document.getElementById('div_validado_'+id_select).hide();
                     
                     $('#div_validado_'+id_select).hide();
                     $('#div_validado2_'+id_select).hide();
                     $('#div_archivo_revisado_'+id_select).show();
                    // alert('div_validado_'+id_select);
                  }
                  else if(cb.checked==false)
                  {
                     update_evaluado=0;

                     $('#div_validado_'+id_select).show();
                     $('#div_validado2_'+id_select).show();
                     $('#div_archivo_revisado_'+id_select).hide();

                     // document.getElementById('estado_'+id_select).innerHTML = 'Esperando Revision';
                      //document.getElementById('estado_'+id_select).style.color = "#FF5733";
                  }

                    $.ajax({
                      type: "POST",
                      url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                      data: {id_indicador:id_archivo,update_estado_archivo: update_evaluado}
                      ,
                      success: function(msg){
                        console.log(msg)

                       // alert( "Informacion Actualizada" );
                      },
                      error: function(XMLHttpRequest, textStatus, errorThrown) {
                         //alert( "Error Verifica tu informacion" );
                      }
                    });
                    

                }

                function enviar()
                {


                 var coment_eva =document.getElementById("comentarios_evaluador_pid").value;

                 var coment_eva_pid ="coment_eva_pid";
                



                  
                      $.ajax({
                      type: "POST",
                      url: "/repositorio/wp-content/themes/EspecialesT2.1/inc/update_evidencia_dependencia.php",
                      data: {id_indicador:id_ind_select,nom_evaluador: nom_evaluador,nom_depen:nom_depen,coment_eva:coment_eva,coment_eva_pid:coment_eva_pid}
                      ,
                      success: function(msg){

                          document.getElementById('estado_'+id_ind_select).innerHTML = 'Esperando Respuesta';
                          document.getElementById('estado_'+id_ind_select).style.color = "#65D630";
                                         
                        console.log(msg)

                      alert( "Mensaje enviado correctamente" );
                      },
                      error: function(XMLHttpRequest, textStatus, errorThrown) {
                         //alert( "Error Verifica tu informacion" );
                      }
                    });

                }


                
              

                




              </script>
            </main>


          </div>
        <?php endwhile; ?>

        </div>
      </div>
    </div>

</section>

<?php get_footer(); ?>
