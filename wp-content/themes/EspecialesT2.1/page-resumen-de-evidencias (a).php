
<?php get_header(); 
/*
*  Template Name: resumen-de-evidencias
*/
$pagina=$_GET['page_id'];

?>


<style>
  body {font-family: Arial;}

  /* Style the tab_resumen */
  .tab_resumen {
      overflow: hidden;
      border: 1px solid #ccc;
      background-color: #f1f1f1;
  }

  /* Style the buttons inside the tab_resumen */
  .tab_resumen button {
      background-color: inherit;
      float: left;
      border: none;
      outline: none;
      cursor: pointer;
      padding: 14px 16px;
      transition: 0.3s;
      font-size: 17px;
  }

  /* Change background color of buttons on hover */
  .tab_resumen button:hover {
      background-color: #ddd;
  }

  /* Create an active/current tab_resumenlink class */
  .tab_resumen button.active {
      background-color: #ccc;
  }

  /* Style the tab_resumen content */
  .tab_resumencontent {
      display: none;
      padding: 6px 12px;
      border: 1px solid #ccc;
      border-top: none;
  }
</style>

<section id="main-content">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">

        <?php while(have_posts()): the_post(); ?>

          <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
          <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
          <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

          <div class="principal contenedor contacto">
              <main class="contenido-paginas ">

                <div>
                  <table>
                    <col width=80%>
                    <col width=20%>
                      <tbody>
                        <td>
                          <h2 id="txt_Titulo_verde"> RESUMEN DE INDICADORES</h2>
                        </td>
                        <td>

                        <?php /* Nombre del usuario activo*/ 
                          $all_users = get_users();
                          $user_name;
                          $user_name=$user->display_name;

                              echo '<h6>Bienvenido: </6>' . $current_user->user_login . '<br />';
                              echo '<h6>Correo:</6> ' . $current_user->user_email . '<br />';
                              $user_name=$current_user->user_login;
                        ?>
                         </td>
                         <td> 
                              <?php
                                  $user = wp_get_current_user();
                                if ( $user ) :
                                    ?>
                                    <img src="<?php echo esc_url( get_avatar_url( $user->ID ) ); ?>" />
                                <?php endif; 
                              ?>
                         </td> 
                      </tbody>
                  </table>
                </div> 

              <div class="tab_resumen">
		<button class="tab_resumenlinks" id="defaultOpen" onclick="openCity(event, 'COVID-19')" >COVID-19</button>
                <button class="tab_resumenlinks" id="defaultOpen" onclick="openCity(event, 'TACTICOS')" >TACTICOS</button>
                <button class="tab_resumenlinks" onclick="openCity(event, 'ESTRATEGICOS')">ESTRATEGICOS</button>
		<button class="tab_resumenlinks" onclick="location.href='http://sigeh.hidalgo.gob.mx/repositorio/?page_id=5';">CARGAR OTRO INDICADOR</button>
                <!--
                <button class="tab_resumenlinks" onclick="openCity(event, 'AVANCE PROGRAMATICO')">AVANCE PROGRAMATICO</button>
                <button class="tab_resumenlinks" onclick="openCity(event, 'ACCIONES CONCURRENTES')">ACCIONES CONCURRENTES</button>
                -->
              </div>

	<!-- COVID-19 -->
	<div id="COVID-19" class="tab_resumencontent">
                  <br>
                  <h3>COVID-19</h3>
                  <br>
                  <table>
                    <col width=5%>
                    <col width=65%>
                    <col width=30%>
                    <br>
                    <br>
                      <tbody>
                          <?php global $wpdb;
                            $evidencias = $wpdb->prefix . 'covid';
                            $registros_1 = $wpdb->get_results("SELECT * FROM $evidencias WHERE nombre_usuario_covid ='$user_name'", ARRAY_A);
                            foreach($registros_1 as $registro_1) 
                              { ?>
                                <?php $id_evidencia=$registro_1['id_covid']; 
                                $tipo="covid";
                                ?>

                                  <tr>
                                    <td>
                                      
                                      <img style="text-align: center" src="http://sigeh.hidalgo.gob.mx/repositorio/wp-content/uploads/2018/04/PDF.png" height="42" width="30" ></img>
                                    </td>
                                     <td id="txt_list_pdf">
                                      <div>
                                         <?php    echo("<p class='demo'><a href='pdf_plugin/pdf_evidencias/crear_evidencia_pdf_covid_19.php?id_covid=$id_evidencia' target='covid' class='demo'>".$registro_1['medida1']."</a></p>");?>
                                      </div>

                                     </td> 

                                     <td id="contenido_datos" >   
                                      
                                      
                                      
                                      <?php echo(  $registro_1['fecha_registro']);?>
                                  
                                   </td>
                              <?php } ?> 
                      </tbody>
                    
                  </table>
                 
                </div>

<!-- FIN COVID -->

                <div id="TACTICOS" class="tab_resumencontent">
                  <br>
                  <h3>INDICADORES TACTICOS</h3>
                  <br>
                  <table>
                    <col width=5%>
                    <col width=65%>
                    <col width=30%>
                    <br>
                    <br>
                      <tbody>
                          <?php global $wpdb;
                            $evidencias = $wpdb->prefix . 'definicion_evidencias';
                            $registros_1 = $wpdb->get_results("SELECT * FROM $evidencias WHERE nombre_dependencia_def ='$user_name'", ARRAY_A);
                            foreach($registros_1 as $registro_1) 
                              { ?>
                                <?php $id_evidencia=$registro_1['id_indicador_asociado_def']; 
                                $tipo="tactico";
                                ?>

                                  <tr>
                                    <td>
                                      
                                      <img style="text-align: center" src="http://sigeh.hidalgo.gob.mx/repositorio/wp-content/uploads/2018/04/PDF.png" height="42" width="30" ></img>
                                    </td>
                                     <td id="txt_list_pdf">
                                      <div>
                                         <?php    echo("<p class='demo'><a href='pdf_plugin/pdf_evidencias/crear_evidencia_pdf.php?id_evidencia= $id_evidencia&user=$user_name&arr_ids_tacticos=0&tipo=$tipo' target='_blank' class='demo'>".$registro_1['nombre_indicador_asociado_def']."</a></p>");?>
                                      </div>

                                     </td> 

                                     <td id="contenido_datos" >   
                                      
                                      
                                      
                                      <?php echo(  $registro_1['fecha_registro']);?>
                                  
                                   </td>
                              <?php } ?> 
                      </tbody>
                    
                  </table>
                 
                </div>

                <div id="ESTRATEGICOS" class="tab_resumencontent">
                  <br>
                  <h3>INDICADORES ESTRATEGICOS</h3>
                  <br>
                    <table>
                      <col width=5%>
                      <col width=65%>
                      <col width=30%>
                      <br>
                      <br>
                        <tbody>
                            <?php global $wpdb;
                              $ind_estrategicos = $wpdb->prefix .'indicadores_estrategicos_definidos';
                              $registros_estrategicos_1 = $wpdb->get_results("SELECT * FROM $ind_estrategicos WHERE dependecia_encargada ='$user_name'", ARRAY_A);
                              foreach($registros_estrategicos_1 as $registro_estrategicos_1) 
                                { ?>
                                  <?php $id_evidencia=$registro_estrategicos_1['id_ind_estrategico']; 
                                   $tipo="estrategicos";
                                  ?>
                                 
                                    <tr>
                                       <td>
                                      
                                      <img style="text-align: center" src="http://189.254.1.60/repositorio/wp-content/uploads/2018/04/PDF.png" height="42" width="30" ></img>
                                    </td>
                                       <td id="txt_list_pdf_estr"> <?php    echo("<p class='demo'><a href='pdf_plugin/pdf_evidencias/crear_evidencia_pdf.php?id_evidencia= $id_evidencia&arr_ids_tacticos=0&user=$user_name&tipo=$tipo' target='_blank' class='demo'>".$registro_estrategicos_1['nom_indicador']."</a></p>");?></td> 

                                       <td id="contenido_datos" >   
                                       
                                        <?php echo(  $registro_estrategicos_1['fecha_registro']);?>
                                    
                                     </td>
                                <?php } ?> 
                        </tbody>
                      
                    </table> 
                </div>
                  <!--
                <div id="AVANCE PROGRAMATICO" class="tab_resumencontent">
                  <h3>AVANCE PROGRAMATICO</h3>
                  <p>AVANCE PROGRAMATICO is the capital of Japan.</p>
                </div>

                <div id="ACCIONES CONCURRENTES" class="tab_resumencontent">
                  <h3>ACCIONES CONCURRENTES</h3>
                  <p>ACCIONES CONCURRENTES is the capital of Japan.</p>
                </div>
                 -->
                <script>
                  function openCity(evt,cityName) 
                  {
                   
                      var i, tab_resumencontent, tab_resumenlinks;
                      tab_resumencontent = document.getElementsByClassName("tab_resumencontent");
                      for (i = 0; i < tab_resumencontent.length; i++) {
                          tab_resumencontent[i].style.display = "none";
                      }
                      tab_resumenlinks = document.getElementsByClassName("tab_resumenlinks");
                      for (i = 0; i < tab_resumenlinks.length; i++) {
                          tab_resumenlinks[i].className = tab_resumenlinks[i].className.replace(" active", "");
                      }
                      document.getElementById(cityName).style.display = "block";
                      evt.currentTarget.className += " active";
                  }
                  // Get the element with id="defaultOpen" and click on it
                document.getElementById("defaultOpen").click();
                </script>
     
              </main>
          </div>
          <?php endwhile; ?>

        </div>
      </div>
    </div>

</section>

<?php get_footer(); ?>
