<?php get_header(); ?>

  <section>
      <div class="pagenation-holder">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <h3><?php single_cat_title(); ?>
                  <?php echo category_description(); ?></h3>
            </div>
            <div class="col-md-6 text-right">
              <div class="pagenation_links"><?php the_breadcrumb(); ?></div>
            </div>
          </div>
        </div>
      </div>
  </section>
  <!--end section-->
  <div class="clearfix"></div>
  	  
  <section>
    <div class="container">
      <div class="row sec-padding">
        <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="img-hover-holder-4 bmargin">
              <div class="img-hover-4">
                <div class="text-box ">
                  <h5 class="text-white"><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
                  <a href="<?php echo get_permalink(); ?>">Leer Más</a>
                </div>
                <div class="img-responsive"><?php the_post_thumbnail( 'categoria-thumb' );  ?></div>
              </div>
            </div>
          </div>
          <!--end item-->
          <?php endwhile; ?>
           <div class="col-md-12 col-sm-12 col-xs-12">
             <div class="col-md-6 col-sm-6 col-xs-6">
              <h3><?php posts_nav_link('','','&laquo; Entradas Anteriores') ?></h3>
             </div>
             <div class="col-md-6 col-sm-6 col-xs-6">
              <h3><?php posts_nav_link('','Siguientes Entradas &raquo;','') ?></h3>
             </div>
           </div>
            <?php else : ?>
                <p class="no-posts"><?php _e('Lo siento, no hay entradas que se ajusten a lo que busca', 'ejemplo'); ?></p>
            <?php endif; ?>
      </div>
    </div>
  </section>

<?php get_footer(); ?>
