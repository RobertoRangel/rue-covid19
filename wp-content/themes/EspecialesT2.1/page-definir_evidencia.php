

/*
*  Template Name: contacto
*/
$pagina=$_GET['page_id'];
?>



<section id="main-content">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
       
      <?php while(have_posts()): the_post(); ?>
          <div class="hero" style="background-image:url(<?php echo get_the_post_thumbnail_url(); ?>);">
              <div class="contenido-hero">
                  <div class="texto-hero">
                      <?php the_title('<h1>', '</h1>'); ?>
                  </div>
              </div>
          </div>
          
          <div class="principal contenedor contacto">
              <main class="contenido-paginas ">
                      <?php get_template_part('templates/formulario-definicion_evidencia', 'definicion_evidencia'); ?>
              </main>
          </div>
      <?php endwhile; ?>

        </div>
      </div>
    </div>

</section>

<?php get_footer(); ?>
