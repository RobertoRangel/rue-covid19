<?php get_header(); ?>
<section id="main-content">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
          <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="entry-content">
                <?php the_content(__('Sigue leyendo', 'ejemplo')); ?>
                <?php wp_link_pages('before=<p class="pages">' . __('Páginas:','ejemplo') . '&after=</p>'); ?>
            </div>
            <p class="entry-meta">
                <?php the_tags('<span class="tags"> <span class="sep">|</span> ' . __('Tags:', 'ejemplo') . ' ', ', ', '</span>'); ?>
            </p>
          </div>
              <?php endwhile; ?>
          <?php else : ?>
            <p class="no-posts"><?php _e('Lo siento, no hay entradas que se ajusten a lo que busca', 'ejemplo'); ?></p>
          <?php endif; ?>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>