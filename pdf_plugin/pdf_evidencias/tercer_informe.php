<?php
require('../fpdf.php');
require 'conexion.php';


$id_evidencia = $_GET["id_evidencia"];
$nombre_usuario = $_GET["user"];
$tipo = $_GET["tipo"];
//$id_evidencia = "";//$_GET["id_evidencia"];
//$nombre_usuario = "";//$_GET["user"];

//$arr_ids_tac = $_GET["arr_ids_tacticos"];
$arr_ids_tac = "186,191,201,246,217,251,71,40,42,178,122,278,234,53,62,268,269,203,176,180,266,216,27,194,272,143,65,131,87,91,235,103,214,106,97,162,164,167,185,262,114,133";
$arr_ids_est = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55";

//5

//$arr_ids_est = "31,42,35,11,7,22,8,17,16,18,23,26,32,6,27,12,37,5"; Arreglo bien
//$tipo = "array_ids";


class PDF_MC_Table extends FPDF
{

	//Cabecera y pie de pagina
	/*function Header()
	{
		// Margen del texto del documento.
		$this->SetMargins(8,65,8);
		$this->SetAutoPageBreak(true, 25);
		// Termina definición del margen.

		$this->Image('logo_hidalgo.png', 0, 0, 50 );
		$this->Image('segundoInforme.png', 158, 20, 50 );
		
		// Agregamos la fuente que vamos a ocupar.
		$this->AddFont('Graphik-Regular','','graphikRegular.php');
		$this->SetFont('Graphik-Regular','',16);
		// Color del texto.
	    $this->SetTextColor(105,105,105);
		if($this->PageNo()==1)
		{
			$this->Ln(12);
			$this->Cell(0,10,'RESUMEN',0,0,'C');
			$this->Ln(43);
		}
	}
	
	function Footer()
	{
		$this->SetY(-15);
		$this->SetFont('Arial','I', 8);
		$this->Cell(0,10, 'Pagina '.$this->PageNo().'',0,0,'C' );
	}*/

	function Header()
	{
		// Margen del texto del documento.
		$this->SetMargins(8,10,8);
		$this->SetAutoPageBreak(true, 25);
		// Termina definición del margen.
	}
	
	function Footer()
	{
		// Número de página
		$this->SetXY(8,-8.1);
		$this->SetTextColor(130,200,73);
		$this->SetFont('Arial','',11);
		$this->Cell(7,10,$this->PageNo(),0,0,'R');

		// Linea
		$this->SetDrawColor(4,47,65);
		$this->SetLineWidth(0.7);
		$this->Line(15.2,350,15.2,354);
		$this->SetLineWidth(0);

		// Texto
		$this->SetXY(15.5,-8.1);
		$this->SetTextColor(0,0,0);
		$this->SetFont('Arial','',8);
		$text = utf8_decode("RESULTADOS QUE TRANSFORMAN - TERCER AÑO DE RESULTADOS - OMAR FAYAD MENESES");
		$this->Cell(193,10,$text,0,0,'L');
	}
	//fin cabecera pie de pagina

	// Formato del texto del documento.
	function titulo($texto)
	{
		// Agregamos la fuente que vamos a ocupar.
		$this->AddFont('Graphik-SemiboldItalic','','graphikSemiboldItalic.php');
		$this->SetTextColor(0,0,0);
		$this->SetFont('Graphik-SemiboldItalic','', 12);
		$this->Row(array(utf8_decode($texto)));
	}

	function subtitulo()
	{
		// Agregamos la fuente que vamos a ocupar.
		$this->AddFont('Graphik-RegularItalic','','graphikRegularItalic.php');
		$this->SetTextColor(130,130,130);
		$this->SetFont('Graphik-RegularItalic','',12);
	}

	function contenido()
	{
		// Agregamos la fuente que vamos a ocupar.
		$this->AddFont('Graphik-Regular','','graphikRegular.php');
		$this->SetTextColor(130,130,130);
		$this->SetFont('Graphik-Regular','',12);
	}

	function contenidoInforme()
	{
		// Agregamos la fuente que vamos a ocupar.
		$this->AddFont('Graphik-Regular','','graphikRegular.php');
		$this->SetTextColor(130,130,130);
		$this->SetFont('Graphik-Regular','',8);
	}
	// Termina definición del formato.

	function columnas($bandera)
	{
		if($bandera=="1")
		{
			$this->SetWidths(array(200));
		}
		else
		{
			$this->SetWidths(array(130,5,60));
		}
	}

	function imp_lugar($arreglo)
	{
		$arr_municipios = "Acatlán,Acaxochitlán,Actopan,Agua Blanca de Iturbide,Ajacuba,Alfajayucan,Almoloya,Apan,Atitalaquia,Atlapexco,Atotonilco de Tula,Atotonilco el Grande,Calnali,Cardonal,Chapantongo,Chapulhuacán,Chilcuautla,Cuautepec de Hinojosa,El Arenal,Eloxochitlán,Emiliano Zapata,Epazoyucan,Francisco I. Madero,Huasca de Ocampo,Huautla,Huazalingo,Huehuetla,Huejutla de Reyes,Huichapan,Ixmiquilpan,Jacala de Ledezma,Jaltocán,Juárez Hidalgo,La Misión,Lolotla,Metepec,Metztitlán,Mineral de la Reforma,Mineral del Chico,Mineral del Monte,Mixquiahuala de Juárez,Molango de Escamilla,Nicolás Flores,Nopala de Villagrán,Omitlán de Juárez,Pachuca de Soto,Pacula,Pisaflores,Progreso de Obregón,San Agustín Metzquititlán,San Agustín Tlaxiaca,San Bartolo Tutotepec,San Felipe Orizatlán,San Salvador,Santiago de Anaya,Santiago Tulantepec de Lugo Guerre,Singuilucan,Tasquillo,Tecozautla,Tenango de Doria,Tepeapulco,Tepehuacán de Guerrero,Tepeji del Río de Ocampo,Tepetitlán,Tetepango,Tezontepec de Aldama,Tianguistengo,Tizayuca,Tlahuelilpan,Tlahuiltepa,Tlanalapa,Tlanchinol,Tlaxcoapan,Tolcayuca,Tula de Allende,Tulancingo de Bravo,Villa de Tezontepec,Xochiatipan,Xochicoatlán,Yahualica,Zacualtipán de angeles,Zapotlán de Juárez,Zempoala,Zimapán";

		$arr_entidades = "Aguascalientes,Baja California,Baja California Sur,Campeche,Chiapas,Chihuahua,Ciudad de México,Coahuila,Colima,Durango,Estado de México,Guanajuato,Guerrero,Hidalgo,Jalisco,Michoacán,Morelos,Nayarit,Nuevo León,Oaxaca,Puebla,Querétaro,Quintana Roo,San Luis Potosí,Sinaloa,Sonora,Tabasco,Tamaulipas,Tlaxcala,Veracruz,Yucatán,Zacatecas";

		$extraer_arr_municipios = explode(',',$arr_municipios);
		$extraer_arr_entidades = explode(',',$arr_entidades);
		$valor = explode(',',$arreglo);
		$valorSeleccion = $valor[0];

		if($valorSeleccion == "Hidalgo")
		{
			$this->Row(array(utf8_decode("Hidalgo")));
		}
		
		else if($valorSeleccion == "Municipios")
		{
			$contadorMuni = 0;
			// La función "count" nos permite contar el número de valores que contiene un arreglo.
			$iteracionMuni = count($valor);
			$iteracionMuni = $iteracionMuni-1;

			for($i=0;$i<$iteracionMuni;$i++)
			{
				for($e=0;$e<84;$e++)
				{
					if($valor[$i+1] == $e)
					{
						$val_arr_muni[$contadorMuni] = $extraer_arr_municipios[$e];
						$contadorMuni = $contadorMuni+1;
					}
				}
			}

			// La función "count" nos permite contar el número de valores que contiene un arreglo.
			$iteracionMuni = count($val_arr_muni);
			$iteracionMuni = $iteracionMuni-1;
			$cont_imp_arreglo = 0;

			for($s=0;$s<$iteracionMuni;$s++)
			{
				if($cont_imp_arreglo == 0)
				{
					$imp_arreglo = $val_arr_muni[$s];
					$cont_imp_arreglo = 1;
				}

				else
					$imp_arreglo = $imp_arreglo.", ".$val_arr_muni[$s];
			}

			$this->Row(array(utf8_decode($imp_arreglo.".")));
		}

		else if($valorSeleccion == "Entidades")
		{
			$contadorEnti = 0;
			// La función "count" nos permite contar el número de valores que contiene un arreglo.
			$iteracionEnti = count($valor);
			$iteracionEnti = $iteracionEnti-1;

			for($i=0;$i<$iteracionEnti;$i++)
			{
				for($e=0;$e<32;$e++)
				{
					if($valor[$i+1] == $e)
					{
						$val_arr_enti[$contadorEnti] = $extraer_arr_entidades[$e];
						$contadorEnti = $contadorEnti+1;
					}
				}
			}

			// La función "count" nos permite contar el número de valores que contiene un arreglo.
			$iteracionEnti = count($val_arr_enti);
			$iteracionEnti = $iteracionEnti-1;
			$cont_imp_arreglo = 0;

			for($s=0;$s<$iteracionEnti;$s++)
			{
				if($cont_imp_arreglo == 0)
				{
					$imp_arreglo = $val_arr_enti[$s];
					$cont_imp_arreglo = 1;
				}

				else
					$imp_arreglo = $imp_arreglo.", ".$val_arr_enti[$s];
			}

			$this->Row(array(utf8_decode($imp_arreglo.".")));
		}

		else
		{
			$this->Row(array(utf8_decode("Localidades")));
		}
	}

	function imp_dependencia($usuario)
	{
		$arr_dependencias = "Secretaría de Contraloría,Secretaría de Cultura,Secretaría de Educación Pública,Secretaría de Gobierno,Procuraduría General de Justicia en el Estado de Hidalgo,Secretaría de Movilidad y Transporte,Secretaría de Salud,Secretaría de Turismo,Secretaría del Trabajo y Previsión Social,Secretaría Ejecutiva de la Política Pública,Secretaría de Desarrollo Agropecuario,Secretaría de Desarrollo Económico,Secretaría de Medio Ambiente y Recursos Naturales,Unidad de Planeación y Prospectiva,Oficialía Mayor,Sistema para el Desarrollo Integral de la Familia del Estado de Hidalgo,Secretaria de Finanzas,Secretaría de Obras Públicas y Ordenamiento Territorial,Seguridad Pública,Secretaría de Desarrollo Social";

		$arr_nom_usuario = "CONTRALORIA,SEC_CULTURA,SEP,SEC_GOB,PROCURADURIA,SEC_MOVYTRANS,SEC_SALUD,SEC_TURISMO,SEC_TRABYPS,SEC_EJECUTIVA,SEC_DESAGRO,SEC_DESECON,SEMARNATH,UNI_PLANEA,OFI_MAYOR,DIFH,SEC_FINANZAS,SOPOT,SEC_SEGUP,SEDESO";

		$extraer_arr_dependencias = explode(',',$arr_dependencias);
		$extraer_arr_nom_usuario = explode(',',$arr_nom_usuario);

		for($i=0;$i<20;$i++)
		{
			if($usuario == $extraer_arr_nom_usuario[$i])
			{
				$this->RowCenter(array(" "," ",utf8_decode($extraer_arr_dependencias[$i])));
			}
		}
	}

	var $widths;
	var $aligns;

	function SetWidths($w)
	{
	    //Set the array of column widths
	    $this->widths=$w;
	}

	function SetAligns($a)
	{
	    //Set the array of column alignments
	    $this->aligns=$a;
	}

	function Row($data)
	{
	    //Calculate the height of the row
	    $nb=0;
	    for($i=0;$i<count($data);$i++)
	        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
	    $h=5*$nb;
	    //Issue a page break first if needed
	    $this->CheckPageBreak($h);
	    //Draw the cells of the row
	    for($i=0;$i<count($data);$i++)
	    {
	        $w=$this->widths[$i];
	        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'J';
	        //Save the current position
	        $x=$this->GetX();
	        $y=$this->GetY();
	        //Draw the border
	       // $this->Rect($x,$y,$w,$h);
	        //Print the text
	        $this->MultiCell($w,5,$data[$i],0,$a);
	        //Put the position to the right of the cell
	        $this->SetXY($x+$w,$y);
	    }
	    //Go to the next line
	    $this->Ln($h);
	}

	function RowCenter($data)
	{
	    //Calculate the height of the row
	    $nb=0;
	    for($i=0;$i<count($data);$i++)
	        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
	    $h=5*$nb;
	    //Issue a page break first if needed
	    $this->CheckPageBreak($h);
	    //Draw the cells of the row
	    for($i=0;$i<count($data);$i++)
	    {
	        $w=$this->widths[$i];
	        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'C';
	        //Save the current position
	        $x=$this->GetX();
	        $y=$this->GetY();
	        //Draw the border
	       // $this->Rect($x,$y,$w,$h);
	        //Print the text
	        $this->MultiCell($w,5,$data[$i],0,$a);
	        //Put the position to the right of the cell
	        $this->SetXY($x+$w,$y);
	    }
	    //Go to the next line
	    $this->Ln($h);
	}

	function CheckPageBreak($h)
	{
	    //If the height h would cause an overflow, add a new page immediately
	    if($this->GetY()+$h>$this->PageBreakTrigger)
	        $this->AddPage($this->CurOrientation);
	}

	function NbLines($w,$txt)
	{
	    //Computes the number of lines a MultiCell of width w will take
	    $cw=&$this->CurrentFont['cw'];
	    if($w==0)
	        $w=$this->w-$this->rMargin-$this->x;
	    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
	    $s=str_replace("\r",'',$txt);
	    $nb=strlen($s);
	    if($nb>0 and $s[$nb-1]=="\n")
	        $nb--;
	    $sep=-1;
	    $i=0;
	    $j=0;
	    $l=0;
	    $nl=1;
	    while($i<$nb)
	    {
	        $c=$s[$i];
	        if($c=="\n")
	        {
	            $i++;
	            $sep=-1;
	            $j=$i;
	            $l=0;
	            $nl++;
	            continue;
	        }
	        if($c==' ')
	            $sep=$i;
	        $l+=$cw[$c];
	        if($l>$wmax)
	        {
	            if($sep==-1)
	            {
	                if($i==$j)
	                    $i++;
	            }
	            else
	                $i=$sep+1;
	            $sep=-1;
	            $j=$i;
	            $l=0;
	            $nl++;
	        }
	        else
	            $i++;
	    }
	    return $nl;
	}

	function Circle($x, $y, $r, $style='D')
	{
	    $this->Ellipse($x,$y,$r,$r,$style);
	}

	function Ellipse($x, $y, $rx, $ry, $style='D')
	{
	    if($style=='F')
	        $op='f';
	    elseif($style=='FD' || $style=='DF')
	        $op='B';
	    else
	        $op='S';
	    $lx=4/3*(M_SQRT2-1)*$rx;
	    $ly=4/3*(M_SQRT2-1)*$ry;
	    $k=$this->k;
	    $h=$this->h;
	    $this->_out(sprintf('%.2F %.2F m %.2F %.2F %.2F %.2F %.2F %.2F c',
	        ($x+$rx)*$k,($h-$y)*$k,
	        ($x+$rx)*$k,($h-($y-$ly))*$k,
	        ($x+$lx)*$k,($h-($y-$ry))*$k,
	        $x*$k,($h-($y-$ry))*$k));
	    $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c',
	        ($x-$lx)*$k,($h-($y-$ry))*$k,
	        ($x-$rx)*$k,($h-($y-$ly))*$k,
	        ($x-$rx)*$k,($h-$y)*$k));
	    $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c',
	        ($x-$rx)*$k,($h-($y+$ly))*$k,
	        ($x-$lx)*$k,($h-($y+$ry))*$k,
	        $x*$k,($h-($y+$ry))*$k));
	    $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c %s',
	        ($x+$lx)*$k,($h-($y+$ry))*$k,
	        ($x+$rx)*$k,($h-($y+$ly))*$k,
	        ($x+$rx)*$k,($h-$y)*$k,
	        $op));
	}

	function Sector($xc, $yc, $r, $a, $b, $style='FD', $cw=true, $o=90)
    {
        $d0 = $a - $b;
        if($cw){
            $d = $b;
            $b = $o - $a;
            $a = $o - $d;
        }else{
            $b += $o;
            $a += $o;
        }
        while($a<0)
            $a += 360;
        while($a>360)
            $a -= 360;
        while($b<0)
            $b += 360;
        while($b>360)
            $b -= 360;
        if ($a > $b)
            $b += 360;
        $b = $b/360*2*M_PI;
        $a = $a/360*2*M_PI;
        $d = $b - $a;
        if ($d == 0 && $d0 != 0)
            $d = 2*M_PI;
        $k = $this->k;
        $hp = $this->h;
        if (sin($d/2))
            $MyArc = 4/3*(1-cos($d/2))/sin($d/2)*$r;
        else
            $MyArc = 0;
        //first put the center
        $this->_out(sprintf('%.2F %.2F m',($xc)*$k,($hp-$yc)*$k));
        //put the first point
        $this->_out(sprintf('%.2F %.2F l',($xc+$r*cos($a))*$k,(($hp-($yc-$r*sin($a)))*$k)));
        //draw the arc
        if ($d < M_PI/2){
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
        }else{
            $b = $a + $d/4;
            $MyArc = 4/3*(1-cos($d/8))/sin($d/8)*$r;
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
            $a = $b;
            $b = $a + $d/4;
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
            $a = $b;
            $b = $a + $d/4;
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
            $a = $b;
            $b = $a + $d/4;
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
        }
        //terminate drawing
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='b';
        else
            $op='s';
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3 )
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c',
            $x1*$this->k,
            ($h-$y1)*$this->k,
            $x2*$this->k,
            ($h-$y2)*$this->k,
            $x3*$this->k,
            ($h-$y3)*$this->k));
    }

}


if($tipo=="arr_tacticos_dep")
{

	$arrayEst = explode(",", $arr_ids_est);

	$pdf=new PDF_MC_Table();
	$pdf->AddPage(P,Legal);

	// Agregamos las fuentes que vamos a ocupar.
	$pdf->AddFont('Graphik_Semibold','','graphikSemibold.php');
	$pdf->AddFont('Graphik-SemiboldItalic','','graphikSemiboldItalic.php');
	$pdf->AddFont('Graphik-Regular','','graphikRegular.php');
	$pdf->AddFont('Graphik-RegularItalic','','graphikRegularItalic.php');
	$pdf->AddFont('Graphik-Bold','','graphikBold.php');

	for ($i=0; $i < count($arrayEst); $i++) 
	{

		$id=$arrayEst[$i];
		$salto = 0;

		$query = "SELECT * FROM wp_datosinforme19 WHERE Id=$id ORDER BY 'wp_datosinforme19.Id' ASC";
		$resultado = $mysqli->query($query);

		if($i == (count($arrayEst)-1))
			$salto = 1;

		while($rowEst = $resultado->fetch_assoc())
		{

			if($rowEst['eje_ped'] != NULL)
			{
			// Selección del número de columnas: 1 columna.
				$pdf->columnas($ban=1);

				$rest = substr($rowEst['eje_ped'], 0, 1);

				// Condiciones para el color de gráficas.
				if($rest=="1")
				{
					$r = 110;
					$g = 8;
					$b = 23;
				}

				else if($rest=="2")
				{
					$r = 94;
					$g = 134;
					$b = 134;
				}

				else if($rest=="3")
				{
					$r = 65;
					$g = 144;
					$b = 210;
				}

				else if($rest=="4")
				{
					$r = 13;
					$g = 69;
					$b = 63;
				}

				else if($rest=="5")
				{
					$r = 24;
					$g = 100;
					$b = 47;
				}

				else
				{
					$r = 4;
					$g = 47;
					$b = 65;
				}

				$pdf->SetXY(0,160);
				$pdf->Ln();
				$pdf->SetTextColor($r,$g,$b);
				$pdf->SetFont('Graphik_Semibold','',25);
				$pdf->RowCenter(array($rowEst['eje_ped']));
				$pdf->AddPage(P,Legal);
			}

			if($rowEst['indicador_est'] != NULL)
			{
			// Cambio del número de columnas: De 1 columna a 3 columnas.
				$pdf->columnas($ban=3);

				$pdf->SetXY(0,0);
				$pdf->Ln();
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFont('Graphik-SemiboldItalic','',12);
				$pdf->RowCenter(array(" "," ",utf8_decode("Indicador estratégico")));
				$pdf->Ln();
				$pdf->SetTextColor(130,130,130);
				$pdf->SetFont('Graphik-Regular','',12);
				$pdf->RowCenter(array(" "," ",$rowEst['indicador_est']));
				$pdf->Ln();

				$yGraficas = $pdf->GetY();
			}

			if($rowEst['ind_tactico'] != NULL && $rowEst['indicador_est'] == NULL)
			{
			// Cambio del número de columnas: De 1 columna a 3 columnas.
				$pdf->columnas($ban=3);

				$pdf->SetXY(0,0);
				$pdf->Ln();
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFont('Graphik-SemiboldItalic','',12);
				$pdf->RowCenter(array(" "," ",utf8_decode("Indicador táctico")));
				$pdf->Ln();
				$pdf->SetTextColor(130,130,130);
				$pdf->SetFont('Graphik-Regular','',12);
				$pdf->RowCenter(array(" "," ",$rowEst['ind_tactico']));
				$pdf->Ln();

				$yGraficas = $pdf->GetY();
			}

			if($rowEst['linea_base'] != NULL || $rowEst['meta_2022'] != NULL || $rowEst['meta_2030'] != NULL)
			{
			// GRÁFICAS - INDICADORES ESTRATEGICOS
			// Gráfica #1.

				// Barra de fondo (color gris).
				$pdf->SetFillColor(196,196,196);
				$pdf->Rect(160,$yGraficas+5,28,2.5,'F');
				
				// Barra de porcentaje.
				$pdf->SetFillColor($r,$g,$b);

				if($rowEst['linea_base']=="ND" || $rowEst['linea_base']=="N.D." || $rowEst['linea_base']==" " || $rowEst['linea_base']==NULL || $rowEst['linea_base']=="0")
				{
					$pdf->Rect(160,$yGraficas+5,0,2.5,'F');
					$imp = "N.D.";
					$impND = 1;
				}

				// La función "strpos" nos permite buscar un caracter específico en la cadena indicada, en este caso "%".
				else if(strpos($rowEst['linea_base'],'%')!==false)
				{
					$valor = explode('%',$rowEst['linea_base']);
					$valor2 = $valor[0];
					$imp = round($valor2, 2);
					$graf = ($valor2*28)/100;

					if($imp>100)
					{
						//$imp = 100;
						$pdf->Rect(160,$yGraficas+5,28,2.5,'F');
					}

					else
						$pdf->Rect(160,$yGraficas+5,$graf,2.5,'F');

					$impND = 0;
				}

				else
				{
					if($rowEst['linea_base']=="Baja")
					{
						$pdf->Rect(160,$yGraficas+5,9.33,2.5,'F');
						$imp = "Baja";
					}

					else if($rowEst['linea_base']=="Media")
					{
						$pdf->Rect(160,$yGraficas+5,18.66,2.5,'F');
						$imp = "Media";
					}

					else if($rowEst['linea_base']=="Alta")
					{
						$pdf->Rect(160,$yGraficas+5,28,2.5,'F');
						$imp = "Alta";
					}

					else
					{
						if($rowEst['meta_2030']==NULL)
						{
							$porcentaje = ($rowEst['linea_base']*100)/1;
							// La función "round" nos permite delimitar el número de decimales que queremos imprimir.
							$imp = $rowEst['linea_base'];
							$graf = ($porcentaje*360)/100;

							if($porcentaje>100)
								$pdf->Rect(160,$yGraficas+5,28,2.5,'F');

							else
								$pdf->Rect(160,$yGraficas+5,$graf,2.5,'F');
						}


						else
						{
							$porcentaje = ($rowEst['linea_base']*100)/$rowEst['meta_2030'];
							// La función "round" nos permite delimitar el número de decimales que queremos imprimir.
							$imp = $rowEst['linea_base'];
							$graf = ($porcentaje*28)/100;

							if($porcentaje>100)
								$pdf->Rect(160,$yGraficas+5,28,2.5,'F');

							else
								$pdf->Rect(160,$yGraficas+5,$graf,2.5,'F');
						}
					}

					$impND = 1;
				}

				// Lineas
				$pdf->SetDrawColor(255,255,255);
				$pdf->Line(162.8,$yGraficas+5,162.8,$yGraficas+7.5);
				$pdf->Line(165.6,$yGraficas+5,165.6,$yGraficas+7.5);
				$pdf->Line(168.4,$yGraficas+5,168.4,$yGraficas+7.5);
				$pdf->Line(171.2,$yGraficas+5,171.2,$yGraficas+7.5);
				$pdf->Line(174,$yGraficas+5,174,$yGraficas+7.5);
				$pdf->Line(176.8,$yGraficas+5,176.8,$yGraficas+7.5);
				$pdf->Line(179.6,$yGraficas+5,179.6,$yGraficas+7.5);
				$pdf->Line(182.4,$yGraficas+5,182.4,$yGraficas+7.5);
				$pdf->Line(185.2,$yGraficas+5,185.2,$yGraficas+7.5);
				// Texto
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFont('Graphik-Bold','',11);
				if($impND == 1)
					$pdf->Text(189.5,$yGraficas+7.6,$imp);

				else
					$pdf->Text(189.5,$yGraficas+7.6,$imp." "."%");

				$pdf->SetFont('Graphik-Regular','',8.5);
				$pdf->Text(144,$yGraficas+7.2,utf8_decode("Línea base"));
				// Se agrego 1.7, anterior (5.5) ahora (7.2).
				//$pdf->Text(148,$yGraficas+8.5,utf8_decode("2016"));

			// Gráfica #2.

				// Barra de fondo (color gris).
				$pdf->SetFillColor(196,196,196);
				$pdf->Rect(160,$yGraficas+15,28,2.5,'F');
				
				// Barra de porcentaje.
				$pdf->SetFillColor($r,$g,$b);

				if($rowEst['meta_2022']=="ND" || $rowEst['meta_2022']=="N.D." || $rowEst['meta_2022']==" " || $rowEst['meta_2022']==NULL || $rowEst['meta_2022']=="0")
				{
					$pdf->Rect(160,$yGraficas+15,0,2.5,'F');
					$imp = "N.D.";
					$impND = 1;
				}

				// La función "strpos" nos permite buscar un caracter específico en la cadena indicada, en este caso "%".
				else if(strpos($rowEst['meta_2022'],'%')!==false)
				{
					$valor = explode('%',$rowEst['meta_2022']);
					$valor2 = $valor[0];
					$imp = round($valor2, 2);
					$graf = ($valor2*28)/100;

					if($imp>100)
					{
						//$imp = 100;
						$pdf->Rect(160,$yGraficas+15,28,2.5,'F');
					}

					else
						$pdf->Rect(160,$yGraficas+15,$graf,2.5,'F');

					$impND = 0;
				}

				else
				{
					if($rowEst['meta_2022']=="Baja")
					{
						$pdf->Rect(160,$yGraficas+15,9.33,2.5,'F');
						$imp = "Baja";
					}

					else if($rowEst['meta_2022']=="Media")
					{
						$pdf->Rect(160,$yGraficas+15,18.66,2.5,'F');
						$imp = "Media";
					}

					else if($rowEst['meta_2022']=="Alta")
					{
						$pdf->Rect(160,$yGraficas+15,28,2.5,'F');
						$imp = "Alta";
					}

					else
					{
						if($rowEst['meta_2030']==NULL)
						{
							$porcentaje = ($rowEst['meta_2022']*100)/1;
							// La función "round" nos permite delimitar el número de decimales que queremos imprimir.
							$imp = $rowEst['meta_2022'];
							$graf = ($porcentaje*360)/100;

							if($porcentaje>100)
								$pdf->Rect(160,$yGraficas+15,28,2.5,'F');

							else
								$pdf->Rect(160,$yGraficas+15,$graf,2.5,'F');
						}


						else
						{
							$porcentaje = ($rowEst['meta_2022']*100)/$rowEst['meta_2030'];
							// La función "round" nos permite delimitar el número de decimales que queremos imprimir.
							$imp = $rowEst['meta_2022'];
							$graf = ($porcentaje*28)/100;

							if($porcentaje>100)
								$pdf->Rect(160,$yGraficas+15,28,2.5,'F');

							else
								$pdf->Rect(160,$yGraficas+15,$graf,2.5,'F');
						}
					}

					$impND = 1;
				}

				// Lineas
				$pdf->SetDrawColor(255,255,255);
				$pdf->Line(162.8,$yGraficas+15,162.8,$yGraficas+17.5);
				$pdf->Line(165.6,$yGraficas+15,165.6,$yGraficas+17.5);
				$pdf->Line(168.4,$yGraficas+15,168.4,$yGraficas+17.5);
				$pdf->Line(171.2,$yGraficas+15,171.2,$yGraficas+17.5);
				$pdf->Line(174,$yGraficas+15,174,$yGraficas+17.5);
				$pdf->Line(176.8,$yGraficas+15,176.8,$yGraficas+17.5);
				$pdf->Line(179.6,$yGraficas+15,179.6,$yGraficas+17.5);
				$pdf->Line(182.4,$yGraficas+15,182.4,$yGraficas+17.5);
				$pdf->Line(185.2,$yGraficas+15,185.2,$yGraficas+17.5);
				// Texto
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFont('Graphik-Bold','',11);
				if($impND == 1)
					$pdf->Text(189.5,$yGraficas+17.6,$imp);

				else
					$pdf->Text(189.5,$yGraficas+17.6,$imp." "."%");

				$pdf->SetFont('Graphik-Regular','',8.5);
				$pdf->Text(148.2,$yGraficas+15.5,utf8_decode("Meta"));
				$pdf->Text(148.3,$yGraficas+18.5,utf8_decode("2022"));

			// Gráfica #3.

				// Barra de fondo (color gris).
				$pdf->SetFillColor(196,196,196);
				$pdf->Rect(160,$yGraficas+25,28,2.5,'F');
				
				// Barra de porcentaje.
				$pdf->SetFillColor($r,$g,$b);

				if($rowEst['meta_2030']=="ND" || $rowEst['meta_2030']=="N.D." || $rowEst['meta_2030']==" " || $rowEst['meta_2030']==NULL || $rowEst['meta_2030']=="0")
				{
					$pdf->Rect(160,$yGraficas+25,0,2.5,'F');
					$imp = "N.D.";
					$impND = 1;
				}

				// La función "strpos" nos permite buscar un caracter específico en la cadena indicada, en este caso "%".
				else if(strpos($rowEst['meta_2030'],'%')!==false)
				{
					$valor = explode('%',$rowEst['meta_2030']);
					$valor2 = $valor[0];
					$imp = round($valor2, 2);
					$graf = ($valor2*28)/100;

					if($imp>100)
					{
						//$imp = 100;
						$pdf->Rect(160,$yGraficas+25,28,2.5,'F');
					}

					else
						$pdf->Rect(160,$yGraficas+25,$graf,2.5,'F');

					$impND = 0;
				}

				else
				{
					if($rowEst['meta_2030']=="Baja")
					{
						$pdf->Rect(160,$yGraficas+25,9.33,2.5,'F');
						$imp = "Baja";
					}

					else if($rowEst['meta_2030']=="Media")
					{
						$pdf->Rect(160,$yGraficas+25,18.66,2.5,'F');
						$imp = "Media";
					}

					else if($rowEst['meta_2030']=="Alta")
					{
						$pdf->Rect(160,$yGraficas+25,28,2.5,'F');
						$imp = "Alta";
					}

					else
					{
						$porcentaje = ($rowEst['meta_2030']*100)/$rowEst['meta_2030'];
						// La función "round" nos permite delimitar el número de decimales que queremos imprimir.
						$imp = $rowEst['meta_2030'];
						$graf = ($porcentaje*28)/100;

						if($porcentaje>100)
							$pdf->Rect(160,$yGraficas+25,28,2.5,'F');

						else
							$pdf->Rect(160,$yGraficas+25,$graf,2.5,'F');
					}

					$impND = 1;
				}

				// Lineas
				$pdf->SetDrawColor(255,255,255);
				$pdf->Line(162.8,$yGraficas+25,162.8,$yGraficas+27.5);
				$pdf->Line(165.6,$yGraficas+25,165.6,$yGraficas+27.5);
				$pdf->Line(168.4,$yGraficas+25,168.4,$yGraficas+27.5);
				$pdf->Line(171.2,$yGraficas+25,171.2,$yGraficas+27.5);
				$pdf->Line(174,$yGraficas+25,174,$yGraficas+27.5);
				$pdf->Line(176.8,$yGraficas+25,176.8,$yGraficas+27.5);
				$pdf->Line(179.6,$yGraficas+25,179.6,$yGraficas+27.5);
				$pdf->Line(182.4,$yGraficas+25,182.4,$yGraficas+27.5);
				$pdf->Line(185.2,$yGraficas+25,185.2,$yGraficas+27.5);
				// Texto
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFont('Graphik-Bold','',11);
				if($impND == 1)
					$pdf->Text(189.5,$yGraficas+27.6,$imp);

				else
					$pdf->Text(189.5,$yGraficas+27.6,$imp." "."%");
				

				$pdf->SetFont('Graphik-Regular','',8.5);
				$pdf->Text(148.2,$yGraficas+25.5,utf8_decode("Meta"));
				$pdf->Text(148,$yGraficas+28.5,utf8_decode("2030"));

			// Gráfica #4.

				// Barra de fondo (color gris).
				$pdf->SetFillColor(196,196,196);
				$pdf->Rect(160,$yGraficas+35,28,2.5,'F');
				
				// Barra de porcentaje.
				$pdf->SetFillColor($r,$g,$b);

				if($rowEst['resultado2019_est']=="ND" || $rowEst['resultado2019_est']=="N.D." || $rowEst['resultado2019_est']==" " || $rowEst['resultado2019_est']==NULL || $rowEst['resultado2019_est']=="0")
				{
					$pdf->Rect(160,$yGraficas+35,0,2.5,'F');
					$imp = "N.D.";
					$impND = 1;
				}

				// La función "strpos" nos permite buscar un caracter específico en la cadena indicada, en este caso "%".
				else if(strpos($rowEst['resultado2019_est'],'%')!==false)
				{
					$valor = explode('%',$rowEst['resultado2019_est']);
					$valor2 = $valor[0];
					$imp = round($valor2, 2);
					$graf = ($valor2*28)/100;

					if($imp>100)
					{
						//$imp = 100;
						$pdf->Rect(160,$yGraficas+35,28,2.5,'F');
					}

					else
						$pdf->Rect(160,$yGraficas+35,$graf,2.5,'F');

					$impND = 0;
				}

				else
				{
					if($rowEst['resultado2019_est']=="Baja")
					{
						$pdf->Rect(160,$yGraficas+35,9.33,2.5,'F');
						$imp = "Baja";
					}

					else if($rowEst['resultado2019_est']=="Media")
					{
						$pdf->Rect(160,$yGraficas+35,18.66,2.5,'F');
						$imp = "Media";
					}

					else if($rowEst['resultado2019_est']=="Alta")
					{
						$pdf->Rect(160,$yGraficas+35,28,2.5,'F');
						$imp = "Alta";
					}

					else
					{
						$porcentaje = ($rowEst['resultado2019_est']*100)/$rowEst['meta_2030'];
						// La función "round" nos permite delimitar el número de decimales que queremos imprimir.
						$imp = $rowEst['resultado2019_est'];
						$graf = ($porcentaje*28)/100;

						if($porcentaje>100)
							$pdf->Rect(160,$yGraficas+35,28,2.5,'F');

						else
							$pdf->Rect(160,$yGraficas+35,$graf,2.5,'F');
					}

					$impND = 1;
				}

				// Lineas
				$pdf->SetDrawColor(255,255,255);
				$pdf->Line(162.8,$yGraficas+35,162.8,$yGraficas+37.5);
				$pdf->Line(165.6,$yGraficas+35,165.6,$yGraficas+37.5);
				$pdf->Line(168.4,$yGraficas+35,168.4,$yGraficas+37.5);
				$pdf->Line(171.2,$yGraficas+35,171.2,$yGraficas+37.5);
				$pdf->Line(174,$yGraficas+35,174,$yGraficas+37.5);
				$pdf->Line(176.8,$yGraficas+35,176.8,$yGraficas+37.5);
				$pdf->Line(179.6,$yGraficas+35,179.6,$yGraficas+37.5);
				$pdf->Line(182.4,$yGraficas+35,182.4,$yGraficas+37.5);
				$pdf->Line(185.2,$yGraficas+35,185.2,$yGraficas+37.5);
				// Texto
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFont('Graphik-Bold','',11);
				if($impND == 1)
					$pdf->Text(189.5,$yGraficas+37.6,$imp);

				else
					$pdf->Text(189.5,$yGraficas+37.6,$imp." "."%");
				

				$pdf->SetFont('Graphik-Regular','',8.5);
				$pdf->Text(144,$yGraficas+35.5,utf8_decode("Resultados"));
				$pdf->Text(148.5,$yGraficas+38.5,$rowEst['anio_rest']);

			// FUENTE
				if($rowEst['fntestrat'] != NULL && $rowEst['fntestrat'] != " ")
				{
					$pdf->SetXY(0,$yGraficas+40.5);
					$pdf->Ln();
					$pdf->SetTextColor(0,0,0);
					$pdf->SetFont('Graphik-Regular','',7);
					$pdf->Row(array(" "," ","Fuente: ".$rowEst['fntestrat']));
				}

			// ODS
				if($rowEst['odsestrat'] != NULL && $rowEst['odsestrat'] != " ")
				{
					$pdf->Row(array(" "," ","Alineado con el Objetivo de Desarrollo Sostenible: ".$rowEst['odsestrat']));
					$pdf->Ln();
					$pdf->Ln();
				}

				if($rowEst['ind_tactico'] != NULL)
				{
				// Título del "Indicador táctico".

					$pdf->SetTextColor(0,0,0);
					$pdf->SetFont('Graphik-SemiboldItalic','',12);
					$pdf->RowCenter(array(" "," ",utf8_decode("Indicador táctico")));
					$pdf->Ln();
					$pdf->SetTextColor(130,130,130);
					$pdf->SetFont('Graphik-Regular','',12);
					$pdf->RowCenter(array(" "," ",$rowEst['ind_tactico']));
					$pdf->Ln();

					$yyGraficas = $pdf->GetY();

				// GRÁFICAS - INDICADORES TÁCTICOS
				// Gráfica #1.

					// Barra de fondo (color gris).
					$pdf->SetFillColor(196,196,196);
					$pdf->Rect(160,$yyGraficas+5,28,2.5,'F');
					
					// Barra de porcentaje.
					$pdf->SetFillColor($r,$g,$b);

					if($rowEst['linea_base_tactico']=="ND" || $rowEst['linea_base_tactico']=="N.D." || $rowEst['linea_base_tactico']==" " || $rowEst['linea_base_tactico']==NULL || $rowEst['linea_base_tactico']=="0")
					{
						$pdf->Rect(160,$yyGraficas+5,0,2.5,'F');
						$imp = "N.D.";
						$impND = 1;
					}

					// La función "strpos" nos permite buscar un caracter específico en la cadena indicada, en este caso "%".
					else if(strpos($rowEst['linea_base_tactico'],'%')!==false)
					{
						$valor = explode('%',$rowEst['linea_base_tactico']);
						$valor2 = $valor[0];
						$imp = round($valor2, 2);
						$graf = ($valor2*28)/100;

						if($imp>100)
						{
							//$imp = 100;
							$pdf->Rect(160,$yyGraficas+5,28,2.5,'F');
						}

						else
							$pdf->Rect(160,$yyGraficas+5,$graf,2.5,'F');

						$impND = 0;
					}

					else
					{
						$porcentaje = ($rowEst['linea_base_tactico']*100)/$rowEst['meta_2019_tactico'];
						// La función "round" nos permite delimitar el número de decimales que queremos imprimir.
						$imp = $rowEst['linea_base_tactico'];
						$graf = ($porcentaje*28)/100;

						if($porcentaje>100)
							$pdf->Rect(160,$yyGraficas+5,28,2.5,'F');

						else
							$pdf->Rect(160,$yyGraficas+5,$graf,2.5,'F');

						$impND = 1;
					}

					// Lineas
					$pdf->SetDrawColor(255,255,255);
					$pdf->Line(162.8,$yyGraficas+5,162.8,$yyGraficas+7.5);
					$pdf->Line(165.6,$yyGraficas+5,165.6,$yyGraficas+7.5);
					$pdf->Line(168.4,$yyGraficas+5,168.4,$yyGraficas+7.5);
					$pdf->Line(171.2,$yyGraficas+5,171.2,$yyGraficas+7.5);
					$pdf->Line(174,$yyGraficas+5,174,$yyGraficas+7.5);
					$pdf->Line(176.8,$yyGraficas+5,176.8,$yyGraficas+7.5);
					$pdf->Line(179.6,$yyGraficas+5,179.6,$yyGraficas+7.5);
					$pdf->Line(182.4,$yyGraficas+5,182.4,$yyGraficas+7.5);
					$pdf->Line(185.2,$yyGraficas+5,185.2,$yyGraficas+7.5);
					// Texto
					$pdf->SetTextColor(0,0,0);
					$pdf->SetFont('Graphik-Bold','',11);
					if($impND == 1)
						$pdf->Text(189.5,$yyGraficas+7.6,$imp);

					else
						$pdf->Text(189.5,$yyGraficas+7.6,$imp." "."%");

					$pdf->SetFont('Graphik-Regular','',8.5);
					$pdf->Text(144,$yyGraficas+7.2,utf8_decode("Línea base"));
					// Se agrego 1.7, anterior (5.5) ahora (7.2).
					//$pdf->Text(148,$yyGraficas+8.5,utf8_decode("2016"));

				// Gráfica #2.

					// Barra de fondo (color gris).
					$pdf->SetFillColor(196,196,196);
					$pdf->Rect(160,$yyGraficas+15,28,2.5,'F');
					
					// Barra de porcentaje.
					$pdf->SetFillColor($r,$g,$b);

					if($rowEst['meta_2019_tactico']=="ND" || $rowEst['meta_2019_tactico']=="N.D." || $rowEst['meta_2019_tactico']==" " || $rowEst['meta_2019_tactico']==NULL || $rowEst['meta_2019_tactico']=="0")
					{
						$pdf->Rect(160,$yyGraficas+15,0,2.5,'F');
						$imp = "N.D.";
						$impND = 1;
					}

					// La función "strpos" nos permite buscar un caracter específico en la cadena indicada, en este caso "%".
					else if(strpos($rowEst['meta_2019_tactico'],'%')!==false)
					{
						$valor = explode('%',$rowEst['meta_2019_tactico']);
						$valor2 = $valor[0];
						$imp = round($valor2, 2);
						$graf = ($valor2*28)/100;

						if($imp>100)
						{
							//$imp = 100;
							$pdf->Rect(160,$yyGraficas+15,28,2.5,'F');
						}

						else
							$pdf->Rect(160,$yyGraficas+15,$graf,2.5,'F');

						$impND = 0;
					}

					else
					{
						$porcentaje = ($rowEst['meta_2019_tactico']*100)/$rowEst['meta_2019_tactico'];
						// La función "round" nos permite delimitar el número de decimales que queremos imprimir.
						$imp = $rowEst['meta_2019_tactico'];
						$graf = ($porcentaje*28)/100;

						if($porcentaje>100)
							$pdf->Rect(160,$yyGraficas+15,28,2.5,'F');

						else
							$pdf->Rect(160,$yyGraficas+15,$graf,2.5,'F');

						$impND = 1;
					}

					// Lineas
					$pdf->SetDrawColor(255,255,255);
					$pdf->Line(162.8,$yyGraficas+15,162.8,$yyGraficas+17.5);
					$pdf->Line(165.6,$yyGraficas+15,165.6,$yyGraficas+17.5);
					$pdf->Line(168.4,$yyGraficas+15,168.4,$yyGraficas+17.5);
					$pdf->Line(171.2,$yyGraficas+15,171.2,$yyGraficas+17.5);
					$pdf->Line(174,$yyGraficas+15,174,$yyGraficas+17.5);
					$pdf->Line(176.8,$yyGraficas+15,176.8,$yyGraficas+17.5);
					$pdf->Line(179.6,$yyGraficas+15,179.6,$yyGraficas+17.5);
					$pdf->Line(182.4,$yyGraficas+15,182.4,$yyGraficas+17.5);
					$pdf->Line(185.2,$yyGraficas+15,185.2,$yyGraficas+17.5);
					// Texto
					$pdf->SetTextColor(0,0,0);
					$pdf->SetFont('Graphik-Bold','',11);
					if($impND == 1)
						$pdf->Text(189.5,$yyGraficas+17.6,$imp);

					else
						$pdf->Text(189.5,$yyGraficas+17.6,$imp." "."%");

					$pdf->SetFont('Graphik-Regular','',8.5);
					$pdf->Text(148.2,$yyGraficas+15.5,utf8_decode("Meta"));
					$pdf->Text(148.5,$yyGraficas+18.5,utf8_decode("2019"));

				// Gráfica #3.

					// Barra de fondo (color gris).
					$pdf->SetFillColor(196,196,196);
					$pdf->Rect(160,$yyGraficas+25,28,2.5,'F');
					
					// Barra de porcentaje.
					$pdf->SetFillColor($r,$g,$b);

					if($rowEst['resultado_19_tactico']=="ND" || $rowEst['resultado_19_tactico']=="N.D." || $rowEst['resultado_19_tactico']==" " || $rowEst['resultado_19_tactico']==NULL || $rowEst['resultado_19_tactico']=="0")
					{
						$pdf->Rect(160,$yyGraficas+25,0,2.5,'F');
						$imp = "N.D.";
						$impND = 1;
					}

					// La función "strpos" nos permite buscar un caracter específico en la cadena indicada, en este caso "%".
					else if(strpos($rowEst['resultado_19_tactico'],'%')!==false)
					{
						$valor = explode('%',$rowEst['resultado_19_tactico']);
						$valor2 = $valor[0];
						$imp = round($valor2, 2);
						$graf = ($valor2*28)/100;

						if($imp>100)
						{
							//$imp = 100;
							$pdf->Rect(160,$yyGraficas+25,28,2.5,'F');
						}

						else
							$pdf->Rect(160,$yyGraficas+25,$graf,2.5,'F');

						$impND = 0;
					}

					else
					{
						$porcentaje = ($rowEst['resultado_19_tactico']*100)/$rowEst['meta_2019_tactico'];
						// La función "round" nos permite delimitar el número de decimales que queremos imprimir.
						$imp = $rowEst['resultado_19_tactico'];
						$graf = ($porcentaje*28)/100;

						if($porcentaje>100)
							$pdf->Rect(160,$yyGraficas+25,28,2.5,'F');

						else
							$pdf->Rect(160,$yyGraficas+25,$graf,2.5,'F');

						$impND = 1;
					}

					// Lineas
					$pdf->SetDrawColor(255,255,255);
					$pdf->Line(162.8,$yyGraficas+25,162.8,$yyGraficas+27.5);
					$pdf->Line(165.6,$yyGraficas+25,165.6,$yyGraficas+27.5);
					$pdf->Line(168.4,$yyGraficas+25,168.4,$yyGraficas+27.5);
					$pdf->Line(171.2,$yyGraficas+25,171.2,$yyGraficas+27.5);
					$pdf->Line(174,$yyGraficas+25,174,$yyGraficas+27.5);
					$pdf->Line(176.8,$yyGraficas+25,176.8,$yyGraficas+27.5);
					$pdf->Line(179.6,$yyGraficas+25,179.6,$yyGraficas+27.5);
					$pdf->Line(182.4,$yyGraficas+25,182.4,$yyGraficas+27.5);
					$pdf->Line(185.2,$yyGraficas+25,185.2,$yyGraficas+27.5);
					// Texto
					$pdf->SetTextColor(0,0,0);
					$pdf->SetFont('Graphik-Bold','',11);
					if($impND == 1)
						$pdf->Text(189.5,$yyGraficas+27.6,$imp);

					else
						$pdf->Text(189.5,$yyGraficas+27.6,$imp." "."%");

					$pdf->SetFont('Graphik-Regular','',8.5);
					$pdf->Text(144,$yyGraficas+25.5,utf8_decode("Resultados"));
					$pdf->Text(148.5,$yyGraficas+28.5,utf8_decode("2019"));

				// FUENTE
					if($rowEst['fntetactc'] != NULL && $rowEst['fntetactc'] != " ")
					{
						$pdf->SetXY(0,$yyGraficas+30.5);
						$pdf->Ln();
						$pdf->SetTextColor(0,0,0);
						$pdf->SetFont('Graphik-Regular','',7);
						$pdf->Row(array(" "," ","Fuente: ".$rowEst['fntetactc']));
					}

				// ODS
					if($rowEst['odstactc'] != NULL && $rowEst['odstactc'] != " ")
						$pdf->Row(array(" "," ","Alineado con el Objetivo de Desarrollo Sostenible: ".$rowEst['odstactc']));
				}
			}

			if($rowEst['indicador_est'] == NULL && $rowEst['ind_tactico'] != NULL)
			{
			// GRÁFICAS - INDICADORES TÁCTICOS
			// Gráfica #1.

				// Barra de fondo (color gris).
				$pdf->SetFillColor(196,196,196);
				$pdf->Rect(160,$yGraficas+5,28,2.5,'F');
				
				// Barra de porcentaje.
				$pdf->SetFillColor($r,$g,$b);

				if($rowEst['linea_base_tactico']=="ND" || $rowEst['linea_base_tactico']=="N.D." || $rowEst['linea_base_tactico']==" " || $rowEst['linea_base_tactico']==NULL || $rowEst['linea_base_tactico']=="0")
				{
					$pdf->Rect(160,$yGraficas+5,0,2.5,'F');
					$imp = "N.D.";
					$impND = 1;
				}

				// La función "strpos" nos permite buscar un caracter específico en la cadena indicada, en este caso "%".
				else if(strpos($rowEst['linea_base_tactico'],'%')!==false)
				{
					$valor = explode('%',$rowEst['linea_base_tactico']);
					$valor2 = $valor[0];
					$imp = round($valor2, 2);
					$graf = ($valor2*28)/100;

					if($imp>100)
					{
						//$imp = 100;
						$pdf->Rect(160,$yGraficas+5,28,2.5,'F');
					}

					else
						$pdf->Rect(160,$yGraficas+5,$graf,2.5,'F');

					$impND = 0;
				}

				else
				{
					$porcentaje = ($rowEst['linea_base_tactico']*100)/$rowEst['meta_2019_tactico'];
					// La función "round" nos permite delimitar el número de decimales que queremos imprimir.
					$imp = $rowEst['linea_base_tactico'];
					$graf = ($porcentaje*28)/100;

					if($porcentaje>100)
						$pdf->Rect(160,$yGraficas+5,28,2.5,'F');

					else
						$pdf->Rect(160,$yGraficas+5,$graf,2.5,'F');

					$impND = 1;
				}

				// Lineas
				$pdf->SetDrawColor(255,255,255);
				$pdf->Line(162.8,$yGraficas+5,162.8,$yGraficas+7.5);
				$pdf->Line(165.6,$yGraficas+5,165.6,$yGraficas+7.5);
				$pdf->Line(168.4,$yGraficas+5,168.4,$yGraficas+7.5);
				$pdf->Line(171.2,$yGraficas+5,171.2,$yGraficas+7.5);
				$pdf->Line(174,$yGraficas+5,174,$yGraficas+7.5);
				$pdf->Line(176.8,$yGraficas+5,176.8,$yGraficas+7.5);
				$pdf->Line(179.6,$yGraficas+5,179.6,$yGraficas+7.5);
				$pdf->Line(182.4,$yGraficas+5,182.4,$yGraficas+7.5);
				$pdf->Line(185.2,$yGraficas+5,185.2,$yGraficas+7.5);
				// Texto
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFont('Graphik-Bold','',11);
				if($impND == 1)
					$pdf->Text(189.5,$yGraficas+7.6,$imp);

				else
					$pdf->Text(189.5,$yGraficas+7.6,$imp." "."%");

				$pdf->SetFont('Graphik-Regular','',8.5);
				$pdf->Text(144,$yGraficas+7.2,utf8_decode("Línea base"));
				// Se agrego 1.7, anterior (5.5) ahora (7.2).
				//$pdf->Text(148,$yGraficas+8.5,utf8_decode("2016"));

			// Gráfica #2.

				// Barra de fondo (color gris).
				$pdf->SetFillColor(196,196,196);
				$pdf->Rect(160,$yGraficas+15,28,2.5,'F');
				
				// Barra de porcentaje.
				$pdf->SetFillColor($r,$g,$b);

				if($rowEst['meta_2019_tactico']=="ND" || $rowEst['meta_2019_tactico']=="N.D." || $rowEst['meta_2019_tactico']==" " || $rowEst['meta_2019_tactico']==NULL || $rowEst['meta_2019_tactico']=="0")
				{
					$pdf->Rect(160,$yGraficas+15,0,2.5,'F');
					$imp = "N.D.";
					$impND = 1;
				}

				// La función "strpos" nos permite buscar un caracter específico en la cadena indicada, en este caso "%".
				else if(strpos($rowEst['meta_2019_tactico'],'%')!==false)
				{
					$valor = explode('%',$rowEst['meta_2019_tactico']);
					$valor2 = $valor[0];
					$imp = round($valor2, 2);
					$graf = ($valor2*28)/100;

					if($imp>100)
					{
						//$imp = 100;
						$pdf->Rect(160,$yGraficas+15,28,2.5,'F');
					}

					else
						$pdf->Rect(160,$yGraficas+15,$graf,2.5,'F');

					$impND = 0;
				}

				else
				{
					$porcentaje = ($rowEst['meta_2019_tactico']*100)/$rowEst['meta_2019_tactico'];
					// La función "round" nos permite delimitar el número de decimales que queremos imprimir.
					$imp = $rowEst['meta_2019_tactico'];
					$graf = ($porcentaje*28)/100;

					if($porcentaje>100)
						$pdf->Rect(160,$yGraficas+15,28,2.5,'F');

					else
						$pdf->Rect(160,$yGraficas+15,$graf,2.5,'F');

					$impND = 1;
				}

				// Lineas
				$pdf->SetDrawColor(255,255,255);
				$pdf->Line(162.8,$yGraficas+15,162.8,$yGraficas+17.5);
				$pdf->Line(165.6,$yGraficas+15,165.6,$yGraficas+17.5);
				$pdf->Line(168.4,$yGraficas+15,168.4,$yGraficas+17.5);
				$pdf->Line(171.2,$yGraficas+15,171.2,$yGraficas+17.5);
				$pdf->Line(174,$yGraficas+15,174,$yGraficas+17.5);
				$pdf->Line(176.8,$yGraficas+15,176.8,$yGraficas+17.5);
				$pdf->Line(179.6,$yGraficas+15,179.6,$yGraficas+17.5);
				$pdf->Line(182.4,$yGraficas+15,182.4,$yGraficas+17.5);
				$pdf->Line(185.2,$yGraficas+15,185.2,$yGraficas+17.5);
				// Texto
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFont('Graphik-Bold','',11);
				if($impND == 1)
					$pdf->Text(189.5,$yGraficas+17.6,$imp);

				else
					$pdf->Text(189.5,$yGraficas+17.6,$imp." "."%");

				$pdf->SetFont('Graphik-Regular','',8.5);
				$pdf->Text(148.2,$yGraficas+15.5,utf8_decode("Meta"));
				$pdf->Text(148.5,$yGraficas+18.5,utf8_decode("2019"));

			// Gráfica #3.

				// Barra de fondo (color gris).
				$pdf->SetFillColor(196,196,196);
				$pdf->Rect(160,$yGraficas+25,28,2.5,'F');
				
				// Barra de porcentaje.
				$pdf->SetFillColor($r,$g,$b);

				if($rowEst['resultado_19_tactico']=="ND" || $rowEst['resultado_19_tactico']=="N.D." || $rowEst['resultado_19_tactico']==" " || $rowEst['resultado_19_tactico']==NULL || $rowEst['resultado_19_tactico']=="0")
				{
					$pdf->Rect(160,$yGraficas+25,0,2.5,'F');
					$imp = "N.D.";
					$impND = 1;
				}

				// La función "strpos" nos permite buscar un caracter específico en la cadena indicada, en este caso "%".
				else if(strpos($rowEst['resultado_19_tactico'],'%')!==false)
				{
					$valor = explode('%',$rowEst['resultado_19_tactico']);
					$valor2 = $valor[0];
					$imp = round($valor2, 2);
					$graf = ($valor2*28)/100;

					if($imp>100)
					{
						//$imp = 100;
						$pdf->Rect(160,$yGraficas+25,28,2.5,'F');
					}

					else
						$pdf->Rect(160,$yGraficas+25,$graf,2.5,'F');

					$impND = 0;
				}

				else
				{
					$porcentaje = ($rowEst['resultado_19_tactico']*100)/$rowEst['meta_2019_tactico'];
					// La función "round" nos permite delimitar el número de decimales que queremos imprimir.
					$imp = $rowEst['resultado_19_tactico'];
					$graf = ($porcentaje*28)/100;

					if($porcentaje>100)
						$pdf->Rect(160,$yGraficas+25,28,2.5,'F');

					else
						$pdf->Rect(160,$yGraficas+25,$graf,2.5,'F');

					$impND = 1;
				}

				// Lineas
				$pdf->SetDrawColor(255,255,255);
				$pdf->Line(162.8,$yGraficas+25,162.8,$yGraficas+27.5);
				$pdf->Line(165.6,$yGraficas+25,165.6,$yGraficas+27.5);
				$pdf->Line(168.4,$yGraficas+25,168.4,$yGraficas+27.5);
				$pdf->Line(171.2,$yGraficas+25,171.2,$yGraficas+27.5);
				$pdf->Line(174,$yGraficas+25,174,$yGraficas+27.5);
				$pdf->Line(176.8,$yGraficas+25,176.8,$yGraficas+27.5);
				$pdf->Line(179.6,$yGraficas+25,179.6,$yGraficas+27.5);
				$pdf->Line(182.4,$yGraficas+25,182.4,$yGraficas+27.5);
				$pdf->Line(185.2,$yGraficas+25,185.2,$yGraficas+27.5);
				// Texto
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFont('Graphik-Bold','',11);
				if($impND == 1)
					$pdf->Text(189.5,$yGraficas+27.6,$imp);

				else
					$pdf->Text(189.5,$yGraficas+27.6,$imp." "."%");
				

				$pdf->SetFont('Graphik-Regular','',8.5);
				$pdf->Text(144,$yGraficas+25.5,utf8_decode("Resultados"));
				$pdf->Text(148.5,$yGraficas+28.5,utf8_decode("2019"));

			// FUENTE
				if($rowEst['fntetactc'] != NULL && $rowEst['fntetactc'] != " ")
				{
					$pdf->SetXY(0,$yGraficas+30.5);
					$pdf->Ln();
					$pdf->SetTextColor(0,0,0);
					$pdf->SetFont('Graphik-Regular','',7);
					$pdf->Row(array(" "," ","Fuente: ".$rowEst['fntetactc']));
				}

			// ODS
				if($rowEst['odstactc'] != NULL && $rowEst['odstactc'] != " ")
					$pdf->Row(array(" "," ","Alineado con el Objetivo de Desarrollo Sostenible: ".$rowEst['odstactc']));
			}

			$y = $pdf->SetXY(8,10);

			// Impresión de la información del indicador estratégico.

		// OBJETIVO ESTRATÉGICO
			if($rowEst['ob_estrategico'] != NULL && $rowEst['ob_estrategico'] != " ")
			{
			// Cambio del número de columnas: De 1 columna a 3 columnas.
				$pdf->columnas($ban=3);

				$pdf->SetTextColor(0,0,0);
				$pdf->SetFont('Graphik_Semibold','',12);
				$pdf->Row(array($rowEst['ob_estrategico']," "," "));
				$pdf->Ln();
			}

		// LÍNEA DISCURSIVA
			if($rowEst['linea_discursiva'] != NULL && $rowEst['linea_discursiva'] != " ")
			{
			// Cambio del número de columnas: De 1 columna a 3 columnas.
				$pdf->columnas($ban=3);

				$pdf->titulo("Situación actual");
				$pdf->Ln();

				$pdf->contenidoInforme();
				$pdf->Row(array($rowEst['linea_discursiva']," "," "));
				$pdf->Ln();
			}

		// LOGROS
			if($rowEst['bullet_logro'] != NULL && $rowEst['bullet_logro'] != " ")
			{
				$pdf->titulo("Logros, premios y/o reconocimientos");
				$pdf->Ln();

				$pdf->contenidoInforme();
				$pdf->Row(array($rowEst['bullet_logro']," "," "));
				$pdf->Ln();
			}

		// RESULTADO, OBRAS Y ACCIONES
			if($rowEst['resultado1_est'] != NULL && $rowEst['resultado1_est'] != " ")
			{
				$pdf->titulo("Resultado, obras y acciones");
				$pdf->Ln();

				$pdf->contenidoInforme();
				$pdf->Row(array($rowEst['resultado1_est']," "," "));
				$pdf->Ln();
			}

			if($rowEst['resultado2_est'] != NULL && $rowEst['resultado2_est'] != " ")
			{
				$pdf->contenidoInforme();
				$pdf->Row(array($rowEst['resultado2_est']," "," "));
				$pdf->Ln();
			}

			if($rowEst['resultado3_est'] != NULL && $rowEst['resultado3_est'] != " ")
			{
				$pdf->contenidoInforme();
				$pdf->Row(array($rowEst['resultado3_est']," "," "));
				$pdf->Ln();
			}

			if($rowEst['resultado4_est'] != NULL && $rowEst['resultado4_est'] != " ")
			{
				$pdf->contenidoInforme();
				$pdf->Row(array($rowEst['resultado4_est']," "," "));
				$pdf->Ln();
			}

			if($rowEst['resultado5_est'] != NULL && $rowEst['resultado5_est'] != " ")
			{
				$pdf->contenidoInforme();
				$pdf->Row(array($rowEst['resultado5_est']," "," "));
				$pdf->Ln();
			}

		// PREMIOS Y/O RECONOCIMIENTOS
			/*if($rowEst['desc_logro'] != NULL)
			{
				$pdf->titulo("Premios y/o reconocimientos");
				$pdf->Ln();

				$pdf->contenidoInforme();
				$pdf->Row(array($rowEst['desc_logro']," "," "));
				$pdf->Ln();
			}*/


			// Impresión de la información de los indicadores tácticos.

		// OBJETIVO GENERAL
			if($rowEst['ob_general'] != NULL && $rowEst['ob_general'] != " ")
			{
			// Cambio del número de columnas: De 1 columna a 3 columnas.
				$pdf->columnas($ban=3);

				$pdf->SetTextColor(0,0,0);
				$pdf->SetFont('Graphik_Semibold','',10);
				$pdf->Row(array($rowEst['ob_general']," "," "));
				$pdf->Ln();
			}

		// LÍNEA DISCURSIVA
			if($rowEst['linead_tactico'] != NULL && $rowEst['linead_tactico'] != " ")
			{
				$pdf->titulo("Situación actual");
				$pdf->Ln();

				$pdf->contenidoInforme();
				$pdf->Row(array($rowEst['linead_tactico']," "," "));
				$pdf->Ln();
			}

		// LOGROS, PREMIOS Y/O RECONOCIMIENTOS
			if($rowEst['bullet_logro_tactico'] != NULL && $rowEst['bullet_logro_tactico'] != " ")
			{
				$pdf->titulo("Logros, premios y/o reconocimientos");
				$pdf->Ln();

				$pdf->contenidoInforme();
				$pdf->Row(array($rowEst['bullet_logro_tactico']," "," "));
				$pdf->Ln();
			}

		// RESULTADOS, OBRAS Y ACCIONES
			if($rowEst['res1_tactico'] != NULL && $rowEst['res1_tactico'] != " ")
			{
				$pdf->titulo("Resultados, obras y acciones");
				$pdf->Ln();

				$pdf->contenidoInforme();
				$pdf->Row(array("- ".$rowEst['res1_tactico']," "," "));

				if ($rowEst['res2_tactico'] != NULL && $rowEst['res2_tactico'] != " ")
				{
					$pdf->Row(array("- ".$rowEst['res2_tactico']," "," "));
				}

				if ($rowEst['res3_tactico'] != NULL && $rowEst['res3_tactico'] != " ")
				{
					$pdf->Row(array("- ".$rowEst['res3_tactico']," "," "));
				}

				if ($rowEst['res4_tactico'] != NULL && $rowEst['res4_tactico'] != " ")
				{
					$pdf->Row(array("- ".$rowEst['res4_tactico']," "," "));
				}

				if ($rowEst['res5_tactico'] != NULL && $rowEst['res5_tactico'] != " ")
				{
					$pdf->Row(array("- ".$rowEst['res5_tactico']," "," "));
				}

				if ($rowEst['res6_tactico'] != NULL && $rowEst['res6_tactico'] != " ")
				{
					$pdf->Row(array("- ".$rowEst['res6_tactico']," "," "));
				}

				if ($rowEst['res7_tactico'] != NULL && $rowEst['res7_tactico'] != " ")
				{
					$pdf->Row(array("- ".$rowEst['res7_tactico']," "," "));
				}

				if ($rowEst['res8_tactico'] != NULL && $rowEst['res8_tactico'] != " ")
				{
					$pdf->Row(array("- ".$rowEst['res8_tactico']," "," "));
				}

				$pdf->Ln();
			}

		// LO QUE SIGUE
			if($rowEst['loquesigue1']!= NULL && $rowEst['loquesigue1']!= " ")
			{
				$pdf->titulo("Lo que sigue");
				$pdf->Ln();

				$pdf->contenidoInforme();
				$pdf->Row(array("- ".$rowEst['loquesigue1']," "," "));

				if ($rowEst['loquesigue2'] != NULL && $rowEst['loquesigue2'] != " ")
				{
					$pdf->Row(array("- ".$rowEst['loquesigue2']," "," "));
				}

				if ($rowEst['loquesigue3'] != NULL && $rowEst['loquesigue3'] != " ")
				{
					$pdf->Row(array("- ".$rowEst['loquesigue3']," "," "));
				}

				/*if ($rowEst['loquesigue4'] != NULL)
				{
					$pdf->Row(array("- ".$rowEst['loquesigue4']," "," "));
				}

				if ($rowEst['loquesigue5'] != NULL)
				{
					$pdf->Row(array("- ".$rowEst['loquesigue5']," "," "));
				}

				if ($rowEst['loquesigue6'] != NULL)
				{
					$pdf->Row(array("- ".$rowEst['loquesigue6']," "," "));
				}*/

				$pdf->Ln();
			}

			if($salto == 0)
				$pdf->AddPage(P,Legal);
		}
	}
}


$pdf->Output();
?>
