<?php
require('../fpdf.php');
require 'conexion.php';


$id_evidencia = $_GET["id_evidencia"];//"8";
$nombre_usuario = $_GET["user"];
$tipo = $_GET["tipo"];//"indicadorPIDS";
//$id_evidencia = "";//$_GET["id_evidencia"];
//$nombre_usuario = "";//$_GET["user"];


class PDF_MC_Table extends FPDF
{

	//Cabecera y pie de pagina
	function Header()
	{
		// Margen del texto del documento.
		$this->SetMargins(8,65,8);
		$this->SetAutoPageBreak(true, 25);
		// Termina definición del margen.

		$this->Image('logo_hidalgo.png', 0, 0, 50 );

		// Agregamos la fuente que vamos a ocupar.
		$this->AddFont('Graphik-Regular','','graphikRegular.php');
		$this->SetFont('Graphik-Regular','',14);
		// Color del texto.
	    $this->SetTextColor(105,105,105);
		if($this->PageNo()==1)
		{
			$this->Ln(12);
			$this->SetX(55);
			$this->Cell(0,10,utf8_decode('INDICADOR TÁCTICO DEL PROGRAMA'),0,0,'C');
			$this->Ln(5);
			$this->SetX(55);
			$this->Cell(0,10,utf8_decode('INSTITUCIONAL DE DESARROLLO'),0,0,'C');
			$this->Ln(38);
		}
	}
	
	function Footer()
	{
		$this->SetY(-15);
		$this->SetFont('Arial','I', 8);
		$this->Cell(0,10, 'Pagina '.$this->PageNo().'',0,0,'C' );
	}
	//fin cabecera pie de pagina

	// Formato del texto del documento.
	function titulo($texto)
	{
		// Agregamos la fuente que vamos a ocupar.
		$this->AddFont('Graphik-SemiboldItalic','','graphikSemiboldItalic.php');
		$this->SetTextColor(0,0,0);
		$this->SetFont('Graphik-SemiboldItalic','', 12);
		$this->Row(array(utf8_decode($texto)));
	}

	function subtitulo()
	{
		// Agregamos la fuente que vamos a ocupar.
		$this->AddFont('Graphik-RegularItalic','','graphikRegularItalic.php');
		$this->SetTextColor(130,130,130);
		$this->SetFont('Graphik-RegularItalic','',12);
	}

	function contenido()
	{
		// Agregamos la fuente que vamos a ocupar.
		$this->AddFont('Graphik-Regular','','graphikRegular.php');
		$this->SetTextColor(130,130,130);
		$this->SetFont('Graphik-Regular','',12);
	}

	function contenidoInforme()
	{
		// Agregamos la fuente que vamos a ocupar.
		$this->AddFont('Graphik-Regular','','graphikRegular.php');
		$this->SetTextColor(130,130,130);
		$this->SetFont('Graphik-Regular','',10);
	}
	// Termina definición del formato.

	function columnas($bandera)
	{
		if($bandera=="1")
		{
			$this->SetWidths(array(200));
		}
		else
		{
			$this->SetWidths(array(130,5,60));
		}
	}

	function imp_lugar($arreglo)
	{
		$arr_municipios = "Acatlán,Acaxochitlán,Actopan,Agua Blanca de Iturbide,Ajacuba,Alfajayucan,Almoloya,Apan,Atitalaquia,Atlapexco,Atotonilco de Tula,Atotonilco el Grande,Calnali,Cardonal,Chapantongo,Chapulhuacán,Chilcuautla,Cuautepec de Hinojosa,El Arenal,Eloxochitlán,Emiliano Zapata,Epazoyucan,Francisco I. Madero,Huasca de Ocampo,Huautla,Huazalingo,Huehuetla,Huejutla de Reyes,Huichapan,Ixmiquilpan,Jacala de Ledezma,Jaltocán,Juárez Hidalgo,La Misión,Lolotla,Metepec,Metztitlán,Mineral de la Reforma,Mineral del Chico,Mineral del Monte,Mixquiahuala de Juárez,Molango de Escamilla,Nicolás Flores,Nopala de Villagrán,Omitlán de Juárez,Pachuca de Soto,Pacula,Pisaflores,Progreso de Obregón,San Agustín Metzquititlán,San Agustín Tlaxiaca,San Bartolo Tutotepec,San Felipe Orizatlán,San Salvador,Santiago de Anaya,Santiago Tulantepec de Lugo Guerre,Singuilucan,Tasquillo,Tecozautla,Tenango de Doria,Tepeapulco,Tepehuacán de Guerrero,Tepeji del Río de Ocampo,Tepetitlán,Tetepango,Tezontepec de Aldama,Tianguistengo,Tizayuca,Tlahuelilpan,Tlahuiltepa,Tlanalapa,Tlanchinol,Tlaxcoapan,Tolcayuca,Tula de Allende,Tulancingo de Bravo,Villa de Tezontepec,Xochiatipan,Xochicoatlán,Yahualica,Zacualtipán de angeles,Zapotlán de Juárez,Zempoala,Zimapán";

		$arr_entidades = "Aguascalientes,Baja California,Baja California Sur,Campeche,Chiapas,Chihuahua,Ciudad de México,Coahuila,Colima,Durango,Estado de México,Guanajuato,Guerrero,Hidalgo,Jalisco,Michoacán,Morelos,Nayarit,Nuevo León,Oaxaca,Puebla,Querétaro,Quintana Roo,San Luis Potosí,Sinaloa,Sonora,Tabasco,Tamaulipas,Tlaxcala,Veracruz,Yucatán,Zacatecas";

		$extraer_arr_municipios = explode(',',$arr_municipios);
		$extraer_arr_entidades = explode(',',$arr_entidades);
		$valor = explode(',',$arreglo);
		$valorSeleccion = $valor[0];

		if($valorSeleccion == "Hidalgo")
		{
			$this->Row(array(utf8_decode("Hidalgo")));
		}
		
		else if($valorSeleccion == "Municipios")
		{
			$contadorMuni = 0;
			// La función "count" nos permite contar el número de valores que contiene un arreglo.
			$iteracionMuni = count($valor);
			$iteracionMuni = $iteracionMuni-1;

			for($i=0;$i<$iteracionMuni;$i++)
			{
				for($e=0;$e<84;$e++)
				{
					if($valor[$i+1] == $e)
					{
						$val_arr_muni[$contadorMuni] = $extraer_arr_municipios[$e];
						$contadorMuni = $contadorMuni+1;
					}
				}
			}

			// La función "count" nos permite contar el número de valores que contiene un arreglo.
			$iteracionMuni = count($val_arr_muni);
			$iteracionMuni = $iteracionMuni-1;
			$cont_imp_arreglo = 0;

			for($s=0;$s<$iteracionMuni;$s++)
			{
				if($cont_imp_arreglo == 0)
				{
					$imp_arreglo = $val_arr_muni[$s];
					$cont_imp_arreglo = 1;
				}

				else
					$imp_arreglo = $imp_arreglo.", ".$val_arr_muni[$s];
			}

			$this->Row(array(utf8_decode($imp_arreglo.".")));
		}

		else if($valorSeleccion == "Entidades")
		{
			$contadorEnti = 0;
			// La función "count" nos permite contar el número de valores que contiene un arreglo.
			$iteracionEnti = count($valor);
			$iteracionEnti = $iteracionEnti-1;

			for($i=0;$i<$iteracionEnti;$i++)
			{
				for($e=0;$e<32;$e++)
				{
					if($valor[$i+1] == $e)
					{
						$val_arr_enti[$contadorEnti] = $extraer_arr_entidades[$e];
						$contadorEnti = $contadorEnti+1;
					}
				}
			}

			// La función "count" nos permite contar el número de valores que contiene un arreglo.
			$iteracionEnti = count($val_arr_enti);
			$iteracionEnti = $iteracionEnti-1;
			$cont_imp_arreglo = 0;

			for($s=0;$s<$iteracionEnti;$s++)
			{
				if($cont_imp_arreglo == 0)
				{
					$imp_arreglo = $val_arr_enti[$s];
					$cont_imp_arreglo = 1;
				}

				else
					$imp_arreglo = $imp_arreglo.", ".$val_arr_enti[$s];
			}

			$this->Row(array(utf8_decode($imp_arreglo.".")));
		}

		else
		{
			$this->Row(array(utf8_decode("Localidades")));
		}
	}

	function imp_dependencia($usuario)
	{
		$arr_dependencias = "Secretaría de Contraloría,Secretaría de Cultura,Secretaría de Educación Pública,Secretaría de Gobierno,Procuraduría General de Justicia en el Estado de Hidalgo,Secretaría de Movilidad y Transporte,Secretaría de Salud,Secretaría de Turismo,Secretaría del Trabajo y Previsión Social,Secretaría Ejecutiva de la Política Pública,Secretaría de Desarrollo Agropecuario,Secretaría de Desarrollo Económico,Secretaría de Medio Ambiente y Recursos Naturales,Unidad de Planeación y Prospectiva,Oficialía Mayor,Sistema para el Desarrollo Integral de la Familia del Estado de Hidalgo,Secretaria de Finanzas,Secretaría de Obras Públicas y Ordenamiento Territorial,Seguridad Pública,Secretaría de Desarrollo Social";

		$arr_nom_usuario = "CONTRALORIA,SEC_CULTURA,SEP,SEC_GOB,PROCURADURIA,SEC_MOVYTRANS,SEC_SALUD,SEC_TURISMO,SEC_TRABYPS,SEC_EJECUTIVA,SEC_DESAGRO,SEC_DESECON,SEMARNATH,UNI_PLANEA,OFI_MAYOR,DIFH,SEC_FINANZAS,SOPOT,SEC_SEGUP,SEDESO";

		$extraer_arr_dependencias = explode(',',$arr_dependencias);
		$extraer_arr_nom_usuario = explode(',',$arr_nom_usuario);

		for($i=0;$i<20;$i++)
		{
			if($usuario == $extraer_arr_nom_usuario[$i])
			{
				$this->RowCenter(array(" "," ",utf8_decode($extraer_arr_dependencias[$i])));
			}
		}
	}

	var $widths;
	var $aligns;

	function SetWidths($w)
	{
	    //Set the array of column widths
	    $this->widths=$w;
	}

	function SetAligns($a)
	{
	    //Set the array of column alignments
	    $this->aligns=$a;
	}

	function Row($data)
	{
	    //Calculate the height of the row
	    $nb=0;
	    for($i=0;$i<count($data);$i++)
	        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
	    $h=5*$nb;
	    //Issue a page break first if needed
	    $this->CheckPageBreak($h);
	    //Draw the cells of the row
	    for($i=0;$i<count($data);$i++)
	    {
	        $w=$this->widths[$i];
	        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'J';
	        //Save the current position
	        $x=$this->GetX();
	        $y=$this->GetY();
	        //Draw the border
	       // $this->Rect($x,$y,$w,$h);
	        //Print the text
	        $this->MultiCell($w,5,$data[$i],0,$a);
	        //Put the position to the right of the cell
	        $this->SetXY($x+$w,$y);
	    }
	    //Go to the next line
	    $this->Ln($h);
	}

	function RowCenter($data)
	{
	    //Calculate the height of the row
	    $nb=0;
	    for($i=0;$i<count($data);$i++)
	        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
	    $h=5*$nb;
	    //Issue a page break first if needed
	    $this->CheckPageBreak($h);
	    //Draw the cells of the row
	    for($i=0;$i<count($data);$i++)
	    {
	        $w=$this->widths[$i];
	        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'C';
	        //Save the current position
	        $x=$this->GetX();
	        $y=$this->GetY();
	        //Draw the border
	       // $this->Rect($x,$y,$w,$h);
	        //Print the text
	        $this->MultiCell($w,5,$data[$i],0,$a);
	        //Put the position to the right of the cell
	        $this->SetXY($x+$w,$y);
	    }
	    //Go to the next line
	    $this->Ln($h);
	}

	function CheckPageBreak($h)
	{
	    //If the height h would cause an overflow, add a new page immediately
	    if($this->GetY()+$h>$this->PageBreakTrigger)
	        $this->AddPage($this->CurOrientation);
	}

	function NbLines($w,$txt)
	{
	    //Computes the number of lines a MultiCell of width w will take
	    $cw=&$this->CurrentFont['cw'];
	    if($w==0)
	        $w=$this->w-$this->rMargin-$this->x;
	    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
	    $s=str_replace("\r",'',$txt);
	    $nb=strlen($s);
	    if($nb>0 and $s[$nb-1]=="\n")
	        $nb--;
	    $sep=-1;
	    $i=0;
	    $j=0;
	    $l=0;
	    $nl=1;
	    while($i<$nb)
	    {
	        $c=$s[$i];
	        if($c=="\n")
	        {
	            $i++;
	            $sep=-1;
	            $j=$i;
	            $l=0;
	            $nl++;
	            continue;
	        }
	        if($c==' ')
	            $sep=$i;
	        $l+=$cw[$c];
	        if($l>$wmax)
	        {
	            if($sep==-1)
	            {
	                if($i==$j)
	                    $i++;
	            }
	            else
	                $i=$sep+1;
	            $sep=-1;
	            $j=$i;
	            $l=0;
	            $nl++;
	        }
	        else
	            $i++;
	    }
	    return $nl;
	}

	function Circle($x, $y, $r, $style='D')
	{
	    $this->Ellipse($x,$y,$r,$r,$style);
	}

	function Ellipse($x, $y, $rx, $ry, $style='D')
	{
	    if($style=='F')
	        $op='f';
	    elseif($style=='FD' || $style=='DF')
	        $op='B';
	    else
	        $op='S';
	    $lx=4/3*(M_SQRT2-1)*$rx;
	    $ly=4/3*(M_SQRT2-1)*$ry;
	    $k=$this->k;
	    $h=$this->h;
	    $this->_out(sprintf('%.2F %.2F m %.2F %.2F %.2F %.2F %.2F %.2F c',
	        ($x+$rx)*$k,($h-$y)*$k,
	        ($x+$rx)*$k,($h-($y-$ly))*$k,
	        ($x+$lx)*$k,($h-($y-$ry))*$k,
	        $x*$k,($h-($y-$ry))*$k));
	    $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c',
	        ($x-$lx)*$k,($h-($y-$ry))*$k,
	        ($x-$rx)*$k,($h-($y-$ly))*$k,
	        ($x-$rx)*$k,($h-$y)*$k));
	    $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c',
	        ($x-$rx)*$k,($h-($y+$ly))*$k,
	        ($x-$lx)*$k,($h-($y+$ry))*$k,
	        $x*$k,($h-($y+$ry))*$k));
	    $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c %s',
	        ($x+$lx)*$k,($h-($y+$ry))*$k,
	        ($x+$rx)*$k,($h-($y+$ly))*$k,
	        ($x+$rx)*$k,($h-$y)*$k,
	        $op));
	}

	function Sector($xc, $yc, $r, $a, $b, $style='FD', $cw=true, $o=90)
    {
        $d0 = $a - $b;
        if($cw){
            $d = $b;
            $b = $o - $a;
            $a = $o - $d;
        }else{
            $b += $o;
            $a += $o;
        }
        while($a<0)
            $a += 360;
        while($a>360)
            $a -= 360;
        while($b<0)
            $b += 360;
        while($b>360)
            $b -= 360;
        if ($a > $b)
            $b += 360;
        $b = $b/360*2*M_PI;
        $a = $a/360*2*M_PI;
        $d = $b - $a;
        if ($d == 0 && $d0 != 0)
            $d = 2*M_PI;
        $k = $this->k;
        $hp = $this->h;
        if (sin($d/2))
            $MyArc = 4/3*(1-cos($d/2))/sin($d/2)*$r;
        else
            $MyArc = 0;
        //first put the center
        $this->_out(sprintf('%.2F %.2F m',($xc)*$k,($hp-$yc)*$k));
        //put the first point
        $this->_out(sprintf('%.2F %.2F l',($xc+$r*cos($a))*$k,(($hp-($yc-$r*sin($a)))*$k)));
        //draw the arc
        if ($d < M_PI/2){
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
        }else{
            $b = $a + $d/4;
            $MyArc = 4/3*(1-cos($d/8))/sin($d/8)*$r;
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
            $a = $b;
            $b = $a + $d/4;
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
            $a = $b;
            $b = $a + $d/4;
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
            $a = $b;
            $b = $a + $d/4;
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
        }
        //terminate drawing
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='b';
        else
            $op='s';
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3 )
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c',
            $x1*$this->k,
            ($h-$y1)*$this->k,
            $x2*$this->k,
            ($h-$y2)*$this->k,
            $x3*$this->k,
            ($h-$y3)*$this->k));
    }

}

if($tipo=="pids")//indicadorPIDS
{

	$query = "SELECT * FROM wp_indicadores_pids where id_tactico_pid=$id_evidencia";
	$resultado = $mysqli->query($query);

	$pdf=new PDF_MC_Table();
	$pdf->AddPage();

	// Agregamos las fuentes que vamos a ocupar.
	$pdf->AddFont('Graphik_Semibold','','graphikSemibold.php');
	$pdf->AddFont('Graphik-SemiboldItalic','','graphikSemiboldItalic.php');
	$pdf->AddFont('Graphik-Regular','','graphikRegular.php');
	$pdf->AddFont('Graphik-RegularItalic','','graphikRegularItalic.php');

	while($row = $resultado->fetch_assoc())
	{
		// Selección del número de columnas: 1 columna.
		$pdf->columnas($ban=1);

		if($row['eje_ped_pid']!="undefined")
		{
			$pdf->SetTextColor(0,0,0);
			$pdf->SetFont('Graphik_Semibold','',14);
			$pdf->Row(array(utf8_decode("EJE:")));
			$pdf->Row(array($row['eje_ped_pid']));
			$pdf->Ln();
		}

		if($row['obj_estrategico_pid']!="undefined")
		{
			$pdf->SetFont('Graphik_Semibold','',12);
			$pdf->Row(array(utf8_decode("OBJETIVO ESTRATÉGICO:")));
			$pdf->Row(array($row['obj_estrategico_pid']));
			$pdf->Ln();
		}

		if($row['obj_general_pid']!="undefined")
		{
			$pdf->SetFont('Graphik_Semibold','',12);
			$pdf->Row(array(utf8_decode("OBJETIVO GENERAL:")));
			$pdf->Row(array($row['obj_general_pid']));
			$pdf->Ln();
		}

		$pdf->Ln();
		$pdf->Ln();

		$pdf->titulo("NOMBRE DEL INDICADOR:");
		$pdf->SetFont('Graphik_Semibold','',12);
		$pdf->Row(array($row['nom_indicador_pid']));
		$pdf->Ln();

		$pdf->titulo("DESCRIPCIÓN DEL INDICADOR:");
		$pdf->contenido();
		$pdf->Row(array($row['descripcion_indicador_pid']));
		$pdf->Ln();

		if($row['logros_indicador_pid']=="Si")
		{
			$pdf->titulo("BULLET DEL LOGRO:");
			$pdf->contenido();
			$pdf->Row(array($row['bullet_logro_pid']));
			$pdf->Ln();
		}

		$pdf->titulo("¿HUBO PREMIO O RECONOCIMIENTO?");
		$pdf->contenido();
		$pdf->Row(array($row['hubo_premio_pid']));
		$pdf->Ln();

			if($row['hubo_premio_pid']=="Si")
			{
				$pdf->subtitulo();
				$pdf->Row(array(utf8_decode("Premio o reconocimiento:")));
				$pdf->contenido();
				$pdf->Row(array($row['premio_reconocimiento_pid']));
				$pdf->Ln();
			}

		$pdf->titulo("RESULTADOS, OBRAS Y ACCIONES");
		$pdf->subtitulo();
		$pdf->Row(array(utf8_decode("Obras y acciones 1:")));
		$pdf->contenido();
		$pdf->Row(array($row['resultado1_pid']));
		$pdf->Ln();
		$pdf->subtitulo();
		$pdf->Row(array(utf8_decode("Lugar:")));
		$pdf->contenido();
		$pdf->imp_lugar($row['lug_result1_pid']);
		$pdf->Ln();

		if ($row['resultado2_pid'] != NULL)
		{
			$pdf->subtitulo();
			$pdf->Row(array(utf8_decode("Obras y acciones 2:")));
			$pdf->contenido();
			$pdf->Row(array($row['resultado2_pid']));
			$pdf->Ln();
			$pdf->subtitulo();
			$pdf->Row(array(utf8_decode("Lugar:")));
			$pdf->contenido();
			$pdf->imp_lugar($row['lug_result2_pid']);
			$pdf->Ln();
		}

		if ($row['resultado3_pid'] != NULL)
		{
			$pdf->subtitulo();
			$pdf->Row(array(utf8_decode("Obras y acciones 3:")));
			$pdf->contenido();
			$pdf->Row(array($row['resultado3_pid']));
			$pdf->Ln();
			$pdf->subtitulo();
			$pdf->Row(array(utf8_decode("Lugar:")));
			$pdf->contenido();
			$pdf->imp_lugar($row['lug_result3_pid']);
			$pdf->Ln();
		}

		if ($row['resultado4_pid'] != NULL)
		{
			$pdf->subtitulo();
			$pdf->Row(array(utf8_decode("Obras y acciones 4:")));
			$pdf->contenido();
			$pdf->Row(array($row['resultado4_pid']));
			$pdf->Ln();
			$pdf->subtitulo();
			$pdf->Row(array(utf8_decode("Lugar:")));
			$pdf->contenido();
			$pdf->imp_lugar($row['lug_result4_pid']);
			$pdf->Ln();
		}

		if ($row['resultado5_pid'] != NULL)
		{
			$pdf->subtitulo();
			$pdf->Row(array(utf8_decode("Obras y acciones 5:")));
			$pdf->contenido();
			$pdf->Row(array($row['resultado5_pid']));
			$pdf->Ln();
			$pdf->subtitulo();
			$pdf->Row(array(utf8_decode("Lugar:")));
			$pdf->contenido();
			$pdf->imp_lugar($row['lug_result5_pid']);
			$pdf->Ln();
		}

		$pdf->titulo("LO QUE SIGUE...");
		$pdf->contenido();
		$pdf->Row(array("-"." ".$row['lo_que_sigue_1_pid']));
		$pdf->Ln();

		if ($row['lo_que_sigue_2_pid'] != NULL)
		{
			$pdf->Row(array("-"." ".$row['lo_que_sigue_2_pid']));
			$pdf->Ln();
		}

		if ($row['lo_que_sigue_3_pid'] != NULL)
		{
			$pdf->Row(array("-"." ".$row['lo_que_sigue_3_pid']));
			$pdf->Ln();
		}

		if ($row['unidad_observacion_ind_pid'] != NULL)
		{
			$pdf->titulo("UNIDAD DE OBSERVACIÓN DEL INDICADOR:");
			$pdf->contenido();
			$pdf->Row(array($row['unidad_observacion_ind_pid']));
			$pdf->Ln();
		}

		if ($row['descripcion_observacion_ficha_tecnica_def_pid'] != NULL)
		{
			$pdf->titulo("DESCRIPCIÓN DE LA UNIDAD DE LA OBSERVACIÓN:");
			$pdf->contenido();
			$pdf->Row(array($row['descripcion_observacion_ficha_tecnica_def_pid']));
			$pdf->Ln();
		}

		if ($row['tipo_ind_pid'] != NULL)
		{
			$pdf->titulo("TIPO DE INDICADOR:");
			$pdf->contenido();
			$pdf->Row(array($row['tipo_ind_pid']));
			$pdf->Ln();
		}

		if ($row['tendencia_pid'] != NULL)
		{
			$pdf->titulo("TENDENCIA:");
			$pdf->contenido();
			$pdf->Row(array($row['tendencia_pid']));
			$pdf->Ln();
		}

		if ($row['linea_base_absolutos_pid'] != NULL)
		{
			$pdf->titulo("LÍNEA BASE:");
			$pdf->contenido();
			$pdf->Row(array($row['linea_base_absolutos_pid']));
			$pdf->Ln();
		}

		if ($row['obj_esp_prog_inst_pid'] != NULL)
		{
			$pdf->titulo("OBJETIVO ESPECIFICO:");
			$pdf->contenido();
			$pdf->Row(array($row['obj_esp_prog_inst_pid']));
			$pdf->Ln();
		}

		if ($row['fuente_pid'] != NULL)
		{
			$pdf->titulo("FUENTE:");
			$pdf->contenido();
			$pdf->Row(array($row['fuente_pid']));
			$pdf->Ln();
		}

		if ($row['referencias_Adi_pid'] != NULL)
		{
			$pdf->titulo("REFERENCIAS ADICIONALES:");
			$pdf->contenido();
			$pdf->Row(array($row['referencias_Adi_pid']));
			$pdf->Ln();
		}

		if ($row['linea_base_2017_pid'] != NULL)
		{
			$pdf->titulo("LÍNEA BASE 2017:");
			$pdf->contenido();
			$pdf->Row(array($row['linea_base_2017_pid']));
			$pdf->Ln();
		}

		if ($row['meta_2018_pid'] != NULL)
		{
			$pdf->titulo("META 2018:");
			$pdf->contenido();
			$pdf->Row(array($row['meta_2018_pid']));
			$pdf->Ln();
		}

		if ($row['meta_2019_pid'] != NULL)
		{
			$pdf->titulo("META 2019:");
			$pdf->contenido();
			$pdf->Row(array($row['meta_2019_pid']));
			$pdf->Ln();
		}

		if ($row['meta_2020_pid'] != NULL)
		{
			$pdf->titulo("META 2020:");
			$pdf->contenido();
			$pdf->Row(array($row['meta_2020_pid']));
			$pdf->Ln();
		}

		if ($row['meta_2021_pid'] != NULL)
		{
			$pdf->titulo("META 2021:");
			$pdf->contenido();
			$pdf->Row(array($row['meta_2021_pid']));
			$pdf->Ln();
		}

		if ($row['meta_2022_pid'] != NULL)
		{
			$pdf->titulo("META 2022:");
			$pdf->contenido();
			$pdf->Row(array($row['meta_2022_pid']));
			$pdf->Ln();
		}

		if ($row['meta_2030_pid'] != NULL)
		{
			$pdf->titulo("META 2030:");
			$pdf->contenido();
			$pdf->Row(array($row['meta_2030_pid']));
			$pdf->Ln();
		}

		if ($row['resultado_pid_2018'] != NULL)
		{
			$pdf->titulo("RESULTADO PID 2018:");
			$pdf->contenido();
			$pdf->Row(array($row['resultado_pid_2018']));
			$pdf->Ln();
		}

		if ($row['tipo_evidencia_pid'] != NULL)
		{
			$pdf->titulo("TIPO DE EVIDENCIA:");
			$pdf->contenido();
			$pdf->Row(array($row['tipo_evidencia_pid']));
			$pdf->Ln();
		}

		if ($row['descripcion_evidencia_pid'] != NULL)
		{
			$pdf->titulo("¿QUE CONTIENE LA EVIDENCIA?");
			$pdf->contenido();
			$pdf->Row(array($row['descripcion_evidencia_pid']));
			$pdf->Ln();
		}

		if ($row['fecha_registro_pid_inicio'] != NULL)
		{
			$pdf->titulo("FECHA INICIO DE LA EVIDENCIA:");
			$pdf->contenido();
			$pdf->Row(array($row['fecha_registro_pid_inicio']));
			$pdf->Ln();
		}

		if ($row['fecha_registro_pid_fin'] != NULL)
		{
			$pdf->titulo("FECHA FIN DE LA EVIDENCIA:");
			$pdf->contenido();
			$pdf->Row(array($row['fecha_registro_pid_fin']));
			$pdf->Ln();
		}

		$pdf->titulo("¿EXISTE EXPEDIENTE DOCUMENTAL?");
		$pdf->contenido();
		$pdf->Row(array($row['existe_expediente_doc_pid']));
		$pdf->Ln();

			if($row['existe_expediente_doc_pid']=="Si")
			{
				$pdf->subtitulo();
				$pdf->Row(array(utf8_decode("Serie de la evidencia:")));
				$pdf->contenido();
				$pdf->Row(array($row['serie_evidencia_pid']));
				$pdf->Ln();

				$pdf->subtitulo();
				$pdf->Row(array(utf8_decode("Sección:")));
				$pdf->contenido();
				$pdf->Row(array($row['seccion_pid']));
				$pdf->Ln();
			}

		if ($row['guia_invetario_expediente_pid'] != NULL)
		{
			$pdf->titulo("GUÍA/INVENTARIO EN LA QUE SE INCLUYE EL EXPEDIENTE:");
			$pdf->contenido();
			$pdf->Row(array($row['guia_invetario_expediente_pid']));
			$pdf->Ln();
		}

		if ($row['comentarios_pid'] != NULL)
		{
			$pdf->titulo("COMENTARIOS:");
		  	$pdf->contenido();
			$pdf->Row(array($row['comentarios_pid']));
			$pdf->Ln();
		}

		if ($row['comentarios_pid'] != NULL)
		{
			$pdf->titulo("PERIODICIDAD:");
			$pdf->contenido();
			$pdf->Row(array($row['periodicidad_pid']));
			$pdf->Ln();
		}



/*
		// -------------------------------------------------------------------------------
	// Cambio del número de columnas: De 1 columna a 3 columnas.
		$pdf->columnas($ban=3);
		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont('Graphik-SemiboldItalic','',12);
		$pdf->Row(array(utf8_decode("LÍNEA BASE:")," "," "));
		$pdf->contenido();
		$pdf->Row(array($row['linea_base_absolutos_pid']," "," "));
		$pdf->Ln();

		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont('Graphik-SemiboldItalic','',12);
		$pdf->Row(array(utf8_decode("META 2018:")," ",utf8_decode("RESULTADO 2018:")));
		$pdf->contenido();
		$pdf->Row(array($row['meta_2018_pid']," ",$row['resultado_pid_2018']));
		$pdf->Ln();
		// -------------------------------------------------------------------------------

	// Cambio del número de columnas: De 3 columnas a 1 columna.
		$pdf->columnas($ban=1);

		/*
		$pdf->titulo("LÍNEA DISCURSIVA:");
		$pdf->contenido();
		$pdf->Row(array($row['linea_discursiva']));
		$pdf->Ln();

		$pdf->titulo("CLASIFICACIÓN DEL INDICADOR:");
		$pdf->contenido();
		$pdf->Row(array($row['indicador_estrella']));
		$pdf->Ln();
*/
		

		// -------------------------------------------------------------------------------
		

		
		// -------------------------------------------------------------------------------

		
/*
		
*/
		

		
/*
		$pdf->titulo("TIPO DE EVIDENCIA PRESENTADA:");
		$pdf->contenido();
		$pdf->Row(array($row['tipo_evidencia_def']));
		$pdf->Ln();

		$pdf->titulo("¿POR QUÉ ES UNA EVIDENCIA VINCULADA AL INDICADOR?");
		$pdf->subtitulo();
		$pdf->Row(array(utf8_decode("(Descripción)")));
		$pdf->contenido();
		$pdf->Row(array($row['descripcion_documento_vinculado_indicador_def']));
		$pdf->Ln();

		$pdf->titulo("FECHA DE REGISTRO:");
		$pdf->contenido();
		$pdf->Row(array($row['fecha_registro_pid']));
		$pdf->Ln();

		$pdf->titulo("¿EXISTE EXPEDIENTE DOCUMENTAL?");
		$pdf->contenido();
		$pdf->Row(array($row['existe_expediente_doc_def']));
		$pdf->Ln();

			if($row['existe_expediente_doc_def']=="Si")  //campos Si
			{
			// Cambio del número de columnas: De 1 columna a 3 columnas.
				$pdf->columnas($ban=3);
				$pdf->subtitulo();
			  	$pdf->Row(array(utf8_decode("Serie de la evidencia:")," ",utf8_decode("Sección:")));
			  	$pdf->contenido();
			  	$pdf->Row(array($row['serie_evidencia_def']," ",$row['seccion_def']));
			  	$pdf->Ln();

			// Cambio del número de columnas: De 3 columnas a 1 columna.
				$pdf->columnas($ban=1);
			  	$pdf->subtitulo();
			  	$pdf->Row(array(utf8_decode("Guía/Inventario en el que se incluye el expediente:")));
			  	$pdf->contenido();
				$pdf->Row(array($row['guia_invetario_expediente_def']));
				$pdf->Ln();
			}
			else if($row['existe_expediente_doc_def']=="No")
			{
				$pdf->subtitulo();
			  	$pdf->Row(array(utf8_decode("Ubicación electrónica o física de la evidencia, o cómo es posible rastrearla:")));
			  	$pdf->contenido();
				$pdf->Row(array($row['medios_verificacion_evidencia_def']));
				$pdf->Ln();
			}
*/
	  	

		/*$pdf->titulo("DOCUMENTO ADJUNTO:");
	  	$pdf->contenido();
		$pdf->Row(array("Nombre del documento"));
		$pdf->Ln();*/

		/*$pdf->Ln();
		$pdf->Ln();
	// Cambio del número de columnas: De 1 columna a 3 columnas.
		$pdf->columnas($ban=3);
		$pdf->RowCenter(array(" "," ","__________________________________"));
		$pdf->imp_dependencia($row['nombre_dependencia_def']);*/

		//$pdf->Row(array(utf8_decode("Linea base de acuerdo al tipo de indicador"),"   ",$row['linea_base_tipo_indicador_def']));
	}
}

$pdf->Output();
?>