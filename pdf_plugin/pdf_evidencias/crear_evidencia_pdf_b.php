<?php
require('../fpdf.php');
require 'conexion.php';


$id_evidencia = $_GET["id_evidencia"];
$nombre_usuario = $_GET["user"];
$tipo = $_GET["tipo"];


class PDF_MC_Table extends FPDF
{

	//Cabecera y pie de pagina
	function Header()
	{
		$this->Image('logo_hidalgo.png', 5, 5, 30 );
		$this->SetFont('Arial','B',15);
		$this->Cell(30);
		$this->Cell(120,10, 'Resumen registro de evidencia ',0,0,'C');
		$this->Ln(20);
	}
	
	function Footer()
	{
		$this->SetY(-15);
		$this->SetFont('Arial','I', 8);
		$this->Cell(0,10, 'Pagina '.$this->PageNo().'',0,0,'C' );
	}
	//fin cabecera pie de pagina

	var $widths;
	var $aligns;

	function SetWidths($w)
	{
	    //Set the array of column widths
	    $this->widths=$w;
	}

	function SetAligns($a)
	{
	    //Set the array of column alignments
	    $this->aligns=$a;
	}

	function Row($data)
	{
	    //Calculate the height of the row
	    $nb=0;
	    for($i=0;$i<count($data);$i++)
	        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
	    $h=5*$nb;
	    //Issue a page break first if needed
	    $this->CheckPageBreak($h);
	    //Draw the cells of the row
	    for($i=0;$i<count($data);$i++)
	    {
	        $w=$this->widths[$i];
	        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
	        //Save the current position
	        $x=$this->GetX();
	        $y=$this->GetY();
	        //Draw the border
	       // $this->Rect($x,$y,$w,$h);
	        //Print the text
	        $this->MultiCell($w,5,$data[$i],0,$a);
	        //Put the position to the right of the cell
	        $this->SetXY($x+$w,$y);
	    }
	    //Go to the next line
	    $this->Ln($h);
	}

	function CheckPageBreak($h)
	{
	    //If the height h would cause an overflow, add a new page immediately
	    if($this->GetY()+$h>$this->PageBreakTrigger)
	        $this->AddPage($this->CurOrientation);
	}

	function NbLines($w,$txt)
	{
	    //Computes the number of lines a MultiCell of width w will take
	    $cw=&$this->CurrentFont['cw'];
	    if($w==0)
	        $w=$this->w-$this->rMargin-$this->x;
	    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
	    $s=str_replace("\r",'',$txt);
	    $nb=strlen($s);
	    if($nb>0 and $s[$nb-1]=="\n")
	        $nb--;
	    $sep=-1;
	    $i=0;
	    $j=0;
	    $l=0;
	    $nl=1;
	    while($i<$nb)
	    {
	        $c=$s[$i];
	        if($c=="\n")
	        {
	            $i++;
	            $sep=-1;
	            $j=$i;
	            $l=0;
	            $nl++;
	            continue;
	        }
	        if($c==' ')
	            $sep=$i;
	        $l+=$cw[$c];
	        if($l>$wmax)
	        {
	            if($sep==-1)
	            {
	                if($i==$j)
	                    $i++;
	            }
	            else
	                $i=$sep+1;
	            $sep=-1;
	            $j=$i;
	            $l=0;
	            $nl++;
	        }
	        else
	            $i++;
	    }
	    return $nl;
	}

}


if($tipo=="tactico")
{
	$query = "SELECT * FROM wp_definicion_evidencias where 	id_indicador_asociado_def=$id_evidencia";
	$resultado = $mysqli->query($query);

	$pdf=new PDF_MC_Table();
	$pdf->AddPage();
	$pdf->SetFont('Arial','',14);
	$pdf->Ln();
	//Table with 20 rows and 4 columns
	$pdf->SetWidths(array(60,10,120));
	

	while($row = $resultado->fetch_assoc())
	{

	
		  $pdf->Row(array(utf8_decode("Nombre del indicador "),"   ",$row['nombre_indicador_asociado_def']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Linea Discursiva"),"   ",$row['linea_discursiva']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Clasificaion del indicador"),"   ",$row['indicador_estrella']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Descripcion del indicador:"),"   ",$row['descripcion_indicador_def']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Unidad de observacion del indicador:"),"   ",$row['unidad_observacion_indicador_def']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Descripción de la unidad de la observación:"),"   ",$row['descripcion_observacion_ficha_tecnica_def']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Tipo de indicador"),"   ",$row['tipo_indicador_def']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Tendencia esperada"),"   ",$row['tendencia_esperada_def']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Linea base en absolutos:"),"   ",$row['linea_base_absolutos_def']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Linea base de acuerdo al tipo de indicador"),"   ",$row['linea_base_tipo_indicador_def']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Resultado reportado para el primer informe:"),"   ",$row['reporte_primer_informe_def']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Avance con relacion a la meta planteada:"),"   ",$row['avance_meta_planeada_def']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("tipo de evidencia se presentada"),"   ",$row['tipo_evidencia_def']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Descripcion por qué es una evidencia vinculada al indicador:"),"   ",$row['descripcion_documento_vinculado_indicador_def']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Fecha corte "),"   ",$row['tipo_evidencia_fecha_def']));
		  $pdf->Ln();
	

		
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Existe expediente documental"),"   ",$row['existe_expediente_doc_def']));

		  if($row['existe_expediente_doc_def']=="Si")  //campos Si
		  {
		  		$pdf->Ln();
		  		$pdf->Row(array(utf8_decode("Serie de la Evidencia:"),"   ",$row['serie_evidencia_def']));
		  		$pdf->Ln();
		  		$pdf->Row(array(utf8_decode("Seccion"),"   ",$row['seccion_def']));
		  		$pdf->Ln();
		  		$pdf->Row(array(utf8_decode("Guía/Inventario en el que se incluye el expediente"),"   ",$row['guia_invetario_expediente_def']));

		  }
		  else if($row['existe_expediente_doc_def']=="No")
		  {

		  		$pdf->Ln();
		  		$pdf->Row(array(utf8_decode("Ubicación electrónica o física de la evidencia o como es posible rastrearla"),"   ",$row['medios_verificacion_evidencia_def']));


		  }

		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Obras y acciones 1"),"   ",$row['resultado1']));

		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Lugar 1"),"   ",$row['lug_result1']));

		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Obras y acciones 2"),"   ",$row['resultado2']));

		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Lugar 2"),"   ",$row['lug_result2']));

		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Obras y acciones 3"),"   ",$row['resultado3']));

		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Lugar 3"),"   ",$row['lug_result3']));

		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Obras y acciones 4"),"   ",$row['resultado4']));

		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Lugar 4"),"   ",$row['lug_result4']));

		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Obras y acciones 5"),"   ",$row['resultado5']));

		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Lugar 5"),"   ",$row['lug_result5']));

		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Bullet del logro"),"   ",$row['bullet_logro']));

		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Lo que sigue 1"),"   ",$row['lo_que_sigue_1']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Lo que sigue 2"),"   ",$row['lo_que_sigue_2']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Lo que sigue 3"),"   ",$row['lo_que_sigue_3']));

		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Comentarios"),"   ",$row['comentarios_def']));









		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Documento adjunto"),"   ","Nombre del documento"));

	}

}
else
{

	$query = "SELECT * FROM wp_indicadores_estrategicos_definidos where id_ind_estrategico=$id_evidencia";
	$resultado = $mysqli->query($query);

	$pdf=new PDF_MC_Table();
	$pdf->AddPage();
	$pdf->SetFont('Arial','',14);
	$pdf->Ln();
	//Table with 20 rows and 4 columns
	$pdf->SetWidths(array(60,10,120));
	

	while($row = $resultado->fetch_assoc())
	{

		$pdf->Row(array(utf8_decode("Nombre del indicador "),"   ",$row['nom_indicador']));
		$pdf->Ln();

		$pdf->Row(array(utf8_decode("Descripcion de Indicador"),"   ",$row['descripcion_indicador']));
		$pdf->Ln();

		$pdf->Row(array(utf8_decode("Bullet del logro"),"   ",$row['bullet_logro']));
		$pdf->Ln();


		$pdf->Row(array(utf8_decode("Linea discursiva"),"   ",$row['linea_discursiva']));
		$pdf->Ln();

		$pdf->Row(array(utf8_decode("Logro del indicador"),"   ",$row['logro_asoc_indicador']));
		$pdf->Ln();
		



		$pdf->Row(array(utf8_decode("Linea base"),"   ",$row['linea_base']));
		$pdf->Ln();

		$pdf->Row(array(utf8_decode("Resultados 2017"),"   ",$row['resultados_2017']));
		$pdf->Ln();

		$pdf->Row(array(utf8_decode("Meta 2018"),"   ",$row['meta_2018']));
		$pdf->Ln();

		$pdf->Row(array(utf8_decode("Resultado Abril 2018"),"   ",$row['result_abril_2018']));
		$pdf->Ln();

		$pdf->Row(array(utf8_decode("Avance Meta"),"   ",$row['avance_meta']));
		$pdf->Ln();

		

	
		 /*

		  $pdf->Row(array(utf8_decode("Nombre del indicador Miércoles"),"   ",$row['nombre_indicador_asociado_def']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Descripcion del indicador:"),"   ",$row['descripcion_indicador_def']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Unidad de observacion del indicador:"),"   ",$row['unidad_observacion_indicador_def']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Descripción de la unidad de la observación:"),"   ",$row['descripcion_observacion_ficha_tecnica_def']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Tipo de indicador"),"   ",$row['tipo_indicador_def']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Tendencia esperada"),"   ",$row['tendencia_esperada_def']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Linea base en absolutos:"),"   ",$row['linea_base_absolutos_def']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Linea base de acuerdo al tipo de indicador"),"   ",$row['linea_base_tipo_indicador_def']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Resultado reportado para el primer informe:"),"   ",$row['reporte_primer_informe_def']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Avance con relacion a la meta planteada:"),"   ",$row['avance_meta_planeada_def']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("tipo de evidencia se presentada"),"   ",$row['tipo_evidencia_def']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Descripcion por qué es una evidencia vinculada al indicador:"),"   ",$row['descripcion_documento_vinculado_indicador_def']));
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("fecha de la evidencia:"),"   ",$fecha_completa));
		  if($row['plan_trabajo_def']=="No")
		  {
		  	$pdf->Ln();
		    $pdf->Row(array(utf8_decode("El indicador cuenta con linea base:"),"   ",$row['plan_trabajo_def']));
		  	$pdf->Ln();
		  	$pdf->Row(array("Documento adjunto(Plan de trabajo)","   ","Nombre del documento"));
		  }
		  else
		  {
		  	$pdf->Ln();
		    $pdf->Row(array(utf8_decode("El indicador cuenta con linea base:"),"   ",$row['plan_trabajo_def']));


		  }

		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Existe informacion de donde se realizaron las acciones:"),"   ",$row['se_desagrego_geograficamente_def']));

		  if($row['se_desagrego_geograficamente_def']=="Si")
		  {
		  	  if($row['es_posible_desagregar_geograficamente_def']=="Estado de Hidalgo")
			  {
			  	$pdf->Ln();
			    $pdf->Row(array(utf8_decode("Lugar donde se realizarion las acciones"),"   ","Estado de Hidalgo"));


			  }
			  else if($row['es_posible_desagregar_geograficamente_def']=="Otras entidades")
			  {
			  	$pdf->Ln();
			    $pdf->Row(array(utf8_decode("Entidades"),"   ",$row['lugar_Acciones']));



			  }
			  else if($row['es_posible_desagregar_geograficamente_def']=="Municipios")
			  {
			  	$pdf->Ln();
			    $pdf->Row(array(utf8_decode("Municipios"),"   ",$row['lugar_Acciones']));


			  }
			  else if($row['es_posible_desagregar_geograficamente_def']=="Localidades")
			  {
			  	$pdf->Ln();
			    $pdf->Row(array(utf8_decode("Se adjunta documento con las localidades"),"   ","Nombre del documento"));

			  }

		  }

		
		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Existe expediente documental"),"   ",$row['existe_expediente_doc_def']));

		  if($row['existe_expediente_doc_def']=="Si")  //campos Si
		  {
		  		$pdf->Ln();
		  		$pdf->Row(array(utf8_decode("Serie de la Evidencia:"),"   ",$row['serie_evidencia_def']));
		  		$pdf->Ln();
		  		$pdf->Row(array(utf8_decode("Seccion"),"   ",$row['seccion_def']));
		  		$pdf->Ln();
		  		$pdf->Row(array(utf8_decode("Guía/Inventario en el que se incluye el expediente"),"   ",$row['guia_invetario_expediente_def']));

		  }
		  else if($row['existe_expediente_doc_def']=="No")
		  {

		  		$pdf->Ln();
		  		$pdf->Row(array(utf8_decode("Ubicación electrónica o física de la evidencia o como es posible rastrearla"),"   ",$row['medios_verificacion_evidencia_def']));


		  }

		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Comentarios"),"   ",$row['comentarios_def']));

		  $pdf->Ln();
		  $pdf->Row(array(utf8_decode("Documento adjunto"),"   ","Nombre del documento"));
*/
	}

}


/*

for($i=0;$i<40;$i++)
    $pdf->Row(array("Pregunta numero uno corta","   ","Respuesta corta a ala pregunta 1"));

    */
$pdf->Output();





/*


	$query = "SELECT * FROM wp_definicion_evidencias ";
	$resultado = $mysqli->query($query);
	
	$pdf = new PDF();
	$pdf->AliasNbPages();
	$pdf->AddPage();
	
	$pdf->SetFillColor(232,232,232);
	$pdf->SetFont('Arial','B',12);

	$pdf->Cell(80,6,'Pregunta',0,1,'C',0);
	//$pdf->Cell(90,6,'Respuesta',1,1,'C',1);
	//$pdf->Cell(70,6,'MUNICIPIO',1,1,'C',1);
	
	$pdf->SetFont('Arial','',10);
	
	while($row = $resultado->fetch_assoc())
	{
		
		//$pdf->MultiCell(80,6,'Nombre del indicador asociado:',1,1,'L','C');
		$pdf->MultiCell(0,6,$row['nombre_indicador_asociado_def'],0,0,'L');

		
	}
	$pdf->Output();
	*/

	
?>


