<?php
require('../fpdf.php');
require 'conexion.php';


$id_evidencia = $_GET["id_evidencia"];
$nombre_usuario = $_GET["user"];
$tipo = $_GET["tipo"];
//$id_evidencia = "";//$_GET["id_evidencia"];
//$nombre_usuario = "";//$_GET["user"];

$arr_ids_tac = $_GET["arr_ids_tacticos"];
//$arr_ids_tac = "27,28,29,30";
//$tipo = "array_ids";


class PDF_MC_Table extends FPDF
{

	//Cabecera y pie de pagina
	function Header()
	{
		// Margen del texto del documento.
		$this->SetMargins(8,65,8);
		$this->SetAutoPageBreak(true, 25);
		// Termina definición del margen.

		$this->Image('logo_hidalgo.png', 0, 0, 50 );
		$this->Image('segundoInforme.png', 158, 20, 50 );
		
		// Agregamos la fuente que vamos a ocupar.
		$this->AddFont('Graphik-Regular','','graphikRegular.php');
		$this->SetFont('Graphik-Regular','',16);
		// Color del texto.
	    $this->SetTextColor(105,105,105);
		if($this->PageNo()==1)
		{
			$this->Ln(12);
			$this->Cell(0,10,'RESUMEN',0,0,'C');
			$this->Ln(43);
		}
	}
	
	function Footer()
	{
		$this->SetY(-15);
		$this->SetFont('Arial','I', 8);
		$this->Cell(0,10, 'Pagina '.$this->PageNo().'',0,0,'C' );
	}
	//fin cabecera pie de pagina

	// Formato del texto del documento.
	function titulo($texto)
	{
		// Agregamos la fuente que vamos a ocupar.
		$this->AddFont('Graphik-SemiboldItalic','','graphikSemiboldItalic.php');
		$this->SetTextColor(0,0,0);
		$this->SetFont('Graphik-SemiboldItalic','', 12);
		$this->Row(array(utf8_decode($texto)));
	}

	function subtitulo()
	{
		// Agregamos la fuente que vamos a ocupar.
		$this->AddFont('Graphik-RegularItalic','','graphikRegularItalic.php');
		$this->SetTextColor(130,130,130);
		$this->SetFont('Graphik-RegularItalic','',12);
	}

	function contenido()
	{
		// Agregamos la fuente que vamos a ocupar.
		$this->AddFont('Graphik-Regular','','graphikRegular.php');
		$this->SetTextColor(130,130,130);
		$this->SetFont('Graphik-Regular','',12);
	}
	// Termina definición del formato.

	function columnas($bandera)
	{
		if($bandera=="1")
		{
			$this->SetWidths(array(200));
		}
		else
		{
			$this->SetWidths(array(130,5,60));
		}
	}

	var $widths;
	var $aligns;

	function SetWidths($w)
	{
	    //Set the array of column widths
	    $this->widths=$w;
	}

	function SetAligns($a)
	{
	    //Set the array of column alignments
	    $this->aligns=$a;
	}

	function Row($data)
	{
	    //Calculate the height of the row
	    $nb=0;
	    for($i=0;$i<count($data);$i++)
	        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
	    $h=5*$nb;
	    //Issue a page break first if needed
	    $this->CheckPageBreak($h);
	    //Draw the cells of the row
	    for($i=0;$i<count($data);$i++)
	    {
	        $w=$this->widths[$i];
	        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
	        //Save the current position
	        $x=$this->GetX();
	        $y=$this->GetY();
	        //Draw the border
	       // $this->Rect($x,$y,$w,$h);
	        //Print the text
	        $this->MultiCell($w,5,$data[$i],0,$a);
	        //Put the position to the right of the cell
	        $this->SetXY($x+$w,$y);
	    }
	    //Go to the next line
	    $this->Ln($h);
	}

	function CheckPageBreak($h)
	{
	    //If the height h would cause an overflow, add a new page immediately
	    if($this->GetY()+$h>$this->PageBreakTrigger)
	        $this->AddPage($this->CurOrientation);
	}

	function NbLines($w,$txt)
	{
	    //Computes the number of lines a MultiCell of width w will take
	    $cw=&$this->CurrentFont['cw'];
	    if($w==0)
	        $w=$this->w-$this->rMargin-$this->x;
	    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
	    $s=str_replace("\r",'',$txt);
	    $nb=strlen($s);
	    if($nb>0 and $s[$nb-1]=="\n")
	        $nb--;
	    $sep=-1;
	    $i=0;
	    $j=0;
	    $l=0;
	    $nl=1;
	    while($i<$nb)
	    {
	        $c=$s[$i];
	        if($c=="\n")
	        {
	            $i++;
	            $sep=-1;
	            $j=$i;
	            $l=0;
	            $nl++;
	            continue;
	        }
	        if($c==' ')
	            $sep=$i;
	        $l+=$cw[$c];
	        if($l>$wmax)
	        {
	            if($sep==-1)
	            {
	                if($i==$j)
	                    $i++;
	            }
	            else
	                $i=$sep+1;
	            $sep=-1;
	            $j=$i;
	            $l=0;
	            $nl++;
	        }
	        else
	            $i++;
	    }
	    return $nl;
	}

	function Circle($x, $y, $r, $style='D')
	{
	    $this->Ellipse($x,$y,$r,$r,$style);
	}

	function Ellipse($x, $y, $rx, $ry, $style='D')
	{
	    if($style=='F')
	        $op='f';
	    elseif($style=='FD' || $style=='DF')
	        $op='B';
	    else
	        $op='S';
	    $lx=4/3*(M_SQRT2-1)*$rx;
	    $ly=4/3*(M_SQRT2-1)*$ry;
	    $k=$this->k;
	    $h=$this->h;
	    $this->_out(sprintf('%.2F %.2F m %.2F %.2F %.2F %.2F %.2F %.2F c',
	        ($x+$rx)*$k,($h-$y)*$k,
	        ($x+$rx)*$k,($h-($y-$ly))*$k,
	        ($x+$lx)*$k,($h-($y-$ry))*$k,
	        $x*$k,($h-($y-$ry))*$k));
	    $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c',
	        ($x-$lx)*$k,($h-($y-$ry))*$k,
	        ($x-$rx)*$k,($h-($y-$ly))*$k,
	        ($x-$rx)*$k,($h-$y)*$k));
	    $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c',
	        ($x-$rx)*$k,($h-($y+$ly))*$k,
	        ($x-$lx)*$k,($h-($y+$ry))*$k,
	        $x*$k,($h-($y+$ry))*$k));
	    $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c %s',
	        ($x+$lx)*$k,($h-($y+$ry))*$k,
	        ($x+$rx)*$k,($h-($y+$ly))*$k,
	        ($x+$rx)*$k,($h-$y)*$k,
	        $op));
	}

}


if($tipo=="array_ids")
{

	$array = explode(",", $arr_ids_tac);

	$pdf=new PDF_MC_Table();

	// Agregamos las fuentes que vamos a ocupar.
	$pdf->AddFont('Graphik_Semibold','','graphikSemibold.php');
	$pdf->AddFont('Graphik-SemiboldItalic','','graphikSemiboldItalic.php');
	$pdf->AddFont('Graphik-Regular','','graphikRegular.php');
	$pdf->AddFont('Graphik-RegularItalic','','graphikRegularItalic.php');

	for ($i=0; $i < count($array); $i++) 
	{

		$id=$array[$i];

		$query = "SELECT * FROM wp_definicion_evidencias where 	id_indicador_asociado_def= $id";
		$resultado = $mysqli->query($query);

		$pdf->AddPage();

		while($row = $resultado->fetch_assoc())
		{
			// Selección del número de columnas: 3 columnas.
			$pdf->columnas($ban=3);

			// Gráfica #1.
			// $pdf->Ellipse(100,50,30,20);
			$pdf->SetFillColor(130,200,73);
			$pdf->Circle(180,90,15,'F');

			$pdf->SetFillColor(255,255,255);
			$pdf->Circle(180,90,10,'F');

			// Gráfica #2.
			$pdf->SetFillColor(196,196,196);
			$pdf->Circle(180,127,15,'F');

			$pdf->SetFillColor(255,255,255);
			$pdf->Circle(180,127,10,'F');

			// Gráfica #3.
			$pdf->SetFillColor(130,200,73);
			$pdf->Circle(180,164,15,'F');

			$pdf->SetFillColor(255,255,255);
			$pdf->Circle(180,164,10,'F');

			$pdf->SetTextColor(0,0,0);
			$pdf->SetFont('Graphik_Semibold','',14);
			$pdf->Row(array($row['eje']," "," "));
			$pdf->Ln();
			$pdf->SetFont('Graphik_Semibold','',12);
			$pdf->Row(array(utf8_decode("OBJETIVO ESTRATÉGICO...")," "," "));
			$pdf->Row(array(utf8_decode("OBJETIVO GENERAL...")," "," "));
			$pdf->Ln();
			$pdf->Ln();

			$pdf->titulo("NOMBRE DEL INDICADOR:");
			$pdf->SetFont('Graphik_Semibold','',12);
			$pdf->Row(array($row['nombre_indicador_asociado_def']," "," "));
			$pdf->Ln();

			$pdf->titulo("BULLET DEL LOGRO:");
			$pdf->contenido();
			$pdf->Row(array($row['bullet_logro']," "," "));
			$pdf->Ln();

			$pdf->titulo("LÍNEA DISCURSIVA:");
			$pdf->contenido();
			$pdf->Row(array($row['linea_discursiva']," "," "));
			$pdf->Ln();

			$pdf->titulo("CLASIFICACIÓN DEL INDICADOR:");
			$pdf->contenido();
			$pdf->Row(array($row['indicador_estrella']," "," "));
			$pdf->Ln();

			$pdf->titulo("¿HUBO PREMIO O RECONOCIMIENTO?");
			$pdf->contenido();
			$pdf->Row(array($row['hubo_premio']," "," "));
			$pdf->Ln();

				if($row['hubo_premio']=="Si")  //campos Si
				{
					$pdf->subtitulo();
					$pdf->Row(array(utf8_decode("Premio o reconocimiento:")," "," "));
					$pdf->contenido();
					$pdf->Row(array($row['premio_reconocimiento']," "," "));
					$pdf->Ln();
				}

			$pdf->titulo("RESULTADOS, OBRAS Y ACCIONES");
			$pdf->subtitulo();
			$pdf->Row(array(utf8_decode("Obras y acciones 1:")," ","Lugar:"));
			$pdf->contenido();
			$pdf->Row(array($row['resultado1']," ",$row['lug_result1']));
			$pdf->Ln();

			if ($row['resultado2'] != NULL)
			{
				$pdf->subtitulo();
				$pdf->Row(array(utf8_decode("Obras y acciones 2:")," ","Lugar:"));
				$pdf->contenido();
				$pdf->Row(array($row['resultado2']," ",$row['lug_result2']));
				$pdf->Ln();
			}

			if ($row['resultado3'] != NULL)
			{
				$pdf->subtitulo();
				$pdf->Row(array(utf8_decode("Obras y acciones 3:")," ","Lugar:"));
				$pdf->contenido();
				$pdf->Row(array($row['resultado3']," ",$row['lug_result3']));
				$pdf->Ln();
			}

			if ($row['resultado4'] != NULL)
			{
				$pdf->subtitulo();
				$pdf->Row(array(utf8_decode("Obras y acciones 4:")," ","Lugar:"));
				$pdf->contenido();
				$pdf->Row(array($row['resultado4']," ",$row['lug_result4']));
				$pdf->Ln();
			}

			if ($row['resultado5'] != NULL)
			{
				$pdf->subtitulo();
				$pdf->Row(array(utf8_decode("Obras y acciones 5:")," ","Lugar:"));
				$pdf->contenido();
				$pdf->Row(array($row['resultado5']," ",$row['lug_result5']));
				$pdf->Ln();
			}

		// Cambio del número de columnas: De 3 columnas a 1 columna.
			$pdf->columnas($ban=1);
			$pdf->titulo("LO QUE SIGUE...");
			$pdf->contenido();
			$pdf->Row(array("-"." ".$row['lo_que_sigue_1']));
			$pdf->Ln();

			if ($row['lo_que_sigue_2'] != NULL)
			{
				$pdf->Row(array("-"." ".$row['lo_que_sigue_2']));
				$pdf->Ln();
			}

			if ($row['lo_que_sigue_3'] != NULL)
			{
				$pdf->Row(array("-"." ".$row['lo_que_sigue_3']));
				$pdf->Ln();
			}

			// -------------------------------------------------------------------------------
			// Valores para gráficas del Segundo Informe de Gobierno.

		// Cambio del número de columnas: De 1 columna a 3 columnas.
			$pdf->columnas($ban=3);
			$pdf->SetTextColor(0,0,0);
			$pdf->SetFont('Graphik-SemiboldItalic','', 12);
			$pdf->Row(array(utf8_decode("LÍNEA BASE 2016:")," ",utf8_decode("RESULTADOS 2017:")));
			$pdf->contenido();
			$pdf->Row(array($row['linea_base_absolutos_def']," ",$row['reporte_primer_informe_def']));
			$pdf->Ln();

			$pdf->SetTextColor(0,0,0);
			$pdf->SetFont('Graphik-SemiboldItalic','', 12);
			$pdf->Row(array(utf8_decode("META 2018:")," ",utf8_decode("RESULTADO REPORTADO 2018:")));
			$pdf->contenido();
			$pdf->Row(array($row['meta_2018']," ",$row['avance_meta_planeada_def']));
			$pdf->Ln();

			// -------------------------------------------------------------------------------

		// Cambio del número de columnas: De 3 columnas a 1 columna.
			$pdf->columnas($ban=1);
			$pdf->titulo("DESCRIPCIÓN DEL INDICADOR:");
			$pdf->contenido();
			$pdf->Row(array($row['descripcion_indicador_def']));
			$pdf->Ln();

			$pdf->titulo("UNIDAD DE OBSERVACIÓN DEL INDICADOR:");
			$pdf->contenido();
			$pdf->Row(array($row['unidad_observacion_indicador_def']));
			$pdf->Ln();

			$pdf->titulo("DESCRIPCIÓN DE LA UNIDAD DE LA OBSERVACIÓN:");
			$pdf->contenido();
			$pdf->Row(array($row['descripcion_observacion_ficha_tecnica_def']));
			$pdf->Ln();

			$pdf->titulo("TIPO DE INDICADOR:");
			$pdf->contenido();
			$pdf->Row(array($row['tipo_indicador_def']));
			$pdf->Ln();

			$pdf->titulo("TENDENCIA ESPERADA:");
			$pdf->contenido();
			$pdf->Row(array($row['tendencia_esperada_def']));
			$pdf->Ln();

			$pdf->titulo("TIPO DE EVIDENCIA PRESENTADA:");
			$pdf->contenido();
			$pdf->Row(array($row['tipo_evidencia_def']));
			$pdf->Ln();

			$pdf->titulo("¿POR QUÉ ES UNA EVIDENCIA VINCULADA AL INDICADOR?");
			$pdf->subtitulo();
			$pdf->Row(array(utf8_decode("(Descripción)")));
			$pdf->contenido();
			$pdf->Row(array($row['descripcion_documento_vinculado_indicador_def']));
			$pdf->Ln();

			$pdf->titulo("FECHA CORTE:");
			$pdf->contenido();
			$pdf->Row(array($row['tipo_evidencia_fecha_def']));
			$pdf->Ln();

			$pdf->titulo("¿EXISTE EXPEDIENTE DOCUMENTAL?");
			$pdf->contenido();
			$pdf->Row(array($row['existe_expediente_doc_def']));
			$pdf->Ln();

				if($row['existe_expediente_doc_def']=="Si")  //campos Si
				{
				// Cambio del número de columnas: De 1 columna a 3 columnas.
					$pdf->columnas($ban=3);
					$pdf->subtitulo();
				  	$pdf->Row(array(utf8_decode("Serie de la evidencia:")," ",utf8_decode("Sección:")));
				  	$pdf->contenido();
				  	$pdf->Row(array($row['serie_evidencia_def']," ",$row['seccion_def']));
				  	$pdf->Ln();

				// Cambio del número de columnas: De 3 columnas a 1 columna.
					$pdf->columnas($ban=1);
				  	$pdf->subtitulo();
				  	$pdf->Row(array(utf8_decode("Guía/Inventario en el que se incluye el expediente:")));
				  	$pdf->contenido();
					$pdf->Row(array($row['guia_invetario_expediente_def']));
					$pdf->Ln();
				}
				else if($row['existe_expediente_doc_def']=="No")
				{
					$pdf->subtitulo();
				  	$pdf->Row(array(utf8_decode("Ubicación electrónica o física de la evidencia, o cómo es posible rastrearla:")));
				  	$pdf->contenido();
					$pdf->Row(array($row['medios_verificacion_evidencia_def']));
					$pdf->Ln();
				}

		  	$pdf->titulo("COMENTARIOS:");
		  	$pdf->contenido();
			$pdf->Row(array($row['comentarios_def']));
			$pdf->Ln();

			$pdf->titulo("DOCUMENTO ADJUNTO:");
		  	$pdf->contenido();
			$pdf->Row(array("Nombre del documento"));
			$pdf->Ln();

			$pdf->Ln();
			$pdf->Ln();
		// Cambio del número de columnas: De 1 columna a 3 columnas.
			$pdf->columnas($ban=3);
			$pdf->Row(array(" "," ","_____________________"));
			$pdf->Row(array(" "," ",$row['nombre_dependencia_def']));

			//$pdf->Row(array(utf8_decode("Linea base de acuerdo al tipo de indicador"),"   ",$row['linea_base_tipo_indicador_def']));
		}
	}
}


else if($tipo=="tactico")
{

	$query = "SELECT * FROM wp_definicion_evidencias where 	id_indicador_asociado_def=$id_evidencia";
	$resultado = $mysqli->query($query);

	$pdf=new PDF_MC_Table();
	$pdf->AddPage();

	// Agregamos las fuentes que vamos a ocupar.
	$pdf->AddFont('Graphik_Semibold','','graphikSemibold.php');
	$pdf->AddFont('Graphik-SemiboldItalic','','graphikSemiboldItalic.php');
	$pdf->AddFont('Graphik-Regular','','graphikRegular.php');
	$pdf->AddFont('Graphik-RegularItalic','','graphikRegularItalic.php');
	//$pdf->Write(10,'Enjoy new fonts with FPDF!');

	while($row = $resultado->fetch_assoc())
	{
		// Selección del número de columnas: 1 columna.
		$pdf->columnas($ban=1);

		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont('Graphik_Semibold','',14);
		$pdf->Row(array($row['eje']));
		$pdf->Ln();
		$pdf->SetFont('Graphik_Semibold','',12);
		$pdf->Row(array(utf8_decode("OBJETIVO ESTRATÉGICO...")));
		$pdf->Row(array(utf8_decode("OBJETIVO GENERAL...")));
		$pdf->Ln();
		$pdf->Ln();

		$pdf->titulo("NOMBRE DEL INDICADOR:");
		$pdf->SetFont('Graphik_Semibold','',12);
		$pdf->Row(array($row['nombre_indicador_asociado_def']));
		$pdf->Ln();

		$pdf->titulo("BULLET DEL LOGRO:");
		$pdf->contenido();
		$pdf->Row(array($row['bullet_logro']));
		$pdf->Ln();

		$pdf->titulo("LÍNEA DISCURSIVA:");
		$pdf->contenido();
		$pdf->Row(array($row['linea_discursiva']));
		$pdf->Ln();

		$pdf->titulo("CLASIFICACIÓN DEL INDICADOR:");
		$pdf->contenido();
		$pdf->Row(array($row['indicador_estrella']));
		$pdf->Ln();

		$pdf->titulo("¿HUBO PREMIO O RECONOCIMIENTO?");
		$pdf->contenido();
		$pdf->Row(array($row['hubo_premio']));
		$pdf->Ln();

			if($row['hubo_premio']=="Si")  //campos Si
			{
				$pdf->subtitulo();
				$pdf->Row(array(utf8_decode("Premio o reconocimiento:")));
				$pdf->contenido();
				$pdf->Row(array($row['premio_reconocimiento']));
				$pdf->Ln();
			}

		$pdf->titulo("RESULTADOS, OBRAS Y ACCIONES");
	// Cambio del número de columnas: De 1 columna a 3 columnas.
		$pdf->columnas($ban=3);
		$pdf->subtitulo();
		$pdf->Row(array(utf8_decode("Obras y acciones 1:")," ","Lugar:"));
		$pdf->contenido();
		$pdf->Row(array($row['resultado1']," ",$row['lug_result1']));
		$pdf->Ln();

		if ($row['resultado2'] != NULL)
		{
			$pdf->subtitulo();
			$pdf->Row(array(utf8_decode("Obras y acciones 2:")," ","Lugar:"));
			$pdf->contenido();
			$pdf->Row(array($row['resultado2']," ",$row['lug_result2']));
			$pdf->Ln();
		}

		if ($row['resultado3'] != NULL)
		{
			$pdf->subtitulo();
			$pdf->Row(array(utf8_decode("Obras y acciones 3:")," ","Lugar:"));
			$pdf->contenido();
			$pdf->Row(array($row['resultado3']," ",$row['lug_result3']));
			$pdf->Ln();
		}

		if ($row['resultado4'] != NULL)
		{
			$pdf->subtitulo();
			$pdf->Row(array(utf8_decode("Obras y acciones 4:")," ","Lugar:"));
			$pdf->contenido();
			$pdf->Row(array($row['resultado4']," ",$row['lug_result4']));
			$pdf->Ln();
		}

		if ($row['resultado5'] != NULL)
		{
			$pdf->subtitulo();
			$pdf->Row(array(utf8_decode("Obras y acciones 5:")," ","Lugar:"));
			$pdf->contenido();
			$pdf->Row(array($row['resultado5']," ",$row['lug_result5']));
			$pdf->Ln();
		}

	// Cambio del número de columnas: De 3 columnas a 1 columna.
		$pdf->columnas($ban=1);
		$pdf->titulo("LO QUE SIGUE...");
		$pdf->contenido();
		$pdf->Row(array("-"." ".$row['lo_que_sigue_1']));
		$pdf->Ln();

		if ($row['lo_que_sigue_2'] != NULL)
		{
			$pdf->Row(array("-"." ".$row['lo_que_sigue_2']));
			$pdf->Ln();
		}

		if ($row['lo_que_sigue_3'] != NULL)
		{
			$pdf->Row(array("-"." ".$row['lo_que_sigue_3']));
			$pdf->Ln();
		}

		// -------------------------------------------------------------------------------
		// Valores para gráficas del Segundo Informe de Gobierno.

	// Cambio del número de columnas: De 1 columna a 3 columnas.
		$pdf->columnas($ban=3);
		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont('Graphik-SemiboldItalic','', 12);
		$pdf->Row(array(utf8_decode("LÍNEA BASE 2016:")," ",utf8_decode("RESULTADOS 2017:")));
		$pdf->contenido();
		$pdf->Row(array($row['linea_base_absolutos_def']," ",$row['reporte_primer_informe_def']));
		$pdf->Ln();

		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont('Graphik-SemiboldItalic','', 12);
		$pdf->Row(array(utf8_decode("META 2018:")," ",utf8_decode("RESULTADO REPORTADO 2018:")));
		$pdf->contenido();
		$pdf->Row(array($row['meta_2018']," ",$row['avance_meta_planeada_def']));
		$pdf->Ln();

		// -------------------------------------------------------------------------------

	// Cambio del número de columnas: De 3 columnas a 1 columna.
		$pdf->columnas($ban=1);
		$pdf->titulo("DESCRIPCIÓN DEL INDICADOR:");
		$pdf->contenido();
		$pdf->Row(array($row['descripcion_indicador_def']));
		$pdf->Ln();

		$pdf->titulo("UNIDAD DE OBSERVACIÓN DEL INDICADOR:");
		$pdf->contenido();
		$pdf->Row(array($row['unidad_observacion_indicador_def']));
		$pdf->Ln();

		$pdf->titulo("DESCRIPCIÓN DE LA UNIDAD DE LA OBSERVACIÓN:");
		$pdf->contenido();
		$pdf->Row(array($row['descripcion_observacion_ficha_tecnica_def']));
		$pdf->Ln();

		$pdf->titulo("TIPO DE INDICADOR:");
		$pdf->contenido();
		$pdf->Row(array($row['tipo_indicador_def']));
		$pdf->Ln();

		$pdf->titulo("TENDENCIA ESPERADA:");
		$pdf->contenido();
		$pdf->Row(array($row['tendencia_esperada_def']));
		$pdf->Ln();

		$pdf->titulo("TIPO DE EVIDENCIA PRESENTADA:");
		$pdf->contenido();
		$pdf->Row(array($row['tipo_evidencia_def']));
		$pdf->Ln();

		$pdf->titulo("¿POR QUÉ ES UNA EVIDENCIA VINCULADA AL INDICADOR?");
		$pdf->subtitulo();
		$pdf->Row(array(utf8_decode("(Descripción)")));
		$pdf->contenido();
		$pdf->Row(array($row['descripcion_documento_vinculado_indicador_def']));
		$pdf->Ln();

		$pdf->titulo("FECHA CORTE:");
		$pdf->contenido();
		$pdf->Row(array($row['tipo_evidencia_fecha_def']));
		$pdf->Ln();

		$pdf->titulo("¿EXISTE EXPEDIENTE DOCUMENTAL?");
		$pdf->contenido();
		$pdf->Row(array($row['existe_expediente_doc_def']));
		$pdf->Ln();

			if($row['existe_expediente_doc_def']=="Si")  //campos Si
			{
			// Cambio del número de columnas: De 1 columna a 3 columnas.
				$pdf->columnas($ban=3);
				$pdf->subtitulo();
			  	$pdf->Row(array(utf8_decode("Serie de la evidencia:")," ",utf8_decode("Sección:")));
			  	$pdf->contenido();
			  	$pdf->Row(array($row['serie_evidencia_def']," ",$row['seccion_def']));
			  	$pdf->Ln();

			// Cambio del número de columnas: De 3 columnas a 1 columna.
				$pdf->columnas($ban=1);
			  	$pdf->subtitulo();
			  	$pdf->Row(array(utf8_decode("Guía/Inventario en el que se incluye el expediente:")));
			  	$pdf->contenido();
				$pdf->Row(array($row['guia_invetario_expediente_def']));
				$pdf->Ln();
			}
			else if($row['existe_expediente_doc_def']=="No")
			{
				$pdf->subtitulo();
			  	$pdf->Row(array(utf8_decode("Ubicación electrónica o física de la evidencia, o cómo es posible rastrearla:")));
			  	$pdf->contenido();
				$pdf->Row(array($row['medios_verificacion_evidencia_def']));
				$pdf->Ln();
			}

	  	$pdf->titulo("COMENTARIOS:");
	  	$pdf->contenido();
		$pdf->Row(array($row['comentarios_def']));
		$pdf->Ln();

		$pdf->titulo("DOCUMENTO ADJUNTO:");
	  	$pdf->contenido();
		$pdf->Row(array("Nombre del documento"));
		$pdf->Ln();

		$pdf->Ln();
		$pdf->Ln();
	// Cambio del número de columnas: De 1 columna a 3 columnas.
		$pdf->columnas($ban=3);
		$pdf->Row(array(" "," ","_____________________"));
		$pdf->Row(array(" "," ",$row['nombre_dependencia_def']));

		//$pdf->Row(array(utf8_decode("Linea base de acuerdo al tipo de indicador"),"   ",$row['linea_base_tipo_indicador_def']));
	}
}


else if($tipo=="estrategico")
{

	$query = "SELECT * FROM wp_indicadores_estrategicos_definidos where id_ind_estrategico=$id_evidencia";
	$resultado = $mysqli->query($query);

	$pdf=new PDF_MC_Table();
	$pdf->AddPage();

	// Agregamos las fuentes que vamos a ocupar.
	$pdf->AddFont('Graphik_Semibold','','graphikSemibold.php');
	$pdf->AddFont('Graphik-SemiboldItalic','','graphikSemiboldItalic.php');
	$pdf->AddFont('Graphik-Regular','','graphikRegular.php');
	$pdf->AddFont('Graphik-RegularItalic','','graphikRegularItalic.php');

	while($row = $resultado->fetch_assoc())
	{
		// Selección del número de columnas: 1 columna.
		$pdf->columnas($ban=1);

		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont('Graphik_Semibold','',14);
		$pdf->Row(array(utf8_decode("EJE...")));
		$pdf->Ln();
		$pdf->SetFont('Graphik_Semibold','',12);
		$pdf->Row(array(utf8_decode("OBJETIVO...")));
		$pdf->Ln();
		$pdf->Ln();

		$pdf->titulo("NOMBRE DEL INDICADOR:");
		$pdf->SetFont('Graphik_Semibold','',12);
		$pdf->Row(array($row['nom_indicador']));
		$pdf->Ln();

		$pdf->titulo("DESCRIPCIÓN DEL INDICADOR:");
		$pdf->contenido();
		$pdf->Row(array($row['descripcion_indicador']));
		$pdf->Ln();

		$pdf->titulo("LÍNEA DISCURSIVA:");
		$pdf->contenido();
		$pdf->Row(array($row['linea_discursiva']));
		$pdf->Ln();

		$pdf->titulo("¿HUBO LOGRO?");
		$pdf->contenido();
		$pdf->Row(array($row['hubo_logro']));
		$pdf->Ln();

			if($row['hubo_logro']=="Si")  //campos Si
			{
				$pdf->subtitulo();
				$pdf->Row(array(utf8_decode("Bullet del logro:")));
				$pdf->contenido();
				$pdf->Row(array($row['bullet_logro']));
				$pdf->Ln();
			}

		$pdf->titulo("¿HUBO PREMIO O RECONOCIMIENTO?");
		$pdf->contenido();
		$pdf->Row(array($row['logro_asoc_indicador']));
		$pdf->Ln();

			if($row['logro_asoc_indicador']=="Si")  //campos Si
			{
				$pdf->subtitulo();
				$pdf->Row(array(utf8_decode("Premio o reconocimiento:")));
				$pdf->contenido();
				$pdf->Row(array($row['descripcion_logro']));
				$pdf->Ln();
			}

		// -------------------------------------------------------------------------------
		// Valores para gráficas del Segundo Informe de Gobierno.

		$pdf->titulo("LÍNEA BASE:");
		$pdf->contenido();
		$pdf->Row(array($row['linea_base']));
		$pdf->Ln();

	// Cambio del número de columnas: De 1 columna a 3 columnas.
		$pdf->columnas($ban=3);
		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont('Graphik-SemiboldItalic','', 12);
		$pdf->Row(array(utf8_decode("META 2022:")," ",utf8_decode("META 2030:")));
		$pdf->contenido();
		$pdf->Row(array($row['meta_2022']," ",$row['meta_2030']));
		$pdf->Ln();

	// Cambio del número de columnas: De 3 columnas a 1 columna.
		$pdf->columnas($ban=1);
		$pdf->titulo("RESULTADO 2018:");
		$pdf->contenido();
		$pdf->Row(array($row['result_abril_2018']));
		$pdf->Ln();

		// -------------------------------------------------------------------------------

		$pdf->Ln();
		$pdf->Ln();
	// Cambio del número de columnas: De 1 columna a 3 columnas.
		$pdf->columnas($ban=3);
		$pdf->Row(array(" "," ","_____________________"));
		$pdf->Row(array(" "," ",$row['dependecia_encargada']));
	}
}


else if($tipo=="concurrentes")
{

	$query = "SELECT * FROM wp_acciones_concurrentes where id_accion_concurrente=$id_evidencia";
	$resultado = $mysqli->query($query);

	$pdf=new PDF_MC_Table();
	$pdf->AddPage();

	// Agregamos las fuentes que vamos a ocupar.
	$pdf->AddFont('Graphik_Semibold','','graphikSemibold.php');
	$pdf->AddFont('Graphik-SemiboldItalic','','graphikSemiboldItalic.php');
	$pdf->AddFont('Graphik-Regular','','graphikRegular.php');
	$pdf->AddFont('Graphik-RegularItalic','','graphikRegularItalic.php');

	while($row = $resultado->fetch_assoc())
	{
		// Selección del número de columnas: 1 columna.
		$pdf->columnas($ban=1);

		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont('Graphik_Semibold','',14);
		$pdf->Row(array($row['eje_principal']));
		$pdf->Ln();
		$pdf->SetFont('Graphik_Semibold','',12);
		$pdf->Row(array($row['objetivo_est']));
		$pdf->Row(array($row['objetivo_gen']));
		$pdf->Ln();
		$pdf->Ln();

		$pdf->titulo("NOMBRE DE LA ACCIÓN:");
		$pdf->SetFont('Graphik_Semibold','',12);
		$pdf->Row(array($row['nombre_accion']));
		$pdf->Ln();

		$pdf->titulo("DEPENDENCIAS INVOLUCRADAS:");
		$pdf->contenido();
		$pdf->Row(array($row['dependencias_involucradas']));
		$pdf->Ln();

		$pdf->titulo("DESCRIPCIÓN DE ACCIONES:");
		$pdf->contenido();
		$pdf->Row(array($row['descripcion_acciones']));
		$pdf->Ln();

		$pdf->titulo("FECHA DE REGISTRO:");
		$pdf->contenido();
		$pdf->Row(array($row['fecha_registro']));
		$pdf->Ln();

		$pdf->Ln();
		$pdf->Ln();
	// Cambio del número de columnas: De 1 columna a 3 columnas.
		$pdf->columnas($ban=3);
		$pdf->Row(array(" "," ","_____________________"));
		$pdf->Row(array(" "," ",$row['nomb_dependencia']));
	}
}


else
{

	$query = "SELECT * FROM wp_acciones_sin_indicador where id_accion_sin_ind=$id_evidencia";
	$resultado = $mysqli->query($query);

	$pdf=new PDF_MC_Table();
	$pdf->AddPage();

	// Agregamos las fuentes que vamos a ocupar.
	$pdf->AddFont('Graphik_Semibold','','graphikSemibold.php');
	$pdf->AddFont('Graphik-SemiboldItalic','','graphikSemiboldItalic.php');
	$pdf->AddFont('Graphik-Regular','','graphikRegular.php');
	$pdf->AddFont('Graphik-RegularItalic','','graphikRegularItalic.php');

	while($row = $resultado->fetch_assoc())
	{
		// Selección del número de columnas: 1 columna.
		$pdf->columnas($ban=1);

		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont('Graphik_Semibold','',14);
		$pdf->Row(array($row['eje_principal']));
		$pdf->Ln();
		$pdf->SetFont('Graphik_Semibold','',12);
		$pdf->Row(array($row['objetivo_est']));
		$pdf->Row(array($row['objetivo_gen']));
		$pdf->Ln();
		$pdf->Ln();

		$pdf->titulo("ACCIONES");
		$pdf->subtitulo();
		$pdf->Row(array(utf8_decode("Acción 1:")));
		$pdf->contenido();
		$pdf->Row(array($row['accion_1']));
		$pdf->Ln();


		$pdf->titulo("FECHA DE REGISTRO:");
		$pdf->contenido();
		$pdf->Row(array($row['fecha_registro']));
		$pdf->Ln();

		$pdf->Ln();
		$pdf->Ln();
	// Cambio del número de columnas: De 1 columna a 3 columnas.
		$pdf->columnas($ban=3);
		$pdf->Row(array(" "," ","_____________________"));
		$pdf->Row(array(" "," ",$row['dependencia']));
	}
}


$pdf->Output();
?>