<?php
require('../fpdf.php');
require '../conexion.php';

$CVE =$_POST["CVE"]; //nombre  a buscar

	//Encabezado y pie de pagina
	class PDF extends FPDF
	{
		function Header()
		{
			$this->Image('logo_hidalgo.png', 5, 5, 30 );
			$this->SetFont('Arial','B',15);
			$this->Cell(30);
			$this->Cell(220,10,$_POST["CVE"],0,0,'C');
			$this->Ln(20);
		}
		
		function Footer()
		{
			$this->SetY(-15);
			$this->SetFont('Arial','I', 8);
			$this->Cell(0,10, 'Pagina '.$this->PageNo().'/{nb}',0,0,'C' );
		}		
	}
	// fin encabezado y pie de pagina

	$query = "SELECT * FROM web_service_prueba WHERE CVE = $CVE";
	$resultado = $mysqli->query($query);
	
	$pdf = new PDF();
	$pdf->AliasNbPages();
	$pdf->addPage('L');			//hoja en LandPage(Horinotal)  
	
	$pdf->SetFont('Arial','B',16);
	$pdf->SetFillColor(232,232,232);
	$pdf->SetFont('Arial','B',10);

	$pdf->Ln(5);

	$pdf->Cell(28,20,"% Rezago Edu.",1,0,'C',1);
	$pdf->Cell(28,20,"% C.Ser. Salud",1,0,'C',1);
	$pdf->Cell(28,20,'% C.C. E.V Viv',1,0,'C',1);
	$pdf->Cell(28,20,'% C.S Vivienda',1,0,'C',1);
	$pdf->Cell(28,20,'% C.Seg Social',1,0,'C',1);
	$pdf->Cell(28,20,'Per. rezago E.',1,0,'C',1);
	$pdf->Cell(28,20,'Per C.Ser. S.',1,0,'C',1);
	$pdf->Cell(28,20,'Per C.C. E.V. ',1,0,'C',1);
	$pdf->Cell(28,20,'Per C.S Viv.',1,0,'C',1);
	$pdf->Cell(28,20,'Per C.Seg Soc.',1,1,'C',1);




	//Porcentaje de poblacion con rezago educativo
	//porcentaje de poblacion con carencias por servicions de salud
	//porcentaje de poblacion  de carencias por calidad y espacios de la vivienda 
	//porcentaje de poblacion con carencias por servicios de la vivienda
	//porcentaje de poblacion en carencias por seguridad social

	//-1 dato no disponible
	//personas con rezago educativo



	$pdf->SetFont('Arial','',10);
	
	while($row = $resultado->fetch_assoc())
	{	
		$pdf->Cell(28,6,$row['P_IndEdu'],1,0,'C');
		$pdf->Cell(28,6,$row['P_IndSalud'],1,0,'C');
		$pdf->Cell(28,6,$row['P_IndCyEVi'],1,0,'C');
		$pdf->Cell(28,6,$row['P_IndServB'],1,0,'C');
		$pdf->Cell(28,6,$row['P_IndSSoci'],1,0,'C');
		
		$pdf->Cell(28,6,$row['Ind_Edu'],1,0,'C');
		$pdf->Cell(28,6,$row['Ind_ASSalu'],1,0,'C');
		$pdf->Cell(28,6,$row['Ind_CyEViv'],1,0,'C');
		$pdf->Cell(28,6,$row['Ind_ASBViv'],1,0,'C');
		$pdf->Cell(28,6,$row['Ind_ASSoci'],1,0,'C');

	}
	$pdf->Output();




	
?>
