<?php
$type = 'TrueType';
$name = 'Graphik-Semibold';
$desc = array('Ascent'=>822,'Descent'=>-178,'CapHeight'=>715,'Flags'=>32,'FontBBox'=>'[-238 -228 1159 1128]','ItalicAngle'=>0,'StemV'=>120,'MissingWidth'=>512);
$up = -75;
$ut = 50;
$cw = array(
	chr(0)=>512,chr(1)=>512,chr(2)=>512,chr(3)=>512,chr(4)=>512,chr(5)=>512,chr(6)=>512,chr(7)=>512,chr(8)=>512,chr(9)=>512,chr(10)=>512,chr(11)=>512,chr(12)=>512,chr(13)=>512,chr(14)=>512,chr(15)=>512,chr(16)=>512,chr(17)=>512,chr(18)=>512,chr(19)=>512,chr(20)=>512,chr(21)=>512,
	chr(22)=>512,chr(23)=>512,chr(24)=>512,chr(25)=>512,chr(26)=>512,chr(27)=>512,chr(28)=>512,chr(29)=>512,chr(30)=>512,chr(31)=>512,' '=>222,'!'=>316,'"'=>430,'#'=>549,'$'=>640,'%'=>811,'&'=>734,'\''=>240,'('=>343,')'=>343,'*'=>434,'+'=>551,
	','=>298,'-'=>364,'.'=>293,'/'=>423,'0'=>713,'1'=>431,'2'=>593,'3'=>635,'4'=>656,'5'=>615,'6'=>654,'7'=>558,'8'=>639,'9'=>655,':'=>300,';'=>310,'<'=>509,'='=>530,'>'=>509,'?'=>532,'@'=>919,'A'=>720,
	'B'=>659,'C'=>748,'D'=>726,'E'=>595,'F'=>567,'G'=>780,'H'=>764,'I'=>325,'J'=>457,'K'=>686,'L'=>560,'M'=>911,'N'=>758,'O'=>816,'P'=>626,'Q'=>816,'R'=>672,'S'=>640,'T'=>597,'U'=>739,'V'=>701,'W'=>971,
	'X'=>719,'Y'=>648,'Z'=>615,'['=>390,'\\'=>423,']'=>390,'^'=>522,'_'=>419,'`'=>400,'a'=>564,'b'=>637,'c'=>578,'d'=>637,'e'=>587,'f'=>371,'g'=>636,'h'=>615,'i'=>281,'j'=>281,'k'=>555,'l'=>280,'m'=>923,
	'n'=>615,'o'=>616,'p'=>637,'q'=>637,'r'=>409,'s'=>502,'t'=>377,'u'=>612,'v'=>572,'w'=>826,'x'=>574,'y'=>567,'z'=>499,'{'=>405,'|'=>262,'}'=>405,'~'=>492,chr(127)=>512,chr(128)=>703,chr(129)=>512,chr(130)=>289,chr(131)=>429,
	chr(132)=>498,chr(133)=>861,chr(134)=>394,chr(135)=>394,chr(136)=>400,chr(137)=>1187,chr(138)=>640,chr(139)=>312,chr(140)=>970,chr(141)=>512,chr(142)=>615,chr(143)=>512,chr(144)=>512,chr(145)=>290,chr(146)=>290,chr(147)=>498,chr(148)=>498,chr(149)=>357,chr(150)=>535,chr(151)=>924,chr(152)=>400,chr(153)=>744,
	chr(154)=>502,chr(155)=>312,chr(156)=>989,chr(157)=>512,chr(158)=>499,chr(159)=>648,chr(160)=>222,chr(161)=>317,chr(162)=>584,chr(163)=>635,chr(164)=>512,chr(165)=>621,chr(166)=>266,chr(167)=>582,chr(168)=>400,chr(169)=>843,chr(170)=>460,chr(171)=>487,chr(172)=>512,chr(173)=>512,chr(174)=>516,chr(175)=>400,
	chr(176)=>443,chr(177)=>551,chr(178)=>329,chr(179)=>351,chr(180)=>400,chr(181)=>512,chr(182)=>564,chr(183)=>293,chr(184)=>400,chr(185)=>266,chr(186)=>511,chr(187)=>487,chr(188)=>753,chr(189)=>760,chr(190)=>773,chr(191)=>532,chr(192)=>720,chr(193)=>720,chr(194)=>720,chr(195)=>720,chr(196)=>720,chr(197)=>720,
	chr(198)=>983,chr(199)=>748,chr(200)=>595,chr(201)=>595,chr(202)=>595,chr(203)=>595,chr(204)=>325,chr(205)=>325,chr(206)=>325,chr(207)=>325,chr(208)=>753,chr(209)=>758,chr(210)=>816,chr(211)=>816,chr(212)=>816,chr(213)=>816,chr(214)=>816,chr(215)=>534,chr(216)=>816,chr(217)=>739,chr(218)=>739,chr(219)=>739,
	chr(220)=>739,chr(221)=>648,chr(222)=>626,chr(223)=>619,chr(224)=>564,chr(225)=>564,chr(226)=>564,chr(227)=>564,chr(228)=>564,chr(229)=>564,chr(230)=>898,chr(231)=>578,chr(232)=>587,chr(233)=>587,chr(234)=>587,chr(235)=>587,chr(236)=>281,chr(237)=>281,chr(238)=>281,chr(239)=>281,chr(240)=>616,chr(241)=>615,
	chr(242)=>616,chr(243)=>616,chr(244)=>616,chr(245)=>616,chr(246)=>616,chr(247)=>553,chr(248)=>616,chr(249)=>612,chr(250)=>612,chr(251)=>612,chr(252)=>612,chr(253)=>567,chr(254)=>637,chr(255)=>567);
$enc = 'cp1252';
$uv = array(0=>array(0,128),128=>8364,130=>8218,131=>402,132=>8222,133=>8230,134=>array(8224,2),136=>710,137=>8240,138=>352,139=>8249,140=>338,142=>381,145=>array(8216,2),147=>array(8220,2),149=>8226,150=>array(8211,2),152=>732,153=>8482,154=>353,155=>8250,156=>339,158=>382,159=>376,160=>array(160,96));
$file = 'Graphik-Semibold.z';
$originalsize = 79072;
$subsetted = true;
?>
