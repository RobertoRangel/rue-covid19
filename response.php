
<?php
	//include connection file 
	include_once("connection.php");
	 
	// initilize all variable
	$params =  $totalRecords = $data = array();

	$sqlTot = $sqlRec = $where = "";
	
	
	$params = $_REQUEST;
	$limit = $params["rowCount"];
	//$limit=10;
	//$palabra_clave="Acatlan";

	if (isset($params["current"])) { $page  = $params["current"]; } else { $page=1; };  
    $start_from = ($page-1) * $limit;
	// check search value exist
	if( !empty($params['searchPhrase']) ) {  

		$palabra_clave= $params['searchPhrase'];

    
		if($palabra_clave=="Acatlan")
		{ $ids_mun="0"; }
		else if($palabra_clave=="Acaxochitlan")
		{ $ids_mun="1"; }
		else if($palabra_clave=="Actopan")
		{ $ids_mun="2"; }
		else if($palabra_clave=="Agua Blanca")
		{ $ids_mun="3"; }
		else if($palabra_clave=="Ajacuba")
		{ $ids_mun="4"; }
		else if($palabra_clave=="Alfajayucan")
		{ $ids_mun="5"; }
		else if($palabra_clave=="Almoloya")
		{ $ids_mun="6"; }
		else if($palabra_clave=="Apan")
		{ $ids_mun="7"; }
		else if($palabra_clave=="Atitalaquia")
		{ $ids_mun="8"; }
		else if($palabra_clave=="Atlapexco")
		{ $ids_mun="9"; }
		else if($palabra_clave=="Atotonilco de Tula")
		{ $ids_mun="10"; }
		else if($palabra_clave=="Atotonilco el Grande")
		{ $ids_mun="11"; }

		else if($palabra_clave=="Calnali")
		{ $ids_mun="12"; }
	    else if($palabra_clave=="Cardonal")
		{ $ids_mun="13"; }
	    else if($palabra_clave=="Chapantongo")
		{ $ids_mun="14"; }
	    else if($palabra_clave=="Chapulhuacán")
		{ $ids_mun="15"; }
		else if($palabra_clave=="Chilcuautla")
		{ $ids_mun="16"; }
		else if($palabra_clave=="Cuautepec")
		{ $ids_mun="17"; }
		else if($palabra_clave=="El Arenal")
		{ $ids_mun="18"; }
	    else if($palabra_clave=="Eloxochitlán")
		{ $ids_mun="19"; }
		else if($palabra_clave=="Emiliano Zapata")
		{ $ids_mun="20"; }
	    else if($palabra_clave=="Epazoyucan")
		{ $ids_mun="21"; }
		else if($palabra_clave=="Francisco I. Madero")
		{ $ids_mun="22"; }
		else if($palabra_clave=="Huasca")
		{ $ids_mun="23"; }
		else if($palabra_clave=="Huautla")
		{ $ids_mun="24"; }
		else if($palabra_clave=="Huazalingo")
		{ $ids_mun="25"; }
		else if($palabra_clave=="Huehuetla")
		{ $ids_mun="26"; }
		else if($palabra_clave=="Huejutla")
		{ $ids_mun="27"; }
		else if($palabra_clave=="Huichapan")
		{ $ids_mun="28"; }
		else if($palabra_clave=="Ixmiquilpan")
		{ $ids_mun="29"; }
		else if($palabra_clave=="Jacala")
		{ $ids_mun="30"; }
		else if($palabra_clave=="Jaltocan")
		{ $ids_mun="31"; }
		else if($palabra_clave=="Juarez Hidalgo")
		{ $ids_mun="32"; }
		else if($palabra_clave=="La Mision")
		{ $ids_mun="33"; }
		else if($palabra_clave=="Lolotla")
		{ $ids_mun="34"; }
		else if($palabra_clave=="Metepec")
		{ $ids_mun="35"; }
		else if($palabra_clave=="Metztitlan")
		{ $ids_mun="36"; }
		else if($palabra_clave=="Mineral de la Reforma")
		{ $ids_mun="37"; }
		else if($palabra_clave=="Mineral del Chico")
		{ $ids_mun="38"; }
		else if($palabra_clave=="Mineral del Monte")
		{ $ids_mun="39"; }
		else if($palabra_clave=="Mixquiahuala")
		{ $ids_mun="40"; }
		else if($palabra_clave=="Molango")
		{ $ids_mun="41"; }
		else if($palabra_clave=="Nicolás Flores")
		{ $ids_mun="42"; }
		else if($palabra_clave=="Nopala")
		{ $ids_mun="43"; }
		else if($palabra_clave=="Omitlan")
		{ $ids_mun="44"; }
		else if($palabra_clave=="Pachuca")
		{ $ids_mun="45"; }
		else if($palabra_clave=="Pacula")
		{ $ids_mun="46"; }
		else if($palabra_clave=="Pisaflores")
		{ $ids_mun="47"; }
		else if($palabra_clave=="Progreso")
		{ $ids_mun="48"; }
		else if($palabra_clave=="San Agustin Metzquititlan")
		{ $ids_mun="49"; }
		else if($palabra_clave=="San Agustin Tlaxiaca")
		{ $ids_mun="50"; }
		else if($palabra_clave=="San Bartolo")
		{ $ids_mun="51"; }
		else if($palabra_clave=="San Felipe")
		{ $ids_mun="52"; }
		else if($palabra_clave=="San Salvador")
		{ $ids_mun="53"; }
		else if($palabra_clave=="Santiago de Anaya")
		{ $ids_mun="54"; }
		else if($palabra_clave=="Santiago Tulantepec")
		{ $ids_mun="55"; }
		else if($palabra_clave=="Singuilucan")
		{ $ids_mun="56"; }
		else if($palabra_clave=="Tasquillo")
		{ $ids_mun="57"; }
		else if($palabra_clave=="Tecozautla")
		{ $ids_mun="58"; }
		else if($palabra_clave=="Tenango")
		{ $ids_mun="59"; }
		else if($palabra_clave=="Tepeapulco")
		{ $ids_mun="60"; }
		else if($palabra_clave=="Tepehuacan")
		{ $ids_mun="61"; }
		else if($palabra_clave=="Tepeji")
		{ $ids_mun="62"; }
		else if($palabra_clave=="Tepetitlan")
		{ $ids_mun="63"; }
		else if($palabra_clave=="Tetepango")
		{ $ids_mun="64"; }
		else if($palabra_clave=="Tezontepec")
		{ $ids_mun="65"; }
		else if($palabra_clave=="Tianguistengo")
		{ $ids_mun="66"; }
		else if($palabra_clave=="Tizayuca")
		{ $ids_mun="67"; }
		else if($palabra_clave=="Tlahuelilpan")
		{ $ids_mun="68"; }
		else if($palabra_clave=="Tlahuiltepa")
		{ $ids_mun="69"; }
		else if($palabra_clave=="Tlanalapa")
		{ $ids_mun="70"; }
		else if($palabra_clave=="Tlanchinol")
		{ $ids_mun="71"; }
		else if($palabra_clave=="Tlaxcoapan")
		{ $ids_mun="72"; }
		else if($palabra_clave=="Tolcayuca")
		{ $ids_mun="73"; }
		else if($palabra_clave=="Tula")
		{ $ids_mun="74"; }
		else if($palabra_clave=="Tulancingo")
		{ $ids_mun="75"; }
		else if($palabra_clave=="Villa de Tezontepec")
		{ $ids_mun="76"; }
		else if($palabra_clave=="Xochiatipan")
		{ $ids_mun="77"; }
		else if($palabra_clave=="Xochicoatlan")
		{ $ids_mun="78"; }
		else if($palabra_clave=="Yahualica")
		{ $ids_mun="79"; }
		else if($palabra_clave=="Zacualtipan")
		{ $ids_mun="80"; }
		else if($palabra_clave=="Zapotlan")
		{ $ids_mun="81"; }
		else if($palabra_clave=="Zempoala")
		{ $ids_mun="82"; }
		else if($palabra_clave=="Zimapan")
		{ $ids_mun="83"; }




		$where .=" WHERE ";//
		
		$query_lug1_ids = mysqli_query($conn, "SELECT id_indicador_asociado_def,lug_result1 FROM wp_definicion_evidencias ") or die("database error:".mysqli_error($conn));

		while( $row = mysqli_fetch_assoc($query_lug1_ids) )
		 { 
		 	//echo("dwedw");

			$arr_id_mun_1 = explode(",", $row['lug_result1']);
			for ($a=0; $a < count($arr_id_mun_1) ; $a++) {

				if($arr_id_mun_1[$a]==$ids_mun)
					{
						//echo($row['id_indicador_asociado_def']." ");
						$where .=" id_indicador_asociado_def = '".$row['id_indicador_asociado_def']."'OR ";
					}	
			}
		 }

		$query_lug2_ids = mysqli_query($conn, "SELECT id_indicador_asociado_def,lug_result2 FROM wp_definicion_evidencias ") or die("database error:".mysqli_error($conn));


		while( $row = mysqli_fetch_assoc($query_lug2_ids) )
		 { 
			$arr_id_mun_2 = explode(",", $row['lug_result2']);
			for ($a=0; $a < count($arr_id_mun_2) ; $a++) {

				if($arr_id_mun_2[$a]==$ids_mun)
					{
						//echo($row['id_indicador_asociado_def']." ");
						$where .=" id_indicador_asociado_def = '".$row['id_indicador_asociado_def']."'OR ";
					}	
			}
		 }

		$query_lug3_ids = mysqli_query($conn, "SELECT id_indicador_asociado_def,lug_result3 FROM wp_definicion_evidencias ") or die("database error:".mysqli_error($conn));


		while( $row = mysqli_fetch_assoc($query_lug3_ids) )
		 { 
			$arr_id_mun_3 = explode(",", $row['lug_result3']);
			for ($a=0; $a < count($arr_id_mun_3) ; $a++) {

				if($arr_id_mun_3[$a]==$ids_mun)
					{
						//echo($row['id_indicador_asociado_def']." ");
						$where .=" id_indicador_asociado_def = '".$row['id_indicador_asociado_def']."'OR ";
					}	
			}
		 }

		$query_lug4_ids = mysqli_query($conn, "SELECT id_indicador_asociado_def,lug_result4 FROM wp_definicion_evidencias ") or die("database error:".mysqli_error($conn));


		while( $row = mysqli_fetch_assoc($query_lug4_ids) )
		 { 
			$arr_id_mun_4 = explode(",", $row['lug_result4']);
			for ($a=0; $a < count($arr_id_mun_4) ; $a++) {

				if($arr_id_mun_4[$a]==$ids_mun)
					{
						//echo($row['id_indicador_asociado_def']." ");
						$where .=" id_indicador_asociado_def = '".$row['id_indicador_asociado_def']."'OR ";
					}	
			}
		 }

		$query_lug5_ids = mysqli_query($conn, "SELECT id_indicador_asociado_def,lug_result5 FROM wp_definicion_evidencias ") or die("database error:".mysqli_error($conn));


		while( $row = mysqli_fetch_assoc($query_lug5_ids) )
		 { 
			$arr_id_mun_5 = explode(",", $row['lug_result5']);
			for ($a=0; $a < count($arr_id_mun_5) ; $a++) {

				if($arr_id_mun_5[$a]==$ids_mun)
					{
						//echo($row['id_indicador_asociado_def']." ");
						$where .=" id_indicador_asociado_def = '".$row['id_indicador_asociado_def']."'OR ";
					}	
			}
		 }

		 $where = substr($where, 0, -3);

	}
	if( !empty($params['sort']) ) {  
		$where .=" ORDER By ".key($params['sort']) .' '.current($params['sort'])." ";
	}
	// getting total number records without any search
	$sql = "SELECT * FROM `wp_definicion_evidencias` ";
	$sqlTot .= $sql;
	$sqlRec .= $sql;
	
	
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

		$sqlTot .= $where;
		$sqlRec .= $where;
	}
	if ($limit!=-1)
	$sqlRec .= "LIMIT $start_from, $limit";

	//echo($sqlRec);
		
	$queryTot = mysqli_query($conn, $sqlTot) or die("database error:". mysqli_error($conn));


	$totalRecords = mysqli_num_rows($queryTot);

	$queryRecords = mysqli_query($conn, $sqlRec) or die("error to fetch employees data");

	//iterate on results row and create new index array of data
	while( $row = mysqli_fetch_assoc($queryRecords) ) { 
		$data[] = $row;
		//echo "<pre>";print_R($data);die;
	}	
	

	$json_data = array(
			"current"            => intval( $params['current'] ), 
			"rowCount"            => 10, 			
			"total"    => intval( $totalRecords ),
			"rows"            => $data   // total data array
			);
			

	echo json_encode($json_data);  // send data as json format
?>
	