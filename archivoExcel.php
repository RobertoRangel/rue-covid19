<?php

$mysqli = new mysqli("localhost","oscar","osocerdo","sigeh"); 

if(mysqli_connect_errno()){
	echo 'Conexion Fallida Men : ', mysqli_connect_error();
	exit();
}	

date_default_timezone_set('America/Mexico_City');

if (PHP_SAPI == 'cli')
    die('Este archivo solo se puede ver desde un navegador web');

// Se agrega la libreria PHPExcel
 require_once 'PHPExcel/Classes/PHPExcel.php';

$id_evidencia = $_GET["id_evidencia"];
$nombre_usuario = $_GET["user"];
$tipo = $_GET["tipo"];  
$arr_ids_tac = $_GET["arr_ids_tacticos"];     // "14,16,18,27";
$arr_muni = array("Acatlán","Acaxochitlán","Actopan","Agua Blanca de Iturbide","Ajacuba","Alfajayucan","Almoloya","Apan","Atitalaquia","Atlapexco","Atotonilco de Tula","Atotonilco el Grande","Calnali","Cardonal","Chapantongo","Chapulhuacán","Chilcuautla","Cuautepec de Hinojosa","El Arenal","Eloxochitlán","Emiliano Zapata","Epazoyucan","Francisco I. Madero","Huasca de Ocampo","Huautla","Huazalingo","Huehuetla","Huejutla de Reyes","Huichapan","Ixmiquilpan","Jacala de Ledezma","Jaltocán","Juárez Hidalgo","La Misión","Lolotla","Metepec","Metztitlán","Mineral de la Reforma","Mineral del Chico","Mineral del Monte","Mixquiahuala de Juárez","Molango de Escamilla","Nicolás Flores","Nopala de Villagrán","Omitlán de Juárez","Pachuca de Soto","Pacula","Pisaflores","Progreso de Obregón","San Agustín Metzquititlán","San Agustín Tlaxiaca","San Bartolo Tutotepec","San Felipe Orizatlán","San Salvador","Santiago de Anaya","Santiago Tulantepec de Lugo Guerre","Singuilucan","Tasquillo","Tecozautla","Tenango de Doria","Tepeapulco","Tepehuacán de Guerrero","Tepeji del Río de Ocampo","Tepetitlán","Tetepango","Tezontepec de Aldama","Tianguistengo","Tizayuca","Tlahuelilpan","Tlahuiltepa","Tlanalapa","Tlanchinol","Tlaxcoapan","Tolcayuca","Tula de Allende","Tulancingo de Bravo","Villa de Tezontepec","Xochiatipan","Xochicoatlán","Yahualica","Zacualtipán de angeles","Zapotlán de Juárez","Zempoala","Zimapán");
$arr_enti = array("Aguascalientes","Baja California","Baja California Sur","Campeche","Chiapas","Chihuahua","Ciudad de México","Coahuila","Colima","Durango","Estado de México","Guanajuato","Guerrero","Hidalgo","Jalisco","Michoacán","Morelos","Nayarit","Nuevo León","Oaxaca","Puebla","Querétaro","Quintana Roo","San Luis Potosí","Sinaloa","Sonora","Tabasco","Tamaulipas","Tlaxcala","Veracruz","Yucatán","Zacatecas");

if($tipo=="array_ids")
{
	$arrayTac = explode(",", $arr_ids_tac);	
 
	// Se crea el objeto PHPExcel
	$objPHPExcel = new PHPExcel();

	// Se asignan las propiedades del libro
	$objPHPExcel->getProperties()->setCreator("Gobierno del Estado de Hidalgo") // Nombre del autor
	    ->setLastModifiedBy("Gobierno del Estado de Hidalgo") //Ultimo usuario que lo modificó
	    ->setTitle("Indicadores por municipio") // Titulo
	    ->setSubject("Indicadores por municipio") //Asunto
	    ->setDescription("Reporte de los indicadores de cada municipio") //Descripción
	    ->setKeywords("Municipios") //Etiquetas
	    ->setCategory("Reporte Excel"); //Categorias

	$titulo = "INDICADORES POR MUNICIPIO";
	$titulosColumnas = array('Indicador','Línea discursiva','Indicador estrella','Descripción del indicador','Unidad de observación','Ficha técnica','Tipo de indicador','Tendencia esperada','Línea base','Reporte primer informe','Meta planeada','Tipo de evidencia','Descripción del documento vinculado','Fecha','Existe expediente','Dependencia','Medios de verificación','Fecha de registro','Comentarios','Estatus','Importancia','Logros','Bullet del logro','Resultado 1','Resultado 2','Resultado 3','Resultado 4','Resultado 5','Lugar 1','Lugar 2','Lugar 3','Lugar 4','Lugar 5','Lo que sigue 1','Lo que sigue 2','Lo que sigue 3','Meta 2018','Meta planteada 2018','Hubo premio','Eje','Objetivo estratégico','Objetivo general','Resultado 2018','Unidad');

	// Se combinan las celdas A1 hasta D1, para colocar ahí el titulo del reporte
	$objPHPExcel->setActiveSheetIndex(0)
	    ->mergeCells('A1:AR1');

	// Se agregan los titulos del reporte
	$objPHPExcel->setActiveSheetIndex(0)
	    ->setCellValue('A1',$titulo) // Titulo del reporte
	    ->setCellValue('A2',$titulosColumnas[0])  //Titulo de las columnas
	    ->setCellValue('B2',$titulosColumnas[1])
	    ->setCellValue('C2',$titulosColumnas[2])
	    ->setCellValue('D2',$titulosColumnas[3])
	    ->setCellValue('E2',$titulosColumnas[4])
	    ->setCellValue('F2',$titulosColumnas[5])
	    ->setCellValue('G2',$titulosColumnas[6])
	    ->setCellValue('H2',$titulosColumnas[7])
	    ->setCellValue('I2',$titulosColumnas[8])
	    ->setCellValue('J2',$titulosColumnas[9])
	    ->setCellValue('K2',$titulosColumnas[10])
	    ->setCellValue('L2',$titulosColumnas[11])
	    ->setCellValue('M2',$titulosColumnas[12])
	    ->setCellValue('N2',$titulosColumnas[13])
	    ->setCellValue('O2',$titulosColumnas[14])
	    ->setCellValue('P2',$titulosColumnas[15])
	    ->setCellValue('Q2',$titulosColumnas[16])
	    ->setCellValue('R2',$titulosColumnas[17])
	    ->setCellValue('S2',$titulosColumnas[18])
	    ->setCellValue('T2',$titulosColumnas[19])
	    ->setCellValue('U2',$titulosColumnas[20])
	    ->setCellValue('V2',$titulosColumnas[21])
	    ->setCellValue('W2',$titulosColumnas[22])
	    ->setCellValue('X2',$titulosColumnas[23])
	    ->setCellValue('Y2',$titulosColumnas[24])
	    ->setCellValue('Z2',$titulosColumnas[25])
	    ->setCellValue('AA2',$titulosColumnas[26])
	    ->setCellValue('AB2',$titulosColumnas[27])
	    ->setCellValue('AC2',$titulosColumnas[28])
	    ->setCellValue('AD2',$titulosColumnas[29])
	    ->setCellValue('AE2',$titulosColumnas[30])
	    ->setCellValue('AF2',$titulosColumnas[31])
	    ->setCellValue('AG2',$titulosColumnas[32])
	    ->setCellValue('AH2',$titulosColumnas[33])
	    ->setCellValue('AI2',$titulosColumnas[34])
	    ->setCellValue('AJ2',$titulosColumnas[35])
	    ->setCellValue('AK2',$titulosColumnas[36])
	    ->setCellValue('AL2',$titulosColumnas[37])
	    ->setCellValue('AM2',$titulosColumnas[38])
	    ->setCellValue('AN2',$titulosColumnas[39])
	    ->setCellValue('AO2',$titulosColumnas[40])
	    ->setCellValue('AP2',$titulosColumnas[41])
	    ->setCellValue('AQ2',$titulosColumnas[42])
	    ->setCellValue('AR2',$titulosColumnas[43]);

    $i = 3; //Numero de fila donde se va a comenzar a rellenar

	for ($s=0; $s < count($arrayTac); $s++)
	{
		$id=$arrayTac[$s];

		$query = "SELECT * FROM wp_definicion_evidencias where id_indicador_asociado_def=$id";
		$resultado = $mysqli->query($query);

		//Se agregan los datos
		while($rowTac = $resultado->fetch_assoc())
		{
			// Conversión de los valores en los arreglos "lug_resultN".
			// LUGAR 1
			if($rowTac['lug_result1']==NULL || $rowTac['lug_result1']=="" || $rowTac['lug_result1']==" ")
			{
				$nom_mun = "";
			}

			else if($rowTac['lug_result1'] == "Hidalgo")
			{
				$nom_mun = "Hidalgo";
			}

			else
			{
				// Elimina la ultima coma del arreglo.
				$cadena_ids_limpia=substr($rowTac['lug_result1'], 0, -1);

				$id_lugar = explode(',',$cadena_ids_limpia);

				if($id_lugar[0] == "Municipios")
				{
					$nom_mun="Municipios";

					for ($a=1; $a < count($id_lugar); $a++)
					{
						$nom_mun = $nom_mun.",".$arr_muni[(int)$id_lugar[$a]];
					}
				}

				else if($id_lugar[0] == "Entidades")
				{
					$nom_mun="Entidades";

					for ($a=1; $a < count($id_lugar); $a++)
					{
						$nom_mun = $nom_mun.",".$arr_enti[(int)$id_lugar[$a]];
					}
				}

				else
				{
					$nom_mun = "Localidades";
				}
			}

			// LUGAR 2
			if($rowTac['lug_result2']==NULL || $rowTac['lug_result2']=="" || $rowTac['lug_result2']==" ")
			{
				$nom_mun2 = "";
			}

			else if($rowTac['lug_result2'] == "Hidalgo")
			{
				$nom_mun2 = "Hidalgo";
			}

			else
			{
				// Elimina la ultima coma del arreglo.
				$cadena_ids_limpia2=substr($rowTac['lug_result2'], 0, -1);

				$id_lugar2 = explode(',',$cadena_ids_limpia2);
				
				if($id_lugar2[0] == "Municipios")
				{
					$nom_mun2="Municipios";

					for ($b=1; $b < count($id_lugar2); $b++)
					{
						$nom_mun2 = $nom_mun2.",".$arr_muni[(int)$id_lugar2[$b]];
					}
				}

				else if($id_lugar2[0] == "Entidades")
				{
					$nom_mun2="Entidades";

					for ($b=1; $b < count($id_lugar2); $b++)
					{
						$nom_mun2 = $nom_mun2.",".$arr_enti[(int)$id_lugar2[$b]];
					}
				}

				else
				{
					$nom_mun2 = "Localidades";
				}
			}

			// LUGAR 3
			if($rowTac['lug_result3']==NULL || $rowTac['lug_result3']=="" || $rowTac['lug_result3']==" ")
			{
				$nom_mun3 = "";
			}

			else if($rowTac['lug_result3'] == "Hidalgo")
			{
				$nom_mun3 = "Hidalgo";
			}

			else
			{
				// Elimina la ultima coma del arreglo.
				$cadena_ids_limpia3=substr($rowTac['lug_result3'], 0, -1);

				$id_lugar3 = explode(',',$cadena_ids_limpia3);

				if($id_lugar3[0] == "Municipios")
				{
					$nom_mun3="Municipios";

					for ($b=1; $b < count($id_lugar3); $b++)
					{
						$nom_mun3 = $nom_mun3.",".$arr_muni[(int)$id_lugar3[$b]];
					}
				}

				else if($id_lugar3[0] == "Entidades")
				{
					$nom_mun3="Entidades";

					for ($b=1; $b < count($id_lugar3); $b++)
					{
						$nom_mun3 = $nom_mun3.",".$arr_enti[(int)$id_lugar3[$b]];
					}
				}

				else
				{
					$nom_mun3 = "Localidades";
				}
			}

			// LUGAR 4
			if($rowTac['lug_result4']==NULL || $rowTac['lug_result4']=="" || $rowTac['lug_result4']==" ")
			{
				$nom_mun4 = "";
			}

			else if($rowTac['lug_result4'] == "Hidalgo")
			{
				$nom_mun4 = "Hidalgo";
			}

			else
			{
				// Elimina la ultima coma del arreglo.
				$cadena_ids_limpia4=substr($rowTac['lug_result4'], 0, -1);

				$id_lugar4 = explode(',',$cadena_ids_limpia4);

				if($id_lugar4[0] == "Municipios")
				{
					$nom_mun4="Municipios";

					for ($b=1; $b < count($id_lugar4); $b++)
					{
						$nom_mun4 = $nom_mun4.",".$arr_muni[(int)$id_lugar4[$b]];
					}
				}

				else if($id_lugar4[0] == "Entidades")
				{
					$nom_mun4="Entidades";

					for ($b=1; $b < count($id_lugar4); $b++)
					{
						$nom_mun4 = $nom_mun4.",".$arr_enti[(int)$id_lugar4[$b]];
					}
				}

				else
				{
					$nom_mun4 = "Localidades";
				}
			}

			// LUGAR 5
			if($rowTac['lug_result5']==NULL || $rowTac['lug_result5']=="" || $rowTac['lug_result5']==" ")
			{
				$nom_mun5 = "";
			}

			else if($rowTac['lug_result5'] == "Hidalgo")
			{
				$nom_mun5 = "Hidalgo";
			}

			else
			{
				// Elimina la ultima coma del arreglo.
				$cadena_ids_limpia5=substr($rowTac['lug_result5'], 0, -1);

				$id_lugar5 = explode(',',$cadena_ids_limpia5);

				if($id_lugar5[0] == "Municipios")
				{
					$nom_mun5="Municipios";

					for ($b=1; $b < count($id_lugar5); $b++)
					{
						$nom_mun5 = $nom_mun5.",".$arr_muni[(int)$id_lugar5[$b]];
					}
				}

				else if($id_lugar5[0] == "Entidades")
				{
					$nom_mun5="Entidades";

					for ($b=1; $b < count($id_lugar5); $b++)
					{
						$nom_mun5 = $nom_mun5.",".$arr_enti[(int)$id_lugar5[$b]];
					}
				}

				else
				{
					$nom_mun5 = "Localidades";
				}
			}

		    $objPHPExcel->setActiveSheetIndex(0)
		        ->setCellValue('A'.$i, utf8_encode($rowTac['nombre_indicador_asociado_def']))
		        ->setCellValue('B'.$i, utf8_encode($rowTac['linea_discursiva']))
		        ->setCellValue('C'.$i, utf8_encode($rowTac['indicador_estrella']))
		        ->setCellValue('D'.$i, utf8_encode($rowTac['descripcion_indicador_def']))
			    ->setCellValue('E'.$i, utf8_encode($rowTac['unidad_observacion_indicador_def']))
			    ->setCellValue('F'.$i, utf8_encode($rowTac['descripcion_observacion_ficha_tecnica_def']))
			    ->setCellValue('G'.$i, utf8_encode($rowTac['tipo_indicador_def']))
			    ->setCellValue('H'.$i, utf8_encode($rowTac['tendencia_esperada_def']))
			    ->setCellValue('I'.$i, utf8_encode($rowTac['linea_base_absolutos_def']))
			    ->setCellValue('J'.$i, utf8_encode($rowTac['reporte_primer_informe_def']))
			    ->setCellValue('K'.$i, utf8_encode($rowTac['avance_meta_planeada_def']))
			    ->setCellValue('L'.$i, utf8_encode($rowTac['tipo_evidencia_def']))
			    ->setCellValue('M'.$i, utf8_encode($rowTac['descripcion_documento_vinculado_indicador_def']))
			    ->setCellValue('N'.$i, utf8_encode($rowTac['tipo_evidencia_fecha_def']))
			    ->setCellValue('O'.$i, utf8_encode($rowTac['existe_expediente_doc_def']))
			    ->setCellValue('P'.$i, utf8_encode($rowTac['nombre_dependencia_def']))
			    ->setCellValue('Q'.$i, utf8_encode($rowTac['medios_verificacion_evidencia_def']))
			    ->setCellValue('R'.$i, utf8_encode($rowTac['fecha_registro']))
			    ->setCellValue('S'.$i, utf8_encode($rowTac['comentarios_def']))
			    ->setCellValue('T'.$i, utf8_encode($rowTac['estatus']))
			    ->setCellValue('U'.$i, utf8_encode($rowTac['discurso_importancia_ind']))
			    ->setCellValue('V'.$i, utf8_encode($rowTac['logros_indicador']))
			    ->setCellValue('W'.$i, utf8_encode($rowTac['bullet_logro']))
			    ->setCellValue('X'.$i, utf8_encode($rowTac['resultado1']))
			    ->setCellValue('Y'.$i, utf8_encode($rowTac['resultado2']))
			    ->setCellValue('Z'.$i, utf8_encode($rowTac['resultado3']))
			    ->setCellValue('AA'.$i, utf8_encode($rowTac['resultado4']))
			    ->setCellValue('AB'.$i, utf8_encode($rowTac['resultado5']))
			    ->setCellValue('AC'.$i, $nom_mun)
			    ->setCellValue('AD'.$i, $nom_mun2)
			    ->setCellValue('AE'.$i, $nom_mun3)
			    ->setCellValue('AF'.$i, $nom_mun4)
			    ->setCellValue('AG'.$i, $nom_mun5)
			    ->setCellValue('AH'.$i, utf8_encode($rowTac['lo_que_sigue_1']))
			    ->setCellValue('AI'.$i, utf8_encode($rowTac['lo_que_sigue_2']))
			    ->setCellValue('AJ'.$i, utf8_encode($rowTac['lo_que_sigue_3']))
			    ->setCellValue('AK'.$i, utf8_encode($rowTac['meta_2018']))
			    ->setCellValue('AL'.$i, utf8_encode($rowTac['meta_planteada_2018']))
			    ->setCellValue('AM'.$i, utf8_encode($rowTac['hubo_premio']))
			    ->setCellValue('AN'.$i, utf8_encode($rowTac['eje_ped']))
			    ->setCellValue('AO'.$i, utf8_encode($rowTac['ob_estrategico']))
			    ->setCellValue('AP'.$i, utf8_encode($rowTac['ob_general']))
			    ->setCellValue('AQ'.$i, utf8_encode($rowTac['resultado_2018_2']))
			    ->setCellValue('AR'.$i, utf8_encode($rowTac['unidad']));
		    $i++;

			$estiloTituloReporte = array(
			    'font' => array(
			        'name'      => 'Arial',
			        'bold'      => true,
			        'italic'    => false,
			        'strike'    => false,
			        'size' =>14,
			        'color'     => array(
			            'rgb' => 'FFFFFF'
			        )
			    ),
			    'fill' => array(
			      'type'  => PHPExcel_Style_Fill::FILL_SOLID,
			      'color' => array(
			            'argb' => 'FF220835')
			  ),
			    'borders' => array(
			        'allborders' => array(
			            'style' => PHPExcel_Style_Border::BORDER_NONE
			        )
			    ),
			    'alignment' => array(
			        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			        'rotation' => 0,
			        'wrap' => TRUE
			    )
			);
			 
			$estiloTituloColumnas = array(
			    'font' => array(
			        'name'  => 'Arial',
			        'bold'  => true,
			        'color' => array(
			            'rgb' => 'FFFFFF'
			        )
			    ),
			    'fill' => array(
			        'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
			  'rotation'   => 90,
			        'startcolor' => array(
			            'rgb' => 'C0E6A5'
			        ),
			        'endcolor' => array(
			            'argb' => '6BBC31'
			        )
			    ),
			    'borders' => array(
			        'top' => array(
			            'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
			            'color' => array(
			                'rgb' => '000000'
			            )
			        ),
			        'bottom' => array(
			            'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
			            'color' => array(
			                'rgb' => '000000'
			            )
			        )
			    ),
			    'alignment' =>  array(
			        'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			        'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			        'wrap'      => TRUE
			    )
			);
			 
			$estiloInformacion = new PHPExcel_Style();
			$estiloInformacion->applyFromArray( array(
			    'font' => array(
			        'name'  => 'Arial',
			        'color' => array(
			            'rgb' => '000000'
			        )
			    ),
			    'fill' => array(
			  'type'  => PHPExcel_Style_Fill::FILL_SOLID,
			  'color' => array(
			            'argb' => 'E4FFD0')
			  ),
			    'borders' => array(
			        'left' => array(
			            'style' => PHPExcel_Style_Border::BORDER_THIN ,
			      'color' => array(
			              'rgb' => '000000'
			            )
			        )
			    )
			));
		}
	}

	$objPHPExcel->getActiveSheet()->getStyle('A1:AR1')->applyFromArray($estiloTituloReporte);
	$objPHPExcel->getActiveSheet()->getStyle('A2:AR2')->applyFromArray($estiloTituloColumnas);
	$objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A3:AR".($i-1));

	for($i = 'A'; $i <= 'AR'; $i++){
	    $objPHPExcel->setActiveSheetIndex(0)->getColumnDimension($i)->setAutoSize(TRUE);
	}

	// Se asigna el nombre a la hoja
	$objPHPExcel->getActiveSheet()->setTitle('Indicadores por municipio');
	 
	// Se activa la hoja para que sea la que se muestre cuando el archivo se abre
	$objPHPExcel->setActiveSheetIndex(0);
	 
	// Inmovilizar paneles
	//$objPHPExcel->getActiveSheet(0)->freezePane('A4');
	$objPHPExcel->getActiveSheet(0)->freezePaneByColumnAndRow(0,3);

	// Se manda el archivo al navegador web, con el nombre que se indica, en formato 2007
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Indicadores por municipio.xlsx"');
	header('Cache-Control: max-age=0');
	 
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit;
}

?>